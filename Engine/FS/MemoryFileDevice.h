//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MemoryFileDevice.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "IStreamDevice.h"
#include "FileSystem.h"
#include "../Math/MathUtils.h"
#include "../Data/Streams.h"
#include "../Data/MemoryStream.h"

namespace Neko
{
	class IAllocator;
    class IStream;
    
	namespace FS
	{
		class NEKO_ENGINE_API MemoryFileDevice final : public IStreamDevice
		{
		public:
            
			explicit MemoryFileDevice(IAllocator& allocator)
            : Allocator(allocator)
            { }

			void DestroyFile(IStream* file) override;
			IStream* CreateFile(IStream* child) override;

			const char* name() const override { return "memory"; }

		private:
            
			IAllocator& Allocator;
		};

	} // ~namespace FS
} // ~namespace Neko
