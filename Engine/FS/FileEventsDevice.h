//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  FileEventsDevice.h
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#define FILE_EVENT_DEVICE

#ifdef FILE_EVENT_DEVICE

#include "../Containers/Delegate.h"
#include "../Neko.h"
#include "IStreamDevice.h"

namespace Neko
{
    class IAllocator;
    
    namespace FS
    {
        enum class EventType
        {
            OPEN_BEGIN = 0,
            OPEN_FINISHED,
            CLOSE_BEGIN,
            CLOSE_FINISHED,
            READ_BEGIN,
            READ_FINISHED,
            WRITE_BEGIN,
            WRITE_FINISHED,
            SIZE_BEGIN,
            SIZE_FINISHED,
            SEEK_BEGIN,
            SEEK_FINISHED,
            POS_BEGIN,
            POS_FINISHED
        };
        
        struct Event
        {
            EventType type;
            uptr handle;
            const char* path;
            int32 ret;
            int32 param;
        };
        
        class NEKO_ENGINE_API FileEventsDevice final : public IStreamDevice
        {
        public:
            FileEventsDevice(IAllocator& allocator)
            : Allocator(allocator)
            {
                
            }
            
            typedef TDelegate<void(const Event&)>  EventCallback;
            
            EventCallback OnEvent;
            
            void DestroyFile(IStream* file) override;
            IStream* CreateFile(IStream* child) override;
            
            NEKO_FORCE_INLINE const char* name() const override
            {
                return "events";
            }
            
        private:
            IAllocator& Allocator;
        };
    }
} // ~namespace Neko

#endif // FILE_EVENT_DEVICE
