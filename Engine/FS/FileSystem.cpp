//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  FileSystem.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "FileSystem.h"
#include "DiskFileDevice.h"
#include "../Containers/Array.h"
#include "../Data/BaseProxyAllocator.h"
#include "../Data/Blob.h"
#include "../Mt/LockFreeFixedQueue.h"
#include "../Mt/Task.h"
#include "../Mt/Transaction.h"
#include "../Core/Path.h"
#include "../Core/Profiler.h"
#include "../Containers/Queue.h"
#include "../Data/Streams.h"
#include "../Utilities/StringUtil.h"

namespace Neko
{
    namespace FS
    {
        /** Asynchronous transaction state flags. */
        enum TransFlags
        {
            E_CLOSE = 0,
            E_SUCCESS = 0x1,
            E_IS_OPEN = E_SUCCESS << 1,
#undef E_FAIL
            E_FAIL = E_IS_OPEN << 1,
            E_CANCELED = E_FAIL << 1
        };
        
        /** Asynchronous object. */
        struct AsyncItem
        {
            IStream* pFile;
            ReadCallback Callback;
            uint32 Id;
            Mode Mode;
            char Path[MAX_PATH_LENGTH];
            uint8 Flags;
        };
        
        //! Maximum amount of asynchronous actions.
        static const int32 C_MAX_TRANS = 16;
        
        typedef MT::Transaction<AsyncItem> AsynTrans;
        typedef MT::LockFreeFixedQueue<AsynTrans, C_MAX_TRANS> TransQueue;
        typedef Queue<AsynTrans*, C_MAX_TRANS> InProgressQueue;
        typedef TArray<AsyncItem> ItemsTable;
        typedef TArray<IStreamDevice*> DevicesTable;
        
        /** Task used to load items asynchronously. */
        class FSTask final : public MT::Task
        {
        public:
            
            FSTask(TransQueue* queue, IAllocator& allocator)
            : MT::Task(allocator)
            , TransactionQueue(queue)
            {
                
            }
            
            ~FSTask()
            {
                
            }
            
            int32 DoTask()
            {
                while (!TransactionQueue->isAborted())
                {
                    PROFILE_SECTION("transaction");
                    auto tr = TransactionQueue->Pop(true);
                    if (!tr)
                    {
                        break;
                    }
                    
                    if ((tr->data.Flags & E_IS_OPEN) == E_IS_OPEN)
                    {
                        tr->data.Flags |= tr->data.pFile->Open(CPath(tr->data.Path), tr->data.Mode) ? E_SUCCESS : E_FAIL;
                    }
                    else if ((tr->data.Flags & E_CLOSE) == E_CLOSE)
                    {
                        tr->data.pFile->Close();
                        tr->data.pFile->Release();
                        tr->data.pFile = nullptr;
                    }
                    tr->SetCompleted();
                }
                return 0;
            }
            
            NEKO_FORCE_INLINE void stop()
            {
                TransactionQueue->abort();
            }
            
        private:
            TransQueue* TransactionQueue;
        };
        
        /// Implementation for filesystem interface.
        class FileSystemImpl final : public FileSystem
        {
        public:
            
            explicit FileSystemImpl(IAllocator& allocator)
            : Allocator(allocator)
            , Devices(allocator)
            , Pending(allocator)
            , InProgress(allocator)
            , LastId(0)
            {
                DiskDevice.Devices[0] = nullptr;
                MemoryDevice.Devices[0] = nullptr;
                DefaultDevice.Devices[0] = nullptr;
                SaveGameDevice.Devices[0] = nullptr;
                
                pTask = NEKO_NEW(Allocator, FSTask)(&TransactionQueue, Allocator);
                pTask->Create("FSTask");
            }
            
            ~FileSystemImpl()
            {
                pTask->stop();
                pTask->Destroy();
                NEKO_DELETE(Allocator, pTask);
                
                while (!InProgress.IsEmpty())
                {
                    auto* trans = InProgress.front();
                    InProgress.Pop();
                    if (trans->data.pFile)
                    {
                        Close(*trans->data.pFile);
                    }
                }
                for (auto& i : Pending)
                {
                    Close(*i.pFile);
                }
            }
            
            NEKO_FORCE_INLINE BaseProxyAllocator& GetAllocator()
            {
                return Allocator;
            }
            
            NEKO_FORCE_INLINE bool HasWork() const override
            {
                return !InProgress.IsEmpty() || !Pending.IsEmpty();
            }
            
            NEKO_FORCE_INLINE void FlushAsyncLoading() override
            {
                while (HasWork())
                {
                    UpdateAsyncActions();
                }
            }
            
            bool Mount(IStreamDevice* device) override
            {
                for (int32 i = 0; i < Devices.GetSize(); ++i)
                {
                    if (Devices[i] == device)
                    {
                        return false;
                    }
                }
                
                if (EqualStrings(device->name(), "memory"))
                {
                    MemoryDevice.Devices[0] = device;
                    MemoryDevice.Devices[1] = nullptr;
                }
                else if (EqualStrings(device->name(), "disk"))
                {
                    DiskDevice.Devices[0] = device;
                    DiskDevice.Devices[1] = nullptr;
                }
                
                Devices.Push(device);
                return true;
            }
            
            bool Unmount(IStreamDevice* device) override
            {
                for (int32 i = 0; i < Devices.GetSize(); ++i)
                {
                    if (Devices[i] == device)
                    {
                        Devices.EraseFast(i);
                        return true;
                    }
                }
                
                return false;
            }
            
            IStream* CreateFile(const DeviceList& device_list)
            {
                IStream* prev = nullptr;
                for (int32 i = 0; i < lengthOf(device_list.Devices); ++i)
                {
                    if (!device_list.Devices[i])
                    {
                        break;
                    }
                    prev = device_list.Devices[i]->CreateFile(prev);
                }
                return prev;
            }
            
            IStream* Open(const DeviceList& device_list, const CPath& file, Mode mode) override
            {
                IStream* prev = CreateFile(device_list);
                
                if (prev)
                {
                    if (prev->Open(file, mode))
                    {
                        return prev;
                    }
                    else
                    {
                        prev->Release();
                        return nullptr;
                    }
                }
                return nullptr;
            }
            
            uint32 OpenAsync(const DeviceList& device_list, const CPath& file, int mode, const ReadCallback& call_back) override
            {
                IStream* prev = CreateFile(device_list);
                
                if (prev)
                {
                    AsyncItem& item = Pending.Emplace();
                    
                    item.pFile = prev;
                    item.Callback = call_back;
                    item.Mode = mode;
                    CopyString(item.Path, file.c_str());
                    item.Flags = E_IS_OPEN;
                    
                    item.Id = LastId;
                    ++LastId;
                    if (LastId == INVALID_ASYNC)
                    {
                        LastId = 0;
                    }
                    return item.Id;
                }
                
                return INVALID_ASYNC;
            }
            
            void CancelAsync(uint32 id) override
            {
                if (id == INVALID_ASYNC)
                {
                    return;
                }
                
                for (int32 i = 0, c = Pending.GetSize(); i < c; ++i)
                {
                    if (Pending[i].Id == id)
                    {
                        Pending[i].Flags |= E_CANCELED;
                        return;
                    }
                }
                
                for (auto iter = InProgress.begin(), end = InProgress.end(); iter != end; ++iter)
                {
                    if (iter.value()->data.Id == id)
                    {
                        iter.value()->data.Flags |= E_CANCELED;
                        return;
                    }
                }
                
            }
            
            void SetDefaultDevice(const char* dev) override
            {
                FillDeviceList(dev, DefaultDevice);
            }
            
            void FillDeviceList(const char* dev, DeviceList& device_list) override
            {
                const char* token = nullptr;
                
                int32 device_index = 0;
                const char* end = dev + StringLength(dev);
                
                while (end > dev)
                {
                    token = ReverseFind(dev, token, ':');
                    char device[32];
                    if (token)
                    {
                        CopyNString(device, (int32)sizeof(device), token + 1, int32(end - token - 1));
                    }
                    else
                    {
                        CopyNString(device, (int32)sizeof(device), dev, int32(end - dev));
                    }
                    end = token;
                    device_list.Devices[device_index] = GetDevice(device);
                    assert(device_list.Devices[device_index]);
                    ++device_index;
                }
                device_list.Devices[device_index] = nullptr;
            }
            
            const DeviceList& GetMemoryDevice() const override
            {
                return MemoryDevice;
            }
            const DeviceList& GetDiskDevice() const override
            {
                return DiskDevice;
            }
            void SetSaveGameDevice(const char* dev) override
            {
                FillDeviceList(dev, SaveGameDevice);
            }
            
            void Close(IStream& file) override
            {
                file.Close();
                file.Release();
            }
            
            void CloseAsync(IStream& file) override
            {
                AsyncItem& item = Pending.Emplace();
                
                item.pFile = &file;
                item.Callback.Bind<CloseAsync>();
                item.Mode = 0;
                item.Flags = E_CLOSE;
            }
            
            void UpdateAsyncActions() override
            {
                PROFILE_FUNCTION();
                while (!InProgress.IsEmpty())
                {
                    AsynTrans* tr = InProgress.front();
                    if (!tr->IsCompleted()) break;
                    
                    PROFILE_SECTION("processAsyncTransaction");
                    InProgress.Pop();
                    
                    if ((tr->data.Flags & E_CANCELED) == 0)
                    {
                        tr->data.Callback.Invoke(*tr->data.pFile, !!(tr->data.Flags & E_SUCCESS));
                    }
                    if ((tr->data.Flags & (E_SUCCESS | E_FAIL)) != 0)
                    {
                        CloseAsync(*tr->data.pFile);
                    }
                    TransactionQueue.dealoc(tr);
                }
                
                int32 can_add = C_MAX_TRANS - InProgress.GetSize();
                while (can_add && !Pending.IsEmpty())
                {
                    AsynTrans* tr = TransactionQueue.Alloc(false);
                    if (tr)
                    {
                        AsyncItem& item = Pending[0];
                        tr->data.pFile = item.pFile;
                        tr->data.Callback = item.Callback;
                        tr->data.Id = item.Id;
                        tr->data.Mode = item.Mode;
                        CopyString(tr->data.Path, sizeof(tr->data.Path), item.Path);
                        tr->data.Flags = item.Flags;
                        tr->Reset();
                        
                        TransactionQueue.Push(tr, true);
                        InProgress.Push(tr);
                        Pending.Erase(0);
                    }
                    can_add--;
                }
            }
            
            const DeviceList& GetDefaultDevice() const override { return DefaultDevice; }
            
            const DeviceList& GetSaveGameDevice() const override { return SaveGameDevice; }
            
            IStreamDevice* GetDevice(const char* device)
            {
                for (int32 i = 0; i < Devices.GetSize(); ++i)
                {
                    if (EqualStrings(Devices[i]->name(), device))
                    {
                        return Devices[i];
                    }
                }
                
                return nullptr;
            }
            
            static void CloseAsync(IStream&, bool) { }
            
        private:
            
            BaseProxyAllocator Allocator;
            
            FSTask* pTask;
            
            DevicesTable Devices;
            
            ItemsTable Pending;
            TransQueue TransactionQueue;
            InProgressQueue InProgress;
            
            uint32 LastId;
            
            DeviceList DiskDevice;
            DeviceList MemoryDevice;
            DeviceList DefaultDevice;
            DeviceList SaveGameDevice;
        };
        
        FileSystem* FileSystem::Create(IAllocator& allocator)
        {
            return NEKO_NEW(allocator, FileSystemImpl)(allocator);
        }
        
        void FileSystem::Destroy(FileSystem* fs)
        {
            NEKO_DELETE(static_cast<FileSystemImpl*>(fs)->GetAllocator().GetSourceAllocator(), fs);
        }
    }
} // namespace Neko
