//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  PlatformFile.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Data/Streams.h"
#include "FileSystem.h"

namespace Neko
{
	class IAllocator;

	namespace FS
	{
        /** Interface for system file handle. */
		class NEKO_ENGINE_API CPlatformFile
		{
		public:
            
			CPlatformFile();
            
			~CPlatformFile();

            /** Opens file at the path. */
			bool Open(const char* path, Mode mode);
            /** Flushes and closes the file. */
			void Close();
            
            /** fflush */
			void Flush();

			size_t Write(const void* data, size_t size);
			size_t WriteText(const char* text);
			size_t Read(void* data, size_t size);

			size_t GetSize();
			size_t Pos();

            bool IsEof() const;
            size_t Tell() const;
            
			bool Seek(ESeekLocator base, size_t pos);

            // Operators
            
			CPlatformFile& operator << (const char* text);
			CPlatformFile& operator << (char c) { Write(&c, sizeof(c)); return *this; }
			CPlatformFile& operator << (int32 value);
			CPlatformFile& operator << (uint32 value);
			CPlatformFile& operator << (uint64 value);
			CPlatformFile& operator << (float value);

		private:
            
			FILE* pHandle;
		};
	} // ~namespace FS
} // ~namespace Neko
