//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  PackFileDevice.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "IStreamDevice.h"
#include "../FS/PlatformFile.h"
#include "../Containers/HashMap.h"
#include "../Neko.h"

namespace Neko
{
    class IAllocator;
    class IStream;
    
    namespace FS
    {
        class NEKO_ENGINE_API PackFileDevice final : public IStreamDevice
        {
            friend class PackFile;
        public:
            
            PackFileDevice(IAllocator& allocator);
            ~PackFileDevice();
            
            IStream* CreateFile(IStream* child) override;
            void DestroyFile(IStream* file) override;
            
            const char* name() const override { return "pack"; }
            
            bool Mount(const char* path);
            
        private:
            
            struct PackFileInfo
            {
                uint64 offset;
                uint64 size;
            };
            
            THashMap<uint32, PackFileInfo> Files;
            size_t Offset;
            CPlatformFile pFile;
            IAllocator& Allocator;
        };
        
    } // namespace FS
} // namespace Neko
