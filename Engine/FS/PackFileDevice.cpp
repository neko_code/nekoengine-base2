#include "FileSystem.h"
#include "../Data/IAllocator.h"
#include "../Data/Streams.h"
#include "../Core/Path.h"
#include "../Utilities/StringUtil.h"
#include "PackFileDevice.h"

namespace Neko
{
    namespace FS
    {
        class PackFile final : public IStream
        {
        public:
            
            PackFile(PackFileDevice& device, IAllocator& allocator)
            : Device(device)
            , LocalOffset(0)
            , Allocator(allocator)
            { }
            
            bool Open(const CPath& path, Mode mode) override
            {
                auto iter = Device.Files.Find(path.GetHash());
                if (iter == Device.Files.end())
                {
                    return false;
                }
                
                pFile = iter.value();
                LocalOffset = 0;
                return Device.pFile.Seek(ESeekLocator::Begin, (size_t)pFile.offset);
            }
            
            size_t Read(void* buffer, const size_t size) override
            {
                if (Device.Offset != pFile.offset + LocalOffset)
                {
                    if (!Device.pFile.Seek(ESeekLocator::Begin, size_t(pFile.offset + LocalOffset)))
                    {
                        return 0;
                    }
                }
                LocalOffset += size;
                return Device.pFile.Read(buffer, size);
            }
            
            bool Seek(size_t Pos, ESeekLocator Locator) override
            {
                LocalOffset = Pos;
                return Device.pFile.Seek(ESeekLocator::Begin, size_t(pFile.offset + Pos));
            }
            
            virtual IAllocator* GetAllocator() override
            {
                return &Allocator;
            }
            
            IStreamDevice* GetDevice() override
            {
                return &Device;
            }
            
            void Close() override
            {
                LocalOffset = 0;
            }
            
            size_t Write(const void* buffer, const size_t size) override
            {
                assert(false);
                return 0;
            }
            
            const void* GetBuffer() const override
            {
                return nullptr;
            }
            
            size_t GetSize() override
            {
                return (size_t)pFile.size;
            }
            
            size_t GetCurrentPos() override
            {
                return LocalOffset;
            }
            
            uint8* GetCurrentPtr() override
            {
                assert(false);
                return nullptr;
            }
            
            size_t Tell() const override
            {
                assert(false);
                return 0;
            }
            
            bool IsEof() const override
            {
                assert(false);
                return false;
            }
            
            bool IsFile() const override
            {
                return true;
            }
            
        private:
            
            virtual ~PackFile() { }
            
            PackFileDevice::PackFileInfo pFile;
            PackFileDevice& Device;
            
            size_t LocalOffset;
            
            IAllocator& Allocator;
        }; // class PackFile
        
        
        PackFileDevice::PackFileDevice(IAllocator& allocator)
        : Files(allocator)
        , Allocator(allocator)
        {
        }
        
        PackFileDevice::~PackFileDevice()
        {
            pFile.Close();
        }
        
        bool PackFileDevice::Mount(const char* path)
        {
            pFile.Close();
            if (!pFile.Open(path, Mode::OPEN_AND_READ))
            {
                return false;
            }
            
            int32 count;
            pFile.Read(&count, sizeof(count));
            for(int i = 0; i < count; ++i)
            {
                uint32 hash;
                pFile.Read(&hash, sizeof(hash));
                PackFileInfo info;
                pFile.Read(&info, sizeof(info));
                Files.Insert(hash, info);
            }
            Offset = pFile.Pos();
            return true;
        }
        
        void PackFileDevice::DestroyFile(IStream* file)
        {
            NEKO_DELETE(Allocator, file);
        }
        
        IStream* PackFileDevice::CreateFile(IStream*)
        {
            return NEKO_NEW(Allocator, PackFile)(*this, Allocator);
        }
    } // namespace FS
} // namespace Neko
