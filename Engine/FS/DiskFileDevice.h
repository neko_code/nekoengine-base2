//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  DiskFileDevice.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../FS/FileSystem.h"
#include "../FS/IStreamDevice.h"
#include "../FS/PlatformFile.h"
#include "../Platform/Platform.h"
#include "../Data/Streams.h"
#include "../Neko.h"

namespace Neko
{
    class IAllocator;
    class IStream;
    namespace FS
    {
        class NEKO_ENGINE_API DiskFileDevice final : public IStreamDevice
        {
        public:
            
            DiskFileDevice(const char* Name, const char* BasePath, IAllocator& Allocator);
            
            IStream* CreateFile(IStream* Child) override;
            
            void DestroyFile(IStream* File) override;
            
            
            NEKO_FORCE_INLINE const char* GetBasePath() const { return Basepath; }
            
            void SetBasePath(const char* Path);
            
            NEKO_FORCE_INLINE const char* name() const override { return Name; }
            
        private:
            
            IAllocator& Allocator;
            char Basepath[MAX_PATH_LENGTH];
            char Name[20];
        };
        
        
        struct DiskFile final : public IStream
        {
            DiskFile(IStream* fallthrough, DiskFileDevice& device, IAllocator& allocator)
            : Device(device)
            , Allocator(allocator)
            , MaskFile(fallthrough)
            {
                bUsesMaskFile = false;
            }
            
            ~DiskFile()
            {
                if (MaskFile)
                {
                    MaskFile->Release();
                }
            }
            
            virtual IAllocator* GetAllocator() override
            {
                return &Allocator;
            }
            
            IStreamDevice* GetDevice() override
            {
                return &Device;
            }
            
            bool Open(const CPath& path, Mode mode) override
            {
                char tmp[MAX_PATH_LENGTH];
                if (path.Length() > 1
#if NEKO_WINDOWS
                    && path.c_str()[1] == ':') // 'C:' etc..
#elif NEKO_OSX      // Absolute are identified by '/'
                    && StartsWith(path.c_str(), "/Users"))
#elif NEKO_LINUX_FAMILY
                    && path.c_str()[0] == '/')
#endif
                {
                    CopyString(tmp, path.c_str());
                }
                else
                {
                    CopyString(tmp, Device.GetBasePath());
                    CatString(tmp, path.c_str());
                }
                
                const bool wantRead = (mode & Mode::READ) != 0;
                if (wantRead && !Neko::Platform::FileExists(tmp) && MaskFile)
                {
                    bUsesMaskFile = true;
                    return MaskFile->Open(path, mode);
                }
                return File.Open(tmp, mode);
            }
            
            void Close() override
            {
                File.Flush();
                if (MaskFile)
                {
                    MaskFile->Close();
                }
                File.Close();
                bUsesMaskFile = false;
            }
            
            size_t Read(void* OutBuffer, const size_t Size) override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->Read(OutBuffer, Size);
                }
                return File.Read(OutBuffer, Size);
            }
            
            size_t Write(const void* InBuffer, const size_t Size) override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->Write(InBuffer, Size);
                }
                return File.Write(InBuffer, Size);
            }
            
            
            const void* GetBuffer() const override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->GetBuffer();
                }
                return nullptr;
            }
            
            size_t GetSize() override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->GetSize();
                }
                return File.GetSize();
            }
            
            bool Seek(size_t Offset, ESeekLocator Locator) override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->Seek(Offset, Locator);
                }
                return File.Seek(Locator, Offset);
            }
            
            size_t GetCurrentPos() override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->GetCurrentPos();
                }
                return File.Pos();
            }
            
            uint8* GetCurrentPtr() override
            {
                assert(false);
                return nullptr;
            }
            
            size_t Tell() const override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->Tell();
                }
                return File.Tell();
            }
            
            bool IsEof() const override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->IsEof();
                }
                return File.IsEof();
            }
            
            bool IsFile() const override
            {
                if (bUsesMaskFile)
                {
                    return MaskFile->IsFile();
                }
                return true;    // our main handle is a system file
            }
            
            DiskFileDevice& Device;
            IAllocator& Allocator;
            CPlatformFile File;
            IStream* MaskFile;
            
            bool bUsesMaskFile : 1;
        };
    } // FS
} // Neko
