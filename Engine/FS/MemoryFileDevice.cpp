//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MemoryFileDevice.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "MemoryFileDevice.h"
#include "../Data/IAllocator.h"
#include "FileSystem.h"
#include "../Math/MathUtils.h"
#include "../Utilities/StringUtil.h"
#include "../Data/Streams.h"

namespace Neko
{
	namespace FS
	{
        /// Memory stream created from any other stream. Capable of reading and writing.
        class NEKO_ENGINE_API MemoryFile final : public IStream
        {
        public:
            MemoryFile(IStream* file, MemoryFileDevice& device, IAllocator& allocator)
            : Allocator(allocator)
            , Device(device)
            , Buffer(nullptr)
            , Stream(nullptr, 0, allocator, true)
            , pFile(file)
            , bIsWrite(false)
            {
            }
            
            ~MemoryFile()
            {
                if (pFile)
                {
                    pFile->Release();
                }
            }
            
            NEKO_FORCE_INLINE IStreamDevice* GetDevice() override
            {
                return &Device;
            }
            
            bool Open(const CPath& path, Mode mode) override
            {
                assert(!Buffer); // reopen is not supported currently
                
                bIsWrite = !!(mode & Mode::WRITE);
                if (pFile)
                {
                    if (pFile->Open(path, mode))
                    {
                        if (mode & Mode::READ)
                        {
                            Stream.Capacity = Stream.ReadSize = pFile->GetSize();
                            Buffer = (uint8*)Allocator.Allocate(sizeof(uint8) * Stream.ReadSize);
                            
                            // Read contents of the file stream
                            pFile->Read(Buffer, Stream.ReadSize);
                            Stream.Buffer = Buffer;
                            Stream.LastPos = 0;
                        }
                        
                        return true;
                    }
                }
                else
                {
                    if (mode & Mode::WRITE)
                    {
                        return true;
                    }
                }
                
                return false;
            }
            
            void Close() override
            {
                if (pFile)
                {
                    if (bIsWrite)
                    {
                        assert(Stream.GetBuffer());
                        // Set the start position and write all collected data
                        pFile->Seek(0, ESeekLocator::Begin);
                        pFile->Write(Stream.GetBuffer(), Stream.ReadSize);
                    }
                    pFile->Close();
                }
                
                Stream.Close();
                Buffer = nullptr;
            }
            
            size_t Read(void* buffer, const size_t size) override { return Stream.Read(buffer, size); /*amount == size;*/ }
            
            size_t Write(const void* buffer, const size_t size) override { return Stream.Write(buffer, size); }
            
            const void* GetBuffer() const override { return Stream.GetBuffer(); }
            
            uint8* GetCurrentPtr() override { return Stream.GetCurrentPtr(); }
            
            size_t GetSize() override { return Stream.ReadSize; }
            
            bool Seek(size_t pos, ESeekLocator Locator) override { return Stream.Seek(pos, Locator); }
            
            size_t Tell() const override { return Stream.Tell(); }
            
            size_t GetCurrentPos() override { return Stream.LastPos; }
            
            bool IsEof() const override { return Stream.LastPos >= Stream.ReadSize; }
            
            bool IsFile() const override { /* assert(false); */ return false; }
            
            virtual IAllocator* GetAllocator() override { return Stream.GetAllocator(); }
            
        private:
            
            IAllocator& Allocator;
            MemoryFileDevice& Device;
            
            uint8* Buffer;
            CMemoryStream Stream;
            IStream* pFile;
            
            bool bIsWrite;
        };

		void MemoryFileDevice::DestroyFile(IStream* file)
		{
			NEKO_DELETE(Allocator, file);
		}

		IStream* MemoryFileDevice::CreateFile(IStream* child)
		{
			return NEKO_NEW(Allocator, MemoryFile)(child, *this, Allocator);
		}
	} // ~namespace FS
} // ~namespace Neko
