//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ResourceFileDevice.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#include "ResourceFileDevice.h"
#include "FileSystem.h"
#include "../Data/IAllocator.h"
#include "../Data/Streams.h"
#include "../Math/MathUtils.h"
#include "../Core/Path.h"
#include "../Utilities/StringUtil.h"
#include <mf_resource.h>

namespace Neko
{
    namespace FS
    {
        class NEKO_ENGINE_API ResourceFile final : public IStream
        {
        public:
            
            ResourceFile(IStream* file, ResourceFileDevice& device, IAllocator& allocator)
            : Device(device)
//            , Allocator(allocator)
            , pResource(nullptr)
            {
                assert(file == nullptr);
            }
            
            ~ResourceFile()
            { }
            
            IStreamDevice* GetDevice() override { return &Device; }
            
            bool Open(const CPath& path, Mode mode) override
            {
                assert(!pResource); // reopen is not supported currently
                assert((mode & Mode::WRITE) == 0);
                
                if ((mode & Mode::WRITE) != 0)
                {
                    return false;
                }
                
                int count = mf_get_all_resources_count();
                const mf_resource* resources = mf_get_all_resources();
                pResource = nullptr;
                for (int i = 0; i < count; ++i)
                {
                    const mf_resource* res = &resources[i];
                    if (Neko::EqualIStrings(path.c_str(), res->path))
                    {
                        pResource = res;
                        break;
                    }
                }
                if (!pResource)
                {
                    return false;
                }
                Pos = 0;
                return true;
            }
            
            void Close() override
            {
                pResource = nullptr;
            }
            
            size_t Read(void* buffer, size_t size) override
            {
                size_t amount = Pos + size < pResource->size ? size : pResource->size - Pos;
                CopyMemory(buffer, pResource->value + Pos, (int)amount);
                Pos += amount;
                return amount;
            }
            
            size_t Write(const void* buffer, size_t size) override
            {
                assert(false);
                return false;
            }
            
            const void* GetBuffer() const override { return pResource->value; }
            
            size_t GetSize() override { return pResource->size; }
            
            bool Seek(size_t pos, ESeekLocator base) override
            {
                switch (base)
                {
                    case ESeekLocator::Begin:
                        assert(pos <= (int32)pResource->size);
                        Pos = pos;
                        break;
                    case ESeekLocator::Current:
                        assert(0 <= int32(Pos + pos) && int32(Pos + pos) <= int32(pResource->size));
                        Pos += pos;
                        break;
                    case ESeekLocator::End:
                        assert(pos <= (int32)pResource->size);
                        Pos = pResource->size - pos;
                        break;
                    default: assert(0); break;
                }
                
                bool ret = Pos <= pResource->size;
                Pos = Math::Minimum(Pos, pResource->size);
                return ret;
            }
            
            size_t GetCurrentPos() override { return Pos; }
            
        private:
//            IAllocator& Allocator;
            ResourceFileDevice& Device;
            const mf_resource* pResource;
            size_t Pos;
        };
        
        
        const mf_resource* ResourceFileDevice::GetResource(int index) const
        {
            return &mf_get_all_resources()[index];
        }
        
        void ResourceFileDevice::DestroyFile(IStream* file)
        {
            NEKO_DELETE(Allocator, file);
        }
        
        int ResourceFileDevice::GetResourceFilesCount() const
        {
            return mf_get_all_resources_count();
        }
        
#undef CreateFile
        IStream* ResourceFileDevice::CreateFile(IStream* child)
        {
            return NEKO_NEW(Allocator, ResourceFile)(child, *this, Allocator);
        }
        
    } // namespace FS
} // namespace Neko
