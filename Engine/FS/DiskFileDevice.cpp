//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  DiskFileDevice.cpp
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#include "../Data/IAllocator.h"
#include "../Data/Streams.h"
#include "../Core/Path.h"
#include "../Core/PathUtils.h"
#include "../Utilities/StringUtil.h"
#include "DiskFileDevice.h"
#include "FileSystem.h"
#include "PlatformFile.h"

namespace Neko
{
    namespace FS
    {
        DiskFileDevice::DiskFileDevice(const char* InName, const char* InBasepath, IAllocator& InAllocator)
        : Allocator(InAllocator)
        {
            CopyString(Name, InName);
            PathUtils::Normalize(InBasepath, Basepath, Neko::lengthOf(Basepath));
            
            if (Basepath[0] != '\0')
            {
                CatString(Basepath, "/");
            }
        }
        
        
        void DiskFileDevice::SetBasePath(const char* path)
        {
            PathUtils::Normalize(path, Basepath, Neko::lengthOf(Basepath));
            
            if (Basepath[0] != '\0')
            {
                CatString(Basepath, "/");
            }
        }
        
        
        void DiskFileDevice::DestroyFile(IStream* file)
        {
            NEKO_DELETE(Allocator, file);
        }
        
        
        IStream* DiskFileDevice::CreateFile(IStream* fallthrough)
        {
            return NEKO_NEW(Allocator, DiskFile)(fallthrough, *this, Allocator);
        }
    } // namespace FS
} // ~namespace Neko
