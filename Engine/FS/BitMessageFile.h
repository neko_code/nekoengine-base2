//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  BitMessageFile.h
//  Neko engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#pragma once

#include "../Containers/BitMessage.h"
#include "../Data/Streams.h"
#include "../Core/Log.h"
#include "../Neko.h"
#include "FileSystem.h"

namespace Neko
{
    // @todo Device system implementation?
    
    /// Stream initialized from bit message.
    class NEKO_ENGINE_API CBitMessageStream final : public IStream
    {
    public:
        
        CBitMessageStream(CBitMessage& message);
        
        CBitMessageStream(const CBitMessage& message);
        
        virtual ~CBitMessageStream();
        
        NEKO_FORCE_INLINE FS::IStreamDevice* GetDevice() override
        {
            return nullptr;
        }
        
        bool Open(const CPath& path, FS::Mode mode) override
        {
            bWrite = !!(mode & FS::Mode::WRITE);
            return true;
        }
        
        void Close() override
        {
            
        }
        
        
        size_t Read(void* buffer, const size_t size) override
        {
            if (bWrite)
            {
                // BitMessageStream::Read: not opened in read mode
                assert(false);
                return 0;
            }
            
            return Message->ReadData(buffer, static_cast<int32>(size));
        }
        
        
        size_t Write(const void* buffer, const size_t size) override
        {
            if (!bWrite)
            {
                // BitMessageStream::Write: not opened in write mode
                assert(false);
                return 0;
            }
            
            Message->WriteData(buffer, static_cast<int32>(size));
            
            return size;
        }
        
        
        NEKO_FORCE_INLINE const void* GetBuffer() const override
        {
            if (bWrite)
            {
                return Message->GetWriteData();
            }
            else
            {
                return Message->GetReadData();
            }
        }
        
        
        NEKO_FORCE_INLINE uint8* GetCurrentPtr() override
        {
            if (bWrite)
            {
                return Message->GetWriteData() + Message->GetReadCount();
            }
            else
            {
                return Message->GetReadData() + Message->GetReadCount();
            }
        }
        
        
        NEKO_FORCE_INLINE size_t GetSize() override
        {
            return Message->GetSize();
        }
        
        
        bool Seek(size_t pos, ESeekLocator Locator) override
        {
            return false;
        }
        
        
        size_t Tell() const override
        {
            if (!bWrite)
            {
                return Message->GetReadCount();
            }
            else
            {
                return Message->GetSize();
            }
        }
        
        
        size_t GetCurrentPos() override
        {
            return Message->GetReadCount();
        }
        
        
        bool IsEof() const override
        {
            return Message->IsOverflowed();
        }
        
        
        bool IsFile() const override
        {
            assert(false);
            return false;
        }
        
        
        virtual IAllocator* GetAllocator() override
        {
            return nullptr;
        }
        
    private:
        
        CBitMessage* Message;
        
        bool bWrite;
    };
} // Neko
