//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  FileSystem.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Platform/SystemShared.h"
#include "../Containers/Delegate.h"
#include "../Utilities/Timer.h"
#include "../Neko.h"

#if NEKO_UNIX_FAMILY
#   define PATH_SEPARATOR "/"
#	define fseeko64 fseeko
#	define ftello64 ftello
#else
#   define PATH_SEPARATOR "\\"
#	define fseeko64 _fseeki64
#	define ftello64 _ftelli64
#endif

namespace Neko
{
    class IAllocator;
    class OutputBlob;
    class CPath;
    class IStream;
    
    namespace FS
    {
        class IStreamDevice;
        /** Interaction modes. */
        struct Mode
        {
            enum Value
            {
                NONE = 0,
                READ = 0x1,
                WRITE = READ << 1,
                OPEN = WRITE << 1,
                CREATE = OPEN << 1,
                
                CREATE_AND_WRITE = CREATE | WRITE,
                OPEN_AND_READ = OPEN | READ
            };
            
            Mode()
            : value(0)
            { }
            
            Mode(Value _value)
            : value(_value)
            { }
            
            Mode(int32 _value)
            : value(_value)
            { }
            
            NEKO_FORCE_INLINE operator Value() const { return (Value)value; }
            
            int32 value;
        };
        
        typedef TDelegate<void(IStream&, bool)> ReadCallback;
        
        struct NEKO_ENGINE_API DeviceList
        {
            IStreamDevice* Devices[8];
        };
        
        /** File system interface. */
        class NEKO_ENGINE_API FileSystem
        {
        public:
            
            static const uint32 INVALID_ASYNC = 0xffffFFFF;
            
            static FileSystem* Create(IAllocator& allocator);
            static void Destroy(FileSystem* fs);
            
        public:
            
            FileSystem()
            { }
            
            virtual ~FileSystem()
            { }
            
            virtual bool Mount(IStreamDevice* device) = 0;
            virtual bool Unmount(IStreamDevice* device) = 0;
            
            virtual IStream* Open(const DeviceList& device_list, const CPath& file, Mode mode) = 0;
            virtual uint32 OpenAsync(const DeviceList& device_list, const CPath& file, int mode, const ReadCallback& callback) = 0;
            virtual void CancelAsync(uint32 id) = 0;
            
            virtual void Close(IStream& file) = 0;
            virtual void CloseAsync(IStream& file) = 0;
            
            virtual void UpdateAsyncActions() = 0;
            
            virtual void FillDeviceList(const char* DeviceName, DeviceList& device_list) = 0;
            virtual const DeviceList& GetDefaultDevice() const = 0;
            virtual const DeviceList& GetSaveGameDevice() const = 0;
            virtual const DeviceList& GetMemoryDevice() const = 0;
            virtual const DeviceList& GetDiskDevice() const = 0;
            
            virtual void SetDefaultDevice(const char* DeviceName) = 0;
            virtual void SetSaveGameDevice(const char* DeviceName) = 0;
            virtual bool HasWork() const = 0;
            
            virtual void FlushAsyncLoading() = 0;
        };
    }
} // ~namespace Neko
