//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  BitMessageFile.cpp
//  Neko engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#include "../Data/Streams.h"
#include "BitMessageFile.h"

namespace Neko
{
    CBitMessageStream::CBitMessageStream(const CBitMessage& InMessage)
    : bWrite(false)
    {
        Message = const_cast<CBitMessage*>(&InMessage);
    }
    
    
    CBitMessageStream::CBitMessageStream(CBitMessage& InMessage)
    : bWrite(true)
    {
        Message = &InMessage;
    }
    
    
    CBitMessageStream::~CBitMessageStream()
    {

    }
} // ~namespace Neko
