//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  FileEventsDevice.cpp
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#include "../Data/IAllocator.h"
#include "../Data/Streams.h"
#include "../Core/Path.h"
#include "FileEventsDevice.h"
#include "FileSystem.h"

#ifdef FILE_EVENT_DEVICE

namespace Neko
{
    namespace FS
    {
        class EventsFile final : public IStream
        {
        public:
            
            EventsFile(IStream& file, FileEventsDevice& device, FileEventsDevice::EventCallback& cb, IAllocator& allocator)
            : Allocator(allocator)
            , Device(device)
            , pFile(file)
            , Callback(cb)
            {
                
            }
            
            virtual ~EventsFile()
            {
                pFile.Release();
            }
            
            IStreamDevice* GetDevice() override
            {
                return &Device;
            }
            
            bool Open(const CPath& path, Mode mode) override
            {
                invokeEvent(EventType::OPEN_BEGIN, path.c_str(), -1, mode);
                bool ret = pFile.Open(path, mode);
                invokeEvent(EventType::OPEN_FINISHED, path.c_str(), ret ? 1 : 0, mode);
                
                return ret;
            }
            
            void Close() override
            {
                invokeEvent(EventType::CLOSE_BEGIN, "", -1, -1);
                pFile.Close();
                
                invokeEvent(EventType::CLOSE_FINISHED, "", -1, -1);
            }
            
            size_t Read(void* buffer, const size_t size) override
            {
                invokeEvent(EventType::READ_BEGIN, "", -1, (int32)size);
                size_t ret = pFile.Read(buffer, size);
                
                invokeEvent(EventType::READ_FINISHED, "", ret == 1 ? 1 : 0, (int32)size);
                return ret;
            }
            
            size_t Write(const void* buffer, const size_t size) override
            {
                invokeEvent(EventType::WRITE_BEGIN, "", -1, (int32)size);
                size_t ret = pFile.Write(buffer, size);
                
                invokeEvent(EventType::WRITE_FINISHED, "", ret == 1 ? 1 : 0, (int32)size);
                return ret;
            }
            
            const void* GetBuffer() const override
            {
                return pFile.GetBuffer();
            }
            
            uint8* GetCurrentPtr() override
            {
                assert(false);
                return nullptr;
            }
            
            size_t GetSize() override
            {
                invokeEvent(EventType::SIZE_BEGIN, "", -1, -1);
                size_t ret = pFile.GetSize();
                
                invokeEvent(EventType::SIZE_FINISHED, "", (int32)ret, -1);
                return ret;
            }
            
            size_t Tell() const override
            {
                return pFile.Tell();
            }
            
            bool Seek(size_t pos, ESeekLocator base) override
            {
                invokeEvent(EventType::SEEK_BEGIN, "", (int32)pos, uint32(base));
                bool ret = pFile.Seek(pos, base);
                
                invokeEvent(EventType::SEEK_FINISHED, "", (int32)ret, uint32(base));
                return ret;
            }
            
            size_t GetCurrentPos() override
            {
                invokeEvent(EventType::POS_BEGIN, "", -1, -1);
                size_t ret = pFile.GetCurrentPos();
                
                invokeEvent(EventType::POS_FINISHED, "", (int32)ret, -1);
                return ret;
            }
            
            bool IsEof() const override
            {
                return pFile.IsEof();
            }
            
            bool IsFile() const override
            {
                return pFile.IsFile();
            }
            
            virtual IAllocator* GetAllocator() override
            {
                return &Allocator;
            }
            
        private:
            
            EventsFile& operator= (const EventsFile& rhs);
            
            void invokeEvent(EventType type, const char* path, int32 ret, int32 param)
            {
                Event event;
                event.type = type;
                event.handle = uptr(this);
                event.path = path;
                event.ret = ret;
                event.param = param;
                
                Callback.Invoke(event);
            }
            
            IAllocator& Allocator;
            FileEventsDevice& Device;
            IStream& pFile;
            FileEventsDevice::EventCallback& Callback;
        };
    
        IStream* FileEventsDevice::CreateFile(IStream* child)
        {
            return NEKO_NEW(Allocator, EventsFile)(*child, *this, OnEvent, Allocator);
        }
        
        void FileEventsDevice::DestroyFile(IStream* file)
        {
            NEKO_DELETE(Allocator, file);
        }
        
    } // FS
} // Neko

#endif
