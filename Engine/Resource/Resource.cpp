//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Resource.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Resource.h"
#include "ResourceManager.h"
#include "ResourceManagerBase.h"
#include "ResourceSavedData.h"
#include "../Utilities/Crc32.h"
#include "../Core/Log.h"
#include "../Neko.h"
#include "../Core/Path.h"
#include "../Data/Streams.h"
#include "../FS/FileSystem.h"
#include "../RTTI/RTTIType.h"
#include "../RTTI/RTTIReflectableField.h"
#include "../RTTI/MemorySerializer.h"
#include "../RTTI/IObject.h"

namespace Neko
{
    ResourceType::ResourceType(const char* TypeName)
    {
        assert(TypeName[0] == 0 || (TypeName[0] >= 'a' && TypeName[0] <= 'z'));
        Type = Crc32(TypeName);
    }
    
    ResourceBase::ResourceBase(const class CPath& path, IResourceManagerBase& resourceManager, IAllocator& allocator)
    : ReferenceCount()
    , EmptyDepencyCount(1)
    , FailedDepencyCount(0)
    , CurrentState(State::EMPTY)
    , DesiredState(State::EMPTY)
    , Path(path)
    , Size()
    , Callback(allocator)
    , ResourceManager(resourceManager)
    , AsyncOpState(FS::FileSystem::INVALID_ASYNC)
    {
        
    }
    
    ResourceBase::~ResourceBase()
    {
        
    }
    
    void ResourceBase::CheckState()
    {
        auto OldState = CurrentState;
        if (FailedDepencyCount > 0 && CurrentState != State::FAILURE)
        {
            CurrentState = State::FAILURE;
            Callback.Invoke(OldState, CurrentState, *this);
        }
        
        if (FailedDepencyCount == 0)
        {
            if (EmptyDepencyCount == 0 && CurrentState != State::READY && DesiredState != State::EMPTY)
            {
                OnBeforeReady();
                CurrentState = State::READY;
                Callback.Invoke(OldState, CurrentState, *this);
            }
            
            if (EmptyDepencyCount > 0 && CurrentState != State::EMPTY)
            {
                OnBeforeEmpty();
                CurrentState = State::EMPTY;
                Callback.Invoke(OldState, CurrentState, *this);
                OnPostEmpty();
            }
        }
    }
    
    void ResourceBase::FileLoaded(IStream& Data, bool bSuccess)
    {
        AsyncOpState = FS::FileSystem::INVALID_ASYNC;
        if (DesiredState != State::READY)
        {
            return;
        }
        
        assert(CurrentState != State::READY);
        assert(EmptyDepencyCount == 1);
        
        if (!bSuccess)
        {
            GLogError.log("Engine") << "Could not open " << GetPath().c_str();
            
            assert(EmptyDepencyCount > 0);
            
            --EmptyDepencyCount;
            ++FailedDepencyCount;
            CheckState();
            AsyncOpState = FS::FileSystem::INVALID_ASYNC;
            return;
        }
        
        
        if (!Load(Data))
        {
            ++FailedDepencyCount;
        }
        
        assert(EmptyDepencyCount > 0);
        
        --EmptyDepencyCount;
        CheckState();
        AsyncOpState = FS::FileSystem::INVALID_ASYNC;
    }
    
    void ResourceBase::DoUnload()
    {
        if (AsyncOpState != FS::FileSystem::INVALID_ASYNC)
        {
            FS::FileSystem& fs = ResourceManager.GetOwner().GetFileSystem();
            fs.CancelAsync(AsyncOpState);
            AsyncOpState = FS::FileSystem::INVALID_ASYNC;
        }
        
        DesiredState = State::EMPTY;
        Unload();
        assert(EmptyDepencyCount <= 1);
        
        Size = 0;
        EmptyDepencyCount = 1;
        FailedDepencyCount = 0;
        CheckState();
    }
    
    void ResourceBase::OnCreated(State state)
    {
        assert(EmptyDepencyCount == 1);
        assert(FailedDepencyCount == 0);
        
        CurrentState = state;
        DesiredState = State::READY;
        FailedDepencyCount = (state == State::FAILURE) ? 1 : 0;
        EmptyDepencyCount = 0;
    }
    
    void ResourceBase::DoLoad()
    {
        if (DesiredState == State::READY)
        {
            return;
        }
        DesiredState = State::READY;
        
        if (AsyncOpState != FS::FileSystem::INVALID_ASYNC)
        {
            return;
        }
        
        FS::FileSystem& fs = ResourceManager.GetOwner().GetFileSystem();
        FS::ReadCallback cb;
        cb.Bind<ResourceBase, &ResourceBase::FileLoaded>(this);
        AsyncOpState = fs.OpenAsync(fs.GetDefaultDevice(), Path, FS::Mode::OPEN_AND_READ, cb);
    }
    
    void ResourceBase::AddDependency(ResourceBase& DependentResource)
    {
        assert(DesiredState != State::EMPTY);
        
        DependentResource.Callback.Bind<ResourceBase, &ResourceBase::OnStateChanged>(this);
        if (DependentResource.IsEmpty())
        {
            ++EmptyDepencyCount;
        }
        if (DependentResource.IsFailure())
        {
            ++FailedDepencyCount;
        }
        CheckState();
    }
    
    void ResourceBase::RemoveDependency(ResourceBase& DependentResource)
    {
        DependentResource.Callback.Unbind<ResourceBase, &ResourceBase::OnStateChanged>(this);
        if (DependentResource.IsEmpty())
        {
            assert(EmptyDepencyCount > 0);
            --EmptyDepencyCount;
        }
        if (DependentResource.IsFailure())
        {
            assert(FailedDepencyCount > 0);
            --FailedDepencyCount;
        }
        CheckState();
    }
    
    void ResourceBase::OnStateChanged(State OldState, State NewState, ResourceBase&)
    {
        assert(OldState != NewState);
        assert(CurrentState != State::EMPTY || DesiredState != State::EMPTY);
        
        if (OldState == State::EMPTY)
        {
            assert(EmptyDepencyCount > 0);
            --EmptyDepencyCount;
        }
        if (OldState == State::FAILURE)
        {
            assert(FailedDepencyCount > 0);
            --FailedDepencyCount;
        }
        
        if (NewState == State::EMPTY)
        {
            ++EmptyDepencyCount;
        }
        if (NewState == State::FAILURE)
        {
            ++FailedDepencyCount;
        }
        
        CheckState();
    }
    
    
    // RTTI helpers
    
    
    void FindResourceDependencies(TArray<ResourceDependency>& OutResources, IReflectable& Object, bool bRecursive)
    {
        TAssociativeArray<Text, ResourceDependency> Dependencies(GetDefaultAllocator());
        FindResourceDependenciesInternal(Object, bRecursive, Dependencies);
        
        OutResources.Resize(Dependencies.GetSize());
        uint32 i = 0;
        for (auto& entry : Dependencies)
        {
            OutResources[i] = entry;
            i++;
        }
    }
    
    
    void FindResourceDependenciesInternal(IReflectable& Object, bool bRecursive, TAssociativeArray<Text, ResourceDependency>& Dependencies)
    {
        static const THashMap<Text, uint64> dummyParams;
        
        RTTITypeBase* rtti = Object.GetRTTI();
        rtti->OnSerializationStarted(&Object, dummyParams);
        
        uint32 NumFields = rtti->GetNumFields();
        for (uint32 i = 0; i < NumFields; i++)
        {
            RTTIField* Field = rtti->GetField(i);
            if ((Field->GetFlags() & RTTI_Flag_SkipInReferenceSearch) != 0)
            {
                continue;
            }
            
            if (Field->IsReflectableType())
            {
                RTTIReflectableFieldBase* ReflectableField = static_cast<RTTIReflectableFieldBase*>(Field);
                
                DebugLog("GetRTTIId() = %d\n", ReflectableField->GetType()->GetRTTIId());
                if (ReflectableField->GetType()->GetRTTIId() == TID_ResourceHandle)
                {
                    if (ReflectableField->IsArray())
                    {
                        uint32 NumElements = ReflectableField->GetArraySize(&Object);
                        for (uint32 j = 0; j < NumElements; j++)
                        {
                            HResource resource = (HResource&)ReflectableField->GetArrayValue(&Object, j);
                            if (!resource->GetName().IsEmpty())
                            {
                                if (!Dependencies.Contains(resource->GetName()))
                                {
                                    ResourceDependency& Dependency = Dependencies.Emplace(resource->GetName());
                                    Dependency.resource = resource;
                                    Dependency.NumReferences++;
                                }
                            }
                        }
                    }
                    else
                    {
                        HResource Resource = (HResource&)ReflectableField->GetValue(&Object);
                        if (!Resource->GetName().IsEmpty())
                        {
                            if (!Dependencies.Contains(Resource->GetName()))
                            {
                                ResourceDependency& Dependency = Dependencies.Emplace(Resource->GetName());
                                Dependency.resource = Resource;;
                                Dependency.NumReferences++;
                            }
                        }
                    }
                }
                else if (bRecursive)
                {
                    // Optimization, no need to retrieve its value and go deeper if it has no
                    // reflectable children that may hold the reference.
                    if (HasReflectableChildren(ReflectableField->GetType()))
                    {
                        if (ReflectableField->IsArray())
                        {
                            uint32 numElements = ReflectableField->GetArraySize(&Object);
                            for (uint32 j = 0; j < numElements; j++)
                            {
                                IReflectable& childObj = ReflectableField->GetArrayValue(&Object, j);
                                FindResourceDependenciesInternal(childObj, true, Dependencies);
                            }
                        }
                        else
                        {
                            IReflectable& childObj = ReflectableField->GetValue(&Object);
                            FindResourceDependenciesInternal(childObj, true, Dependencies);
                        }
                    }
                }
            }
            else if (Field->IsReflectablePtrType() && bRecursive)
            {
                RTTIReflectablePtrFieldBase* reflectablePtrField = static_cast<RTTIReflectablePtrFieldBase*>(Field);
                
                // Optimization, no need to retrieve its value and go deeper if it has no
                // reflectable children that may hold the reference.
                if (HasReflectableChildren(reflectablePtrField->GetType()))
                {
                    if (reflectablePtrField->IsArray())
                    {
                        uint32 numElements = reflectablePtrField->GetArraySize(&Object);
                        for (uint32 j = 0; j < numElements; j++)
                        {
                            TSharedPtr<IReflectable> childObj = reflectablePtrField->GetArrayValue(&Object, j);
                            
                            if (childObj != nullptr)
                            {
                                FindResourceDependenciesInternal(*childObj, true, Dependencies);
                            }
                        }
                    }
                    else
                    {
                        TSharedPtr<IReflectable> childObj = reflectablePtrField->GetValue(&Object);
                        
                        if (childObj != nullptr)
                        {
                            FindResourceDependenciesInternal(*childObj, true, Dependencies);
                        }
                    }
                }
            }
        }
        
        rtti->OnSerializationEnded(&Object, dummyParams);
    }
    
    bool HasReflectableChildren(RTTITypeBase* type)
    {
        uint32 numFields = type->GetNumFields();
        for (uint32 i = 0; i < numFields; i++)
        {
            RTTIField* field = type->GetField(i);
            if (field->IsReflectableType() || field->IsReflectablePtrType())
            {
                return true;
            }
        }
        
        const TArray<RTTITypeBase*>& derivedClasses = type->GetDerivedClasses();
        for (auto& derivedClass : derivedClasses)
        {
            numFields = derivedClass->GetNumFields();
            for (uint32 i = 0; i < numFields; i++)
            {
                RTTIField* field = derivedClass->GetField(i);
                if (field->IsReflectableType() || field->IsReflectablePtrType())
                {
                    return true;
                }
            }
        }
        
        return false;
    }
}
