//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ResourceSavedData.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "Array.h"
#include "Text.h"
#include "RTTIType.h"
#include "IReflectable.h"

namespace Neko
{
    class CResourceSavedData final : public IReflectable
    {
    public:
        
        CResourceSavedData();
        
        CResourceSavedData(const TArray<Text>& Dependencies);
        
        /** Returns a list of all resource dependencies. */
        const TArray<Text>& GetDependencies() const { return Dependencies; }
        
    private:
        
        TArray<Text> Dependencies;
        
    public:
        
        DECLARE_RTTI_BASE(CResourceSavedData)
    };
    
    class CResourceSavedDataRTTI : public RTTIType<CResourceSavedData, IReflectable, CResourceSavedDataRTTI>
    {
    private:
        
        NK_BEGIN_RTTI_MEMBERS
            NK_RTTI_MEMBER_PLAIN_ARRAY(Dependencies, 0)
        NK_END_RTTI_MEMBERS
        
    public:
        
        CResourceSavedDataRTTI()
        : InitMembers(this)
        { }
        
        const Text& GetRTTIName() override
        {
            static Text Name = "ResourceDependencies";
            return Name;
        }
        
        uint32 GetRTTIId() override { return TID_ResourceDependencies; }
        
        TSharedPtr<IReflectable> NewRTTIObject() override { return shared_ptr_new<CResourceSavedData>(); }
    };
    
} // namespace Neko
