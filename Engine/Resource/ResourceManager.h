//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ResourceManager.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Containers/HashMap.h"

namespace Neko
{
    class CPath;
    class ResourceBase;
    struct ResourceType;
    
    namespace FS
    {
        class FileSystem;
    }
    
    class IResourceManagerBase;
    
    /// Manages all engine resources.
    class NEKO_ENGINE_API CResourceManager
    {
        typedef THashMap<uint32, IResourceManagerBase*> ResourceManagerTable;
        
    public:
        
        explicit CResourceManager(IAllocator& Allocator);
        
        ~CResourceManager();
        
        /**
         * Initializes the resource manager. 
         *
         * @param FileSystem Engine file system.
         */
        void Create(FS::FileSystem& FileSystem);
        
        /** Destroys the resource manager instance. */
        void Destroy();
        
        NEKO_FORCE_INLINE IAllocator& GetAllocator()
        {
            return Allocator;
        }
        
        /**
         * Returns the resource manager for the requested resource type.
         */
        IResourceManagerBase* Get(ResourceType Type);
        
        /**
         * Returns the table with all resource types.
         */
        const ResourceManagerTable& GetAll() const
        {
            return ResourceManagers;
        }
        
        /**
         * Adds new type of resource into storage with its manager.
         */
        void Add(ResourceType Type, IResourceManagerBase* Manager);
        
        /** 
         * Removes resource type from our storage.
         */
        void Remove(ResourceType Type);
        
        /**
         * Reloads resource at path.
         */
        void Reload(const CPath& Path);
        
        /** 
         * Clears unreferenced resources.
         */
        void RemoveUnreferenced();
        
        void EnableUnload(bool Enable);
        
        NEKO_FORCE_INLINE FS::FileSystem& GetFileSystem()
        {
            return *pFileSystem;
        }
        
    private:
        
        IAllocator& Allocator;
        
        ResourceManagerTable ResourceManagers;
        
        FS::FileSystem* pFileSystem;
    };
} // namespace Neko
