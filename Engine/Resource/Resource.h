//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Resource.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Containers/AssociativeArray.h"
#include "../Containers/DelegateList.h"
#include "../FS/FileSystem.h"
#include "../RTTI/IReflectable.h"
#include "../RTTI/RTTIType.h"
#include "../RTTI/IObject.h"
#include "../Core/Path.h"
#include "../Utilities/Text.h"

#include "../RTTI/IObject.h"

namespace Neko
{
    class CResourceManager;
    class IResourceManagerBase;
    
    /// Represents resource's identifier
    struct NEKO_ENGINE_API ResourceType
    {
        ResourceType()
        : Type(0)
        { }
        
        explicit ResourceType(const char* TypeName);
        
        NEKO_FORCE_INLINE bool operator !=(const ResourceType& Other) const { return Other.Type != Type; }
        
        NEKO_FORCE_INLINE bool operator ==(const ResourceType& Other) const { return Other.Type == Type; }
        
        uint32 Type;
    };
    
    NEKO_FORCE_INLINE bool IsValid(ResourceType type)
    {
        return type.Type != 0;
    }
    
    const ResourceType INVALID_RESOURCE_TYPE("");
    
    /// Underlying resource base implementation.
    class NEKO_ENGINE_API ResourceBase : public IReflectable
    {
    public:
        
        friend class IObject; 
        friend class IResourceManagerBase;
        
        /** Resource state. */
        enum class State : uint32
        {
			/* Resource instance exists, but resource is not loaded. */
            EMPTY = 0,
            
			/* Resource loaded. */
            READY,
            
			/* Resource failed to load. */
            FAILURE,
        };
        
        typedef DelegateList<void(State, State, ResourceBase&)> ObserverCallback;
        
    public:
        
        /** 
         * Returns the current resource load state.
         *
         * @see ResourceBase::State
         */
        NEKO_FORCE_INLINE State GetState() const
        {
            return CurrentState;
        }
        
        NEKO_FORCE_INLINE bool IsEmpty() const { return CurrentState == State::EMPTY; }
        NEKO_FORCE_INLINE bool IsReady() const { return CurrentState == State::READY; }
        NEKO_FORCE_INLINE bool IsFailure() const { return CurrentState == State::FAILURE; }
        
        /**
         * Returns the amount of references we have on this resource.
         */
        NEKO_FORCE_INLINE uint32 GetReferenceCount() const
        {
            return ReferenceCount;
        }
        
        /**
         * Returns the size in bytes for the resource. 
         */
        NEKO_FORCE_INLINE size_t GetSize() const
        {
            return Size;
        }
        
        /**
         * Returns the path of the resource.
         */
        NEKO_FORCE_INLINE const CPath& GetPath() const
        {
            return Path;
        }
        
        /** 
         * Returns the resource manager handle for the resource.
         */
        NEKO_FORCE_INLINE IResourceManagerBase& GetResourceManager()
        {
            return ResourceManager;
        }
        
        NEKO_FORCE_INLINE ObserverCallback& GetObserverCb()
        {
            return Callback;
        }
        
        /**
         * Binds the function to callback when resource state changes to 'Ready'.
         */
        template <typename C, void (C::*Function)(State, State, ResourceBase&)> void OnLoaded(C* instance)
        {
            Callback.Bind<C, Function>(instance);
            if (IsReady())
            {
                (instance->*Function)(State::READY, State::READY, *this);
            }
        }
        
    public:
        
        void AddDependency(ResourceBase& DependentResource);
        void RemoveDependency(ResourceBase& DependentResource);
        
    protected:
        
        ResourceBase(const CPath& Path, IResourceManagerBase& ResourceManager, IAllocator& Allocator);
        
        virtual ~ResourceBase();
        
        virtual void OnBeforeReady()
        { }
        
        virtual void OnBeforeEmpty()
        { }
        
        virtual void OnPostEmpty()
        { }
        
        /**
         * Unload the resource and clear its data.
         */
        virtual void Unload(void) = 0;
        
        /** 
         * Setup object with the data from the stream.
         *
         * @param Data  Raw resource data.
         * @return True if resource has finished setting up.
         */
        virtual bool Load(IStream& Data) = 0;
        
        void OnCreated(State state);
        void DoUnload();
        
        //! Current size of the resource.
        size_t Size;
        
    protected:
        
        //! State we want to be.
        State DesiredState;
        
        uint16 EmptyDepencyCount;
        
        IResourceManagerBase& ResourceManager;
        
        
    private:
        
        ObserverCallback Callback;
        
        class CPath Path;
        
        uint16 ReferenceCount;
        
        uint16 FailedDepencyCount;
        
        State CurrentState;
        
        //! State of asynchronous resource load.
        uint32 AsyncOpState;
        
    protected:
        
        void CheckState();
        
    private:
        
        void DoLoad();
        
        void FileLoaded(IStream& File, bool Success);
        
        void OnStateChanged(State OldState, State NewState, ResourceBase&);
        
    public:
        
        uint32 AddReference(void) { return ++ReferenceCount; }
        
        uint32 RemoveReference(void) { return --ReferenceCount; }
        
    private:
        
        ResourceBase(const ResourceBase&);
        void operator = (const ResourceBase&);
        
    public:
        
        friend class ResourceRTTI;
        static RTTITypeBase* GetRTTIStatic();
        RTTITypeBase* GetRTTI() const override;
    }; // class ResourceBase
    

    class NEKO_ENGINE_API ResourceRTTI : public RTTIType<ResourceBase, IReflectable, ResourceRTTI>
    {
    private:
        
    public:
        
        ResourceRTTI()
        { }
        
        const Text& GetRTTIName() override
        {
            static Text name = "Resource";
            return name;
        }
        
        uint32 GetRTTIId() override { return TID_Resource; }
        
        TSharedPtr<IReflectable> NewRTTIObject() override
        {
            assert(false && "Cannot instantiate an abstract class.");
            return nullptr;
        }
    };
    
    inline RTTITypeBase* ResourceBase::GetRTTIStatic()
    {
        return ResourceRTTI::Instance();
    }
    
    inline RTTITypeBase* ResourceBase::GetRTTI() const
    {
        return ResourceBase::GetRTTIStatic();
    }
   
    
    class IResourceBaseRTTI : public RTTIType<IObject, IReflectable, IResourceBaseRTTI>
    {
    public:
        
        IResourceBaseRTTI()
        { }
        
        const Text& GetRTTIName() override
        {
            static Text name = "ResourceHandle";
            return name;
        }
        
        uint32 GetRTTIId() override { return TID_ResourceBase; }
        
        TSharedPtr<IReflectable> NewRTTIObject() override
        {
            assert(false && "Cannot instantiate an abstract class.");
            return nullptr;
        }
        
    private:
    };
} // namespace Neko
