//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  IResourceManagerBase.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../Neko.h"
#include "../Core/Log.h"
#include "../Core/Path.h"
#include "../Core/PathUtils.h"
#include "../Utilities/Crc32.h"
#include "ResourceManagerBase.h"
#include "Resource.h"
#include "ResourceManager.h"

namespace Neko
{
    void IResourceManagerBase::ILoadHook::ContinueLoad(ResourceBase& resource)
    {
        assert(resource.IsEmpty());
        resource.RemoveReference(); // release from hook
        resource.DesiredState = ResourceBase::State::EMPTY;
        resource.DoLoad();
    }
    
    void IResourceManagerBase::SetLoadHook(ILoadHook& loadHook)
    {
        assert(!LoadHook);
        LoadHook = &loadHook;
    }

    void IResourceManagerBase::Create(ResourceType type, CResourceManager& owner)
    {
        owner.Add(type, this);
        Owner = &owner;
    }
    
    void IResourceManagerBase::Destroy(void)
    {
        for (auto iter = Resources.begin(), end = Resources.end(); iter != end; ++iter)
        {
            ResourceBase* resource = iter.value();
            if (!resource->IsEmpty())
            {
                GLogError.log("Engine") << "Leaking resource " << *resource->GetPath() << "\n";
            }
            DestroyResource(*resource);
        }
        Resources.Clear();
    }
    
    ResourceBase* IResourceManagerBase::Get(const CPath& path)
    {
        ResourceTable::iterator it = Resources.Find(path.GetHash());
        
        if (Resources.end() != it)
        {
            return *it;
        }
        
        return nullptr;
    }
    
	ResourceBase* IResourceManagerBase::Load(const CPath& path)
	{
        if (!path.IsValid())
        {
            return nullptr;
        }
		ResourceBase* resource = Get(path);

		if (!resource)
		{
			resource = CreateResource(path);
			Resources.Insert(path.GetHash(), resource);
		}
		
		if (resource->IsEmpty())
		{
			resource->DoLoad();
		}

		resource->AddReference();
		return resource;
	}
    
	void IResourceManagerBase::RemoveUnreferenced()
    {
        if (!bIsUnloadEnabled)
        {
            return;
        }
        
		TArray<ResourceBase*> ToRemove(Allocator);
		for (auto* i : Resources)
		{
			if (i->GetReferenceCount() == 0)
            {
                ToRemove.Push(i);
            }
		}

		for (auto* i : ToRemove)
		{
			Resources.Erase(i->GetPath().GetHash());
			DestroyResource(*i);
		}
	}
    
	void IResourceManagerBase::Load(ResourceBase& resource)
	{
        if (resource.IsEmpty() && resource.DesiredState == ResourceBase::State::EMPTY)
        {
            if (LoadHook != nullptr && LoadHook->OnBeforeLoad(resource))
            {
                resource.AddReference(); // hook
                resource.AddReference(); // return value
                return;
            }
            resource.DoLoad();
        }
        
        resource.AddReference();
	}
    
	void IResourceManagerBase::Unload(const CPath& path)
	{
		ResourceBase* resource = Get(path);
		if (resource)
        {
            Unload(*resource);
        }
	}
    
	void IResourceManagerBase::Unload(ResourceBase& resource)
	{
		int32 ReferenceCount = resource.RemoveReference();
		assert(ReferenceCount >= 0);
		if (ReferenceCount == 0 && bIsUnloadEnabled)
		{
			resource.DoUnload();
		}
	}
    
    void IResourceManagerBase::EnableUnload(bool enable)
    {
        bIsUnloadEnabled = enable;
        
        if (!enable)
        {
            return;
        }
        
        for (auto* resource : Resources)
        {
            if (resource->GetReferenceCount() == 0)
            {
                resource->DoUnload();
            }
        }
    }
    
	void IResourceManagerBase::Reload(const CPath& path)
	{
		ResourceBase* resource = Get(path);
		if (resource)
        {
            Reload(*resource);
        }
	}
    
	void IResourceManagerBase::Reload(ResourceBase& resource)
	{
		resource.DoUnload();
		resource.DoLoad();
	}
    
    IResourceManagerBase::IResourceManagerBase(IAllocator& allocator)
    : Resources(allocator)
    , Allocator(allocator)
    , Owner(nullptr)
    , bIsUnloadEnabled(true)
    , LoadHook(nullptr)
    {
        
    }
    
	IResourceManagerBase::~IResourceManagerBase()
	{
		assert(Resources.IsEmpty());
	}
}
