//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ResourceManager.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Resource.h"
#include "ResourceManager.h"
#include "ResourceManagerBase.h"
#include "../Neko.h"
#include "../Core/Path.h"

namespace Neko
{
    CResourceManager::CResourceManager(IAllocator& allocator)
    : ResourceManagers(allocator)
    , Allocator(allocator)
    , pFileSystem(nullptr)
    {
        
    }
    
	CResourceManager::~CResourceManager()
	{
        
	}
    
	void CResourceManager::Create(FS::FileSystem& fs)
	{
		pFileSystem = &fs;
	}
    
	void CResourceManager::Destroy()
	{
	}
    
	IResourceManagerBase* CResourceManager::Get(ResourceType type)
	{
		return ResourceManagers[type.Type];
	}
    
	void CResourceManager::Add(ResourceType type, IResourceManagerBase* rm)
	{ 
		ResourceManagers.Insert(type.Type, rm);
	}
    
	void CResourceManager::Remove(ResourceType Type)
	{ 
		ResourceManagers.Erase(Type.Type);
	}
    
	void CResourceManager::RemoveUnreferenced()
	{
        for (auto* Manager : ResourceManagers)
        {
            Manager->RemoveUnreferenced();
        }
	}
    
	void CResourceManager::Reload(const CPath& Path)
	{
        for (auto* Manager : ResourceManagers)
        {
            Manager->Reload(Path);
        }
    }
    
    void CResourceManager::EnableUnload(bool bEnable)
    {
        for (auto* Manager : ResourceManagers)
        {
            Manager->EnableUnload(bEnable);
        }
    }
}
