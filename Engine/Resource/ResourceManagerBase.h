//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ResourceManagerBase.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Containers/HashMap.h"

namespace Neko
{
    namespace FS
    {
        class FileSystem;
    }
    
    class CPath;
    class ResourceBase;
    struct ResourceType;
    class CResourceManager;
    
    /**
     * Resource manager interface for the other resource managers.
     */
    class NEKO_ENGINE_API IResourceManagerBase
    {
        friend class ResourceBase;
        
    public:
        
        struct ILoadHook
        {
            ILoadHook(IResourceManagerBase& manager)
            : Manager(manager)
            { }
            
            virtual bool OnBeforeLoad(ResourceBase& resource) = 0;
            void ContinueLoad(ResourceBase& resource);
            
            IResourceManagerBase& Manager;
        };

        typedef THashMap<uint32, ResourceBase*> ResourceTable;
        
    public:
        
        /**
         * Creates a new instance of resource manager for a type.
         */
        void Create(ResourceType Type, CResourceManager& Owner);
        
        /**
         * Destroys the resource manager. 
         */
        void Destroy();
        
        void SetLoadHook(ILoadHook& loadHook);
        
        void EnableUnload(bool bEnable);
        
        /**
         * Loads the resource from the given path.
         * @return Null if path is not valid.
         */
        ResourceBase* Load(const CPath& Path);
        
        /**
         * Loads the resource for the provided resource.
         */
        void Load(ResourceBase& Resource);
        
        /** 
         * Clears all unreferenced resources. 
         * @note If unloading is disabled then this will do nothing. @see 'EnableUnload'.
         */
        void RemoveUnreferenced();
        
        /** 
         * Unloads the resource found by the given path.
         */
        void Unload(const CPath& Path);
        
        /** 
         * Unloads the resource.
         */
        void Unload(ResourceBase& Resource);
        
        /** 
         * Reloads the resource at path. 
         */
        void Reload(const CPath& Path);
        
        /**
         * Reloads the resource.
         */
        void Reload(ResourceBase& Resource);
        
        /** 
         * Returns all managed resources. 
         */
        NEKO_FORCE_INLINE ResourceTable& GetResourceTable()
        {
            return Resources;
        }
        
        IResourceManagerBase(IAllocator& Allocator);
        
        virtual ~IResourceManagerBase();
        
        NEKO_FORCE_INLINE CResourceManager& GetOwner() const
        {
            return *Owner;
        }
        
    protected:
        
        virtual ResourceBase* CreateResource(const CPath& Path) = 0;
        
        virtual void DestroyResource(ResourceBase& Resource) = 0;
        
        ResourceBase* Get(const CPath& Path);
        
    private:
        
        IAllocator& Allocator;
        ResourceTable Resources;
        CResourceManager* Owner;
        bool bIsUnloadEnabled;
        ILoadHook* LoadHook;
    };
}
