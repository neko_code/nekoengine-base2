//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ResourceHandle.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../RTTI/IReflectable.h"
#include "../RTTI/RTTIType.h"
#include "../Core/Path.h"
#include "../Utilities/Text.h"

#include "../RTTI/IObject.h"

namespace Neko
{
    class ResourceBase;
    class IObject;
    /// Represents a handle to a resource. The handle will be made valid as soon as the resource is loaded; Handles can be serialized and deserialized, therefore saving/restoring references to their original resource. This is also used to properly serialize resources.
    class NEKO_ENGINE_API ResourceHandleBase : public IReflectable
    {
    public:
        
        virtual ~ResourceHandleBase()
        {
        }
        
        /**
         * Checks if the resource is loaded. Until resource is loaded this handle is invalid and you may not get the
         * internal resource from it.
         *
         * @param bCheckDependencies    If true, and if resource has any dependencies, this method will also check if
         *                                    they are loaded.
         */
//        bool IsLoaded(bool const bCheckDependencies = true) const; // @todo
        
        /** Returns the UUID of the resource the handle is referring to. */
        const Neko::Text& GetName() const;
        
        /** Destroys and unloads this resource's base. */
        void Destroy();
        
    public:
        
        /** Gets the handle data. For internal use only. */
        const TSharedPtr<IObject>& GetHandleData() const { return Data; }
        
    protected:
        
        ResourceHandleBase()
        {
            Data = nullptr;
        }
        
        /**
         * Sets the created flag to true and assigns the resource pointer. Called by the constructors, or if you constructed just using a UUID, then you need to call this manually before you can access the resource from this handle.
         */
        void SetHandleData(const TSharedPtr<IObject>& ptr);
        
        void EnsureIfLoaded() const;
        
        /**
         * @note
         * All handles to the same source must share this same handle data. Otherwise things like counting number of
         * references or replacing pointed to resource become impossible without additional logic.
         */
        TSharedPtr<IObject> Data;
    };
    
    /**
     * @copydoc    ResourceHandleBase
     *
     * Handles differences in reference counting depending if the handle is normal or weak.
     */
    template <bool WeakHandle>
    class NEKO_ENGINE_API TResourceHandleBase : public ResourceHandleBase
    { };
    
    /**    Specialization of TResourceHandleBase for weak handles. Weak handles do no reference counting. */
    template<>
    class NEKO_ENGINE_API TResourceHandleBase<true> : public ResourceHandleBase
    {
    public:
        
        virtual ~TResourceHandleBase() { }
        
    protected:
        
        void AddReference() { };
        void ReleaseRef() { };
        
    public:
        
        friend class WeakResourceHandleRTTI;
        static RTTITypeBase* GetRTTIStatic();
        virtual RTTITypeBase* GetRTTI() const override;
    };
    
    /**    Specialization of TResourceHandleBase for normal (non-weak) handles. */
    template<>
    class NEKO_ENGINE_API TResourceHandleBase<false> : public ResourceHandleBase
    {
    public:
        
        virtual ~TResourceHandleBase() { }
        
    protected:
        
        void AddReference();
        void ReleaseRef();
        
    public:
        
        friend class IObjectResource;
        friend class ResourceBase;
        friend class WeakResourceHandleRTTI;
        friend class ResourceHandleRTTI;
        static RTTITypeBase* GetRTTIStatic();
        virtual RTTITypeBase* GetRTTI() const override;
    };
    
    /** @copydoc ResourceHandleBase */
    template <typename T, bool WeakHandle>
    class TResourceHandle : public TResourceHandleBase<WeakHandle>
    {
    public:
        
        TResourceHandle()
        { }
        
        /**    Copy constructor. */
        TResourceHandle(const TResourceHandle<T, WeakHandle>& ptr)
        {
            this->Data = ptr.GetHandleData();
            this->AddReference();
        }
        
        virtual ~TResourceHandle()
        {
            this->ReleaseRef();
        }
        
        /**    Converts a specific handle to generic Resource handle. */
        NEKO_FORCE_INLINE operator TResourceHandle<IObject, WeakHandle>() const
        {
            TResourceHandle<IObject, WeakHandle> handle;
            handle.SetHandleData(this->GetHandleData());
            
            return handle;
        }
        
        /**
         * Returns internal resource pointer.
         *
         * @note    Throws exception if handle is invalid.
         */
        NEKO_FORCE_INLINE T* operator->() const { return Get(); }
        
        /**
         * Returns internal resource pointer and dereferences it.
         *
         * @note    Throws exception if handle is invalid.
         */
        NEKO_FORCE_INLINE T& operator*() const { return *Get(); }
        
        /** Clears the handle making it invalid and releases any references held to the resource. */
        NEKO_FORCE_INLINE TResourceHandle<T, WeakHandle>& operator=(std::nullptr_t ptr)
        {
            this->ReleaseRef();
            this->Data = nullptr;
            
            return *this;
        }
        
        /**    Normal assignment operator. */
        TResourceHandle<T, WeakHandle>& operator=(const TResourceHandle<T, WeakHandle>& rhs)
        {
            SetHandleData(rhs.GetHandleData());
            return *this;
        }
        
        
        template<class _Ty>
        struct Bool_struct
        {
            int _Member;
        };
        
        /**
         * Allows direct conversion of handle to bool.
         *
         * @note    This is needed because we can't directly convert to bool since then we can assign pointer to bool and
         *            that's weird.
         */
        operator int Bool_struct<T>::*() const
        {
            return ((this->Data != nullptr && !this->Data->GetName().IsEmpty()) ? &Bool_struct<T>::_Member : 0);
        }
        
        
        /**
         * Returns internal resource pointer and dereferences it.
         *
         * @note    Throws exception if handle is invalid.
         */
        NEKO_FORCE_INLINE T* Get() const
        {
            this->EnsureIfLoaded();
            
            auto Info = this->Data;
            return reinterpret_cast<T*>(Info.get());
        }
        
        /**
         * Returns the internal shared pointer to the resource.
         *
         * @note    Throws exception if handle is invalid.
         */
        NEKO_FORCE_INLINE TSharedPtr<T> GetInternalPtr() const
        {
            this->EnsureIfLoaded();
            
            return std::static_pointer_cast<T>(this->Data);
        }
        
        /** Converts a handle into a weak handle. */
        TResourceHandle<T, true> GetWeak() const
        {
            TResourceHandle<T, true> handle;
            handle.SetHandleData(this->GetHandleData());
            
            return handle;
        }
        
//    protected: // @todo
        
        friend ResourceBase;
        template<class _T, bool _Weak>
        friend class TResourceHandle;
        template<class _Ty1, class _Ty2, bool Weak>
        friend TResourceHandle<_Ty1, Weak> static_resource_cast(const TResourceHandle<_Ty2, Weak>& other);
        
        /**
         * Constructs a new valid handle for the provided resource with the provided UUID.
         *
         * @note    Handle will take ownership of the provided resource pointer, so make sure you don't delete it elsewhere.
         */
        explicit TResourceHandle(T* ptr)
        : TResourceHandleBase<WeakHandle>()
        {
            this->SetHandleData(TSharedPtr<IObject>(ptr));
            this->AddReference();
        }
        
        /**    Constructs a new valid handle for the provided resource with the provided UUID. */
        TResourceHandle(const TSharedPtr<T> ptr)
        {
            SetHandleData(ptr);
            this->AddReference();
        }
        
        /**    Replaces the internal handle data pointer, effectively transforming the handle into a different handle. */
        void SetHandleData(const TSharedPtr<IObject>& data)
        {
            this->ReleaseRef();
            this->Data = data;
            this->AddReference();
        }
        
        /**    Converts a weak handle into a normal handle. */
        TResourceHandle<T, false> Lock() const
        {
            TResourceHandle<IObject, false> handle;
            handle.SetHandleData(this->GetHandleData());
            
            return handle;
        }
        
        using ResourceHandleBase::SetHandleData;
    };
    
    class NEKO_ENGINE_API ResourceHandleRTTI : public RTTIType<TResourceHandleBase<false>, IReflectable, ResourceHandleRTTI>
    {
    private:
        Text& GetUUID(TResourceHandleBase<false>* obj);
        
        void SetUUID(TResourceHandleBase<false>* obj, Text& uuid);
        
    public:
        
        ResourceHandleRTTI()
        {
            // Serialize its unique identifier so dependencies can be easily loaded
            AddPlainField("UUID", 0, &ResourceHandleRTTI::GetUUID, &ResourceHandleRTTI::SetUUID);
        }
        
        void OnDeserializationEnded(IReflectable* obj, const THashMap<Text, uint64>& params) override;
        
        void OnDeserializationStarted(IReflectable* obj, const THashMap<Text, uint64>& params) override;
        
        const Text& GetRTTIName() override
        {
            static Text name = "ResourceHandleBase";
            return name;
        }
        
        uint32 GetRTTIId() override
        {
            return TID_ResourceHandle;
        }
        
        TSharedPtr<IReflectable> NewRTTIObject() override;
    };
    
    
    class NEKO_ENGINE_API WeakResourceHandleRTTI : public RTTIType<TResourceHandleBase<true>, IReflectable, WeakResourceHandleRTTI>
    {
    public:
        
        WeakResourceHandleRTTI()
        {
        }
        
        void OnDeserializationEnded(IReflectable* obj, const THashMap<Text, uint64>& params) override;
        
        const Text& GetRTTIName() override
        {
            static Text name = "WeakResourceHandleBase";
            return name;
        }
        
        uint32 GetRTTIId() override
        {
            return TID_WeakResourceHandle;
        }
        
        TSharedPtr<IReflectable> NewRTTIObject() override
        {
            TSharedPtr<TResourceHandleBase<true>> obj = TSharedPtr<TResourceHandleBase<true>>(new TResourceHandleBase<true>());
            obj->Data = shared_ptr_new<IObject>();
            return obj;
        }
    };
    
    /** @copydoc ResourceHandleBase */
    template <typename T>
    using ResourceHandle = TResourceHandle<T, false>;
    
    /**
     * @copydoc ResourceHandleBase
     *
     * Weak handles don't prevent the resource from being unloaded.
     */
    template <typename T>
    using WeakResourceHandle = TResourceHandle<T, true>;
    
    /**    Casts one resource handle to another. */
    template<class _Ty1, class _Ty2, bool Weak>
    TResourceHandle<_Ty1, Weak> static_resource_cast(const TResourceHandle<_Ty2, Weak>& other)
    {
        TResourceHandle<_Ty1, Weak> handle;
        handle.SetHandleData(other.GetHandleData());
        
        return handle;
    }
    
    
    typedef ResourceHandle<IObject> HResource;
    
    /** Contains information about a resource dependency, including the dependant resource and number of references to it. */
    struct ResourceDependency
    {
        ResourceDependency()
        : NumReferences(0)
        { }
        
        HResource resource;
        uint32 NumReferences;
    };
    
    /**
     * Finds all resources referenced by the specified object.
     *
     * @param Object     Object to search for resource dependencies.
     * @param Recursive  Determines whether or not child objects will also be searched (if the object has any children).
     * @return A list of unique, non-null resources.
     */
    NEKO_ENGINE_API void FindResourceDependencies(TArray<ResourceDependency>& OutDependencies, IReflectable& Object, bool Recursive = true);
    
    /**
     * Helper method for for recursion when finding resource dependencies.
     *
     * @see FindDependencies
     */
    NEKO_ENGINE_API void FindResourceDependenciesInternal(IReflectable& Object, bool Recursive, TAssociativeArray<Text, ResourceDependency>& OutDependencies);
    
    /**
     * Checks if the specified type (or any of its derived classes) have any IReflectable pointer or value types as
     * their fields.
     */
    NEKO_ENGINE_API bool HasReflectableChildren(RTTITypeBase* Type);
} // namespace Neko
