//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Neko.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "Platform/SystemShared.h"

#include <assert.h>
#include <float.h>

#define INDEX_NONE -1

#define GAME_PROJECT_NAME "game"

#include "RTTI/RTTIPrerequesities.h"

namespace Neko
{
    struct ComponentHandle
    {
        int index;
        NEKO_FORCE_INLINE bool operator == (const ComponentHandle& rhs) const { return rhs.index == index; };
        NEKO_FORCE_INLINE bool operator < (const ComponentHandle& rhs) const { return rhs.index < index; };
        NEKO_FORCE_INLINE bool operator > (const ComponentHandle& rhs) const { return rhs.index > index; };
        NEKO_FORCE_INLINE bool operator != (const ComponentHandle& rhs) const { return rhs.index != index; };
        
        NEKO_FORCE_INLINE bool IsValid() const { return index >= 0; }
    };
    
    struct Entity
    {
        int index;
        NEKO_FORCE_INLINE bool operator == (const Entity& rhs) const { return rhs.index == index; };
        NEKO_FORCE_INLINE bool operator < (const Entity& rhs) const { return rhs.index < index; };
        NEKO_FORCE_INLINE bool operator > (const Entity& rhs) const { return rhs.index > index; };
        NEKO_FORCE_INLINE bool operator != (const Entity& rhs) const { return rhs.index != index; };
        
        NEKO_FORCE_INLINE bool IsValid() const { return index >= 0; }
    };
    
    struct ComponentType
    {
        enum { MAX_TYPES_COUNT = 64 };
        
        int index;
        NEKO_FORCE_INLINE bool operator == (const ComponentType& rhs) const { return rhs.index == index; };
        NEKO_FORCE_INLINE bool operator < (const ComponentType& rhs) const { return rhs.index < index; }
        NEKO_FORCE_INLINE bool operator > (const ComponentType& rhs) const { return rhs.index > index; }
        NEKO_FORCE_INLINE bool operator != (const ComponentType& rhs) const { return rhs.index != index; }
    };
    
    const ComponentType INVALID_COMPONENT_TYPE = {-1};
    const Entity INVALID_ENTITY = {-1};
    const ComponentHandle INVALID_COMPONENT = {-1};
    
    template <typename T, int32 count> int lengthOf(const T (&)[count])
    {
        return count;
    }
    
    static_assert(sizeof(int64) == 8, "Incorrect size of int64");
    static_assert(sizeof(int32) == 4, "Incorrect size of int32");
    static_assert(sizeof(int16) == 2, "Incorrect size of int16");
    static_assert(sizeof(int8) == 1, "Incorrect size of int8");
} // namespace Neko


