//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Compressor.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#include "Compressor.h"

namespace Neko
{
    /// Empty compressor object layer.
    class CompressorNone final : public ICompressor
    {
    public:
        CompressorNone( void );
        
        void Init(IStream *f, bool const compress, int wordLength) override;
        
        void FinishCompression( void ) override;
        
        float GetCompressionRatio( void ) const override;
        
        size_t Read( void *outData, size_t outLength ) override;
        
        size_t Write( const void *inData, size_t inLength ) override;
        
        size_t GetSize( void ) override;
        
        size_t Tell() const override;
        
        bool Seek(size_t offset, ESeekLocator origin ) override;
        
    protected:
        
        IStream* Stream;
        bool bCompress;
    };

    CompressorNone::CompressorNone()
    {
        Stream = nullptr;
        bCompress = true;
    }
    
    void CompressorNone::Init(IStream* stream, bool const Compress, int Length)
    {
        this->Stream = stream;
        this->bCompress = Compress;
    }
    
    void CompressorNone::FinishCompression() { }
    
    float CompressorNone::GetCompressionRatio() const { return 0.0f; }
    
    size_t CompressorNone::Write(const void* Data, size_t Length)
    {
        if (bCompress == false || Length <= 0)
        {
            return 0;
        }
        return Stream->Write(Data, Length);
    }
    
    size_t CompressorNone::Read(void* Data, size_t Length)
    {
        if (bCompress == true || Length <= 0)
        {
            return 0;
        }
        return Stream->Read(Data, Length);
    }
    
    size_t CompressorNone::GetSize(void)
    {
        if (Stream)
        {
            return Stream->GetSize();
        }
        else
        {
            return 0;
        }
    }

    size_t CompressorNone::Tell(void) const
    {
        if (Stream)
        {
            return Stream->Tell();
        }
        else
        {
            return 0;
        }
    }
    
    bool CompressorNone::Seek(size_t Offset, ESeekLocator Origin)
    {
        assert(false && "Cannot seek on Compressor object");
        return false;
    }

    ICompressor* ICompressor::AllocNoCompression() { return new CompressorNone(); }
}
