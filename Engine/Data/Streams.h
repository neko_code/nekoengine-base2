//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_\     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Streams.h
//  Neko engine
//
//  Created by Neko Code on 7/27/14.
//  Copyright (c) 2014 Neko Code. All rights reserved.
//

#pragma once

#include "../Math/MathUtils.h"
#include "../Core/Path.h"

#include "../../bx/uint32_t.h"

#if NEKO_WINDOWS_FAMILY
#   include <malloc.h>
#endif

namespace Neko
{
    namespace FS
    {
        struct Mode;
        class IStreamDevice;
    }
    
    class IAllocator;
    
    /** Seek position flags. */
    enum class ESeekLocator
    {
        Begin = 0,
        
        Current,
        
        End
    };
    
    /// Stream interface. General purpose class used for encapsulating the reading and writing of data from and to various sources using a common interface.
    class NEKO_ENGINE_API IStream
    {
    public:
        
        virtual ~IStream()
        {
        };
        
        /**
         * Opens stream.
         * @param Path Path of the file to open.
         * @param Mode Determines should the file be opened in read, write or read/write mode.
         */
        virtual bool                Open(const CPath& Path, FS::Mode Mode);
        
        /** Closes stream. */
        virtual void                Close();
        
        /** 
         * Reads data from the stream buffer. 
         */
        virtual size_t              Read(void* OutValue, const size_t Size) = 0;
        
        /** 
         * Writes to the stream buffer. 
         */
        virtual size_t              Write(const void* InValue, const size_t Size) = 0;
        
        /** @copydoc IStream::Read */
        template <typename T> NEKO_FORCE_INLINE IStream& operator >> (T& Value)
        {
            Read(static_cast<void*>(&Value), sizeof(T));
            return *this;
        }
        
        /** Writes text data to the buffer. */
        IStream& operator << (const char* Text);
        
        /** @copydoc IStream::Write */
        template <typename T>
        NEKO_FORCE_INLINE IStream& operator << (T& Value)
        {
            Write(static_cast<void*>(&Value), sizeof(T));
            return *this;
        }
        
        /** @copydoc IStream::Read */
        template<typename Ty>
        NEKO_FORCE_INLINE size_t Read(Ty* OutValue)
        {
            return Read(OutValue, sizeof(Ty));
        }
        
        /** @copydoc IStream::Write */
        template<typename Ty>
        NEKO_FORCE_INLINE size_t Write(const Ty& OutValue)
        {
            return Write(&OutValue, sizeof(Ty));
        }
        
        /** 
         * Reads all data from the buffer beginning. 
         */
        NEKO_FORCE_INLINE size_t Peek(void* OutValue, size_t Size)
        {
            size_t offset = Seek(0, ESeekLocator::Current);
            size_t size = Read(OutValue, Size);
            
            Seek(offset, ESeekLocator::Begin);
            
            return size;
        }
        
        /** @copydoc IStream::Peek */
        template<typename Ty>
        NEKO_FORCE_INLINE size_t Peek(Ty& OutValue)
        {
            return Peek(&OutValue, sizeof(Ty));
        }
        
        /** Reads value and converts it to host endianess. */
        template<typename Ty>
        NEKO_FORCE_INLINE bool ReadHostEndian(Ty& OutValue, bool bFromLittleEndian)
        {
            Ty  value;
            
            size_t Result = Read(&value, sizeof(Ty));
            OutValue = toHostEndian(value, bFromLittleEndian);
            
            return Result;
        }
        
        
        /** Repositions the read point to a specified offset. */
        virtual bool                Seek(size_t Offset, ESeekLocator SeekPos = ESeekLocator::Current) = 0;
        
        /** Returns the current byte offset from beginning. */
        virtual size_t              Tell() const;
        
        /** Returns raw data. */
        virtual const void*                 GetBuffer() const;
        
        /** Returns the size of the data. */
        virtual size_t              GetSize();
        
        /** Copies all data to the 'OutputBlob' object. */
        void GetContents(OutputBlob& Blob);
        
        /** Releases data. */
        void Release();
        
        /** Returns true if the stream has reached the end. */
        virtual bool                IsEof() const;
        
        /** Returns true if the stream object is a file. */
        virtual bool                IsFile() const;
        
        /** Returns current read position. */
        virtual size_t              GetCurrentPos();
        
        /** Returns the pointer to the byte where read position is. */
        virtual uint8*              GetCurrentPtr();
        
        /** Returns the allocator handle used for the stream buffer. */
        virtual IAllocator*                 GetAllocator();
        
    protected:
        
        /** 
         * Returns the stream device handle.
         *
         * @note Can be null.
         */
        virtual FS::IStreamDevice*              GetDevice();
    };
    
    // Helpers
    
    template<class Ty>
    NEKO_FORCE_INLINE size_t Write(IStream* Stream, const Ty* InValue)
    {
        return Stream->Write(InValue, sizeof(Ty));
    }
    
    
    template<class Ty>
    NEKO_FORCE_INLINE size_t Write(IStream* Stream, const Ty& InValue)
    {
        return Stream->Write(&InValue, sizeof(Ty));
    }
    
    
    template<class Ty>
    NEKO_FORCE_INLINE size_t Read(IStream* Stream, Ty* OutValue)
    {
        return Stream->Read(OutValue, sizeof(Ty));
    }
    
    
    /** 
     * Repeatedly write the same value we had previously written.
     */
    inline int32 WriteRepeat(IStream* Stream, uint8 Byte, int32 Size)
    {
        const uint32 tmp0      = bx::uint32_sels(64   - Size,   64, Size);
        const uint32 tmp1      = bx::uint32_sels(256  - Size,  256, tmp0);
        const uint32 blockSize = bx::uint32_sels(1024 - Size, 1024, tmp1);
        uint8* temp = (uint8*)alloca(blockSize);
        memset(temp, Byte, blockSize);
        
        int32 size = 0;
        while (0 < Size)
        {
            int32 bytes = static_cast<int32>(Stream->Write(temp, Math::Minimum<uint32>(blockSize, Size)));
            size  += bytes;
            Size -= bytes;
        }
        
        return size;
    }
}

