//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Blob.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Utilities/Text.h"

namespace Neko
{
    class InputBlob;
    class IAllocator;
    
    /// Writable blob data.
    class NEKO_ENGINE_API OutputBlob
    {
    public:
        
        explicit OutputBlob(IAllocator& allocator);
        
        OutputBlob(void* InData, int32 InSize);
        
        OutputBlob(const OutputBlob& blob, IAllocator& allocator);
        
        OutputBlob(const InputBlob& blob, IAllocator& allocator);
        
        OutputBlob(const OutputBlob& rhs);
        
        
        void operator = (const OutputBlob& rhs);
        
        ~OutputBlob();
        
        void Resize(int32 InSize);
        void Reserve(int32 InSize);
        
        NEKO_FORCE_INLINE const void* GetData() const { return Data; }
        
        NEKO_FORCE_INLINE void* GetMutableData() { return Data; }
        
        NEKO_FORCE_INLINE int32 GetPos() const { return Pos; }
        
        void Write(const void* InData, int32 InSize);
        void WriteString(const char* String);
        void Write(const Text& String);
        template <class T> inline void Write(const T& Value);
        
        void Clear();
        
        OutputBlob& operator << (const char* String);
        OutputBlob& operator << (uint64 value);
        OutputBlob& operator << (int64 value);
        OutputBlob& operator << (int32 Value);
        OutputBlob& operator << (uint32 Value);
        OutputBlob& operator << (float Value);
        
    private:
        
        void* Data;
        int32 Size;
        int32 Pos;
        IAllocator* Allocator;
    };
    
    
    template <class T> inline void OutputBlob::Write(const T& value)
    {
        Write(&value, sizeof(T));
    }
    
    
    template <> inline void OutputBlob::Write<bool>(const bool& value)
    {
        uint8 v = value;
        Write(&v, sizeof(v));
    }
    
    
    /// Readable(non-writable) blob data.
    class NEKO_ENGINE_API InputBlob
    {
    public:
        
        InputBlob(const void* InData, int32 InSize);
        
        explicit InputBlob(const OutputBlob& InBlob);
        
        
        bool Read(void* OutData, int32 Size);
        bool ReadString(char* OutData, int32 MaxSize);
        
        template <class T> NEKO_FORCE_INLINE void Read(T& value) { Read(&value, sizeof(T)); }
        template <class T> NEKO_FORCE_INLINE T Read();
        bool Read(Text& string);
        
        template <class T> NEKO_FORCE_INLINE void operator << (T& value) { Read(&value, sizeof(T)); }
        
        const void* Skip(int32 SkipSize);
        NEKO_FORCE_INLINE const void* GetData() const { return (const void*)Data; }
        
        NEKO_FORCE_INLINE int32 GetSize() const { return Size; }
        /** Returns current read locator position. */
        NEKO_FORCE_INLINE int32 GetPosition() { return Pos; }
        /** Sets read locator position. */
        NEKO_FORCE_INLINE void SetPosition(int32 pos) { Pos = pos; }
        /** Resets read locator position.*/
        NEKO_FORCE_INLINE void Rewind() { Pos = 0; }
        
        
        NEKO_FORCE_INLINE uint8 ReadChar() { ++Pos; return Data[Pos - 1]; }
        
    private:
        
        const uint8* Data;
        
        int32 Size;
        int32 Pos;
    };
    
    template <class T> inline T InputBlob::Read()
    {
        T v;
        Read(&v, sizeof(v));
        return v;
    }
    
    template <> inline bool InputBlob::Read<bool>()
    {
        uint8 v;
        Read(&v, sizeof(v));
        return v != 0;
    }
    
} // !namespace Neko
