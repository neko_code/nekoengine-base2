//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  BaseProxyAllocator.h
//  Neko engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#pragma once

#include "IAllocator.h"
#include "../Mt/Atomic.h"

namespace Neko
{
    class BaseProxyAllocator final : public IAllocator
    {
    public:
        
        BaseProxyAllocator(IAllocator& source)
        : Source(source)
        {
            AllocationCount = 0;
        }
        
        virtual ~BaseProxyAllocator()
        {
            assert(AllocationCount == 0);
        }
        
        void* AllocateAligned(size_t size, size_t align) override
        {
            Atomics::Increment(&AllocationCount);
            return Source.AllocateAligned(size, align);
        }
        
        void DeallocateAligned(void* ptr) override
        {
            if (ptr)
            {
                Atomics::Decrement(&AllocationCount);
                Source.DeallocateAligned(ptr);
            }
        }
        
        void* ReallocateAligned(void* ptr, size_t size, size_t align) override
        {
            if (!ptr)
            {
                Atomics::Increment(&AllocationCount);
            }
            if (size == 0)
            {
                Atomics::Decrement(&AllocationCount);
            }
            return Source.ReallocateAligned(ptr, size, align);
        }
        
        void* Allocate(size_t size) override
        {
            Atomics::Increment(&AllocationCount);
            return Source.Allocate(size);
        }
        
        void Deallocate(void* ptr) override
        {
            if (ptr)
            {
                Atomics::Decrement(&AllocationCount);
                Source.Deallocate(ptr);
            }
        }
        
        void* Reallocate(void* ptr, size_t size) override
        {
            if (!ptr)
            {
                Atomics::Increment(&AllocationCount);
            }
            if (size == 0)
            {
                Atomics::Decrement(&AllocationCount);
            }
            return Source.Reallocate(ptr, size);
        }
        
        IAllocator& GetSourceAllocator() { return Source; }
        
    private:
        
        IAllocator& Source;
        
        volatile int32 AllocationCount;
    };
} // namespace Neko
