//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MemoryStream.cpp
//  Neko engine
//
//  Created by Neko on 8/23/15.
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#include "MemoryStream.h"
#include "../Math/MathUtils.h"

namespace Neko
{
    CMemoryStream::CMemoryStream(Byte* Memory, uint32 Size, IAllocator& allocator, bool const FreeOnClose/* = true*/)
    : Buffer((uint8*)Memory)
    , LastPos(0)
    , Capacity(Size)
    , ReadSize(Size)
    , Allocator(&allocator)
    , bFreeOnClose(FreeOnClose)
    {

    }

    CMemoryStream::CMemoryStream(IMemoryBlock* Memory, IAllocator& allocator, bool const FreeOnClose/* = true*/)
    : Buffer(nullptr)
    , MemoryBlock(Memory)
    , Capacity(0)
    , ReadSize(0)
    , Allocator(&allocator)
    , bFreeOnClose(FreeOnClose)
    {
        assert(Memory); // Can't pass null memory block!
    }
    
    CMemoryStream::CMemoryStream(IStream& Stream)
    : Buffer(nullptr)
    , MemoryBlock(nullptr)
    , Allocator(Stream.GetAllocator())
    {
        Capacity = ReadSize = Stream.GetSize();
        Buffer = (uint8* )Allocator->Allocate(sizeof(uint8) * (uint32)Capacity);
        Stream.Read(Buffer, Capacity);
        
        LastPos = 0;
        bFreeOnClose = true;
    }
    
    CMemoryStream::CMemoryStream(const TSharedPtr<IStream>& Stream)
    : Buffer(nullptr)
    , MemoryBlock(nullptr)
    , Allocator(Stream->GetAllocator())
    {
        Capacity = ReadSize = Stream->GetSize();
        Buffer = (uint8*)Allocator->Allocate(sizeof(uint8) * (uint32)Capacity);
        Stream->Read(Buffer, Capacity);
        
        LastPos = 0;
        bFreeOnClose = true;
    }
    
    CMemoryStream::~CMemoryStream()
    {
        Close();
    }
    
	size_t CMemoryStream::Read(void* OutData, const size_t InSize)
	{
        size_t amount = LastPos + InSize < ReadSize ? InSize : ReadSize - LastPos;
        CopyMemory(OutData, Buffer + LastPos, (int32)amount);
        LastPos += amount;
        return amount;//amount == size;
	}
    
    bool CMemoryStream::Seek(size_t Offset, ESeekLocator SeekPos)
    {
        switch (SeekPos)
        {
            case ESeekLocator::Begin:
            {
                assert(Offset <= (int32)ReadSize);
                LastPos = Offset;
                break;
            }
            case ESeekLocator::Current:
            {
                assert(0 <= int32(LastPos + Offset) && int32(LastPos + Offset) <= int32(ReadSize));
                LastPos = Math::Clamp<int64>(LastPos + Offset, 0, ReadSize);
                break;
            }
            case ESeekLocator::End:
            {
                assert(Offset <= (int32)ReadSize);
                LastPos = Math::Clamp<int64>(ReadSize - Offset, 0, ReadSize);
                break;
            }
        }
        
        return true;
    }
    
    size_t CMemoryStream::Write(const void* InData, const size_t InSize)
    {
        size_t pos = LastPos;
        size_t cap = Capacity;
        size_t sz = ReadSize;
        
        if (pos + InSize > cap)
        {
            size_t new_cap = Math::Maximum(cap * 2, pos + InSize);
            uint8* new_data = (uint8*)Allocator->Allocate(sizeof(uint8) * new_cap);
            CopyMemory(new_data, Buffer, (int32)sz);
            Allocator->Deallocate(Buffer);
            Buffer = new_data;
            Capacity = new_cap;
        }
        
        CopyMemory(Buffer + pos, InData, (int32)InSize);
        LastPos += InSize;
        ReadSize = pos + InSize > sz ? pos + InSize : sz;
        
        return InSize;
    }
}
