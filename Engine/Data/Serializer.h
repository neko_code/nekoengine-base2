//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Serializer.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    class InputBlob;
    class OutputBlob;
    struct Quat;
    struct RigidTransform;
    struct Vec3;
    struct Vec4;
    
    struct EntityGUID
    {
        NEKO_FORCE_INLINE bool operator ==(const EntityGUID& Other) const
        {
            return value == Other.value;
        }
        
        uint64 value;
    };
    
    const EntityGUID INVALID_ENTITY_GUID = { 0xffffFFFFffffFFFF };
    
    inline bool IsValid(EntityGUID guid)
    {
        return guid.value != INVALID_ENTITY_GUID.value;
    }
    
    struct ISaveEntityGUIDMap
    {
        virtual EntityGUID Get(Entity entity) = 0;
    };
    
    
    struct ILoadEntityGUIDMap
    {
        virtual Entity Get(EntityGUID guid) = 0;
    };

    
    /**
     * Interface class for intermediate representations of objects that are being encoded.
     */
    struct ISerializer
    {
        virtual void Write(const char* Label, Entity entity) = 0;
        virtual void Write(const char* Label, ComponentHandle value) = 0;
        virtual void Write(const char* Label, const RigidTransform& value) = 0;
        virtual void Write(const char* Label, const Vec4& value) = 0;
        virtual void Write(const char* Label, const Vec3& value) = 0;
        virtual void Write(const char* Label, const Quat& value) = 0;
        virtual void Write(const char* Label, float value) = 0;
        virtual void Write(const char* Label, bool value) = 0;
        virtual void Write(const char* Label, int64 value) = 0;
        virtual void Write(const char* Label, uint64 value) = 0;
        virtual void Write(const char* Label, int32 value) = 0;
        virtual void Write(const char* Label, uint32 value) = 0;
        virtual void Write(const char* Label, int8 value) = 0;
        virtual void Write(const char* Label, uint8 value) = 0;
        virtual void Write(const char* Label, const char* value) = 0;
        
        virtual EntityGUID GetGuid(Entity entity) = 0;
    };
    
    /**
     * Interface class for intermediate representations of objects that are being decoded.
     */
    struct IDeserializer
    {
        virtual void Read(Entity* entity) = 0;
        virtual void Read(ComponentHandle* value) = 0;
        virtual void Read(RigidTransform* value) = 0;
        virtual void Read(Vec4* value) = 0;
        virtual void Read(Vec3* value) = 0;
        virtual void Read(Quat* value) = 0;
        virtual void Read(float* value) = 0;
        virtual void Read(bool* value) = 0;
        virtual void Read(uint64* value) = 0;
        virtual void Read(int64* value) = 0;
        virtual void Read(uint32* value) = 0;
        virtual void Read(int32* value) = 0;
        virtual void Read(uint8* value) = 0;
        virtual void Read(int8* value) = 0;
        virtual void Read(char* value, int MaxSize) = 0;
        
        template<class T> NEKO_FORCE_INLINE void operator << (T& Value)
        {
            Read(&Value);
        }
        
        virtual Entity GetEntity(EntityGUID guid) = 0;
    };
    
    /**
     * Interface for encoding all the fields of the provided object into a text format.
     */
    struct NEKO_ENGINE_API TextSerializer : public ISerializer
    {
        TextSerializer(OutputBlob& InBlob, ISaveEntityGUIDMap& InEntityMap)
        : Blob(InBlob)
        , EntityMap(InEntityMap)
        {
        }
        
        virtual void Write(const char* Label, Entity entity)  override;
        virtual void Write(const char* Label, ComponentHandle value)  override;
        virtual void Write(const char* Label, const RigidTransform& value)  override;
        virtual void Write(const char* Label, const Vec4& value)  override;
        virtual void Write(const char* Label, const Vec3& value)  override;
        virtual void Write(const char* Label, const Quat& value)  override;
        virtual void Write(const char* Label, float value)  override;
        virtual void Write(const char* Label, bool value)  override;
        virtual void Write(const char* Label, int64 value)  override;
        virtual void Write(const char* Label, uint64 value)  override;
        virtual void Write(const char* Label, int32 value)  override;
        virtual void Write(const char* Label, uint32 value)  override;
        virtual void Write(const char* Label, int8 value)  override;
        virtual void Write(const char* Label, uint8 value)  override;
        virtual void Write(const char* Label, const char* value)  override;
        
        virtual EntityGUID GetGuid(Entity entity) override;
        
        OutputBlob& Blob;
        ISaveEntityGUIDMap& EntityMap;
    };
    
    /**
     * Interface for decoding all the fields of the text format into a provided object.
     */
    struct NEKO_ENGINE_API TextDeserializer : public IDeserializer
    {
        TextDeserializer(InputBlob& InBlob, ILoadEntityGUIDMap& InEntityMap)
        : Blob(InBlob)
        , EntityMap(InEntityMap)
        {
        }
        
        virtual void Read(Entity* entity)  override;
        virtual void Read(ComponentHandle* value)  override;
        virtual void Read(RigidTransform* value)  override;
        virtual void Read(Vec4* value)  override;
        virtual void Read(Vec3* value)  override;
        virtual void Read(Quat* value)  override;
        virtual void Read(float* value)  override;
        virtual void Read(bool* value)  override;
        virtual void Read(uint64* value)  override;
        virtual void Read(int64* value)  override;
        virtual void Read(uint32* value)  override;
        virtual void Read(int32* value)  override;
        virtual void Read(uint8* value)  override;
        virtual void Read(int8* value)  override;
        virtual void Read(char* value, int MaxSize)  override;
        
        virtual Entity GetEntity(EntityGUID guid) override;
        
        void Skip();
        uint32 ReadU32();
        
        InputBlob& Blob;
        ILoadEntityGUIDMap& EntityMap;
    };
}
