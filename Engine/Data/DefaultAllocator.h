//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  DefaultAllocator.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Platform/SystemShared.h"
#include "IAllocator.h"
#include <limits>

namespace Neko
{
    /// Allocator based on system memory interface (malloc/free and so on).
    class NEKO_ENGINE_API DefaultAllocator final : public IAllocator
    {
    public:
        
        virtual ~DefaultAllocator()
        { }
        
        void* Allocate(size_t Size) override;
        void Deallocate(void* Ptr) override;
        void* Reallocate(void* Ptr, size_t Size) override;
        
        void* AllocateAligned(size_t Size, size_t Align) override;
        void DeallocateAligned(void* Ptr) override;
        void* ReallocateAligned(void* Ptr, size_t Size, size_t Align) override;
    };
    
    NEKO_ENGINE_API void SetDefaultAllocator(IAllocator& Allocator);
    
    NEKO_ENGINE_API IAllocator& GetDefaultAllocator();
    
    NEKO_ENGINE_API void ResetDefaultAllocator();
} // ~namespace Neko
