//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  JsonSerializer.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "JsonSerializer.h"
#include "Streams.h"
#include "../FS/FileSystem.h"
#include "../FS/IStreamDevice.h"
#include "../Math/MathUtils.h"
#include "../Core/Path.h"
#include "../Core/Log.h"
#include <cstdlib>

namespace Neko
{    
    class ErrorProxy
    {
    public:
        explicit ErrorProxy(JsonSerializer& serializer)
        : Log(GLogError, "serializer", serializer.Allocator)
        {
            serializer.bIsError = true;
            const char* c = serializer.Data;
            int line = 0;
            int column = 0;
            while (c < serializer.Token)
            {
                if (*c == '\n')
                {
                    ++line;
                    column = 0;
                }
                ++column;
                ++c;
            }
            
            Log << serializer.Path << "(line " << (line + 1) << ", column " << column << "): ";
        }
        LogProxy& log() { return Log; }
        
    private:
        LogProxy Log;
    };
    
    
    JsonSerializer::JsonSerializer(IStream& file,
                                   EAccessMode access_mode,
                                   const class CPath& path,
                                   IAllocator& allocator)
    : File(file)
    , AccessMode(access_mode)
    , Allocator(allocator)
    {
        bIsError = false;
        CopyString(Path, path.c_str());
        bIsFirstInBlock = true;
        Data = nullptr;
        bIsStringToken = false;
        
        if (AccessMode == READ)
        {
            DataSize = (int32)file.GetSize();
            if (file.GetBuffer() != nullptr)
            {
                Data = (const char*)file.GetBuffer();
                bOwnsData = false;
            }
            else
            {
                int32 size = (int32)File.GetSize();
                char* data = (char*)Allocator.Allocate(size);
                bOwnsData = true;
                file.Read(data, DataSize);
                data[size-1] = '\0';
                Data = data;
            }
            
            Token = Data;
            TokenSize = 0;
            DeserializeToken();
        }
    }
    
    
    JsonSerializer::~JsonSerializer()
    {
        if (AccessMode == READ && bOwnsData)
        {
            Allocator.Deallocate((void*)Data);
        }
    }
    
    
#pragma region serialization
    
    
    void JsonSerializer::Serialize(const char* label, Entity value)
    {
        Serialize(label, value.index);
    }
    
    
    void JsonSerializer::Serialize(const char* label, ComponentHandle value)
    {
        Serialize(label, value.index);
    }
    
    
    void JsonSerializer::Serialize(const char* label, unsigned int value)
    {
        WriteBlockComma();
        char tmp[20];
        WriteString(label);
        ToCString(value, tmp, 20);
        File.Write(" : ", StringLength(" : "));
        File.Write(tmp, StringLength(tmp));
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::Serialize(const char* label, float value)
    {
        WriteBlockComma();
        char tmp[20];
        WriteString(label);
        ToCString(value, tmp, 20, 8);
        File.Write(" : ", StringLength(" : "));
        File.Write(tmp, StringLength(tmp));
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::Serialize(const char* label, int value)
    {
        WriteBlockComma();
        char tmp[20];
        WriteString(label);
        ToCString(value, tmp, 20);
        File.Write(" : ", StringLength(" : "));
        File.Write(tmp, StringLength(tmp));
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::Serialize(const char* label, const class CPath& value)
    {
        WriteBlockComma();
        WriteString(label);
        File.Write(" : \"", 4);
        File.Write(value.c_str(), value.Length());
        File.Write("\"", 1);
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::Serialize(const char* label, const char* value)
    {
        WriteBlockComma();
        WriteString(label);
        File.Write(" : \"", 4);
        if (value == nullptr)
        {
            File.Write("", 1);
        }
        else
        {
            File.Write(value, StringLength(value));
        }
        File.Write("\"", 1);
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::Serialize(const char* label, bool value)
    {
        WriteBlockComma();
        WriteString(label);
        File.Write(value ? " : true" : " : false", value ? 7 : 8);
        bIsFirstInBlock = false;
    }
    
    void JsonSerializer::Serialize(const char* label, uint16 value)
    {
        WriteBlockComma();
        char tmp[20];
        WriteString(label);
        ToCString(value, tmp, 20);
        File.Write(" : ", StringLength(" : "));
        File.Write(tmp, StringLength(tmp));
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::BeginObject()
    {
        WriteBlockComma();
        File.Write("{", 1);
        bIsFirstInBlock = true;
    }
    
    
    void JsonSerializer::BeginObject(const char* label)
    {
        WriteBlockComma();
        WriteString(label);
        File.Write(" : {", 4);
        bIsFirstInBlock = true;
    }
    
    void JsonSerializer::EndObject()
    {
        File.Write("}", 1);
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::BeginArray(const char* label)
    {
        WriteBlockComma();
        WriteString(label);
        File.Write(" : [", 4);
        bIsFirstInBlock = true;
    }
    
    
    void JsonSerializer::EndArray()
    {
        File.Write("]", 1);
        bIsFirstInBlock = false;
    }
    
    void JsonSerializer::SerializeArrayItem(const char* value)
    {
        WriteBlockComma();
        WriteString(value);
        bIsFirstInBlock = false;
    }
    
    void JsonSerializer::SerializeArrayItem(uint32 value)
    {
        WriteBlockComma();
        char tmp[20];
        ToCString(value, tmp, 20);
        File.Write(tmp, StringLength(tmp));
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::SerializeArrayItem(Entity value)
    {
        SerializeArrayItem(value.index);
    }
    
    
    void JsonSerializer::SerializeArrayItem(ComponentHandle value)
    {
        SerializeArrayItem(value.index);
    }
    
    
    void JsonSerializer::SerializeArrayItem(int32 value)
    {
        WriteBlockComma();
        char tmp[20];
        ToCString(value, tmp, 20);
        File.Write(tmp, StringLength(tmp));
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::SerializeArrayItem(int64 value)
    {
        WriteBlockComma();
        char tmp[30];
        ToCString(value, tmp, 30);
        File.Write(tmp, StringLength(tmp));
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::SerializeArrayItem(float value)
    {
        WriteBlockComma();
        char tmp[20];
        ToCString(value, tmp, 20, 8);
        File.Write(tmp, StringLength(tmp));
        bIsFirstInBlock = false;
    }
    
    
    void JsonSerializer::SerializeArrayItem(bool value)
    {
        WriteBlockComma();
        File.Write(value ? "true" : "false", value ? 4 : 5);
        bIsFirstInBlock = false;
    }
    
#pragma endregion
    
    
#pragma region deserialization
    
    
    bool JsonSerializer::IsNextBoolean() const
    {
        if (bIsStringToken)
        {
            return false;
        }
        if (TokenSize == 4 && CompareStringN(Token, "true", 4) == 0)
        {
            return true;
        }
        if (TokenSize == 5 && CompareStringN(Token, "false", 5) == 0)
        {
            return true;
        }
        return false;
    }
    
    
    void JsonSerializer::Deserialize(const char* label, Entity& value, Entity DefaultValue)
    {
        Deserialize(label, value.index, DefaultValue.index);
    }
    
    
    void JsonSerializer::Deserialize(const char* Label, uint16& Value, uint16 DefaultValue)
    {
        DeserializeLabel(Label);
        if (bIsStringToken || !FromCString(Token, TokenSize, &Value))
        {
            Value = DefaultValue;
        }
        else
        {
            DeserializeToken();
        }
    }
    
    
    void JsonSerializer::Deserialize(const char* label, ComponentHandle& value, ComponentHandle DefaultValue)
    {
        Deserialize(label, value.index, DefaultValue.index);
    }
    
    
    void JsonSerializer::Deserialize(bool& value, bool DefaultValue)
    {
        value = !bIsStringToken ? (TokenSize == 4) && (CompareStringN(Token, "true", 4) == 0) : DefaultValue;
        DeserializeToken();
    }
    
    
    void JsonSerializer::Deserialize(float& OutValue, float DefaultValue)
    {
        if (!bIsStringToken)
        {
            OutValue = TokenToFloat();
        }
        else
        {
            OutValue = DefaultValue;
        }
        DeserializeToken();
    }
    
    
    void JsonSerializer::Deserialize(int32& OutValue, int32 DefaultValue)
    {
        if (bIsStringToken || !FromCString(Token, TokenSize, &OutValue))
        {
            OutValue = DefaultValue;
        }
        DeserializeToken();
    }
    
    
    void JsonSerializer::Deserialize(const char* Label, class CPath& OutValue, const class CPath& DefaultValue)
    {
        DeserializeLabel(Label);
        if (!bIsStringToken)
        {
            OutValue = DefaultValue;
        }
        else
        {
            char tmp[MAX_PATH_LENGTH];
            int size = Math::Minimum(lengthOf(tmp) - 1, TokenSize);
            CopyMemory(tmp, Token, size);
            tmp[size] = '\0';
            OutValue = tmp;
            DeserializeToken();
        }
    }
    
    
    void JsonSerializer::Deserialize(class CPath& OutValue, const class CPath& DefaultValue)
    {
        if (!bIsStringToken)
        {
            OutValue = DefaultValue;
        }
        else
        {
            char tmp[MAX_PATH_LENGTH];
            int32 size = Math::Minimum(lengthOf(tmp) - 1, TokenSize);
            CopyMemory(tmp, Token, size);
            tmp[size] = '\0';
            OutValue = tmp;
            DeserializeToken();
        }
    }
    
    
    void JsonSerializer::Deserialize(char* OutValue, int32 MaxLength, const char* DefaultValue)
    {
        if (!bIsStringToken)
        {
            CopyString(OutValue, MaxLength, DefaultValue);
        }
        else
        {
            int32 size = Math::Minimum(MaxLength - 1, TokenSize);
            CopyMemory(OutValue, Token, size);
            OutValue[size] = '\0';
            DeserializeToken();
        }
    }
    
    
    void JsonSerializer::Deserialize(const char* Label, float& OutValue, float DefaultValue)
    {
        DeserializeLabel(Label);
        if (!bIsStringToken)
        {
            OutValue = TokenToFloat();
            DeserializeToken();
        }
        else
        {
            OutValue = DefaultValue;
        }
    }
    
    
    void JsonSerializer::Deserialize(const char* label, uint32& value, uint32 DefaultValue)
    {
        DeserializeLabel(label);
        if (bIsStringToken || !FromCString(Token, TokenSize, &value))
        {
            value = DefaultValue;
        }
        else
        {
            DeserializeToken();
        }
    }
    
    
    bool JsonSerializer::IsObjectEnd()
    {
        if (Token == (Data + DataSize))
        {
            ErrorProxy(*this).log() << "Unexpected end of file while looking for the end of an object.";
            return true;
        }
        
        return (!bIsStringToken && TokenSize == 1 && Token[0] == '}');
    }
    
    
    void JsonSerializer::Deserialize(const char* Label, int32& OutValue, int32 DefaultValue)
    {
        DeserializeLabel(Label);
        if (bIsStringToken || !FromCString(Token, TokenSize, &OutValue))
        {
            OutValue = DefaultValue;
        }
        DeserializeToken();
    }
    
    
    void JsonSerializer::Deserialize(const char* Label, char* OutValue, int32 MaxLength, const char* DefaultValue)
    {
        DeserializeLabel(Label);
        if (!bIsStringToken)
        {
            CopyString(OutValue, MaxLength, DefaultValue);
        }
        else
        {
            int32 size = Math::Minimum(MaxLength - 1, TokenSize);
            CopyMemory(OutValue, Token, size);
            OutValue[size] = '\0';
            DeserializeToken();
        }
    }
    
    
    void JsonSerializer::DeserializeArrayBegin(const char* Label)
    {
        DeserializeLabel(Label);
        ExpectToken('[');
        bIsFirstInBlock = true;
        DeserializeToken();
    }
    
    
    void JsonSerializer::ExpectToken(char ExpectedToken)
    {
        if (bIsStringToken || Token[0] != ExpectedToken || TokenSize != 1)
        {
            char tmp[2];
            tmp[0] = ExpectedToken;
            tmp[1] = 0;
            
            ErrorProxy(*this).log() << "Unexpected token \"" << Text(Token, TokenSize, Allocator) << "\", expected "
            << tmp << ".";
            DeserializeToken();
        }
    }
    
    
    void JsonSerializer::DeserializeArrayBegin()
    {
        ExpectToken('[');
        bIsFirstInBlock = true;
        DeserializeToken();
    }
    
    
    void JsonSerializer::DeserializeRawString(char* OutBuffer, int32 MaxLength)
    {
        int32 size = Math::Minimum(MaxLength - 1, TokenSize);
        CopyMemory(OutBuffer, Token, size);
        OutBuffer[size] = '\0';
        DeserializeToken();
    }
    
    
    void JsonSerializer::NextArrayItem()
    {
        if (!bIsFirstInBlock)
        {
            ExpectToken(',');
            DeserializeToken();
        }
    }
    
    
    bool JsonSerializer::IsArrayEnd()
    {
        if (Token == (Data + DataSize))
        {
            ErrorProxy(*this).log() << "Unexpected end of file while looking for the end of an array.";
            return true;
        }
        
        return (!bIsStringToken && Token[0] == ']' && TokenSize == 1);
    }
    
    
    void JsonSerializer::DeserializeArrayEnd()
    {
        ExpectToken(']');
        bIsFirstInBlock = false;
        DeserializeToken();
    }
    
    
    void JsonSerializer::DeserializeArrayItem(char* OutValue, int32 MaxLength, const char* DefaultValue)
    {
        DeserializeArrayComma();
        if (bIsStringToken)
        {
            int32 size = Math::Minimum(MaxLength - 1, TokenSize);
            CopyMemory(OutValue, Token, size);
            OutValue[size] = '\0';
            DeserializeToken();
        }
        else
        {
            ErrorProxy(*this).log() << "Unexpected token \"" << Text(Token, TokenSize, Allocator) << "\", expected string.";
            DeserializeToken();
            CopyString(OutValue, MaxLength, DefaultValue);
        }
    }
    
    
    void JsonSerializer::DeserializeArrayItem(Entity& OutValue, Entity DefaultValue)
    {
        DeserializeArrayItem(OutValue.index, DefaultValue.index);
    }
    
    
    void JsonSerializer::DeserializeArrayItem(ComponentHandle& OutValue, ComponentHandle DefaultValue)
    {
        DeserializeArrayItem(OutValue.index, DefaultValue.index);
    }
    
    
    void JsonSerializer::DeserializeArrayItem(uint32& OutValue, uint32 DefaultValue)
    {
        DeserializeArrayComma();
        if (bIsStringToken || !FromCString(Token, TokenSize, &OutValue))
        {
            OutValue = DefaultValue;
        }
        DeserializeToken();
    }
    
    
    void JsonSerializer::DeserializeArrayItem(int32& OutValue, int32 DefaultValue)
    {
        DeserializeArrayComma();
        if (bIsStringToken || !FromCString(Token, TokenSize, &OutValue))
        {
            OutValue = DefaultValue;
        }
        DeserializeToken();
    }
    
    
    void JsonSerializer::DeserializeArrayItem(int64& OutValue, int64 DefaultValue)
    {
        DeserializeArrayComma();
        if (bIsStringToken || !FromCString(Token, TokenSize, &OutValue))
        {
            OutValue = DefaultValue;
        }
        DeserializeToken();
    }
    
    
    void JsonSerializer::DeserializeArrayItem(float& OutValue, float DefaultValue)
    {
        DeserializeArrayComma();
        if (bIsStringToken)
        {
            OutValue = DefaultValue;
        }
        else
        {
            OutValue = TokenToFloat();
        }
        DeserializeToken();
    }
    
    
    void JsonSerializer::DeserializeArrayItem(bool& OutValue, bool DefaultValue)
    {
        DeserializeArrayComma();
        if (bIsStringToken)
        {
            OutValue = DefaultValue;
        }
        else
        {
            OutValue = CompareStringN("true", Token, TokenSize) == 0 && TokenSize == 4;
        }
        DeserializeToken();
    }
    
    
    void JsonSerializer::Deserialize(const char* Label, bool& OutValue, bool DefaultValue)
    {
        DeserializeLabel(Label);
        if (!bIsStringToken)
        {
            OutValue = TokenSize == 4 && CompareStringN("true", Token, 4) == 0;
        }
        else
        {
            OutValue = DefaultValue;
        }
        DeserializeToken();
    }
    
    
    static bool isDelimiter(char c)
    {
        return c == '\t' || c == '\n' || c == ' ' || c == '\r';
    }
    
    
    void JsonSerializer::DeserializeArrayComma()
    {
        if (bIsFirstInBlock)
        {
            bIsFirstInBlock = false;
        }
        else
        {
            
            ExpectToken(',');
            DeserializeToken();
        }
    }
    
    
    static bool isSingleCharToken(char c)
    {
        return c == ',' || c == '[' || c == ']' || c == '{' || c == '}' || c == ':';
    }
    
    
    void JsonSerializer::DeserializeToken()
    {
        Token += TokenSize;
        if (bIsStringToken)
        {
            ++Token;
        }
        
        while (Token < Data + DataSize && isDelimiter(*Token))
        {
            ++Token;
        }
        if (*Token == '/' && Token < Data + DataSize - 1 && Token[1] == '/')
        {
            TokenSize = int32((Data + DataSize) - Token);
            bIsStringToken = false;
        }
        else if (*Token == '"')
        {
            ++Token;
            bIsStringToken = true;
            const char* token_end = Token;
            while (token_end < Data + DataSize && *token_end != '"')
            {
                ++token_end;
            }
            if (token_end == Data + DataSize)
            {
                ErrorProxy(*this).log() << "Unexpected end of file while looking for \".";
                TokenSize = 0;
            }
            else
            {
                TokenSize = int(token_end - Token);
            }
        }
        else if (isSingleCharToken(*Token))
        {
            bIsStringToken = false;
            TokenSize = 1;
        }
        else
        {
            bIsStringToken = false;
            const char* token_end = Token;
            while (token_end < Data + DataSize
                   && !isDelimiter(*token_end)
                   && !isSingleCharToken(*token_end))
            {
                ++token_end;
            }
            TokenSize = int32(token_end - Token);
        }
    }
    
    
    void JsonSerializer::DeserializeObjectBegin()
    {
        bIsFirstInBlock = true;
        ExpectToken('{');
        DeserializeToken();
    }
    
    void JsonSerializer::DeserializeObjectEnd()
    {
        ExpectToken('}');
        bIsFirstInBlock = false;
        DeserializeToken();
    }
    
    
    void JsonSerializer::DeserializeLabel(char* OutLabel, int32 MaxLength)
    {
        if (!bIsFirstInBlock)
        {
            ExpectToken(',');
            DeserializeToken();
        }
        else
        {
            bIsFirstInBlock = false;
        }
        if (!bIsStringToken)
        {
            ErrorProxy(*this).log() << "Unexpected token \"" << Text(Token, TokenSize, Allocator) << "\", expected string.";
            DeserializeToken();
        }
        CopyNString(OutLabel, MaxLength, Token, TokenSize);
        DeserializeToken();
        ExpectToken(':');
        DeserializeToken();
    }
    
    
    void JsonSerializer::WriteString(const char* String)
    {
        File.Write("\"", 1);
        if (String)
        {
            File.Write(String, StringLength(String));
        }
        File.Write("\"", 1);
    }
    
    
    void JsonSerializer::WriteBlockComma()
    {
        if (!bIsFirstInBlock)
        {
            File.Write(",\n", 2);
        }
    }
    
    
    void JsonSerializer::DeserializeLabel(const char* OutLabel)
    {
        if (!bIsFirstInBlock)
        {
            ExpectToken(',');
            DeserializeToken();
        }
        else
        {
            bIsFirstInBlock = false;
        }
        if (!bIsStringToken)
        {
            ErrorProxy(*this).log() << "Unexpected token \"" << Text(Token, TokenSize, Allocator) << "\", expected string.";
            DeserializeToken();
        }
        if (CompareStringN(OutLabel, Token, TokenSize) != 0)
        {
            ErrorProxy(*this).log() << "Unexpected label \"" << Text(Token, TokenSize, Allocator) << "\", expected \"" << OutLabel << "\".";
            DeserializeToken();
        }
        DeserializeToken();
        if (bIsStringToken || Token[0] != ':' || TokenSize != 1)
        {
            ErrorProxy(*this).log() << "Unexpected label \"" << Text(Token, TokenSize, Allocator) << "\", expected \"" << OutLabel << "\".";
            DeserializeToken();
        }
        DeserializeToken();
    }
    
    
#pragma endregion
    
    
    float JsonSerializer::TokenToFloat()
    {
        char tmp[64];
        int size = Math::Minimum((int)sizeof(tmp) - 1, TokenSize);
        CopyMemory(tmp, Token, size);
        tmp[size] = '\0';
        return (float)atof(tmp);
    }
} // ~namespace Neko
