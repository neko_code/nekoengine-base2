//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Compressor.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "Streams.h"

namespace Neko
{
    /**
     * Interface for compressor object. 
     */
    class ICompressor : public IStream
    {
    public:
        
        /** Allocates empty compressor layer object. */
        static NEKO_ENGINE_API ICompressor*     AllocNoCompression();
        
        
        /** Initialises compressor object with stream data. */
        virtual void                Init(IStream* stream, bool const compress, int length) = 0;
        
        /** Finishes compression. */
        virtual void                FinishCompression() = 0;
        
        /** Returns compression ratio percent. */
        virtual float               GetCompressionRatio() const = 0;
        
        /** Reads from the object. */
        virtual size_t				Read(void* outData, size_t Length) = 0;
        
        /** Writes to the object. */
        virtual size_t				Write(const void *inData, size_t Length) = 0;
        
        /** Size of the object. */
        virtual size_t				GetSize( void ) = 0;
        
        virtual size_t              Tell() const = 0;
        
        virtual bool                Seek(size_t Offset, ESeekLocator SeekPos = ESeekLocator::Current) = 0;
    };
} // namespace Neko
