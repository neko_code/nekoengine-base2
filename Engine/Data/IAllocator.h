//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  IAllocator.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include <cstring>

#include "../Platform/SystemShared.h"
#include <stdlib.h>
#include <new>

namespace Neko
{
    struct NewPlaceholder {};
}

inline void* operator new(size_t, Neko::NewPlaceholder, void* where)
{
    return where;
}

inline void operator delete(void*, Neko::NewPlaceholder,  void*)
{
}

namespace Neko
{
    
#define ByteInMegabyte(Value) (Value / (1024LL * 1024LL))
#define Kilobyte(Value) (Value * 1024LL)
#define Megabyte(Value) (Kilobyte( Value ) * 1024LL)
    
    /**	Statistics reported by a platform hardware. */
    struct PlatformMemoryStats
    {
        PlatformMemoryStats()
        : AvailablePhysical(0)
        , AvailableVirtual(0)
        , UsedPhysical(0)
        , PeakUsedPhysical(0)
        , UsedVirtual(0)
        , PeakUsedVirtual(0)
        {
            
        }
        
        uint64 AvailablePhysical;
        uint64 AvailableVirtual;
        
        uint64 UsedPhysical;
        uint64 PeakUsedPhysical;
        uint64 UsedVirtual;
        uint64 PeakUsedVirtual;
    };
    
    /** Constant hardware statistics. */
    struct PlatformMemoryHWConstants
    {
        PlatformMemoryHWConstants()
        : TotalPhysicalGB(0)
        , TotalPhysical(0)
        , TotalVirtual(0)
        , PageSize(0)
        {
            
        }
        
        uint64 TotalPhysicalGB;
        uint64 TotalPhysical;
        uint64 TotalVirtual;
        uint64 PageSize;
    };
    
    
#define ALIGN_OF(...) __alignof(__VA_ARGS__)
#define NEKO_NEW(allocator, ...) new (Neko::NewPlaceholder(), (allocator).AllocateAligned(sizeof(__VA_ARGS__), ALIGN_OF(__VA_ARGS__))) __VA_ARGS__
#define NEKO_DELETE(allocator, var) (allocator).deleteObject(var);
    
    
    class NEKO_ENGINE_API IAllocator
    {
    public:
        virtual ~IAllocator()
        {
            
        }
        
        virtual void* Allocate(size_t size) = 0;
        virtual void Deallocate(void* ptr) = 0;
        virtual void* Reallocate(void* ptr, size_t size) = 0;
        
        virtual void* AllocateAligned(size_t size, size_t align) = 0;
        virtual void DeallocateAligned(void* ptr) = 0;
        virtual void* ReallocateAligned(void* ptr, size_t size, size_t align) = 0;
        
        template <class T> void deleteObject(T* ptr)
        {
            if (ptr)
            {
                ptr->~T();
                DeallocateAligned(ptr);
            }
        }
    };
    
    /// Pool allocator.
    class NEKO_ENGINE_API CPoolAllocator final : public IAllocator
    {
    public:
        CPoolAllocator(size_t objsz, uint8 alignment, size_t sz, void* mem);
        
        ~CPoolAllocator();
        
        void* Allocate(size_t size/*, uint8 align*/) override;
        void Deallocate(void* ptr) override;
        void* Reallocate(void* ptr, size_t size/*, size_t Alignment*/) override;
        void* AllocateAligned(size_t size, size_t align) override
        { return Allocate(size); };
        void DeallocateAligned(void* ptr) override
        { };
        void* ReallocateAligned(void* ptr, size_t size, size_t align) override
        { return nullptr; };
    private:
        
        CPoolAllocator(const CPoolAllocator&);
        CPoolAllocator& operator = (const CPoolAllocator&);
        
        size_t    ObjectSize;
        uint8     ObjectAlign;
        
        void**  pFreeList;
    };
    
    /** @addtogroup Internal-Utility
     *  @{
     */
    
    /** @addtogroup Memory-Internal
     *  @{
     */
    
    /**
     * Describes a memory stack of a certain block capacity. See MemStack for more information.
     *
     * @tparam	BlockCapacity Minimum size of a block. Larger blocks mean less memory allocations, but also potentially
     *						  more wasted memory. If an allocation requests more bytes than BlockCapacity, first largest
     *						  multiple is used instead.
     */
    template <int BlockCapacity = 1024 * 1024>
    class MemStackInternal
    {
    private:
        /**
         * A single block of memory of BlockCapacity size. A pointer to the first free address is stored, and a remaining
         * size.
         */
        class MemBlock
        {
        public:
            MemBlock(uint32 size)
            :Data(nullptr), mFreePtr(0), mSize(size),
            mNextBlock(nullptr), mPrevBlock(nullptr)
            { }
            
            ~MemBlock()
            { }
            
            /**
             * Returns the first free address and increments the free pointer. Caller needs to ensure the remaining block
             * size is adequate before calling.
             */
            uint8* Alloc(uint32 amount)
            {
                uint8* freePtr = &Data[mFreePtr];
                mFreePtr += amount;
                
                return freePtr;
            }
            
            /**
             * Deallocates the provided pointer. Deallocation must happen in opposite order from allocation otherwise
             * corruption will occur.
             *
             * @note	Pointer to @p data isn't actually needed, but is provided for debug purposes in order to more
             * 			easily track out-of-order deallocations.
             */
            void dealloc(uint8* data, uint32 amount)
            {
                mFreePtr -= amount;
                assert((&Data[mFreePtr]) == data && "Out of order stack deallocation detected. Deallocations need to happen in order opposite of allocations.");
            }
            
            uint8* Data;
            uint32 mFreePtr;
            uint32 mSize;
            MemBlock* mNextBlock;
            MemBlock* mPrevBlock;
        };
        
    public:
        
        MemStackInternal()
        : mFreeBlock(nullptr)
        {
            mFreeBlock = allocBlock(BlockCapacity);
        }
        
        ~MemStackInternal()
        {
            assert(mFreeBlock->mFreePtr == 0 && "Not all blocks were released before shutting down the stack allocator.");
            
            MemBlock* curBlock = mFreeBlock;
            while (curBlock != nullptr)
            {
                MemBlock* nextBlock = curBlock->mNextBlock;
                deallocBlock(curBlock);
                
                curBlock = nextBlock;
            }
        }
        
        /**
         * Allocates the given amount of memory on the stack.
         *
         * @param	amount	The amount to allocate in bytes.
         *
         * @note
         * Allocates the memory in the currently active block if it is large enough, otherwise a new block is allocated.
         * If the allocation is larger than default block size a separate block will be allocated only for that allocation,
         * making it essentially a slower heap allocator.
         * @note
         * Each allocation comes with a 4 Byte overhead.
         */
        uint8* Alloc(uint32 amount)
        {
            amount += sizeof(uint32);
            
            uint32 freeMem = mFreeBlock->mSize - mFreeBlock->mFreePtr;
            if (amount > freeMem)
            {
                allocBlock(amount);
            }
            
            uint8* data = mFreeBlock->Alloc(amount);
            
            uint32* storedSize = reinterpret_cast<uint32*>(data);
            *storedSize = amount;
            
            return data + sizeof(uint32);
        }
        
        /** Deallocates the given memory. Data must be deallocated in opposite order then when it was allocated. */
        void dealloc(uint8* data)
        {
            data -= sizeof(uint32);
            
            uint32* storedSize = reinterpret_cast<uint32*>(data);
            mFreeBlock->dealloc(data, *storedSize);
            
            if (mFreeBlock->mFreePtr == 0)
            {
                MemBlock* emptyBlock = mFreeBlock;
                
                if (emptyBlock->mPrevBlock != nullptr)
                {
                    mFreeBlock = emptyBlock->mPrevBlock;
                }
                
                // Merge with next block
                if (emptyBlock->mNextBlock != nullptr)
                {
                    uint32 totalSize = emptyBlock->mSize + emptyBlock->mNextBlock->mSize;
                    
                    if (emptyBlock->mPrevBlock != nullptr)
                    {
                        emptyBlock->mPrevBlock->mNextBlock = nullptr;
                    }
                    else
                    {
                        mFreeBlock = nullptr;
                    }
                    
                    deallocBlock(emptyBlock->mNextBlock);
                    deallocBlock(emptyBlock);
                    
                    allocBlock(totalSize);
                }
            }
        }
        
    private:
        
        MemBlock* mFreeBlock;
        
        /**
         * Allocates a new block of memory using a heap allocator. Block will never be smaller than BlockCapacity no matter
         * the @p wantedSize.
         */
        MemBlock* allocBlock(uint32 wantedSize)
        {
            uint32 blockSize = BlockCapacity;
            if (wantedSize > blockSize)
            {
                blockSize = wantedSize;
            }
            MemBlock* newBlock = nullptr;
            MemBlock* curBlock = mFreeBlock;
            
            while (curBlock != nullptr)
            {
                MemBlock* nextBlock = curBlock->mNextBlock;
                if (nextBlock != nullptr && nextBlock->mSize >= blockSize)
                {
                    newBlock = nextBlock;
                    break;
                }
                
                curBlock = nextBlock;
            }
            
            if (newBlock == nullptr)
            {
                uint8* data = (uint8*)reinterpret_cast<uint8*>(malloc(blockSize + sizeof(MemBlock)));
                newBlock = new (data)MemBlock(blockSize);
                data += sizeof(MemBlock);
                
                newBlock->Data = data;
                newBlock->mPrevBlock = mFreeBlock;
                
                if (mFreeBlock != nullptr)
                {
                    if (mFreeBlock->mNextBlock != nullptr)
                    {
                        mFreeBlock->mNextBlock->mPrevBlock = newBlock;
                    }
                    newBlock->mNextBlock = mFreeBlock->mNextBlock;
                    mFreeBlock->mNextBlock = newBlock;
                }
            }
            
            mFreeBlock = newBlock;
            return newBlock;
        }
        
        /** Deallocates a block of memory. */
        void deallocBlock(MemBlock* block)
        {
            block->~MemBlock();
            free(block);
        }
    };
    
    /**
     * One of the fastest, but also very limiting type of allocator. All deallocations must happen in opposite order from
     * allocations.
     *
     * @note
     * It's mostly useful when you need to allocate something temporarily on the heap, usually something that gets
     * allocated and freed within the same method.
     * @note
     * Each allocation comes with a pretty hefty 4 Byte memory overhead, so don't use it for small allocations.
     * @note
     * Thread safe. But you cannot allocate on one thread and deallocate on another. Threads will keep
     * separate stacks internally. Make sure to call BeginThread()/EndThread() for any thread this stack is used on.
     */
    class MemStack
    {
    public:
        /**
         * Sets up the stack with the currently active thread. You need to call this on any thread before doing any
         * allocations or deallocations.
         */
        static NEKO_LIBRARY_EXPORT void BeginThread();
        
        /**
         * Cleans up the stack for the current thread. You may not perform any allocations or deallocations after this is
         * called, unless you call BeginThread again.
         */
        static NEKO_LIBRARY_EXPORT void EndThread();
        
        /** @copydoc MemStackInternal::Alloc() */
        static NEKO_LIBRARY_EXPORT uint8* Alloc(uint32 amount);
        
        /** @copydoc MemStackInternal::dealloc() */
        static NEKO_LIBRARY_EXPORT void DeallocLast(uint8* data);
        
    private:
        static NEKO_THREAD_LOCAL MemStackInternal<1024 * 1024>* ThreadMemStack;
    };
    
    /** @copydoc MemStackInternal::Alloc() */
    inline void* bs_stack_alloc(uint32 amount)
    {
        return (void*)MemStack::Alloc(amount);
    }
    
    
    /** @copydoc MemStackInternal::dealloc() */
    inline void bs_stack_free(void* data)
    {
        return MemStack::DeallocLast((uint8*)data);
    }
    
    
    
    
    /// Forward alignment.
    extern inline void* GetForwardAlign(void* address, uint8 alignment);
    extern inline const void* GetForwardAlign(const void* address, uint8 alignment);
    
    /// Backward alignment.
    extern inline void* GetBackwardAlign(void* address, uint8 alignment);
    extern inline const void* GetBackwardAlign(const void* address, uint8 alignment);
    
    /// Alignments with adjustment.
    extern inline uint8 GetForwardAlignAdjustment(const void* address, uint8 alignment);
    extern inline uint8 GetForwardAlignAdjustmentHeader(const void* address, uint8 alignment, uint8 headerSize);
    extern inline uint8 GetBackwardAlignAdjustment(const void* address, uint8 alignment);
    
    extern NEKO_ENGINE_API void* AddPtrSize(void* p, size_t x);
    extern NEKO_ENGINE_API const void* AddPtrSize(const void* p, size_t x);
    extern NEKO_ENGINE_API inline void* SubtractPtrSize(void* p, size_t x);
    extern NEKO_ENGINE_API inline const void* SubtractPtrSize(const void* p, size_t x);
    
    
    
    /* Pool allocator */
    
    inline CPoolAllocator* NewPoolAllocator(size_t objectSize, uint8 objectAlignment, size_t size, IAllocator& allocator)
    {
        void* p = allocator.AllocateAligned(size + sizeof(CPoolAllocator), __alignof(CPoolAllocator));
        return new (p)CPoolAllocator(objectSize, objectAlignment, size, AddPtrSize(p, sizeof(CPoolAllocator)));
    }
    
    inline void DeletePoolAllocator(CPoolAllocator* poolAllocator, IAllocator* allocator)
    {
        poolAllocator->~CPoolAllocator();
        allocator->Deallocate(poolAllocator);
    }
    
    
    /*  Allocations with constructor initialiser */
    
    /** Allocates the object in the specified memory with arguments with the constructor call. */
    template <class T, typename ... args> T* Alloc(IAllocator& allocator, args&&...a)
    {
        return new (allocator.AllocateAligned(sizeof(T), __alignof(T))) T(a...);
    }
    
    /** Allocates the object using the specified memory with the constructor call. */
    template <class T> T* Alloc(IAllocator& allocator)
    {
        return new (allocator.AllocateAligned(sizeof(T), __alignof(T))) T();
    }

} // namespace Neko
