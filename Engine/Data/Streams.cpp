//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Streams.cpp
//  Neko engine
//
//  Copyright (c) 2014 Neko Code. All rights reserved.
//

#include "Blob.h"
#include "Streams.h"
#include "../FS/FileSystem.h"
#include "../FS/IStreamDevice.h"
#include "../Neko.h"

namespace Neko
{
    bool IStream::Open(const CPath& path, FS::Mode mode)
    {
        return false;
    }
    
    void IStream::Close()
    {
    
    }
    
    size_t IStream::Read(void* OutValue, const size_t Size)
    {
        return -1;
    }
    
    size_t IStream::Write(const void* InValue, const size_t Size)
    {
        return -1;
    }
    
    size_t IStream::Tell() const
    {
        return -1;
    }
   
    const void* IStream::GetBuffer() const
    {
        return nullptr;
    }
    
    size_t IStream::GetSize()
    {
        return -1;
    }
    
    bool IStream::IsEof() const
    {
        return false;
    }
    
    bool IStream::IsFile() const
    {
        return false;
    }
    
    size_t IStream::GetCurrentPos()
    {
        return -1;
    }
    
    uint8* IStream::GetCurrentPtr()
    {
        return nullptr;
    }
   
    IAllocator* IStream::GetAllocator()
    {
        return nullptr;
    }
    
    FS::IStreamDevice* IStream::GetDevice()
    {
        return nullptr;
    }

    void IStream::Release()
    {
        FS::IStreamDevice* pDevice = GetDevice(); // can be null
        if (pDevice)
        {
            pDevice->DestroyFile(this);
        }
    }
    
    IStream& IStream::operator << (const char* text)
    {
        Write(text, StringLength(text));
        return *this;
    }
    
    void IStream::GetContents(OutputBlob& blob)
    {
        size_t tmp = GetSize();
        blob.Resize((int32)tmp);
        Read(blob.GetMutableData(), tmp);
    }
}
