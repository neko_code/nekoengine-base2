//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Serializer.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Blob.h"
#include "Matrix.h"
#include "Serializer.h"

namespace Neko
{
    static uint32 AsU32(float v)
    {
        return *(uint32*)&v;
    }
    
    static float AsFloat(uint32 v)
    {
        return *(float*)&v;
    }
    
    EntityGUID TextSerializer::GetGuid(Entity entity)
    {
        return EntityMap.Get(entity);
    }
    
    void TextSerializer::Write(const char* label, Entity entity)
    {
        EntityGUID guid = EntityMap.Get(entity);
        Blob << "#" << label << "\n\t" << guid.value << "\n";
    }
    
    void TextSerializer::Write(const char* label, ComponentHandle value)
    {
        Blob << "#" << label << "\n\t" << value.index << "\n";
    }
    
    void TextSerializer::Write(const char* label, const RigidTransform& value)
    {
        Blob << "#" << label << " (" << value.pos.x << ", " << value.pos.y << ", " << value.pos.z << ") "
        << " (" << value.rot.x << ", " << value.rot.y << ", " << value.rot.z << ", " << value.rot.w << ")\n\t"
        << AsU32(value.pos.x) << "\n\t" << AsU32(value.pos.y) << "\n\t" << AsU32(value.pos.z) << "\n\t"
        << AsU32(value.rot.x) << "\n\t" << AsU32(value.rot.y) << "\n\t" << AsU32(value.rot.z) << "\n\t"
        << AsU32(value.rot.w) << "\n";
    }
    
    void TextSerializer::Write(const char* label, const Vec3& value)
    {
        Blob << "#" << label << " (" << value.x << ", " << value.y << ", " << value.z << ")\n\t" << AsU32(value.x) << "\n\t"
        << AsU32(value.y) << "\n\t" << AsU32(value.z) << "\n";
    }
    
    void TextSerializer::Write(const char* label, const Vec4& value)
    {
        Blob << "#" << label << " (" << value.x << ", " << value.y << ", " << value.z << ", " << value.w << ")\n\t"
        << AsU32(value.x) << "\n\t" << AsU32(value.y) << "\n\t" << AsU32(value.z) << "\n\t" << AsU32(value.w) << "\n";
    }
    
    void TextSerializer::Write(const char* label, const Quat& value)
    {
        Blob << "#" << label << " (" << value.x << ", " << value.y << ", " << value.z << ", " << value.w << ")\n\t"
        << AsU32(value.x) << "\n\t" << AsU32(value.y) << "\n\t" << AsU32(value.z) << "\n\t" << AsU32(value.w) << "\n";
    }
    
    void TextSerializer::Write(const char* label, float value)
    {
        Blob << "#" << label << " " << value << "\n\t" << AsU32(value) << "\n";
    }
    
    void TextSerializer::Write(const char* label, bool value)
    {
        Blob << "#" << label << "\n\t" << (uint32)value << "\n";
    }
    
    void TextSerializer::Write(const char* label, const char* value)
    {
        Blob << "#" << label << "\n\t\"" << value << "\"\n";
    }
    
    void TextSerializer::Write(const char* label, uint32 value)
    {
        Blob << "#" << label << "\n\t" << value << "\n";
    }
    
    void TextSerializer::Write(const char* label, int64 value)
    {
        Blob << "#" << label << "\n\t" << value << "\n";
    }
    
    void TextSerializer::Write(const char* label, uint64 value)
    {
        Blob << "#" << label << "\n\t" << value << "\n";
    }
    
    void TextSerializer::Write(const char* label, int32 value)
    {
        Blob << "#" << label << "\n\t" << value << "\n";
    }
    
    void TextSerializer::Write(const char* label, int8 value)
    {
        Blob << "#" << label << "\n\t" << value << "\n";
    }
    
    void TextSerializer::Write(const char* label, uint8 value)
    {
        Blob << "#" << label << "\n\t" << value << "\n";
    }
    
    Entity TextDeserializer::GetEntity(EntityGUID guid)
    {
        return EntityMap.Get(guid);
    }
    
    void TextDeserializer::Read(Entity* entity)
    {
        EntityGUID guid;
        Read(&guid.value);
        *entity = EntityMap.Get(guid);
    }
    
    void TextDeserializer::Read(RigidTransform* value)
    {
        Skip();
        value->pos.x = AsFloat(ReadU32());
        Skip();
        value->pos.y = AsFloat(ReadU32());
        Skip();
        value->pos.z = AsFloat(ReadU32());
        Skip();
        value->rot.x = AsFloat(ReadU32());
        Skip();
        value->rot.y = AsFloat(ReadU32());
        Skip();
        value->rot.z = AsFloat(ReadU32());
        Skip();
        value->rot.w = AsFloat(ReadU32());
    }
    
    void TextDeserializer::Read(Vec3* value)
    {
        Skip();
        value->x = AsFloat(ReadU32());
        Skip();
        value->y = AsFloat(ReadU32());
        Skip();
        value->z = AsFloat(ReadU32());
    }
    
    void TextDeserializer::Read(Vec4* value)
    {
        Skip();
        value->x = AsFloat(ReadU32());
        Skip();
        value->y = AsFloat(ReadU32());
        Skip();
        value->z = AsFloat(ReadU32());
        Skip();
        value->w = AsFloat(ReadU32());
    }
    
    void TextDeserializer::Read(Quat* value)
    {
        Skip();
        value->x = AsFloat(ReadU32());
        Skip();
        value->y = AsFloat(ReadU32());
        Skip();
        value->z = AsFloat(ReadU32());
        Skip();
        value->w = AsFloat(ReadU32());
    }
    
    void TextDeserializer::Read(ComponentHandle* value)
    {
        Skip();
        value->index = ReadU32();
    }
    
    void TextDeserializer::Read(float* value)
    {
        Skip();
        *value = AsFloat(ReadU32());
    }
    
    void TextDeserializer::Read(bool* value)
    {
        Skip();
        *value = ReadU32() != 0;
    }
    
    void TextDeserializer::Read(uint32* value)
    {
        Skip();
        *value = ReadU32();
    }
    
    void TextDeserializer::Read(uint64* value)
    {
        Skip();
        char tmp[40];
        char* c = tmp;
        *c = Blob.ReadChar();
        while (*c >= '0' && *c <= '9' && (c - tmp) < lengthOf(tmp))
        {
            ++c;
            *c = Blob.ReadChar();
        }
        *c = 0;
        FromCString(tmp, lengthOf(tmp), value);
    }
    
    void TextDeserializer::Read(int64* value)
    {
        Skip();
        char tmp[40];
        char* c = tmp;
        *c = Blob.ReadChar();
        if (*c == '-')
        {
            ++c;
            *c = Blob.ReadChar();
        }
        while (*c >= '0' && *c <= '9' && (c - tmp) < lengthOf(tmp))
        {
            ++c;
            *c = Blob.ReadChar();
        }
        *c = 0;
        FromCString(tmp, lengthOf(tmp), value);
    }
    
    void TextDeserializer::Read(int32* value)
    {
        Skip();
        char tmp[20];
        char* c = tmp;
        *c = Blob.ReadChar();
        if (*c == '-')
        {
            ++c;
            *c = Blob.ReadChar();
        }
        while (*c >= '0' && *c <= '9' && (c - tmp) < lengthOf(tmp))
        {
            ++c;
            *c = Blob.ReadChar();
        }
        *c = 0;
        FromCString(tmp, lengthOf(tmp), value);
    }
    
    void TextDeserializer::Read(uint8* value)
    {
        Skip();
        *value = (uint8)ReadU32();
    }
    
    void TextDeserializer::Read(int8* value)
    {
        Skip();
        *value = (int8)ReadU32();
    }
    
    void TextDeserializer::Read(char* value, int MaxSize)
    {
        Skip();
        uint8 c = Blob.ReadChar();
        assert(c == '"');
        char* out = value;
        *out = Blob.ReadChar();
        while (*out != '"' && out - value < MaxSize - 1)
        {
            ++out;
            *out = Blob.ReadChar();
        }
        assert(*out == '"');
        *out = 0;
    }
    
    uint32 TextDeserializer::ReadU32()
    {
        char tmp[20];
        char* c = tmp;
        *c = Blob.ReadChar();
        while (*c >= '0' && *c <= '9' && (c - tmp) < lengthOf(tmp))
        {
            ++c;
            *c = Blob.ReadChar();
        }
        *c = 0;
        uint32 v;
        FromCString(tmp, lengthOf(tmp), &v);
        return v;
    }
    
    void TextDeserializer::Skip()
    {
        uint8 c = Blob.ReadChar();
        if (c == '#')
        {
            while (Blob.ReadChar() != '\n');
        }
        if (c == '\t')
        {
            return;
        }
        while (Blob.ReadChar() != '\t');
    }
}
