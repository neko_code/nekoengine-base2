//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Blob.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#include "Blob.h"
#include "../Utilities/StringUtil.h"

namespace Neko
{
    OutputBlob::OutputBlob(IAllocator& allocator)
    : Data(nullptr)
    , Size(0)
    , Pos(0)
    , Allocator(&allocator)
    { }
    
    OutputBlob::OutputBlob(void* data, int size)
    : Data(data)
    , Size(size)
    , Pos(0)
    , Allocator(nullptr)
    { }
    
    OutputBlob::OutputBlob(const OutputBlob& blob, IAllocator& allocator)
    : Pos(blob.Pos)
    , Allocator(&allocator)
    {
        if (blob.Size > 0)
        {
            Data = allocator.Allocate(blob.Size);
            CopyMemory(Data, blob.Data, blob.Size);
            Size = blob.Size;
        }
        else
        {
            Data = nullptr;
            Size = 0;
        }
    }
    
    OutputBlob::OutputBlob(const InputBlob& blob, IAllocator& allocator)
    : Pos(blob.GetSize())
    , Allocator(&allocator)
    {
        if (blob.GetSize() > 0)
        {
            Data = allocator.Allocate(blob.GetSize());
            CopyMemory(Data, blob.GetData(), blob.GetSize());
            Size = blob.GetSize();
        }
        else
        {
            Data = nullptr;
            Size = 0;
        }
    }
    
    OutputBlob::~OutputBlob()
    {
        if (Allocator)
        {
            Allocator->Deallocate(Data);
        }
    }
    
    OutputBlob::OutputBlob(const OutputBlob& rhs)
    {
        Allocator = rhs.Allocator;
        Pos = rhs.Pos;
        if (rhs.Size > 0)
        {
            Data = Allocator->Allocate(rhs.Size);
            CopyMemory(Data, rhs.Data, rhs.Size);
            Size = rhs.Size;
        }
        else
        {
            Data = nullptr;
            Size = 0;
        }
    }
    
    OutputBlob& OutputBlob::operator << (const char* String)
    {
        Write(String, Neko::StringLength(String));
        return *this;
    }
    
    OutputBlob& OutputBlob::operator << (int32 Value)
    {
        char tmp[20];
        Neko::ToCString(Value, tmp, lengthOf(tmp));
        Write(tmp, Neko::StringLength(tmp));
        return *this;
    }
    
    OutputBlob& OutputBlob::operator << (uint32 Value)
    {
        char tmp[20];
        ToCString(Value, tmp, lengthOf(tmp));
        Write(tmp, Neko::StringLength(tmp));
        return *this;
    }
    
    OutputBlob& OutputBlob::operator << (uint64 value)
    {
        char tmp[40];
        Neko::ToCString(value, tmp, Neko::lengthOf(tmp));
        Write(tmp, Neko::StringLength(tmp));
        return *this;
    }
    
    OutputBlob& OutputBlob::operator << (int64 value)
    {
        char tmp[40];
        Neko::ToCString(value, tmp, Neko::lengthOf(tmp));
        Write(tmp, Neko::StringLength(tmp));
        return *this;
    }

    OutputBlob& OutputBlob::operator << (float Value)
    {
        char tmp[30];
        ToCString(Value, tmp, lengthOf(tmp), 6);
        Write(tmp, Neko::StringLength(tmp));
        return *this;
    }
    
    void OutputBlob::operator =(const OutputBlob& rhs)
    {
        assert(rhs.Allocator);
        if (Allocator)
        {
            Allocator->Deallocate(Data);
        }
        
        Allocator = rhs.Allocator;
        Pos = rhs.Pos;
        if (rhs.Size > 0)
        {
            Data = Allocator->Allocate(rhs.Size);
            CopyMemory(Data, rhs.Data, rhs.Size);
            Size = rhs.Size;
        }
        else
        {
            Data = nullptr;
            Size = 0;
        }
    }
    
    void OutputBlob::Write(const void* InData, int32 InSize)
    {
        if (!InSize)
        {
            return;
        }
        
        if (Pos + InSize > Size)
        {
            Reserve((Pos + InSize) << 1);
        }
        CopyMemory((uint8*)Data + Pos, InData, InSize);
        Pos += InSize;
    }
    
    void OutputBlob::WriteString(const char* String)
    {
        if (String)
        {
            int32 size = StringLength(String) + 1;
            Write(size);
            Write(String, size);
        }
        else
        {
            Write((int32)0);
        }
    }
    
    void OutputBlob::Write(const Text& string)
    {
        WriteString(string.c_str());
    }
    
    void OutputBlob::Clear()
    {
        Pos = 0;
    }
    
    void OutputBlob::Reserve(int32 size)
    {
        if (size <= Size)
        {
            return;
        }
        
        assert(Allocator);
        
        uint8* tmp = (uint8*)Allocator->Allocate(size);
        CopyMemory(tmp, Data, Size);
        Allocator->Deallocate(Data);
        
        Data = tmp;
        Size = size;
    }
    
    void OutputBlob::Resize(int32 size)
    {
        Pos = size;
        if (size <= Size)
        {
            return;
        }
        
        assert(Allocator);
        
        uint8* tmp = (uint8*)Allocator->Allocate(size);
        CopyMemory(tmp, Data, Size);
        Allocator->Deallocate(Data);
        
        Data = tmp;
        Size = size;
    }
    
    // Input blob
    
    InputBlob::InputBlob(const void* InData, int32 InSize)
    : Data((const uint8*)InData)
    , Size(InSize)
    , Pos(0)
    { }
    
    InputBlob::InputBlob(const OutputBlob& blob)
    : Data((const uint8*)blob.GetData())
    , Size(blob.GetPos())
    , Pos(0)
    { }
    
    const void* InputBlob::Skip(int32 size)
    {
        auto* pos = Data + Pos;
        Pos += size;
        if (Pos > Size)
        {
            Pos = Size;
        }
        
        return (const void*)pos;
    }
    
    bool InputBlob::Read(void* OutData, int32 InSize)
    {
        // leftovers
        if (Pos + (int32)InSize > Size)
        {
            for (int32 i = 0; i < InSize; ++i)
            {
                ((uint8*)OutData)[i] = 0;
            }
            return false;
        }
        // Read actual data
        if (InSize)
        {
            CopyMemory(OutData, ((char*)Data) + Pos, InSize);
        }
        Pos += InSize;
        return true;
    }
    
    bool InputBlob::ReadString(char* OutData, int32 MaxSize)
    {
        int32 size;
        Read(size);
        assert(size <= MaxSize);
        return Read(OutData, size < MaxSize ? size : MaxSize);
    }
    
    bool InputBlob::Read(Text& string)
    {
        int32 size;
        Read(size);
        
        // @todo wtf
        char* buffer = (char*)malloc(sizeof(char) * size);
        bool res = Read(buffer, size);
        string = Text(buffer, size);
        free(buffer);
        return res;
    }
}
