//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  LifoAllocator.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "IAllocator.h"

namespace Neko
{
	class LIFOAllocator : public IAllocator
	{
		public:
			LIFOAllocator(IAllocator& source, size_t bucket_size)
				: Source(source)
				, m_bucket_size(bucket_size)
			{
				m_bucket = source.Allocate(bucket_size);
				m_current = m_bucket;
			}


			~LIFOAllocator()
			{
				Source.Deallocate(m_bucket);
			}


			void* Allocate(size_t size) override
			{
				uint8* new_address = (uint8*)m_current;
				assert(new_address + size <= (uint8*)m_bucket + m_bucket_size);
				m_current = new_address + size + sizeof(size_t);
				*(size_t*)(new_address + size) = size;
				return new_address;
			}


			void Deallocate(void* ptr) override
			{
				if (!ptr) return;
				size_t last_size = *(size_t*)((uint8*)m_current - sizeof(size_t));
				uint8* last_mem = (uint8*)m_current - last_size - sizeof(size_t);
				assert(last_mem == ptr);
				m_current = ptr;
			}


			void* Reallocate(void* ptr, size_t size) override
			{
				if (!ptr) return Allocate(size);

				size_t last_size = *(size_t*)((uint8*)m_current - sizeof(size_t));
				uint8* last_mem = (uint8*)m_current - last_size - sizeof(size_t);
				assert(last_mem == ptr);
				m_current = last_mem + size + sizeof(size_t);
				*(size_t*)(last_mem + size) = size;
				return ptr;
			}


			void* AllocateAligned(size_t size, size_t align) override
			{
				size_t padding = (align - ((uptr)m_current % align)) % align;
				uint8* new_address = (uint8*)m_current + padding;
				assert(new_address + size <= (uint8*)m_bucket + m_bucket_size);
				m_current = new_address + size + sizeof(size_t);
				*(size_t*)(new_address + size) = size + padding;
				assert((uptr)new_address % align == 0);
				return new_address;
			}


			void DeallocateAligned(void* ptr) override
			{
				if (!ptr) return;
//				size_t last_size_with_padding = *(size_t*)((uint8*)m_current - sizeof(size_t));
//				uint8* last_mem = (uint8*)m_current - last_size_with_padding - sizeof(size_t);
				m_current = ptr;
			}


			void* ReallocateAligned(void* ptr, size_t size, size_t align) override
			{
				if (!ptr) return AllocateAligned(size, align);

				size_t last_size_with_padding = *(size_t*)((uint8*)m_current - sizeof(size_t));
				uint8* last_mem = (uint8*)m_current - last_size_with_padding - sizeof(size_t);

				size_t padding = (align - ((uptr)last_mem % align)) % align;
				uint8* new_address = (uint8*)last_mem + padding;
				assert(new_address + size <= (uint8*)m_bucket + m_bucket_size);
				m_current = new_address + size + sizeof(size_t);
				*(size_t*)(new_address + size) = size + padding;
				assert((uptr)new_address % align == 0);
				return new_address;
			}


		private:
			IAllocator& Source;
			size_t m_bucket_size;
			void* m_bucket;
			void* m_current;
	};
} // namespace Neko
