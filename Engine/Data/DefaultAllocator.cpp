#include "DefaultAllocator.h"
#include <cstdlib>
#ifndef _WIN32
//	#include <malloc.h>
#include <mm_malloc.h>
#include <malloc/malloc.h>
#endif

#include "../Platform/SystemShared.h"

namespace Neko
{
    NEKO_THREAD_LOCAL MemStackInternal<1024 * 1024>* MemStack::ThreadMemStack = nullptr;
    
    void MemStack::BeginThread()
    {
        if (ThreadMemStack != nullptr)
        {
            EndThread();
        }
        
        ThreadMemStack = new MemStackInternal<1024 * 1024>();//NEKO_NEW(GetDefaultAllocator(), MemStackInternal<1024 * 1024>)();
    }
    
    void MemStack::EndThread()
    {
        if (ThreadMemStack != nullptr)
        {
            delete ThreadMemStack;//NEKO_DELETE(GetDefaultAllocator(), ThreadMemStack);
            ThreadMemStack = nullptr;
        }
    }
    
    uint8* MemStack::Alloc(uint32 numBytes)
    {
        assert(ThreadMemStack != nullptr && "Stack allocation failed. Did you call BeginThread?");
        
        return ThreadMemStack->Alloc(numBytes);
    }
    
    void MemStack::DeallocLast(uint8* data)
    {
        assert(ThreadMemStack != nullptr && "Stack deallocation failed. Did you call BeginThread?");
        
        ThreadMemStack->dealloc(data);
    }
    
    // @move me
    void* AddPtrSize(void* p, size_t x)
    {
        return (void*)(reinterpret_cast<uptr>(p)+x);
    }
    
    const void* AddPtrSize(const void* p, size_t x)
    {
        return (const void*)(reinterpret_cast<uptr>(p)+x);
    }
    
    void* SubtractPtrSize(void* p, size_t x)
    {
        return (void*)(reinterpret_cast<uptr>(p)-x);
    }
    
    const void* SubtractPtrSize(const void* p, size_t x)
    {
        return (const void*)(reinterpret_cast<uptr>(p)-x);
    }
    
    CPoolAllocator::CPoolAllocator(size_t objsz, uint8 align, size_t sz, void* mem)
    : IAllocator(/*sz, mem*/)
    , ObjectSize(objsz)
    , ObjectAlign(align)
    {
        assert(objsz > sizeof(void*));
        
        uint8 adjustment = GetForwardAlignAdjustment(mem, align);
        pFreeList = (void **)AddPtrSize(mem, adjustment);
        
        size_t numObj = (sz - adjustment) / objsz;
        
        void** ptr = pFreeList;
        
        // Free blocks list.
        for (size_t i(0); i < numObj - 1; ++i)
        {
            *ptr = AddPtrSize(ptr, objsz);
            ptr = (void**)* ptr;
        }
        
        *ptr = nullptr;
    }
    
    CPoolAllocator::~CPoolAllocator()
    {
        pFreeList = nullptr;
    }
    
    void* CPoolAllocator::Allocate(size_t size/*, uint8 align*/)
    {
        // assert size == objsz and if alignment == objalign
        
        if (pFreeList == nullptr)
        {
            return nullptr;
        }
        
        void* ptr = pFreeList;
        pFreeList = (void**)(*pFreeList);
        
        // @todo
//        UsedMemory += size;
//        ++NumAllocs;
        
        return ptr;
    }
    
    void* CPoolAllocator::Reallocate(void *ptr, size_t size/*, size_t Alignment*/)
    {
        return nullptr;
    }
    
    void CPoolAllocator::Deallocate(void* ptr)
    {
        *((void **)ptr) = pFreeList;
        pFreeList = (void **)ptr;
        
//        UsedMemory -= ObjectSize;
//        --NumAllocs;
    }
    

    


	void* DefaultAllocator::Allocate(size_t n)
	{
		return malloc(n);
	}


	void DefaultAllocator::Deallocate(void* p)
	{
		free(p);
	}


	void* DefaultAllocator::Reallocate(void* ptr, size_t size)
	{
		return realloc(ptr, size);
	}

#if NEKO_WINDOWS_FAMILY
	void* DefaultAllocator::AllocateAligned(size_t size, size_t align)
	{
		return _aligned_malloc(size, align);
	}


	void DefaultAllocator::DeallocateAligned(void* ptr)
	{
		_aligned_free(ptr);
	}


	void* DefaultAllocator::ReallocateAligned(void* ptr, size_t size, size_t align)
	{
		return _aligned_realloc(ptr, size, align);
	}
#else
	void* DefaultAllocator::AllocateAligned(size_t size, size_t align)
	{
        return _mm_malloc(size, align);// aligned_alloc(align, size);
	}


	void DefaultAllocator::DeallocateAligned(void* ptr)
	{
		free(ptr);
	}


	void* DefaultAllocator::ReallocateAligned(void* ptr, size_t size, size_t align)
	{
		// POSIX and glibc do not provide a way to realloc with alignment preservation
		if (size == 0) {
			free(ptr);
			return nullptr;
		}
        void* newptr = _mm_malloc(size, align);// aligned_alloc(align, size);
		if (newptr == nullptr) {
			return nullptr;
		}
		memcpy(newptr, ptr, malloc_size(ptr));
		free(ptr);
		return newptr;
	}
#endif

    
    uint8 GetBackwardAlignAdjustment(const void* address, uint8 alignment)
    {
        uint8 adjustment = reinterpret_cast<uptr>(address)& static_cast<uptr>(alignment - 1);
        
        if (adjustment == alignment)
        {
            return 0; // Already aligned
        }
        
        return adjustment;
    }
    
    uint8 GetForwardAlignAdjustmentHeader(const void* address, uint8 alignment, uint8 headerSize)
    {
        uint8 adjustment = GetForwardAlignAdjustment(address, alignment);
        
        uint8 neededSpace = headerSize;
        
        if (adjustment < neededSpace)
        {
            neededSpace -= adjustment;
            
            // increase so we can fit header struct
            adjustment += alignment * (neededSpace / alignment);
            
            if (neededSpace % alignment > 0)
            {
                adjustment += alignment;
            }
        }
        
        return adjustment;
    }
    
    uint8 GetForwardAlignAdjustment(const void* address, uint8 alignment)
    {
        uint8 adjustment = alignment - (reinterpret_cast<uptr>(address)& static_cast<uptr>(alignment - 1));
        
        if (adjustment == alignment)
        {
            return 0; // Already aligned
        }
        
        return adjustment;
    }
    
    const void* GetBackwardAlign(const void* address, uint8 alignment)
    {
        return (void*)(reinterpret_cast<uptr>(address)& static_cast<uptr>(~(alignment - 1)));
    }
    
    void* GetBackwardAlign(void* address, uint8 alignment)
    {
        return (void*)(reinterpret_cast<uptr>(address)& static_cast<uptr>(~(alignment - 1)));
    }
    
    const void* GetForwardAlign(const void* address, uint8 alignment)
    {
        return (void*)((reinterpret_cast<uptr>(address)+static_cast<uptr>(alignment - 1)) & static_cast<uptr>(~(alignment - 1)));
    }
    
    void* GetForwardAlign(void* address, uint8 alignment)
    {
        return (void*)((reinterpret_cast<uptr>(address)+static_cast<uptr>(alignment - 1)) & static_cast<uptr>(~(alignment - 1)));
    }
    
    static thread_local IAllocator* GDefaultAllocator = nullptr;
    
    void SetDefaultAllocator(IAllocator& allocator)
    {
        // this can be dangerous if we allocate something and then change the default allocator, deallocating may fail
        assert(!GDefaultAllocator);
        GDefaultAllocator = &allocator;
    }
    
    IAllocator& GetDefaultAllocator()
    {
        assert(GDefaultAllocator);
        return *GDefaultAllocator;
    }
    
    void ResetDefaultAllocator()
    {
        GDefaultAllocator = nullptr;
    }
    
} // ~namespace Neko
