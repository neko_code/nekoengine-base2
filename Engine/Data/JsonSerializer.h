//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  JsonSerializer.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Core/Path.h"

namespace Neko
{    
    class IAllocator;
    class CPath;
    class IStream;
    
    /// JSON object serialiser and deserialiser.
    class NEKO_ENGINE_API JsonSerializer
    {
        friend class ErrorProxy;
    public:
        enum EAccessMode
        {
            READ,
            WRITE
        };
        
    public:
        /** Initialise serialiser instance for a file. */
        JsonSerializer(IStream& file, EAccessMode AccessMode, const CPath& path, IAllocator& allocator);
        
        ~JsonSerializer();
        
        // Serialize
        void Serialize(const char* Label, Entity value);
        void Serialize(const char* Label, ComponentHandle value);
        void Serialize(const char* Label, uint32 value);
        void Serialize(const char* Label, uint16 Value);
        void Serialize(const char* Label, float value);
        void Serialize(const char* Label, int32 value);
        void Serialize(const char* Label, const char* value);
        void Serialize(const char* Label, const CPath& value);
        void Serialize(const char* Label, bool value);
        void BeginObject();
        void BeginObject(const char* Label);
        void EndObject();
        void BeginArray(const char* Label);
        void EndArray();
        void SerializeArrayItem(Entity value);
        void SerializeArrayItem(ComponentHandle value);
        void SerializeArrayItem(uint32 value);
        void SerializeArrayItem(int32 value);
        void SerializeArrayItem(int64 value);
        void SerializeArrayItem(float value);
        void SerializeArrayItem(bool value);
        void SerializeArrayItem(const char* value);
        
        // Deserialize
        void Deserialize(const char* Label, Entity& OutValue, Entity DefaultValue);
        void Deserialize(const char* Label, ComponentHandle& OutValue, ComponentHandle DefaultValue);
        void Deserialize(const char* Label, uint32& OutValue, uint32 DefaultValue);
        void Deserialize(const char* Label, uint16& Value, uint16 DefaultValue);
        void Deserialize(const char* Label, float& OutValue, float DefaultValue);
        void Deserialize(const char* Label, int32& OutValue, int32 DefaultValue);
        void Deserialize(const char* Label, char* OutValue, int32 MaxLength, const char* DefaultValue);
        void Deserialize(const char* Label, CPath& OutValue, const CPath& DefaultValue);
        void Deserialize(const char* Label, bool& OutValue, bool DefaultValue);
        void Deserialize(char* OutValue, int32 MaxLength, const char* DefaultValue);
        void Deserialize(CPath& OutPath, const CPath& DefaultValue);
        void Deserialize(bool& OutValue, bool DefaultValue);
        void Deserialize(float& OutValue, float DefaultValue);
        void Deserialize(int32& OutValue, int32 DefaultValue);
        
        void DeserializeArrayBegin(const char* Label);
        void DeserializeArrayBegin();
        void DeserializeArrayEnd();
        bool IsArrayEnd();
        
        void DeserializeArrayItem(Entity& value, Entity DefaultValue);
        void DeserializeArrayItem(ComponentHandle& value, ComponentHandle DefaultValue);
        void DeserializeArrayItem(uint32& value, uint32 DefaultValue);
        void DeserializeArrayItem(int32& value, int32 DefaultValue);
        void DeserializeArrayItem(int64& value, int64 DefaultValue);
        void DeserializeArrayItem(float& value, float DefaultValue);
        void DeserializeArrayItem(bool& value, bool DefaultValue);
        void DeserializeArrayItem(char* value, int32 MaxLength, const char* DefaultValue);
        void DeserializeObjectBegin();
        void DeserializeObjectEnd();
        void DeserializeLabel(char* Label, int32 MaxLength);
        void DeserializeRawString(char* Buffer, int32 MaxLength);
        void NextArrayItem();
        bool IsNextBoolean() const;
        bool IsObjectEnd();
        
        NEKO_FORCE_INLINE bool IsError() const
        {
            return bIsError;
        }
        
    private:
        void DeserializeLabel(const char* label);
        void DeserializeToken();
        void DeserializeArrayComma();
        
        float TokenToFloat();
        void ExpectToken(char ExpectedToken);
        
        void WriteString(const char* String);
        void WriteBlockComma();
        
    private:
        void operator=(const JsonSerializer&);
        JsonSerializer(const JsonSerializer&);
        
    private:
        EAccessMode AccessMode;
        bool bIsFirstInBlock;
        const char* Token;
        bool bIsStringToken;
        int32 TokenSize;
        
        char Path[MAX_PATH_LENGTH];
        
        IStream& File;
        IAllocator& Allocator;
        
        const char* Data;
        int32 DataSize;
        
        bool bIsError;
        bool bOwnsData;
    };
    
    
} // !namespace Neko
