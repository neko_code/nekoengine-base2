//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MemoryStream.h
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "../FS/FileSystem.h"
#include "../Platform/SystemShared.h"
#include "../Core/Engine.h"

#include "Streams.h"

namespace Neko
{
    namespace FS
    {
        class MemoryFile;
    }
    
    struct NEKO_NO_VTABLE IMemoryBlock
    {
        virtual void* Require(const uint32 Size = 0) = 0;
        virtual uint32 GetSize() = 0;
    };
    
    /// Memory block. @todo Allocator use implementation
    class FMemoryBlock : public IMemoryBlock
    {
    public:
        
        FMemoryBlock()
        : Data(nullptr)
        , Size(0)
        { }
        
        virtual ~FMemoryBlock()
        {
            ::free(Data);
        }
        
        virtual void* Require(uint32 MoreSize = 0) override
        {
            if (MoreSize > 0)
            {
                Size += MoreSize;
                Data = ::realloc(Data, Size);
            }
            
            return Data;
        }
        
        virtual NEKO_FORCE_INLINE uint32 GetSize() override
        {
            return Size;
        }
        
    private:
        void* Data;
        uint32 Size;
    };
    
    class FStaticMemoryBlock : public IMemoryBlock
    {
    public:
        FStaticMemoryBlock(void* InData, const uint32 InSize)
        : Data((uint8*)InData)
        , Size(InSize)
        { }
        
        virtual ~FStaticMemoryBlock()
        { }
        
        virtual void* Require(uint32 MoreSize = 0) override
        {
            return Data;
        }
        
        virtual uint32 GetSize() override
        {
            return Size;
        }
        
        uint8* Data;
        uint32 Size;
    };
    
    
	/// Memory stream. Able to read and write.
    class CMemoryStream : public IStream
	{
        friend class FS::MemoryFile;
	public:

		/** 
         *  Creates stream with existing data.
         *  @param data Existing data buffer..
         *  @param size Stream buffer size.
         *  @param freeOnClose Needs to be set to TRUE only if buffer must be freed after usage.
         *  @note Allocator passed through constructors must be exactly the same which we used to allocate  buffer handle!
         */
		CMemoryStream(Byte* data, uint32 size, IAllocator& allocator, bool const freeOnClose = true);
        
        CMemoryStream(IStream& Stream);
        CMemoryStream(const TSharedPtr<IStream>& Stream);
        
        /**
         *  Creates stream with existing data.
         *  @param Memory Existing data block.
         *  @param bFreeOnClose Needs to be set to TRUE only if buffer must be freed after usage.
         *  @note Allocator passed through constructors must be exactly the same which we used to allocate  buffer handle!
         */
        CMemoryStream(IMemoryBlock* Memory, IAllocator& allocator, bool const bFreeOnClose = true);

        virtual ~CMemoryStream();
        
        virtual bool Open(const CPath& path, FS::Mode mode) override { return true; }
        
		/** Reads from defined stream. */
		virtual size_t Read(void* OutValue, const size_t size) override;

        template<typename Ty> NEKO_FORCE_INLINE bool Read(Ty* OutValue)
        {
            return Read(OutValue, sizeof(Ty));
        }

        /** Writes to this memory stream. */
        virtual size_t Write(const void* InData, const size_t Size) override;
        
        /** Write value. */ 
        template<typename Ty> NEKO_FORCE_INLINE size_t Write(const Ty& OutValue)
        {
            return Write(&OutValue, sizeof(Ty));
        }
        
        /** Sets buffer current read position. */
        virtual bool Seek(size_t Offset, ESeekLocator SeekPos) override;

        /** Returns the amount of memory we can still read. */
        NEKO_FORCE_INLINE int64 GetRemaining() const
        {
            return (ReadSize - LastPos);
        }
        
        NEKO_FORCE_INLINE const void* GetBuffer() const override
        {
            return Buffer;
        }
        
        /** Returns data pointer from last read/write position. */
        NEKO_FORCE_INLINE uint8* GetCurrentPtr() override
        {
            return &Buffer[LastPos];
        }
        
        /** Returns size of file. */ 
        virtual NEKO_FORCE_INLINE size_t GetSize() override
        {
            int64 offset = Seek(0, ESeekLocator::Current);
            int64 size = Seek(0, ESeekLocator::End);
            
            Seek(offset, ESeekLocator::Begin);
            
            return size;
        }
        
        virtual NEKO_FORCE_INLINE void Close() override
        {
            if (Buffer != nullptr)
            {
                if (bFreeOnClose)
                {
                    Allocator->Deallocate((void*)Buffer);
                    Buffer = nullptr;
                }
            }
        }
        
        virtual size_t Tell() const override { return LastPos; }
        
        virtual NEKO_FORCE_INLINE bool IsEof() const override { return LastPos >= ReadSize; }
    
        virtual NEKO_FORCE_INLINE bool IsFile() const override { return false; }
        
        virtual NEKO_FORCE_INLINE size_t GetCurrentPos() override { return LastPos; }
        
        virtual NEKO_FORCE_INLINE IAllocator* GetAllocator() override { return Allocator; }
        
    protected:
        
        virtual FS::IStreamDevice* GetDevice() override { return nullptr; }
        
    private:
        //! Buffer size.
		size_t    ReadSize;
        //! Last read position.
		size_t    LastPos;

        size_t Capacity;
        
        bool    bFreeOnClose;
        
        //! Used for buffer reallocations
        IAllocator* Allocator;
        
        uint8*  Buffer;
        
        IMemoryBlock* MemoryBlock;
	};

    class FStaticMemoryBlockStream : public CMemoryStream
    {
    public:
        
        FStaticMemoryBlockStream(void* InData, const uint32 InSize, IAllocator& allocator)
        : CMemoryStream(&MemoryBlock, allocator)
        , MemoryBlock(InData, InSize)
        { }
        
        ~FStaticMemoryBlockStream()
        { }
        
        FStaticMemoryBlock MemoryBlock;
    };
}
