//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Universe.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Universe.h"
#include "../Core/Log.h"
#include "../Core/IPlugin.h"
#include "../Data/Blob.h"
#include "../Core/Reflection.h"
#include "../Data/JsonSerializer.h"
#include "../Utilities/Crc32.h"
#include "../Math/Matrix.h"
#include <cstdint>

namespace Neko
{
    static const int32 RESERVED_ENTITIES_COUNT = 4096;
    
    Universe::Universe(IAllocator& allocator)
    : Allocator(allocator)
    , Names(allocator)
    , Entities(allocator)
    , OnComponentAddedDelegate(allocator)
    , OnComponentDestroyedDelegate(allocator)
    , OnEntityCreatedDelegate(allocator)
    , OnEntityDestroyedDelegate(allocator)
    , OnEntityMovedDelegate(allocator)
    , FirstFreeSlotIndex(-1)
    , Scenes(allocator)
    , Hierarchy(allocator)
    {
        Entities.Reserve(RESERVED_ENTITIES_COUNT);
    }
    
    Universe::~Universe()
    {
        
    }
    
    IScene* Universe::GetScene(ComponentType type) const
    {
        return ComponentTypeMap[type.index].scene;
    }
    
    IScene* Universe::GetScene(uint32 hash) const
    {
        for (auto* scene : Scenes)
        {
            if (Crc32(scene->GetPlugin().GetName()) == hash)
            {
                return scene;
            }
        }
        return nullptr;
    }
    
    
    TArray<IScene*>& Universe::GetScenes()
    {
        return Scenes;
    }
    
    void Universe::AddScene(IScene* scene)
    {
        Scenes.Push(scene);
    }
    
    
    const Vec3& Universe::GetPosition(Entity entity) const
    {
        return Entities[entity.index].position;
    }
    
    const Quat& Universe::GetRotation(Entity entity) const
    {
        return Entities[entity.index].rotation;
    }
    
    
    void Universe::TransformEntity(Entity entity, bool update_local)
    {
        int32 hierarchyIndex = Entities[entity.index].hierarchy;
        OnEntityTransformed().Invoke(entity);
        if (hierarchyIndex >= 0)
        {
            SHierarchy& h = Hierarchy[hierarchyIndex];
            FTransform myTransform = GetTransform(entity);
            if (update_local && h.parent.IsValid())
            {
                FTransform parentTransform = GetTransform(h.parent);
                
                h.local_transform = (parentTransform.Inverted() * myTransform);
            }
            
            Entity child = h.first_child;
            
            while (child.IsValid())
            {
                SHierarchy& child_h = Hierarchy[Entities[child.index].hierarchy];
                FTransform abs_tr = myTransform * child_h.local_transform;
                EntityData& child_data = Entities[child.index];
                child_data.position = abs_tr.pos;
                child_data.rotation = abs_tr.rot;
                child_data.scale = abs_tr.scale;
                TransformEntity(child, false);
                
                child = child_h.next_sibling;
            }
        }
    }

    
    void Universe::SetRotation(Entity entity, const Quat& rot)
    {
        Entities[entity.index].rotation = rot;
        TransformEntity(entity, true);
    }
    
    void Universe::SetRotation(Entity entity, float x, float y, float z, float w)
    {
        Entities[entity.index].rotation.Set(x, y, z, w);
        TransformEntity(entity, true);
    }
    
    
    bool Universe::HasEntity(Entity entity) const
    {
        return entity.index >= 0 && entity.index < Entities.GetSize() && Entities[entity.index].valid;
    }

    
    void Universe::SetMatrix(Entity entity, const Matrix& mtx)
    {
        EntityData& out = Entities[entity.index];
        mtx.Decompose(out.position, out.rotation, out.scale);
        TransformEntity(entity, true);
    }
    
    Matrix Universe::GetPositionAndRotation(Entity entity) const
    {
        auto& transform = Entities[entity.index];
        Matrix mtx = transform.rotation.ToMatrix();
        mtx.SetTranslation(transform.position);
        return mtx;
    }
    
    void Universe::SetTransform(Entity entity, const Vec3& pos, const Quat& rot, float scale)
    {
        auto& tmp = Entities[entity.index];
        tmp.position = pos;
        tmp.rotation = rot;
        tmp.scale = scale;
        TransformEntity(entity, true);
    }
    
    void Universe::SetTransform(Entity entity, const FTransform& transform)
    {
        auto& tmp = Entities[entity.index];
        tmp.position = transform.pos;
        tmp.rotation = transform.rot;
        tmp.scale = transform.scale;
        TransformEntity(entity, true);
    }
    
    FTransform Universe::GetTransform(Entity entity) const
    {
        auto& transform = Entities[entity.index];
        return { transform.position, transform.rotation, transform.scale };
    }
    
    Matrix Universe::GetMatrix(Entity entity) const
    {
        auto& transform = Entities[entity.index];
        Matrix mtx = transform.rotation.ToMatrix();
        mtx.SetTranslation(transform.position);
        mtx.Multiply3x3(transform.scale);
        return mtx;
    }
    
    void Universe::SetPosition(Entity entity, float x, float y, float z)
    {
        auto& transform = Entities[entity.index];
        transform.position.Set(x, y, z);
        TransformEntity(entity, true);
    }
    
    void Universe::SetPosition(Entity entity, const Vec3& pos)
    {
        auto& transform = Entities[entity.index];
        transform.position = pos;
        TransformEntity(entity, true);
    }
    
    void Universe::SetEntityName(Entity entity, const char* name)
    {
        int32 NameIndex = Entities[entity.index].name;
        if (NameIndex < 0)
        {
            if (name[0] == '\0')
            {
                return;
            }
            Entities[entity.index].name = Names.GetSize();
            EntityName& name_data = Names.Emplace();
            name_data.entity = entity;
            CopyString(name_data.name, name);
        }
        else
        {
            CopyString(Names[NameIndex].name, name);
        }
    }
    
    const char* Universe::GetEntityName(Entity entity) const
    {
        int32 NameIndex = Entities[entity.index].name;
        if (NameIndex < 0)
        {
            return "";
        }
        return Names[NameIndex].name;
    }
    
    void Universe::CreateEntity(Entity entity)
    {
        while (Entities.GetSize() <= entity.index)
        {
            EntityData& data = Entities.Emplace();
            data.valid = false;
            data.prev = -1;
            data.name = -1;
            data.hierarchy = -1;
            data.next = FirstFreeSlotIndex;
            data.scale = -1;
            if (FirstFreeSlotIndex >= 0)
            {
                Entities[FirstFreeSlotIndex].prev = Entities.GetSize() - 1;
            }
            FirstFreeSlotIndex = Entities.GetSize() - 1;
        }
        if (FirstFreeSlotIndex == entity.index)
        {
            FirstFreeSlotIndex = Entities[entity.index].next;
        }
        if (Entities[entity.index].prev >= 0)
        {
            Entities[Entities[entity.index].prev].next = Entities[entity.index].next;
        }
        if (Entities[entity.index].next >= 0)
        {
            Entities[Entities[entity.index].next].prev= Entities[entity.index].prev;
        }
        
        EntityData& data = Entities[entity.index];
        data.position.Set(0, 0, 0);
        data.rotation.Set(0, 0, 0, 1);
        data.scale = 1;
        data.hierarchy = -1;
        data.components = 0;
        data.valid = true;
        OnEntityCreatedDelegate.Invoke(entity);
    }
    
    Entity Universe::CreateEntity(const Vec3& position, const Quat& rotation)
    {
        EntityData* data;
        Entity entity;
        if (FirstFreeSlotIndex >= 0)
        {
            data = &Entities[FirstFreeSlotIndex];
            entity.index = FirstFreeSlotIndex;
            if (data->next >= 0)
            {
                Entities[data->next].prev = -1;
            }
            FirstFreeSlotIndex = data->next;
        }
        else
        {
            entity.index = Entities.GetSize();
            data = &Entities.Emplace();
        }
        data->position = position;
        data->rotation = rotation;
        data->scale = 1;
        data->name = -1;
        data->hierarchy = -1;
        data->components = 0;
        data->valid = true;
        OnEntityCreatedDelegate.Invoke(entity);
        
        return entity;
    }
    
    void Universe::DestroyEntity(Entity entity)
    {
        if (!entity.IsValid() || entity.index < 0)
        {
            return;
        }
        
       	EntityData& entity_data = Entities[entity.index];
        for (Entity first_child = GetFirstChild(entity); first_child.IsValid(); first_child = GetFirstChild(entity))
        {
            SetParent(INVALID_ENTITY, first_child);
        }
        SetParent(INVALID_ENTITY, entity);
        
        uint64 mask = entity_data.components;
        for (int32 i = 0; i < MAX_COMPONENTS_TYPES_COUNT; ++i)
        {
            if ((mask & ((uint64)1 << i)) != 0)
            {
                ComponentType type = {i};
                auto original_mask = mask;
                IScene* scene = ComponentTypeMap[i].scene;
                scene->DestroyComponent(scene->GetComponent(entity, type), type);
                mask = entity_data.components;
                assert(original_mask != mask);
            }
        }
        
        entity_data.next = FirstFreeSlotIndex;
        entity_data.prev = -1;
        entity_data.hierarchy = -1;
        entity_data.valid = false;
        if (FirstFreeSlotIndex >= 0)
        {
            Entities[FirstFreeSlotIndex].prev = entity.index;
        }
        
        if (entity_data.name >= 0)
        {
            Entities[Names.back().entity.index].name = entity_data.name;
            Names.EraseFast(entity_data.name);
            entity_data.name = -1;
        }
        
        FirstFreeSlotIndex = entity.index;
        OnEntityDestroyedDelegate.Invoke(entity);
    }
    
    void Universe::SerializeComponent(ISerializer& serializer, ComponentType type, ComponentHandle component)
    {
        auto* scene = ComponentTypeMap[type.index].scene;
        auto& method = ComponentTypeMap[type.index].serialize;
        (scene->*method)(serializer, component);
    }
    
    void Universe::DeserializeComponent(IDeserializer& serializer, Entity entity, ComponentType type, int SceneVersion)
    {
        auto* scene = ComponentTypeMap[type.index].scene;
        auto& method = ComponentTypeMap[type.index].deserialize;
        (scene->*method)(serializer, entity, SceneVersion);
    }

    Entity Universe::GetFirstEntity() const
    {
        for (int32 i = 0; i < Entities.GetSize(); ++i)
        {
            if (Entities[i].scale >= 0)
            {
                return { i };
            }
        }
        return INVALID_ENTITY;
    }
    
    Entity Universe::GetNextEntity(Entity entity) const
    {
        for (int32 i = entity.index + 1; i < Entities.GetSize(); ++i)
        {
            if (Entities[i].scale >= 0)
            {
                return { i };
            }
        }
        return INVALID_ENTITY;
    }
    
    Entity Universe::GetParent(Entity entity) const
    {
        int32 Index = Entities[entity.index].hierarchy;
        if (Index < 0)
        {
            return INVALID_ENTITY;
        }
        return Hierarchy[Index].parent;
    }
    
    Entity Universe::GetFirstChild(Entity entity) const
    {
        int32 Index = Entities[entity.index].hierarchy;
        if (Index < 0)
        {
            return INVALID_ENTITY;
        }
        return Hierarchy[Index].first_child;
    }
    
    Entity Universe::GetNextSibling(Entity entity) const
    {
        int32 Index = Entities[entity.index].hierarchy;
        if (Index < 0)
        {
            return INVALID_ENTITY;
        }
        return Hierarchy[Index].next_sibling;
    }
    
    bool Universe::IsDescendant(Entity ancestor, Entity descendant) const
    {
        if (!ancestor.IsValid())
        {
            return false;
        }
        
        for (Entity e = GetFirstChild(ancestor); e.IsValid(); e = GetNextSibling(e))
        {
            if (e == descendant)
            {
                return true;
            }
            if (IsDescendant(e, descendant))
            {
                return true;
            }
        }
        
        return false;
    }
    
    void Universe::SetParent(Entity newParent, Entity child)
    {
        bool bWouldCreateLoop = IsDescendant(child, newParent);
        if (bWouldCreateLoop)
        {
            GLogError.log("Engine") << "Could not make a hierarchy because it would create a loop.";
            return;
        }
        
        auto CollectGarbage = [this](Entity entity)
        {
            SHierarchy& h = Hierarchy[Entities[entity.index].hierarchy];
            if (h.parent.IsValid())
            {
                return;
            }
            if (h.first_child.IsValid())
            {
                return;
            }
            
            const SHierarchy& last = Hierarchy.back();
            Entities[last.entity.index].hierarchy = Entities[entity.index].hierarchy;
            Entities[entity.index].hierarchy = -1;
            h = last;
            Hierarchy.Pop();
        };
        
        int32 childIndex = Entities[child.index].hierarchy;
        
        if (childIndex >= 0)
        {
            Entity old_parent = Hierarchy[childIndex].parent;
            
            if (old_parent.IsValid())
            {
                SHierarchy& old_parent_h = Hierarchy[Entities[old_parent.index].hierarchy];
                Entity* x = &old_parent_h.first_child;
                while (x->IsValid())
                {
                    if (*x == child)
                    {
                        *x = GetNextSibling(child);
                        break;
                    }
                    x = &Hierarchy[Entities[x->index].hierarchy].next_sibling;
                }
                Hierarchy[childIndex].parent = INVALID_ENTITY;
                Hierarchy[childIndex].next_sibling = INVALID_ENTITY;
                CollectGarbage(old_parent);
            }
        }
        else if (newParent.IsValid())
        {
            childIndex = Hierarchy.GetSize();
            Entities[child.index].hierarchy = childIndex;
            SHierarchy& h = Hierarchy.Emplace();
            h.entity = child;
            h.parent = INVALID_ENTITY;
            h.first_child = INVALID_ENTITY;
            h.next_sibling = INVALID_ENTITY;
        }
        
        if (newParent.IsValid())
        {
            int32 newParentIndex = Entities[newParent.index].hierarchy;
            if (newParentIndex < 0)
            {
                newParentIndex = Hierarchy.GetSize();
                Entities[newParent.index].hierarchy = newParentIndex;
                SHierarchy& h = Hierarchy.Emplace();
                h.entity = newParent;
                h.parent = INVALID_ENTITY;
                h.first_child = INVALID_ENTITY;
                h.next_sibling = INVALID_ENTITY;
            }
            
            Hierarchy[childIndex].parent = newParent;
            FTransform parentTransform = GetTransform(newParent);
            FTransform child_tr = GetTransform(child);
            
            Hierarchy[childIndex].local_transform = parentTransform.Inverted() * child_tr;
            Hierarchy[childIndex].next_sibling = Hierarchy[newParentIndex].first_child;
            Hierarchy[newParentIndex].first_child = child;
        }
        else
        {
            if (childIndex >= 0)
            {
                CollectGarbage(child);
            }
        }
    }
    
    void Universe::UpdateGlobalTransform(Entity entity)
    {
        const SHierarchy& h = Hierarchy[Entities[entity.index].hierarchy];
        FTransform parentTransform = GetTransform(h.parent);
        
        FTransform new_tr = parentTransform * h.local_transform;
        SetTransform(entity, new_tr);
    }

    void Universe::SetLocalPosition(Entity entity, const Vec3& pos)
    {
        int hierarchyIndex = Entities[entity.index].hierarchy;
        if (hierarchyIndex < 0)
        {
            SetPosition(entity, pos);
            return;
        }
        Hierarchy[hierarchyIndex].local_transform.pos = pos;
        UpdateGlobalTransform(entity);
    }
    
    void Universe::SetLocalRotation(Entity entity, const Quat& rot)
    {
        int hierarchyIndex = Entities[entity.index].hierarchy;
        if (hierarchyIndex < 0)
        {
            SetRotation(entity, rot);
            return;
        }
        Hierarchy[hierarchyIndex].local_transform.rot = rot;
        UpdateGlobalTransform(entity);
    }
    
    FTransform Universe::ComputeLocalTransform(Entity parent, const FTransform& globalTransform) const
    {
        FTransform parentTransform = GetTransform(parent);
        return parentTransform.Inverted() * globalTransform;
    }
    
    void Universe::SetLocalTransform(Entity entity, const FTransform& transform)
    {
        int hierarchyIndex = Entities[entity.index].hierarchy;
        if (hierarchyIndex < 0)
        {
            SetTransform(entity, transform);
            return;
        }
        
        SHierarchy& h = Hierarchy[hierarchyIndex];
        h.local_transform = transform;
        UpdateGlobalTransform(entity);
    }

    FTransform Universe::GetLocalTransform(Entity entity) const
    {
        int hierarchyIndex = Entities[entity.index].hierarchy;
        if (hierarchyIndex < 0)
        {
            return GetTransform(entity);
        }
        
        return Hierarchy[hierarchyIndex].local_transform;
    }
    
    float Universe::GetLocalScale(Entity entity) const
    {
        int hierarchyIndex = Entities[entity.index].hierarchy;
        if (hierarchyIndex < 0)
        {
            return GetScale(entity);
        }
        
        return Hierarchy[hierarchyIndex].local_transform.scale;
    }

    void Universe::SerializeBlob(OutputBlob& serializer)
    {
        serializer.Write((int32)Entities.GetSize());
        serializer.Write(&Entities[0], sizeof(Entities[0]) * Entities.GetSize());
        serializer.Write((int32)Names.GetSize());
        for (const EntityName& name : Names)
        {
            serializer.Write(name.entity);
            serializer.WriteString(name.name);
        }
        serializer.Write(FirstFreeSlotIndex);
        
        serializer.Write(Hierarchy.GetSize());
        if (!Hierarchy.IsEmpty())
        {
            serializer.Write(&Hierarchy[0], sizeof(Hierarchy[0]) * Hierarchy.GetSize());
        }
    }
    
    void Universe::DeserializeBlob(InputBlob& serializer)
    {
        int32 count;
        serializer.Read(count);
        Entities.Resize(count);

        if (count > 0)
        {
            serializer.Read(&Entities[0], sizeof(Entities[0]) * Entities.GetSize());
        }
        
        serializer.Read(count);
        
        for (int32 i = 0; i < count; ++i)
        {
            EntityName& name = Names.Emplace();
            serializer.Read(name.entity);
            serializer.ReadString(name.name, lengthOf(name.name));
            Entities[name.entity.index].name = Names.GetSize() - 1;
        }
        
        serializer.Read(FirstFreeSlotIndex);
        
        serializer.Read(count);
        Hierarchy.Resize(count);
        if (count > 0)
        {
            serializer.Read(&Hierarchy[0], sizeof(Hierarchy[0]) * Hierarchy.GetSize());
        }
    }
    

    struct PrefabEntityGUIDMap final : public ILoadEntityGUIDMap
    {
        PrefabEntityGUIDMap(const TArray<Entity>& _entities)
        : entities(_entities)
        {
        }
        
        Entity Get(EntityGUID guid) override
        {
            if (guid.value >= entities.GetSize())
            {
                return INVALID_ENTITY;
            }
            return entities[(int)guid.value];
        }
        
        const TArray<Entity>& entities;
    };
    
    
    Entity Universe::InstantiatePrefab(const PrefabResource& prefab, const Vec3& pos, const Quat& rot, float scale)
    {
        InputBlob blob(prefab.blob.GetData(), prefab.blob.GetPos());
        TArray<Entity> entities(Allocator);
        PrefabEntityGUIDMap entity_map(entities);
        TextDeserializer deserializer(blob, entity_map);
        
        uint32 version;
        deserializer.Read(&version);
        if (version > (int)PrefabVersion::LAST)
        {
            GLogError.log("Engine") << "Prefab " << prefab.GetPath() << " has unsupported version.";
            return INVALID_ENTITY;
        }
        int count;
        deserializer.Read(&count);
        
        int entity_idx = 0;
        entities.Reserve(count);
        
        for (int32 i = 0; i < count; ++i)
        {
            entities.Push(CreateEntity({0, 0, 0}, {0, 0, 0, 1}));
        }
        
        while (blob.GetPosition() < blob.GetSize())
        {
            uint64 prefab;
            deserializer.Read(&prefab);
            Entity entity = entities[entity_idx];
            SetTransform(entity, {pos, rot, scale});
            
            if (version > (int)PrefabVersion::WITH_HIERARCHY)
            {
                Entity parent;
                
                deserializer.Read(&parent);
                if (parent.IsValid())
                {
                    RigidTransform local_tr;
                    deserializer.Read(&local_tr);
                    float scale;
                    deserializer.Read(&scale);
                    SetParent(parent, entity);
                    SetLocalTransform(entity, {local_tr.pos, local_tr.rot, scale});
                }
            }
            
            uint32 cmp_type_hash;
            deserializer.Read(&cmp_type_hash);
            while (cmp_type_hash != 0)
            {
                ComponentType componentType = Reflection::GetComponentTypeFromHash(cmp_type_hash);
                int SceneVersion;
                deserializer.Read(&SceneVersion);
                DeserializeComponent(deserializer, entity, componentType, SceneVersion);
                deserializer.Read(&cmp_type_hash);
            }
            ++entity_idx;
        }
        return entities[0];
    }

    void Universe::SetScale(Entity entity, float scale)
    {
        auto& transform = Entities[entity.index];
        transform.scale = scale;
        TransformEntity(entity, true);
    }
    
    float Universe::GetScale(Entity entity) const
    {
        auto& transform = Entities[entity.index];
        return transform.scale;
    }
    
    ComponentUID Universe::GetFirstComponent(Entity entity) const
    {
        uint64 mask = Entities[entity.index].components;
        for (int32 i = 0; i < MAX_COMPONENTS_TYPES_COUNT; ++i)
        {
            if ((mask & (uint64(1) << i)) != 0)
            {
                IScene* scene = ComponentTypeMap[i].scene;
                return ComponentUID(entity, { i }, scene, scene->GetComponent(entity, {i}));
            }
        }
        return ComponentUID::INVALID;
    }
    
    ComponentUID Universe::GetNextComponent(const ComponentUID& component) const
    {
        uint64 mask = Entities[component.entity.index].components;
        for (int32 i = component.type.index + 1; i < MAX_COMPONENTS_TYPES_COUNT; ++i)
        {
            if ((mask & (uint64(1) << i)) != 0)
            {
                IScene* scene = ComponentTypeMap[i].scene;
                return ComponentUID(component.entity, {i}, scene, scene->GetComponent(component.entity, {i}));
            }
        }
        return ComponentUID::INVALID;
    }
    
    ComponentUID Universe::GetComponent(Entity entity, ComponentType componentType) const
    {
        uint64 mask = Entities[entity.index].components;
        if ((mask & (uint64(1) << componentType.index)) == 0)
        {
            return ComponentUID::INVALID;
        }
        IScene* scene = ComponentTypeMap[componentType.index].scene;
        return ComponentUID(entity, componentType, scene, scene->GetComponent(entity, componentType));
    }
    
    bool Universe::HasComponent(Entity entity, ComponentType componentType) const
    {
        uint64 mask = Entities[entity.index].components;
        return (mask & (uint64(1) << componentType.index)) != 0;
    }
    
    void Universe::DestroyComponent(Entity entity, ComponentType componentType, IScene* scene, ComponentHandle index)
    {
        auto mask = Entities[entity.index].components;
        auto old_mask = mask;
        mask &= ~((uint64)1 << componentType.index);
//        auto x = Reflection::GetComponentTypeID(componentType.index);
        assert(old_mask != mask);
        Entities[entity.index].components = mask;
        OnComponentDestroyedDelegate.Invoke(ComponentUID(entity, componentType, scene, index));
    }
    
    void Universe::AddComponent(Entity entity, ComponentType componentType, IScene* scene, ComponentHandle index)
    {
        ComponentUID component(entity, componentType, scene, index);
        Entities[entity.index].components |= (uint64)1 << componentType.index;
        OnComponentAddedDelegate.Invoke(component);
    }
    
    void Universe::SetTransformKeepChildren(Entity entity, const FTransform& transform)
    {
        auto& tmp = Entities[entity.index];
        tmp.position = transform.pos;
        tmp.rotation = transform.rot;
        tmp.scale = transform.scale;
        int hierarchyIndex = Entities[entity.index].hierarchy;
        OnEntityTransformed().Invoke(entity);
        if (hierarchyIndex >= 0)
        {
            SHierarchy& h = Hierarchy[hierarchyIndex];
            FTransform myTransform = GetTransform(entity);
            if (h.parent.IsValid())
            {
                FTransform parentTransform = GetTransform(h.parent);
                h.local_transform = parentTransform.Inverted() * myTransform;
            }
            Entity child = h.first_child;
            while (child.IsValid())
            {
                SHierarchy& child_h = Hierarchy[Entities[child.index].hierarchy];
                child_h.local_transform = myTransform.Inverted() * GetTransform(child);
                child = child_h.next_sibling;
            }
        }
        
    }
    
} // namespace Neko
