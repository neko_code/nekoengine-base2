//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Universe.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Utilities/StringUtil.h"
#include "../Platform/SystemShared.h"
#include "../Containers/Array.h"
#include "../Containers/AssociativeArray.h"
#include "../Containers/DelegateList.h"
#include "../Data/Serializer.h"
#include "../Math/Quat.h"
#include "../Math/Matrix.h"
#include "../Math/Vec.h"
#include "../Core/Prefab.h"
#include "../Core/Path.h"
#include "Component.h"

namespace Neko
{
    class Universe;
    class InputBlob;
    class OutputBlob;
    struct Matrix;
    struct FTransform;
    
    enum
    {
        MAX_COMPONENTS_TYPES_COUNT = 64
    };
    
    class NEKO_ENGINE_API Universe
    {
    public:
        
        typedef void (IScene::*Serialize)(ISerializer&, ComponentHandle);
        typedef void (IScene::*Deserialize)(IDeserializer&, Entity, int);
        
        struct ComponentTypeEntry
        {
            IScene* scene = nullptr;
            void (IScene::*serialize)(ISerializer&, ComponentHandle);
            void (IScene::*deserialize)(IDeserializer&, Entity, int);
        };
        
        // constants
        enum
        {
            ENTITY_NAME_MAX_LENGTH = 32
        };
        
    public:
        
        explicit Universe(IAllocator& allocator);
        
        ~Universe();
        
        
        NEKO_FORCE_INLINE IAllocator& GetAllocator() { return Allocator; }
        
        /** Creates the new entity in the universe. */
        void CreateEntity(Entity entity);
        
        /** 
         * Creates the new entity in the universe.
         * 
         * @param Position  Entity translation in the world.
         * @param Rotation  Entity rotation in the world.
         */
        Entity CreateEntity(const Vec3& position, const Quat& rotation);
        
        /** Destroys the entity. */
        void DestroyEntity(Entity entity);
        
        void AddComponent(Entity entity, ComponentType componentType, IScene* scene, ComponentHandle index);
        void DestroyComponent(Entity entity, ComponentType componentType, IScene* scene, ComponentHandle index);
        bool HasComponent(Entity entity, ComponentType componentType) const;
        
        ComponentUID GetComponent(Entity entity, ComponentType type) const;
        ComponentUID GetFirstComponent(Entity entity) const;
        ComponentUID GetNextComponent(const ComponentUID& component) const;
        
        ComponentTypeEntry& RegisterComponentType(ComponentType type)
        {
            return ComponentTypeMap[type.index];
        }
        
        template <typename T1, typename T2>
        void RegisterComponentType(ComponentType type, IScene* scene, T1 serialize, T2 deserialize)
        {
            ComponentTypeMap[type.index].scene = scene;
            ComponentTypeMap[type.index].serialize = static_cast<Serialize>(serialize);
            ComponentTypeMap[type.index].deserialize = static_cast<Deserialize>(deserialize);
        }
        
        Entity InstantiatePrefab(const PrefabResource& prefab, const Vec3& pos, const Quat& rot, float scale);
        
        void SerializeComponent(ISerializer& serializer, ComponentType type, ComponentHandle component);
        void DeserializeComponent(IDeserializer& serializer, Entity entity, ComponentType type, int SceneVersion);
        
        /** Returns the first iterated valid entity. */
        Entity GetFirstEntity() const;
        
        /** Iterates after the given entity and returns the next valid one. */
        Entity GetNextEntity(Entity entity) const;
        
        /** Returns the name string of entity. */
        const char* GetEntityName(Entity entity) const;
        
        /** Sets the entity name in the universe. */
        void SetEntityName(Entity entity, const char* Name);
        
        /** Returns true if the entity is in the universe. */
        bool HasEntity(Entity entity) const;
        
        bool IsDescendant(Entity ancestor, Entity descendant) const;
        Entity GetParent(Entity entity) const;
        Entity GetFirstChild(Entity entity) const;
        Entity GetNextSibling(Entity entity) const;
        FTransform GetLocalTransform(Entity entity) const;
        float GetLocalScale(Entity entity) const;
        void SetParent(Entity parent, Entity child);
        void SetLocalPosition(Entity entity, const Vec3& pos);
        void SetLocalRotation(Entity entity, const Quat& rot);
        void SetLocalTransform(Entity entity, const FTransform& transform);
        
        FTransform ComputeLocalTransform(Entity parent, const FTransform& globalTransform) const;
        
        void SetMatrix(Entity entity, const Matrix& mtx);
        Matrix GetPositionAndRotation(Entity entity) const;
        Matrix GetMatrix(Entity entity) const;
        void SetTransformKeepChildren(Entity entity, const FTransform& transform);
        void SetTransform(Entity entity, const FTransform& transform);
        void SetTransform(Entity entity, const Vec3& pos, const Quat& rot, float scale);
        FTransform GetTransform(Entity entity) const;
        void SetRotation(Entity entity, float x, float y, float z, float w);
        void SetRotation(Entity entity, const Quat& rot);
        void SetPosition(Entity entity, float x, float y, float z);
        void SetPosition(Entity entity, const Vec3& pos);
        void SetScale(Entity entity, float scale);
        float GetScale(Entity entity) const;
        const Vec3& GetPosition(Entity entity) const;
        const Quat& GetRotation(Entity entity) const;
       	const char* GetName() const { return Name; }
        void SetName(const char* name) { Name = name; }
        
        DelegateList<void(Entity)>& OnEntityTransformed()
        {
            return OnEntityMovedDelegate;
        }
        DelegateList<void(Entity)>& OnEntityCreated()
        {
            return OnEntityCreatedDelegate;
        }
        DelegateList<void(Entity)>& OnEntityDestroyed()
        {
            return OnEntityDestroyedDelegate;
        }
        DelegateList<void(const ComponentUID&)>& OnComponentDestroyed()
        {
            return OnComponentDestroyedDelegate;
        }
        DelegateList<void(const ComponentUID&)>& OnComponentAdded()
        {
            return OnComponentAddedDelegate;
        }
        
        void SerializeBlob(OutputBlob& Serializer);
        void DeserializeBlob(InputBlob& Serializer);
        
        IScene* GetScene(ComponentType Type) const;
        IScene* GetScene(uint32 Hash) const;
        TArray<IScene*>& GetScenes();
        void AddScene(IScene* Scene);
        
    private:
        
        void TransformEntity(Entity entity, bool update_local);
        void UpdateGlobalTransform(Entity entity);
        
        struct SHierarchy
        {
            Entity entity;
            Entity parent;
            Entity first_child;
            Entity next_sibling;
            
            FTransform local_transform;
        };
        
        struct EntityData
        {
            EntityData() { }
            
            Vec3 position;
            Quat rotation;
            
            int hierarchy;
            int name;
            
            union
            {
                struct
                {
                    float scale;
                    uint64 components;
                };
                struct
                {
                    int prev;
                    int next;
                };
            };
            bool valid;
        };

        
        struct EntityName
        {
            Entity entity;
            char name[ENTITY_NAME_MAX_LENGTH];
        };
        
    private:
        
        IAllocator& Allocator;
        TArray<IScene*> Scenes;
        ComponentTypeEntry ComponentTypeMap[MAX_COMPONENTS_TYPES_COUNT];
        
        TArray<EntityData> Entities;
        TArray<SHierarchy> Hierarchy;
        TArray<EntityName> Names;
        
        DelegateList<void(Entity)> OnEntityMovedDelegate;
        DelegateList<void(Entity)> OnEntityCreatedDelegate;
        DelegateList<void(Entity)> OnEntityDestroyedDelegate;
        
        DelegateList<void(const ComponentUID&)> OnComponentDestroyedDelegate;
        DelegateList<void(const ComponentUID&)> OnComponentAddedDelegate;
        
        int FirstFreeSlotIndex;
        
        StaticString<64> Name;
    };
} // !namespace Neko
