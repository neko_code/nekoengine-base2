//         *               *   *         *
//  Component.cpp

#include "Component.h"

const Neko::ComponentUID Neko::ComponentUID::INVALID(Neko::INVALID_ENTITY, { -1 }, 0, Neko::INVALID_COMPONENT);
