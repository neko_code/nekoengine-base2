//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Component.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Platform/Platform.h"

namespace Neko
{
    class IScene;
    
    struct NEKO_ENGINE_API ComponentUID final
    {
        ComponentUID()
        : entity(INVALID_ENTITY)
        , scene(nullptr)
        , handle(INVALID_COMPONENT)
        {
            type = { -1 };
        }
        
        ComponentUID(Entity _entity, ComponentType _type, IScene* _scene, ComponentHandle _handle)
        : entity(_entity)
        , type(_type)
        , scene(_scene)
        , handle(_handle)
        { }
        
        Entity entity;
        ComponentType type;
        IScene* scene;
        ComponentHandle handle;
        
        static const ComponentUID INVALID;
        
        bool operator == (const ComponentUID& rhs) const
        {
            return type == rhs.type && scene == rhs.scene && handle == rhs.handle;
        }
        
        NEKO_FORCE_INLINE bool IsValid() const
        {
            return handle.IsValid();
        }
    };
} // namespace Neko

