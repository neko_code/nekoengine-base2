//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Platform.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../Platform.h"

#if NEKO_UNIX_FAMILY

#include "../../Utilities/CommandLineParser.h"
#include "../../Utilities/StringUtil.h"
#include "../../Utilities/Timer.h"
#include "../../Data/IAllocator.h"
#include "../../Core/Log.h"
#include <cstdio>
#include <dlfcn.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#include <dirent.h>
#include <copyfile.h>

#include <time.h>
#include <fenv.h>

#if NEKO_APPLE_FAMILY
#   include <mach/mach_time.h>
#endif

#include <signal.h>

namespace Neko
{
    namespace Platform
    {
        struct FileIterator
        {
        };
        
        FileIterator* CreateFileIterator(const char* path, Neko::IAllocator& allocator)
        {
            return (FileIterator*)opendir(path);
        }
        
        
        void DestroyFileIterator(FileIterator* iterator)
        {
            if (iterator)
            {
                closedir((DIR*)iterator);
            }
        }

        bool GetNextFile(FileIterator* iterator, FileInfo* info)
        {
            if (!iterator)
            {
                return false;
            }
            
            auto* dir = (DIR*)iterator;
            auto* dir_ent = readdir(dir);
            if (!dir_ent)
            {
                return false;
            }

            info->bIsDirectory = (dir_ent->d_type == DT_DIR);
            Neko::CopyString(info->Filename, dir_ent->d_name);
            return true;
        }

        void GetSystemTime(int32& Year, int32& Month, int32& DayOfWeek, int32& Day, int32& Hour, int32& Min, int32& Sec, int32& MSec)
        {
            // query for calendar time
            struct timeval Time;
            gettimeofday(&Time, NULL);
            
            // convert it to local time
            struct tm LocalTime;
            localtime_r(&Time.tv_sec, &LocalTime);
            
            // pull out data/time
            Year		= LocalTime.tm_year + 1900;
            Month		= LocalTime.tm_mon + 1;
            DayOfWeek	= LocalTime.tm_wday;
            Day			= LocalTime.tm_mday;
            Hour		= LocalTime.tm_hour;
            Min			= LocalTime.tm_min;
            Sec			= LocalTime.tm_sec;
            MSec		= Time.tv_usec / 1000;
        }
        
        void UtcTime( int32& Year, int32& Month, int32& DayOfWeek, int32& Day, int32& Hour, int32& Min, int32& Sec, int32& MSec )
        {
            // query for calendar time
            struct timeval Time;
            gettimeofday(&Time, NULL);
            
            // convert it to UTC
            struct tm LocalTime;
            gmtime_r(&Time.tv_sec, &LocalTime);
            
            // pull out data/time
            Year		= LocalTime.tm_year + 1900;
            Month		= LocalTime.tm_mon + 1;
            DayOfWeek	= LocalTime.tm_wday;
            Day			= LocalTime.tm_mday;
            Hour		= LocalTime.tm_hour;
            Min			= LocalTime.tm_min;
            Sec			= LocalTime.tm_sec;
            MSec		= Time.tv_usec / 1000;
        }
        
        int64 GetTicks()
        {
            // Since macOS Sierra 'clock_gettime' is supported.
            
            // replaced gettimeofday
            timespec tv;
            clock_gettime(CLOCK_MONOTONIC, &tv);
            int64 Ticks = (uint64)tv.tv_sec * 1000000ULL + tv.tv_nsec / 1000ULL;

            return Ticks;
        }
        
        int64 GetTicksPerSecond()
        {
            return 1000000;
        }

        
        
        uint32 GetCurrentProcessId()
        {
            return getpid();
        }
        
        
        static int32 GArgc = 0;
        static char** GArgv = nullptr;
        
        // Command line
        void SetSystemCommandLine(int32 argc, char* argv[])
        {
            GArgc = argc;
            GArgv = argv;
        }
        
        bool GetSystemCommandLine(char* Output, int32 MaxSize)
        {
            if (MaxSize <= 0)
            {
                return false;
            }
            
            if (GArgc < 2)
            {
                *Output = '\0';
            }
            else
            {
                CopyString(Output, MaxSize, GArgv[1]);
                for (int32 i = 2; i < GArgc; ++i)
                {
                    CatString(Output, MaxSize, " ");
                    CatString(Output, MaxSize, GArgv[i]);
                }
            }
            return true;
        }
        
        
        
        
        struct Process
        {
            explicit Process(Neko::IAllocator& allocator)
            : allocator(allocator)
            {
            }
            
            Neko::IAllocator& allocator;
            pid_t handle;
            int pipes[2];
            int exit_code;
        };
        
        
        bool IsProcessFinished(Process& process)
        {
            if (process.handle == -1) return true;
            int status;
            int success = waitpid(process.handle, &status, WNOHANG);
            if (success == 0) return false;
            process.exit_code = WEXITSTATUS(status);
            process.handle = -1;
            return true;
        }
        
        
        int GetProcessExitCode(Process& process)
        {
            if (process.handle != -1)
            {
                int status;
                int success = waitpid(process.handle, &status, WNOHANG);
                assert(success != -1 && success != 0);
                process.exit_code = WEXITSTATUS(status);
                process.handle = -1;
            }
            return process.exit_code;
        }
        
        
        void DestroyProcess(Process& process)
        {
            if (process.handle != -1)
            {
                kill(process.handle, SIGKILL);
                int status;
                waitpid(process.handle, &status, 0);
            }
            NEKO_DELETE(process.allocator, &process);
        }
        
        
        Process* CreateProcess(const char* cmd, const char* args, Neko::IAllocator& allocator)
        {
            struct stat sb;
            if(stat (cmd, &sb) == 0 && (sb.st_mode & S_IXUSR) == 0)
            {
                Neko::GLogError.log("Engine") << cmd << " is not executable.";
                return nullptr;
            }
            
            
            auto* process = NEKO_NEW(allocator, Process)(allocator);
            int res = pipe(process->pipes);
            assert(res == 0);
            
            process->handle = fork();
            assert(process->handle != -1);
            if (process->handle == 0)
            {
                dup2(process->pipes[1], STDOUT_FILENO);
                dup2(process->pipes[1], STDERR_FILENO);
                close(process->pipes[0]);
                close(process->pipes[1]);
                
                Neko::CCommandLineParser parser(args);
                char* args_array[256];
                char** i = args_array;
                *i = (char*)cmd;
                ++i;
                while (i - args_array < Neko::lengthOf(args_array) - 2 && parser.Next())
                {
                    char tmp[1024];
                    parser.GetCurrent(tmp, Neko::lengthOf(tmp));
                    int len = Neko::StringLength(tmp) + 1;
                    auto* copy = (char*)malloc(len);
                    Neko::CopyString(copy, len, tmp);
                    *i = copy;
                    ++i;
                }
                *i = nullptr;
                
                Text Directory;
                Neko::Platform::GetBundlePath(Directory);
                chdir(Directory);
                execv(cmd, args_array);
                _exit(-1);
            }
            else
            {
                int fl = fcntl(process->pipes[0], F_GETFL, 0);
                fcntl(process->pipes[0], F_SETFL, fl | O_NONBLOCK);
                close(process->pipes[1]);
            }
            return process;
        }
        
        
        int GetProcessOutput(Process& process, char* buf, int buf_size)
        {
            if(buf_size <= 0) return -1;
            buf[0] = 0;
            size_t ret = read(process.pipes[0], buf, buf_size);
            return (int)ret;
        }
        
    }
}
#endif
