//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Sync.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../../Mt/Sync.h"
#include "../../Mt/Atomic.h"
#include "../../Mt/Thread.h"

#if NEKO_UNIX_FAMILY

#include "Prerequisites.h"

namespace Neko
{
    namespace MT
    {
        Semaphore::Semaphore(int32 InitCount, int32 MaxCount)
        {
            Id.count = InitCount;
            
            int res = pthread_mutex_init(&Id.mutex, nullptr);
            assert(res == 0);
            
            res = pthread_cond_init(&Id.cond, nullptr);
            assert(res == 0);
        }
        
        Semaphore::~Semaphore()
        {
            int res = pthread_mutex_destroy(&Id.mutex);
            assert(res == 0);
            
            res = pthread_cond_destroy(&Id.cond);
            assert(res == 0);
        }
        
        void Semaphore::signal()
        {
            int res = pthread_mutex_lock(&Id.mutex);
            assert(res == 0);
            
            res = pthread_cond_signal(&Id.cond);
            assert(res == 0);
            
            ++Id.count;
            
            res = pthread_mutex_unlock(&Id.mutex);
            assert(res == 0);
        }
        
        void Semaphore::Wait()
        {
            int res = pthread_mutex_lock(&Id.mutex);
            assert(res == 0);
            
            while(Id.count <= 0)
            {
                res = pthread_cond_wait(&Id.cond, &Id.mutex);
                assert(res == 0);
            }
            
            --Id.count;
            
            res = pthread_mutex_unlock(&Id.mutex);
            assert(res == 0);
        }
        
        bool Semaphore::poll()
        {
            int res = pthread_mutex_lock(&Id.mutex);
            assert(res == 0);
            
            bool ret = false;
            if (Id.count > 0)
            {
                --Id.count;
                ret = true;
            }
            
            res = pthread_mutex_unlock(&Id.mutex);
            assert(res == 0);
            
            return ret;
        }
        
        
        Event::Event(const bool bManualReset)
        {
            Id.signaled = false;
            Id.manual_reset = bManualReset;
            int res = pthread_mutex_init(&Id.mutex, nullptr);
            assert(res == 0);
            
            res = pthread_cond_init(&Id.cond, nullptr);
            assert(res == 0);
        }
        
        Event::~Event()
        {
            int res = pthread_mutex_destroy(&Id.mutex);
            assert(res == 0);
            
            res = pthread_cond_destroy(&Id.cond);
            assert(res == 0);
        }
        
        void Event::Reset()
        {
            int res = pthread_mutex_lock(&Id.mutex);
            assert(res == 0);
            
            res = pthread_cond_signal(&Id.cond);
            assert(res == 0);
            
            Id.signaled = false;
            res = pthread_mutex_unlock(&Id.mutex);
            assert(res == 0);
        }
        
        void Event::Trigger()
        {
            int res = pthread_mutex_lock(&Id.mutex);
            assert(res == 0);
            
            res = pthread_cond_signal(&Id.cond);
            assert(res == 0);
            
            Id.signaled = true;
            res = pthread_mutex_unlock(&Id.mutex);
            assert(res == 0);
        }
        
        void Event::WaitTimeout(uint32 timeout_ms)
        {
            int res = pthread_mutex_lock(&Id.mutex);
            assert(res == 0);
            
            timespec ts;
            clock_gettime(CLOCK_REALTIME, &ts);
            ts.tv_nsec += (long)timeout_ms * 1000 * 1000;
            if (ts.tv_nsec > 1000000000)
            {
                ts.tv_nsec -= 1000000000;
                ts.tv_sec += 1;
            }
            while (!Id.signaled)
            {
                res = pthread_cond_timedwait(&Id.cond, &Id.mutex, &ts);
                if(res == ETIMEDOUT)
                {
                    break;
                }
//                assert(res == 0);
                if (res != 0)
                printf("error %i: time: %zu, possible string: %s\n", res, ts.tv_nsec, strerror(res));
            }
            
            if (!Id.manual_reset)
            {
                Id.signaled = false;
            }
            
            res = pthread_mutex_unlock(&Id.mutex);
            assert(res == 0);
        }
        
        void Event::Wait()
        {
            int res = pthread_mutex_lock(&Id.mutex);
            assert(res == 0);
            
            while (!Id.signaled)
            {
                res = pthread_cond_wait(&Id.cond, &Id.mutex);
                assert(res == 0);
            }
            
            if (!Id.manual_reset) Id.signaled = false;
            
            res = pthread_mutex_unlock(&Id.mutex);
            assert(res == 0);
        }
        
        bool Event::poll()
        {
            int res = pthread_mutex_lock(&Id.mutex);
            assert(res == 0);
            
            bool ret = false;
            if (Id.signaled)
            {
                Id.signaled = false;
                ret = true;
            }
            
            res = pthread_mutex_unlock(&Id.mutex);
            assert(res == 0);
            
            return ret;
        }
        
        
        SpinMutex::SpinMutex(bool bLocked)
        : Id(0)
        {
            if (bLocked)    // locked on start
            {
                Lock();
            }
        }
        
        SpinMutex::~SpinMutex()
        {
        }
        
        void SpinMutex::Lock()
        {
            for (;;)
            {
                if (Atomics::CompareAndExchange(&Id, 1, 0))
                {
                    Atomics::MemoryBarrierFence();
                    return;
                }
                
                while (Id)
                {
                    YieldCpu();
                }
            }
        }
        
        bool SpinMutex::poll()
        {
            if (Atomics::CompareAndExchange(&Id, 1, 0))
            {
                Atomics::MemoryBarrierFence();
                return true;
            }
            return false;
        }
        
        void SpinMutex::Unlock()
        {
            Atomics::MemoryBarrierFence();
            Id = 0;
        }
    } // namespace MT
} // namespace Neko

#endif
