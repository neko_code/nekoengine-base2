//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Stacc.cpp
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#include "../../Neko.h"

#if NEKO_UNIX_FAMILY

#include "../../../Engine/Utilities/Stack.h"

#include <unistd.h>
#include <sys/mman.h>

#define CORO_MMAP 1
#ifndef CORO_GUARDPAGES
#   define CORO_GUARDPAGES 4
#endif

namespace Neko
{
    namespace Stack
    {
#if CORO_STACKALLOC
        static size_t pagesize(void)
        {
            static size_t pagesize;
            
            if (!pagesize)
            {
                pagesize = sysconf(_SC_PAGESIZE);
            }
            
            return pagesize;
        }
        
#define PAGESIZE pagesize()
        
        int StackAlloc(struct CoroutineStack* stack, unsigned int size)
        {
            // No size set, use default one
            if (!size)
            {
                size = 256 * 1024;
            }
            
            stack->sptr = 0;
            stack->ssze = ((size_t)size * sizeof (void *) + PAGESIZE - 1) / PAGESIZE * PAGESIZE;
            
            size_t ssze = stack->ssze + CORO_GUARDPAGES * PAGESIZE;
            void* base;
            
            // mmap supposedly does allocate-on-write for us
            base = mmap(0, ssze, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
            
            if (base == (void* )-1)
            {
                // some systems don't let us have executable heap
                // we assume they won't need executable stack in that case
                base = mmap(0, ssze, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
                
                if (base == (void* )-1)
                {
                    return 0;
                }
            }
            
#if CORO_GUARDPAGES
            mprotect(base, CORO_GUARDPAGES * PAGESIZE, PROT_NONE);
#endif
            
            base = (void*)((char* )base + CORO_GUARDPAGES * PAGESIZE);
            
            stack->sptr = base;
            return 1;
        }
        
        void StackFree(struct CoroutineStack *stack)
        {
            if (stack->sptr)
            {
                munmap((void*)((char* )stack->sptr - CORO_GUARDPAGES * PAGESIZE)
                       , stack->ssze + CORO_GUARDPAGES * PAGESIZE);
            }
        }
#endif
    } // namespace Stack
} // namespace Neko

#endif
