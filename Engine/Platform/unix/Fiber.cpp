//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Fiber.cpp
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#include "../../Neko.h"

#if NEKO_UNIX_FAMILY

#include "../../Core/Fiber.h"
#include "../../Utilities/Stack.h"
#include "../../Mt/Sync.h"

extern "C"
{
    struct FiberContext
    {
        void** sp; // must be at offset 0
    };
    void __attribute__ ((__noinline__, __regparm__(2)))
    SwitchFiber(FiberContext *prev, FiberContext *next);
}

static Neko::Fiber::FiberProc FiberInitFunc;
static void* FiberInitArg;
static FiberContext* GNewFiber, *GCreateFiber;

static void CoroutineInit(void)
{
    volatile Neko::Fiber::FiberProc Func = FiberInitFunc;
    volatile void* arg = FiberInitArg;
    
    SwitchFiber(GNewFiber, GCreateFiber);
    
    asm(".cfi_undefined rip");
    
    Func((void *)arg);
    
    // the new coro returned. bad. just abort() for now
    abort();
}

//SwitchToFiber
asm (
#   define NUM_SAVED 6
     "\t.text\n"
     "\t.globl _SwitchFiber\n"
     "_SwitchFiber:\n"
     "\tpushq %rbp\n"
     "\tpushq %rbx\n"
     "\tpushq %r12\n"
     "\tpushq %r13\n"
     "\tpushq %r14\n"
     "\tpushq %r15\n"
     "\tmovq %rsp, (%rdi)\n"
     "\tmovq (%rsi), %rsp\n"
     "\tpopq %r15\n"
     "\tpopq %r14\n"
     "\tpopq %r13\n"
     "\tpopq %r12\n"
     "\tpopq %rbx\n"
     "\tpopq %rbp\n"
     "\tpopq %rcx\n"
     "\tjmpq *%rcx\n"
     );


namespace Neko
{
    namespace Fiber
    {
        typedef struct
        {
            FiberContext* Context;
            Stack::CoroutineStack Stack;
        } FiberData;
        
        void InitThread(Handle* Out)
        {
            *Out = Create(1024 * 64, nullptr, nullptr);
        }
        
        Handle Create(int StackSize, FiberProc Proc, void* Parameter)
        {
            FiberData* fiber = (FiberData*)malloc(sizeof(FiberData));
            fiber->Context = (FiberContext*)malloc(sizeof(FiberContext));
       
            // No function set, return valid fiber
            if (!Proc)
            {
                return fiber;
            }
            
            // Allocate stack for this fiber
            Neko::Stack::StackAlloc(&fiber->Stack, StackSize);
            
            FiberContext nctx;
            // Set fiber parameters, will be used later
            FiberInitFunc = Proc;
            FiberInitArg  = Parameter;
            
            FiberContext* ctx = (FiberContext*)fiber->Context;
            GNewFiber = ctx;
            GCreateFiber = &nctx;

            ctx->sp = (void** )(StackSize + (char *)fiber->Stack.sptr);
            *--ctx->sp = (void* )abort; // alignment
            *--ctx->sp = (void* )CoroutineInit;
            
            ctx->sp -= NUM_SAVED;
            memset(ctx->sp, 0, sizeof (*ctx->sp) * NUM_SAVED);
           
            SwitchFiber(GCreateFiber, GNewFiber);
            
            return fiber;
        }
        
        void SwitchTo(Handle* prev, Handle fiber)
        {
            FiberContext* current = ((FiberData*)fiber)->Context;
            FiberContext* previous = ((FiberData*)*prev)->Context;
            SwitchFiber(previous, current);
        }
        
        void Destroy(Handle fiber)
        {
            free(((FiberData*)fiber)->Context);
            free(fiber);
            fiber = nullptr;
        }
        
        void* GetParameter()
        {
            assert(false);
            return nullptr;
        }
        
    } // namespace Fibers
} // namespace Neko

#endif
