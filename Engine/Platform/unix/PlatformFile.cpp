//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  PlatformFile.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../../Neko.h"

#if NEKO_UNIX_FAMILY

#include "../../Utilities/StringUtil.h"
#include "../../FS/PlatformFile.h"
#include "../../Data/IAllocator.h"

#include <unistd.h>

namespace Neko
{
    namespace FS
    {
        CPlatformFile::CPlatformFile()
        {
            pHandle = nullptr;
        }
        
        CPlatformFile::~CPlatformFile()
        {
            assert(!pHandle);
        }
        
        bool CPlatformFile::Open(const char* path, Mode mode)
        {
            pHandle = fopen(path, Mode::WRITE & mode ? "wb+" : "rb+");
            return pHandle;
        }
        
        void CPlatformFile::Flush()
        {
            //assert(pHandle != nullptr);
            if (pHandle != nullptr)
            {
                fflush(pHandle);
            }
        }
        
        void CPlatformFile::Close()
        {
            if (pHandle)
            {
                fclose(pHandle);
                pHandle = nullptr;
            }
        }
        
        size_t CPlatformFile::WriteText(const char* text)
        {
            int len = StringLength(text);
            return Write(text, len);
        }
        
        size_t CPlatformFile::Write(const void* data, size_t size)
        {
            assert(pHandle != nullptr);
            size_t written = fwrite(data, size, 1, pHandle);
            return written;
        }
        
        size_t CPlatformFile::Read(void* data, size_t size)
        {
            assert(pHandle != nullptr);
            size_t read = fread(data, 1, size, pHandle);
            return read;
        }
        
        size_t CPlatformFile::GetSize()
        {
            assert(pHandle != nullptr);
            long pos = ftell(pHandle);
            fseek(pHandle, 0, SEEK_END);
            size_t size = (size_t)ftell(pHandle);
            fseek(pHandle, pos, SEEK_SET);
            return size;
        }
        
        bool CPlatformFile::IsEof() const
        {
            return feof(pHandle);
        }
        
        size_t CPlatformFile::Tell() const
        {
            return ftello64(pHandle);
        }
        
        size_t CPlatformFile::Pos()
        {
            assert(pHandle != nullptr);
            long pos = ftell(pHandle);
            return (size_t)pos;
        }
        
        bool CPlatformFile::Seek(ESeekLocator base, size_t pos)
        {
            assert(nullptr != pHandle);
            int dir = 0;
            switch (base)
            {
                case ESeekLocator::Begin:
                    dir = SEEK_SET;
                    break;
                case ESeekLocator::End:
                    dir = SEEK_END;
                    break;
                case ESeekLocator::Current:
                    dir = SEEK_CUR;
                    break;
            }
            
            return fseeko64(pHandle, pos, dir) == 0;
        }
        
        CPlatformFile& CPlatformFile::operator <<(const char* text)
        {
            Write(text, StringLength(text));
            return *this;
        }
        
        CPlatformFile& CPlatformFile::operator <<(int32 value)
        {
            char buf[20];
            ToCString(value, buf, lengthOf(buf));
            Write(buf, StringLength(buf));
            return *this;
        }
        
        CPlatformFile& CPlatformFile::operator <<(uint32 value)
        {
            char buf[20];
            ToCString(value, buf, lengthOf(buf));
            Write(buf, StringLength(buf));
            return *this;
        }
        
        CPlatformFile& CPlatformFile::operator <<(uint64 value)
        {
            char buf[30];
            ToCString(value, buf, lengthOf(buf));
            Write(buf, StringLength(buf));
            return *this;
        }
        
        CPlatformFile& CPlatformFile::operator <<(float value)
        {
            char buf[30];
            ToCString(value, buf, lengthOf(buf), 7);
            Write(buf, StringLength(buf));
            return *this;
        }
    } // namespace FS
} // namespace Neko
#endif
