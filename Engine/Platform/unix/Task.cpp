//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Task.cpp
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#include "../../Neko.h"

#if NEKO_UNIX_FAMILY

#include "../../Mt/Task.h"
#include "../../Mt/Thread.h"
#include "../../Data/IAllocator.h"
#include "../../Core/Profiler.h"

#include "Prerequisites.h"

namespace Neko
{
    namespace MT
    {
        struct TaskImpl
        {
            TaskImpl(Task* This, IAllocator& allocator)
            : Allocator(allocator)
            , Owner(This)
            , ThreadName("")
            , bForceExit(false)
            , bExited(false)
            , bIsRunning(false)
            , AffinityMask(-1)
            {
                AffinityMask = GetThreadAffinityMask();
            }
            
            bool bForceExit:1;
            bool bExited:1;
            bool bIsRunning:1;
            
            uint64 AffinityMask;
            
            pthread_t Handle;
            
            const char* ThreadName;
            Task* Owner;
            IAllocator& Allocator;
        };
        
        
        static void* ThreadFunction(void* ptr)
        {
            struct TaskImpl* Internal = reinterpret_cast<TaskImpl*>(ptr);
            SetThreadName(GetCurrentThreadID(), Internal->ThreadName);
            Profiler::SetThreadName(Internal->ThreadName);
            
            uint32 ret = 0xffffFFFF;
            if (!Internal->bForceExit)
            {
                ret = Internal->Owner->DoTask();
            }
            Internal->bExited = true;
            Internal->bIsRunning = false;
            
            return nullptr;
        }
        
        
        Task::Task(IAllocator& allocator)
        {
            auto Internal = NEKO_NEW(allocator, TaskImpl)( this, allocator );
            Implementation = Internal;
        }
        
        
        Task::~Task()
        {
            NEKO_DELETE(Implementation->Allocator, Implementation);
        }
        
        
        bool Task::Create(const char* name)
        {
            pthread_attr_t attr;
            
            int Result = pthread_attr_init(&attr);
            assert(Result == 0);
            if (Result != 0)
            {
                return false;
            }
            
            Result = pthread_create(&Implementation->Handle, &attr, ThreadFunction, Implementation);
            assert(Result == 0);
            if (Result != 0)
            {
                return false;
            }
            
            Implementation->ThreadName = name;
            
            return true;
        }
        
        
        
        
        bool Task::Destroy()
        {
            return pthread_join(Implementation->Handle, nullptr) == 0;
        }
        
        
        void Task::SetAffinityMask(uint64 affinity_mask)
        {
            thread_affinity_policy AP;
            AP.affinity_tag = affinity_mask;
            thread_policy_set(pthread_mach_thread_np(pthread_self()), THREAD_AFFINITY_POLICY, (integer_t*)&AP, THREAD_AFFINITY_POLICY_COUNT);
            Implementation->AffinityMask = affinity_mask;
        }
        
        
        uint64 Task::GetAffinityMask() const
        {
            return Implementation->AffinityMask;
        }
        
        
        bool Task::IsRunning() const
        {
            return Implementation->bIsRunning;
        }
        
        
        bool Task::IsFinished() const
        {
            return Implementation->bExited;
        }
        
        
        bool Task::IsForceExit() const
        {
            return Implementation->bForceExit;
        }
        
        
        IAllocator& Task::GetAllocator()
        {
            return Implementation->Allocator;
        }
        
        
        void Task::ForceExit(bool bWaits)
        {
            Implementation->bForceExit = true;
            
            if (bWaits)
            {
                pthread_join(Implementation->Handle, nullptr);
            }
        }
    } // namespace MT
} // namespace Neko

#endif
