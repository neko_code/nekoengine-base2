//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Thread.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../../Neko.h"
#include "../Platform.h"

#if NEKO_UNIX_FAMILY

#include "../../Mt/Thread.h"
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <dlfcn.h>

#include "Prerequisites.h"

namespace Neko
{
    namespace MT
    {
        void Sleep(uint32 milliseconds)
        {
            //	if (milliseconds)
            usleep(useconds_t(milliseconds * 1000));
        }
    
        
        void YieldCpu()
        {
            pthread_yield_np();
        }
        
        
        ThreadID GetCurrentThreadID()
        {
            return ThreadID(pthread_self());
        }
        
        
        uint32 GetThreadAffinityMask()
        {
            if (Neko::Platform::GetNumberOfLogicalCores() > 1)
            {
                thread_affinity_policy AP;
                
                boolean_t GetDefault;
                mach_msg_type_number_t Count;
                kern_return_t krc;
                
                Count = THREAD_AFFINITY_POLICY_COUNT;
                krc = thread_policy_get(pthread_mach_thread_np(pthread_self())
                                        , THREAD_AFFINITY_POLICY
                                        , (integer_t*)&AP
                                        , &Count
                                        , &GetDefault);
                
                return AP.affinity_tag;
            }
            
            return 0;
        }
        
        void SetThreadName(ThreadID threadId, const char* thread_name)
        {
            pthread_setname_np(thread_name);
        }
    } //! namespace MT
} //! namespace Neko

#endif
