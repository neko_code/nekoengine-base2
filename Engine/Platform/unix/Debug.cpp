//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Debug.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../SystemShared.h"

#if NEKO_UNIX_FAMILY

#include "../../Core/Debug.h"
#include "../../Mt/Atomic.h"
#include "../../Utilities/StringUtil.h"
#include "../../Platform/Platform.h"

#include <execinfo.h>
#include <cxxabi.h>
#include <dlfcn.h>

static bool GbIsCrashReportingEnabled = false;

#if NEKO_APPLE_FAMILY

#include <CoreFoundation/CoreFoundation.h>
#include <mach/mach.h>

extern "C"
{
    // CoreSymbolication
    
    struct CSRange
    {
        uint64 Location;
        uint64 Length;
    };
    struct FApplePlatformSymbolCache
    {
        void* Buffer0;
        void* Buffer1;
    };

    typedef FApplePlatformSymbolCache CSTypeRef;
    typedef CSTypeRef CSSymbolicatorRef;
    typedef CSTypeRef CSSourceInfoRef;
    typedef CSTypeRef CSSymbolRef;
    typedef CSTypeRef CSSymbolOwnerRef;
    
    typedef int (^CSSymbolIterator)(CSSymbolRef Symbol);
    typedef int (^CSSourceInfoIterator)(CSSourceInfoRef SourceInfo);
    
#define kCSNow								0x80000000u
    
    typedef bool (*CSEqualPtr)(CSTypeRef Cs1, CSTypeRef Cs2);
    typedef bool (*CSIsNullPtr)(CSTypeRef CS);
    typedef void (*CSReleasePtr)(CSTypeRef CS);
    typedef void (*CSRetainPtr)(CSTypeRef CS);
    
    typedef CSSymbolicatorRef (*CSSymbolicatorCreateWithPidPtr)(pid_t pid);
    typedef CSSymbolicatorRef (*CSSymbolicatorCreateWithPathAndArchitecturePtr)(const char* path, cpu_type_t type);
    
    typedef CSSymbolRef (*CSSymbolicatorGetSymbolWithAddressAtTimePtr)(CSSymbolicatorRef Symbolicator, vm_address_t Address, uint64_t Time);
    typedef CSSourceInfoRef (*CSSymbolicatorGetSourceInfoWithAddressAtTimePtr)(CSSymbolicatorRef Symbolicator, vm_address_t Address, uint64_t Time);
    typedef CSSymbolOwnerRef (*CSSymbolicatorGetSymbolOwnerWithUUIDAtTimePtr)(CSSymbolicatorRef Symbolicator, CFUUIDRef UUID, uint64_t Time);
    typedef CSSymbolOwnerRef (*CSSymbolicatorGetSymbolOwnerPtr)(CSSymbolicatorRef cs);
    typedef int (*CSSymbolicatorForeachSymbolAtTimePtr)(CSSymbolicatorRef Symbolicator, uint64_t Time, CSSymbolIterator It);
    
    typedef const char* (*CSSymbolGetNamePtr)(CSSymbolRef Symbol);
    typedef CSRange (*CSSymbolGetRangePtr)(CSSymbolRef Symbol);
    typedef CSSymbolOwnerRef (*CSSourceInfoGetSymbolOwnerPtr)(CSSourceInfoRef Info);
    typedef CSSymbolOwnerRef (*CSSymbolGetSymbolOwnerPtr)(CSSymbolRef Sym);
    typedef int (*CSSymbolForeachSourceInfoPtr)(CSSymbolRef Sym, CSSourceInfoIterator It);
    
    typedef const char* (*CSSymbolOwnerGetNamePtr)(CSSymbolOwnerRef Owner);
    typedef CFUUIDRef (*CSSymbolOwnerGetUUIDPtr)(CSSymbolOwnerRef Owner);
    typedef vm_address_t (*CSSymbolOwnerGetBaseAddressPtr)(CSSymbolOwnerRef Owner);
    
    typedef int (*CSSourceInfoGetLineNumberPtr)(CSSourceInfoRef Info);
    typedef const char* (*CSSourceInfoGetPathPtr)(CSSourceInfoRef Info);
    typedef CSRange (*CSSourceInfoGetRangePtr)(CSSourceInfoRef Info);
    typedef CSSymbolRef (*CSSourceInfoGetSymbolPtr)(CSSourceInfoRef Info);
}

static bool GAllowApplePlatformSymbolication = false;
static void* GCoreSymbolicationHandle = nullptr;
static CSIsNullPtr CSIsNull = nullptr;
static CSReleasePtr CSRelease = nullptr;
static CSRetainPtr CSRetain = nullptr;
static CSSymbolicatorCreateWithPidPtr CSSymbolicatorCreateWithPid = nullptr;
static CSSymbolicatorGetSymbolWithAddressAtTimePtr CSSymbolicatorGetSymbolWithAddressAtTime = nullptr;
static CSSymbolicatorGetSourceInfoWithAddressAtTimePtr CSSymbolicatorGetSourceInfoWithAddressAtTime = nullptr;
static CSSymbolGetNamePtr CSSymbolGetName = nullptr;
static CSSourceInfoGetSymbolOwnerPtr CSSourceInfoGetSymbolOwner = nullptr;
static CSSymbolOwnerGetNamePtr CSSymbolOwnerGetName = nullptr;
static CSSourceInfoGetLineNumberPtr CSSourceInfoGetLineNumber = nullptr;
static CSSourceInfoGetPathPtr CSSourceInfoGetPath = nullptr;
static CSSourceInfoGetRangePtr CSSourceInfoGetRange = nullptr;
static CSSourceInfoGetSymbolPtr CSSourceInfoGetSymbol = nullptr;

#endif

namespace Neko
{
    namespace Debug
    {
        void DebugOutput(const char* message)
        {
            printf("%s", message);
        }
        
        void DebugBreak()
        {
          //  abort();
        }
        
        int StackTree::Instances = 0;
        
        class StackNode
        {
        public:
            
            StackNode()
            : Instruction(nullptr)
            , FirstChild(nullptr)
            , Parent(nullptr)
            , Next(nullptr)
            {
                
            }
            
            ~StackNode()
            {
                delete Next;
                delete FirstChild;
            }
            
            void* Instruction;
            
            StackNode* Next;
            StackNode* FirstChild;
            StackNode* Parent;
        };
        
        StackTree::StackTree()
        {
            Root = nullptr;
        }
    
        StackTree::~StackTree()
        {
            delete Root;
        }
        
        void StackTree::RefreshModuleList()
        {
            
        }
        
        int StackTree::GetPath(StackNode* node, StackNode** output, int MaxSize)
        {
            int i = 0;
            while (i < MaxSize && node)
            {
                output[i] = node;
                i++;
                node = node->Parent;
            }
            return i;
        }
        
        StackNode* StackTree::GetParent(StackNode* node)
        {
            return node ? node->Parent : nullptr;
        }

        bool SymbolInfoForAddress(uint64 AddressCounter, PlatformSymbolInfo& OutInfo)
        {
            bool bOK = false;
            if (GAllowApplePlatformSymbolication)
            {
                CSSymbolicatorRef Symbolicator = CSSymbolicatorCreateWithPid(Neko::Platform::GetCurrentProcessId());
                if (!CSIsNull(Symbolicator))
                {
                    CSSourceInfoRef Symbol = CSSymbolicatorGetSourceInfoWithAddressAtTime(Symbolicator, (vm_address_t)AddressCounter, kCSNow);
                    
                    if (!CSIsNull(Symbol))
                    {
                        OutInfo.LineNumber = CSSourceInfoGetLineNumber(Symbol);
                        sprintf(OutInfo.Filename, "%s", CSSourceInfoGetPath(Symbol));
                        sprintf(OutInfo.FunctionName, "%s", CSSymbolGetName(CSSourceInfoGetSymbol(Symbol)));
//                        CSRange CodeRange = CSSourceInfoGetRange(Symbol);
//                        out_SymbolInfo.SymbolDisplacement = (AddressCounter - CodeRange.Location);
                        
                        CSSymbolOwnerRef Owner = CSSourceInfoGetSymbolOwner(Symbol);
                        if (!CSIsNull(Owner))
                        {
                            char const* DylibName = CSSymbolOwnerGetName(Owner);
                            Neko::CopyString(OutInfo.ModuleName, DylibName);
                            
                            bOK = OutInfo.LineNumber != 0;
                        }
                    }
                    
                    CSRelease(Symbolicator);
                }
            }
            
            return bOK;
        }
        
        bool StackTree::GetFunction(StackNode* node, char* out, int MaxSize, int* line)
        {
            PlatformSymbolInfo Info;
            bool bSuccess = SymbolInfoForAddress((uint64)node->Instruction, Info);
            
            if (bSuccess)
            {
                Neko::CopyString(out, MaxSize, Info.FunctionName);
                *line = Info.LineNumber;
            }
            else
            {
                *line = -1;
            }
            
            return bSuccess;
        }
        
        void StackTree::PrintCallstack(StackNode* node)
        {
            char Buffer[1024];
            int Index = 0;
            while (node)
            {
                /*
                 
                 Typically this is how the backtrace looks like:
                 
                 0   <app/lib-name>     0x0000000100000e98 _Z5tracev + 72
                 1   <app/lib-name>     0x00000001000015c1 _ZNK7functorclEv + 17
                 2   <app/lib-name>     0x0000000100000f71 _Z3fn0v + 17
                 3   <app/lib-name>     0x0000000100000f89 _Z3fn1v + 9
                 4   <app/lib-name>     0x0000000100000f99 _Z3fn2v + 9
                 5   <app/lib-name>     0x0000000100000fa9 _Z3fn3v + 9
                 6   <app/lib-name>     0x0000000100000fb9 _Z3fn4v + 9
                 7   <app/lib-name>     0x0000000100000fc9 _Z3fn5v + 9
                 8   <app/lib-name>     0x0000000100000fd9 _Z3fn6v + 9
                 9   <app/lib-name>     0x0000000100001018 main + 56
                 10  libdyld.dylib      0x00007fff91b647e1 start + 0
                 
                 */
                
                Dl_info info;
                if (dladdr(node->Instruction, &info))
                {
                    char* demangled = NULL;
                    int status;
                    demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
                    snprintf(Buffer, sizeof(Buffer), "%-3d %*p %s + %zd\n", Index, (int)(2 + sizeof(void*) * 2), node->Instruction, (status == 0) ? demangled : info.dli_sname, (char *)node->Instruction - (char *)info.dli_saddr);
                    free(demangled);
                }
                else
                {
                    snprintf(Buffer, sizeof(Buffer), "%-3d %*p\n", Index, (int)(2 + sizeof(void*) * 2), node->Instruction);
                }
                
                DebugOutput(Buffer);
                
                ++Index;
                
                node = node->Parent;
            }
        }
    
        StackNode* StackTree::InsertChildren(StackNode* root_node, void** instruction, void** stack)
        {
            StackNode* node = root_node;
            while (instruction >= stack)
            {
                StackNode* newNode = new StackNode();
                node->FirstChild = newNode;
                newNode->Parent = node;
                newNode->Next = nullptr;
                newNode->FirstChild = nullptr;
                newNode->Instruction = *instruction;
                node = newNode;
                --instruction;
            }
            return node;
        }
        
        StackNode* StackTree::Record()
        {
            //  record stack trace upto 128 frames
            void* callstack[128] = {};
            // collect stack frames
            int frames = backtrace(callstack, 128);
            
            void** ptr = callstack + frames - 1;
            
            if (!Root)
            {
                Root = new StackNode();
                Root->Instruction = *ptr;
                Root->FirstChild = nullptr;
                Root->Next = nullptr;
                Root->Parent = nullptr;
                --ptr;
                return InsertChildren(Root, ptr, callstack);
            }

            
            StackNode* node = Root;
            while (ptr >= callstack)
            {
                while (node->Instruction != *ptr && node->Next)
                {
                    node = node->Next;
                }
                if (node->Instruction != *ptr)
                {
                    node->Next = new StackNode;
                    node->Next->Parent = node->Parent;
                    node->Next->Instruction = *ptr;
                    node->Next->Next = nullptr;
                    node->Next->FirstChild = nullptr;
                    --ptr;
                    return InsertChildren(node->Next, ptr, callstack);
                }
                else if (node->FirstChild)
                {
                    --ptr;
                    node = node->FirstChild;
                }
                else if (ptr >= callstack)
                {
                    --ptr;
                    return InsertChildren(node, ptr, callstack);
                }
                else
                {
                    return node;
                }
            }
            
            return node;
        }
        
        static const uint32 UNINITIALIZED_MEMORY_PATTERN = 0xCD;
        static const uint32 FREED_MEMORY_PATTERN = 0xDD;
        static const uint32 ALLOCATION_GUARD = 0xFDFDFDFD;
        
        Allocator::Allocator(IAllocator& source)
        : Source(source)
        , Root(nullptr)
        , Mutex(false)
        , TotalSize(0)
        , IsFillEnabled(true)
        , AreGuardsEnabled(true)
        {
            Sentinels[0].next = &Sentinels[1];
            Sentinels[0].previous = nullptr;
            Sentinels[0].stack_leaf = nullptr;
            Sentinels[0].size = 0;
            Sentinels[0].align = 0;
            
            Sentinels[1].next = nullptr;
            Sentinels[1].previous = &Sentinels[0];
            Sentinels[1].stack_leaf = nullptr;
            Sentinels[1].size = 0;
            Sentinels[1].align = 0;
            
            Root = &Sentinels[1];
        }
        
        
        Allocator::~Allocator()
        {
            AllocationInfo* last_sentinel = &Sentinels[1];
            if (Root != last_sentinel)
            {
                DebugOutput("Memory leaks detected!\n");
                AllocationInfo* info = Root;
                while (info != last_sentinel)
                {
                    char tmp[2048];
                    sprintf(tmp, "\nAllocation size : %zu, memory %p\n", info->size, (void*)(info + sizeof(info)));
                    DebugOutput(tmp);
                    StackTree.PrintCallstack(info->stack_leaf);
                    info = info->next;
                }
                assert(false);
            }
        }
        
        
        void Allocator::Lock()
        {
            Mutex.Lock();
        }
        
        
        void Allocator::Unlock()
        {
            Mutex.Unlock();
        }
        
        
        void Allocator::CheckGuards()
        {
            if (AreGuardsEnabled) return;
            
            auto* info = Root;
            while (info)
            {
                auto user_ptr = GetUserPtrFromAllocationInfo(info);
                void* system_ptr = GetSystemFromUser(user_ptr);
                assert(*(uint32*)system_ptr == ALLOCATION_GUARD);
                assert(*(uint32*)((uint8*)user_ptr + info->size) == ALLOCATION_GUARD);
                
                info = info->next;
            }
        }
        
        
        size_t Allocator::GetAllocationOffset()
        {
            return sizeof(AllocationInfo) + (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) : 0);
        }
        
        
        size_t Allocator::GetNeededMemory(size_t size)
        {
            return size + sizeof(AllocationInfo) + (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) << 1 : 0);
        }
        
        
        size_t Allocator::GetNeededMemory(size_t size, size_t align)
        {
            return size + sizeof(AllocationInfo) + (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) << 1 : 0) +
            align;
        }
        
        
        Allocator::AllocationInfo* Allocator::GetAllocationInfoFromSystem(void* system_ptr)
        {
            return (AllocationInfo*)(AreGuardsEnabled ? (uint8*)system_ptr + sizeof(ALLOCATION_GUARD)
                                     : system_ptr);
        }
        
        
        void* Allocator::GetUserPtrFromAllocationInfo(AllocationInfo* info)
        {
            return ((uint8*)info + sizeof(AllocationInfo));
        }
        
        
        Allocator::AllocationInfo* Allocator::GetAllocationInfoFromUser(void* user_ptr)
        {
            return (AllocationInfo*)((uint8*)user_ptr - sizeof(AllocationInfo));
        }
        
        
        uint8* Allocator::GetUserFromSystem(void* system_ptr, size_t align)
        {
            size_t diff = (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) : 0) + sizeof(AllocationInfo);
            
            if (align) diff += (align - diff % align) % align;
            return (uint8*)system_ptr + diff;
        }
        
        
        uint8* Allocator::GetSystemFromUser(void* user_ptr)
        {
            AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
            size_t diff = (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) : 0) + sizeof(AllocationInfo);
            if (info->align) diff += (info->align - diff % info->align) % info->align;
            return (uint8*)user_ptr - diff;
        }
        
        
        void* Allocator::Reallocate(void* user_ptr, size_t size)
        {
#ifndef _DEBUG
            return Source.Reallocate(user_ptr, size);
#else
            if (user_ptr == nullptr) return Allocate(size);
            if (size == 0) return nullptr;
            
            void* new_data = Allocate(size);
            if (!new_data) return nullptr;
            
            AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
            CopyMemory(new_data, user_ptr, info->size < size ? info->size : size);
            
            Deallocate(user_ptr);
            
            return new_data;
#endif
        }
        
        
        void* Allocator::AllocateAligned(size_t size, size_t align)
        {
#ifndef _DEBUG
            return Source.AllocateAligned(size, align);
#else
            void* system_ptr;
            AllocationInfo* info;
            uint8* user_ptr;
            
            size_t system_size = GetNeededMemory(size, align);
            {
                MT::SpinLock lock(Mutex);
                system_ptr = Source.AllocateAligned(system_size, align);
                user_ptr = GetUserFromSystem(system_ptr, align);
                info = new (NewPlaceholder(), GetAllocationInfoFromUser(user_ptr)) AllocationInfo();
                
                info->previous = Root->previous;
                Root->previous->next = info;
                
                info->next = Root;
                Root->previous = info;
                
                Root = info;
                
                TotalSize += size;
            } // because of the SpinLock
            
            info->align = uint16(align);
            info->stack_leaf = StackTree.Record();
            info->size = size;
            if (IsFillEnabled)
            {
                memset(user_ptr, UNINITIALIZED_MEMORY_PATTERN, size);
            }
            
            if (AreGuardsEnabled)
            {
                *(uint32*)system_ptr = ALLOCATION_GUARD;
                *(uint32*)((uint8*)system_ptr + system_size - sizeof(ALLOCATION_GUARD)) = ALLOCATION_GUARD;
            }
            
            return user_ptr;
#endif
        }
        
        
        void Allocator::DeallocateAligned(void* user_ptr)
        {
#ifndef _DEBUG
            Source.DeallocateAligned(user_ptr);
#else
            if (user_ptr)
            {
                AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
                void* system_ptr = GetSystemFromUser(user_ptr);
                if (IsFillEnabled)
                {
                    memset(user_ptr, FREED_MEMORY_PATTERN, info->size);
                }
                
                if (AreGuardsEnabled)
                {
                    assert(*(uint32*)system_ptr == ALLOCATION_GUARD);
                    size_t system_size = GetNeededMemory(info->size, info->align);
                    assert(*(uint32*)((uint8*)system_ptr + system_size - sizeof(ALLOCATION_GUARD)) == ALLOCATION_GUARD);
                }
                
                {
                    MT::SpinLock lock(Mutex);
                    if (info == Root)
                    {
                        Root = info->next;
                    }
                    info->previous->next = info->next;
                    info->next->previous = info->previous;
                    
                    TotalSize -= info->size;
                } // because of the SpinLock
                
                info->~AllocationInfo();
                
                Source.DeallocateAligned((void*)system_ptr);
            }
#endif
        }
        
        
        void* Allocator::ReallocateAligned(void* user_ptr, size_t size, size_t align)
        {
#ifndef _DEBUG
            return Source.ReallocateAligned(user_ptr, size, align);
#else
            if (user_ptr == nullptr) return AllocateAligned(size, align);
            if (size == 0) return nullptr;
            
            void* new_data = AllocateAligned(size, align);
            if (!new_data) return nullptr;
            
            AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
            CopyMemory(new_data, user_ptr, info->size < size ? info->size : size);
            
            DeallocateAligned(user_ptr);
            
            return new_data;
#endif
        }
        
        
        void* Allocator::Allocate(size_t size)
        {
#ifndef _DEBUG
            return Source.Allocate(size);
#else
            void* system_ptr;
            AllocationInfo* info;
            size_t system_size = GetNeededMemory(size);
            {
                MT::SpinLock lock(Mutex);
                system_ptr = Source.Allocate(system_size);
                info = new (NewPlaceholder(), GetAllocationInfoFromSystem(system_ptr)) AllocationInfo();
                
                info->previous = Root->previous;
                Root->previous->next = info;
                
                info->next = Root;
                Root->previous = info;
                
                Root = info;
                
                TotalSize += size;
            } // because of the SpinLock
            
            void* user_ptr = GetUserFromSystem(system_ptr, 0);
            info->stack_leaf = StackTree.Record();
            info->size = size;
            info->align = 0;
            if (IsFillEnabled)
            {
                memset(user_ptr, UNINITIALIZED_MEMORY_PATTERN, size);
            }
            
            if (AreGuardsEnabled)
            {
                *(uint32*)system_ptr = ALLOCATION_GUARD;
                *(uint32*)((uint8*)system_ptr + system_size - sizeof(ALLOCATION_GUARD)) = ALLOCATION_GUARD;
            }
            
            return user_ptr;
#endif
        }
        
        void Allocator::Deallocate(void* user_ptr)
        {
#ifndef _DEBUG
            Source.Deallocate(user_ptr);
#else
            if (user_ptr)
            {
                AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
                void* system_ptr = GetSystemFromUser(user_ptr);
                if (IsFillEnabled)
                {
                    memset(user_ptr, FREED_MEMORY_PATTERN, info->size);
                }
                
                if (AreGuardsEnabled)
                {
                    assert(*(uint32*)system_ptr == ALLOCATION_GUARD);
                    size_t system_size = GetNeededMemory(info->size);
                    assert(*(uint32*)((uint8*)system_ptr + system_size - sizeof(ALLOCATION_GUARD)) == ALLOCATION_GUARD);
                }
                
                {
                    MT::SpinLock lock(Mutex);
                    if (info == Root)
                    {
                        Root = info->next;
                    }
                    info->previous->next = info->next;
                    info->next->previous = info->previous;
                    
                    TotalSize -= info->size;
                } // because of the SpinLock
                
                info->~AllocationInfo();
                
                Source.Deallocate((void*)system_ptr);
            }
#endif
        }
        
        
    } // namespace Debug
    
    void EnablePlatformSymbolication(bool bEnable)
    {
#if NEKO_APPLE_FAMILY
        GAllowApplePlatformSymbolication = bEnable;
        if (bEnable && !GCoreSymbolicationHandle)
        {
            GCoreSymbolicationHandle = Neko::Platform::LoadSystemLibrary("/System/Library/PrivateFrameworks/CoreSymbolication.framework/Versions/Current/CoreSymbolication");
            if (GCoreSymbolicationHandle)
            {
                CSIsNull = (CSIsNullPtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSIsNull");
                GAllowApplePlatformSymbolication &= (CSIsNull != nullptr);
                
                CSRelease = (CSReleasePtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSRelease");
                GAllowApplePlatformSymbolication &= (CSRelease != nullptr);
                
                CSRetain = (CSRetainPtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSRetain");
                GAllowApplePlatformSymbolication &= (CSRetain != nullptr);
                
                CSSymbolicatorCreateWithPid = (CSSymbolicatorCreateWithPidPtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSymbolicatorCreateWithPid");
                GAllowApplePlatformSymbolication &= (CSSymbolicatorCreateWithPid != nullptr);
                
                CSSymbolicatorGetSymbolWithAddressAtTime = (CSSymbolicatorGetSymbolWithAddressAtTimePtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSymbolicatorGetSymbolWithAddressAtTime");
                GAllowApplePlatformSymbolication &= (CSSymbolicatorGetSymbolWithAddressAtTime != nullptr);
                
                CSSymbolicatorGetSourceInfoWithAddressAtTime = (CSSymbolicatorGetSourceInfoWithAddressAtTimePtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSymbolicatorGetSourceInfoWithAddressAtTime");
                GAllowApplePlatformSymbolication &= (CSSymbolicatorGetSourceInfoWithAddressAtTime != nullptr);
                
                CSSourceInfoGetLineNumber = (CSSourceInfoGetLineNumberPtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSourceInfoGetLineNumber");
                GAllowApplePlatformSymbolication &= (CSSourceInfoGetLineNumber != nullptr);
                
                CSSourceInfoGetPath = (CSSourceInfoGetPathPtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSourceInfoGetPath");
                GAllowApplePlatformSymbolication &= (CSSourceInfoGetPath != nullptr);
                
                CSSymbolGetName = (CSSymbolGetNamePtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSymbolGetName");
                GAllowApplePlatformSymbolication &= (CSSymbolGetName != nullptr);
                
                CSSourceInfoGetRange = (CSSourceInfoGetRangePtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSourceInfoGetRange");
                GAllowApplePlatformSymbolication &= (CSSourceInfoGetRange != nullptr);
                
                CSSourceInfoGetSymbol = (CSSourceInfoGetSymbolPtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSourceInfoGetSymbol");
                GAllowApplePlatformSymbolication &= (CSSourceInfoGetSymbol != nullptr);
                
                CSSourceInfoGetSymbolOwner = (CSSourceInfoGetSymbolOwnerPtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSourceInfoGetSymbolOwner");
                GAllowApplePlatformSymbolication &= (CSSourceInfoGetSymbolOwner != nullptr);
                
                CSSymbolOwnerGetName = (CSSymbolOwnerGetNamePtr)Platform::GetLibrarySymbol(GCoreSymbolicationHandle, "CSSymbolOwnerGetName");
                GAllowApplePlatformSymbolication &= (CSSymbolOwnerGetName != nullptr);
                
            }
            else
            {
                GAllowApplePlatformSymbolication = false;
            }
        }
        else if (!bEnable && GCoreSymbolicationHandle)
        {
            CSIsNull = nullptr;
            CSRelease = nullptr;
            CSRetain = nullptr;
            CSSymbolicatorCreateWithPid = nullptr;
            CSSymbolicatorGetSymbolWithAddressAtTime = nullptr;
            CSSymbolicatorGetSourceInfoWithAddressAtTime = nullptr;
            CSSymbolGetName = nullptr;
            CSSourceInfoGetSymbolOwner = nullptr;
            CSSymbolOwnerGetName = nullptr;
            CSSourceInfoGetLineNumber = nullptr;
            CSSourceInfoGetPath = nullptr;
            CSSourceInfoGetRange = nullptr;
            CSSourceInfoGetSymbol = nullptr;
            Platform::UnloadLibrary(GCoreSymbolicationHandle);
            GCoreSymbolicationHandle = nullptr;
        }
#endif
    }
    
    void EnableCrashReporting(bool enable)
    {
        GbIsCrashReportingEnabled = false;
    }
    
    
    void InstallUnhandledExceptionHandler()
    {
        
    }
} // namespace Neko

#endif
