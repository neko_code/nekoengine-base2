//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  IGenericWindow.h
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "../Math/Vec.h"
#include "../Neko.h"
#include "../Utilities/Text.h"
#include "../Core/InputSystem.h"

namespace Neko
{
    /** System window modes. */
    enum ENativeWindowMode
    {
        //! Typical window with borders.
        Windowed = 0,
        //! Fullscreen.
        Fullscreen,
        //! Borderless window with size of the screen.
        WindowedFullscreen,
        
        TotalNativeWindowModes
    };
    
    /** Window type. */
    enum class EWindowType
    {
        Normal = 0,
        
        Menu,
        
        Notification,
        
        ToolTip,
        
        Main
    };
    
    /** Window creation flags. */
    struct NEKO_ENGINE_API FWindowDesc
    {
        //! Desired width.
        uint32 SizeW;
        //! Desired height.
        uint32 SizeH;
        
        //! Desired X origin on the screen.
        uint32 OriginX;
        //! Desired Y origin on the screen.
        uint32 OriginY;
        
        //! Runtime flags (e.g. determines if window is currently focused and so on).
        uint32 RuntimeFlags;
        
        bool IsTopmostWindow;
        
        bool IsRegularWindow;
        
        //! True if window should have close button (else disables the window titlebar icon).
        bool HasCloseButton;
        //! If true then window will support minimisation (else disables the window titlebar icon).
        bool SupportsMinimisation;
        //! If true then window will support maximisation (else disables the window titlebar icon).
        bool SupportsMaximisation;
        
        //! Window sizing frame (if we won't let the user to resize a window then it should be set to false).
        bool HasSizingFrame;
        
        //! Window borders.
        bool HasBorder;
        
        //! True if window should be modal.
        bool IsModal;
        
        //! True if window should appear in the task bar (or Dock on macOS).
        bool AppearsInTaskbar;
        
        //! True if window should accept any type of input (mouse, keyboard).
        bool AcceptsInput;
        
        //! Keep aspect ratio value (window will be resized proportionally).
        bool ShouldPreserveAspectRatio;
        
        //! Should we take user's focus to the window which has just shown?
        bool ActivateWhenFirstShown;
        
        //! Opacity of the window.
        float Opacity;
        //! Window corner radius.
		float CornerRadius;
        
        //! Window type.
        EWindowType Type;
        
        //! Window title.
        Text Title;
    };
    
    /// Interface for OS native window.
    class NEKO_ENGINE_API IGenericWindow
    {
    public:
        
        IGenericWindow();
        
        ~IGenericWindow();
        
        /**
         * Resizes and moves window if needed.
         * @param X New value for desired X origin.
         * @param Y New value for desired Y origin.
         * @param Width New value for desired window width.
         * @param Height New value for desired window height.
         */
        virtual void                ReshapeWindow(int32 X, int32 Y, int32 Width, int32 Height);
        
        /** Moves window to the point. */
        virtual void                MoveWindowTo(int32 X, int32 Y);
        
        /** Brings this window to front and activates it if needed. */
        virtual void                BringToFront(bool bForce = false);
        
        /** Destroys the window. */
        virtual void                Destroy();
        
        /** Maximizes the window. */
        virtual void                Maximize();
        /** Restores the window (e.g. collapse from taskbar). */
        virtual void                Restore();
        
        /** Shows the window. */
        virtual void                Show();
        
        /** Hides the window. */
        virtual void                Hide();
        
        /** Window size. */
        virtual Int2                GetSize();
        /** Window position. */
        virtual Vec2                GetLocation();
        
        /** Sets the window mode. @see 'ENativeWindowMode'. */
        virtual void                SetWindowMode(ENativeWindowMode Mode);
        /** Returns the current window mode. */
        virtual ENativeWindowMode               GetWindowMode() const;
        
        /** Returns pointer to OS native window. */
        virtual void*               GetNativeHandle() const;
        
        /** @return TRUE if the native window is maximized, false otherwise */
        virtual bool                IsMaximized() const;
        
        /** @return TRUE if the native window is minimized, false otherwise */
        virtual bool                IsMinimized() const;
        
        /** @return TRUE if the native window is visible, false otherwise */
        virtual bool                IsVisible() const;

        /** @return TRUE if native window exists underneath the coordinates */
        virtual bool                IsPointInWindow(int32 X, int32 Y) const;
        
        /** Returns TRUE if Definition.RegularWindow = TRUE. */
        virtual bool                IsRegularWindow() const;
        
        /** @return Ratio of pixels in this window. */
        virtual float               GetDPIScaleFactor() const;
        
        /** Sets window titlebar text. */
        virtual void                SetTitle(const char* Text);
        
        virtual FWindowDesc& GetDefinition()
        {
            return Definition;
        }
        
        FWindowDesc Definition;
    };
}
