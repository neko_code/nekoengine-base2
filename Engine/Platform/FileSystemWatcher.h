//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  FileSystemWatcher.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Containers/Delegate.h"
#include "../Utilities/Text.h"
#include "../Core/Path.h"

/** Simple object containing information about file/directory changes. */
struct FFileChangeData
{
	enum EFileChangeAction
	{
		FCA_Unknown,
		FCA_Added,
		FCA_Modified,
		FCA_Removed
	};
	
	FFileChangeData(const char* InFilename, EFileChangeAction InAction)
	: Filename(InFilename)
	, Action(InAction)
	{ }
	
	Neko::Text Filename;
	EFileChangeAction Action;
	
	/** Options for a watcher */
	enum WatchOptions : uint32
	{
		IncludeDirectoryChanges     = (1 << 0),
        
		IgnoreChangesInSubtree      = (1 << 1),
	};
};

/// Interface for platform file/directory system watcher.
class IFileSystemWatcher
{
public:
	
    virtual ~IFileSystemWatcher()
    { }
	
	/**
	 * Creates new directory watcher.
     *
	 * @param Path	Directory path to watch.
	 * @param Flags	See FFileChangeData::WatchOptions.
	 */
    static NEKO_ENGINE_API IFileSystemWatcher* Create(const char* Path, uint32 Flags, Neko::IAllocator& allocator);
	/** Destroys directory watcher. */
    static NEKO_ENGINE_API void Destroy(IFileSystemWatcher* Watcher);
	/** Binds function call and calls its delegate everytime directory event comes. */
    virtual Neko::TDelegate<void (const FFileChangeData& Data)>& GetCallback() = 0;
};

