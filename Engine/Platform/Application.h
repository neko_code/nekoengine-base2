//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Application.h
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "SystemShared.h"

#include "../Containers/Queue.h"
#include "../Core/Application.h"
#include "../Core/Joystick.h"
#include "../Mt/Thread.h"

// @todo Use custom allocator for allocating events

/** Says where we are. */
extern NEKO_ENGINE_API bool GIsEditor;

/** True if we're about to exit the application. */
extern NEKO_ENGINE_API bool GIsRequestingExit;

/** If true then logging will be turned off. */
extern NEKO_ENGINE_API bool GIsSilent;

/** True if engine is active. */
extern NEKO_ENGINE_API bool GIsRunning;

/** Main/game thread id. */
extern NEKO_ENGINE_API Neko::ThreadID GMainThreadId;

/** Audio thread id. */
extern NEKO_ENGINE_API Neko::ThreadID GAudioThreadId;

/** Game thread id. */
extern NEKO_ENGINE_API Neko::ThreadID GGameThreadId;

/** Renderer thread id. */
extern NEKO_ENGINE_API Neko::ThreadID GRenderThreadId;

namespace Neko
{
	/** @return True if called from the main (system) thread. */
	NEKO_FORCE_INLINE NEKO_ENGINE_API bool IsInMainThread()
	{
		const ThreadID CurrentThreadId = Neko::MT::GetCurrentThreadID();
		return (CurrentThreadId == GMainThreadId);
	}
	
    /** @return True if called from the audio thread. */
    NEKO_FORCE_INLINE NEKO_ENGINE_API bool IsInAudioThread()
    {
        const ThreadID CurrentThreadId = Neko::MT::GetCurrentThreadID();
        return (CurrentThreadId == GAudioThreadId);
    }
    
    /** @return True if called from the game thread. */
    NEKO_FORCE_INLINE NEKO_ENGINE_API bool IsInGameThread()
    {
        const ThreadID CurrentThreadId = Neko::MT::GetCurrentThreadID();
        return (CurrentThreadId == GGameThreadId);
    }
    
    /** @return True if called from the renderer thread. */
    NEKO_FORCE_INLINE NEKO_ENGINE_API bool IsInRenderingThread()
    {
        const ThreadID CurrentThreadId = Neko::MT::GetCurrentThreadID();
        return (CurrentThreadId == GRenderThreadId);
    }

    static int32 GetScaledMouseDelta(float scale, int32 value, float* accum)
    {
        if (scale != 1.0f)
        {
            *accum += scale * value;
            if (*accum >= 0.0f)
            {
                value = Neko::Math::FloorToInt(*accum);
            }
            else
            {
                value = Neko::Math::CeilToInt(*accum);
            }
            *accum -= value;
        }
        return value;
    }
    
    
    /// Our main application. Handles system interface for the engine.
    class NEKO_ENGINE_API MainApplication final : public IGenericMessageHandler
    {
    public:
        
        virtual ~MainApplication();
        
    public:
        
        /** Initializes the application and also initialises the platform interface. */
        static void Create();
        
        NEKO_FORCE_INLINE static bool IsInitialised()
        {
            return (CurrentApplication != nullptr);
        }
        
        /**
         * Returns the current instance of the application. The application should have been initialized before this method is called.
         *
         * @return  Reference to the application
         */
        NEKO_FORCE_INLINE static MainApplication& Get()
        {
            assert( IsInitialised() );
            return *CurrentApplication;
        }
        
        
        static void Shutdown();
 
        /** Ticks this application */
        void Tick();
        
        /** Pumps OS messages. */
        void PumpMessages();
 
        
        NEKO_FORCE_INLINE IPlatformApplication* GetPlatformApplication()
        {
            return PlatformApplication;
        }
        
        
        NEKO_FORCE_INLINE double GetCurrentTime() const
        {
            return CurrentTime;
        }
        
        /** True whether we need to switch mouse to high precision mode. */
        void SetRelativeMouseMode(bool bEnabled);
        
    public:
        
        /** Creates system window. */
        static IGenericWindow* MakeWindow(const FWindowDesc& InDesc);
        /** Destroys the window. */
        static void DestroyWindow(IGenericWindow* Window);

        /** Returns currently active (focused) window. */
        static IGenericWindow* GetActiveWindow();
        
        /** True whether we need to show cursor. */
        static void SetCursorShow(bool bShown);
        /** Wraps mouse to the given 2D point. */
        static void WarpMouse(int32 X, int32 Y);
        /** Sets cursor type. */
		static void SetCursorType(ICursor::EMouseCursorType Type);
        /** Returns current cursor type. */
		static ICursor::EMouseCursorType GetCursorType();
		
    private:
        
        // Hidden default constructor.
        MainApplication(IAllocator& allocator);
        
        static IPlatformApplication* Create(IPlatformApplication* InPlatformApplication);
        
        static IPlatformApplication* PlatformApplication;
        static MainApplication* CurrentApplication;
        
        // Platform events queue.
        typedef SpScUnboundedQueueLf<Event> TSystemEventQueue;
        TSystemEventQueue  Queue;
        
        //! Current time in seconds.
        double CurrentTime;
        
        IAllocator& Allocator;
        
    public:
        
        // Mouse, synthesizing relative moves
        
        //! True whether mouse should wrap or not to the center of the viewport in relative mode.
        bool bMouseRelativeWrap;
        //! True if mouse is currently in high precision mode.
        bool bMouseRelative;
        //! Current mouse relative X.
        int32 RelativeX;
        //! Current mouse relative Y.
        int32 RelativeY;
        //! Accumulated mouse scale.
        Vec2 ScaleAccum;
        
        Vec2 MousePosition;
        
    public:
        
        // Events
        
        const NEKO_FORCE_INLINE Event* Poll()
        {
            return Queue.Pop();
        }
        
        const NEKO_FORCE_INLINE Event* Poll(IGenericWindow* handle)
        {
            // Check if handle is valid
            if (handle != nullptr)
            {
                Event* ev = Queue.Peek();
                if (ev == nullptr || ev->Handle != handle)
                {
                    return nullptr;
                }
            }
            
            return Poll();
        }
        
        void NEKO_FORCE_INLINE Release(const Event* event)
        {
            Event* ev = (Event*)event;
            NEKO_DELETE(Allocator, ev) ;
        }
        
    public:
        
        // IGenericMessageHandler
        
        virtual void OnCharEvent(IGenericWindow* Handle, const uint32 Char) override;
        virtual void OnExitEvent() override;
        virtual void OnKeyEvent(IGenericWindow* Handle, EScancode Key, uint32 Modifiers, bool _down) override;
        virtual void OnMouseEvent(IGenericWindow* Handle, int32 X, int32 Y, bool bHighPrecision) override;
        virtual void OnMouseEvent(IGenericWindow* Handle, int32 X, int32 Y, EMouseButtons Button, bool bDown) override;
		virtual void OnBeginGesture() override;
		virtual void OnEndGesture() override;
        virtual void OnTouchGesture(IGenericWindow* Handle, ETouchGestureType Type, Vec2 Delta, bool bInverted) override;
        
        virtual void OnControllerConnectionChange(const GamepadHandle DeviceIndex, const GamepadHardwareInfo Info, bool Connected) override;
        virtual void OnControllerButton(const GamepadHandle DeviceIndex, uint8 Button, uint8 Value) override;
        virtual void OnControllerHat(const GamepadHandle DeviceIndex, uint8 Hat, uint8 Value) override;
        virtual void OnControllerAxis(const GamepadHandle DeviceIndex, uint8 Axis, int16 Value) override;
        virtual void OnSizeEvent(IGenericWindow* Handle, uint32 Width, uint32 Height) override;
        virtual void OnMovedWindow(IGenericWindow* Handle, int32 X, int32 Y) override;
        virtual void OnWindowEvent(IGenericWindow* Handle, void* Native = nullptr) override;
        virtual void OnSuspendEvent(IGenericWindow* Handle, ESuspendState State) override;
        
        virtual void OnWindowFocus(IGenericWindow* Handle, bool bGained) override;
        virtual void OnWindowClose(IGenericWindow* Handle) override;
        
    };
    
    
    inline void MainApplication::OnCharEvent(IGenericWindow* Handle, const uint32 Char)
    {
        CharEvent* ev = NEKO_NEW(Allocator, CharEvent)(Handle);
        ev->CurrentInputChar = Char;
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnExitEvent()
    {
        Event* ev = new Event(Event::Exit);
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnKeyEvent(IGenericWindow* Handle, EScancode Key, uint32 Modifiers, bool bDown)
    {
        KeyEvent* ev = NEKO_NEW(Allocator, KeyEvent)(Handle);
        ev->Key = Key;
        ev->Modifiers = Modifiers;
        ev->bDown = bDown;
        Queue.Push(ev);
    }

    inline void MainApplication::OnMouseEvent(IGenericWindow* Handle, int32 X, int32 Y, bool bHighPrecision)
    {
        // Preprocess mouse now
        
        // Take raw mouse values
        Vec2 MousePos(X, Y);
        
        static int32 LastX = 0;
        static int32 LastY = 0;
        
        // Center the mouse cursor only if in the relative mode and active
        if (bMouseRelativeWrap)
        {
            Int2 windowSize;
            
            IGenericWindow* window = GetActiveWindow();
            windowSize = window->GetSize();
            
            windowSize.x /= 2;
            windowSize.y /= 2;
            if (X == windowSize.x && Y == windowSize.y)
            {
                LastX = windowSize.x;
                LastY = windowSize.y;
                return;
            }
            WarpMouse(windowSize.x, windowSize.y);
        }
        
        const float fRelativeSpeedScale = 1.0f;
        const float fNormalSpeedScale = 1.0f;
        
        if (bHighPrecision)
        {
            if (bMouseRelative)
            {
                X = GetScaledMouseDelta(1.0f, X, &ScaleAccum.x);
                Y = GetScaledMouseDelta(fRelativeSpeedScale, Y, &ScaleAccum.y);
            }
            else
            {
                X = GetScaledMouseDelta(1.0f, X, &ScaleAccum.x);
                Y = GetScaledMouseDelta(fNormalSpeedScale, Y, &ScaleAccum.y);
            }
            RelativeX = X;
            RelativeY = Y;
            X = (LastX + RelativeX);
            Y = (LastY + RelativeY);
        }
        else
        {
            RelativeX = X - LastX;
            RelativeY = Y - LastY;
        }
        
        // Drop events that don't change state
        if (!RelativeX && !RelativeY)
        {
            return;
        }
        
        // Update internal mouse coordinates
        if (!bMouseRelative)
        {
            MousePos.x = float(X);
            MousePos.y = float(Y);
        }
        else
        {
            MousePos.x += RelativeX;
            MousePos.y += RelativeY;
        }
        
        // Create the event
        MouseEvent* ev = NEKO_NEW(Allocator, MouseEvent)(Handle);
        ev->MouseX = MousePos.x;
        ev->MouseY = MousePos.y;
        ev->RelativeX = RelativeX;
        ev->RelativeY = RelativeY;
        
        ev->Button = MouseButton_None;
        ev->Flags = 0 | MouseEvent::MOUSE_FLAG_MOVED | (bHighPrecision ? MouseEvent::MOUSED_FLAG_PRECISION : 0);

        Queue.Push(ev);
        
        if (bHighPrecision)
        {
            LastX = MousePos.x;
            LastY = MousePos.y;
        }
        else
        {
            // Use unclamped values if we're getting events outside the window
            LastX = X;
            LastY = Y;
        }
        
        MousePosition.x = MousePos.x;
        MousePosition.y = MousePos.y;
    }
    
    inline void MainApplication::OnMouseEvent(IGenericWindow* Handle, int32 X, int32 Y, EMouseButtons Button, bool bDown)
    {
        MouseEvent* ev = NEKO_NEW(Allocator, MouseEvent)(Handle);
        ev->MouseX = X;
        ev->MouseY = Y;
        
        ev->RelativeX = RelativeX;
        ev->RelativeY = RelativeY;
        
        ev->Button = Button;
        ev->Flags = 0 | (bDown ? MouseEvent::MOUSE_FLAG_DOWN : 0);
        
        Queue.Push(ev);
    }
    
	inline void MainApplication::OnBeginGesture()
	{
		TouchEvent* ev = NEKO_NEW(Allocator, TouchEvent)(nullptr);
		ev->TouchType = ETouchType::Began;
		
		Queue.Push(ev);
	}
	
	inline void MainApplication::OnEndGesture()
	{
		TouchEvent* ev = NEKO_NEW(Allocator, TouchEvent)(nullptr);
		ev->TouchType = ETouchType::Ended;
		
		Queue.Push(ev);
	}
	
    inline void MainApplication::OnTouchGesture(IGenericWindow *Handle, ETouchGestureType Type, Vec2 Delta, bool bInverted)
    {
        TouchEvent* ev = NEKO_NEW(Allocator, TouchEvent)(Handle);
        ev->Delta = Delta;
        ev->bInverted = bInverted;
        ev->Type = Type;
		ev->TouchType = ETouchType::Moved;
        
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnControllerConnectionChange(const GamepadHandle DeviceIndex, const GamepadHardwareInfo Info, bool Connected)
    {
//        assert(DeviceIndex != -1);
        
        GamepadEvent* ev = NEKO_NEW(Allocator, GamepadEvent)(nullptr);
        ev->Connected = Connected;
        ev->DeviceIndex = DeviceIndex;
        ev->Info = Info;
        
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnControllerButton(const GamepadHandle DeviceIndex, uint8 Button, uint8 Value)
    {
        GamepadButtonEvent* ev = NEKO_NEW(Allocator, GamepadButtonEvent)(nullptr);
        ev->Button = Button;
        ev->Value = Value;
        ev->Instance = DeviceIndex;
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnControllerHat(const GamepadHandle DeviceIndex, uint8 Hat, uint8 Value)
    {
        
    }
    
    inline void MainApplication::OnControllerAxis(const GamepadHandle DeviceIndex, uint8 Axis, int16 Value)
    {
        GamepadAxisEvent* ev = NEKO_NEW(Allocator, GamepadAxisEvent)(nullptr);
        ev->Axis = Axis;
        ev->Value = Value;
        ev->Instance = DeviceIndex;
        Queue.Push(ev);
    }

    inline void MainApplication::OnSizeEvent(IGenericWindow* Handle, uint32 Width, uint32 Height)
    {
        SizeEvent* ev = NEKO_NEW(Allocator, SizeEvent)(Handle);
        ev->X = Width;
        ev->Y = Height;
        ev->Flags = 0 | SizeEvent::Sized;
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnMovedWindow(IGenericWindow* Handle, int32 X, int32 Y)
    {
        SizeEvent* ev = NEKO_NEW(Allocator, SizeEvent)(Handle);
        ev->X = X;
        ev->Y = Y;
        ev->Flags = 0 | SizeEvent::Moved;
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnWindowEvent(IGenericWindow* Handle, void* Native)
    {
        WindowEvent* ev = NEKO_NEW(Allocator, WindowEvent)(Handle);
        ev->Flags = 0;
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnWindowFocus(IGenericWindow *Handle, bool bGained)
    {
        WindowEvent* ev = NEKO_NEW(Allocator, WindowEvent)(Handle);
        ev->Flags = 0 | (bGained ? WindowEvent::BecameActive : WindowEvent::BecameResigned);
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnWindowClose(IGenericWindow* Handle)
    {
        WindowEvent* ev = NEKO_NEW(Allocator, WindowEvent)(Handle);
        ev->Flags = 0 | WindowEvent::Closed;
        ev->Window = Handle;
        Queue.Push(ev);
    }
    
    inline void MainApplication::OnSuspendEvent(IGenericWindow* Handle, ESuspendState State)
    {
        SuspendEvent* ev = NEKO_NEW(Allocator, SuspendEvent)(Handle);
        ev->State = State;
        Queue.Push(ev);
    }
}
