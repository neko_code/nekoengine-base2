//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Application.cpp
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#include "SystemShared.h"

#include "Application.h"
#include "Platform.h"

#include "../Core/Profiler.h"

bool GIsEditor = true;
bool GIsRequestingExit = false;
bool GIsSilent = false;
bool GIsRunning = false;

/** Main/game thread id. */
Neko::ThreadID GMainThreadId(0);
/** Audio thread id. */
Neko::ThreadID GAudioThreadId(0);
/** Game thread id. */
Neko::ThreadID GGameThreadId(0);
/** Renderer thread id. */
Neko::ThreadID GRenderThreadId(0);

namespace Neko
{
    IPlatformApplication* MainApplication::PlatformApplication;
    MainApplication* MainApplication::CurrentApplication = nullptr;
  
    
    MainApplication::MainApplication(IAllocator& allocator)
    : bMouseRelativeWrap(false)
    , bMouseRelative(false)
    , Allocator(allocator)
    {
    }
    
    MainApplication::~MainApplication()
    {
		
    }
    
    void MainApplication::Create()
    {
        Create(IPlatformApplication::Create());
    }
    
    IPlatformApplication* MainApplication::Create(IPlatformApplication* InPlatformApplication)
    {
        auto& allocator = GetDefaultAllocator();
        // Initialise this instance..
        CurrentApplication = NEKO_NEW(allocator, MainApplication)(allocator);
        PlatformApplication = InPlatformApplication;
        // All system events will be sent there
        PlatformApplication->SetMessageHandler(CurrentApplication);
        
        return PlatformApplication;
    }
    
    void MainApplication::Shutdown()
    {
        // Release all remaining events..
        for (const Event* ev = CurrentApplication->Poll(); ev != nullptr; ev = CurrentApplication->Poll())
        {
            CurrentApplication->Release(ev);
		}
		// Delete platform application base.
		delete PlatformApplication;
		
		NEKO_DELETE(GetDefaultAllocator(), CurrentApplication);
    }
    
    IGenericWindow* MainApplication::MakeWindow(const FWindowDesc& InDescriptor)
    {
        return PlatformApplication->MakeWindow(InDescriptor);
    }
    
    void MainApplication::DestroyWindow(IGenericWindow* Window)
    {
        PlatformApplication->DestroyWindow(Window);
    }
    
    IGenericWindow* MainApplication::GetActiveWindow()
    {
        return PlatformApplication->GetActiveWindow();
    }
    
    void MainApplication::PumpMessages()
    {
        PROFILE_FUNCTION();
        
        PlatformApplication->GetEvents();
    }
    
    void MainApplication::Tick()
    {
        PROFILE_FUNCTION();
        
        CurrentTime = PlatformTime::Seconds();
        
        PlatformApplication->ProcessEvents();
    }

    void MainApplication::SetRelativeMouseMode(bool bEnabled)
    {
        if (bEnabled == bMouseRelative)
        {
            return;
        }
        
        if (bEnabled)
        {
            IGenericWindow* Window = GetActiveWindow();
            Int2 WindowLocation = Window->GetSize();
            WarpMouse(WindowLocation.x / 2, WindowLocation.y / 2);
        }
        
        // Set the relative mode
        if (!bEnabled && bMouseRelativeWrap)
        {
            bMouseRelativeWrap = false;
        }
        else if (bEnabled)
        {
            bMouseRelativeWrap = true;
        }
        else if (!PlatformApplication->SetRelativeMouseMode(bEnabled))
        {
            if (bEnabled)
            {
                // Fall back to warp mode if native relative mode failed
                bMouseRelativeWrap = true;
            }
        }
        
        bMouseRelative = bEnabled;
        ScaleAccum.x = 0.0f;
        ScaleAccum.y = 0.0f;
        
        // Put the cursor back where application expects it
        if (!bEnabled)
        {
            WarpMouse(MousePosition.x, MousePosition.y);
        }
    }
    
    void MainApplication::SetCursorShow(bool bEnabled)
    {
        PlatformApplication->SetCursorShow(bEnabled);
    }
    
    void MainApplication::WarpMouse(int32 X, int32 Y)
    {
        PlatformApplication->WarpMouse(X, Y);
    }
    
	void MainApplication::SetCursorType(ICursor::EMouseCursorType Type)
	{
		PlatformApplication->SetCursorType(Type);
	}
	
	ICursor::EMouseCursorType MainApplication::GetCursorType()
	{
		return PlatformApplication->GetCursorType();
	}
}
