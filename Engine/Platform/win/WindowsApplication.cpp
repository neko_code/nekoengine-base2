//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/


#include "../SystemShared.h"

#if NEKO_WINDOWS

#include "WindowsApplication.h"

#include "../../../App/Launch.h"

#include "../../Platform/Application.h"
#include "../../Platform/Platform.h"
#include "../../Mt/Task.h"
#include "../../Hashmap.h"
#include "../../EventHandler.h"
#include "../../Engine.h"
#include "../../Log.h"

#include "../../../Editor/StudioApp.h"

#include <windowsx.h>

// This might not be defined by Windows when maintaining backwards-compatibility to pre-Vista builds
#ifndef WM_MOUSEHWHEEL
#define WM_MOUSEHWHEEL                  0x020E
#endif

#ifndef WM_DPICHANGED
#define WM_DPICHANGED                   0x02E0
#endif

namespace Neko
{
	extern "C"
	{
		HINSTANCE hInstance = NULL;
	}

	const Int2 CWindowsApplication::MinimizedWindowPosition(-32000, -32000);

    CWindowsApplication* WindowsApplication = nullptr;

	CWindowsApplication* CWindowsApplication::CreateApplication()
	{
		HICON AppIconHandle = NULL;// LoadIcon(hInstance, MAKEINTRESOURCE(GetAppIcon()));
		if (AppIconHandle == NULL)
		{
			AppIconHandle = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
		}

		return CWindowsApplication::Create(hInstance, AppIconHandle);
	}
    
	CWindowsApplication* CWindowsApplication::Create(const HINSTANCE HInstance, const HICON IconHandle)
    {
		WindowsApplication = new CWindowsApplication(HInstance, IconHandle);
        return WindowsApplication;
    }
    


	IPlatformApplication* IPlatformApplication::Create()
	{
		return CWindowsApplication::CreateApplication();
	}


    void CWindowsApplication::Destroy(Neko::CWindowsApplication* Application)
    {
        delete Application;
    }
    
	CWindowsApplication::CWindowsApplication(const HINSTANCE HInstance, const HICON IconHandle)
    : Allocator()
		, InstanceHandle(HInstance)
		, bUsingHighPrecisionMouseInput(false)
		, bIsMouseAttached(false)
		, bForceActivateByMouse(false)
		//, XInput(XInputInterface::Create(MessageHandler))
		//, bHasLoadedInputPlugins(false)
		, bAllowedToDeferMessageProcessing(true)
		, bInModalSizeLoop(false)
		, CurrentWindow(nullptr)
		, MessageHandlers(Allocator)
		, Windows(Allocator)
		, DeferredMessages(Allocator)
    {
		memset(&ModifierKeyState, 0, sizeof(ModifierKeyState));

		// Disable the process from being showing "ghosted" not responding messages during slow tasks
		// This is a hack.  A more permanent solution is to make our slow tasks not block the editor for so long
		// that message pumping doesn't occur (which causes these messages).
		::DisableProcessWindowsGhosting();

		//FWindowsPlatformMisc::SetHighDPIMode();

		// Register the Win32 class for Slate windows and assign the application instance and icon
		const bool bClassRegistered = RegisterClass(InstanceHandle, IconHandle);

		// Initialize OLE for Drag and Drop support.
		//OleInitialize(NULL);

		// Save the current sticky/toggle/filter key settings so they can be restored them later
		// If there are .ini settings, use them instead of the current system settings.
		// NOTE: Whenever we exit and restore these settings gracefully, the .ini settings are removed.
		ZeroMemory(&StartupStickyKeys, sizeof(StartupStickyKeys));
		ZeroMemory(&StartupToggleKeys, sizeof(StartupToggleKeys));
		ZeroMemory(&StartupFilterKeys, sizeof(StartupFilterKeys));

		StartupStickyKeys.cbSize = sizeof(StartupStickyKeys);
		StartupToggleKeys.cbSize = sizeof(StartupToggleKeys);
		StartupFilterKeys.cbSize = sizeof(StartupFilterKeys);

		SystemParametersInfo(SPI_GETSTICKYKEYS, sizeof(STICKYKEYS), &StartupStickyKeys, 0);
		SystemParametersInfo(SPI_GETTOGGLEKEYS, sizeof(TOGGLEKEYS), &StartupToggleKeys, 0);
		SystemParametersInfo(SPI_GETFILTERKEYS, sizeof(FILTERKEYS), &StartupFilterKeys, 0);

		bool bSKHotkey = (StartupStickyKeys.dwFlags & SKF_HOTKEYACTIVE) ? true : false;
		bool bTKHotkey = (StartupToggleKeys.dwFlags & TKF_HOTKEYACTIVE) ? true : false;
		bool bFKHotkey = (StartupFilterKeys.dwFlags & FKF_HOTKEYACTIVE) ? true : false;
		bool bSKConfirmation = (StartupStickyKeys.dwFlags & SKF_CONFIRMHOTKEY) ? true : false;
		bool bTKConfirmation = (StartupToggleKeys.dwFlags & TKF_CONFIRMHOTKEY) ? true : false;
		bool bFKConfirmation = (StartupFilterKeys.dwFlags & FKF_CONFIRMHOTKEY) ? true : false;

		StartupStickyKeys.dwFlags = bSKHotkey ? (StartupStickyKeys.dwFlags | SKF_HOTKEYACTIVE) : (StartupStickyKeys.dwFlags & ~SKF_HOTKEYACTIVE);
		StartupToggleKeys.dwFlags = bTKHotkey ? (StartupToggleKeys.dwFlags | TKF_HOTKEYACTIVE) : (StartupToggleKeys.dwFlags & ~TKF_HOTKEYACTIVE);
		StartupFilterKeys.dwFlags = bFKHotkey ? (StartupFilterKeys.dwFlags | FKF_HOTKEYACTIVE) : (StartupFilterKeys.dwFlags & ~FKF_HOTKEYACTIVE);
		StartupStickyKeys.dwFlags = bSKConfirmation ? (StartupStickyKeys.dwFlags | SKF_CONFIRMHOTKEY) : (StartupStickyKeys.dwFlags & ~SKF_CONFIRMHOTKEY);
		StartupToggleKeys.dwFlags = bTKConfirmation ? (StartupToggleKeys.dwFlags | TKF_CONFIRMHOTKEY) : (StartupToggleKeys.dwFlags & ~TKF_CONFIRMHOTKEY);
		StartupFilterKeys.dwFlags = bFKConfirmation ? (StartupFilterKeys.dwFlags | FKF_CONFIRMHOTKEY) : (StartupFilterKeys.dwFlags & ~FKF_CONFIRMHOTKEY);

		// Disable accessibility shortcuts
		AllowAccessibilityShortcutKeys(false);

		QueryConnectedMice();
    }

	void CWindowsApplication::AllowAccessibilityShortcutKeys(const bool bAllowKeys)
	{
		if (bAllowKeys)
		{
			// Restore StickyKeys/etc to original state and enable Windows key      
			SystemParametersInfo(SPI_SETSTICKYKEYS, sizeof(STICKYKEYS), &StartupStickyKeys, 0);
			SystemParametersInfo(SPI_SETTOGGLEKEYS, sizeof(TOGGLEKEYS), &StartupToggleKeys, 0);
			SystemParametersInfo(SPI_SETFILTERKEYS, sizeof(FILTERKEYS), &StartupFilterKeys, 0);
		}
		else
		{
			// Disable StickyKeys/etc shortcuts but if the accessibility feature is on, 
			// then leave the settings alone as its probably being usefully used

			STICKYKEYS skOff = StartupStickyKeys;
			if ((skOff.dwFlags & SKF_STICKYKEYSON) == 0)
			{
				// Disable the hotkey and the confirmation
				skOff.dwFlags &= ~SKF_HOTKEYACTIVE;
				skOff.dwFlags &= ~SKF_CONFIRMHOTKEY;

				SystemParametersInfo(SPI_SETSTICKYKEYS, sizeof(STICKYKEYS), &skOff, 0);
			}

			TOGGLEKEYS tkOff = StartupToggleKeys;
			if ((tkOff.dwFlags & TKF_TOGGLEKEYSON) == 0)
			{
				// Disable the hotkey and the confirmation
				tkOff.dwFlags &= ~TKF_HOTKEYACTIVE;
				tkOff.dwFlags &= ~TKF_CONFIRMHOTKEY;

				SystemParametersInfo(SPI_SETTOGGLEKEYS, sizeof(TOGGLEKEYS), &tkOff, 0);
			}

			FILTERKEYS fkOff = StartupFilterKeys;
			if ((fkOff.dwFlags & FKF_FILTERKEYSON) == 0)
			{
				// Disable the hotkey and the confirmation
				fkOff.dwFlags &= ~FKF_HOTKEYACTIVE;
				fkOff.dwFlags &= ~FKF_CONFIRMHOTKEY;

				SystemParametersInfo(SPI_SETFILTERKEYS, sizeof(FILTERKEYS), &fkOff, 0);
			}
		}
	}

	bool CWindowsApplication::RegisterClass(const HINSTANCE HInstance, const HICON HIcon)
	{
		WNDCLASS wc;
		memset(&wc, 0, sizeof(wc));
		wc.style = CS_DBLCLKS; // We want to receive double clicks
		wc.lpfnWndProc = AppWndProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = HInstance;
		wc.hIcon = HIcon;
		wc.hCursor = NULL;	// We manage the cursor ourselves
		wc.hbrBackground = NULL; // Transparent
		wc.lpszMenuName = NULL;
		wc.lpszClassName = CWindowsWindow::AppWindowClass;

		if (!::RegisterClass(&wc))
		{
			//ShowLastError();

			// @todo Slate: Error message should be localized!
			//FSlowHeartBeatScope SuspendHeartBeat;
			MessageBox(NULL, TEXT("Window Registration Failed!"), TEXT("Error!"), MB_ICONEXCLAMATION | MB_OK);

			return false;
		}

		return true;
	}

	/* Windows scancode to SDL scancode mapping table */
	/* derived from Microsoft scan code document, http://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/scancode.doc */

	/* *INDENT-OFF* */
	static const EScancode windows_scancode_table[] =
	{
		/*	0						1							2							3							4						5							6							7 */
		/*	8						9							A							B							C						D							E							F */
		SC_UNKNOWN,		SC_ESCAPE,		SC_1,				SC_2,				SC_3,			SC_4,				SC_5,				SC_6,			/* 0 */
		SC_7,				SC_8,				SC_9,				SC_0,				SC_MINUS,		SC_EQUALS,		SC_BACKSPACE,		SC_TAB,		/* 0 */

		SC_Q,				SC_W,				SC_E,				SC_R,				SC_T,			SC_Y,				SC_U,				SC_I,			/* 1 */
		SC_O,				SC_P,				SC_LEFTBRACKET,	SC_RIGHTBRACKET,	SC_RETURN,	SC_LCTRL,			SC_A,				SC_S,			/* 1 */

		SC_D,				SC_F,				SC_G,				SC_H,				SC_J,			SC_K,				SC_L,				SC_SEMICOLON,	/* 2 */
		SC_APOSTROPHE,	SC_GRAVE,			SC_LSHIFT,		SC_BACKSLASH,		SC_Z,			SC_X,				SC_C,				SC_V,			/* 2 */

		SC_B,				SC_N,				SC_M,				SC_COMMA,			SC_PERIOD,	SC_SLASH,			SC_RSHIFT,		SC_PRINTSCREEN,/* 3 */
		SC_LALT,			SC_SPACE,			SC_CAPSLOCK,		SC_F1,			SC_F2,		SC_F3,			SC_F4,			SC_F5,		/* 3 */

		SC_F6,			SC_F7,			SC_F8,			SC_F9,			SC_F10,		SC_NUMLOCKCLEAR,	SC_SCROLLLOCK,	SC_HOME,		/* 4 */
		SC_UP,			SC_PAGEUP,		SC_KP_MINUS,		SC_LEFT,			SC_KP_5,		SC_RIGHT,			SC_KP_PLUS,		SC_END,		/* 4 */

		SC_DOWN,			SC_PAGEDOWN,		SC_INSERT,		SC_DELETE,		SC_UNKNOWN,	SC_UNKNOWN,		SC_NONUSBACKSLASH,SC_F11,		/* 5 */
		SC_F12,			SC_PAUSE,			SC_UNKNOWN,		SC_LGUI,			SC_RGUI,		SC_APPLICATION,	SC_UNKNOWN,		SC_UNKNOWN,	/* 5 */

		SC_UNKNOWN,		SC_UNKNOWN,		SC_UNKNOWN,		SC_UNKNOWN,		SC_F13,		SC_F14,			SC_F15,			SC_F16,		/* 6 */
		SC_F17,			SC_F18,			SC_F19,			SC_UNKNOWN,		SC_UNKNOWN,	SC_UNKNOWN,		SC_UNKNOWN,		SC_UNKNOWN,	/* 6 */

		SC_INTERNATIONAL2,		SC_UNKNOWN,		SC_UNKNOWN,		SC_INTERNATIONAL1,		SC_UNKNOWN,	SC_UNKNOWN,		SC_UNKNOWN,		SC_UNKNOWN,	/* 7 */
		SC_UNKNOWN,		SC_INTERNATIONAL4,		SC_UNKNOWN,		SC_INTERNATIONAL5,		SC_UNKNOWN,	SC_INTERNATIONAL3,		SC_UNKNOWN,		SC_UNKNOWN	/* 7 */
	};
    
	CWindowsApplication::~CWindowsApplication()
    {
		// Restore accessibility shortcuts and remove the saved state from the .ini
		AllowAccessibilityShortcutKeys(true);

		assert(Windows.GetSize() == 0);
        Windows.clear(); // @note Created windows are deallocated manually
        

		//::CoUninitialize();
		//OleUninitialize();

		WindowsApplication = nullptr;
    }

	void CWindowsApplication::UpdateAllModifierKeyStates()
	{
		ModifierKeyState[EModifierKey::LeftShift] = (::GetAsyncKeyState(VK_LSHIFT) & 0x8000) != 0;
		ModifierKeyState[EModifierKey::RightShift] = (::GetAsyncKeyState(VK_RSHIFT) & 0x8000) != 0;
		ModifierKeyState[EModifierKey::LeftControl] = (::GetAsyncKeyState(VK_LCONTROL) & 0x8000) != 0;
		ModifierKeyState[EModifierKey::RightControl] = (::GetAsyncKeyState(VK_RCONTROL) & 0x8000) != 0;
		ModifierKeyState[EModifierKey::LeftAlt] = (::GetAsyncKeyState(VK_LMENU) & 0x8000) != 0;
		ModifierKeyState[EModifierKey::RightAlt] = (::GetAsyncKeyState(VK_RMENU) & 0x8000) != 0;
		ModifierKeyState[EModifierKey::CapsLock] = (::GetKeyState(VK_CAPITAL) & 0x0001) != 0;
	}

	static CWindowsWindow* FindWindowByHWND(TArray<CWindowsWindow*>& WindowsToSearch, HWND HandleToFind)
	{
		for (int32 WindowIndex = 0; WindowIndex < WindowsToSearch.GetSize(); ++WindowIndex)
		{
			CWindowsWindow* Window = WindowsToSearch[WindowIndex];
			if (Window->GetNativeHandle() == HandleToFind)
			{
				return Window;
			}
		}

		return nullptr;
	}


	int32 CWindowsApplication::ProcessMessage(HWND hwnd, uint32 msg, WPARAM wParam, LPARAM lParam)
	{
		CWindowsWindow* CurrentNativeEventWindowPtr = FindWindowByHWND(Windows, hwnd);

		if (Windows.GetSize() && CurrentNativeEventWindowPtr)
		{
			CWindowsWindow* CurrentNativeEventWindow = CurrentNativeEventWindowPtr;

			static const AssociativeArray<uint32, Text> WindowsMessageStrings = []()
			{
				static AssociativeArray<uint32, Text> Result(Allocator);
#define ADD_WINDOWS_MESSAGE_STRING(WMCode) Result.Insert(WMCode, TEXT(#WMCode))
				ADD_WINDOWS_MESSAGE_STRING(WM_INPUTLANGCHANGEREQUEST);
				ADD_WINDOWS_MESSAGE_STRING(WM_INPUTLANGCHANGE);
				ADD_WINDOWS_MESSAGE_STRING(WM_IME_SETCONTEXT);
				ADD_WINDOWS_MESSAGE_STRING(WM_IME_NOTIFY);
				ADD_WINDOWS_MESSAGE_STRING(WM_IME_REQUEST);
				ADD_WINDOWS_MESSAGE_STRING(WM_IME_STARTCOMPOSITION);
				ADD_WINDOWS_MESSAGE_STRING(WM_IME_COMPOSITION);
				ADD_WINDOWS_MESSAGE_STRING(WM_IME_ENDCOMPOSITION);
				ADD_WINDOWS_MESSAGE_STRING(WM_IME_CHAR);
#undef ADD_WINDOWS_MESSAGE_STRING
				return Result;
			}();

			static const AssociativeArray<uint32, Text> IMNStrings = []()
			{
				AssociativeArray<uint32, Text> Result(Allocator);
#define ADD_IMN_STRING(IMNCode) Result.Insert(IMNCode, TEXT(#IMNCode))
				ADD_IMN_STRING(IMN_CLOSESTATUSWINDOW);
				ADD_IMN_STRING(IMN_OPENSTATUSWINDOW);
				ADD_IMN_STRING(IMN_CHANGECANDIDATE);
				ADD_IMN_STRING(IMN_CLOSECANDIDATE);
				ADD_IMN_STRING(IMN_OPENCANDIDATE);
				ADD_IMN_STRING(IMN_SETCONVERSIONMODE);
				ADD_IMN_STRING(IMN_SETSENTENCEMODE);
				ADD_IMN_STRING(IMN_SETOPENSTATUS);
				ADD_IMN_STRING(IMN_SETCANDIDATEPOS);
				ADD_IMN_STRING(IMN_SETCOMPOSITIONFONT);
				ADD_IMN_STRING(IMN_SETCOMPOSITIONWINDOW);
				ADD_IMN_STRING(IMN_SETSTATUSWINDOWPOS);
				ADD_IMN_STRING(IMN_GUIDELINE);
				ADD_IMN_STRING(IMN_PRIVATE);
#undef ADD_IMN_STRING
				return Result;
			}();

			static const AssociativeArray<uint32, Text> IMRStrings = []()
			{
				AssociativeArray<uint32, Text> Result(Allocator);
#define ADD_IMR_STRING(IMRCode) Result.Insert(IMRCode, TEXT(#IMRCode))
				ADD_IMR_STRING(IMR_CANDIDATEWINDOW);
				ADD_IMR_STRING(IMR_COMPOSITIONFONT);
				ADD_IMR_STRING(IMR_COMPOSITIONWINDOW);
				ADD_IMR_STRING(IMR_CONFIRMRECONVERTSTRING);
				ADD_IMR_STRING(IMR_DOCUMENTFEED);
				ADD_IMR_STRING(IMR_QUERYCHARPOSITION);
				ADD_IMR_STRING(IMR_RECONVERTSTRING);
#undef ADD_IMR_STRING
				return Result;
			}();

			bool bMessageExternallyHandled = false;
			int32 ExternalMessageHandlerResult = 0;

			// give others a chance to handle messages
			for (IWindowsMessageHandler* Handler : MessageHandlers)
			{
				int32 HandlerResult = 0;
				if (Handler->ProcessMessage(hwnd, msg, wParam, lParam, HandlerResult))
				{
					if (!bMessageExternallyHandled)
					{
						bMessageExternallyHandled = true;
						ExternalMessageHandlerResult = HandlerResult;
					}
				}
			}

			switch (msg)
			{
			case WM_INPUTLANGCHANGEREQUEST:
			case WM_INPUTLANGCHANGE:
			case WM_IME_SETCONTEXT:
			case WM_IME_STARTCOMPOSITION:
			case WM_IME_COMPOSITION:
			case WM_IME_ENDCOMPOSITION:
			case WM_IME_CHAR:
				//printf("%s", WindowsMessageStrings[msg].c_str());
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				return 0;
			case WM_IME_NOTIFY:
				//printf(TEXT("WM_IME_NOTIFY - %s"), IMNStrings.Find(wParam) ? (IMNStrings[wParam].c_str()) : nullptr);
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				return 0;
			case WM_IME_REQUEST:
				//printf(TEXT("WM_IME_REQUEST - %s"), IMRStrings.Find(wParam) ? (IMRStrings[wParam].c_str()) : nullptr);
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				return 0;
				// Character
			case WM_CHAR:
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				return 0;
			case WM_SYSCHAR:
			{
				if ((HIWORD(lParam) & 0x2000) != 0 && wParam == VK_SPACE)
				{
					// Do not handle Alt+Space so that it passes through and opens the window system menu
					break;
				}
				else
				{
					return 0;
				}
			}

			break;

			case WM_SYSKEYDOWN:
			{
				// Alt-F4 or Alt+Space was pressed. 
				// Allow alt+f4 to close the window and alt+space to open the window menu
				if (wParam != VK_F4 && wParam != VK_SPACE)
				{
					DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				}
			}
			break;

			case WM_KEYDOWN:
			case WM_SYSKEYUP:
			case WM_KEYUP:
			case WM_LBUTTONDBLCLK:
			case WM_LBUTTONDOWN:
			case WM_MBUTTONDBLCLK:
			case WM_MBUTTONDOWN:
			case WM_RBUTTONDBLCLK:
			case WM_RBUTTONDOWN:
			case WM_XBUTTONDBLCLK:
			case WM_XBUTTONDOWN:
			case WM_XBUTTONUP:
			case WM_LBUTTONUP:
			case WM_MBUTTONUP:
			case WM_RBUTTONUP:
			case WM_NCMOUSEMOVE:
			case WM_MOUSEMOVE:
			case WM_MOUSEWHEEL:
#if WINVER >= 0x0601
			case WM_TOUCH:
#endif
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				// Handled
				return 0;
			}
			break;

			case WM_SETCURSOR:
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);

				// If we're rendering our own window border, we'll "handle" this event so that Windows doesn't try to manage the cursor
				// appearance for us in the non-client area.  However, for OS window borders we need to fall through to DefWindowProc to
				// allow Windows to draw the resize cursor
				if (!CurrentNativeEventWindow->GetDefinition().HasBorder)
				{
					// Handled
					return 0;
				}
			}
			break;

			// Mouse Movement
			case WM_INPUT:
			{
				uint32 Size = 0;
				::GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &Size, sizeof(RAWINPUTHEADER));

				std::unique_ptr<uint8[]> RawData = std::make_unique<uint8[]>(Size);

				if (::GetRawInputData((HRAWINPUT)lParam, RID_INPUT, RawData.Get(), &Size, sizeof(RAWINPUTHEADER)) == Size)
				{
					const RAWINPUT* const Raw = (const RAWINPUT* const)RawData.Get();

					if (Raw->header.dwType == RIM_TYPEMOUSE)
					{
						const bool IsAbsoluteInput = (Raw->data.mouse.usFlags & MOUSE_MOVE_ABSOLUTE) == MOUSE_MOVE_ABSOLUTE;
						if (IsAbsoluteInput)
						{
							// Since the raw input is coming in as absolute it is likely the user is using a tablet
							// or perhaps is interacting through a virtual desktop
							DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam, 0, 0, MOUSE_MOVE_ABSOLUTE);
							return 1;
						}

						// Since raw input is coming in as relative it is likely a traditional mouse device
						const int xPosRelative = Raw->data.mouse.lLastX;
						const int yPosRelative = Raw->data.mouse.lLastY;

						DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam, xPosRelative, yPosRelative, MOUSE_MOVE_RELATIVE);
						return 1;
					}
				}
			}
			break;


			case WM_NCCALCSIZE:
			{
				// Let windows absorb this message if using the standard border
				if (wParam && !CurrentNativeEventWindow->GetDefinition().HasBorder)
				{
					// Borderless game windows are not actually borderless, they have a thick border that we simply draw game content over (client
					// rect contains the window border). When maximized Windows will bleed our border over the edges of the monitor. So that we
					// don't draw content we are going to later discard, we change a maximized window's size and position so that the entire
					// window rect (including the border) sits inside the monitor. The size adjustments here will be sent to WM_MOVE and
					// WM_SIZE and the window will still be considered maximized.
#undef IsMaximized
					if (CurrentNativeEventWindow->GetDefinition().Type == EWindowType::Main && CurrentNativeEventWindow->IsMaximized())
					{
						// Ask the system for the window border size as this is the amount that Windows will bleed our window over the edge
						// of our desired space. The value returned by CurrentNativeEventWindow will be incorrect for our usage here as it
						// refers to the border of the window that Slate should consider.
						WINDOWINFO WindowInfo;
						memset(&WindowInfo, 0, sizeof(WindowInfo));
						WindowInfo.cbSize = sizeof(WindowInfo);
						::GetWindowInfo(hwnd, &WindowInfo);

						// A pointer to the window size data that Windows will use is passed to us in lParam
						LPNCCALCSIZE_PARAMS ResizingRects = (LPNCCALCSIZE_PARAMS)lParam;
						// The first rectangle contains the client rectangle of the resized window. Decrease window size on all sides by
						// the border size.
						ResizingRects->rgrc[0].left += WindowInfo.cxWindowBorders;
						ResizingRects->rgrc[0].top += WindowInfo.cxWindowBorders;
						ResizingRects->rgrc[0].right -= WindowInfo.cxWindowBorders;
						ResizingRects->rgrc[0].bottom -= WindowInfo.cxWindowBorders;
						// The second rectangle contains the destination rectangle for the content currently displayed in the window's
						// client rect. Windows will blit the previous client content into this new location to simulate the move of
						// the window until the window can repaint itself. This should also be adjusted to our new window size.
						ResizingRects->rgrc[1].left = ResizingRects->rgrc[0].left;
						ResizingRects->rgrc[1].top = ResizingRects->rgrc[0].top;
						ResizingRects->rgrc[1].right = ResizingRects->rgrc[0].right;
						ResizingRects->rgrc[1].bottom = ResizingRects->rgrc[0].bottom;
						// A third rectangle is passed in that contains the source rectangle (client area from window pre-maximize).
						// It's value should not be changed.

						// The new window position. Pull in the window on all sides by the width of the window border so that the
						// window fits entirely on screen. We'll draw over these borders with game content.
						ResizingRects->lppos->x += WindowInfo.cxWindowBorders;
						ResizingRects->lppos->y += WindowInfo.cxWindowBorders;
						ResizingRects->lppos->cx -= 2 * WindowInfo.cxWindowBorders;
						ResizingRects->lppos->cy -= 2 * WindowInfo.cxWindowBorders;

						// Informs Windows to use the values as we altered them.
						return WVR_VALIDRECTS;
					}

					return 0;
				}
			}
			break;

			case WM_SHOWWINDOW:
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
			}
			break;

			case WM_SIZE:
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);

				const bool bWasMaximized = (wParam == SIZE_MAXIMIZED);
				const bool bWasRestored = (wParam == SIZE_RESTORED);

				if (bWasMaximized || bWasRestored)
				{
					//MessageHandler->OnWindowAction(CurrentNativeEventWindow, bWasMaximized ? EWindowAction::Maximize : EWindowAction::Restore);
				}

				return 0;
			}
			break;

			case WM_SIZING:
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam, 0, 0);

				if (CurrentNativeEventWindowPtr->GetDefinition().ShouldPreserveAspectRatio)
				{
					// The rect we get in lParam is window rect, but we need to preserve client's aspect ratio,
					// so we need to find what the border and title bar sizes are, if window has them and adjust the rect.
					WINDOWINFO WindowInfo;
					memset(&WindowInfo, 0, sizeof(WindowInfo));
					WindowInfo.cbSize = sizeof(WindowInfo);
					::GetWindowInfo(hwnd, &WindowInfo);

					RECT TestRect;
					TestRect.left = TestRect.right = TestRect.top = TestRect.bottom = 0;
					AdjustWindowRectEx(&TestRect, WindowInfo.dwStyle, false, WindowInfo.dwExStyle);

					RECT* Rect = (RECT*)lParam;
					Rect->left -= TestRect.left;
					Rect->right -= TestRect.right;
					Rect->top -= TestRect.top;
					Rect->bottom -= TestRect.bottom;

					const float AspectRatio = CurrentNativeEventWindowPtr->GetAspectRatio();
					int32 NewWidth = Rect->right - Rect->left;
					int32 NewHeight = Rect->bottom - Rect->top;

					switch (wParam)
					{
					case WMSZ_LEFT:
					case WMSZ_RIGHT:
					{
						int32 AdjustedHeight = NewWidth / AspectRatio;
						Rect->top -= (AdjustedHeight - NewHeight) / 2;
						Rect->bottom += (AdjustedHeight - NewHeight) / 2;
						break;
					}
					case WMSZ_TOP:
					case WMSZ_BOTTOM:
					{
						int32 AdjustedWidth = NewHeight * AspectRatio;
						Rect->left -= (AdjustedWidth - NewWidth) / 2;
						Rect->right += (AdjustedWidth - NewWidth) / 2;
						break;
					}
					case WMSZ_TOPLEFT:
					{
						int32 AdjustedHeight = NewWidth / AspectRatio;
						Rect->top -= AdjustedHeight - NewHeight;
						break;
					}
					case WMSZ_TOPRIGHT:
					{
						int32 AdjustedHeight = NewWidth / AspectRatio;
						Rect->top -= AdjustedHeight - NewHeight;
						break;
					}
					case WMSZ_BOTTOMLEFT:
					{
						int32 AdjustedHeight = NewWidth / AspectRatio;
						Rect->bottom += AdjustedHeight - NewHeight;
						break;
					}
					case WMSZ_BOTTOMRIGHT:
					{
						int32 AdjustedHeight = NewWidth / AspectRatio;
						Rect->bottom += AdjustedHeight - NewHeight;
						break;
					}
					}

					AdjustWindowRectEx(Rect, WindowInfo.dwStyle, false, WindowInfo.dwExStyle);

					return TRUE;
				}
			}
			break;
			case WM_ENTERSIZEMOVE:
			{
				bInModalSizeLoop = true;
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam, 0, 0);
			}
			break;
			case WM_EXITSIZEMOVE:
			{
				bInModalSizeLoop = false;
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam, 0, 0);
			}
			break;


			case WM_MOVE:
			{
				// client area position
				const int32 NewX = (int)(short)(LOWORD(lParam));
				const int32 NewY = (int)(short)(HIWORD(lParam));
				Int2 NewPosition(NewX, NewY);

				// Only cache the screen position if its not minimized
				if (CWindowsApplication::MinimizedWindowPosition.x != NewPosition.x 
					&& CWindowsApplication::MinimizedWindowPosition.y != NewPosition.y)
				{
					MessageHandler->OnMovedWindow(CurrentNativeEventWindow, NewX, NewY);

					return 0;
				}
			}
			break;

			case WM_NCHITTEST:
			{
				// Only needed if not using the os window border as this is determined automatically
				if (!CurrentNativeEventWindow->GetDefinition().HasBorder)
				{
					RECT rcWindow;
					GetWindowRect(hwnd, &rcWindow);

					const int32 LocalMouseX = (int)(short)(LOWORD(lParam)) - rcWindow.left;
					const int32 LocalMouseY = (int)(short)(HIWORD(lParam)) - rcWindow.top;
					/*if (CurrentNativeEventWindow->IsRegularWindow())
					{
						ENativeWindowZone Zone;

						if (MessageHandler->ShouldProcessUserInputMessages(CurrentNativeEventWindowPtr))
						{
							// Assumes this is not allowed to leave Slate or touch rendering
							Zone = MessageHandler->GetWindowZoneForPoint(CurrentNativeEventWindow, LocalMouseX, LocalMouseY);
						}
						else
						{
							// Default to client area so that we are able to see the feedback effect when attempting to click on a non-modal window when a modal window is active
							// Any other window zones could have side effects and NotInWindow prevents the feedback effect.
							Zone = ENativeWindowZone::ClientArea;
						}

						static const LRESULT Results[] = { HTNOWHERE, HTTOPLEFT, HTTOP, HTTOPRIGHT, HTLEFT, HTCLIENT,
							HTRIGHT, HTBOTTOMLEFT, HTBOTTOM, HTBOTTOMRIGHT,
							HTCAPTION, HTMINBUTTON, HTMAXBUTTON, HTCLOSE, HTSYSMENU };

						return Results[Zone];
					}*/
				}
			}
			break;

#if WINVER > 0x502
			case WM_DWMCOMPOSITIONCHANGED:
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
			}
			break;
#endif

			// Window focus and activation
			case WM_MOUSEACTIVATE:
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
			}
			break;

			// Window focus and activation
			case WM_ACTIVATE:
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
			}
			break;

			case WM_ACTIVATEAPP:
			{
				// When window activation changes we are not in a modal size loop
				bInModalSizeLoop = false;
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
			}
			break;

			case WM_SETTINGCHANGE:
			{
				// Convertible mode change
				/*if ((lParam != NULL) && (wcscmp(TEXT("ConvertibleSlateMode"), (TCHAR *)lParam) == 0))
				{
					DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				}*/
			}
			break;

			case WM_PAINT:
			{
				if (bInModalSizeLoop && IsInGameThread())
				{
					//MessageHandler->OnOSPaint(CurrentNativeEventWindowPtr.ToSharedRef());
				}
			}
			break;

			case WM_ERASEBKGND:
			{
				// Intercept background erasing to eliminate flicker.
				// Return non-zero to indicate that we'll handle the erasing ourselves
				return 1;
			}
			break;

			case WM_NCACTIVATE:
			{
				if (!CurrentNativeEventWindow->GetDefinition().HasBorder)
				{
					// Unless using the OS window border, intercept calls to prevent non-client area drawing a border upon activation or deactivation
					// Return true to ensure standard activation happens
					return true;
				}
			}
			break;

			case WM_NCPAINT:
			{
				if (!CurrentNativeEventWindow->GetDefinition().HasBorder)
				{
					// Unless using the OS window border, intercept calls to draw the non-client area - we do this ourselves
					return 0;
				}
			}
			break;

			case WM_DESTROY:
			{
				Windows.EraseItem(CurrentNativeEventWindow);
				return 0;
			}
			break;

			case WM_CLOSE:
			{
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				return 0;
			}
			break;

			case WM_SYSCOMMAND:
			{
				switch (wParam & 0xfff0)
				{
				case SC_RESTORE:
					// Checks to see if the window is minimized.
					if (IsIconic(hwnd))
					{
						// This is required for restoring a minimized fullscreen window
						::ShowWindow(hwnd, SW_RESTORE);
						return 0;
					}
					else
					{
						/*if (!MessageHandler->OnWindowAction(CurrentNativeEventWindow, EWindowAction::Restore))
						{
							return 1;
						}*/
					}
					break;
				case SC_MAXIMIZE:
				{
					/*if (!MessageHandler->OnWindowAction(CurrentNativeEventWindow, EWindowAction::Maximize))
					{
						return 1;
					}*/
				}
				break;
				default:
					/*if (!(MessageHandler->ShouldProcessUserInputMessages(CurrentNativeEventWindow) && IsInputMessage(msg)))
					{
						return 0;
					}*/
					break;
				}
			}
			break;

			case WM_GETMINMAXINFO:
			{
				/*MINMAXINFO* MinMaxInfo = (MINMAXINFO*)lParam;
				FWindowSizeLimits SizeLimits = MessageHandler->GetSizeLimitsForWindow(CurrentNativeEventWindow);

				// We need to inflate the max values if using an OS window border
				int32 BorderWidth = 0;
				int32 BorderHeight = 0;
				if (CurrentNativeEventWindow->GetDefinition().HasOSWindowBorder)
				{
					const DWORD WindowStyle = ::GetWindowLong(hwnd, GWL_STYLE);
					const DWORD WindowExStyle = ::GetWindowLong(hwnd, GWL_EXSTYLE);

					// This adjusts a zero rect to give us the size of the border
					RECT BorderRect = { 0, 0, 0, 0 };
					::AdjustWindowRectEx(&BorderRect, WindowStyle, false, WindowExStyle);

					BorderWidth = BorderRect.right - BorderRect.left;
					BorderHeight = BorderRect.bottom - BorderRect.top;
				}

				// We always apply BorderWidth and BorderHeight since Slate always works with client area window sizes
				MinMaxInfo->ptMinTrackSize.x = FMath::RoundToInt(SizeLimits.GetMinWidth().Get(MinMaxInfo->ptMinTrackSize.x));
				MinMaxInfo->ptMinTrackSize.y = FMath::RoundToInt(SizeLimits.GetMinHeight().Get(MinMaxInfo->ptMinTrackSize.y));
				MinMaxInfo->ptMaxTrackSize.x = FMath::RoundToInt(SizeLimits.GetMaxWidth().Get(MinMaxInfo->ptMaxTrackSize.x)) + BorderWidth;
				MinMaxInfo->ptMaxTrackSize.y = FMath::RoundToInt(SizeLimits.GetMaxHeight().Get(MinMaxInfo->ptMaxTrackSize.y)) + BorderHeight;
				*/return 0;
			}
			break;

			case WM_NCLBUTTONDOWN:
			case WM_NCRBUTTONDOWN:
			case WM_NCMBUTTONDOWN:
			{
				switch (wParam)
				{
				case HTMINBUTTON:
				{
					/*if (!MessageHandler->OnWindowAction(CurrentNativeEventWindow, EWindowAction::ClickedNonClientArea))
					{
						return 1;
					}*/
				}
				break;
				case HTMAXBUTTON:
				{
				/*	if (!MessageHandler->OnWindowAction(CurrentNativeEventWindow, EWindowAction::ClickedNonClientArea))
					{
						return 1;
					}*/
				}
				break;
				case HTCLOSE:
				{
					/*if (!MessageHandler->OnWindowAction(CurrentNativeEventWindow, EWindowAction::ClickedNonClientArea))
					{
						return 1;
					}*/
				}
				break;
				case HTCAPTION:
				{
					/*if (!MessageHandler->OnWindowAction(CurrentNativeEventWindow, EWindowAction::ClickedNonClientArea))
					{
						return 1;
					}*/
				}
				break;
				}
			}
			break;

			case WM_DISPLAYCHANGE:
			{
				// Slate needs to know when desktop size changes.
				//FDisplayMetrics DisplayMetrics;
				//FDisplayMetrics::GetDisplayMetrics(DisplayMetrics);
				//BroadcastDisplayMetricsChanged(DisplayMetrics);
			}
			break;

			case WM_DPICHANGED:
				DeferMessage(CurrentNativeEventWindowPtr, hwnd, msg, wParam, lParam);
				break;

			case WM_GETDLGCODE:
			{
				// Slate wants all keys and messages.
				return DLGC_WANTALLKEYS;
			}
			break;

			case WM_CREATE:
				return 0;

			case WM_DEVICECHANGE:
			{
				//XInput->SetNeedsControllerStateUpdate();
				QueryConnectedMice();
			}
			break;

			default:
				if (bMessageExternallyHandled)
				{
					return ExternalMessageHandlerResult;
				}
			}
		}

		return DefWindowProc(hwnd, msg, wParam, lParam);
	}

	void CWindowsApplication::CheckForShiftUpEvents(const int32 KeyCode)
	{
		assert(KeyCode == VK_LSHIFT || KeyCode == VK_RSHIFT);

		// Since VK_SHIFT doesn't get an up message if the other shift key is held we need to poll for it
		const EModifierKey::Type ModifierKeyIndex = KeyCode == VK_LSHIFT ? EModifierKey::LeftShift : EModifierKey::RightShift;
		if (ModifierKeyState[ModifierKeyIndex] && ((::GetKeyState(KeyCode) & 0x8000) == 0))
		{
			ModifierKeyState[ModifierKeyIndex] = false;
			// @todo
		//	MessageHandler->OnKeyEvent(CurrentWindow, KeyCode, 0, false);
		}
	}


	EScancode HandleKeyEvent(LPARAM lParam, WPARAM wParam)
	{
		EScancode code;
		char bIsExtended;
		int nScanCode = (lParam >> 16) & 0xFF;

		/* 0x45 here to work around both pause and numlock sharing the same scancode, so use the VK key to tell them apart */
		if (nScanCode == 0 || nScanCode == 0x45) {
			switch (wParam) {
			case VK_CLEAR: return SC_CLEAR;
			case VK_MODECHANGE: return SC_MODE;
			case VK_SELECT: return SC_SELECT;
			case VK_EXECUTE: return SC_EXECUTE;
			case VK_HELP: return SC_HELP;
			case VK_PAUSE: return SC_PAUSE;
			case VK_NUMLOCK: return SC_NUMLOCKCLEAR;

			case VK_F13: return SC_F13;
			case VK_F14: return SC_F14;
			case VK_F15: return SC_F15;
			case VK_F16: return SC_F16;
			case VK_F17: return SC_F17;
			case VK_F18: return SC_F18;
			case VK_F19: return SC_F19;
			case VK_F20: return SC_F20;
			case VK_F21: return SC_F21;
			case VK_F22: return SC_F22;
			case VK_F23: return SC_F23;
			case VK_F24: return SC_F24;

			case VK_OEM_NEC_EQUAL: return SC_KP_EQUALS;
			case VK_BROWSER_BACK: return SC_AC_BACK;
			case VK_BROWSER_FORWARD: return SC_AC_FORWARD;
			case VK_BROWSER_REFRESH: return SC_AC_REFRESH;
			case VK_BROWSER_STOP: return SC_AC_STOP;
			case VK_BROWSER_SEARCH: return SC_AC_SEARCH;
			case VK_BROWSER_FAVORITES: return SC_AC_BOOKMARKS;
			case VK_BROWSER_HOME: return SC_AC_HOME;
			case VK_VOLUME_MUTE: return SC_AUDIOMUTE;
			case VK_VOLUME_DOWN: return SC_VOLUMEDOWN;
			case VK_VOLUME_UP: return SC_VOLUMEUP;

			case VK_MEDIA_NEXT_TRACK: return SC_AUDIONEXT;
			case VK_MEDIA_PREV_TRACK: return SC_AUDIOPREV;
			case VK_MEDIA_STOP: return SC_AUDIOSTOP;
			case VK_MEDIA_PLAY_PAUSE: return SC_AUDIOPLAY;
			case VK_LAUNCH_MAIL: return SC_MAIL;
			case VK_LAUNCH_MEDIA_SELECT: return SC_MEDIASELECT;

			case VK_OEM_102: return SC_NONUSBACKSLASH;

			case VK_ATTN: return SC_SYSREQ;
			case VK_CRSEL: return SC_CRSEL;
			case VK_EXSEL: return SC_EXSEL;
			case VK_OEM_CLEAR: return SC_CLEAR;

			case VK_LAUNCH_APP1: return SC_APP1;
			case VK_LAUNCH_APP2: return SC_APP2;

			default: return SC_UNKNOWN;
			}
		}

		if (nScanCode > 127)
			return SC_UNKNOWN;

		code = windows_scancode_table[nScanCode];

		bIsExtended = (lParam & (1 << 24)) != 0;
		if (!bIsExtended) {
			switch (code) {
			case SC_HOME:
				return SC_KP_7;
			case SC_UP:
				return SC_KP_8;
			case SC_PAGEUP:
				return SC_KP_9;
			case SC_LEFT:
				return SC_KP_4;
			case SC_RIGHT:
				return SC_KP_6;
			case SC_END:
				return SC_KP_1;
			case SC_DOWN:
				return SC_KP_2;
			case SC_PAGEDOWN:
				return SC_KP_3;
			case SC_INSERT:
				return SC_KP_0;
			case SC_DELETE:
				return SC_KP_PERIOD;
			case SC_PRINTSCREEN:
				return SC_KP_MULTIPLY;
			default:
				break;
			}
		}
		else {
			switch (code) {
			case SC_RETURN:
				return SC_KP_ENTER;
			case SC_LALT:
				return SC_RALT;
			case SC_LCTRL:
				return SC_RCTRL;
			case SC_SLASH:
				return SC_KP_DIVIDE;
			case SC_CAPSLOCK:
				return SC_KP_PLUS;
			default:
				break;
			}
		}

		return code;
	}

	int32 CWindowsApplication::ProcessDeferredMessage(const FDeferredWindowsMessage& DeferredMessage)
	{
		if (Windows.GetSize() && DeferredMessage.NativeWindow)
		{
			HWND hwnd = DeferredMessage.hWND;
			uint32 msg = DeferredMessage.Message;
			WPARAM wParam = DeferredMessage.wParam;
			LPARAM lParam = DeferredMessage.lParam;

			CWindowsWindow* CurrentNativeEventWindowPtr = DeferredMessage.NativeWindow;

			// This effectively disables a window without actually disabling it natively with the OS.
			// This allows us to continue receiving messages for it.
			/*if (!MessageHandler->ShouldProcessUserInputMessages(CurrentNativeEventWindowPtr) && IsInputMessage(msg))
			{
				if (IsKeyboardInputMessage(msg))
				{
					// Force an update since we may have just consumed a modifier key state change
					UpdateAllModifierKeyStates();
				}
				return 0;	// consume input messages
			}*/

			switch (msg)
			{
			case WM_INPUTLANGCHANGEREQUEST:
			case WM_INPUTLANGCHANGE:
			case WM_IME_SETCONTEXT:
			case WM_IME_NOTIFY:
			case WM_IME_REQUEST:
			case WM_IME_STARTCOMPOSITION:
			case WM_IME_COMPOSITION:
			case WM_IME_ENDCOMPOSITION:
			case WM_IME_CHAR:
			{
				//if (TextInputMethodSystem.IsValid())
				//{
				//	TextInputMethodSystem->ProcessMessage(hwnd, msg, wParam, lParam);
				//}
				return 0;
			}
			break;
			// Character
			case WM_CHAR:
			{
				// Character code is stored in WPARAM
				const TCHAR Character = wParam;

				// LPARAM bit 30 will be ZERO for new presses, or ONE if this is a repeat
				const bool bIsRepeat = (lParam & 0x40000000) != 0;

				uint8 pressed[4];
				pressed[0] = Character;
				MessageHandler->OnCharEvent(CurrentNativeEventWindowPtr, 1, pressed);// bIsRepeat);

				// Note: always return 0 to handle the message.  Win32 beeps if WM_CHAR is not handled...
				return 0;
			}
			break;


			// Key down
			case WM_SYSKEYDOWN:
			case WM_KEYDOWN:
			{
				// Character code is stored in WPARAM
				const int32 Win32Key = wParam;

				// The actual key to use.  Some keys will be translated into other keys. 
				// I.E VK_CONTROL will be translated to either VK_LCONTROL or VK_RCONTROL as these
				// keys are never sent on their own
				int32 ActualKey = Win32Key;

				// LPARAM bit 30 will be ZERO for new presses, or ONE if this is a repeat
				bool bIsRepeat = (lParam & 0x40000000) != 0;

				switch (Win32Key)
				{
				case VK_MENU:
					// Differentiate between left and right alt
					if ((lParam & 0x1000000) == 0)
					{
						ActualKey = VK_LMENU;
						bIsRepeat = ModifierKeyState[EModifierKey::LeftAlt];
						ModifierKeyState[EModifierKey::LeftAlt] = true;
					}
					else
					{
						ActualKey = VK_RMENU;
						bIsRepeat = ModifierKeyState[EModifierKey::RightAlt];
						ModifierKeyState[EModifierKey::RightAlt] = true;
					}
					break;
				case VK_CONTROL:
					// Differentiate between left and right control
					if ((lParam & 0x1000000) == 0)
					{
						ActualKey = VK_LCONTROL;
						bIsRepeat = ModifierKeyState[EModifierKey::LeftControl];
						ModifierKeyState[EModifierKey::LeftControl] = true;
					}
					else
					{
						ActualKey = VK_RCONTROL;
						bIsRepeat = ModifierKeyState[EModifierKey::RightControl];
						ModifierKeyState[EModifierKey::RightControl] = true;
					}
					break;
				case VK_SHIFT:
					// Differentiate between left and right shift
					ActualKey = MapVirtualKey((lParam & 0x00ff0000) >> 16, MAPVK_VSC_TO_VK_EX);
					if (ActualKey == VK_LSHIFT)
					{
						bIsRepeat = ModifierKeyState[EModifierKey::LeftShift];
						ModifierKeyState[EModifierKey::LeftShift] = true;
					}
					else
					{
						bIsRepeat = ModifierKeyState[EModifierKey::RightShift];
						ModifierKeyState[EModifierKey::RightShift] = true;
					}
					break;
				case VK_CAPITAL:
					ModifierKeyState[EModifierKey::CapsLock] = (::GetKeyState(VK_CAPITAL) & 0x0001) != 0;
					break;
				default:
					// No translation needed
					break;
				}

				// Get the character code from the virtual key pressed.  If 0, no translation from virtual key to character exists
				uint32 CharCode = ::MapVirtualKey(Win32Key, MAPVK_VK_TO_CHAR);

				EScancode scancode = HandleKeyEvent(lParam, wParam);

				MessageHandler->OnKeyEvent(CurrentNativeEventWindowPtr, scancode, 0, true);// bIsRepeat);

				// Always return 0 to handle the message or else windows will beep
				if (/*Result ||*/ msg != WM_SYSKEYDOWN)
				{
					// Handled
					return 0;
				}
			}
			break;


			// Key up
			case WM_SYSKEYUP:
			case WM_KEYUP:
			{
				// Character code is stored in WPARAM
				int32 Win32Key = wParam;

				// The actual key to use.  Some keys will be translated into other keys. 
				// I.E VK_CONTROL will be translated to either VK_LCONTROL or VK_RCONTROL as these
				// keys are never sent on their own
				int32 ActualKey = Win32Key;

				bool bModifierKeyReleased = false;
				switch (Win32Key)
				{
				case VK_MENU:
					// Differentiate between left and right alt
					if ((lParam & 0x1000000) == 0)
					{
						ActualKey = VK_LMENU;
						ModifierKeyState[EModifierKey::LeftAlt] = false;
					}
					else
					{
						ActualKey = VK_RMENU;
						ModifierKeyState[EModifierKey::RightAlt] = false;
					}
					break;
				case VK_CONTROL:
					// Differentiate between left and right control
					if ((lParam & 0x1000000) == 0)
					{
						ActualKey = VK_LCONTROL;
						ModifierKeyState[EModifierKey::LeftControl] = false;
					}
					else
					{
						ActualKey = VK_RCONTROL;
						ModifierKeyState[EModifierKey::RightControl] = false;
					}
					break;
				case VK_SHIFT:
					// Differentiate between left and right shift
					ActualKey = MapVirtualKey((lParam & 0x00ff0000) >> 16, MAPVK_VSC_TO_VK_EX);
					if (ActualKey == VK_LSHIFT)
					{
						ModifierKeyState[EModifierKey::LeftShift] = false;
					}
					else
					{
						ModifierKeyState[EModifierKey::RightShift] = false;
					}
					break;
				case VK_CAPITAL:
					ModifierKeyState[EModifierKey::CapsLock] = (::GetKeyState(VK_CAPITAL) & 0x0001) != 0;
					break;
				default:
					// No translation needed
					break;
				}

				// Get the character code from the virtual key pressed.  If 0, no translation from virtual key to character exists
				uint32 CharCode = ::MapVirtualKey(Win32Key, MAPVK_VK_TO_CHAR);

				// Key up events are never repeats
				const bool bIsRepeat = false;

				EScancode scancode = HandleKeyEvent(lParam, wParam);

				MessageHandler->OnKeyEvent(CurrentNativeEventWindowPtr, scancode, 0, false);

				// Note that we allow system keys to pass through to DefWndProc here, so that core features
				// like Alt+F4 to close a window work.
				if (/*Result || */msg != WM_SYSKEYUP)
				{
					// Handled
					return 0;
				}
			}
			break;

			// Mouse Button Down
			case WM_LBUTTONDBLCLK:
			case WM_LBUTTONDOWN:
			case WM_MBUTTONDBLCLK:
			case WM_MBUTTONDOWN:
			case WM_RBUTTONDBLCLK:
			case WM_RBUTTONDOWN:
			case WM_XBUTTONDBLCLK:
			case WM_XBUTTONDOWN:
			case WM_LBUTTONUP:
			case WM_MBUTTONUP:
			case WM_RBUTTONUP:
			case WM_XBUTTONUP:
			{
				POINT CursorPoint;
				CursorPoint.x = GET_X_LPARAM(lParam);
				CursorPoint.y = GET_Y_LPARAM(lParam);

				ClientToScreen(hwnd, &CursorPoint);

				const Vec2 CursorPos(CursorPoint.x, CursorPoint.y);

				EMouseButtons MouseButton = EMouseButtons::MouseButton_None;
				bool bDoubleClick = false;
				bool bMouseUp = false;
				switch (msg)
				{
				case WM_LBUTTONDBLCLK:
					bDoubleClick = true;
					MouseButton = EMouseButtons::MouseButton_Left;
					break;
				case WM_LBUTTONUP:
					bMouseUp = true;
					MouseButton = EMouseButtons::MouseButton_Left;
					break;
				case WM_LBUTTONDOWN:
					MouseButton = EMouseButtons::MouseButton_Left;
					break;
				case WM_MBUTTONDBLCLK:
					bDoubleClick = true;
					MouseButton = EMouseButtons::MouseButton_Middle;
					break;
				case WM_MBUTTONUP:
					bMouseUp = true;
					MouseButton = EMouseButtons::MouseButton_Middle;
					break;
				case WM_MBUTTONDOWN:
					MouseButton = EMouseButtons::MouseButton_Middle;
					break;
				case WM_RBUTTONDBLCLK:
					bDoubleClick = true;
					MouseButton = EMouseButtons::MouseButton_Right;
					break;
				case WM_RBUTTONUP:
					bMouseUp = true;
					MouseButton = EMouseButtons::MouseButton_Right;
					break;
				case WM_RBUTTONDOWN:
					MouseButton = EMouseButtons::MouseButton_Right;
					break;
				case WM_XBUTTONDBLCLK:
					bDoubleClick = true;
					MouseButton = (HIWORD(wParam) & XBUTTON1) ? EMouseButtons::Thumb01 : EMouseButtons::Thumb02;
					break;
				case WM_XBUTTONUP:
					bMouseUp = true;
					MouseButton = (HIWORD(wParam) & XBUTTON1) ? EMouseButtons::Thumb01 : EMouseButtons::Thumb02;
					break;
				case WM_XBUTTONDOWN:
					MouseButton = (HIWORD(wParam) & XBUTTON1) ? EMouseButtons::Thumb01 : EMouseButtons::Thumb02;
					break;
				default:
					assert(0);
				}

				if (bMouseUp)
				{
					MessageHandler->OnMouseEvent(CurrentNativeEventWindowPtr, CursorPos.x, CursorPos.y, MouseButton, false);
					return 1; 
				}
				else if (bDoubleClick)
				{
					//MessageHandler->OnMouseDoubleClick(CurrentNativeEventWindowPtr, MouseButton, CursorPos);
				}
				else
				{
					MessageHandler->OnMouseEvent(CurrentNativeEventWindowPtr, CursorPos.x, CursorPos.y, MouseButton, true);
				}
				return 0;
			}
			break;

			// Mouse Movement
			case WM_INPUT:
			{
				if (DeferredMessage.RawInputFlags == MOUSE_MOVE_RELATIVE)
				{
					MessageHandler->OnMouseEvent(CurrentNativeEventWindowPtr, DeferredMessage.X, DeferredMessage.Y, true);
				}
				else
				{
					// Absolute coordinates given through raw input are simulated using MouseMove to get relative coordinates
					MessageHandler->OnMouseEvent(CurrentNativeEventWindowPtr, DeferredMessage.X, DeferredMessage.Y, false);
				}

				return 0;
			}
			break;

			// Mouse Movement
			case WM_NCMOUSEMOVE:
			case WM_MOUSEMOVE:
			{
				BOOL Result = false;
				if (!bRelativeMouseMode)
				{

					MessageHandler->OnMouseEvent(CurrentNativeEventWindowPtr, DeferredMessage.X, DeferredMessage.Y, false);
				}

				return 1;// Result ? 0 : 1;
			}
			break;
			// Mouse Wheel
			case WM_MOUSEWHEEL:
			{
				const float SpinFactor = 1 / 120.0f;
				const SHORT WheelDelta = GET_WHEEL_DELTA_WPARAM(wParam);

				POINT CursorPoint;
				CursorPoint.x = GET_X_LPARAM(lParam);
				CursorPoint.y = GET_Y_LPARAM(lParam);

				const Vec2 CursorPos(CursorPoint.x, CursorPoint.y);

				const float Y = static_cast<float>(WheelDelta) * SpinFactor;
				MessageHandler->OnTouchGesture(CurrentNativeEventWindowPtr, ETouchGestureType::Scroll, Vec2(0, Y), Y, false);
				return 1;
			}
			break;

			// Mouse Cursor
			case WM_SETCURSOR:
			{
				//return MessageHandler->OnCursorSet() ? 0 : 1;
			}
			break;

#if WINVER >= 0x0601
			/*case WM_TOUCH:
			{
				UINT InputCount = LOWORD(wParam);
				if (InputCount > 0)
				{
					TUniquePtr<TOUCHINPUT[]> Inputs = MakeUnique<TOUCHINPUT[]>(InputCount);
					if (GetTouchInputInfo((HTOUCHINPUT)lParam, InputCount, Inputs.Get(), sizeof(TOUCHINPUT)))
					{
						for (uint32 i = 0; i < InputCount; i++)
						{
							TOUCHINPUT Input = Inputs[i];
							FVector2D Location(Input.x / 100.0f, Input.y / 100.0f);
							if (Input.dwFlags & TOUCHEVENTF_DOWN)
							{
								int32 TouchIndex = GetTouchIndexForID(Input.dwID);
								if (TouchIndex < 0)
								{
									TouchIndex = GetFirstFreeTouchIndex();
									if (TouchIndex >= 0)
									{
										TouchIDs[TouchIndex] = TOptional<int32>(Input.dwID);
										UE_LOG(LogWindowsDesktop, Verbose, TEXT("OnTouchStarted at (%f, %f), finger %d (system touch id %d)"), Location.X, Location.Y, TouchIndex + 1, Input.dwID);
										MessageHandler->OnTouchStarted(CurrentNativeEventWindowPtr, Location, TouchIndex + 1, 0);
									}
									else
									{
										// TODO: Error handling for more than 10 touches?
									}
								}
							}
							else if (Input.dwFlags & TOUCHEVENTF_MOVE)
							{
								int32 TouchIndex = GetTouchIndexForID(Input.dwID);
								if (TouchIndex >= 0)
								{
									UE_LOG(LogWindowsDesktop, Verbose, TEXT("OnTouchMoved at (%f, %f), finger %d (system touch id %d)"), Location.X, Location.Y, TouchIndex + 1, Input.dwID);
									MessageHandler->OnTouchMoved(Location, TouchIndex + 1, 0);
								}
							}
							else if (Input.dwFlags & TOUCHEVENTF_UP)
							{
								int32 TouchIndex = GetTouchIndexForID(Input.dwID);
								if (TouchIndex >= 0)
								{
									TouchIDs[TouchIndex] = TOptional<int32>();
									UE_LOG(LogWindowsDesktop, Verbose, TEXT("OnTouchEnded at (%f, %f), finger %d (system touch id %d)"), Location.X, Location.Y, TouchIndex + 1, Input.dwID);
									MessageHandler->OnTouchEnded(Location, TouchIndex + 1, 0);
								}
								else
								{
									// TODO: Error handling.
								}
							}
						}
						CloseTouchInputHandle((HTOUCHINPUT)lParam);
						return 0;
					}
				}
				break;
			}*/
#endif

			// Window focus and activation
			case WM_MOUSEACTIVATE:
			{
				// If the mouse activate isn't in the client area we'll force the WM_ACTIVATE to be EWindowActivation::ActivateByMouse
				// This ensures that clicking menu buttons on the header doesn't generate a WM_ACTIVATE with EWindowActivation::Activate
				// which may cause mouse capture to be taken because is not differentiable from Alt-Tabbing back to the application.
				bForceActivateByMouse = !(LOWORD(lParam) & HTCLIENT);
				return 0;
			}
			break;

			// Window focus and activation
			case WM_ACTIVATE:
			{
				ENativeWindowActivation ActivationType;

				if (LOWORD(wParam) & WA_ACTIVE)
				{
					ActivationType = bForceActivateByMouse ? ENativeWindowActivation::ActivateByMouse : ENativeWindowActivation::Activate;
				}
				else if (LOWORD(wParam) & WA_CLICKACTIVE)
				{
					ActivationType = ENativeWindowActivation::ActivateByMouse;
				}
				else
				{
					ActivationType = ENativeWindowActivation::Deactivate;
				}
				bForceActivateByMouse = false;

				UpdateAllModifierKeyStates();

				CurrentWindow = CurrentNativeEventWindowPtr;

				if (CurrentNativeEventWindowPtr)
				{
					MessageHandler->OnWindowFocus(CurrentNativeEventWindowPtr, ActivationType == ENativeWindowActivation::Deactivate ? false : true);
					return 1;
				}

				return 1;
			}
			break;

			case WM_ACTIVATEAPP:
				UpdateAllModifierKeyStates();
				//MessageHandler->OnApplicationActivationChanged(!!wParam);
				break;

			case WM_SETTINGCHANGE:
				if ((lParam != 0) && (Neko::EqualStrings((LPCTSTR)lParam, TEXT("ConvertibleSlateMode"))))
				{
				//	MessageHandler->OnConvertibleLaptopModeChanged();
				}
				break;

			case WM_NCACTIVATE:
			{
				if (CurrentNativeEventWindowPtr && !CurrentNativeEventWindowPtr->GetDefinition().HasBorder)
				{
					// Unless using the OS window border, intercept calls to prevent non-client area drawing a border upon activation or deactivation
					// Return true to ensure standard activation happens
					return true;
				}
			}
			break;

			case WM_NCPAINT:
			{
				if (CurrentNativeEventWindowPtr && !CurrentNativeEventWindowPtr->GetDefinition().HasBorder)
				{
					// Unless using the OS window border, intercept calls to draw the non-client area - we do this ourselves
					return 0;
				}
			}
			break;

			case WM_CLOSE:
			{
				if (CurrentNativeEventWindowPtr)
				{
					// Called when the OS close button is pressed
					MessageHandler->OnWindowClose(CurrentNativeEventWindowPtr);
				}
				return 0;
			}
			break;

			case WM_SHOWWINDOW:
			{
				if (CurrentNativeEventWindowPtr)
				{
					switch (lParam)
					{
					case SW_PARENTCLOSING:
					//	CurrentNativeEventWindowPtr->OnParentWindowMinimized();
						break;
					case SW_PARENTOPENING:
					//	CurrentNativeEventWindowPtr->OnParentWindowRestored();
						break;
					default:
						break;
					}
				}
			}
			break;

			case WM_SIZE:
			{
				if (CurrentNativeEventWindowPtr)
				{
					// @todo Fullscreen - Perform deferred resize
					// Note WM_SIZE provides the client dimension which is not equal to the window dimension if there is a windows border 
					const int32 NewWidth = (int)(short)(LOWORD(lParam));
					const int32 NewHeight = (int)(short)(HIWORD(lParam));

					const FWindowDesc& Definition = CurrentNativeEventWindowPtr->GetDefinition();
					if (Definition.IsRegularWindow && !Definition.HasBorder)
					{
						CurrentNativeEventWindowPtr->AdjustWindowRegion(NewWidth, NewHeight);
					}

					const bool bWasMinimized = (wParam == SIZE_MINIMIZED);

					const bool bIsFullscreen = (CurrentNativeEventWindowPtr->GetWindowMode() == ENativeWindowMode::Fullscreen);

					// When in fullscreen Windows rendering size should be determined by the application. Do not adjust based on WM_SIZE messages.
					if (!bIsFullscreen)
					{
						MessageHandler->OnSizeEvent(CurrentNativeEventWindowPtr, NewWidth, NewHeight);// , bWasMinimized);
					}
				}
			}
			break;
			case WM_SIZING:
			{
				if (CurrentNativeEventWindowPtr)
				{
					//MessageHandler->OnSizeEvent(CurrentNativeEventWindowPtr);
				}
			}
			break;
			case WM_ENTERSIZEMOVE:
			{
				if (CurrentNativeEventWindowPtr)
				{
					MessageHandler->BeginResizingWindow(CurrentNativeEventWindowPtr);
				}
			}
			break;
			case WM_EXITSIZEMOVE:
			{
				if (CurrentNativeEventWindowPtr)
				{
					MessageHandler->EndResizingWindow(CurrentNativeEventWindowPtr);
				}
			}
			break;

#if WINVER > 0x502
			case WM_DWMCOMPOSITIONCHANGED:
			{
				//CurrentNativeEventWindowPtr->OnTransparencySupportChanged(GetWindowTransparencySupport());
			}
			break;
#endif

			case WM_DPICHANGED:
			{
				if (CurrentNativeEventWindowPtr)
				{
					CurrentNativeEventWindowPtr->SetDPIScaleFactor(LOWORD(wParam) / 96.0f);


					LPRECT NewRect = (LPRECT)lParam;
					SetWindowPos(hwnd, nullptr, NewRect->left, NewRect->top, NewRect->right - NewRect->left, NewRect->bottom - NewRect->top, SWP_NOZORDER | SWP_NOACTIVATE);
				}
			}
			break;
			}
		}

		return 0;
	}


    void CWindowsApplication::ProcessEvents(void)
    {
		// Process windows messages
		{
			// This function can be reentered when entering a modal tick loop.
			// We need to make a copy of the events that need to be processed or we may end up processing the same messages twice 
			TArray<FDeferredWindowsMessage> EventsToProcess(DeferredMessages);

			DeferredMessages.Empty();
			for (int32 MessageIndex = 0; MessageIndex < EventsToProcess.GetSize(); ++MessageIndex)
			{
				const FDeferredWindowsMessage& DeferredMessage = EventsToProcess[MessageIndex];
				ProcessDeferredMessage(DeferredMessage);
			}

			CheckForShiftUpEvents(VK_LSHIFT);
			CheckForShiftUpEvents(VK_RSHIFT);
		}
    }
    
    const uint32 RESET_EVENT_SUBTYPE = 0x0f00;

    void CWindowsApplication::GetEvents()
    {
		MSG Message;

		// standard Windows message handling
		while (PeekMessage(&Message, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&Message);
			DispatchMessage(&Message);
		}
    }

	bool GPumpingMessagesOutsideOfMainLoop = false;
	void CWindowsApplication::DeferMessage(CWindowsWindow* NativeWindow, HWND InHWnd, uint32 InMessage, WPARAM InWParam, LPARAM InLParam, int32 MouseX, int32 MouseY, uint32 RawInputFlags)
	{
		if (GPumpingMessagesOutsideOfMainLoop && bAllowedToDeferMessageProcessing)
		{
			DeferredMessages.Push(FDeferredWindowsMessage(NativeWindow, InHWnd, InMessage, InWParam, InLParam, MouseX, MouseY, RawInputFlags));
		}
		else
		{
			// When not deferring messages, process them immediately
			ProcessDeferredMessage(FDeferredWindowsMessage(NativeWindow, InHWnd, InMessage, InWParam, InLParam, MouseX, MouseY, RawInputFlags));
		}
	}
    

	bool CWindowsApplication::IsInputMessage(uint32 msg)
	{
		if (IsKeyboardInputMessage(msg) || IsMouseInputMessage(msg))
		{
			return true;
		}

		switch (msg)
		{
			// Raw input notification messages...
		case WM_INPUT:
		case WM_INPUT_DEVICE_CHANGE:
			return true;
		}
		return false;
	}

	bool CWindowsApplication::IsKeyboardInputMessage(uint32 msg)
	{
		switch (msg)
		{
			// Keyboard input notification messages...
		case WM_CHAR:
		case WM_SYSCHAR:
		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
		case WM_SYSKEYUP:
		case WM_KEYUP:
		case WM_SYSCOMMAND:
			return true;
		}
		return false;
	}


	bool CWindowsApplication::IsMouseInputMessage(uint32 msg)
	{
		switch (msg)
		{
			// Mouse input notification messages...
		case WM_MOUSEHWHEEL:
		case WM_MOUSEWHEEL:
		case WM_MOUSEHOVER:
		case WM_MOUSELEAVE:
		case WM_MOUSEMOVE:
		case WM_NCMOUSEHOVER:
		case WM_NCMOUSELEAVE:
		case WM_NCMOUSEMOVE:
		case WM_NCMBUTTONDBLCLK:
		case WM_NCMBUTTONDOWN:
		case WM_NCMBUTTONUP:
		case WM_NCRBUTTONDBLCLK:
		case WM_NCRBUTTONDOWN:
		case WM_NCRBUTTONUP:
		case WM_NCXBUTTONDBLCLK:
		case WM_NCXBUTTONDOWN:
		case WM_NCXBUTTONUP:
		case WM_LBUTTONDBLCLK:
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MBUTTONDBLCLK:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
		case WM_RBUTTONDBLCLK:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_XBUTTONDBLCLK:
		case WM_XBUTTONDOWN:
		case WM_XBUTTONUP:
			return true;
		}
		return false;
	}
    
    void CWindowsApplication::UpdateKeymap()
    {
		int i;
		EScancode scancode;
		EKeyCode keymap[SC_COUNT];

		IInputSystem::GetDefaultKeymap(keymap);

		for (i = 0; i < lengthOf(windows_scancode_table); i++) {
			int vk;
			/* Make sure this scancode is a valid character scancode */
			scancode = windows_scancode_table[i];
			if (scancode == SC_UNKNOWN) {
				continue;
			}

			/* If this key is one of the non-mappable keys, ignore it */
			/* Not mapping numbers fixes the French layout, giving numeric keycodes for the number keys, which is the expected behavior */
			if ((keymap[scancode] & SC_MASK) ||
				/*  scancode == SC_GRAVE || */ /* Uncomment this line to re-enable the behavior of not mapping the "`"(grave) key to the users actual keyboard layout */
				(scancode >= SC_1 && scancode <= SC_0)) {
				continue;
			}

			vk = MapVirtualKey(i, MAPVK_VSC_TO_VK);
			if (vk) 
			{
				int ch = (MapVirtualKey(vk, MAPVK_VK_TO_CHAR) & 0x7FFF);
				if (ch) {
					if (ch >= 'A' && ch <= 'Z') {
						keymap[scancode] = KC_a + (ch - 'A');
					}
					else {
						keymap[scancode] = ch;
					}
				}
			}
		}

		IInputSystem::SetKeymap(0, keymap, SC_COUNT);
    }

    void CWindowsApplication::InitializeDevices()
    {
        UpdateKeymap();
		
		IInputSystem::SetScancodeName(SC_LALT, "Left Option");
		IInputSystem::SetScancodeName(SC_LGUI, "Left Command");
		IInputSystem::SetScancodeName(SC_RALT, "Right Option");
		IInputSystem::SetScancodeName(SC_RGUI, "Right Command");
    }
	

    
    void CWindowsApplication::SetMessageHandler(IGenericMessageHandler* InMessageHandler)
    {
        IPlatformApplication::SetMessageHandler(InMessageHandler);
		// 	XInput->SetMessageHandler( InMessageHandler );
    }

    void CWindowsApplication::CloseWindow(CWindowsWindow* Window)
    {
        MessageHandler->OnWindowClose(Window);
    }
    

    bool CWindowsApplication::SetRelativeMouseMode(bool bEnabled)
    {
		HWND hwnd = NULL;
		DWORD flags = RIDEV_REMOVE;
		bRelativeMouseMode = bEnabled;

		if (bEnabled)
		{
			flags = 0;

			//if (InWindow)
			{
				//hwnd = (HWND)InWindow->GetOSWindowHandle();
			}
		}

		// NOTE: Currently has to be created every time due to conflicts with Direct8 Input used by the wx unrealed
		RAWINPUTDEVICE RawInputDevice;

		//The HID standard for mouse
		const uint16 StandardMouse = 0x02;

		RawInputDevice.usUsagePage = 0x01;
		RawInputDevice.usUsage = StandardMouse;
		RawInputDevice.dwFlags = flags;

		// Process input for just the window that requested it.  NOTE: If we pass NULL here events are routed to the window with keyboard focus
		// which is not always known at the HWND level with Slate
		RawInputDevice.hwndTarget = hwnd;

		// Register the raw input device
		::RegisterRawInputDevices(&RawInputDevice, 1, sizeof(RAWINPUTDEVICE));
		return true;
    }
    
    void CWindowsApplication::SetCursorShow(bool bShow)
    {
        Cursor->Show(bShow);
    }
    
    void CWindowsApplication::WarpMouse(int32 X, int32 Y)
    {
        Vec2 origin = CurrentWindow->GetLocation();
        
        Cursor->Wrap(CurrentWindow, origin.x + X, origin.y + Y );
    }
	
	void CWindowsApplication::SetCursorType(Neko::ICursor::EMouseCursorType Type)
	{
		Cursor->SetType(Type);
	}
    
    IGenericWindow* CWindowsApplication::MakeWindow(const FWindowDesc &InDesc)
    {
        CWindowsWindow* window;
        
        window = new CWindowsWindow();
        window->Create(InDesc, this, InstanceHandle, nullptr, false);
        
        Windows.Push(window);
        
        return window;
    }
    
    void CWindowsApplication::DestroyWindow(IGenericWindow* Window)
    {
        if (Window)
        {
            Window->Destroy(); // calls 'OnWindowDestroyed'
        }
        // Don't do anything here.
    }

    bool CWindowsApplication::OnWindowDestroyed(CWindowsWindow* Window)
    {
		Windows.EraseItem(Window);
		return true;
	}
    
    IGenericWindow* CWindowsApplication::GetActiveWindow()
    {
        return CurrentWindow;
    }


	void CWindowsApplication::AddMessageHandler(IWindowsMessageHandler& InMessageHandler)
	{
		WindowsApplication->MessageHandlers.Push(&InMessageHandler);
	}


	void CWindowsApplication::RemoveMessageHandler(IWindowsMessageHandler& InMessageHandler)
	{
		WindowsApplication->MessageHandlers.EraseItem(&InMessageHandler);
	}

	LRESULT CALLBACK CWindowsApplication::AppWndProc(HWND hwnd, uint32 msg, WPARAM wParam, LPARAM lParam)
	{
		//ensure(IsInGameThread());

		return WindowsApplication->ProcessMessage(hwnd, msg, wParam, lParam);
	}

	void CWindowsApplication::QueryConnectedMice()
	{
		
	}


#if WINVER >= 0x0601
	/*uint32 CWindowsApplication::GetTouchIndexForID(int32 TouchID)
	{
		for (int i = 0; i < MaxTouches; i++)
		{
			if (TouchIDs[i].IsSet() && TouchIDs[i] == TouchID)
			{
				return i;
			}
		}
		return -1;
	}

	uint32 CWindowsApplication::GetFirstFreeTouchIndex()
	{
		for (int i = 0; i < MaxTouches; i++)
		{
			if (TouchIDs[i].IsSet() == false)
			{
				return i;
			}
		}
		return -1;
	}*/
#endif

}
#endif
