//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/


#include "../SystemShared.h"

#if NEKO_WINDOWS

#include "../../Log.h"
#include "../Application.h"

#include "WindowsWindow.h"
#include "WindowsCursor.h"

namespace Neko
{
    CWindowsCursor::CWindowsCursor()
    : bRelativeMode(false)
    {
		// Load up cursors that we'll be using
		for (int32 CurCursorIndex = 0; CurCursorIndex < EMouseCursorType::TotalCursorCount; ++CurCursorIndex)
		{
			CursorHandles[CurCursorIndex] = NULL;

			HCURSOR CursorHandle = NULL;
			switch (CurCursorIndex)
			{
			case EMouseCursorType::None:
			case EMouseCursorType::Custom:
				// The mouse cursor will not be visible when None is used
				break;

			case EMouseCursorType::Default:
				CursorHandle = ::LoadCursor(NULL, IDC_ARROW);
				break;

			case EMouseCursorType::TextEditBeam:
				CursorHandle = ::LoadCursor(NULL, IDC_IBEAM);
				break;

			case EMouseCursorType::ResizeLeftRight:
				CursorHandle = ::LoadCursor(NULL, IDC_SIZEWE);
				break;

			case EMouseCursorType::ResizeUpDown:
				CursorHandle = ::LoadCursor(NULL, IDC_SIZENS);
				break;

			/*case EMouseCursorType::ResizeSouthEast:
				CursorHandle = ::LoadCursor(NULL, IDC_SIZENWSE);
				break;

			case EMouseCursorType::ResizeSouthWest:
				CursorHandle = ::LoadCursor(NULL, IDC_SIZENESW);
				break;

			case EMouseCursorType::CardinalCross:
				CursorHandle = ::LoadCursor(NULL, IDC_SIZEALL);
				break;*/

			case EMouseCursorType::Crosshairs:
				CursorHandle = ::LoadCursor(NULL, IDC_CROSS);
				break;

			case EMouseCursorType::Hand:
			case EMouseCursorType::GrabHand:
				CursorHandle = ::LoadCursor(NULL, IDC_HAND);
				break;

				/*
			case EMouseCursorType::GrabHand:
				CursorHandle = LoadCursorFromFile((LPCTSTR)*(FString(FPlatformProcess::BaseDir()) / FString::Printf(TEXT("%sEditor/Slate/Cursor/grabhand.cur"), *FPaths::EngineContentDir())));
				if (CursorHandle == NULL)
				{
					// Failed to load file, fall back
					CursorHandle = ::LoadCursor(NULL, IDC_HAND);
				}
				break;
			case EMouseCursor::GrabHandClosed:
				CursorHandle = LoadCursorFromFile((LPCTSTR)*(FString(FPlatformProcess::BaseDir()) / FString::Printf(TEXT("%sEditor/Slate/Cursor/grabhand_closed.cur"), *FPaths::EngineContentDir())));
				if (CursorHandle == NULL)
				{
					// Failed to load file, fall back
					CursorHandle = ::LoadCursor(NULL, IDC_HAND);
				}
				break;

			case EMouseCursorType::SlashedCircle:
				CursorHandle = ::LoadCursor(NULL, IDC_NO);
				break;

			case EMouseCursorType::EyeDropper:
				CursorHandle = LoadCursorFromFile((LPCTSTR)*(FString(FPlatformProcess::BaseDir()) / FString::Printf(TEXT("%sEditor/Slate/Icons/eyedropper.cur"), *FPaths::EngineContentDir())));
				break;*/

				// NOTE: For custom app cursors, use:
				//		CursorHandle = ::LoadCursor( InstanceHandle, (LPCWSTR)MY_RESOURCE_ID );

			default:
				// Unrecognized cursor type!
				assert(0);
				break;
			}

			CursorHandles[CurCursorIndex] = CursorHandle;
		}

		// Set the default cursor
		SetType(EMouseCursorType::Default);
    }
    
    CWindowsCursor::~CWindowsCursor()
    {
		// Release cursors
		// NOTE: Shared cursors will automatically be destroyed when the application is destroyed.
		//       For dynamically created cursors, use DestroyCursor
		for (int32 CurCursorIndex = 0; CurCursorIndex < EMouseCursorType::TotalCursorCount; ++CurCursorIndex)
		{
			switch (CurCursorIndex)
			{
			case EMouseCursorType::None:
			case EMouseCursorType::Default:
			case EMouseCursorType::TextEditBeam:
			case EMouseCursorType::ResizeLeftRight:
			case EMouseCursorType::ResizeUpDown:
			//case EMouseCursorType::ResizeSouthEast:
			//case EMouseCursorType::ResizeSouthWest:
			//case EMouseCursorType::CardinalCross:
			case EMouseCursorType::Crosshairs:
			case EMouseCursorType::Hand:
			case EMouseCursorType::GrabHand:
			case EMouseCursorType::GrabHandClosed:
			case EMouseCursorType::SlashedCircle:
			//case EMouseCursorType::EyeDropper:
			case EMouseCursorType::Custom:
				// Standard shared cursors don't need to be destroyed
				break;

			default:
				// Unrecognized cursor type!
				assert(0);
				break;
			}
		}
    }
    
    void CWindowsCursor::Show(bool bShow)
    {
		if (bShow)
		{
			// Show mouse cursor. Each time ShowCursor(true) is called an internal value is incremented so we 
			// call ShowCursor until the cursor is actually shown (>= 0 value returned by showcursor)
			while (::ShowCursor(true)<0);
		}
		else
		{		// Disable the cursor.  Wait until its actually disabled.
			while (::ShowCursor(false) >= 0);
		}
    }
    
    bool CWindowsCursor::SetRelativeMode(bool bEnable)
    {

        return true;
    }

    Vec2 CWindowsCursor::GetPosition() const
    {
		POINT CursorPos;
		::GetCursorPos(&CursorPos);

		return Vec2(CursorPos.x, CursorPos.y);
    };
    
    void CWindowsCursor::Wrap(CWindowsWindow* Window, const int32 X, const int32 Y)
    {

    }
    
    void CWindowsCursor::SetType(const EMouseCursorType InNewCursor)
    {
		if (InNewCursor == CurrentType)
		{
			return;
		}
		// NOTE: Watch out for contention with FWindowsViewport::UpdateMouseCursor
		assert(InNewCursor < EMouseCursorType::TotalCursorCount);// , TEXT("Invalid cursor(%d) supplied"), (int)InNewCursor);
		CurrentType = InNewCursor;
		::SetCursor(CursorHandles[InNewCursor]);
    }
    
    void CWindowsCursor::SetPosition(const int32 X, const int32 Y)
    {
		::SetCursorPos(X, Y);
    }

}
#endif

