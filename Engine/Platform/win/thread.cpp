
#include "../../Neko.h"

#if NEKO_WINDOWS_FAMILY

#include "../../Mt/Thread.h"
//#include "engine/win/simple_win.h"

#include <Windows.h>

namespace Neko
{
	namespace MT
	{
		void Sleep(uint32 milliseconds) { ::Sleep(milliseconds); }
#undef Yield
		void YieldCpu() { Sleep(0); }
		

		ThreadID GetCurrentThreadID() { return ::GetCurrentThreadId(); }

		uint32 GetThreadAffinityMask()
		{
			PROCESSOR_NUMBER proc_number;
			BOOL ret = ::GetThreadIdealProcessorEx(::GetCurrentThread(), &proc_number);
			assert(ret);
			return proc_number.Number;
		}

		static const DWORD MS_VC_EXCEPTION = 0x406D1388;

#pragma pack(push,8)
		typedef struct tagTHREADNAME_INFO
		{
			DWORD type;
			LPCSTR name;
			DWORD threadId;
			DWORD flags;
		} THREADNAME_INFO;
#pragma pack(pop)

		void SetThreadName(ThreadID threadId, const char* thread_name)
		{
			THREADNAME_INFO info;
			info.type = 0x1000;
			info.name = thread_name;
			info.threadId = threadId;
			info.flags = 0;

			__try
			{
				RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
			}
			__except (EXCEPTION_EXECUTE_HANDLER)
			{
			}
		}
	} //!namespace MT
} //!namespace Neko
#endif
