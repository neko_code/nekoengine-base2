
#include "../SystemShared.h"

#if NEKO_WINDOWS_FAMILY
#include "../../Mt/Sync.h"
#include "../../Mt/atomic.h"
#include "../../Mt/thread.h"
//#include "engine/win/simple_win.h"

#include <windows.h>

namespace Neko
{
    namespace MT
    {
        Semaphore::Semaphore(int init_count, int max_count)
        {
            Id = ::CreateSemaphore(nullptr, init_count, max_count, nullptr);
        }
        
        Semaphore::~Semaphore()
        {
            ::CloseHandle(Id);
        }
        
        void Semaphore::signal()
        {
            ::ReleaseSemaphore(Id, 1, nullptr);
        }
        
        void Semaphore::Wait()
        {
            ::WaitForSingleObject(Id, INFINITE);
        }
        
        bool Semaphore::poll()
        {
            return WAIT_OBJECT_0 == ::WaitForSingleObject(Id, 0);
        }
        
        
        Event::Event()
        {
            Id = ::CreateEvent(nullptr, FALSE, FALSE, nullptr);
        }
        
        Event::~Event()
        {
            ::CloseHandle(Id);
        }
        
        void Event::Reset()
        {
            ::ResetEvent(Id);
        }
        
        void Event::Trigger()
        {
            ::SetEvent(Id);
        }
        
        void Event::Wait()
        {
            ::WaitForSingleObject(Id, INFINITE);
        }
        
        bool Event::poll()
        {
            return WAIT_OBJECT_0 == ::WaitForSingleObject(Id, 0);
        }
        
        
        SpinMutex::SpinMutex(bool locked)
        : Id(0)
        {
            if (locked)
            {
                Lock();
            }
        }
        
        SpinMutex::~SpinMutex()
        {
        }
        
        void SpinMutex::Lock()
        {
            for (;;)
            {
                if (Atomics::CompareAndExchange(&Id, 1, 0))
                {
					Atomics::MemoryBarrierFence();
                    return;
                }
                
                while (Id)
                {
                    YieldCpu();
                }
            }
        }
        
        bool SpinMutex::poll()
        {
            if (Atomics::CompareAndExchange(&Id, 1, 0))
            {
				Atomics::MemoryBarrierFence();
                return true;
            }
            return false;
        }
        
        void SpinMutex::Unlock()
        {
			Atomics::MemoryBarrierFence();
            Id = 0;
        }
        
        
    } // namespace MT
} // namespace Neko
#endif
