//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  PlatformFile.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../../Neko.h"

#if NEKO_WINDOWS_FAMILY

#include "../../FS/PlatformFile.h"
#include "../../IAllocator.h"
#include "../../StringUtil.h"

namespace Neko
{
    namespace FS
    {
        struct PlatformFileImpl
        {
            explicit PlatformFileImpl(IAllocator& allocator)
            : Allocator(allocator)
            {
            }
            
            
            IAllocator& Allocator;
            FILE* pFile;
        };
        
        CPlatformFile::CPlatformFile()
        {
            pHandle = nullptr;
        }
        
        CPlatformFile::~CPlatformFile()
        {
            assert(!pHandle);
        }
        
        bool CPlatformFile::Open(const char* path, Mode mode, IAllocator& allocator)
        {
            FILE* fp = fopen(path, Mode::WRITE & mode ? "wb+" : "rb+");
            if (fp)
            {
                PlatformFileImpl* impl = NEKO_NEW(allocator, PlatformFileImpl)(allocator);
                impl->pFile = fp;
                pHandle = impl;
                
                return true;
            }
            else
            {
                const char* error = strerror(errno);
                printf("%s: %s\n", path, error);
            }
            return false;
        }
        
        void CPlatformFile::Flush()
        {
//            assert(nullptr != pHandle);
            if (pHandle)
            {
                fflush(pHandle->pFile);
            }
        }
        
        void CPlatformFile::Close()
        {
            if (pHandle)
            {
                fclose(pHandle->pFile);
                NEKO_DELETE(pHandle->Allocator, pHandle);
                pHandle = nullptr;
            }
        }
        
        size_t CPlatformFile::WriteText(const char* text)
        {
            int len = StringLength(text);
            return Write(text, len);
        }
        
        size_t CPlatformFile::Write(const void* data, size_t size)
        {
            assert(nullptr != pHandle);
            size_t written = fwrite(data, size, 1, pHandle->pFile);
            return written;
        }
        
        size_t CPlatformFile::Read(void* data, size_t size)
        {
            assert(nullptr != pHandle);
            size_t read = fread(data, size, 1, pHandle->pFile);
            return read;
        }
        
        size_t CPlatformFile::GetSize()
        {
            assert(nullptr != pHandle);
            long pos = ftell(pHandle->pFile);
            fseek(pHandle->pFile, 0, SEEK_END);
            size_t size = (size_t)ftell(pHandle->pFile);
            fseek(pHandle->pFile, pos, SEEK_SET);
            return size;
        }
        
        bool CPlatformFile::IsEof() const
        {
            return feof(pHandle->pFile);
        }
        
        size_t CPlatformFile::Tell() const
        {
            return ftello64(pHandle->pFile);
        }

        size_t CPlatformFile::Pos()
        {
            assert(nullptr != pHandle);
            long pos = ftell(pHandle->pFile);
            return (size_t)pos;
        }
        
        bool CPlatformFile::Seek(ESeekLocator base, size_t pos)
        {
            assert(nullptr != pHandle);
            int dir = 0;
            switch (base)
            {
                case ESeekLocator::Begin:
                    dir = SEEK_SET;
                    break;
                case ESeekLocator::End:
                    dir = SEEK_END;
                    break;
                case ESeekLocator::Current:
                    dir = SEEK_CUR;
                    break;
            }
            
            return fseeko64(pHandle->pFile, pos, dir) == 0;
        }
        
        CPlatformFile& CPlatformFile::operator <<(const char* text)
        {
            Write(text, StringLength(text));
            return *this;
        }
        
        CPlatformFile& CPlatformFile::operator <<(int32 value)
        {
            char buf[20];
            ToCString(value, buf, lengthOf(buf));
            Write(buf, StringLength(buf));
            return *this;
        }
        
        CPlatformFile& CPlatformFile::operator <<(uint32 value)
        {
            char buf[20];
            ToCString(value, buf, lengthOf(buf));
            Write(buf, StringLength(buf));
            return *this;
        }
        
        CPlatformFile& CPlatformFile::operator <<(uint64 value)
        {
            char buf[30];
            ToCString(value, buf, lengthOf(buf));
            Write(buf, StringLength(buf));
            return *this;
        }
        
        CPlatformFile& CPlatformFile::operator <<(float value)
        {
            char buf[30];
            ToCString(value, buf, lengthOf(buf), 7);
            Write(buf, StringLength(buf));
            return *this;
        }
    } // namespace FS
} // namespace Neko
#endif
