//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/

#include "../SystemShared.h"

#if NEKO_WINDOWS

#include <memory.h>

#include "../../Core/Log.h"
#include "../Application.h"
#include "WindowsWindow.h"
#include "WindowsApplication.h"

namespace Neko
{
	const char CWindowsWindow::AppWindowClass[] = TEXT("UnrealWindow");
	// This is a hard-coded constant for Windows' WS_THICKFRAME
	// There does not seem to be a way to alter this
	static int32 WindowsAeroBorderSize = 8;
	static int32 WindowsStandardBorderSize = 4;


    CWindowsWindow::CWindowsWindow()
    : HWnd(nullptr)
	, WindowMode(ENativeWindowMode::Windowed)
    , bIsVisible(true)
	, DPIScaleFactor(1.0f)
    {
		// PreFullscreenWindowPlacement.length will be set when we save the window placement and then used to check if the structure is valid
		memset(&PreFullscreenWindowPlacement, 0, sizeof(PreFullscreenWindowPlacement));

		memset(&PreParentMinimizedWindowPlacement, 0, sizeof(PreParentMinimizedWindowPlacement));
		PreParentMinimizedWindowPlacement.length = sizeof(WINDOWPLACEMENT);
    }
    
    CWindowsWindow::~CWindowsWindow()
    {
        
    }

	static float GetDPIScaleFactorAtPoint(float X, float Y)
	{
		return 1.0f;
	}


	int32 CWindowsWindow::GetWindowBorderSize() const
	{
		if (/*GetDefinition().Type == EWindowType::GameWindow &&*/ !Definition.HasBorder)
		{
			// Our borderless game windows actually have a thick border to allow sizing, which we draw over to simulate
			// a borderless window. We return zero here so that the game will correctly behave as if this is truly a
			// borderless window.
			return 0;
		}

		WINDOWINFO WindowInfo;
		memset(&WindowInfo, 0, sizeof(WindowInfo));
		WindowInfo.cbSize = sizeof(WindowInfo);
		::GetWindowInfo(HWnd, &WindowInfo);

		return WindowInfo.cxWindowBorders;
	}

	HRGN CWindowsWindow::MakeWindowRegionObject(bool bIncludeBorderWhenMaximized) const
	{
		HRGN Region;
		if (RegionWidth != INDEX_NONE && RegionHeight != INDEX_NONE)
		{
			const bool bIsBorderlessGameWindow = /*GetDefinition().Type == EWindowType::GameWindow &&*/ !Definition.HasBorder;
			if (IsMaximized())
			{
				if (bIsBorderlessGameWindow)
				{
					// Windows caches the cxWindowBorders size at window creation. Even if borders are removed or resized Windows will continue to use this value when evaluating regions
					// and sizing windows. When maximized this means that our window position will be offset from the screen origin by (-cxWindowBorders,-cxWindowBorders). We want to
					// display only the region within the maximized screen area, so offset our upper left and lower right by cxWindowBorders.
					WINDOWINFO WindowInfo;
					memset(&WindowInfo, 0, sizeof(WindowInfo));
					WindowInfo.cbSize = sizeof(WindowInfo);
					::GetWindowInfo(HWnd, &WindowInfo);

					const int32 WindowBorderSize = bIncludeBorderWhenMaximized ? WindowInfo.cxWindowBorders : 0;
					Region = CreateRectRgn(WindowBorderSize, WindowBorderSize, RegionWidth + WindowBorderSize, RegionHeight + WindowBorderSize);
				}
				else
				{
					const int32 WindowBorderSize = bIncludeBorderWhenMaximized ? GetWindowBorderSize() : 0;
					Region = CreateRectRgn(WindowBorderSize, WindowBorderSize, RegionWidth - WindowBorderSize, RegionHeight - WindowBorderSize);
				}
			}
			else
			{
				const bool bUseCornerRadius = WindowMode == ENativeWindowMode::Windowed && !bIsBorderlessGameWindow &&
#if ALPHA_BLENDED_WINDOWS
					// Corner radii cause DWM window composition blending to fail, so we always set regions to full size rectangles
					Definition->TransparencySupport != EWindowTransparency::PerPixel &&
#endif
					Definition.CornerRadius > 0;

				if (bUseCornerRadius)
				{
					// CreateRoundRectRgn gives you a duff region that's 1 pixel smaller than you ask for. CreateRectRgn behaves correctly.
					// This can be verified by uncommenting the assert below
					Region = CreateRoundRectRgn(0, 0, RegionWidth + 1, RegionHeight + 1, Definition.CornerRadius, Definition.CornerRadius);

					// Test that a point that should be in the region, is in the region
					// check(!!PtInRegion(Region, RegionWidth-1, RegionHeight/2));
				}
				else
				{
					Region = CreateRectRgn(0, 0, RegionWidth, RegionHeight);
				}
			}
		}
		else
		{
			RECT rcWnd;
			GetWindowRect(HWnd, &rcWnd);
			Region = CreateRectRgn(0, 0, rcWnd.right - rcWnd.left, rcWnd.bottom - rcWnd.top);
		}

		return Region;
	}

	void CWindowsWindow::AdjustWindowRegion(int32 Width, int32 Height)
	{
		RegionWidth = Width;
		RegionHeight = Height;

		HRGN Region = MakeWindowRegionObject(true);

		// NOTE: We explicitly don't delete the Region object, because the OS deletes the handle when it no longer needed after
		// a call to SetWindowRgn.
		SetWindowRgn(HWnd, Region, false);
	}
    
    void CWindowsWindow::Create(const FWindowDesc& InDefinition, CWindowsApplication* const Application, HINSTANCE InHInstance, const CWindowsWindow* InParent, const bool bShowImmediately)
    {
		Definition = InDefinition;
		//OwningApplication = Application;

		// Finally, let's initialize the new native window object.  Calling this function will often cause OS
		// window messages to be sent! (such as activation messages)
		uint32 WindowExStyle = 0;
		uint32 WindowStyle = 0;

		RegionWidth = RegionHeight = -1;

		const float XInitialRect = Definition.OriginX;
		const float YInitialRect = Definition.OriginY;

		const float WidthInitial = Definition.SizeW;
		const float HeightInitial = Definition.SizeH;

		DPIScaleFactor = GetDPIScaleFactorAtPoint(XInitialRect, YInitialRect);

		int32 ClientX = Math::TruncToInt(XInitialRect);
		int32 ClientY = Math::TruncToInt(YInitialRect);
		int32 ClientWidth = Math::TruncToInt(WidthInitial);
		int32 ClientHeight = Math::TruncToInt(HeightInitial);
		int32 WindowX = ClientX;
		int32 WindowY = ClientY;
		int32 WindowWidth = ClientWidth;
		int32 WindowHeight = ClientHeight;
		const bool bApplicationSupportsPerPixelBlending =
#if ALPHA_BLENDED_WINDOWS
			Application->GetWindowTransparencySupport() == EWindowTransparency::PerPixel;
#else
			false;
#endif

		if (!Definition.HasBorder)
		{
			WindowExStyle = WS_EX_WINDOWEDGE;

			//if (Definition->TransparencySupport == EWindowTransparency::PerWindow)
			//{
			//	WindowExStyle |= WS_EX_LAYERED;
			//}
#if ALPHA_BLENDED_WINDOWS
			else if (Definition->TransparencySupport == EWindowTransparency::PerPixel)
			{
				if (bApplicationSupportsPerPixelBlending)
				{
					WindowExStyle |= WS_EX_COMPOSITED;
				}
			}
#endif

			WindowStyle = WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
			//if (Definition->AppearsInTaskbar)
			//{
				WindowExStyle |= WS_EX_APPWINDOW;
			//}
			//else
			//{
			//	WindowExStyle |= WS_EX_TOOLWINDOW;
			//}

			if (Definition.IsTopmostWindow)
			{
				// Tool tips are always top most windows
				WindowExStyle |= WS_EX_TOPMOST;
			}

			if (!Definition.AcceptsInput)
			{
				// Window should never get input
				WindowExStyle |= WS_EX_TRANSPARENT;
			}
		}
		else
		{
			// OS Window border setup
			WindowExStyle = WS_EX_APPWINDOW;
			WindowStyle = WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION;

			if (IsRegularWindow())
			{
				if (Definition.SupportsMaximisation)
				{
					WindowStyle |= WS_MAXIMIZEBOX;
				}

				if (Definition.SupportsMinimisation)
				{
					WindowStyle |= WS_MINIMIZEBOX;
				}

				if (Definition.HasSizingFrame)
				{
					WindowStyle |= WS_THICKFRAME;
				}
				else
				{
					WindowStyle |= WS_BORDER;
				}
			}
			else
			{
				WindowStyle |= WS_POPUP | WS_BORDER;
			}

			// X,Y, Width, Height defines the top-left pixel of the client area on the screen
			// This adjusts a zero rect to give us the size of the border
			RECT BorderRect = { 0, 0, 0, 0 };
			::AdjustWindowRectEx(&BorderRect, WindowStyle, false, WindowExStyle);

			// Border rect size is negative - see MoveWindowTo
			WindowX += BorderRect.left;
			WindowY += BorderRect.top;

			// Inflate the window size by the OS border
			WindowWidth += BorderRect.right - BorderRect.left;
			WindowHeight += BorderRect.bottom - BorderRect.top;
		}

		// Creating the Window
		HWnd = CreateWindowEx(
			WindowExStyle,
			AppWindowClass,
			Definition.Title.c_str(),
			WindowStyle,
			WindowX, WindowY,
			WindowWidth, WindowHeight,
			(InParent != nullptr) ? static_cast<HWND>(InParent->HWnd) : NULL,
			NULL, InHInstance, NULL);

#if WINVER >= 0x0601
		if (RegisterTouchWindow(HWnd, 0) == false)
		{
			uint32 Error = GetLastError();
			//printf("Register touch input failed!");
		}
#endif

		VirtualWidth = ClientWidth;
		VirtualHeight = ClientHeight;

		// We call reshape window here because we didn't take into account the non-client area
		// in the initial creation of the window. Slate should only pass client area dimensions.
		// Reshape window may resize the window if the non-client area is encroaching on our
		// desired client area space.
		ReshapeWindow(ClientX, ClientY, ClientWidth, ClientHeight);

		if (HWnd == NULL)
		{
			//FSlowHeartBeatScope SuspendHeartBeat;

			// @todo Error message should be localized!
			MessageBox(NULL, TEXT("Window Creation Failed!"), TEXT("Error!"), MB_ICONEXCLAMATION | MB_OK);
			//checkf(0, TEXT("Window Creation Failed (%d)"), ::GetLastError());
			return;
		}

		//if (Definition->TransparencySupport == EWindowTransparency::PerWindow)
		//{
		//	SetOpacity(Definition->Opacity);
		//}
		/*
#if WINVER > 0x502	// Windows Vista or better required for DWM
		// Disable DWM Rendering and Nonclient Area painting if not showing the os window border
		// This prevents the standard windows frame from ever being drawn
		if (!Definition.HasBorder)
		{
			const DWMNCRENDERINGPOLICY RenderingPolicy = DWMNCRP_DISABLED;
			verify(SUCCEEDED(DwmSetWindowAttribute(HWnd, DWMWA_NCRENDERING_POLICY, &RenderingPolicy, sizeof(RenderingPolicy))));

			const BOOL bEnableNCPaint = false;
			verify(SUCCEEDED(DwmSetWindowAttribute(HWnd, DWMWA_ALLOW_NCPAINT, &bEnableNCPaint, sizeof(bEnableNCPaint))));

#if ALPHA_BLENDED_WINDOWS
			if (bApplicationSupportsPerPixelBlending && Definition->TransparencySupport == EWindowTransparency::PerPixel)
			{
				MARGINS Margins = { -1 };
				verify(SUCCEEDED(::DwmExtendFrameIntoClientArea(HWnd, &Margins)));
			}
#endif
		}

#endif	// WINVER 
		*/

		// No region for non regular windows or windows displaying the os window border
		if (IsRegularWindow() && !Definition.HasBorder)
		{
			WindowStyle |= WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;
			if (Definition.SupportsMaximisation)
			{
				WindowStyle |= WS_MAXIMIZEBOX;
			}
			if (Definition.SupportsMinimisation)
			{
				WindowStyle |= WS_MINIMIZEBOX;
			}
			if (Definition.HasSizingFrame)
			{
				WindowStyle |= WS_THICKFRAME;
			}

			SetWindowLong(HWnd, GWL_STYLE, WindowStyle);

			uint32 SetWindowPositionFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED;

			if (!Definition.ActivateWhenFirstShown)
			{
				SetWindowPositionFlags |= SWP_NOACTIVATE;
			}

			::SetWindowPos(HWnd, nullptr, 0, 0, 0, 0, SetWindowPositionFlags);

			AdjustWindowRegion(ClientWidth, ClientHeight);
		}

		if (IsRegularWindow())
		{
			// Tell OLE that we are opting into drag and drop.
			// Only makes sense for regular windows (windows that last a while.)
			//RegisterDragDrop(HWnd, this);
		}
	}
	
    void CWindowsWindow::Maximize()
    {
		::ShowWindow(HWnd, SW_MAXIMIZE);
    }
	
    void CWindowsWindow::BringToFront(bool bForce)
    {
		if (IsRegularWindow())
		{
			if (::IsIconic(HWnd))
			{
				::ShowWindow(HWnd, SW_RESTORE);
			}
			else
			{
				::SetActiveWindow(HWnd);
			}
		}
		else
		{
			HWND HWndInsertAfter = HWND_TOP;
			// By default we activate the window or it isn't actually brought to the front 
			uint32 Flags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER;

			if (!bForce)
			{
				Flags |= SWP_NOACTIVATE;
			}

			if (Definition.IsTopmostWindow)
			{
				HWndInsertAfter = HWND_TOPMOST;
			}

			::SetWindowPos(HWnd, HWndInsertAfter, 0, 0, 0, 0, Flags);
		}
    }

    void CWindowsWindow::ReshapeWindow(int32 NewX, int32 NewY, int32 NewWidth, int32 NewHeight)
    {
		WINDOWINFO WindowInfo;
		memset(&WindowInfo, 0, sizeof(WindowInfo));
		WindowInfo.cbSize = sizeof(WindowInfo);
		::GetWindowInfo(HWnd, &WindowInfo);

		AspectRatio = (float)NewWidth / (float)NewHeight;

		// X,Y, Width, Height defines the top-left pixel of the client area on the screen
		if (Definition.HasBorder)
		{
			// This adjusts a zero rect to give us the size of the border
			RECT BorderRect = { 0, 0, 0, 0 };
			::AdjustWindowRectEx(&BorderRect, WindowInfo.dwStyle, false, WindowInfo.dwExStyle);

			// Border rect size is negative - see MoveWindowTo
			NewX += BorderRect.left;
			NewY += BorderRect.top;

			// Inflate the window size by the OS border
			NewWidth += BorderRect.right - BorderRect.left;
			NewHeight += BorderRect.bottom - BorderRect.top;
		}

		// the window position is the requested position
		int32 WindowX = NewX;
		int32 WindowY = NewY;

		// If the window size changes often, only grow the window, never shrink it
		const bool bVirtualSizeChanged = NewWidth != VirtualWidth || NewHeight != VirtualHeight;
		VirtualWidth = NewWidth;
		VirtualHeight = NewHeight;

	/*	if (Definition->SizeWillChangeOften)
		{
			// When SizeWillChangeOften is set, we set a minimum width and height window size that we'll keep allocated
			// even when the requested actual window size is smaller.  This just avoids constantly resizing the window
			// and associated GPU buffers, which can be very slow on some platforms.

			const RECT OldWindowRect = WindowInfo.rcWindow;
			const int32 OldWidth = OldWindowRect.right - OldWindowRect.left;
			const int32 OldHeight = OldWindowRect.bottom - OldWindowRect.top;

			const int32 MinRetainedWidth = Definition->ExpectedMaxWidth != INDEX_NONE ? Definition->ExpectedMaxWidth : OldWidth;
			const int32 MinRetainedHeight = Definition->ExpectedMaxHeight != INDEX_NONE ? Definition->ExpectedMaxHeight : OldHeight;

			NewWidth = FMath::Max(NewWidth, FMath::Min(OldWidth, MinRetainedWidth));
			NewHeight = FMath::Max(NewHeight, FMath::Min(OldHeight, MinRetainedHeight));
		}*/

		if (IsMaximized())
		{
			Restore();
		}

		// We use SWP_NOSENDCHANGING when in fullscreen mode to prevent Windows limiting our window size to the current resolution, as that 
		// prevents us being able to change to a higher resolution while in fullscreen mode
		::SetWindowPos(HWnd, nullptr, WindowX, WindowY, NewWidth, NewHeight, SWP_NOZORDER | SWP_NOACTIVATE | ((WindowMode == ENativeWindowMode::Fullscreen) ? SWP_NOSENDCHANGING : 0));

		CurrentX = WindowX;
		CurrentY = WindowY;
		//if (Definition->SizeWillChangeOften && bVirtualSizeChanged)
		//{
		//	AdjustWindowRegion(VirtualWidth, VirtualHeight);
		//}
    }
    
    void CWindowsWindow::MoveWindowTo(int32 X, int32 Y)
    {
		// Slate gives the window position as relative to the client area of a window, so we may need to compensate for the OS border
		if (Definition.HasBorder)
		{
			const LONG WindowStyle = ::GetWindowLong(HWnd, GWL_STYLE);
			const LONG WindowExStyle = ::GetWindowLong(HWnd, GWL_EXSTYLE);

			// This adjusts a zero rect to give us the size of the border
			RECT BorderRect = { 0, 0, 0, 0 };
			::AdjustWindowRectEx(&BorderRect, WindowStyle, false, WindowExStyle);

			// Border rect size is negative
			X += BorderRect.left;
			Y += BorderRect.top;
		}

		::SetWindowPos(HWnd, nullptr, X, Y, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
    }
    
    Int2 CWindowsWindow::GetSize()
    {
		return { VirtualWidth, VirtualHeight };
    }
    
    Vec2 CWindowsWindow::GetLocation()
    {
        Vec2 result;
		result.x = CurrentX;
		result.y = CurrentY;

        return result;
    }
    
    void CWindowsWindow::SetTitle(const char* Title)
    {
		SetWindowText(HWnd, Title);
    }
    
    bool CWindowsWindow::IsRegularWindow() const
    {
        return Definition.IsRegularWindow;
    }
    
    float CWindowsWindow::GetDPIScaleFactor() const
    {
       return DPIScaleFactor;
    }
    
    void CWindowsWindow::Show()
    {
		if (!bIsVisible)
		{
			bIsVisible = true;

			// Do not activate windows that do not take input; e.g. tool-tips and cursor decorators
			// Also dont activate if a window wants to appear but not activate itself
			const bool bShouldActivate = Definition.AcceptsInput && Definition.ActivateWhenFirstShown;
			::ShowWindow(HWnd, bShouldActivate ? SW_SHOW : SW_SHOWNOACTIVATE);

			// Turns out SW_SHOWNA doesn't work correctly if the window has never been shown before.  If the window
			// was already maximized, (and hidden) and we're showing it again, SW_SHOWNA would be right.  But it's not right
			// to use SW_SHOWNA when the window has never been shown before!
			// 
			// TODO Add in a more complicated path that involves SW_SHOWNA if we hide windows in their maximized/minimized state.
			//::ShowWindow(HWnd, bShouldActivate ? SW_SHOW : SW_SHOWNA);
		}
    }
    
    void CWindowsWindow::Hide()
    {
		if (bIsVisible)
		{
			bIsVisible = false;
			::ShowWindow(HWnd, SW_HIDE);
		}
    }
    
    void CWindowsWindow::SetWindowMode(ENativeWindowMode NewWindowMode)
    {
		if (NewWindowMode != WindowMode)
		{
			ENativeWindowMode PreviousWindowMode = WindowMode;
			WindowMode = NewWindowMode;

			const bool bTrueFullscreen = NewWindowMode == ENativeWindowMode::Fullscreen;

			// Setup Win32 Flags to be used for Fullscreen mode
			LONG WindowStyle = GetWindowLong(HWnd, GWL_STYLE);
			const LONG FullscreenModeStyle = WS_POPUP;

			LONG WindowedModeStyle = WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION;
			if (IsRegularWindow())
			{
				if (Definition.SupportsMaximisation)
				{
					WindowedModeStyle |= WS_MAXIMIZEBOX;
				}

				if (Definition.SupportsMinimisation)
				{
					WindowedModeStyle |= WS_MINIMIZEBOX;
				}

				if (Definition.HasSizingFrame)
				{
					WindowedModeStyle |= WS_THICKFRAME;
				}
				else
				{
					WindowedModeStyle |= WS_BORDER;
				}
			}
			else
			{
				WindowedModeStyle |= WS_POPUP | WS_BORDER;
			}

			// If we're not in fullscreen, make it so
			if (NewWindowMode == ENativeWindowMode::WindowedFullscreen || NewWindowMode == ENativeWindowMode::Fullscreen)
			{
				if (PreviousWindowMode == ENativeWindowMode::Windowed)
				{
					PreFullscreenWindowPlacement.length = sizeof(WINDOWPLACEMENT);
					::GetWindowPlacement(HWnd, &PreFullscreenWindowPlacement);
				}

				// Setup Win32 flags for fullscreen window
				WindowStyle &= ~WindowedModeStyle;
				WindowStyle |= FullscreenModeStyle;

				SetWindowLong(HWnd, GWL_STYLE, WindowStyle);
				::SetWindowPos(HWnd, nullptr, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);

				if (!bTrueFullscreen)
				{
					// Ensure the window is restored if we are going for WindowedFullscreen
					ShowWindow(HWnd, SW_RESTORE);
				}

				// Get the current window position.
				RECT ClientRect;
				GetClientRect(HWnd, &ClientRect);

				// Grab current monitor data for sizing
				HMONITOR Monitor = MonitorFromWindow(HWnd, bTrueFullscreen ? MONITOR_DEFAULTTOPRIMARY : MONITOR_DEFAULTTONEAREST);
				MONITORINFO MonitorInfo;
				MonitorInfo.cbSize = sizeof(MONITORINFO);
				GetMonitorInfo(Monitor, &MonitorInfo);

				// Get the target client width to send to ReshapeWindow.
				// Preserve the current res if going to true fullscreen and the monitor supports it and allow the calling code
				// to resize if required.
				// Else, use the monitor's res for windowed fullscreen.
				LONG MonitorWidth = MonitorInfo.rcMonitor.right - MonitorInfo.rcMonitor.left;
				LONG TargetClientWidth = bTrueFullscreen ?
					Math::minimum(MonitorWidth, ClientRect.right - ClientRect.left) :
					MonitorWidth;

				LONG MonitorHeight = MonitorInfo.rcMonitor.bottom - MonitorInfo.rcMonitor.top;
				LONG TargetClientHeight = bTrueFullscreen ?
					Math::minimum(MonitorHeight, ClientRect.bottom - ClientRect.top) :
					MonitorHeight;


				// Resize and position fullscreen window
				ReshapeWindow(
					MonitorInfo.rcMonitor.left,
					MonitorInfo.rcMonitor.top,
					TargetClientWidth,
					TargetClientHeight);
			}
			else
			{
				// Windowed:

				// Setup Win32 flags for restored window
				WindowStyle &= ~FullscreenModeStyle;
				WindowStyle |= WindowedModeStyle;
				SetWindowLong(HWnd, GWL_STYLE, WindowStyle);
				::SetWindowPos(HWnd, nullptr, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);

				if (PreFullscreenWindowPlacement.length) // Was PreFullscreenWindowPlacement initialized?
				{
					::SetWindowPlacement(HWnd, &PreFullscreenWindowPlacement);
				}
			}
		}
    }

    ENativeWindowMode CWindowsWindow::GetWindowMode() const
    {
        return WindowMode;
    }
    
    bool CWindowsWindow::IsMaximized() const
    {
        return !!::IsZoomed(HWnd);
    }
    
    bool CWindowsWindow::IsMinimized() const
    {
        return !!::IsIconic(HWnd);
    }
    
    bool CWindowsWindow::IsVisible() const
    {
        return bIsVisible;
    }
    
    void CWindowsWindow::Destroy()
    {
		//if (OLEReferenceCount > 0 && IsWindow(HWnd))
		//{
		//	HRESULT Result = RevokeDragDrop(HWnd);
		//	// If we decremented OLEReferenceCount check it for being null (shutdown)
		//	if (Result == S_OK)
		//	{
		//		checkf(OLEReferenceCount == 0, TEXT("Not all references to window are released, %i left"), OLEReferenceCount);
		//	}
		//}

		::DestroyWindow(HWnd);
    }
}
#endif
