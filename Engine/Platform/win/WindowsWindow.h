//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/


#pragma once

#include "../SystemShared.h"
#include "../IGenericWindow.h"

#if NEKO_WINDOWS

#include <windows.h>

namespace Neko
{
	class CWindowsApplication;
    /// Implementation for Windows window.
    class CWindowsWindow final : public IGenericWindow
    {
    public:
        
        CWindowsWindow();
        
        ~CWindowsWindow();
        
        virtual void ReshapeWindow(int32 X, int32 Y, int32 Width, int32 Height) override;
        virtual void Maximize() override;
        virtual void BringToFront(bool bForce) override;
        virtual void MoveWindowTo(int32 X, int32 Y) override;
        virtual Int2 GetSize() override;
        virtual Vec2 GetLocation() override;
        virtual void SetTitle(const char* Title) override;
        virtual void* GetNativeHandle() const override { return (void*)HWnd; }
        
        virtual ENativeWindowMode GetWindowMode() const override;
        virtual void SetWindowMode(ENativeWindowMode NewWindowMode) override;
        virtual bool IsMaximized() const override;
        virtual bool IsMinimized() const override;
        virtual bool IsVisible() const override;
        virtual float GetDPIScaleFactor() const override;

		void SetDPIScaleFactor(float Value)
		{
			DPIScaleFactor = Value;
		}

		void AdjustWindowRegion(int32 Width, int32 Height);
		HRGN MakeWindowRegionObject(bool bIncludeBorderWhenMaximized) const;
		int32 GetWindowBorderSize() const;

		float GetAspectRatio() const
		{
			return AspectRatio;
		}

        virtual bool IsRegularWindow() const override;

        virtual void Show() override;
        virtual void Hide() override;
        
        virtual void Destroy() override;

		/** Win32 requirement: see CreateWindowEx and RegisterClassEx. */
		static const char AppWindowClass[];


    public:
        void Create(const FWindowDesc& InDefinition, CWindowsApplication* const Application, HINSTANCE InHInstance, const CWindowsWindow* InParent, const bool bShowImmediately);
        
        NEKO_FORCE_INLINE void* GetNativeHandle()
        {
            return HWnd;
        }
        
    private:

        bool bIsClosed;
        bool bIsVisible;
     
    public:
        
		/** The application that owns this window. */
		CWindowsApplication* OwningApplication;

		/** The window's handle. */
		HWND HWnd;

		/** Store the window region size for querying whether a point lies within the window. */
		int32 RegionWidth;
		int32 RegionHeight;

		/** The mode that the window is in (windowed, fullscreen, windowedfullscreen ) */
		ENativeWindowMode WindowMode;

		/** This object's reference count (for the IUnknown interface). */
		int32 OLEReferenceCount;

		/** The placement of the window before it entered a fullscreen state. */
		WINDOWPLACEMENT PreFullscreenWindowPlacement;

		/** The placement of the window before it entered a minimized state due to its parent window being minimized. */
		WINDOWPLACEMENT PreParentMinimizedWindowPlacement;

		/** Virtual width and height of the window.  This is only different than the actual width and height for
		windows which we're trying to optimize because their size changes frequently.  We'll create a larger
		window and have Windows draw it "cropped" so that it appears smaller, rather than actually resizing
		it and incurring a GPU buffer resize performance hit */
		int32 VirtualWidth;
		int32 VirtualHeight;

		int32 CurrentX;
		int32 CurrentY;

		/** Current aspect ratio of window's client area */
		float AspectRatio;

		/**
		* Ratio of pixels to SlateUnits in this window.
		* E.g. DPIScale of 2.0 means there is a 2x2 pixel square for every 1x1 SlateUnit.
		*/
		float DPIScaleFactor;
    };
}

#endif
