//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  FileSystemWatcher.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../Platform.h"

#if NEKO_WINDOWS_FAMILY

#include "../../Log.h"
#include "../../Profiler.h"
#include "../FileSystemWatcher.h"
#include "../../Mt/Task.h"

#include <windows.h>

static const DWORD READ_DIR_CHANGE_FILTER =
FILE_NOTIFY_CHANGE_SECURITY | FILE_NOTIFY_CHANGE_CREATION | FILE_NOTIFY_CHANGE_LAST_ACCESS |
FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_SIZE | FILE_NOTIFY_CHANGE_ATTRIBUTES |
FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_FILE_NAME;



struct FileSystemWatcherImpl;

/// Implementation for file watcher.
struct FileSystemWatcherImpl final : public IFileSystemWatcher
{


	static void wcharToCharArray(const WCHAR* src, char* dest, int len)
	{
		for (unsigned int i = 0; i < len / sizeof(WCHAR); ++i)
		{
			dest[i] = static_cast<char>(src[i]);
		}
		dest[len / sizeof(WCHAR)] = '\0';
	}


	static void CALLBACK notif(DWORD status, DWORD tferred, LPOVERLAPPED over)
	{
		auto* task = (FileSystemTask*)over->hEvent;
		if (status == ERROR_OPERATION_ABORTED)
		{
			task->bRunning = false;
			return;
		}

		FILE_NOTIFY_INFORMATION* info = &task->m_info[0];
		while (info)
		{
			auto action = info->Action;

			FFileChangeData::EFileChangeAction Action;

			switch (action)
			{
			case FILE_ACTION_ADDED:
			case FILE_ACTION_RENAMED_NEW_NAME:
				Action = FFileChangeData::FCA_Added;
				break;

			case FILE_ACTION_REMOVED:
			case FILE_ACTION_RENAMED_OLD_NAME:
				Action = FFileChangeData::FCA_Removed;
				break;

			case FILE_ACTION_MODIFIED:
				Action = FFileChangeData::FCA_Modified;
				break;

			default:
				Action = FFileChangeData::FCA_Unknown;
				break;
			}

			char tmp[MAX_PATH];
			wcharToCharArray(info->FileName, tmp, info->FileNameLength);
			task->Watcher.callback.Invoke({ tmp, Action });

			info = info->NextEntryOffset == 0 ? nullptr : (FILE_NOTIFY_INFORMATION*)(((char*)info) + info->NextEntryOffset);
		}
	}


	struct FileSystemTask final : public Neko::MT::Task
	{
	public:

		FileSystemTask(const char* Path, const uint32 Flags, FileSystemWatcherImpl& Owner, Neko::IAllocator& Allocator)
			: Watcher(Owner)
			, bRunning(false)
			, Task(Allocator)
		{
			bIncludeDirectoryEvents = (Flags & FFileChangeData::WatchOptions::IncludeDirectoryChanges) != 0;
			bIgnoreChangesInSubtree = (Flags & FFileChangeData::WatchOptions::IgnoreChangesInSubtree) != 0;

			Neko::CopyString(WatchPath, Path);
			int32 len = Neko::StringLength(Path);
			if (len > 0 && Path[len - 1] != '/')
			{
				Neko::CatString(WatchPath, "/");
			}
		}
		
		int32 DoTask()
		{
			return 1;
			m_handle = CreateFile(WatchPath,
				FILE_LIST_DIRECTORY,
				FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
				nullptr,
				OPEN_EXISTING,
				FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED,
				nullptr);
			if (m_handle == INVALID_HANDLE_VALUE) return -1;

			Neko::SetMemory(&m_overlapped, 0, sizeof(m_overlapped));
			m_overlapped.hEvent = this;
			bRunning = true;
			while (bRunning)
			{
				PROFILE_SECTION("Change handling");
				BOOL status = ReadDirectoryChangesW(m_handle,
					m_info,
					sizeof(m_info),
					TRUE,
					READ_DIR_CHANGE_FILTER,
					&m_received,
					&m_overlapped,
					&notif);
				if (status == FALSE) break;
				SleepEx(INFINITE, TRUE);
			}
			return 0;
		}

		bool bRunning;

		bool bIncludeDirectoryEvents;
		bool bIgnoreChangesInSubtree;

		char WatchPath[Neko::MAX_PATH_LENGTH];

		FILE_NOTIFY_INFORMATION m_info[10];
		HANDLE m_handle;
		DWORD m_received;
		OVERLAPPED m_overlapped;

		FileSystemWatcherImpl& Watcher;
	};

    explicit FileSystemWatcherImpl(Neko::IAllocator& Allocator_)
    : Allocator(Allocator_)
    {
		WatcherTask = nullptr;
    }
    
    ~FileSystemWatcherImpl()
    {
		if (WatcherTask)
		{
			CancelIoEx(WatcherTask->m_handle, nullptr);
			CloseHandle(WatcherTask->m_handle);

			WatcherTask->Destroy();
			NEKO_DELETE(Allocator, WatcherTask);
		}
    }

	bool Create(const LPCSTR Path, const uint32 Flags)
	{
		WatcherTask = NEKO_NEW(Allocator, FileSystemTask)(Path, Flags, *this, Allocator);
		if (!WatcherTask->Create("FileSystemWatcher"))
		{
			NEKO_DELETE(Allocator, WatcherTask);
			WatcherTask = nullptr;
			return false;
		}
		return true;
	}

    virtual Neko::TDelegate<void(const FFileChangeData& Data)>& GetCallback() { return callback; }

    Neko::IAllocator& Allocator;
    
    Neko::TDelegate<void(const FFileChangeData& Data)> callback;

	FileSystemTask* WatcherTask;
};


IFileSystemWatcher* IFileSystemWatcher::Create(const char* path, uint32 Flags, Neko::IAllocator& allocator)
{
    auto* watcher = NEKO_NEW(allocator, FileSystemWatcherImpl)(allocator);
    if (!watcher->Create(path, Flags))
    {
        NEKO_DELETE(allocator, watcher);
        return nullptr;
    }
    return watcher;
}


void IFileSystemWatcher::Destroy(IFileSystemWatcher* watcher)
{
    if (!watcher)
    {
        return;
    }
    auto* impl_watcher = (FileSystemWatcherImpl*)watcher;
    NEKO_DELETE(impl_watcher->Allocator, impl_watcher);
}


#endif
