//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/

#pragma once

#include "../SystemShared.h"
#include "../InputInterfaces.h"
#include "../../Vec.h"

#if NEKO_WINDOWS

namespace Neko
{
    /// Implementation for cursor on Windows.
    class CWindowsCursor final : public ICursor
    {
    public:
        
        CWindowsCursor();
        
        virtual ~CWindowsCursor();
        
        virtual Vec2 GetPosition() const override;
        virtual void SetPosition(const int32 X, const int32 Y) override;
        
        virtual void SetType(EMouseCursorType Type) override;
        virtual EMouseCursorType GetType() const override
		{
			return CurrentType;
		}
        
        virtual void Show(bool bShow) override;
        
    public:
        bool SetRelativeMode(bool bEnable);
        
        bool IsRelativeMouseMode() { return bRelativeMode; }
        
        void Wrap(CWindowsWindow* Window, const int32 X, const int32 Y);
        
    private:

        EMouseCursorType CurrentType;
        
		/** Cursors */
		HCURSOR CursorHandles[EMouseCursorType::TotalCursorCount];

        bool bRelativeMode; // high precision
    };
}

#endif
