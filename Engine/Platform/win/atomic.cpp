
#include "../../Mt/Atomic.h"

#if NEKO_WINDOWS_FAMILY
#include <intrin.h>
#include <windows.h>

namespace Neko
{
    namespace Atomics
    {
        int32 Increment(int32 volatile* value)
        {
            return _InterlockedIncrement((volatile long*)value);
        }
        
        int32 Decrement(int32 volatile* value)
        {
            return _InterlockedDecrement((volatile long*)value);
        }
        
        int32 Add(int32 volatile* addend, int32 value)
        {
            return _InterlockedExchangeAdd((volatile long*)addend, value);
        }
        
        int32 Subtract(int32 volatile* addend, int32 value)
        {
            return _InterlockedExchangeAdd((volatile long*)addend, -value);
        }
        
        bool CompareAndExchange(int32 volatile* dest, int32 exchange, int32 comperand)
        {
            return _InterlockedCompareExchange((volatile long*)dest, exchange, comperand) == comperand;
        }
        
        bool CompareAndExchange64(int64 volatile* dest, int64 exchange, int64 comperand)
        {
            return _InterlockedCompareExchange64(dest, exchange, comperand) == comperand;
        }
        
		void* InterlockedExchangePtr(void** Dest, void* Exchange)
		{
			return InterlockedExchangePointer(Dest, Exchange);
		}
        
#undef MemoryBarrier
        void MemoryBarrierFence()
        {
#ifdef _M_AMD64
            __faststorefence();
#elif defined _IA64_
            __mf();
#else
            int Barrier;
            __asm {
                xchg Barrier, eax
            }
#endif
        }
        
        
    } // ~namespace MT
} // ~namespace Neko
#endif
