

#include "../../Neko.h"
#if NEKO_WINDOWS_FAMILY

#include "../Platform.h"
#include "../../Debug.h"
#include "../../Mt/Atomic.h"
#include "../../Text.h"
#include <windows.h>
#include <DbgHelp.h>
#include <mapi.h>
#include <cstdlib>
#include <cstdio>


#pragma comment(lib, "DbgHelp.lib")


static bool g_is_crash_reporting_enabled = true;


namespace Neko
{


namespace Debug
{


void DebugOutput(const char* message)
{
	OutputDebugString(message);
}


void DebugBreak()
{
	::DebugBreak();
}


int StackTree::Instances = 0;


class StackNode
{
public:
	~StackNode()
	{
		delete Next;
		delete m_first_child;
	}

	void* Instruction;
	StackNode* Next;
	StackNode* m_first_child;
	StackNode* m_parent;
};


StackTree::StackTree()
{
	Root = nullptr;
	if (Atomics::Increment(&Instances) == 1)
	{
		HANDLE process = GetCurrentProcess();
		SymInitialize(process, nullptr, TRUE);
	}
}


StackTree::~StackTree()
{
	delete Root;
	if (Atomics::Decrement(&Instances) == 0)
	{
		HANDLE process = GetCurrentProcess();
		SymCleanup(process);
	}
}


void StackTree::RefreshModuleList()
{
	//assert(Instances > 0);
	SymRefreshModuleList(GetCurrentProcess());
}


int StackTree::GetPath(StackNode* node, StackNode** output, int MaxSize)
{
	int i = 0;
	while (i < MaxSize && node)
	{
		output[i] = node;
		i++;
		node = node->m_parent;
	}
	return i;
}


StackNode* StackTree::GetParent(StackNode* node)
{
	return node ? node->m_parent : nullptr;
}


bool StackTree::GetFunction(StackNode* node, char* out, int MaxSize, int* line)
{
	HANDLE process = GetCurrentProcess();
	uint8 symbol_mem[sizeof(SYMBOL_INFO) + 256 * sizeof(char)] = {};
	SYMBOL_INFO* symbol = reinterpret_cast<SYMBOL_INFO*>(symbol_mem);
	symbol->MaxNameLen = 255;
	symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
	BOOL success = SymFromAddr(process, (DWORD64)(node->Instruction), 0, symbol);
	IMAGEHLP_LINE64 line_info;
	DWORD displacement;
	if (SymGetLineFromAddr64(process, (DWORD64)(node->Instruction), &displacement, &line_info))
	{
		*line = line_info.LineNumber;
	}
	else
	{
		*line = -1;
	}
	if (success) Neko::CopyString(out, MaxSize, symbol->Name);

	return success == TRUE;
}


void StackTree::PrintCallstack(StackNode* node)
{
	while (node)
	{
		HANDLE process = GetCurrentProcess();
		uint8 symbol_mem[sizeof(SYMBOL_INFO) + 256 * sizeof(char)];
		SYMBOL_INFO* symbol = reinterpret_cast<SYMBOL_INFO*>(symbol_mem);
		memset(symbol_mem, 0, sizeof(symbol_mem));
		symbol->MaxNameLen = 255;
		symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
		BOOL success = SymFromAddr(process, (DWORD64)(node->Instruction), 0, symbol);
		if (success)
		{
			IMAGEHLP_LINE line;
			DWORD offset;
			if (SymGetLineFromAddr(process, (DWORD64)(node->Instruction), &offset, &line))
			{
				OutputDebugString("\t");
				OutputDebugString(line.FileName);
				OutputDebugString("(");
				char tmp[20];
				ToCString((uint32)line.LineNumber, tmp, sizeof(tmp));
				OutputDebugString(tmp);
				OutputDebugString("):");
			}
			OutputDebugString("\t");
			OutputDebugString(symbol->Name);
			OutputDebugString("\n");
		}
		else
		{
			OutputDebugString("\tN/A\n");
		}
		node = node->m_parent;
	}
}


StackNode* StackTree::InsertChildren(StackNode* root_node, void** instruction, void** stack)
{
	StackNode* node = root_node;
	while (instruction >= stack)
	{
		StackNode* newNode = new StackNode();
		node->m_first_child = newNode;
		newNode->m_parent = node;
		newNode->Next = nullptr;
		newNode->m_first_child = nullptr;
		newNode->Instruction = *instruction;
		node = newNode;
		--instruction;
	}
	return node;
}


StackNode* StackTree::Record()
{
	static const int frames_to_capture = 256;
	void* stack[frames_to_capture];
	USHORT captured_frames_count = CaptureStackBackTrace(2, frames_to_capture, stack, 0);

	void** ptr = stack + captured_frames_count - 1;
	if (!Root)
	{
		Root = new StackNode();
		Root->Instruction = *ptr;
		Root->m_first_child = nullptr;
		Root->Next = nullptr;
		Root->m_parent = nullptr;
		--ptr;
		return InsertChildren(Root, ptr, stack);
	}

	StackNode* node = Root;
	while (ptr >= stack)
	{
		while (node->Instruction != *ptr && node->Next)
		{
			node = node->Next;
		}
		if (node->Instruction != *ptr)
		{
			node->Next = new StackNode;
			node->Next->m_parent = node->m_parent;
			node->Next->Instruction = *ptr;
			node->Next->Next = nullptr;
			node->Next->m_first_child = nullptr;
			--ptr;
			return InsertChildren(node->Next, ptr, stack);
		}
		else if (node->m_first_child)
		{
			--ptr;
			node = node->m_first_child;
		}
		else if (ptr != stack)
		{
			--ptr;
			return InsertChildren(node, ptr, stack);
		}
		else
		{
			return node;
		}
	}

	return node;
}


static const uint32 UNINITIALIZED_MEMORY_PATTERN = 0xCD;
static const uint32 FREED_MEMORY_PATTERN = 0xDD;
static const uint32 ALLOCATION_GUARD = 0xFDFDFDFD;


Allocator::Allocator(IAllocator& source)
	: Source(source)
	, Root(nullptr)
	, Mutex(false)
	, TotalSize(0)
	, IsFillEnabled(true)
	, AreGuardsEnabled(true)
{
	Sentinels[0].next = &Sentinels[1];
	Sentinels[0].previous = nullptr;
	Sentinels[0].stack_leaf = nullptr;
	Sentinels[0].size = 0;
	Sentinels[0].align = 0;

	Sentinels[1].next = nullptr;
	Sentinels[1].previous = &Sentinels[0];
	Sentinels[1].stack_leaf = nullptr;
	Sentinels[1].size = 0;
	Sentinels[1].align = 0;

	Root = &Sentinels[1];
}


Allocator::~Allocator()
{
	AllocationInfo* last_sentinel = &Sentinels[1];
	if (Root != last_sentinel)
	{
		OutputDebugString("Memory leaks detected!\n");
		AllocationInfo* info = Root;
		while (info != last_sentinel)
		{
			char tmp[2048];
			sprintf(tmp, "\nAllocation size : %Iu, memory %p\n", info->size, info + sizeof(info));
			OutputDebugString(tmp);
			StackTree.PrintCallstack(info->stack_leaf);
			info = info->next;
		}
		assert(false);
	}
}


void Allocator::Lock()
{
	Mutex.Lock();
}


void Allocator::Unlock()
{
	Mutex.Unlock();
}


void Allocator::CheckGuards()
{
	if (AreGuardsEnabled) return;

	auto* info = Root;
	while (info)
	{
		auto user_ptr = GetUserPtrFromAllocationInfo(info);
		void* system_ptr = GetSystemFromUser(user_ptr);
		assert(*(uint32*)system_ptr == ALLOCATION_GUARD);
		assert(*(uint32*)((uint8*)user_ptr + info->size) == ALLOCATION_GUARD);

		info = info->next;
	}
}


size_t Allocator::GetAllocationOffset()
{
	return sizeof(AllocationInfo) + (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) : 0);
}


size_t Allocator::GetNeededMemory(size_t size)
{
	return size + sizeof(AllocationInfo) + (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) << 1 : 0);
}


size_t Allocator::GetNeededMemory(size_t size, size_t align)
{
	return size + sizeof(AllocationInfo) + (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) << 1 : 0) +
		   align;
}


Allocator::AllocationInfo* Allocator::GetAllocationInfoFromSystem(void* system_ptr)
{
	return (AllocationInfo*)(AreGuardsEnabled ? (uint8*)system_ptr + sizeof(ALLOCATION_GUARD)
												  : system_ptr);
}


void* Allocator::GetUserPtrFromAllocationInfo(AllocationInfo* info)
{
	return ((uint8*)info + sizeof(AllocationInfo));
}


Allocator::AllocationInfo* Allocator::GetAllocationInfoFromUser(void* user_ptr)
{
	return (AllocationInfo*)((uint8*)user_ptr - sizeof(AllocationInfo));
}


uint8* Allocator::GetUserFromSystem(void* system_ptr, size_t align)
{
	size_t diff = (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) : 0) + sizeof(AllocationInfo);

	if (align) diff += (align - diff % align) % align;
	return (uint8*)system_ptr + diff;
}


uint8* Allocator::GetSystemFromUser(void* user_ptr)
{
	AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
	size_t diff = (AreGuardsEnabled ? sizeof(ALLOCATION_GUARD) : 0) + sizeof(AllocationInfo);
	if (info->align) diff += (info->align - diff % info->align) % info->align;
	return (uint8*)user_ptr - diff;
}


void* Allocator::Reallocate(void* user_ptr, size_t size)
{
#ifndef _DEBUG
	return Source.Reallocate(user_ptr, size);
#else
	if (user_ptr == nullptr) return Allocate(size);
	if (size == 0) return nullptr;

	void* new_data = Allocate(size);
	if (!new_data) return nullptr;

	AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
	CopyMemory(new_data, user_ptr, info->size < size ? info->size : size);

	Deallocate(user_ptr);

	return new_data;
#endif
}


void* Allocator::AllocateAligned(size_t size, size_t align)
{
#ifndef _DEBUG
	return Source.AllocateAligned(size, align);
#else
	void* system_ptr;
	AllocationInfo* info;
	uint8* user_ptr;

	size_t system_size = GetNeededMemory(size, align);
	{
		MT::SpinLock lock(Mutex);
		system_ptr = Source.AllocateAligned(system_size, align);
		user_ptr = GetUserFromSystem(system_ptr, align);
		info = new (NewPlaceholder(), GetAllocationInfoFromUser(user_ptr)) AllocationInfo();

		info->previous = Root->previous;
		Root->previous->next = info;

		info->next = Root;
		Root->previous = info;

		Root = info;

		TotalSize += size;
	} // because of the SpinLock

	info->align = uint16(align);
	info->stack_leaf = StackTree.Record();
	info->size = size;
	if (IsFillEnabled)
	{
		memset(user_ptr, UNINITIALIZED_MEMORY_PATTERN, size);
	}

	if (AreGuardsEnabled)
	{
		*(uint32*)system_ptr = ALLOCATION_GUARD;
		*(uint32*)((uint8*)system_ptr + system_size - sizeof(ALLOCATION_GUARD)) = ALLOCATION_GUARD;
	}

	return user_ptr;
#endif
}


void Allocator::DeallocateAligned(void* user_ptr)
{
#ifndef _DEBUG
	Source.DeallocateAligned(user_ptr);
#else
	if (user_ptr)
	{
		AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
		void* system_ptr = GetSystemFromUser(user_ptr);
		if (IsFillEnabled)
		{
			memset(user_ptr, FREED_MEMORY_PATTERN, info->size);
		}

		if (AreGuardsEnabled)
		{
			assert(*(uint32*)system_ptr == ALLOCATION_GUARD);
			size_t system_size = GetNeededMemory(info->size, info->align);
			assert(*(uint32*)((uint8*)system_ptr + system_size - sizeof(ALLOCATION_GUARD)) == ALLOCATION_GUARD);
		}

		{
			MT::SpinLock lock(Mutex);
			if (info == Root)
			{
				Root = info->next;
			}
			info->previous->next = info->next;
			info->next->previous = info->previous;

			TotalSize -= info->size;
		} // because of the SpinLock

		info->~AllocationInfo();

		Source.DeallocateAligned((void*)system_ptr);
	}
#endif
}


void* Allocator::ReallocateAligned(void* user_ptr, size_t size, size_t align)
{
#ifndef _DEBUG
	return Source.ReallocateAligned(user_ptr, size, align);
#else
	if (user_ptr == nullptr) return AllocateAligned(size, align);
	if (size == 0) return nullptr;

	void* new_data = AllocateAligned(size, align);
	if (!new_data) return nullptr;

	AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
	CopyMemory(new_data, user_ptr, info->size < size ? info->size : size);

	DeallocateAligned(user_ptr);

	return new_data;
#endif
}


void* Allocator::Allocate(size_t size)
{
#ifndef _DEBUG
	return Source.Allocate(size);
#else
	void* system_ptr;
	AllocationInfo* info;
	size_t system_size = GetNeededMemory(size);
	{
		MT::SpinLock lock(Mutex);
		system_ptr = Source.Allocate(system_size);
		info = new (NewPlaceholder(), GetAllocationInfoFromSystem(system_ptr)) AllocationInfo();

		info->previous = Root->previous;
		Root->previous->next = info;

		info->next = Root;
		Root->previous = info;

		Root = info;

		TotalSize += size;
	} // because of the SpinLock

	void* user_ptr = GetUserFromSystem(system_ptr, 0);
	info->stack_leaf = StackTree.Record();
	info->size = size;
	info->align = 0;
	if (IsFillEnabled)
	{
		memset(user_ptr, UNINITIALIZED_MEMORY_PATTERN, size);
	}

	if (AreGuardsEnabled)
	{
		*(uint32*)system_ptr = ALLOCATION_GUARD;
		*(uint32*)((uint8*)system_ptr + system_size - sizeof(ALLOCATION_GUARD)) = ALLOCATION_GUARD;
	}

	return user_ptr;
#endif
}

void Allocator::Deallocate(void* user_ptr)
{
#ifndef _DEBUG
	Source.Deallocate(user_ptr);
#else
	if (user_ptr)
	{
		AllocationInfo* info = GetAllocationInfoFromUser(user_ptr);
		void* system_ptr = GetSystemFromUser(user_ptr);
		if (IsFillEnabled)
		{
			memset(user_ptr, FREED_MEMORY_PATTERN, info->size);
		}

		if (AreGuardsEnabled)
		{
			assert(*(uint32*)system_ptr == ALLOCATION_GUARD);
			size_t system_size = GetNeededMemory(info->size);
			assert(*(uint32*)((uint8*)system_ptr + system_size - sizeof(ALLOCATION_GUARD)) == ALLOCATION_GUARD);
		}

		{
			MT::SpinLock lock(Mutex);
			if (info == Root)
			{
				Root = info->next;
			}
			info->previous->next = info->next;
			info->next->previous = info->previous;

			TotalSize -= info->size;
		} // because of the SpinLock

		info->~AllocationInfo();

		Source.Deallocate((void*)system_ptr);
	}
#endif
}


} // namespace Debug


BOOL SendFile(LPCSTR lpszSubject,
	LPCSTR lpszTo,
	LPCSTR lpszName,
	LPCSTR lpszText,
	LPCSTR lpszFullFileName)
{
	HINSTANCE hMAPI = ::LoadLibrary("mapi32.dll");
	if (!hMAPI) return FALSE;
	LPMAPISENDMAIL lpfnMAPISendMail = (LPMAPISENDMAIL)::GetProcAddress(hMAPI, "MAPISendMail");

	char szDrive[_MAX_DRIVE] = {0};
	char szDir[_MAX_DIR] = {0};
	char szName[_MAX_FNAME] = {0};
	char szExt[_MAX_EXT] = {0};
	_splitpath_s(lpszFullFileName, szDrive, szDir, szName, szExt);

	char szFileName[MAX_PATH] = {0};
	strcat_s(szFileName, szName);
	strcat_s(szFileName, szExt);

	char szFullFileName[MAX_PATH] = {0};
	strcat_s(szFullFileName, lpszFullFileName);

	MapiFileDesc MAPIfile = {0};
	ZeroMemory(&MAPIfile, sizeof(MapiFileDesc));
	MAPIfile.nPosition = 0xFFFFFFFF;
	MAPIfile.lpszPathName = szFullFileName;
	MAPIfile.lpszFileName = szFileName;

	char szTo[MAX_PATH] = {0};
	strcat_s(szTo, lpszTo);

	char szNameTo[MAX_PATH] = {0};
	strcat_s(szNameTo, lpszName);

	MapiRecipDesc recipient = {0};
	recipient.ulRecipClass = MAPI_TO;
	recipient.lpszAddress = szTo;
	recipient.lpszName = szNameTo;

	char szSubject[MAX_PATH] = {0};
	strcat_s(szSubject, lpszSubject);

	char szText[MAX_PATH] = {0};
	strcat_s(szText, lpszText);

	MapiMessage MAPImsg = {0};
	MAPImsg.lpszSubject = szSubject;
	MAPImsg.lpRecips = &recipient;
	MAPImsg.nRecipCount = 1;
	MAPImsg.lpszNoteText = szText;
	MAPImsg.nFileCount = 1;
	MAPImsg.lpFiles = &MAPIfile;

	ULONG nSent = lpfnMAPISendMail(0, 0, &MAPImsg, 0, 0);

	FreeLibrary(hMAPI);
	return (nSent == SUCCESS_SUCCESS || nSent == MAPI_E_USER_ABORT);
}


static void getStack(CONTEXT& context, char* out, int MaxSize)
{
	BOOL result;
	HANDLE process;
	HANDLE thread;
	STACKFRAME64 stack;
	char symbol_mem[sizeof(IMAGEHLP_SYMBOL64) + 256];
	IMAGEHLP_SYMBOL64* symbol = (IMAGEHLP_SYMBOL64*)symbol_mem;
	DWORD64 displacement;
	char name[256];
	Neko::CopyString(out, MaxSize, "Crash callstack:\n");
	memset(&stack, 0, sizeof(STACKFRAME64));

	process = GetCurrentProcess();
	thread = GetCurrentThread();
	displacement = 0;
	DWORD machineType;
#ifdef _WIN64
	machineType = IMAGE_FILE_MACHINE_IA64;
	stack.AddrPC.Offset = context.Rip;
	stack.AddrPC.Mode = AddrModeFlat;
	stack.AddrStack.Offset = context.Rsp;
	stack.AddrStack.Mode = AddrModeFlat;
	stack.AddrFrame.Offset = context.Rbp;
	stack.AddrFrame.Mode = AddrModeFlat;
#else
	machineType = IMAGE_FILE_MACHINE_I386;
	stack.AddrPC.Offset = context.Eip;
	stack.AddrPC.Mode = AddrModeFlat;
	stack.AddrStack.Offset = context.Esp;
	stack.AddrStack.Mode = AddrModeFlat;
	stack.AddrFrame.Offset = context.Ebp;
	stack.AddrFrame.Mode = AddrModeFlat;
#endif

	do
	{
		result = StackWalk64(machineType,
			process,
			thread,
			&stack,
			&context,
			NULL,
			SymFunctionTableAccess64,
			SymGetModuleBase64,
			NULL);

		symbol->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL64);
		symbol->MaxNameLength = 255;

		SymGetSymFromAddr64(process, (ULONG64)stack.AddrPC.Offset, &displacement, symbol);
		UnDecorateSymbolName(symbol->Name, (PSTR)name, 256, UNDNAME_COMPLETE);

		CatString(out, MaxSize, symbol->Name);
		CatString(out, MaxSize, "\n");

	} while (result);
}


static LONG WINAPI unhandledExceptionHandler(LPEXCEPTION_POINTERS info)
{
	if (!g_is_crash_reporting_enabled) return EXCEPTION_CONTINUE_SEARCH;

	struct CrashInfo
	{
		LPEXCEPTION_POINTERS info;
		DWORD threadId;
	};

	auto dumper = [](void* data) -> DWORD {
		auto info = ((CrashInfo*)data)->info;
		char message[4096];
		if (info)
		{
			getStack(*info->ContextRecord, message, sizeof(message));
			Neko::Platform::ShowMessageBox("Exception", message, 0, EAppMsgType::Ok);
		}
		else
		{
			message[0] = '\0';
		}

		char minidump_path[Neko::MAX_PATH_LENGTH];
		GetCurrentDirectory(sizeof(minidump_path), minidump_path);
		Neko::CatString(minidump_path, "\\minidump.dmp");

		HANDLE process = GetCurrentProcess();
		DWORD process_id = GetProcessId(process);
		HANDLE file = CreateFile(
			minidump_path, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
		MINIDUMP_TYPE minidump_type = (MINIDUMP_TYPE)(
			MiniDumpWithFullMemoryInfo | MiniDumpFilterMemory | MiniDumpWithHandleData |
			MiniDumpWithThreadInfo | MiniDumpWithUnloadedModules);

		MINIDUMP_EXCEPTION_INFORMATION minidump_exception_info;
		minidump_exception_info.ThreadId = ((CrashInfo*)data)->threadId;
		minidump_exception_info.ExceptionPointers = info;
		minidump_exception_info.ClientPointers = FALSE;

		MiniDumpWriteDump(process,
			process_id,
			file,
			minidump_type,
			info ? &minidump_exception_info : nullptr,
			nullptr,
			nullptr);
		CloseHandle(file);

		SendFile("Neko Studio crash",
			"SMTP:mikulas.florek@gamedev.sk",
			"Neko Studio",
			message,
			minidump_path);

		minidump_type = (MINIDUMP_TYPE)(MiniDumpWithFullMemory | MiniDumpWithFullMemoryInfo |
			MiniDumpFilterMemory | MiniDumpWithHandleData |
			MiniDumpWithThreadInfo | MiniDumpWithUnloadedModules);
		file = CreateFile(
			"fulldump.dmp", GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
		MiniDumpWriteDump(process,
			process_id,
			file,
			minidump_type,
			info ? &minidump_exception_info : nullptr,
			nullptr,
			nullptr);
		CloseHandle(file);
		return 0;
	};

	DWORD threadId;
	CrashInfo crash_info = { info, GetCurrentThreadId() };
	auto handle = CreateThread(0, 0x8000, dumper, &crash_info, 0, &threadId);
	WaitForSingleObject(handle, INFINITE);

	return EXCEPTION_CONTINUE_SEARCH;
}


void EnableCrashReporting(bool enable)
{
	g_is_crash_reporting_enabled = enable;
}


void InstallUnhandledExceptionHandler()
{
	SetUnhandledExceptionFilter(unhandledExceptionHandler);
}


} // namespace Neko
#endif
