
#include "../../Neko.h"

#if NEKO_WINDOWS_FAMILY

#include "../../Data/IAllocator.h"
#include "../../Mt/Task.h"
#include "../../Mt/Thread.h"
//#include "../../Win/simple_win.h"
#include "../../Core/Profiler.h"


#include <windows.h>

#if !NEKO_SINGLE_THREAD()

namespace Neko
{
	namespace MT
	{
		const uint32 STACK_SIZE = 0x8000;

		struct TaskImpl
		{
			explicit TaskImpl(IAllocator& allocator)
				: Allocator(allocator)
			{
			}

			IAllocator& Allocator;
			HANDLE m_handle;
			DWORD m_thread_id;
			uint32 m_affinity_mask;
			uint32 m_priority;
			volatile bool m_is_running;
			volatile bool m_force_exit;
			volatile bool m_exited;
			const char* m_thread_name;
			Task* m_owner;
		};

		static DWORD WINAPI threadFunction(LPVOID ptr)
		{
			uint32 ret = 0xffffFFFF;
			struct TaskImpl* impl = reinterpret_cast<TaskImpl*>(ptr);
			SetThreadName(impl->m_thread_id, impl->m_thread_name);
			Profiler::SetThreadName(impl->m_thread_name);
			if (!impl->m_force_exit)
			{
				ret = impl->m_owner->DoTask();
			}
			impl->m_exited = true;
			impl->m_is_running = false;

			return ret;
		}

		Task::Task(IAllocator& allocator)
		{
			TaskImpl* impl = NEKO_NEW(allocator, TaskImpl)(allocator);
			impl->m_handle = nullptr;
			impl->m_affinity_mask = GetThreadAffinityMask();
			impl->m_priority = ::GetThreadPriority(GetCurrentThread());
			impl->m_is_running = false;
			impl->m_force_exit = false;
			impl->m_exited = false;
			impl->m_thread_name = "";
			impl->m_owner = this;

			Implementation = impl;
		}

		Task::~Task()
		{
			assert(!Implementation->m_handle);
			NEKO_DELETE(Implementation->Allocator, Implementation);
		}

		bool Task::Create(const char* name)
		{
			HANDLE handle = CreateThread(
				nullptr, STACK_SIZE, threadFunction, Implementation, CREATE_SUSPENDED, &Implementation->m_thread_id);
			if (handle)
			{
				Implementation->m_exited = false;
				Implementation->m_thread_name = name;
				Implementation->m_handle = handle;
				Implementation->m_is_running = true;

				bool success = ::ResumeThread(Implementation->m_handle) != -1;
				if (success)
				{
					return true;
				}
				::CloseHandle(Implementation->m_handle);
				Implementation->m_handle = nullptr;
				return false;
			}
			return false;
		}

		bool Task::Destroy()
		{
			while (Implementation->m_is_running)
			{
				YieldCpu();
			}

			::CloseHandle(Implementation->m_handle);
			Implementation->m_handle = nullptr;
			return true;
		}

		void Task::SetAffinityMask(uint32 affinity_mask)
		{
			Implementation->m_affinity_mask = affinity_mask;
			if (Implementation->m_handle)
			{
				::SetThreadIdealProcessor(Implementation->m_handle, affinity_mask);
			}
		}

		uint32 Task::GetAffinityMask() const
		{
			return Implementation->m_affinity_mask;
		}

		bool Task::IsRunning() const
		{
			return Implementation->m_is_running;
		}

		bool Task::IsFinished() const
		{
			return Implementation->m_exited;
		}

		bool Task::IsForceExit() const
		{
			return Implementation->m_force_exit;
		}

		IAllocator& Task::GetAllocator()
		{
			return Implementation->Allocator;
		}

		void Task::ForceExit(bool wait)
		{
			Implementation->m_force_exit = true;

			while (!IsFinished() && wait)
			{
				YieldCpu();
			}
		}


	} // namespace MT
} // namespace Neko


#endif
#endif
