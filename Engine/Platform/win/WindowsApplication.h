//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/


#include "../SystemShared.h"

#if NEKO_WINDOWS

#include "WindowsWindow.h"
#include <Windows.h>

#include "../../Vec.h"
#include "../../Engine.h"
#include "../../EventHandler.h"
#include "../../DefaultAllocator.h"
#include "../../Application.h"

#include "../../Mt/Sync.h"

#include "WindowsWindow.h"
#include "WindowsCursor.h"

namespace Neko
{

	struct FDeferredWindowsMessage
	{
		FDeferredWindowsMessage(CWindowsWindow* InNativeWindow, HWND InHWnd, uint32 InMessage, WPARAM InWParam, LPARAM InLParam, int32 InX = 0, int32 InY = 0, uint32 InRawInputFlags = 0)
			: NativeWindow(InNativeWindow)
			, hWND(InHWnd)
			, Message(InMessage)
			, wParam(InWParam)
			, lParam(InLParam)
			, X(InX)
			, Y(InY)
			, RawInputFlags(InRawInputFlags)
		{ }

		/** Native window that received the message */
		CWindowsWindow* NativeWindow;

		/** Window handle */
		HWND hWND;

		/** Message code */
		uint32 Message;

		/** Message data */
		WPARAM wParam;
		LPARAM lParam;

		/** Mouse coordinates */
		int32 X;
		int32 Y;
		uint32 RawInputFlags;
	};

	/**
	* Interface for classes that handle Windows events.
	*/
	class IWindowsMessageHandler
	{
	public:

		/**
		* Processes a Windows message.
		*
		* @param hwnd Handle to the window that received the message.
		* @param msg The message.
		* @param wParam Additional message information.
		* @param lParam Additional message information.
		* @param OutResult Will contain the result if the message was handled.
		* @return true if the message was handled, false otherwise.
		*/
		virtual bool ProcessMessage(HWND hwnd, uint32 msg, WPARAM wParam, LPARAM lParam, int32& OutResult) = 0;
	};

	class IAllocator;

	/// Native system interface for the engine.
	class CWindowsApplication final : public IPlatformApplication
	{
	public:
		
		/**
		 *  Constructor/destructor
		 */
		CWindowsApplication(const HINSTANCE HInstance, const HICON IconHandle);
		
		virtual ~CWindowsApplication();
		
		/**
		* Adds a Windows message handler with the application instance.
		*
		* @param MessageHandler The message handler to register.
		* @see RemoveMessageHandler
		*/
		virtual void AddMessageHandler(IWindowsMessageHandler& InMessageHandler);

		/**
		* Removes a Windows message handler with the application instance.
		*
		* @param MessageHandler The message handler to register.
		* @see AddMessageHandler
		*/
		virtual void RemoveMessageHandler(IWindowsMessageHandler& InMessageHandler);


		/** Registers the Windows class for windows and assigns the application instance and icon */
		static bool RegisterClass(const HINSTANCE HInstance, const HICON HIcon);

		/**  @return  True if a windows message is related to user input from the keyboard */
		static bool IsKeyboardInputMessage(uint32 msg);

		/**  @return  True if a windows message is related to user input from the mouse */
		static bool IsMouseInputMessage(uint32 msg);

		/**  @return  True if a windows message is related to user input (mouse, keyboard) */
		static bool IsInputMessage(uint32 msg);

		/** Defers a Windows message for later processing. */
		void DeferMessage(CWindowsWindow* NativeWindow, HWND InHWnd, uint32 InMessage, WPARAM InWParam, LPARAM InLParam, int32 MouseX = 0, int32 MouseY = 0, uint32 RawInputFlags = 0);

		/** Checks a key code for release of the Shift key. */
		void CheckForShiftUpEvents(const int32 KeyCode);

		/** Shuts down the application (called after an unrecoverable error occurred). */
		void ShutDownAfterError();

		/** Enables or disables Windows accessibility features, such as sticky keys. */
		void AllowAccessibilityShortcutKeys(const bool bAllowKeys);

		/** Queries and caches the number of connected mouse devices. */
		void QueryConnectedMice();

#if WINVER >= 0x0601
		/** Gets the touch index for a given windows touch ID. */
		uint32 GetTouchIndexForID(int32 TouchID);

		/** Searches for a free touch index. */
		uint32 GetFirstFreeTouchIndex();
#endif

		void UpdateKeymap();

		/** Helper function to update the cached states of all modifier keys */
		void UpdateAllModifierKeyStates();



		
		virtual void InitializeDevices() override;
		
		virtual void ProcessEvents() override;
		
		virtual void GetEvents() override;
		
		virtual IGenericWindow* MakeWindow(const FWindowDesc& InDesc) override;
		virtual void DestroyWindow(IGenericWindow* Window) override;
		virtual IGenericWindow* GetActiveWindow() override;
		
		virtual bool SetRelativeMouseMode(bool bEnabled) override;
		virtual void SetCursorShow(bool bShown) override;
		virtual void SetCursorType(Neko::ICursor::EMouseCursorType Type) override;
		virtual Neko::ICursor::EMouseCursorType GetCursorType() const override 
		{
			return Cursor->GetType();
		}
		virtual void WarpMouse(int32 X, int32 Y) override;
		
		/** Message handler. */
		virtual void SetMessageHandler(/*const*/ IGenericMessageHandler* InMessageHandler) override;
		
		static CWindowsApplication* CreateApplication();
		static CWindowsApplication* Create(const HINSTANCE HInstance, const HICON IconHandle);
		static void Destroy(CWindowsApplication* Application);
		
		
		NEKO_FORCE_INLINE CWindowsWindow* GetWindow(int32 Index) { return Windows[Index]; }
		
		void CloseWindow(CWindowsWindow* Window);
		bool OnWindowDestroyed(CWindowsWindow* Window);

		CWindowsCursor* Cursor;



		static const Int2 MinimizedWindowPosition;

		HINSTANCE InstanceHandle;

		bool bUsingHighPrecisionMouseInput;

		bool bIsMouseAttached;

		bool bForceActivateByMouse;

		TArray<FDeferredWindowsMessage> DeferredMessages;

		//TArray<FDeferredWindowsDragDropOperation> DeferredDragDropOperations;

		/** Registered Windows message handlers. */
		TArray<IWindowsMessageHandler*> MessageHandlers;

		TArray<CWindowsWindow*> Windows;


		struct EModifierKey
		{
			enum Type
			{
				LeftShift,		// VK_LSHIFT
				RightShift,		// VK_RSHIFT
				LeftControl,	// VK_LCONTROL
				RightControl,	// VK_RCONTROL
				LeftAlt,		// VK_LMENU
				RightAlt,		// VK_RMENU
				CapsLock,		// VK_CAPITAL
				Count,
			};
		};

		/** Cached state of the modifier keys. True if the modifier key is pressed (or toggled in the case of caps lock), false otherwise */
		bool ModifierKeyState[EModifierKey::Count];

		int32 bAllowedToDeferMessageProcessing;


		/** True if we are in the middle of a windows modal size loop */
		bool bInModalSizeLoop;

		// Accessibility shortcut keys
		STICKYKEYS							StartupStickyKeys;
		TOGGLEKEYS							StartupToggleKeys;
		FILTERKEYS							StartupFilterKeys;

#if WINVER >= 0x0601
		static const int32 MaxTouches = 10;

		/** Maps touch indexes to windows touch IDs. */
		int32 TouchIDs[MaxTouches];
#endif

		CWindowsWindow* CurrentWindow;
		
		DefaultAllocator Allocator; // Using own allocator to prevent using the engine instance.
	public:
	protected:

		/** Windows callback for message processing (forwards messages to the FWindowsApplication instance). */
		static LRESULT CALLBACK AppWndProc(HWND hwnd, uint32 msg, WPARAM wParam, LPARAM lParam);

		/** Processes a single Windows message. */
		int32 ProcessMessage(HWND hwnd, uint32 msg, WPARAM wParam, LPARAM lParam);

		/** Processes a deferred Windows message. */
		int32 ProcessDeferredMessage(const FDeferredWindowsMessage& DeferredMessage);

		/** Processes deferred drag and drop operations. */
		//void ProcessDeferredDragDropOperation(const FDeferredWindowsDragDropOperation& Op);

	private:
		bool bRelativeMouseMode;
	};
	
	extern CWindowsApplication* WindowsApplication;
}
#endif
