//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/


#include "../SystemShared.h"

#if NEKO_WINDOWS_FAMILY

#include "../Platform.h"
#include "../../IAllocator.h"
#include "../../StringUtil.h"
#include "../../PathUtils.h"
#include "../../FS/FileSystem.h"
#include "../../Platform/Application.h"

#include <cstdio>
#include <fcntl.h>
#include <sys/stat.h>
#include <Windows.h>
#include <shellapi.h>
#include <direct.h>
#include <ShlObj.h>
#include <mmsystem.h>

#include <xmmintrin.h>

#include <fenv.h>

#include "../../Text.h"

namespace Neko
{
    namespace Platform
    {
		struct FileIterator
		{
			HANDLE handle;
			Neko::IAllocator* allocator;
			WIN32_FIND_DATAA ffd;
			bool is_valid;
		};


		FileIterator* CreateFileIterator(const char* path, Neko::IAllocator& allocator)
		{
			char tmp[Neko::MAX_PATH_LENGTH];
			Neko::CopyString(tmp, path);
			Neko::CatString(tmp, "/*");
			auto* iter = NEKO_NEW(allocator, FileIterator);
			iter->allocator = &allocator;
			iter->handle = ::FindFirstFile(tmp, &iter->ffd);
			iter->is_valid = iter->handle != INVALID_HANDLE_VALUE;
			return iter;
		}


		void DestroyFileIterator(FileIterator* iterator)
		{
			FindClose(iterator->handle);
			NEKO_DELETE(*iterator->allocator, iterator);
		}


		bool GetNextFile(FileIterator* iterator, FileInfo* info)
		{
			if (!iterator->is_valid) return false;

			info->bIsDirectory = (iterator->ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
			Neko::CopyString(info->Filename, iterator->ffd.cFileName);

			iterator->is_valid = FindNextFile(iterator->handle, &iterator->ffd) != FALSE;
			return true;
		}


		bool FileDialogShared(const char* Title, Text& Out, const Text& Filter, const char* StartingFile, bool bSave)
		{
			return true;
		}
		
		
        bool GetSaveFilename(const Text& Title, Text& Out, const Text& Filter, const char* DefaultExtension)
        {
            bool bSuccess = FileDialogShared(Title, Out, Filter, DefaultExtension, true);
            if (bSuccess)
            {
                char Temp[Neko::MAX_PATH_LENGTH]; // @remove this garbage
                Neko::PathUtils::Normalize(Out, Temp, sizeof(Temp));
                Out = { Temp };
            }
            return bSuccess;
        }
		
		
        bool GetOpenFilename(const Text& Title, Text& Out, const Text& Filter, const char* BaseFile)
        {
            bool bSuccess = FileDialogShared(Title, Out, Filter, BaseFile, false);
            return bSuccess;
        }
		
        bool GetOpenDirectory(const Text& DialogTitle, Text& Out, const char* BaseDir)
        {
            return true ;
		}
		
        bool ShellExecuteOpen(const char* path)
        {
            return (uintptr_t)::ShellExecute(NULL, NULL, path, NULL, NULL, SW_SHOW) > 32;
        }
		
        void ExploreFolder(const char* FilePath)
        {
			if (DirectoryExists(FilePath))
			{
				// Explore the folder
				::ShellExecute(NULL, "explore", FilePath, NULL, NULL, SW_SHOWNORMAL);
			}
			else
			{
				// Explore the file
				//Text NativeFilePath = Text(FilePath).Replace(TEXT("/"), TEXT("\\"));
				//FString Parameters = FString::Printf(TEXT("/select,%s"), *NativeFilePath);
				//::ShellExecuteW(NULL, TEXT("open"), TEXT("explorer.exe"), *Parameters, NULL, SW_SHOWNORMAL);
			}
        }
        
        bool DeleteDirectory(const char* Path)
        {
			::RemoveDirectory(/*NormalizeDirectory(*/Path/*)*/);
			return !DirectoryExists(Path);
        }
        
        bool DeleteFile(const char* path)
        {
            return true;// unlink(path) == 0;
        }
        
        bool MoveFile(const char* from, const char* to)
        {
			return ::MoveFile(from, to) != FALSE;
        }
        
        size_t GetFileSize(const char* path)
        {
			WIN32_FILE_ATTRIBUTE_DATA fad;
			if (!GetFileAttributesEx(path, GetFileExInfoStandard, &fad)) return -1;
			LARGE_INTEGER size;
			size.HighPart = fad.nFileSizeHigh;
			size.LowPart = fad.nFileSizeLow;
			return (size_t)size.QuadPart;
        }
        
        const CDateTime Epoch(1970, 1, 1);
        
        SFileStatData GetFileData(const char* Path)
        {
            return SFileStatData();
        }
        
        bool FileExists(const char* Filename)
        {
			uint32 Result = GetFileAttributes(/*NormalizeFilename(*/Filename/*)*/);
			if (Result != 0xFFFFFFFF && !(Result & FILE_ATTRIBUTE_DIRECTORY))
			{
				return true;
			}
			return false;
        }
        
        bool DirectoryExists(const char* Directory)
        {
			// Empty Directory is the current directory so assume it always exists.
			bool bExists = !Neko::StringLength(Directory);
			if (!bExists)
			{
				uint32 Result = GetFileAttributes(/*NormalizeDirectory(*/Directory/*)*/);
				bExists = (Result != 0xFFFFFFFF && (Result & FILE_ATTRIBUTE_DIRECTORY));
			}
			return bExists;
        }
        
        uint64 GetLastModified(const char* file)
        {
			FILETIME ft;
			HANDLE handle = CreateFile(file, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			if (GetFileTime(handle, NULL, NULL, &ft) == FALSE)
			{
				return 0;
			}
			CloseHandle(handle);

			ULARGE_INTEGER i;
			i.LowPart = ft.dwLowDateTime;
			i.HighPart = ft.dwHighDateTime;
			return i.QuadPart;
        }
        
        bool MakePath(const char* path)
        {
			return SHCreateDirectoryEx(NULL, path, NULL) == ERROR_SUCCESS;
        }
        
        void CopyToClipboard(const char* Data)
        {
			if (!OpenClipboard(NULL)) return;
			int len = Neko::StringLength(Data);
			HGLOBAL mem_handle = GlobalAlloc(GMEM_MOVEABLE, len * sizeof(char));
			if (!mem_handle) return;

			char* mem = (char*)GlobalLock(mem_handle);
			Neko::CopyString(mem, len, Data);
			GlobalUnlock(mem_handle);
			EmptyClipboard();
			SetClipboardData(CF_TEXT, mem_handle);
			CloseClipboard();
        }
		
		void PasteFromClipboard(Text& Result)
		{

		}

        void ClipCursor(int x, int y, int w, int h)
        {
            //assert(false); // TODO
        }
		
        void UnclipCursor()
        {
            //assert(false); // TODO
        }
    
        bool CopyFile(const char* from, const char* to)
        {
            return ::CopyFile(from, to, FALSE) != FALSE;
        }
        
        void* LoadSystemLibrary(const char* path)
        {
            return ::LoadLibrary(path);
        }
        
        void UnloadLibrary(void* handle)
        {
            FreeLibrary((HMODULE)handle);
        }
        
        void* GetLibrarySymbol(void* handle, const char* name)
        {
            return GetProcAddress((HMODULE)handle, name);
        }
        
        
        EAppMsgReturnType ShowMessageBox(const Text& TitleDialog, const Text& Message, uint8 Icon, EAppMsgType Type)
        {
            return EAppMsgReturnType::Ok;
        }
        
        
        
        void EnableFloatingPointTraps(bool enable)
        {
            //static const int FLAGS = FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW | FE_UNDERFLOW;
            //	if(enable)
            //	{
            //			feenableexcept(FLAGS);
            //	}
            //	else
            //	{
            //			fedisableexcept(FLAGS);
            //	}
        }
        
        void GetCurrentUsername(Text& OutName)
        {
            
        }

        void GetBundlePath(Text& OutPath)
        {
            char buffer[MAX_PATH_LENGTH];
            
            if (!_getcwd(buffer, sizeof(buffer)))
            {
                buffer[0] = 0;
            }
            OutPath = Text(buffer);
        }
    
        PlatformMemoryStats GetMemoryStats()
        {
            PlatformMemoryHWConstants& MemoryInfo = GetHWStats();
            PlatformMemoryStats MemoryStats;
            
            return MemoryStats;
        }
        
        PlatformMemoryHWConstants& GetHWStats()
        {
            static PlatformMemoryHWConstants MemoryConstants;
            
            return MemoryConstants;
        }
        
        void GetSystemNameVersionString(Text& Version)
        {

        }
          

		void GetSystemTime(int32& Year, int32& Month, int32& DayOfWeek, int32& Day, int32& Hour, int32& Min, int32& Sec, int32& MSec)
		{
			SYSTEMTIME st;
			GetLocalTime(&st);

			Year = st.wYear;
			Month = st.wMonth;
			DayOfWeek = st.wDayOfWeek;
			Day = st.wDay;
			Hour = st.wHour;
			Min = st.wMinute;
			Sec = st.wSecond;
			MSec = st.wMilliseconds;
		}


		void UtcTime(int32& Year, int32& Month, int32& DayOfWeek, int32& Day, int32& Hour, int32& Min, int32& Sec, int32& MSec)
		{
			SYSTEMTIME st;
			GetSystemTime(&st);

			Year = st.wYear;
			Month = st.wMonth;
			DayOfWeek = st.wDayOfWeek;
			Day = st.wDay;
			Hour = st.wHour;
			Min = st.wMinute;
			Sec = st.wSecond;
			MSec = st.wMilliseconds;
		}

		int64 GetTicksPerSecond()
		{
			LARGE_INTEGER li;
			QueryPerformanceFrequency(&li);
			return li.QuadPart;
		}

		int64 GetTicks()
		{
			LARGE_INTEGER li;
			QueryPerformanceCounter(&li);
			return li.QuadPart;
		}

        // Command line
        void SetSystemCommandLine(int32 argc, char* argv[])
        {
            assert(false)
        }
        
        bool GetSystemCommandLine(char* Output, int32 MaxSize)
        {
            return CopyString(Output, MaxSize, ::GetCommandLine());
        }
        
        namespace Math
        {
            int32 TruncToInt(float F)
            {
                return _mm_cvtt_ss2si(_mm_set_ss(F));
            }
            
            int32 FloorToInt(float F)
            {
                return _mm_cvt_ss2si(_mm_set_ss(F + F - 0.5f)) >> 1;
            }
        }
        
    }
    
}
#endif
