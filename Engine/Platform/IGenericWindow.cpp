//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  IGenericWindow.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#include "IGenericWindow.h"

namespace Neko
{
    // Empty functionality
    
    IGenericWindow::IGenericWindow() { }
    
    IGenericWindow::~IGenericWindow() { }
    
    void IGenericWindow::ReshapeWindow(int32 X, int32 Y, int32 Width, int32 Height) { }
    
    void IGenericWindow::MoveWindowTo(int32 X, int32 Y) { }
    
    void IGenericWindow::BringToFront(bool bForce) { }
    
    void IGenericWindow::Destroy() { }
    
    void IGenericWindow::Maximize() { }
    
    void IGenericWindow::Restore() { }
    
    void IGenericWindow::Show() { }
    
    void IGenericWindow::Hide() { }
    
    void IGenericWindow::SetWindowMode(ENativeWindowMode Mode) { }
    
    Int2 IGenericWindow::GetSize() { return Int2::Zero; }
    
    Vec2 IGenericWindow::GetLocation() { return Vec2::Zero; }
    
    ENativeWindowMode IGenericWindow::GetWindowMode() const { assert(false); return ENativeWindowMode::TotalNativeWindowModes; }
    
    void* IGenericWindow::GetNativeHandle() const { return nullptr; }
    
    bool IGenericWindow::IsMaximized() const { return false; }
    
    bool IGenericWindow::IsMinimized() const { return false; }
    
    bool IGenericWindow::IsVisible() const { return false; }
    
    bool IGenericWindow::IsPointInWindow(int32 X, int32 Y) const { return false; }

    bool IGenericWindow::IsRegularWindow() const { return false; }

    float IGenericWindow::GetDPIScaleFactor() const { return 1.0f; }
    
    void IGenericWindow::SetTitle(const char* Text) { }
}
