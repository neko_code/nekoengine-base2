//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Platform.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Core/Path.h"
#include "../Utilities/Timer.h"

#if NEKO_APPLE_FAMILY
#   include "apple/ApplePlatform.h"
#elif NEKO_WINDOWS_FAMILY
#   include "win/WindowsApplication.h"
#endif

namespace Neko
{
    class IAllocator;

    /** Message box button types. */
    enum class EAppMsgType
    {
        Ok,
        YesNo,
        OkCancel,
        YesNoCancel,
        CancelRetryContinue,
        YesNoYesAllNoAll,
        YesNoYesAllNoAllCancel,
        YesNoYesAll,
    };
    
    /** Returned message box action types. */
    enum class EAppMsgReturnType
    {
        Ok,
        Yes,
        No,
        Cancel,
        Continue,
        Retry,
        YesAll,
        NoAll,
    };
    
    struct SGuid;
    
    /** 
	 * Platform depended utilities. 
	 */
    namespace Platform
    {
        /** 
         * Basic file statistics data. @see 'Platform::GetFileData'.
         */
        struct SFileStatData
        {
            SFileStatData()
            : CreationTime(CDateTime::MinValue())
            , AccessTime(CDateTime::MinValue())
            , ModificationTime(CDateTime::MinValue())
            , FileSize(-1)
            , bIsDirectory(false)
            , bIsReadOnly(false)
            , bIsValid(false)
            {
            }
            
            SFileStatData(CDateTime InCreationTime, CDateTime InAccessTime, CDateTime InModificationTime, const int64 InFileSize, const bool InIsDirectory, const bool InIsReadOnly)
            : CreationTime(InCreationTime)
            , AccessTime(InAccessTime)
            , ModificationTime(InModificationTime)
            , FileSize(InFileSize)
            , bIsDirectory(InIsDirectory)
            , bIsReadOnly(InIsReadOnly)
            , bIsValid(true)
            {
            }
            
            CDateTime CreationTime;
            CDateTime AccessTime;
            CDateTime ModificationTime;
            
            /** Size of the file (in bytes), or -1 if the file size is unknown */
            int64 FileSize;
            
            /** True if this data is for a directory, false if it's for a file */
            bool bIsDirectory : 1;
            
            /** True if this file is read-only */
            bool bIsReadOnly : 1;
            
            bool bIsValid : 1;
        };
        
        struct FileInfo // used for file iteration
        {
            bool bIsDirectory;
            char Filename[Neko::MAX_PATH_LENGTH];
        };
        
        struct FileIterator;
		
        
        /*--------------------------------------------------------
         File system
         --------------------------------------------------------*/
        
        NEKO_ENGINE_API FileIterator* CreateFileIterator(const char* Path, Neko::IAllocator& Allocator);
        NEKO_ENGINE_API void DestroyFileIterator(FileIterator* Iterator);
        NEKO_ENGINE_API bool GetNextFile(FileIterator* Iterator, FileInfo* Info);
        
        /** 
         * Opens the file system dialog to pick the file. 
         */
        NEKO_ENGINE_API bool GetOpenFilename(const Text& DialogTitle, Text& Out, const Text& Filter, const char* BaseFile);
        
        /** 
         * Opens the file system dialog to pick save file path. 
         */
        NEKO_ENGINE_API bool GetSaveFilename(const Text& Title, Text& Out, const Text& Filter, const char* DefaultExtension);
        
        /** 
         * Opens the file system dialog to pick the directory. 
         */
        NEKO_ENGINE_API bool GetOpenDirectory(const Text& DialogTitle, Text& Out, const char* BaseDir);

        /** 
         * Deletes a directory at the given path.
         *
         * @note This is not recursive and may return false if directory contains items.
         */
        NEKO_ENGINE_API bool DeleteDirectory(const char* Path);
        
        /** 
         * Deletes a file at the given path. 
         */
        NEKO_ENGINE_API bool DeleteFile(const char* Path);
		
        /** 
         * Moves file.
         */
        NEKO_ENGINE_API bool MoveFile(const char* From, const char* To);
        
        /** 
         * Copies file.
         */
        NEKO_ENGINE_API bool CopyFile(const char* From, const char* To);
        
        /** 
         * Returns file size. 
         */
        NEKO_ENGINE_API size_t GetFileSize(const char* Path);
        
        /** 
         * Checks whether file exists or not. 
         */
        NEKO_ENGINE_API bool FileExists(const char* Path);
        
        /** 
         * Checks whether directory exists or not. 
         */
        NEKO_ENGINE_API bool DirectoryExists(const char* Path);
        
        /** 
         * Returns the last modified time value. 
         */
        NEKO_ENGINE_API uint64 GetLastModified(const char* File);
        
        /** 
         * Creates desired directory. 
         */
        NEKO_ENGINE_API bool MakePath(const char* Path);
        
        /** 
         * Collects file data from the given path. 
         */
        NEKO_ENGINE_API SFileStatData GetFileData(const char* Path);
        
        /*--------------------------------------------------------
         System
         --------------------------------------------------------*/
        
        /**
         * Opens system file explorer with a selected file or a directory.
         */
        NEKO_ENGINE_API void ExploreFolder(const char* Path);
        
        /**
         * Performs system 'open' on the given path (file or directory).
         */
        NEKO_ENGINE_API bool ShellExecuteOpen(const char* Path);
        
        /**
         * Copies the given text data to the system clipboard.
         */
        NEKO_ENGINE_API void CopyToClipboard(const char* Text);
        
        /**
         * Returns the data from the system clipboard.
         */
        NEKO_ENGINE_API void PasteFromClipboard(Text& OutResult);
        
        
        /*--------------------------------------------------------
         System
         --------------------------------------------------------*/
        
        NEKO_ENGINE_API void* LoadSystemLibrary(const char* Path);
        NEKO_ENGINE_API void UnloadLibrary(void* Handle);
        NEKO_ENGINE_API void* GetLibrarySymbol(void* Handle, const char* Name);
		
        /** Shows the system message box. */
        NEKO_ENGINE_API EAppMsgReturnType ShowMessageBox(const Text& Title, const Text& Message, uint8 Icon, EAppMsgType Type);
        
        /** Pops up system notification. */
        NEKO_ENGINE_API void ShowNotification(const char* Title, const char* Message);
        
        // @todo Only in debug!
        NEKO_ENGINE_API void EnableFloatingPointTraps(bool bEnable);
        
        /** Returns current user name. */
        NEKO_ENGINE_API void GetCurrentUsername(Text& OutName);
        /** Returns application bundle path. */
        NEKO_ENGINE_API void GetBundlePath(Text& OutPath);
      
        NEKO_ENGINE_API uint32 GetCurrentProcessId();
        
        /** 
         * Returns the current hardware statistics.
         * @note Result will never change, call it only once.
         */
        NEKO_ENGINE_API PlatformMemoryHWConstants& GetHWStats();
        
        /** 
         * Returns memory statistics. 
         */
        NEKO_ENGINE_API PlatformMemoryStats GetMemoryStats();
        
        /** 
         * Returns operating system version as a string (e.g. 'macOS Sierra 10.12.4').
         */
        NEKO_ENGINE_API void GetSystemNameVersionString(Text& Version);
        
        /**
         * Returns the amount of logical CPU cores (e.g. hyperthreading).
         */
        NEKO_ENGINE_API int32 GetNumberOfLogicalCores();
        
        /**
         * Returns the amount of physical CPU cores.
         */
        NEKO_ENGINE_API int32 GetNumberOfPhysicalCores();
        
        /**
         * Generates unique identificator.
         */
        NEKO_ENGINE_API void CreateGuid(SGuid& Guid);
        
        
        NEKO_ENGINE_API void SetSystemCommandLine(int32 argc, char* Argv[]);
        NEKO_ENGINE_API bool GetSystemCommandLine(char* Output, int MaxSize);
        
        NEKO_EDITOR_API void ClipCursor(int32 X, int32 Y, int32 W, int32 H);
        NEKO_EDITOR_API void UnclipCursor();
        
        // System time
        NEKO_ENGINE_API void GetSystemTime(int32& Year, int32& Month, int32& DayOfWeek, int32& Day, int32& Hour, int32& Min, int32& Sec, int32& MSec);
        NEKO_ENGINE_API void UtcTime(int32& Year, int32& Month, int32& DayOfWeek, int32& Day, int32& Hour, int32& Min, int32& Sec, int32& MSec);
        
        /** 
         * Returns the current amount of ticks passed. 
         */
        NEKO_ENGINE_API int64 GetTicks();
        
        /** 
         * Returns the fixed value of ticks per seconds. 
         */
        NEKO_ENGINE_API int64 GetTicksPerSecond();

        
        struct Process;
        
        /*--------------------------------------------------------
         Tools for creating processes.
         --------------------------------------------------------*/
        
        NEKO_ENGINE_API Process* CreateProcess(const char* Cmd, const char* Args, IAllocator& Allocator);
        
        NEKO_ENGINE_API void DestroyProcess(Process& process);
        
        NEKO_ENGINE_API bool IsProcessFinished(Process& process);
        
        NEKO_ENGINE_API int GetProcessExitCode(Process& process);
        
        NEKO_ENGINE_API int GetProcessOutput(Process& process, char* Buffer, int BufferSize);
        
        
        /*--------------------------------------------------------
         Initialization
         --------------------------------------------------------*/
        
        NEKO_ENGINE_API void Init();
        
        NEKO_ENGINE_API void Teardown();
    }
}
