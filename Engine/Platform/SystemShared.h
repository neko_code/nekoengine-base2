//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//
//  Neko engine.
//
//  Common headers and defines..
//
//  Created by Neko Vision on 24/07/2014.
//  Copyright (c) 2013 Neko Vision. All rights reserved.
//

/**
 # This header contains all system headers and some necessary stuff.
 #
 #  *                ,MMM8&&&.            *
 #         *        MMMM88&&&&&    .
 #                 MMMM88&&&&&&&
 #  *              MMM88&&&&&&&&
 #                 MMM88&&&&&&&&
 #                 'MMM88&&&&&&'
 #        #          'MMM8&&&'      *
 #   *    |\___/|
 #        )     (             .              '
 #       >\     /<
 #         )===(       *
 #        /     \
 #        |     |
 #       /       \           *
 #       \       /
 #_/\_/\_/\__  _/_/\_/\_/\_/\_/\_/\_/\_/\_/\_
 #|  |  |  |( (  |  |  |  |  |  |  |  |  |  |
 #|  |  |  | ) ) |  |  |  |  |  |  |  |  |  |
 #|  |  |  |(_(  |  |  |  |  |  |  |  |  |  |
 #|  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 #|                    |                    |
 **/


#pragma once

#define NEKO_STRING_(_x) #_x
#define NEKO_STRING(_x) NEKO_STRING_(_x)

#if defined(__APPLE__)
#   include <AvailabilityMacros.h>  // Required for TARGET_OS_* defines
#   include <TargetConditionals.h>
#endif

#if !defined( NEKO_PLATFORM )
#	define NEKO_PLATFORM

#	if defined( WINAPI_FAMILY ) && (WINAPI_FAMILY == WINAPI_PARTITION_APP)
#		define NEKO_WINRT 1   // Windows Runtime, either on Windows RT or Windows 8
#	elif defined(XBOXONE)
#		define NEKO_XBOXONE 1
#	elif defined(_WIN64)    // @note: XBOXONE implies _WIN64
#		define NEKO_WIN64 1
#	elif defined(_M_PPC)
#		define NEKO_X360 1
#	elif defined(__ANDROID__)
#       include <android/api-level.h>
#		define NEKO_ANDROID __ANDROID_API__
#	elif defined(_WIN32)    // @note: _M_PPC implies _WIN32 // WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP)
#		define NEKO_WIN32 1
#	elif defined(__linux__) || defined(__CYGWIN__) // @note: __ANDROID__ implies __linux__
#		define NEKO_LINUX 1
#	elif defined(__APPLE__) && (defined(__arm__) || defined(__arm64__))
#		define NEKO_IOS 1
#	elif defined(__APPLE__)
#		define NEKO_OSX 1
#	elif defined(__APPLE__) && TARGET_OS_TV
#		define NEKO_TVOS 1
#	elif defined(__ORBIS__)
#		define NEKO_PS4 1
#	elif defined(__EMSCRIPTEN__)
#		define NEKO_EMSCRIPTEN 1
#	elif defined(__CELLOS_LV2__)
#		define NEKO_PS3 1
#	elif defined(__SNC__) && defined(__arm__)
#		define NEKO_PSP2 1
#	elif defined(__ghs__)
#		define NEKO_WIIU 1
#	elif defined(__native_client__)
#		define NEKO_NACL 1
#	elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__DragonFly__)
#		define NEKO_BSD 1
#	elif defined(__STEAMLINK__)
#		define NEKO_STEAMLINK 1
#	elif defined(__VCCOREVER__)
#		define NEKO_RPI 1
#	elif defined(__QNX__)
#		define NEKO_QNX 1
#	else
#		error "Unknown operating system"
#	endif

// Zero unset
#	ifndef NEKO_WINRT
#		define NEKO_WINRT 0
#	endif
#	ifndef NEKO_XBOXONE
#		define NEKO_XBOXONE 0
#	endif
#	ifndef NEKO_WIN64
#		define NEKO_WIN64 0
#	endif
#	ifndef NEKO_X360
#		define NEKO_X360 0
#	endif
#	ifndef NEKO_WIN32
#		define NEKO_WIN32 0
#	endif
#	ifndef NEKO_ANDROID
#		define NEKO_ANDROID 0
#	endif
#	ifndef NEKO_LINUX
#		define NEKO_LINUX 0
#	endif
#	ifndef NEKO_IOS
#		define NEKO_IOS 0
#	endif
#	ifndef NEKO_OSX
#		define NEKO_OSX 0
#	endif
#	ifndef NEKO_TVOS
#		define NEKO_TVOS 0
#	endif
#	ifndef NEKO_PS3
#		define NEKO_PS3 0
#	endif
#	ifndef NEKO_PS4
#		define NEKO_PS4 0
#	endif
#	ifndef NEKO_EMSCRIPTEN
#		define NEKO_EMSCRIPTEN 0
#	endif
#	ifndef NEKO_PSP2
#		define NEKO_PSP2 0
#	endif
#	ifndef NEKO_WIIU
#		define NEKO_WIIU 0
#	endif
#	ifndef NEKO_NACL
#		define NEKO_NACL 0
#	endif
#	ifndef NEKO_BSD
#		define NEKO_BSD 0
#	endif
#	ifndef NEKO_STEAMLINK
#		define NEKO_STEAMLINK 0
#	endif
#	ifndef NEKO_RPI
#		define NEKO_RPI 0
#	endif
#	ifndef NEKO_QNX
#		define NEKO_QNX 0
#	endif

#endif // NEKO_PLATFORM

#if !defined( NEKO_COMPILER )
#	define NEKO_COMPILER

#	if defined(_MSC_VER)
#		if _MSC_VER >= 1900
#			define NEKO_VC 14
#		elif _MSC_VER >= 1800
#			define NEKO_VC 12
#		elif _MSC_VER >= 1700
#			define NEKO_VC 11
#		elif _MSC_VER >= 1600
#			define NEKO_VC 10
#		elif _MSC_VER >= 1500
#			define NEKO_VC 9
#		else
#			error "Unknown VC version"
#		endif
#	elif defined(__clang__)
#		define NEKO_CLANG 1
#	elif defined(__SNC__)
#		define NEKO_SNC 1
#	elif defined(__ghs__)
#		define NEKO_GHS 1
#	elif defined(__GNUC__) // @note: __clang__, __SNC__, or __ghs__ imply __GNUC__
#		define NEKO_GCC 1
#	else
#		error "Unknown compiler"
#	endif

// Zero unset
#	ifndef NEKO_VC
#		define NEKO_VC 0
#	endif
#	ifndef NEKO_CLANG
#		define NEKO_CLANG 0
#	endif
#	ifndef NEKO_SNC
#		define NEKO_SNC 0
#	endif
#	ifndef NEKO_GHS
#		define NEKO_GHS 0
#	endif
#	ifndef NEKO_GCC
#		define NEKO_GCC 0
#	endif

#endif // NEKO_COMPILER


#if defined(__x86_64__)    || \
	defined(_M_X64) || \
	defined(__aarch64__) || \
	defined(__64BIT__) || \
	defined(__mips64) || \
	defined(__powerpc64__) || \
	defined(__ppc64__)
#	undef  NEKO_64BIT
#	define NEKO_64BIT 64
#else
#	undef  NEKO_32BIT
#	define NEKO_32BIT 32
#endif

#if !NEKO_64BIT
#   define NEKO_64BIT 0
#endif

#define NEKO_CPU_ARM  0
#define NEKO_CPU_JIT  0
#define NEKO_CPU_MIPS 0
#define NEKO_CPU_PPC  0
#define NEKO_CPU_X86  0

// Processor
// http://sourceforge.net/apps/mediawiki/predef/index.php?title=Architectures
#if defined(__arm__)         || \
	defined(__aarch64__) || \
	defined(_M_ARM)
#	undef  NEKO_CPU_ARM
#	define NEKO_CPU_ARM 1
#	define NEKO_CACHE_LINE_SIZE 64
#elif defined(__MIPSEL__)       || \
	defined(__mips_isa_rev) || \
	defined(__mips64)
#	undef  NEKO_CPU_MIPS
#	define NEKO_CPU_MIPS 1
#	define NEKO_CACHE_LINE_SIZE 64
#elif defined(_M_PPC)          || \
	defined(__powerpc__) || \
	defined(__powerpc64__)
#	undef  NEKO_CPU_PPC
#	define NEKO_CPU_PPC 1
#	define NEKO_CACHE_LINE_SIZE 128
#elif defined(_M_IX86)      || \
	defined(_M_X64) || \
	defined(__i386__) || \
	defined(__x86_64__)
#	undef  NEKO_CPU_X86
#	define NEKO_CPU_X86 1
#	define NEKO_CACHE_LINE_SIZE 64
#else // NaCl doesn't have CPU defined.
#	undef  NEKO_CPU_JIT
#	define NEKO_CPU_JIT 1
#	define NEKO_CACHE_LINE_SIZE 64
#endif

#if NEKO_CPU_ARM
#	define NEKO_CPU_NAME "ARM"
#elif NEKO_CPU_MIPS
#	define NEKO_CPU_NAME "MIPS"
#elif NEKO_CPU_PPC
#	define NEKO_CPU_NAME "PowerPC"
#elif NEKO_CPU_JIT
#	define NEKO_CPU_NAME "JIT-VM"
#elif NEKO_CPU_X86
#	define NEKO_CPU_NAME "x86"
#endif

#if NEKO_32BIT
#	define NEKO_ARCH_NAME "32-bit"
#elif NEKO_64BIT
#	define NEKO_ARCH_NAME "64-bit"
#endif


// OS
#define NEKO_WINDOWS (NEKO_WIN32 || NEKO_WIN64) // Desktop
#define NEKO_WINDOWS_FAMILY (NEKO_WINRT || NEKO_WIN32 || NEKO_WIN64)	// Desktop and phones.
#define NEKO_MICROSOFT_FAMILY (NEKO_XBOXONE || NEKO_X360 || NEKO_WINDOWS_FAMILY)
#define NEKO_LINUX_FAMILY (NEKO_LINUX || NEKO_ANDROID || NEKO_NACL)
#define NEKO_APPLE_FAMILY (NEKO_IOS || NEKO_OSX || NEKO_TVOS)
#define NEKO_UNIX_FAMILY (NEKO_LINUX_FAMILY || NEKO_APPLE_FAMILY || NEKO_EMSCRIPTEN || NEKO_BSD || NEKO_STEAMLINK || NEKO_RPI)
// Compilers
#define NEKO_GCC_FAMILY (NEKO_CLANG || NEKO_SNC || NEKO_GHS || NEKO_GCC)

#if !defined( NEKO_ALIGN )
#	if NEKO_MICROSOFT_FAMILY
#		define NEKO_ALIGN( alignment, decl ) __declspec(align(alignment)) decl
#		define NEKO_ALIGN_PREFIX( alignment ) __declspec(align(alignment))
#		define NEKO_ALIGN_SUFFIX( alignment )
#	elif NEKO_GCC_FAMILY
#		define NEKO_ALIGN( alignment, decl ) decl __attribute__((aligned(alignment)))
#		define NEKO_ALIGN_PREFIX( alignment )
#		define NEKO_ALIGN_SUFFIX( alignment ) __attribute__((aligned(alignment)))
#	else
#		define NEKO_ALIGN( alignment, decl )
#		define NEKO_ALIGN_PREFIX( alignment )
#		define NEKO_ALIGN_SUFFIX( alignment )
#	endif
#endif

#define NEKO_INLINE inline

#define NEKO_ALIGN_DECL_16(_decl) NEKO_ALIGN(16, _decl)
#define NEKO_ALIGN_DECL_256(_decl) NEKO_ALIGN(256, _decl)
#define NEKO_ALIGN_DECL_CACHE_LINE(_decl) NEKO_ALIGN(NEKO_CACHE_LINE_SIZE, _decl)



// GCC Specific
#if NEKO_GCC_FAMILY
#	define NEKO_NO_INLINE __attribute__((noinline))

#   define NEKO_ATTRIBUTE_USED  __attribute__((used))


#   define NEKO_LIBRARY_EXPORT __attribute__((visibility("default")))
#   define NEKO_LIBRARY_IMPORT


#   define NEKO_RESTRICT __restrict__

#	if !NEKO_LINUX
#		define NEKO_FORCE_INLINE inline __attribute__((always_inline))
#	endif

#   define __cdecl
#   define __stdcall

#	if !NEKO_SPU
#		define NEKO_PUSH_PACK_DEFAULT _Pragma("pack(push, 8)")
#		define NEKO_POP_PACK _Pragma("pack(pop)")
#	endif

#   define NEKO_BREAKPOINT( id ) __builtin_trap();

#	if (__cplusplus >= 201103L) && ((__GNUC__ > 4) || (__GNUC__ ==4 && __GNUC_MINOR__ >= 6))
#		define NEKO_NULL	NULL
#	else
#		define NEKO_NULL	__null
#	endif

#	define NEKO_ALIGN_OF(T)	__alignof__(T)
#	define NEKO_FUNCTION_SIG	__PRETTY_FUNCTION__

#	define NEKO_FORMAT_FUNCTION				__attribute__((format(printf, 1, 2)))
#	define NEKO_FORMAT_FUNCTION_IN_CLASS		__attribute__((format(printf, 2, 3)))

#	define NEKO_UNLIKELY(_x) __builtin_expect(!!(_x), 0)

#endif // NEKO_GCC_FAMILY



// Microsoft VC specific
#if NEKO_MICROSOFT_FAMILY

#	pragma inline_depth(255)

#	pragma warning(disable : 4324 )     // C4324: structure was padded due to alignment specifier
#	pragma warning(disable : 4514 )     // 'function' : unreferenced inline function has been removed
#	pragma warning(disable : 4710 )     // 'function' : function not inlined
#	pragma warning(disable : 4711 )     // function 'function' selected for inline expansion

#   define NEKO_ATTRIBUTE_USED

#   define NEKO_LIBRARY_EXPORT __declspec(dllexport)
#   define NEKO_LIBRARY_IMPORT __declspec(dllimport)

#   define NEKO_RESTRICT __restrict

#	define NEKO_NO_ALIAS __declspec( noalias )
#	define NEKO_NO_INLINE __declspec( noinline )
#	define NEKO_FORCE_INLINE __forceinline
#	define NEKO_PUSH_PACK_DEFAULT __pragma( pack( push, 8 ) )
#	define NEKO_POP_PACK __pragma( pack( pop ) )

#	define NEKO_BREAKPOINT( id ) __debugbreak();


#	ifdef __cplusplus
#		define NEKO_NULL	NULL
#	endif

#	define NEKO_ALIGN_OF( T ) __alignof( T )

#	define NEKO_FUNCTION_SIG	__FUNCSIG__

#	define NEKO_INT64(x) (x##i64)
#	define NEKO_UINT64(x) (x##ui64)

#	define NEKO_STDCALL __stdcall
#	define NEKO_CALL_CONV __cdecl

#	define NEKO_FORMAT_FUNCTION
#endif // NEKO_MICROSOFT_FAMILY

#if !defined( NEKO_FORCE_INLINE )
#	define NEKO_FORCE_INLINE inline
#endif


#if defined(__has_feature)
#	define NEKO_CLANG_HAS_FEATURE(_x) __has_feature(_x)
#else
#	define NEKO_CLANG_HAS_FEATURE(_x) 0
#endif // defined(__has_feature)

#if defined(__has_extension)
#	define NEKO_CLANG_HAS_EXTENSION(_x) __has_extension(_x)
#else
#	define NEKO_CLANG_HAS_EXTENSION(_x) 0
#endif // defined(__has_extension)


// Gcc
#if NEKO_GCC_FAMILY
// Check for C++11
#	if (__GNUC__ * 100 + __GNUC_MINOR__) >= 407
#		define NEKO_OVERRIDE override
#	endif
#	define NEKO_NO_VTABLE
#	if NEKO_CLANG_HAS_FEATURE(cxx_thread_local)
#		define NEKO_THREAD_LOCAL thread_local
#	endif
#	if (!NEKO_OSX && (NEKO_GCC >= 40200)) || (NEKO_GCC >= 40500)
#		define NEKO_THREAD_LOCAL __thread
#	endif
#endif // NEKO_GCC_FAMILY

// MSVC
#if NEKO_VC
// C4481: nonstandard extension used: override specifier 'override'
#	if _MSC_VER < 1700
#		pragma warning(disable : 4481)
#	endif
#	define NEKO_OVERRIDE	override

#	define NEKO_UNLIKELY(_x) (_x)
#	define NEKO_NO_VTABLE __declspec(novtable)
#	define NEKO_THREAD_LOCAL __declspec(thread)
#endif // NEKO_VC



// Platform names
#if NEKO_ANDROID
#   define NEKO_PLATFORM_NAME "Android"
#elif NEKO_EMSCRIPTEN
#   define NEKO_PLATFORM_NAME "asm.js " \
	NEKO_STRING(__EMSCRIPTEN_major__) "." \
	NEKO_STRING(__EMSCRIPTEN_minor__) "." \
	NEKO_STRING(__EMSCRIPTEN_tiny__)
#elif NEKO_BSD
#   define NEKO_PLATFORM_NAME "BSD"
#elif NEKO_IOS
#   define NEKO_PLATFORM_NAME "iOS"
#elif NEKO_OSX
#   define NEKO_PLATFORM_NAME "OSX"
#elif NEKO_LINUX
#   define NEKO_PLATFORM_NAME "Linux"
#elif NEKO_NACL
#   define NEKO_PLATFORM_NAME "NaCL"
#elif NEKO_PS4
#   define NEKO_PLATFORM_NAME "PlayStation 4"
#elif NEKO_QNX
#   define NEKO_PLATFORM_NAME "QNX"
#elif NEKO_RPI
#   define NEKO_PLATFORM_NAME "RaspberryPi"
#elif NEKO_STEAMLINK
#   define NEKO_PLATFORM_NAME "SteamLink"
#elif NEKO_WINDOWS
#   define NEKO_PLATFORM_NAME "Windows"
#elif NEKO_WINRT
#   define NEKO_PLATFORM_NAME "WinRT"
#elif NEKO_X360
#   define NEKO_PLATFORM_NAME "Xbox 360"
#elif NEKO_XBOXONE
#   define NEKO_PLATFORM_NAME "Xbox One"
#endif

// Compiler names
#if NEKO_GCC
#   define NEKO_COMPILER_NAME "GCC " \
	NEKO_STRING(__GNUC__) "." \
	NEKO_STRING(__GNUC_MINOR__) "." \
	NEKO_STRING(__GNUC_PATCHLEVEL__)
#elif NEKO_CLANG
#   define NEKO_COMPILER_NAME "Clang " \
	NEKO_STRING(__clang_major__) "." \
	NEKO_STRING(__clang_minor__) "." \
	NEKO_STRING(__clang_patchlevel__)
#elif NEKO_VC
#   if NEKO_VC >= 1900
#       define NEKO_COMPILER_NAME "MSVC 14.0"
#   elif NEKO_VC >= 1800
#       define NEKO_COMPILER_NAME "MSVC 12.0"
#   elif NEKO_VC >= 1700
#       define NEKO_COMPILER_NAME "MSVC 11.0"
#   elif NEKO_VC >= 1600
#       define NEKO_COMPILER_NAME "MSVC 10.0"
#   elif NEKO_VC >= 1500
#       define NEKO_COMPILER_NAME "MSVC 9.0"
#   else
#       define NEKO_COMPILER_NAME "MSVC"
#   endif
#endif



#if !defined( NEKO_UNUSED )
#	define NEKO_UNUSED(v) (void)v;
#endif

// Concatenates two preprocessor tokens, performing macro expansion on them first
#define PREPROCESSOR_JOIN(x, y) PREPROCESSOR_JOIN_INNER(x, y)
#define PREPROCESSOR_JOIN_INNER(x, y) x##y

///
#if NEKO_CLANG
#	define BX_PRAGMA_DIAGNOSTIC_PUSH_CLANG()      _Pragma("clang diagnostic push")
#	define BX_PRAGMA_DIAGNOSTIC_POP_CLANG()       _Pragma("clang diagnostic pop")
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_CLANG(_x) _Pragma(NEKO_STRING(clang diagnostic ignored _x) )
#else
#	define BX_PRAGMA_DIAGNOSTIC_PUSH_CLANG()
#	define BX_PRAGMA_DIAGNOSTIC_POP_CLANG()
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_CLANG(_x)
#endif // BX_COMPILER_CLANG

#if NEKO_GCC && NEKO_GCC >= 40600
#	define BX_PRAGMA_DIAGNOSTIC_PUSH_GCC()        _Pragma("GCC diagnostic push")
#	define BX_PRAGMA_DIAGNOSTIC_POP_GCC()         _Pragma("GCC diagnostic pop")
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_GCC(_x)   _Pragma(NEKO_STRING(GCC diagnostic ignored _x) )
#else
#	define BX_PRAGMA_DIAGNOSTIC_PUSH_GCC()
#	define BX_PRAGMA_DIAGNOSTIC_POP_GCC()
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_GCC(_x)
#endif // BX_COMPILER_GCC

#if NEKO_MSVC
#	define BX_PRAGMA_DIAGNOSTIC_PUSH_MSVC()      __pragma(warning(push) )
#	define BX_PRAGMA_DIAGNOSTIC_POP_MSVC()       __pragma(warning(pop) )
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_MSVC(_x) __pragma(warning(disable:_x) )
#else
#	define BX_PRAGMA_DIAGNOSTIC_PUSH_MSVC()
#	define BX_PRAGMA_DIAGNOSTIC_POP_MSVC()
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_MSVC(_x)
#endif // BX_COMPILER_CLANG

#if NEKO_CLANG
#	define BX_PRAGMA_DIAGNOSTIC_PUSH              BX_PRAGMA_DIAGNOSTIC_PUSH_CLANG
#	define BX_PRAGMA_DIAGNOSTIC_POP               BX_PRAGMA_DIAGNOSTIC_POP_CLANG
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_CLANG_GCC BX_PRAGMA_DIAGNOSTIC_IGNORED_CLANG
#elif NEKO_GCC
#	define BX_PRAGMA_DIAGNOSTIC_PUSH              BX_PRAGMA_DIAGNOSTIC_PUSH_GCC
#	define BX_PRAGMA_DIAGNOSTIC_POP               BX_PRAGMA_DIAGNOSTIC_POP_GCC
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_CLANG_GCC BX_PRAGMA_DIAGNOSTIC_IGNORED_GCC
#elif NEKO_VC
#	define BX_PRAGMA_DIAGNOSTIC_PUSH              BX_PRAGMA_DIAGNOSTIC_PUSH_MSVC
#	define BX_PRAGMA_DIAGNOSTIC_POP	              BX_PRAGMA_DIAGNOSTIC_POP_MSVC
#	define BX_PRAGMA_DIAGNOSTIC_IGNORED_CLANG_GCC(_x)
#endif


#if NEKO_CPU_PPC
#	undef  NEKO_CPU_ENDIAN_BIG
#	define NEKO_CPU_ENDIAN_BIG 1
#else
#	undef  NEKO_CPU_ENDIAN_LITTLE
#	define NEKO_CPU_ENDIAN_LITTLE 1
#endif // NEKO_CPU_PPC


/* expands to the first argument */
#define FIRST(...) FIRST_HELPER(__VA_ARGS__, throwaway)
#define FIRST_HELPER(first, ...) first


/*
 * if there's only one argument, expands to nothing.  if there is more
 * than one argument, expands to a comma followed by everything but
 * the first argument.  only supports up to 9 arguments but can be
 * trivially expanded.
 */
#define REST(...) REST_HELPER(NUM(__VA_ARGS__), __VA_ARGS__)
#define REST_HELPER(qty, ...) REST_HELPER2(qty, __VA_ARGS__)
#define REST_HELPER2(qty, ...) REST_HELPER_##qty(__VA_ARGS__)
#define REST_HELPER_ONE(first)
#define REST_HELPER_TWOORMORE(first, ...) , __VA_ARGS__
#define NUM(...) \
	SELECT_10TH(__VA_ARGS__, TWOORMORE, TWOORMORE, TWOORMORE, TWOORMORE, \
	TWOORMORE, TWOORMORE, TWOORMORE, TWOORMORE, ONE, throwaway)
#define SELECT_10TH(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, ...) a10


#ifndef BIT
#define BIT( num )				( 1 << ( num ) )
#endif

#include <stdint.h> // Standart integer types.

// Wisely used types.
typedef int32_t int32;
typedef uint32_t uint32;
typedef int64_t int64;
typedef uint64_t uint64;
typedef int8_t int8;
typedef uint8_t uint8;
typedef int16_t int16;
typedef uint16_t uint16;
#if NEKO_64BIT
typedef uint64 uptr;
#else
typedef uint32 uintptr;
#endif

typedef unsigned char Byte;

#include <errno.h>
#include <time.h>

#include <fcntl.h>
//#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>

/*                ,MMM8&&&.            *
 #      *        MMMM88&&&&&    .
 #              MMMM88&&&&&&&
 #*             MMM88&&&&&&&&
 #              MMM88&&&&&&&&
 #              'MMM88&&&&&&'
 #                'MMM8&&&'      *
 #   *     |\___/|
 #         )     (             .              '
 #        >\     /<
 #          )===(       *
 #         /     \
 #         |     |
 #        /       \           *
 #        \       /
 # _/\_/\_/\__  _/_/\_/\_/\_/\_/\_/\_/\_/\_/\_
 # |  |  |  |( (  |  |  |  |  |  |  |  |  |  |
 # |  |  |  | ) ) |  |  |  |  |  |  |  |  |  |
 # |  |  |  |(_(  |  |  |  |  |  |  |  |  |  |
 # |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 # |                    |                    |
 */

// Defaults
#ifndef PLATFORM_NUM_AUDIODECOMPRESSION_PRECACHE_BUFFERS
#   define PLATFORM_NUM_AUDIODECOMPRESSION_PRECACHE_BUFFERS 2
#endif


#if NEKO_APPLE_FAMILY

//#	define NEKO_ARC_ENABLED				(__has_feature(objc_arc))
//#if (NEKO_ARC_ENABLED)
//#
//#	define NEKO_AUTORELEASE(VALUE)		(VALUE)
//#	define NEKO_RETAIN(VALUE)			(VALUE)
//#	define NEKO_RELEASE(VALUE)			{ VALUE = nil; }
//#   define NEKO_BRIDGE_CAST(TYPE, VALUE) (__bridge TYPE)VALUE;
//#
//#else
#
#	define NEKO_AUTORELEASE(VALUE)		[VALUE autorelease]
#	define NEKO_RETAIN(VALUE)			[VALUE retain]
#	define NEKO_RELEASE(VALUE)			{ [VALUE release]; VALUE = nil; }
#   define NEKO_BRIDGE_CAST(TYPE, VALUE)      (TYPE)VALUE;
#
//#endif

#	include "SharedApple.h"

#	if !defined(MAX_PATH)
#		define MAX_PATH            256
#	endif // MAX_PATH

#elif NEKO_WINDOWS_FAMILY
#	include "SharedWin.h"
#elif NEKO_LINUX_FAMILY
#	include "SharedLinux.h"
#endif

#if NEKO_X360
#   include <malloc.h>
#   include <xtl.h>
#endif

#if NEKO_ANDROID
#   include <android/native_window.h>
#endif

/**
 #			  *     ,MMM8&&&.            *
 #	   *           MMMM88&&&&&    .                       *       *
 #		  *       MMMM88&&&&&&&      *
 # *              MMM88&&&&&&&&                   *
 #			*     MMM88&&&&&&&&           *           *
 #				  'MMM88&&&&&&'               *               *
 # *		        'MMM8&&&'      *    _
 #		 |\___/|                      \\                *
 #  *   =) ^Y^ (=   |\_/|              ||    '      *
 #		 \  ^  /    )a a '._.-""""-.  //                *       *
 #  *     )=*=(    =\T_= /    ~  ~  \//             *
 #		 /     \     `"`\   ~   / ~  /    *                 *
 # *     |     |         |~   \ |  ~/                               *
 #		/| | | |\         \  ~/- \ ~\           *
 #		\| | |_|/|        || |  // /`
 #       /_\_/\_//_// __//\_/\_/\_((_|\((_//\_/\_/\_
 # |  |  |  | \_) |  |  |  |  |  |  |  |  |  |
 # |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 # |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 # |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 # |                    |                    |
 **/


#define NEKO_SIZEOF( s ) (sizeof(s) / sizeof(s[0]))
#define NEKO_ALIGN_MASK(_value, _mask) ( ( (_value)+(_mask) ) & ( (~0)&(~(_mask) ) ) )
#define NEKO_ALIGN_16(_value) NEKO_ALIGN_MASK(_value, 0xf)
#define NEKO_ALIGN_256(_value) NEKO_ALIGN_MASK(_value, 0xff)
#define NEKO_ALIGN_4096(_value) NEKO_ALIGN_MASK(_value, 0xfff)

#include <assert.h>

#ifdef STATIC_PLUGINS
#   define NEKO_ENGINE_API
#elif defined BUILDING_ENGINE
#   define NEKO_ENGINE_API NEKO_LIBRARY_EXPORT
#else
#   define NEKO_ENGINE_API NEKO_LIBRARY_IMPORT
#endif

#ifdef STATIC_PLUGINS
#   define NEKO_EDITOR_API
#elif defined BUILDING_EDITOR
#   define NEKO_EDITOR_API NEKO_LIBRARY_EXPORT
#else
#   define NEKO_EDITOR_API NEKO_LIBRARY_IMPORT
#endif

#ifdef STATIC_PLUGINS
#   define NEKO_RENDERER_API
#elif defined BUILDING_RENDERER
#   define NEKO_RENDERER_API NEKO_LIBRARY_EXPORT
#else
#   define NEKO_RENDERER_API NEKO_LIBRARY_IMPORT
#endif

#ifdef _MSC_VER
#   pragma warning(disable : 4251)
#   pragma warning(disable : 4996)
#   if _MSC_VER == 1900
#       pragma warning(disable : 4091)
#   endif
#endif

/*          *           *         *             *   *
 #*        *    ,MMM8&&&&.            *      *
 #             MMMM88&&&&&&    .                         *
 #            MMMM88&&&&&&&&
 # *          MMM88&&&&&&&&&         *
 #      *     MMM88&&&&&&&&&  *
 #            'MMM88&&&&&&'             *
 *              'MMM8&&&'      *    *       *
 #       |\___/|     /\___/\
 #      ~(     )     (     )    .      *       '
 #      =\     /=   =\~    /=
 #  *     )===(       ) ~ (     *               *
 #       /     \     /     \        *
 #       |     |     ) ~   (            *
 #      /       \   /     ~ \   *               *
 #      \       /   \~     ~/
 _/\_/\_/\__  _/_/\_/\__~__/_/\_/\_/\_/\_/\_
 |  |  |  |( (  |  |  | ))  |  |  |  |  |  |
 |  |  |  | ) ) |  |  |//|  |  |  |  |  |  |
 |  |  |  |(_(  |  |  (( |  |  |  |  |  |  |
 |  |  |  |  |  |  |  |\)|  |  |  |  |  |  |
 |                    |                    |
 */


