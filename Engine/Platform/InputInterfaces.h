//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  InputInterfaces.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Math/Vec.h"

namespace Neko
{
    // Various platform input interfaces.
    
    /** Cursor interface for native cursors. */
    class ICursor
    {
    public:
        
        /** Cursor types. */
        enum EMouseCursorType
        {
            // Not visible
            None = 0,
            
            Default,
            
            TextEditBeam, 
			
			ResizeLeftRight,
			
			ResizeUpDown,
			
			Crosshairs,
			
			Hand,
			
			GrabHand,
			
			GrabHandClosed,
			
			SlashedCircle,
			
			Custom,
			
            TotalCursorCount
        };
        
        /** Returns the current position of the cursor. */
        virtual Vec2                GetPosition() const = 0;
        
        /** Wraps the mouse cursor to the given position. */
        virtual void                SetPosition(const int32 X, const int32 Y) = 0;
        
        /** Sets the cursor type. @see 'EMouseCursorType'. */
        virtual void                SetType(EMouseCursorType Type) = 0;
        
        /** Returns the current cursor type. */
        virtual EMouseCursorType    GetType() const = 0;
        
        /** Set true of false whether we need to show the mouse cursor or not. */
        virtual void                Show(bool bShow) = 0;
        
    };
}
