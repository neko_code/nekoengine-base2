//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Atomic.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../SystemShared.h"

#if NEKO_APPLE_FAMILY

#include "../../Mt/Atomic.h"
#include <libkern/OSAtomic.h>

namespace Neko
{
    /** Apple platform atomic operations. */
    namespace Atomics
    {
        
        // @todo Replace deprecations by something new
        
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"
        int32 Increment(int32 volatile* value)
        {
            return OSAtomicIncrement32Barrier(value);
        }
        
        int32 Decrement(int32 volatile* value)
        {
            return OSAtomicDecrement32Barrier(value);
        }
        
        int32 Add(int32 volatile* addend, int32 value)
        {
            return OSAtomicAdd32Barrier(value, addend);
        }
        
        int32 Subtract(int32 volatile* addend, int32 value)
        {
            return OSAtomicAdd32Barrier(-value, addend);
        }
        
        bool CompareAndExchange(int32 volatile* dest, int32 exchange, int32 comperand)
        {
            return OSAtomicCompareAndSwap32Barrier(comperand, exchange, dest);
        }
        
        bool CompareAndExchange64(int64 volatile* dest, int64 exchange, int64 comperand)
        {
            return OSAtomicCompareAndSwap64Barrier(comperand, exchange, dest);
        }
        
        
        int32 Exchange( volatile int32* Value, int32 Exchange )
        {
            int32 RetVal;
            
            do
            {
                RetVal = *Value;
            }
            while (!OSAtomicCompareAndSwap32Barrier(RetVal, Exchange, (int32_t*) Value));
            
            return RetVal;
        }
        
        int64 Exchange( volatile int64* Value, int64 Exchange )
        {
            int64 RetVal;
            
            do
            {
                RetVal = *Value;
            }
            while (!OSAtomicCompareAndSwap64Barrier(RetVal, Exchange, (int64_t*) Value));
            
            return RetVal;
        }
        
        void* InterlockedExchangePtr(void** Dest, void* Exchange)
        {
            void* RetVal;
            
            do {
                RetVal = *Dest;
            } while (!OSAtomicCompareAndSwapPtrBarrier(RetVal, Exchange, Dest));
            
            return RetVal;
        }

        NEKO_ENGINE_API void MemoryBarrierFence()
        {
            OSMemoryBarrier();// __sync_synchronize();
        }
        #pragma clang diagnostic pop
    } // ~namespace MT
} // ~namespace Neko
#endif
