//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ApplePlatformUtils.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../../Neko.h"
#include "../SharedApple.h"

#include <wchar.h>

#import <Foundation/Foundation.h>

namespace Neko
{
#ifdef __OBJC__
    
    class ScopeAutoreleasePool
    {
    public:
        
        ScopeAutoreleasePool()
        {
            Pool = [[NSAutoreleasePool alloc] init];
        }
        
        ~ScopeAutoreleasePool()
        {
            [Pool release];
        }
        
    private:
        
        NSAutoreleasePool*	Pool;
    };
    
#define SCOPED_AUTORELEASE_POOL const Neko::ScopeAutoreleasePool PREPROCESSOR_JOIN(Pool,__LINE__);
    
#endif // __OBJC__
    
    namespace Platform
    {
        void CFStringToTCHAR(CFStringRef CFStr, wchar_t* TChar);
        CFStringRef TCHARToCFString(const wchar_t* TChar);
        
        void CFStringToTCHAR(CFStringRef CFStr, char* TChar);
        
        CFStringRef TCHARToCFString(const char* TChar);
    }
}
