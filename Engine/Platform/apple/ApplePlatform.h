//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ApplePlatform.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../SharedApple.h"
#include <mach/mach_time.h>

namespace Neko
{
    struct AppleSpecificPlatform
    {
        static double InitTiming();
        
        static NEKO_FORCE_INLINE double Seconds()
        {
            uint64 Cycles = mach_absolute_time();
            // Add big number to make bugs apparent where return value is being passed to float
            return Cycles * SecondsPerCycle + 16777216.0;
        }
        
        static NEKO_FORCE_INLINE uint32 Cycles()
        {
            uint64 Cycles = mach_absolute_time();
            return (uint32)Cycles;
        }
        
        static NEKO_FORCE_INLINE uint64 Cycles64()
        {
            uint64 Cycles = mach_absolute_time();
            return Cycles;
        }
        
        static double SecondsPerCycle;
    };
    
    typedef AppleSpecificPlatform PlatformTime;
}
