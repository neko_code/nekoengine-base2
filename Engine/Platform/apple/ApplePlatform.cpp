//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ApplePlatform.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../SystemShared.h"

#if NEKO_APPLE_FAMILY

#include "../Platform.h"
#include "../../Core/Log.h"
#include "../../Core/Debug.h"
#include "../../Core/Guid.h"
#include "../../Core/PathUtils.h"
#include "../../Data/IAllocator.h"
#include "../../Utilities/StringUtil.h"
#include "../../FS/FileSystem.h"
#include "../../Platform/Application.h"

#include <cstdio>
#include <dlfcn.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/sysctl.h>
#include <unistd.h>

#include <dirent.h>
#include <copyfile.h>
#include <fenv.h>

#include <AppKit/AppKit.h>

#include <pwd.h>
#include <libproc.h>
#include <notify.h>
#include <IOKit/IOKitLib.h>
#include <IOKit/ps/IOPowerSources.h>
#include <IOKit/ps/IOPSKeys.h>
#include <IOKit/kext/KextManager.h>
#include <IOKit/pwr_mgt/IOPMLib.h>

#include "ApplePlatformUtils.h"

#include "Mac/MacApplication.h"
#include "Mac/Runloop.h"

#include "../../Utilities/Text.h"


class ScopedSystemModalMode
{
public:
    
    ScopedSystemModalMode() { Neko::MacApplication->SetModalMode(true); }
    ~ScopedSystemModalMode() { Neko::MacApplication->SetModalMode(false); }
};

/**
 * Custom accessory view class to allow choose kind of file extension
 */
@interface FileDialogAccessoryView : NSView
{
@private
    NSPopUpButton*	PopUpButton;
    NSTextField*	TextField;
    NSSavePanel*	DialogPanel;
    NSMutableArray*	AllowedFileTypes;
    NSInteger       SelectedExtension;
}

- (id)initWithFrame:(NSRect)frameRect dialogPanel:(NSSavePanel*) panel;
- (void)PopUpButtonAction: (id) sender;
- (void)AddAllowedFileTypes: (NSArray*) array;
- (void)SetExtensionsAtIndex: (NSInteger) index;
- (NSInteger)SelectedExtension;

@end

@implementation FileDialogAccessoryView

- (id)initWithFrame:(NSRect)frameRect dialogPanel:(NSSavePanel*) panel
{
    self = [super initWithFrame: frameRect];
    DialogPanel = panel;
    
    Neko::Text FieldText = "File extension:";
    NSString* FieldTextCFString = [[NSString alloc] initWithCString:FieldText encoding:NSUTF8StringEncoding];
    TextField = [[NSTextField alloc] initWithFrame: NSMakeRect(0.0, 48.0, 90.0, 25.0) ];
    [TextField setStringValue:(NSString*)FieldTextCFString];
    [TextField setEditable:NO];
    [TextField setBordered:NO];
    [TextField setBackgroundColor:[NSColor controlColor]];
    
    
    PopUpButton = [[NSPopUpButton alloc] initWithFrame: NSMakeRect(88.0, 50.0, 160.0, 25.0) ];
    [PopUpButton setTarget: self];
    [PopUpButton setAction:@selector(PopUpButtonAction:)];
    
    [self addSubview: TextField];
    [self addSubview: PopUpButton];
    
    return self;
}


- (void)AddAllowedFileTypes: (NSMutableArray*) array
{
    assert(array);
    
    AllowedFileTypes = array;
    int32 ArrayCount = static_cast<int32>([AllowedFileTypes count]);
    if (ArrayCount)
    {
        assert(ArrayCount % 2 == 0);
        
        [PopUpButton removeAllItems];
        
        for (int32 Index = 0; Index < ArrayCount; Index += 2)
        {
            [PopUpButton addItemWithTitle: [AllowedFileTypes objectAtIndex: Index]];
        }
        
        // Set allowed extensions
        [self SetExtensionsAtIndex: 0];
    }
    else
    {
        // Allow all file types
        [DialogPanel setAllowedFileTypes:nil];
    }
}

- (void)PopUpButtonAction: (id) sender
{
    NSInteger Index = [PopUpButton indexOfSelectedItem];
    [self SetExtensionsAtIndex: Index];
}

- (void)SetExtensionsAtIndex: (NSInteger) index
{
    assert( [AllowedFileTypes count] >= index * 2 );
    SelectedExtension = index;
    
    NSString* ExtsToParse = [AllowedFileTypes objectAtIndex:index * 2 + 1];
    if ( [ExtsToParse compare:@"*.*"] == NSOrderedSame )
    {
        [DialogPanel setAllowedFileTypes: nil];
    }
    else
    {
        NSArray* ExtensionsWildcards = [ExtsToParse componentsSeparatedByString:@";"];
        NSMutableArray* Extensions = [NSMutableArray arrayWithCapacity: [ExtensionsWildcards count]];
        
        for( int32 Index = 0; Index < [ExtensionsWildcards count]; ++Index )
        {
            NSString* Temp = [[ExtensionsWildcards objectAtIndex:Index] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"*."]];
            [Extensions addObject: Temp];
        }
        
        [DialogPanel setAllowedFileTypes: Extensions];
    }
}

- (NSInteger)SelectedExtension
{
    return SelectedExtension;
}

@end

class CocoaScopeContext
{
public:
    
	CocoaScopeContext()
	{
		@autoreleasepool {
			PreviousContext = [NSOpenGLContext currentContext];
		}
	}
	
	~CocoaScopeContext()
	{
		@autoreleasepool {
			NSOpenGLContext* NewContext = [NSOpenGLContext currentContext];
			if (PreviousContext != NewContext)
			{
				if (PreviousContext)
				{
					[PreviousContext makeCurrentContext];
				}
				else
				{
					[NSOpenGLContext clearCurrentContext];
				}
			}
		}
	}
	
private:
    
	NSOpenGLContext*	PreviousContext;
};



@interface NSAutoReadPipe : NSObject

/** The pipe itself */
@property (readonly) NSPipe*			Pipe;
/** A file associated with the pipe from which we shall read data */
@property (readonly) NSFileHandle*		File;
/** Buffer that stores the output from the pipe */
@property (readonly) NSMutableData*		PipeOutput;

/** Initialization function */
-(id)init;

/** Deallocation function */
-(void)dealloc;

/** Callback function that is invoked when data is pushed onto the pipe */
-(void)readData: (NSNotification *)Notification;

/** Shutdown the background reader, and copy all the data from the pipe as a UTF8 encoded string */
-(void)copyPipeData: (Neko::Text&)OutString;

@end

@implementation NSAutoReadPipe

-(id)init
{
    [super init];
    
    _PipeOutput = [NSMutableData new];
    _Pipe = [NSPipe new];
    _File = [_Pipe fileHandleForReading];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(readData:)
                                                 name: NSFileHandleDataAvailableNotification
                                               object: _File];
    
    [_File waitForDataInBackgroundAndNotify];
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_Pipe release];
    [_PipeOutput release];
    
    [super dealloc];
}

-(void)readData: (NSNotification *)Notification
{
    NSFileHandle* FileHandle = (NSFileHandle*)Notification.object;
    
    // Ensure we're reading from the right file
    if (FileHandle == _File)
    {
        [_PipeOutput appendData: [FileHandle availableData]];
        [FileHandle waitForDataInBackgroundAndNotify];
    }
}

-(void)copyPipeData: (Neko::Text&)OutString
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Read any remaining data in from the pipe
    NSData* Data = [_File readDataToEndOfFile];
    if (Data && [Data length])
    {
        [_PipeOutput appendData: Data];
    }
    
    // Encode the data as a string
    NSString* String = [[NSString alloc] initWithData:_PipeOutput encoding:NSUTF8StringEncoding];
    
    OutString = Neko::Text(String);
    
    [String release];
}

@end // NSAutoReadPipe

namespace Neko
{
    namespace Platform
    {
        static Neko::DefaultAllocator SharedAllocator;
        
		bool FileDialogShared(const char* Title, Text& Out, const Text& Filter, const char* StartingFile, bool bSave)
		{
			bool bResult = false;
			{
				ScopedSystemModalMode SystemModalScope;
				bResult = MainThreadReturn(^{
					@autoreleasepool {
						
						CocoaScopeContext ContextGuard;
						
						NSSavePanel* Panel = bSave ? [NSSavePanel savePanel] : [NSOpenPanel openPanel];
						if (!bSave)
						{
							NSOpenPanel* OpenPanel = (NSOpenPanel*)Panel;
							[OpenPanel setCanChooseFiles: true];
							[OpenPanel setCanChooseDirectories: false];
							[OpenPanel setAllowsMultipleSelection:false];// @todo Flags & EFileDialogFlags::Multiple];
						}
						
						// Allow us to create folder if we need to save a file
						[Panel setCanCreateDirectories: bSave];
						
						// Set the title
						NSString* TitleRef = [[NSString alloc] initWithCString:Title encoding:NSUTF8StringEncoding];
						[Panel setTitle: TitleRef];
						[TitleRef release];
						
						// Set directory path
						if (!Out.IsEmpty())
						{
							NSString* DefaultPathRef = [[NSString alloc] initWithUTF8String:Out];
							NSURL* DefaultPathURL = [NSURL fileURLWithPath: DefaultPathRef];
							[Panel setDirectoryURL: DefaultPathURL];
							[DefaultPathRef release];
						}
						
						if (StartingFile)
						{
							NSString* FileNameRef = [[NSString alloc] initWithUTF8String:StartingFile];
							[Panel setNameFieldStringValue: FileNameRef];
							[FileNameRef release];
						}
						
						if (bSave)
						{
							NSText* editor = [Panel fieldEditor:NO forObject:nil];
							if (editor)
							{
								NSString* exportFilename = [Panel nameFieldStringValue];
								NSString* extension = [exportFilename pathExtension];
								if (extension != nil)
								{
									NSInteger extensionLength = [exportFilename length] - [extension length] - 1;
									[editor setSelectedRange:NSMakeRange(0, extensionLength)];
								}
							}
						}
						
						FileDialogAccessoryView* AccessoryView = [[FileDialogAccessoryView alloc] initWithFrame: NSMakeRect( 0.0, 0.0, 250.0, 85.0 ) dialogPanel: Panel];
						[Panel setAccessoryView: AccessoryView];
						
						// Sort the extensions list
						TArray<Text> FileTypesArray(SharedAllocator);
						int32 NumFileTypes = Filter.ParseIntoArray(FileTypesArray, "|", true);
						NSMutableArray* AllowedFileTypes = [NSMutableArray arrayWithCapacity: NumFileTypes];
						
						if (NumFileTypes > 0)
						{
							for (int32 Index = 0; Index < NumFileTypes; ++Index)
							{
								NSString* Type = [[NSString alloc] initWithUTF8String:FileTypesArray[Index].c_str()];
								[AllowedFileTypes addObject: (NSString*)Type];
								[Type release];
							}
						}
						
						if (NumFileTypes > 0)
						{
							[AccessoryView AddAllowedFileTypes:AllowedFileTypes];
						}
						
						bool bOkPressed = false;
						NSWindow* FocusWindow = [[NSApplication sharedApplication] keyWindow];
						
						NSInteger Result = [Panel runModal];
						[AccessoryView release];
						
                        if (Result == NSModalResponseOK) // "Ok" pressed
						{
							if (bSave)
							{
								Out = { [[[Panel URL] path] UTF8String] };
							}
							else
							{
								NSOpenPanel* OpenPanel = (NSOpenPanel*)Panel;
								for (NSURL* FileURL in [OpenPanel URLs])
								{
									Out = { [[FileURL path] UTF8String] };
								}
							}
							
							bOkPressed = true;
						}
						
						[Panel close];
						
						if (FocusWindow)
						{
							[FocusWindow makeKeyWindow];
						}
						
						return bOkPressed;
					}
				});
			}
			MacApplication->ResetModifierKeys();
			return bResult;
		}
		
		
        bool GetSaveFilename(const Text& Title, Text& Out, const Text& Filter, const char* DefaultExtension)
        {
            bool bSuccess = FileDialogShared(Title, Out, Filter, DefaultExtension, true);
            if (bSuccess)
            {
                char Temp[Neko::MAX_PATH_LENGTH]; // @remove this garbage
                Neko::PathUtils::Normalize(Out, Temp, sizeof(Temp));
                Out = { Temp };
            }
            return bSuccess;
        }
		
        bool GetOpenFilename(const Text& Title, Text& Out, const Text& Filter, const char* BaseFile)
        {
            bool bSuccess = FileDialogShared(Title, Out, Filter, BaseFile, false);
            return bSuccess;
        }
		
        bool GetOpenDirectory(const Text& DialogTitle, Text& Out, const char* BaseDir)
        {
			bool bResult = false;
			{
				ScopedSystemModalMode SystemModalScope;
				bResult = MainThreadReturn(^
				{
					SCOPED_AUTORELEASE_POOL;
					
					CocoaScopeContext ContextGuard;
					
					NSOpenPanel* Panel = [NSOpenPanel openPanel];
					[Panel setCanChooseFiles: false];
					[Panel setCanChooseDirectories: true];
					[Panel setAllowsMultipleSelection: false];
					[Panel setCanCreateDirectories: true];
					
					NSString* Title = [[NSString alloc] initWithUTF8String:DialogTitle];
					[Panel setTitle: (NSString*)Title];
					[Title release];
					
					if (BaseDir)
					{
						NSString* DefaultPathCFString = [[NSString alloc] initWithUTF8String:BaseDir];
						NSURL* DefaultPathURL = [NSURL fileURLWithPath: DefaultPathCFString];
						[Panel setDirectoryURL: DefaultPathURL];
						[DefaultPathCFString release];
					}
					
					NSInteger Result = [Panel runModal];
					
					bool bOk = false;
					
                    if (Result == NSModalResponseOK)
					{
						NSURL *FolderURL = [[Panel URLs] objectAtIndex: 0];
						char FolderName[MAX_PATH];
						CopyString(FolderName, sizeof(FolderName), [[FolderURL path] UTF8String]);
						char FolderNormalizedName[MAX_PATH];
						PathUtils::Normalize(FolderName, FolderNormalizedName, sizeof(FolderName));
						
						Out = { FolderNormalizedName };
						
						bOk = true;
					}
					
					[Panel close];
					return bOk;
				});
			}
			MacApplication->ResetModifierKeys();
			
			return bResult ;
		}
		
        bool ShellExecuteOpen(const char* path)
        {
            return system(Neko::va("open \"%s\"", path)) == 0;
        }
		
        void ExploreFolder(const char* FilePath)
        {
            SCOPED_AUTORELEASE_POOL;
            CFStringRef CFFilePath = TCHARToCFString(FilePath);
            BOOL IsDirectory = NO;
            if ([[NSFileManager defaultManager] fileExistsAtPath:(NSString*)CFFilePath isDirectory:&IsDirectory])
            {
                if (IsDirectory)
                {
                    [[NSWorkspace sharedWorkspace] selectFile:nil inFileViewerRootedAtPath:(NSString*)CFFilePath];
                }
                else
                {
                    NSString* Directory = [(NSString*)CFFilePath stringByDeletingLastPathComponent];
                    [[NSWorkspace sharedWorkspace] selectFile:(NSString*)CFFilePath inFileViewerRootedAtPath: Directory];
                }
            }
            CFRelease(CFFilePath);
        }
        
        bool DeleteDirectory(const char* Path)
        {
            return rmdir(Path) == 0;
        }
        
        bool DeleteFile(const char* path)
        {
            return unlink(path) == 0;
        }
        
        bool MoveFile(const char* from, const char* to)
        {
            int32 Result = rename(from, to);
            if (Result == -1 && errno == EXDEV)
            {
                if (CopyFile(from, to))
                {
                    DeleteFile(from);
                    Result = 0;
                }
            }
            return Result != -1;
        }
        
        size_t GetFileSize(const char* path)
        {
            struct stat tmp;
            stat(path, &tmp);
            return tmp.st_size;
        }
        
        const CDateTime Epoch(1970, 1, 1);
        
        SFileStatData GetFileData(const char* Path)
        {
            struct stat FileInfo;
            if (stat(Path, &FileInfo) != -1)
            {
                const bool bIsDirectory = S_ISDIR(FileInfo.st_mode);
                
                int64 FileSize = -1;
                if (!bIsDirectory)
                {
                    FileSize = FileInfo.st_size;
                }
                
                return SFileStatData(Epoch + CTimeValue(0, 0, (int32)FileInfo.st_ctime)
                                     , Epoch + CTimeValue(0, 0, (int32)FileInfo.st_atime)
                                     , Epoch + CTimeValue(0, 0, (int32)FileInfo.st_mtime), FileSize, bIsDirectory, !!(FileInfo.st_mode & S_IWUSR));
            }
            return SFileStatData();
        }
        
        bool FileExists(const char* path)
        {
            struct stat tmp;
            return ((stat(path, &tmp) == 0) && (((tmp.st_mode) & S_IFMT) != S_IFDIR));
        }
        
        bool DirectoryExists(const char* path)
        {
            struct stat tmp;
            return ((stat(path, &tmp) == 0) && (((tmp.st_mode) & S_IFMT) == S_IFDIR));
        }
        
        uint64 GetLastModified(const char* file)
        {
            struct stat tmp;
            uint64 ret = 0;
            if (stat(file, &tmp) != 0)
            {
                return 0;
            }
            ret = tmp.st_mtimespec.tv_sec * 1000 + uint64(tmp.st_mtimespec.tv_nsec / 1000000);
            return ret;
        }
        
        bool MakePath(const char* path)
        {
            return mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == 0;
        }
        
        void CopyToClipboard(const char* Data)
        {
            MainThreadCall(^{
                @autoreleasepool
                {
                    NSString* String = (NSString*)Platform::TCHARToCFString(Data);
                    NSPasteboard* board = [NSPasteboard generalPasteboard];
                    [board clearContents];
                    [board declareTypes:[NSArray arrayWithObjects:NSStringPboardType, nil] owner:nil];
                    [board setString:String forType: NSStringPboardType];
                    CFRelease(String);
                }
            });
        }
		
		void PasteFromClipboard(Text& Result)
		{
			MainThreadCall(^{
				@autoreleasepool
				{
					NSPasteboard *Pasteboard = [NSPasteboard generalPasteboard];
					NSString *CocoaString = [Pasteboard stringForType: NSPasteboardTypeString];
					if (CocoaString)
					{
						TArray<char> Ch(SharedAllocator);
						Ch.Resize(int32([CocoaString length] + 1));
						
						CFStringToTCHAR((CFStringRef)CocoaString, &Ch[0]);
						Result = &Ch[0];
					}
					else
					{
						Result = "";
					}
				}
			});
		}

        void ClipCursor(int X, int Y, int W, int H)
        {
            
        }
		
        void UnclipCursor()
        {
           
        }
    
        bool CopyFile(const char* from, const char* to)
        {
            int returnCode;
            copyfile_state_t _copyfileState;
            
            _copyfileState = copyfile_state_alloc();
            returnCode = copyfile(from, to, 0, COPYFILE_ALL);
            copyfile_state_free(_copyfileState);
            
            return returnCode == 0;
        }
        
        void* LoadSystemLibrary(const char* Path)
        {
            assert(Path);
            
            @autoreleasepool
            {
                NSFileManager* FileManager = [NSFileManager defaultManager];
                NSString* DylibPath = [NSString stringWithUTF8String:Path];
                NSString* ExecutablePath = [[[NSBundle mainBundle] executablePath] stringByDeletingLastPathComponent];
                if (![FileManager fileExistsAtPath:DylibPath])
                {
                    // Else lookup in the bundle
                    DylibPath = [ExecutablePath stringByAppendingString:Text(Path).GetNSString()];
                }
                
                // Check if library is already loaded
                void* Handle = dlopen([DylibPath fileSystemRepresentation], RTLD_NOLOAD | RTLD_LAZY | RTLD_LOCAL);
                if (!Handle)
                {
                    NSString* DylibName;
                    if ([DylibPath hasPrefix:ExecutablePath])
                    {
                        DylibName = [DylibPath substringFromIndex:[ExecutablePath length] + 1];
                    }
                    else
                    {
                        DylibName = [DylibPath lastPathComponent];
                    }
                    Handle = dlopen([[@"rpath" stringByAppendingPathComponent:DylibName] fileSystemRepresentation], RTLD_NOLOAD | RTLD_LAZY | RTLD_LOCAL);
                }
                
                if (!Handle)
                {
                    // Not loaded yet, open it
                    Handle = dlopen([DylibPath fileSystemRepresentation], RTLD_LOCAL | RTLD_LAZY);
                }
                
                if (!Handle)
                {
                    Neko::GLogError.log("System") << "dlopen failed: " << dlerror();
                }
                
                return Handle;
            }
        }
        
        void UnloadLibrary(void* handle)
        {
            dlclose(handle);
        }
        
        void* GetLibrarySymbol(void* handle, const char* name)
        {
            return dlsym(handle, name);
        }
        
        void ShowNotification(const char* TitleStr, const char* Message)
        {
            // @todo
        }
        
        EAppMsgReturnType ShowMessageBox(const Text& TitleDialog, const Text& Message, uint8 Icon, EAppMsgType Type)
        {
            @autoreleasepool
            {
                EAppMsgReturnType ReturnValue = MainThreadReturn(^ {
                    EAppMsgReturnType RetValue = EAppMsgReturnType::Cancel;
                    NSInteger Result;
                    
                    NSAlert* AlertPanel = [NSAlert new];
                    
                    
                    NSString* Title = [[NSString alloc] initWithCString:TitleDialog encoding:NSUTF8StringEncoding];
                    [AlertPanel setMessageText:Title];
                    [Title release];
                    
                    NSString* Information = [[NSString alloc] initWithCString:Message encoding:NSUTF8StringEncoding];
                    [AlertPanel setInformativeText:Information];
                    [Information release];
                    
                    switch (Type)
                    {
                        case EAppMsgType::Ok:
                            [AlertPanel addButtonWithTitle:@"OK"];
                            [AlertPanel runModal];
                            RetValue = EAppMsgReturnType::Ok;
                            break;
                            
                        case EAppMsgType::YesNo:
                            [AlertPanel addButtonWithTitle:@"Yes"];
                            [AlertPanel addButtonWithTitle:@"No"];
                            Result = [AlertPanel runModal];
                            if (Result == NSAlertFirstButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Yes;
                            }
                            else if (Result == NSAlertSecondButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::No;
                            }
                            break;
                            
                        case EAppMsgType::OkCancel:
                            [AlertPanel addButtonWithTitle:@"OK"];
                            [AlertPanel addButtonWithTitle:@"Cancel"];
                            Result = [AlertPanel runModal];
                            if (Result == NSAlertFirstButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Ok;
                            }
                            else if (Result == NSAlertSecondButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Cancel;
                            }
                            break;
                            
                        case EAppMsgType::YesNoCancel:
                            [AlertPanel addButtonWithTitle:@"Yes"];
                            [AlertPanel addButtonWithTitle:@"No"];
                            [AlertPanel addButtonWithTitle:@"Cancel"];
                            Result = [AlertPanel runModal];
                            if (Result == NSAlertFirstButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Yes;
                            }
                            else if (Result == NSAlertSecondButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::No;
                            }
                            else
                            {
                                RetValue = EAppMsgReturnType::Cancel;
                            }
                            break;
                            
                        case EAppMsgType::CancelRetryContinue:
                            [AlertPanel addButtonWithTitle:@"Continue"];
                            [AlertPanel addButtonWithTitle:@"Retry"];
                            [AlertPanel addButtonWithTitle:@"Cancel"];
                            Result = [AlertPanel runModal];
                            if (Result == NSAlertFirstButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Continue;
                            }
                            else if (Result == NSAlertSecondButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Retry;
                            }
                            else
                            {
                                RetValue = EAppMsgReturnType::Cancel;
                            }
                            break;
                            
                        case EAppMsgType::YesNoYesAllNoAll:
                            [AlertPanel addButtonWithTitle:@"Yes"];
                            [AlertPanel addButtonWithTitle:@"No"];
                            [AlertPanel addButtonWithTitle:@"Yes to all"];
                            [AlertPanel addButtonWithTitle:@"No to all"];
                            Result = [AlertPanel runModal];
                            if (Result == NSAlertFirstButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Yes;
                            }
                            else if (Result == NSAlertSecondButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::No;
                            }
                            else if (Result == NSAlertThirdButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::YesAll;
                            }
                            else
                            {
                                RetValue = EAppMsgReturnType::NoAll;
                            }
                            break;
                            
                        case EAppMsgType::YesNoYesAllNoAllCancel:
                            [AlertPanel addButtonWithTitle:@"Yes"];
                            [AlertPanel addButtonWithTitle:@"No"];
                            [AlertPanel addButtonWithTitle:@"Yes to all"];
                            [AlertPanel addButtonWithTitle:@"No to all"];
                            [AlertPanel addButtonWithTitle:@"Cancel"];
                            Result = [AlertPanel runModal];
                            if (Result == NSAlertFirstButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Yes;
                            }
                            else if (Result == NSAlertSecondButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::No;
                            }
                            else if (Result == NSAlertThirdButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::YesAll;
                            }
                            else if (Result == NSAlertThirdButtonReturn + 1)
                            {
                                RetValue = EAppMsgReturnType::NoAll;
                            }
                            else
                            {
                                RetValue = EAppMsgReturnType::Cancel;
                            }
                            break;
                            
                        case EAppMsgType::YesNoYesAll:
                            [AlertPanel addButtonWithTitle:@"Yes"];
                            [AlertPanel addButtonWithTitle:@"No"];
                            [AlertPanel addButtonWithTitle:@"Yes to all"];
                            Result = [AlertPanel runModal];
                            if (Result == NSAlertFirstButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::Yes;
                            }
                            else if (Result == NSAlertSecondButtonReturn)
                            {
                                RetValue = EAppMsgReturnType::No;
                            }
                            else
                            {
                                RetValue = EAppMsgReturnType::YesAll;
                            }
                            break;
                            
                        default:
                            break;
                    }
                    
                    [AlertPanel release];
                    return RetValue;
                });
                
                return ReturnValue;
            }
        }
        
        void EnableFloatingPointTraps(bool enable)
        {

        }
        
        void GetCurrentUsername(Text& OutName)
        {
            uid_t uid = geteuid();
            struct passwd* pw = getpwuid(uid);
            if (pw)
            {
                OutName = Text(pw->pw_name);
            }
            OutName = "";
        }

        void GetBundlePath(Text& OutPath)
        {
            char buffer[MAX_PATH_LENGTH];
            
            // @note If running as root this returns only user home path
            
            if (!getcwd(buffer, sizeof(buffer)))
            {
                buffer[0] = 0;
            }
            OutPath = Text(buffer);
            
//            @autoreleasepool
//            {
//                // Since getcwd won't work properly in root, we need to write custom path finder.
//                NSString *bundlePath = [[NSBundle mainBundle] resourcePath];
//                // NSString *secondParentPath = [[bundlePath stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
//                // NSString *thirdParentPath = [[secondParentPath stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
//                
//                OutPath = Text([bundlePath UTF8String]);
//            }
        }
        
        
//        /**
//         *  Returns graphical devices information.
//         *  @see https://www.starcoder.com/wordpress/2011/10/using-iokit-to-detect-graphics-hardware/
//         */
//        void GetGPUDevices(TArray<GPUDescriptor>& Adapters)
//        {
//            if (Adapters.GetCount() > 0)
//            {
//                Adapters.ClearAll();
//            }
//            
//            io_iterator_t Iterator;
//            CFMutableDictionaryRef MatchDictionary = IOServiceMatching("IOPCIDevice");
//            
//            if (IOServiceGetMatchingServices(kIOMasterPortDefault
//                                             , MatchDictionary
//                                             , &Iterator) == kIOReturnSuccess)
//            {
//                uint32 Index = 0;
//                io_registry_entry_t RegEntry;
//                while ((RegEntry = IOIteratorNext(Iterator)))
//                {
//                    CFMutableDictionaryRef ServiceInfo;
//                    if (IORegistryEntryCreateCFProperties(RegEntry, &ServiceInfo, kCFAllocatorDefault, kNilOptions) == kIOReturnSuccess)
//                    {
//                        const CFDataRef ClassCode = (const CFDataRef)CFDictionaryGetValue(ServiceInfo, CFSTR("class-code"));
//                        if (ClassCode && CFGetTypeID(ClassCode) == CFDataGetTypeID())
//                        {
//                            const uint32* ClassCodeValue = reinterpret_cast<const uint32*>(CFDataGetBytePtr(ClassCode));
//                            if (ClassCodeValue && *ClassCodeValue == 0x30000)    // GPU
//                            {
//                                GPUDescriptor Desc;
//                                
//                                Desc.DeviceId = Index;
//                                Index++;
//                                
//                                IOObjectRetain(RegEntry);
//                                Desc.PCIDevice = (uint32)RegEntry;
//                                
//                                const CFDataRef Model = (const CFDataRef)CFDictionaryGetValue(ServiceInfo, CFSTR("model"));
//                                if (Model)
//                                {
//                                    if (CFGetTypeID(Model) == CFDataGetTypeID())
//                                    {
//                                        CFStringRef ModelName = CFStringCreateFromExternalRepresentation(kCFAllocatorDefault, Model, kCFStringEncodingASCII);
//                                        
//                                        Desc.Name = CStr([((__bridge NSString*)ModelName) UTF8String]);
//                                    }
//                                    else
//                                    {
//                                        CFRelease(Model);
//                                    }
//                                }
//                                
//                                const CFDataRef DeviceID = (const CFDataRef)CFDictionaryGetValue(ServiceInfo, CFSTR("device-id"));
//                                if (DeviceID && CFGetTypeID(DeviceID) == CFDataGetTypeID())
//                                {
//                                    const uint32* Value = reinterpret_cast<const uint32*>(CFDataGetBytePtr(DeviceID));
//                                    Desc.DeviceId = *Value;
//                                }
//                                
//                                const CFDataRef VendorID = (const CFDataRef)CFDictionaryGetValue(ServiceInfo, CFSTR("vendor-id"));
//                                if (DeviceID && CFGetTypeID(DeviceID) == CFDataGetTypeID())
//                                {
//                                    const uint32* Value = reinterpret_cast<const uint32*>(CFDataGetBytePtr(VendorID));
//                                    Desc.VendorId = *Value;
//                                }
//                                
//                                const CFBooleanRef Headless = (const CFBooleanRef)CFDictionaryGetValue(ServiceInfo, CFSTR("headless"));
//                                if (Headless && CFGetTypeID(Headless) == CFBooleanGetTypeID())
//                                {
//                                    Desc.Headless = (bool)CFBooleanGetValue(Headless);
//                                }
//                                
//                                CFTypeRef Memory = IORegistryEntrySearchCFProperty(RegEntry, kIOServicePlane, CFSTR("VRAM,totalMB"), kCFAllocatorDefault, kIORegistryIterateRecursively);
//                                if (Memory)
//                                {
//                                    if (CFGetTypeID(Memory) == CFDataGetTypeID())
//                                    {
//                                        const uint32* Value = reinterpret_cast<const uint32*>(CFDataGetBytePtr((CFDataRef)Memory));
//                                        Desc.MemoryInMbs = *Value;
//                                    }
//                                    else if (CFGetTypeID(Memory) == CFNumberGetTypeID())
//                                    {
//                                        CFNumberGetValue((CFNumberRef)Memory, kCFNumberSInt32Type, &Desc.MemoryInMbs);
//                                    }
//                                    CFRelease(Memory);
//                                }
//                                
//                                Adapters.PushBack(Desc);
//                            }
//                        }
//                        CFRelease(ServiceInfo);
//                    }
//                    IOObjectRelease(RegEntry);
//                }
//                IOObjectRelease(Iterator);
//            }
//        }
        
        PlatformMemoryStats GetMemoryStats()
        {
            PlatformMemoryHWConstants& MemoryInfo = GetHWStats();
            PlatformMemoryStats MemoryStats;
            
            // Gather platform memory stats.
            vm_statistics Stats;
            mach_msg_type_number_t StatsSize = sizeof(Stats);
            host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&Stats, &StatsSize);
            uint64 FreeMem = Stats.free_count * MemoryInfo.PageSize;
            MemoryStats.AvailablePhysical = FreeMem;
            
            // Get swap file info
            xsw_usage SwapUsage;
            size_t Size = sizeof(SwapUsage);
            sysctlbyname("vm.swapusage", &SwapUsage, &Size, nullptr, 0);
            MemoryStats.AvailableVirtual = FreeMem + SwapUsage.xsu_avail;
            
            // Memory information
            mach_task_basic_info_data_t TaskInfo;
            mach_msg_type_number_t TaskInfoCount = MACH_TASK_BASIC_INFO_COUNT;
            task_info(mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t)&TaskInfo, &TaskInfoCount);
            
            MemoryStats.UsedPhysical = TaskInfo.resident_size;
            if (MemoryStats.UsedPhysical > MemoryStats.PeakUsedPhysical)
            {
                MemoryStats.PeakUsedPhysical = MemoryStats.UsedPhysical;
            }
            
            MemoryStats.UsedVirtual = TaskInfo.virtual_size;
            if (MemoryStats.UsedVirtual > MemoryStats.PeakUsedVirtual)
            {
                MemoryStats.PeakUsedVirtual = MemoryStats.UsedVirtual;
            }
            
            return MemoryStats;
        }
        
        PlatformMemoryHWConstants& GetHWStats()
        {
            static PlatformMemoryHWConstants MemoryConstants;
            
            if (MemoryConstants.TotalPhysical == 0)
            {
                // Get page size.
                vm_size_t PageSize;
                host_page_size(mach_host_self(), &PageSize);
                
                // Get swap file info
                xsw_usage SwapUsage;
                size_t Size = sizeof(SwapUsage);
                sysctlbyname("vm.swapusage", &SwapUsage, &Size, nullptr, 0);
                
                // Get memory.
                int64 AvailablePhysical = 0;
                int Mib[] = { CTL_HW, HW_MEMSIZE };
                size_t Length = sizeof(int64);
                sysctl(Mib, 2, &AvailablePhysical, &Length, nullptr, 0);
                
                MemoryConstants.TotalPhysical = AvailablePhysical;
                MemoryConstants.TotalVirtual = AvailablePhysical + SwapUsage.xsu_total;
                MemoryConstants.PageSize = (uint32)PageSize;
                
                MemoryConstants.TotalPhysicalGB = (MemoryConstants.TotalPhysical + 1024 * 1024 * 1024 - 1) / 1024 / 1024 / 1024;
            }
            
            return MemoryConstants;
        }
        

        
        bool ExecProcess(const char* URL, const char* Params, int32* OutReturnCode, Text* OutStdOut, Text* OutStdErr)
        {
            SCOPED_AUTORELEASE_POOL;
            
            Text ProcessPath = URL;
            NSString* LaunchPath = ProcessPath.GetNSString();
            
            if (![[NSFileManager defaultManager] fileExistsAtPath: LaunchPath])
            {
                NSString* AppName = [[LaunchPath lastPathComponent] stringByDeletingPathExtension];
                LaunchPath = [[NSWorkspace sharedWorkspace] fullPathForApplication:AppName];
            }
            
            if ([[NSFileManager defaultManager] fileExistsAtPath: LaunchPath])
            {
                if ([[NSWorkspace sharedWorkspace] isFilePackageAtPath: LaunchPath])
                {
                    NSBundle* Bundle = [NSBundle bundleWithPath:LaunchPath];
                    LaunchPath = Bundle ? [Bundle executablePath] : NULL;
                }
            }
            else
            {
                LaunchPath = NULL;
            }
            
            if (LaunchPath == NULL)
            {
                if (OutReturnCode)
                {
                    *OutReturnCode = ENOENT;
                }
                if (OutStdErr)
                {
                    *OutStdErr = "No such executable";
                }
                return false;
            }
            
            NSTask* ProcessHandle = [[NSTask new] autorelease];
            if (ProcessHandle)
            {
                [ProcessHandle setLaunchPath: LaunchPath];
                
                TArray<Text> ArgsArray(SharedAllocator);
                Text(Params).ParseIntoArray(ArgsArray, " ", true);
                
                NSMutableArray *Arguments = [[NSMutableArray new] autorelease];
                
                Text MultiPartArg;
                for (int32 Index = 0; Index < ArgsArray.GetSize(); Index++)
                {
                    if (MultiPartArg.IsEmpty())
                    {
                        if ((StartsWith(*ArgsArray[Index], "\"") && !EndsWith(*ArgsArray[Index], "\"")) // check for a starting quote but no ending quote, excludes quoted single arguments
                            || (ArgsArray[Index].Find("=\"") != INDEX_NONE && !EndsWith(ArgsArray[Index], "\"")) // check for quote after =, but no ending quote, this gets arguments of the type -blah="string string string"
                            || EndsWith(ArgsArray[Index], "=\"")) // check for ending quote after =, this gets arguments of the type -blah=" string string string "
                        {
                            MultiPartArg = ArgsArray[Index];
                        }
                        else
                        {
                            NSString* Arg;
                            if (ArgsArray[Index].Find("=\"") != INDEX_NONE)
                            {
                                Text SingleArg = ArgsArray[Index];
                                SingleArg = SingleArg.Replace("=\"", "=");
                                Arg = (NSString*)TCHARToCFString(*SingleArg.TrimQuotes(NULL));
                            }
                            else
                            {
                                Arg = (NSString*)TCHARToCFString(*ArgsArray[Index].TrimQuotes(NULL));
                            }
                            [Arguments addObject: Arg];
                            CFRelease((CFStringRef)Arg);
                        }
                    }
                    else
                    {
                        MultiPartArg += (" ");
                        MultiPartArg += ArgsArray[Index];
                        if (EndsWith(ArgsArray[Index], "\""))
                        {
                            NSString* Arg;
                            if (StartsWith(MultiPartArg, "\""))
                            {
                                Arg = (NSString*)TCHARToCFString(*MultiPartArg.TrimQuotes(NULL));
                            }
                            else
                            {
                                Arg = (NSString*)TCHARToCFString(*MultiPartArg.Replace("\"", ""));
                            }
                            [Arguments addObject: Arg];
                            CFRelease((CFStringRef)Arg);
                            MultiPartArg.Clear();
                        }
                    }
                }
                
                [ProcessHandle setArguments: Arguments];
                
                NSAutoReadPipe* StdOutPipe = [[NSAutoReadPipe new] autorelease];
                [ProcessHandle setStandardOutput: (id)[StdOutPipe Pipe]];
                
                NSAutoReadPipe* StdErrPipe = [[NSAutoReadPipe new] autorelease];
                [ProcessHandle setStandardError: (id)[StdErrPipe Pipe]];
                
                @try
                {
                    [ProcessHandle launch];
                    
                    [ProcessHandle waitUntilExit];
                    
                    if (OutReturnCode)
                    {
                        *OutReturnCode = [ProcessHandle terminationStatus];
                    }
                    
                    if (OutStdOut)
                    {
                        [StdOutPipe copyPipeData: *OutStdOut];
                    }
                    
                    if (OutStdErr)
                    {
                        [StdErrPipe copyPipeData: *OutStdErr];
                    }
                    
                    return true;
                }
                @catch (NSException* Exc)
                {
                    if (OutReturnCode)
                    {
                        *OutReturnCode = ENOENT;
                    }
                    if (OutStdErr)
                    {
                        *OutStdErr = Text([Exc reason]);
                    }
                    return false;
                }
            }
            return false;
        }
        
        
        void GetSystemNameVersionString(Text& Version)
        {
            NSString* operatingSystemVersionString = [[NSProcessInfo processInfo] operatingSystemVersionString ];
            Version = Text([operatingSystemVersionString  UTF8String]);
        }
        
        void CFStringToTCHAR(CFStringRef CFStr, wchar_t* TChar)
        {
            const size_t Length = CFStringGetLength( CFStr );
            CFRange Range = CFRangeMake( 0, Length );
            CFStringGetBytes( CFStr, Range, kCFStringEncodingUTF32LE, '?', false, ( uint8 *)TChar, Length * sizeof( wchar_t ) + 1, NULL );
            TChar[Length] = 0;
        }
        
        CFStringRef TCHARToCFString(const wchar_t* TChar)
        {
            const size_t Length = wcslen(TChar);    // @todo wchar (wcslen)
            CFStringRef String = CFStringCreateWithBytes(kCFAllocatorDefault, (const uint8 *)TChar, Length * sizeof(wchar_t), kCFStringEncodingUTF32LE, false);
            assert(String);
            return String;
        }
        
        void CFStringToTCHAR(CFStringRef CFStr, char* TChar)
        {
            const size_t Length = CFStringGetLength( CFStr );
            CFRange Range = CFRangeMake( 0, Length );
            CFStringGetBytes( CFStr, Range, kCFStringEncodingUTF8, '?', false, ( uint8 *)TChar, Length * sizeof( char ) + 1, NULL );
            TChar[Length] = 0;
            
        }
        
        CFStringRef TCHARToCFString(const char* TChar)
        {
            const size_t Length = strlen(TChar);
            CFStringRef String = CFStringCreateWithBytes(kCFAllocatorDefault, (const uint8 *)TChar, Length * sizeof(char), kCFStringEncodingUTF8, false);
            assert(String);
            return String;
        }
        
        // this is from UE
        struct SMacInfo
        {
            void Init()
            {
                @autoreleasepool
                {
                    {
                        std::nothrow_t t;
                        char* d = (char*)(operator new(8, t));
                        delete d;
                        
                        d = (char*)operator new[](8, t);
                        delete [] d;
                    }
                }
                
                
                AppPath = Text([[NSBundle mainBundle] executablePath]);
                
                const NSString* BundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
                AppBundleID = BundleIdentifier != nil ? Text(BundleIdentifier) : Text("");
                
                NumCores = GetNumberOfLogicalCores();
                
//                RunUUID = RunGUID();
                
                // @todo GPU info
                OSXVersion = [[NSProcessInfo processInfo] operatingSystemVersion];
                OSVersion = Neko::va("%ld.%ld.%ld", OSXVersion.majorVersion, OSXVersion.minorVersion, OSXVersion.patchVersion);
                CopyString(OSVersionUTF8, PATH_MAX+1, *OSVersion);
                
                char TempSysCtlBuffer[PATH_MAX] = {};
                size_t TempSysCtlBufferSize = PATH_MAX;
                
                pid_t ParentPID = getppid();
                proc_pidpath(ParentPID, TempSysCtlBuffer, PATH_MAX);
                ParentProcess = TempSysCtlBuffer;
                
                MachineUUID = "00000000-0000-0000-0000-000000000000";
                io_service_t PlatformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,IOServiceMatching("IOPlatformExpertDevice"));
                if (PlatformExpert)
                {
                    CFTypeRef SerialNumberAsCFString = IORegistryEntryCreateCFProperty(PlatformExpert,CFSTR(kIOPlatformUUIDKey),kCFAllocatorDefault, 0);
                    if (SerialNumberAsCFString)
                    {
                        MachineUUID = Text((NSString*)SerialNumberAsCFString);
                        CFRelease(SerialNumberAsCFString);
                    }
                    IOObjectRelease(PlatformExpert);
                }
                
                
                sysctlbyname("kern.osrelease", TempSysCtlBuffer, &TempSysCtlBufferSize, NULL, 0);
                BiosRelease = TempSysCtlBuffer;
                uint32 KernelRevision = 0;
                TempSysCtlBufferSize = 4;
                sysctlbyname("kern.osrevision", &KernelRevision, &TempSysCtlBufferSize, NULL, 0);
                BiosRevision = Neko::va("%d", KernelRevision);
                TempSysCtlBufferSize = PATH_MAX;
                sysctlbyname("kern.uuid", TempSysCtlBuffer, &TempSysCtlBufferSize, NULL, 0);
                BiosUUID = TempSysCtlBuffer;
                TempSysCtlBufferSize = PATH_MAX;
                sysctlbyname("hw.model", TempSysCtlBuffer, &TempSysCtlBufferSize, NULL, 0);
                MachineModel = TempSysCtlBuffer;
                TempSysCtlBufferSize = PATH_MAX+1;
                sysctlbyname("machdep.cpu.brand_string", MachineCPUString, &TempSysCtlBufferSize, NULL, 0);
                
                gethostname(MachineName, lengthOf(MachineName));
                
                // Notification handler to check we are running from a battery - this only applies to MacBook's.
                notify_handler_t PowerSourceNotifyHandler = ^(int32 Token){
                    bRunningOnBattery = false;
                    CFTypeRef PowerSourcesInfo = IOPSCopyPowerSourcesInfo();
                    if (PowerSourcesInfo)
                    {
                        CFArrayRef PowerSourcesArray = IOPSCopyPowerSourcesList(PowerSourcesInfo);
                        for (CFIndex Index = 0; Index < CFArrayGetCount(PowerSourcesArray); Index++)
                        {
                            CFTypeRef PowerSource = CFArrayGetValueAtIndex(PowerSourcesArray, Index);
                            NSDictionary* Description = (NSDictionary*)IOPSGetPowerSourceDescription(PowerSourcesInfo, PowerSource);
                            if ([(NSString*)[Description objectForKey: @kIOPSPowerSourceStateKey] isEqualToString: @kIOPSBatteryPowerValue])
                            {
                                bRunningOnBattery = true;
                                break;
                            }
                        }
                        CFRelease(PowerSourcesArray);
                        CFRelease(PowerSourcesInfo);
                    }
                };
                
                // Call now to fetch the status
                PowerSourceNotifyHandler(0);
                
                uint32 Status = notify_register_dispatch(kIOPSNotifyPowerSource, &PowerSourceNotification, dispatch_get_main_queue(), PowerSourceNotifyHandler);
                assert(Status == NOTIFY_STATUS_OK);
                
                
                ExecProcess("/usr/bin/xcode-select", "--print-path", nullptr, &XcodePath, nullptr);
                if (XcodePath.Length() > 0)
                {
                    XcodePath.Erase(XcodePath.Length() - 1, 1); // Remove \n at the end of the string
                }
//
//                PLCrashReporterConfig* Config = [[[PLCrashReporterConfig alloc] initWithSignalHandlerType: PLCrashReporterSignalHandlerTypeBSD
//                                                                                    symbolicationStrategy: PLCrashReporterSymbolicationStrategyNone
//                                                                                        crashReportFolder:FMacApplicationInfo::TemporaryCrashReportFolder().GetNSString()
//                                                                                          crashReportName:FMacApplicationInfo::TemporaryCrashReportName().GetNSString()] autorelease];
//                FMacApplicationInfo::CrashReporter = [[PLCrashReporter alloc] initWithConfiguration: Config];
//                
//                PLCrashReporterCallbacks CrashReportCallback = {
//                    .version = 0,
//                    .context = nullptr,
//                    .handleSignal = PLCrashReporterHandler
//                };
//                
//                [FMacApplicationInfo::CrashReporter setCrashCallbacks: &CrashReportCallback];

                
                NSString* PLCrashReportFile = [TemporaryCrashReportFolder().GetNSString() stringByAppendingPathComponent:TemporaryCrashReportName().GetNSString()];
                [PLCrashReportFile getCString:PLCrashReportPath maxLength:PATH_MAX encoding:NSUTF8StringEncoding];
                
            }
            
            
            ~SMacInfo()
            {
                if (PowerSourceNotification)
                {
                    notify_cancel(PowerSourceNotification);
                    PowerSourceNotification = 0;
                }
            }
            
            static SGuid RunGUID()
            {
                static SGuid Guid;
                if (!Guid.IsValid())
                {
                    CreateGuid(Guid);
                }
                return Guid;
            }
            
            
            static Text TemporaryCrashReportFolder()
            {
                static Text PLCrashReportFolder;
                if (PLCrashReportFolder.IsEmpty())
                {
                    SCOPED_AUTORELEASE_POOL;
                    
                    NSArray* Paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                    NSString* CacheDir = [Paths objectAtIndex: 0];
                    
                    NSString* BundleID = [[NSBundle mainBundle] bundleIdentifier];
                    if (!BundleID)
                    {
                        BundleID = [[NSProcessInfo processInfo] processName];
                    }
                    assert(BundleID);
                    
                    NSString* PLCrashReportFolderPath = [CacheDir stringByAppendingPathComponent: BundleID];
                    PLCrashReportFolder = Text(PLCrashReportFolderPath);
                }
                return PLCrashReportFolder;
            }
            
            
            static Text TemporaryCrashReportName()
            {
                static Text PLCrashReportFileName(RunGUID().ToString().Append(".plcrash"));
                return PLCrashReportFileName;
            }
            
            
            bool bRunningOnBattery;
            bool bIsSandboxed;
            
            int32 PowerSourceNotification;
            int32 NumCores;
            int64 SystemLogSize;
            
            char AppNameUTF8[PATH_MAX+1];
            char AppLogPath[PATH_MAX+1];
            char CrashReportPath[PATH_MAX+1];
            char PLCrashReportPath[PATH_MAX+1];
            char CrashReportClient[PATH_MAX+1];
            char CrashReportVideo[PATH_MAX+1];
            char OSVersionUTF8[PATH_MAX+1];
            char MachineName[PATH_MAX+1];
            char MachineCPUString[PATH_MAX+1];
            Text AppPath;
            Text AppName;
            Text AppBundleID;
            Text OSVersion;
            Text OSBuild;
            Text MachineUUID;
            Text MachineModel;
            Text BiosRelease;
            Text BiosRevision;
            Text BiosUUID;
            Text ParentProcess;
            Text LCID;
            Text CommandLine;
            Text BranchBaseDir;
            Text PrimaryGPU;
            NSOperatingSystemVersion OSXVersion;
            Text XcodePath;
            
            
//            static PLCrashReporter* CrashReporter;
        };
        
        static SMacInfo GMacInfo;
        
        void Init()
        {
            signal(SIGPIPE, SIG_IGN);
            
            GMacInfo.Init();
            
            // Increase the maximum number of simultaneously open files
            uint32 MaxFilesPerProc = OPEN_MAX;
            size_t UInt32Size = sizeof(uint32);
            sysctlbyname("kern.maxfilesperproc", &MaxFilesPerProc, &UInt32Size, NULL, 0);
            
            EnablePlatformSymbolication(true);
        }
        
        
        void Teardown()
        {
            EnablePlatformSymbolication(false);
        }
        
        
        int32 GetNumberOfLogicalCores()
        {
            static int32 NumberOfCores = -1;
            if (NumberOfCores == -1)
            {
                size_t Size = sizeof(int32);
                
                if (sysctlbyname("hw.ncpu", &NumberOfCores, &Size, NULL, 0) != 0)
                {
                    NumberOfCores = 1;
                }
            }
            return NumberOfCores;
        }
        
        
        int32 GetNumberOfPhysicalCores()
        {
            static int32 NumberOfCores = -1;
            if (NumberOfCores == -1)
            {
                size_t Size = sizeof(int32);
                
                if (sysctlbyname("hw.physicalcpu", &NumberOfCores, &Size, NULL, 0) != 0)
                {
                    NumberOfCores = 1;
                }
            }
            return NumberOfCores;
        }
        
        
        void CreateGuid(SGuid& Result)
        {
            uuid_t UUID;
            uuid_generate(UUID);
            
            uint32* Values = (uint32*)(&UUID[0]);
            Result[0] = Values[0];
            Result[1] = Values[1];
            Result[2] = Values[2];
            Result[3] = Values[3];
        }
    }
    
    
    double AppleSpecificPlatform::SecondsPerCycle = 0;
    
    double AppleSpecificPlatform::InitTiming()
    {
        // Time base is in nano seconds.
        mach_timebase_info_data_t Info;
        assert (mach_timebase_info(&Info) == 0);
        SecondsPerCycle = 1e-9 * (double)Info.numer / (double)Info.denom;
        return Seconds();
    }
    
    
    
    namespace Math
    {
        int32 TruncToInt(float F)
        {
            return _mm_cvtt_ss2si(_mm_set_ss(F));
        }
        
        int32 FloorToInt(float F)
        {
            return _mm_cvt_ss2si(_mm_set_ss(F + F - 0.5f)) >> 1;
        }
        
        float InvSqrt(float F)
        {
            // Performs two passes of Newton-Raphson iteration on the hardware estimate
            //    v^-0.5 = x
            // => x^2 = v^-1
            // => 1/(x^2) = v
            // => F(x) = x^-2 - v
            //    F'(x) = -2x^-3
            
            //    x1 = x0 - F(x0)/F'(x0)
            // => x1 = x0 + 0.5 * (x0^-2 - Vec) * x0^3
            // => x1 = x0 + 0.5 * (x0 - Vec * x0^3)
            // => x1 = x0 + x0 * (0.5 - 0.5 * Vec * x0^2)
            //
            // This final form has one more operation than the legacy factorization (X1 = 0.5*X0*(3-(Y*X0)*X0)
            // but retains better accuracy (namely InvSqrt(1) = 1 exactly).
            
            const __m128 fOneHalf = _mm_set_ss(0.5f);
            __m128 Y0, X0, X1, X2, FOver2;
            float temp;
            
            Y0 = _mm_set_ss(F);
            X0 = _mm_rsqrt_ss(Y0);    // 1/sqrt estimate (12 bits)
            FOver2 = _mm_mul_ss(Y0, fOneHalf);
            
            // 1st Newton-Raphson iteration
            X1 = _mm_mul_ss(X0, X0);
            X1 = _mm_sub_ss(fOneHalf, _mm_mul_ss(FOver2, X1));
            X1 = _mm_add_ss(X0, _mm_mul_ss(X0, X1));
            
            // 2nd Newton-Raphson iteration
            X2 = _mm_mul_ss(X1, X1);
            X2 = _mm_sub_ss(fOneHalf, _mm_mul_ss(FOver2, X2));
            X2 = _mm_add_ss(X1, _mm_mul_ss(X1, X2));
            
            _mm_store_ss(&temp, X2);
            return temp;
        }
    }
    
}
#endif
