//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MacWindow.cpp
//  Neko engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#include "../../../Core/Log.h"
#include "../../SystemShared.h"

#if NEKO_OSX

#include "../../Application.h"
#include "Runloop.h"
#include "MacApplication.h"
#include "MacWindow.h"

namespace Neko
{
    CMacWindow::CMacWindow()
    : Handle(nullptr)
    , bIsVisible(true)
    , bIsClosed(false)
    , PositionX(0)
    , PositionY(0)
    {
        PreFullscreenWindowRect.origin.x = PreFullscreenWindowRect.origin.y = PreFullscreenWindowRect.size.width = PreFullscreenWindowRect.size.height = 0.0f;
    }
    
    
    CMacWindow::~CMacWindow()
    {
        
    }
    
    
    void CMacWindow::Create(const FWindowDesc& Definition)
    {
        this->Definition = Definition;
        
        NSRect rect;
        
        const int32 SizeX = Math::Maximum(Math::TruncToInt( Definition.SizeW ), 1);
        const int32 SizeY = Math::Maximum(Math::TruncToInt( Definition.SizeH ), 1);
        
        rect.origin.x = Definition.OriginX;
        rect.origin.y = Definition.OriginY;
        rect.size.width = SizeX;
        rect.size.height = SizeY;
        
        NSArray* screens = [NSScreen screens];
        // Figure out which screen to place this windowю
        NSScreen* screen = nil;
        for (NSScreen *candidate in screens)
        {
            NSRect screenRect = [candidate frame];
            if (rect.origin.x >= screenRect.origin.x
                && rect.origin.x < screenRect.origin.x + screenRect.size.width
                && rect.origin.y >= screenRect.origin.y
                && rect.origin.y < screenRect.origin.y + screenRect.size.height)
            {
                screen = candidate;
            }
        }
        
        const Vec2 CocoaPosition = CMacApplication::ConvertSlatePositionToCocoa(PositionX, PositionY);
        const NSRect ViewRect = NSMakeRect(CocoaPosition.x, CocoaPosition.y - (SizeY / screen.backingScaleFactor) + 1, SizeX / screen.backingScaleFactor, SizeY / screen.backingScaleFactor);
        
        PositionX = Definition.OriginX;
        PositionY = Definition.OriginY;
        
		MainThreadCall(^{
			@autoreleasepool {
				CCocoaWindow* CocoaWindow = nullptr;
				
				NSString* WindowTitle;
				WindowTitle = [NSString stringWithCString:Definition.Title.c_str() encoding:NSUTF8StringEncoding];
				
				uint32 WindowStyle = 0;
				if (Definition.IsRegularWindow)
				{
					if ( Definition.HasCloseButton )
					{
						WindowStyle = NSWindowStyleMaskClosable;
					}
					
					// In order to support rounded, shadowed windows set the window to be titled - we'll set the graphics context view to cover the whole window
					WindowStyle |= NSWindowStyleMaskTitled | NSWindowStyleMaskTexturedBackground;
					
					if ( Definition.SupportsMinimisation )
					{
						WindowStyle |= NSWindowStyleMaskMiniaturizable;
					}
					if ( Definition.SupportsMaximisation || Definition.HasSizingFrame )
					{
						WindowStyle |= NSWindowStyleMaskResizable;
					}
				}
				else
				{
					WindowStyle = NSWindowStyleMaskBorderless;
				}
				
				if ( Definition.HasBorder )
				{
					WindowStyle |= NSWindowStyleMaskTitled;
					WindowStyle &= ~NSWindowStyleMaskTexturedBackground;// : ~NSFullSizeContentViewWindowMask;
				}
				
				@try
				{
					CocoaWindow = [[CCocoaWindow alloc] initWithContentRect: ViewRect styleMask: WindowStyle backing: NSBackingStoreBuffered defer: NO screen:screen];
				}
				@catch(NSException* e)
				{
					GLogError.log("System") << "Couldn't create window: " << [[e reason] UTF8String];
				}
				
				if ( CocoaWindow != nullptr )
				{
					[CocoaWindow setReleasedWhenClosed: NO];
					[CocoaWindow setWindowMode: ENativeWindowMode::Windowed];
					[CocoaWindow setAcceptsInput: Definition.AcceptsInput];
					[CocoaWindow setDisplayReconfiguring: false];
					[CocoaWindow setAcceptsMouseMovedEvents: YES];
					[CocoaWindow setIgnoresMouseEvents: NO];
					[CocoaWindow setDelegate: CocoaWindow];
					[CocoaWindow setBackgroundColor:[NSColor blackColor]];
					
					int32 WindowLevel = NSNormalWindowLevel;
					
					if (Definition.IsModal)
					{
						WindowLevel = NSFloatingWindowLevel;
					}
					else
					{
						switch (Definition.Type)
						{
							case EWindowType::Normal:
							{
								WindowLevel = NSNormalWindowLevel;
								break;
							}
							case EWindowType::Menu:
							{
								WindowLevel = NSStatusWindowLevel;
								break;
							}
							case EWindowType::ToolTip:
							{
								WindowLevel = NSPopUpMenuWindowLevel;
								break;
							}
							case EWindowType::Notification:
							{
								WindowLevel = NSModalPanelWindowLevel;
								break;
							}
							case EWindowType::Main:
							{
								WindowLevel = NSMainMenuWindowLevel;
								break;
							}
						}
					}
					
					[CocoaWindow setLevel:WindowLevel];
					
					if (!Definition.HasBorder)
					{
						[CocoaWindow setBackgroundColor: [NSColor clearColor]];
						[CocoaWindow setHasShadow: YES];
					}
					
					[CocoaWindow setOpaque: NO];
					ReshapeWindow(PositionX, PositionY, SizeX, SizeY);
					
					if (Definition.IsRegularWindow)
					{
						[NSApp addWindowsItem:CocoaWindow title:@GAME_PROJECT_NAME filename:NO];
						
						// Tell Cocoa that we are opting into drag and drop.
						// Only makes sense for regular windows (windows that last a while.)
						[CocoaWindow registerForDraggedTypes:@[NSFilenamesPboardType, NSPasteboardTypeString]];
						
						if ( Definition.HasBorder )
						{
							[CocoaWindow setCollectionBehavior: NSWindowCollectionBehaviorDefault|NSWindowCollectionBehaviorManaged|NSWindowCollectionBehaviorParticipatesInCycle];
						}
						else
						{
							[CocoaWindow setCollectionBehavior: NSWindowCollectionBehaviorFullScreenAuxiliary|NSWindowCollectionBehaviorDefault|NSWindowCollectionBehaviorManaged|NSWindowCollectionBehaviorParticipatesInCycle];
						}
					}
					else if (Definition.AppearsInTaskbar)
					{
						if (!Definition.Title.IsEmpty())
						{
							[NSApp addWindowsItem:CocoaWindow title:[NSString stringWithUTF8String:Definition.Title.c_str()] filename:NO];
						}
						
						[CocoaWindow setCollectionBehavior:NSWindowCollectionBehaviorFullScreenAuxiliary|NSWindowCollectionBehaviorDefault|NSWindowCollectionBehaviorManaged|NSWindowCollectionBehaviorParticipatesInCycle];
					}
					else
					{
						[CocoaWindow setCollectionBehavior:NSWindowCollectionBehaviorCanJoinAllSpaces|NSWindowCollectionBehaviorTransient|NSWindowCollectionBehaviorIgnoresCycle];
					}
					
					[CocoaWindow setAlphaValue:Definition.Opacity];
					
					
					//  Window settings.
					[CocoaWindow setTitle:WindowTitle];

					// @todo Should be optional
//					NSString* StyleMode = [[NSUserDefaults standardUserDefaults] stringForKey:@"AppleInterfaceStyle"];
//					if ([StyleMode isEqualToString:@"Dark"])
//					{
//                        CocoaWindow.appearance = [NSAppearance appearanceNamed:NSAppearanceNameVibrantDark];
//					}
					
					Handle = CocoaWindow;
				}
				else
				{
					GLogError.log("System") << "Could not create window.";
				}
			}
		}, NekoShowEventMode, true);
	}
	
    
    void CMacWindow::Maximize()
    {
        MainThreadCall(^{
            @autoreleasepool
            {
                [Handle zoom:nil];
                //            ScheduleContextUpdates(windata);
            }
        }, NekoResizeEventMode, true);
    }
    
    
    void CMacWindow::BringToFront(bool bForce)
    {
        if (!bIsClosed && bIsVisible)
        {
            MainThreadCall(^{
                @autoreleasepool
                {
                    [Handle orderFrontAndMakeMain:IsRegularWindow() andKey:IsRegularWindow()];
                    
                    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
                    if (bForce)
                    {
                        // Focus to our window.
                        [NSApp activateIgnoringOtherApps:YES];
                    }
                };
            }, NekoShowEventMode, true);
        }
    }
    
    
    static float GetDPIScaleFactorAtPoint(float X, float Y)
    {
		if (MacApplication && MacApplication->IsHighDPIModeEnabled())
		{
			NSRect framePixels;
			NSScreen* Screen = CMacApplication::FindScreenBySlatePosition(X, Y, framePixels);
			return Screen.backingScaleFactor;
		}
		return 1.0f;
    }
	
    
    void CMacWindow::ReshapeWindow(int32 X, int32 Y, int32 Width, int32 Height)
    {
        if (Handle)
        {
            @autoreleasepool {
                const float DPIScaleFactor = GetDPIScaleFactorAtPoint(X, Y);
                Width /= DPIScaleFactor;
                Height /= DPIScaleFactor;
				
                if (GetWindowMode() == ENativeWindowMode::Windowed || GetWindowMode() == ENativeWindowMode::WindowedFullscreen)
                {
                    const Vec2 CocoaPosition = CMacApplication::ConvertSlatePositionToCocoa(X, Y);
                    NSRect Rect = NSMakeRect(CocoaPosition.x, CocoaPosition.y - Height + 1, Math::Maximum(Width, 1), Math::Maximum(Height, 1));
					
                    if (Definition.HasBorder)
                    {
                        Rect = [Handle frameRectForContentRect: Rect];
                    }
					
                    if ( !NSEqualRects([Handle frame], Rect) )
                    {
                        MainThreadCall(^{
                            @autoreleasepool {
                                BOOL DisplayIfNeeded = (GetWindowMode() == ENativeWindowMode::Windowed);
								
                                [Handle setFrame: Rect display:DisplayIfNeeded];
								
                                // Force resize back to screen size in fullscreen - not ideally pretty but means we don't
                                // have to subvert the OS X or UE fullscreen handling events elsewhere.
                                if (GetWindowMode() != ENativeWindowMode::Windowed)
                                {
                                    [Handle setFrame: [Handle screen].frame display:YES];
                                }
                                else if (Definition.ShouldPreserveAspectRatio)
                                {
                                    [Handle setContentAspectRatio:NSMakeSize((float)Width / (float)Height, 1.0f)];
                                }
								
                                Handle->bZoomed = [Handle isZoomed];
                            }
                        }, NekoResizeEventMode, true);
                    }
                }
                else
                {
                    NSRect NewRect = NSMakeRect(Handle.PreFullScreenRect.origin.x, Handle.PreFullScreenRect.origin.y, (CGFloat)Width, (CGFloat)Height);
                    if ( !NSEqualRects(Handle.PreFullScreenRect, NewRect) )
                    {
                        Handle.PreFullScreenRect = NewRect;
                        MainThreadCall(^{
                            CMacCursor* MacCursor = (CMacCursor*)MacApplication->Cursor;
                            if ( MacCursor )
                            {
//                                NSSize WindowSize = [Handle frame].size;
//                                NSSize ViewSize = [[Handle contentView] frame].size;
//                                float WidthScale = ViewSize.width / WindowSize.width;
//                                float HeightScale = ViewSize.height / WindowSize.height;
                                //                            MacCursor->SetMouseScaling(FVector2D(WidthScale, HeightScale), WindowHandle);
                            }
                        }, NekoResizeEventMode, true);
                        MacApplication->PreProcessEvent([NSNotification notificationWithName:NSWindowDidResizeNotification object:Handle]);
                    }
                }
            }
        }
    }
    
    
    void CMacWindow::MoveWindowTo(int32 X, int32 Y)
    {
        MainThreadCall(^{
            @autoreleasepool {
                NSScreen* Screen = [Handle screen];
                
                NSRect ScreenRect = [Screen frame];
                CGFloat MenuBarHeight = [[[NSApplication sharedApplication] mainMenu] menuBarHeight];
                
                NSPoint Position = { float(X), ScreenRect.size.height - MenuBarHeight - float(Y) };
                
                [Handle setFrameTopLeftPoint: Position];
            }
        }, NekoResizeEventMode, true);
    }
    
    
    Int2 CMacWindow::GetSize()
    {
        Int2 result;
        
        NSRect rect = (GetWindowMode() == ENativeWindowMode::Windowed) ? [Handle openGLFrame] : [Handle frame];
        
        result.x = (int32)rect.size.width;
        result.y = (int32)rect.size.height;
        
        return result;
    }
    
    
    Vec2 CMacWindow::GetLocation()
    {
        Vec2 result;
        
        NSRect rect = [Handle contentRectForFrameRect:[Handle frame]];
        ConvertNSRect([Handle screen], &rect);
        
        result.x = rect.origin.x;
        result.y = rect.origin.y;
        
        return result;
    }
    
    
    void CMacWindow::SetTitle(const char* Title)
    {
		if ( Text([Handle title]) != Text(Title) )
		{
            MainThreadCall(^ {
                @autoreleasepool {
                    NSString* title = [[NSString alloc] initWithCString:Title encoding:1];
                    [Handle setTitle: title];
                    NEKO_RELEASE(title);
                }
            }, NekoNilEventMode, true);
        }

    }
    
    
    bool CMacWindow::IsRegularWindow() const
    {
        return Definition.IsRegularWindow;
    }
    
    
    float CMacWindow::GetDPIScaleFactor() const
    {
       return MacApplication->IsHighDPIModeEnabled() ? Handle.backingScaleFactor : 1.0f;
    }
    
    
    void CMacWindow::Show()
    {
        @autoreleasepool
        {
            if (!bIsClosed && !bIsVisible)
            {
                const bool bMakeMainAndKey = ([Handle canBecomeKeyWindow] && Definition.ActivateWhenFirstShown);
                
                MainThreadCall(^ {
                    @autoreleasepool {
                        [Handle orderFrontAndMakeMain:bMakeMainAndKey andKey:bMakeMainAndKey];
                    }
                }, NekoShowEventMode, false);
                
                bIsVisible = true;
            }
        }
    }
    
    
    void CMacWindow::Hide()
    {
        if (bIsVisible)
        {
            @autoreleasepool
            {
                bIsVisible = false;
                MainThreadCall(^{
                    @autoreleasepool {
                        [Handle orderOut:nil];
                    }
                }, NekoCloseEventMode, false);
            }
        }
    }
    
    
    void CMacWindow::SetWindowMode(ENativeWindowMode NewWindowMode)
    {
        @autoreleasepool
        {
            // In OS X fullscreen and windowed fullscreen are the same
            bool bMakeFullscreen = NewWindowMode != ENativeWindowMode::Windowed;
            bool bIsFullscreen = GetWindowMode() != ENativeWindowMode::Windowed;
            
            if (bIsFullscreen == bMakeFullscreen && NewWindowMode != GetWindowMode())
            {
                SetWindowMode(ENativeWindowMode::Windowed);
            }
            
            if (bIsFullscreen != bMakeFullscreen || NewWindowMode != GetWindowMode())
            {
                NSWindowCollectionBehavior Behaviour = [Handle collectionBehavior];
                if (bMakeFullscreen)
                {
                    Behaviour &= ~(NSWindowCollectionBehaviorFullScreenAuxiliary);
                    Behaviour |= NSWindowCollectionBehaviorFullScreenPrimary;
                }
                
                if (!bIsFullscreen)
                {
                    PreFullscreenWindowRect.origin = [Handle frame].origin;
                    PreFullscreenWindowRect.size = [Handle openGLFrame].size;
                    Handle.PreFullScreenRect = PreFullscreenWindowRect;
                }
                
                Handle.TargetWindowMode = NewWindowMode;
                
                // Push system event
                MainThreadCall(^{
                    @autoreleasepool {
                        [Handle setCollectionBehavior: Behaviour];
                        [Handle toggleFullScreen:nil];
                    }
                }, NekoFullscreenEventMode, true);
                
                bool WindowIsFullScreen = !bMakeFullscreen;
                // Make sure window transition is completed
                do
                {
                    MainThreadCall(^{
                        // Pump system events so window will complete the transition
                        MainApplication::Get().PumpMessages();
                        // Also make sure that all system events are passed through
                        MainApplication::Get().Tick();
                    });
                    WindowIsFullScreen = [Handle windowMode] != ENativeWindowMode::Windowed;
                } while(WindowIsFullScreen != bMakeFullscreen);
            }
            else // Already in/out fullscreen but a different mode - we should just update the mode rather than forcing the window to change again
            {
                Handle.TargetWindowMode = NewWindowMode;
                [Handle setWindowMode:NewWindowMode];
            }
        }
    }
	
    
	void CMacWindow::OnDisplayReconfiguration(CGDirectDisplayID Display, CGDisplayChangeSummaryFlags Flags)
	{
		if (Handle)
		{
			MainThreadCall(^{
				@autoreleasepool {
					if (Flags & kCGDisplayBeginConfigurationFlag)
					{
						[Handle setMovable:YES];
						[Handle setMovableByWindowBackground:NO];
						
						[Handle setDisplayReconfiguring: true];
					}
					else if (Flags & kCGDisplayDesktopShapeChangedFlag)
					{
						[Handle setDisplayReconfiguring:false];
					}
				}
			});
		}
	}
    
    
    ENativeWindowMode CMacWindow::GetWindowMode() const
    {
        return [Handle windowMode];
    }
    
    
    bool CMacWindow::IsMaximized() const
    {
        return Handle && Handle->bZoomed;
    }
    
    
    bool CMacWindow::IsMinimized() const
    {
        @autoreleasepool {
            return [Handle isMiniaturized];
        }
    }
    
    
    bool CMacWindow::IsVisible() const
    {
        @autoreleasepool {
            return bIsVisible && [NSApp isHidden] == false;
        }
    }
    
    
    void CMacWindow::Destroy()
    {
        if (Handle)
        {
            @autoreleasepool {
                bIsClosed = true;
                [Handle setAlphaValue:0.0f];
                [Handle setBackgroundColor:[NSColor clearColor]];
                MacApplication->OnWindowDestroyed(this);
                Handle = nullptr;
            }
        }
    }
}
#endif

