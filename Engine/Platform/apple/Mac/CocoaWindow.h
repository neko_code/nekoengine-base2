//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  CocoaWindow.h
//  Neko engine.
//
//  Created by Neko Code on 8/26/15.
//  Copyright (c) 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "../../SystemShared.h"

#if NEKO_OSX

#include "../../IGenericWindow.h"

#import <AppKit/AppKit.h>

/**
 *  Custom window class used for input handling
 */
@interface CCocoaWindow : NSWindow <NSWindowDelegate/*, NSDraggingDestination*/>
{
	Neko::ENativeWindowMode WindowMode;

	bool bAcceptsInput;
	bool bDisplayReconfiguring;
	bool bRenderInitialized;

	float Opacity;

	@public
    
    bool bZoomed;
    

    
	bool bIsOnActiveSpace;
}

////////////////////////////////////////////////////////////
@property(assign) NSRect PreFullScreenRect;
@property(assign) Neko::ENativeWindowMode TargetWindowMode;

-(NSRect)openGLFrame;
-(NSView*)openGLView;

-(void)setAcceptsInput:(bool)InAcceptsInput;
-(void)setWindowMode:(Neko::ENativeWindowMode)WindowMode;

-(Neko::ENativeWindowMode)windowMode;

-(void)setDisplayReconfiguring:(bool)bIsDisplayReconfiguring;

-(void)orderFrontAndMakeMain:(bool)bMain andKey : (bool)bKey;

-(void)startRendering;
-(bool)isRenderInitialized;

@end

////////////////////////////////////////////////////////////
extern NSString* NSDraggingExited;
extern NSString* NSDraggingUpdated;
extern NSString* NSPrepareForDragOperation;
extern NSString* NSPerformDragOperation;

#endif
