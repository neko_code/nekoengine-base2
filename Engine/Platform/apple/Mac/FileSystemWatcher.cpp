//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  FileSystemWatcher.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "../../Platform.h"

#if NEKO_APPLE_FAMILY

#include "../../../Core/Log.h"
#include "../../FileSystemWatcher.h"
#include "../ApplePlatformUtils.h"
#include "MacApplication.h"

#include "Runloop.h"

#include <sys/queue.h>
#include <sys/event.h>
#include <sys/kernel.h>

#include <CoreServices/CoreServices.h>

struct FileSystemWatcherImpl;

/// Implementation for file watcher. For macOS this is running on the main system thread.
struct FileSystemWatcherImpl final : public IFileSystemWatcher
{
    explicit FileSystemWatcherImpl(Neko::IAllocator& Allocator_)
    : Allocator(Allocator_)
    , bRunning(false)
    , bEndWatchRequestInvoked(false)
    {
        
    }
    
    ~FileSystemWatcherImpl()
    {
        Shutdown();
    }
    
    bool Start(const char* Path, uint32 Flags)
    {
        bIncludeDirectoryEvents = (Flags & FFileChangeData::WatchOptions::IncludeDirectoryChanges) != 0;
        bIgnoreChangesInSubtree = (Flags & FFileChangeData::WatchOptions::IgnoreChangesInSubtree) != 0;
        
        Neko::CopyString(WatchPath, Path);
        int32 len = Neko::StringLength(Path);
        if (len > 0 && Path[len - 1] != '/')
        {
            Neko::CatString(WatchPath, "/");
        }
        
        if (!Init())
        {
            return false;
        }
        
        return true;
    }
    
    virtual Neko::TDelegate<void(const FFileChangeData& Data)>& GetCallback() { return callback; }
    
//    virtual void ProcessPendingChanges() override
//    {
//        if (FileChanges.GetSize() > 0)
//        {
//            callback.Invoke(FileChanges);
//            FileChanges.Clear();
//        }
//    }
    
    /** Creates the file event handler, invokes on the main thread. */
    bool Init();
    
    void Shutdown(void);
    
    void ProcessChanges(size_t EventCount, void* EventPaths, const FSEventStreamEventFlags EventFlags[]);
    
    void EndWatchRequest()
    {
        bEndWatchRequestInvoked = true;
    }
    
    bool bRunning;
    bool bEndWatchRequestInvoked;
    bool bIncludeDirectoryEvents;
    bool bIgnoreChangesInSubtree;
    
    Neko::IAllocator& Allocator;
    
    char WatchPath[Neko::MAX_PATH_LENGTH];
    
    FSEventStreamRef EventStream;
    
//    Neko::TArray<FFileChangeData> FileChanges;
    
    Neko::TDelegate<void(const FFileChangeData& Data)> callback;
};

void DirectoryWatchMacCallback(ConstFSEventStreamRef StreamRef, void* WatchRequestPtr, size_t EventCount, void* EventPaths, const FSEventStreamEventFlags EventFlags[], const FSEventStreamEventId EventIDs[])
{
    FileSystemWatcherImpl* WatchRequest = (FileSystemWatcherImpl*)WatchRequestPtr;
    assert(WatchRequest);
    assert(WatchRequest->EventStream == StreamRef);
    
    WatchRequest->ProcessChanges(EventCount, EventPaths, EventFlags);
}


IFileSystemWatcher* IFileSystemWatcher::Create(const char* path, uint32 Flags, Neko::IAllocator& allocator)
{
    auto* watcher = NEKO_NEW(allocator, FileSystemWatcherImpl)(allocator);
    if (!watcher->Start(path, Flags))
    {
        NEKO_DELETE(allocator, watcher);
        return nullptr;
    }
    return watcher;
}


void IFileSystemWatcher::Destroy(IFileSystemWatcher* watcher)
{
    if (!watcher)
    {
        return;
    }
    auto* impl = (FileSystemWatcherImpl*)watcher;
    NEKO_DELETE(impl->Allocator, impl);
}

void FileSystemWatcherImpl::Shutdown(void)
{
    if (bRunning)
    {
        assert(EventStream);
        
        MainThreadCall(^{
            FSEventStreamStop(EventStream);
            FSEventStreamUnscheduleFromRunLoop(EventStream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
            FSEventStreamInvalidate(EventStream);
            FSEventStreamRelease(EventStream);
            
            bRunning = false;
        }, NSDefaultRunLoopMode, true);
    }
}

void FileSystemWatcherImpl::ProcessChanges(size_t EventCount, void* EventPaths, const FSEventStreamEventFlags EventFlags[])
{
    if (bEndWatchRequestInvoked)
    {
        // ignore all events
        return;
    }
    
    char FilePath[MAX_PATH];
    
    CFArrayRef EventPathArray = (CFArrayRef)EventPaths;
    
    for( size_t EventIndex = 0; EventIndex < EventCount; ++EventIndex )
    {
        const FSEventStreamEventFlags Flags = EventFlags[EventIndex];
        if (!(Flags & kFSEventStreamEventFlagItemIsFile))
        {
            if (!bIncludeDirectoryEvents || !(Flags & kFSEventStreamEventFlagItemIsDir))
            {
                // events about directories and symlinks don't concern us
                continue;
            }
        }
        
        // Warning: some events have more than one of created, removed nd modified flag.
        // For safety, I think removed should take preference over created, and created over modified.
        FFileChangeData::EFileChangeAction Action;
        
        const bool bDirectory = ( Flags & kFSEventStreamEventFlagItemIsDir );
        
        const bool bFileAdded = ( Flags & kFSEventStreamEventFlagItemCreated );
        const bool bFileRenamed = ( Flags & kFSEventStreamEventFlagItemRenamed );
        const bool bFileModified = ( Flags & kFSEventStreamEventFlagItemModified );
        const bool bFileRemoved = ( Flags & kFSEventStreamEventFlagItemRemoved );
        
        bool bFileNeedsChecking = false;
        
        // File modifications take precendent over everything, unless the file has actually been deleted. We only handle newly created files when
        // kFSEventStreamEventFlagItemCreated is exclusively set. This flag can often be set when files have been renamed or copied over the top,
        // but we abstract this behavior and just mark it as modified.
        if (bFileModified)
        {
            bFileNeedsChecking = true;
            Action = FFileChangeData::FCA_Modified;
        }
        else if (bFileRenamed)
        {
            // for now, renames are abstracted as deletes/adds
            bFileNeedsChecking = true;
            Action = FFileChangeData::FCA_Added;
        }
        else if (bFileAdded)
        {
            if (bFileRemoved)
            {
                // Event indicates that file was both created and removed. To find out which of those events
                // happened later, we have to check for file's presence.
                bFileNeedsChecking = true;
            }
            Action = FFileChangeData::FCA_Added;
        }
        else if (bFileRemoved)
        {
            Action = FFileChangeData::FCA_Removed;
        }
        else
        {
            // events about inode, Finder info, owner change, extended attributes modification don't concern us
            continue;
        }
        
        Neko::Platform::CFStringToTCHAR((CFStringRef)CFArrayGetValueAtIndex(EventPathArray,EventIndex), FilePath);
        
        
        if (bFileNeedsChecking)
        {
            if (!bDirectory && !Neko::Platform::FileExists(FilePath))
            {
                Action = FFileChangeData::FCA_Removed;
            }
            if (bDirectory && !Neko::Platform::DirectoryExists(FilePath))
            {
                Action = FFileChangeData::FCA_Removed;
            }
        }
        
//        FileChanges.Push(FFileChangeData(FilePath, Action));
        callback.Invoke(FFileChangeData(FilePath, Action));
    }
}

bool FileSystemWatcherImpl::Init()
{
    __block bool bResult = true;
    // Do that on the main system thread, we got plenty of room here
    MainThreadCall(^{
        if (Neko::StringLength(WatchPath) == 0)
        {
            assert(false);
            bResult = false;
        }
        
        if (bRunning)
        {
            Shutdown();
        }
        
        DebugLog("FileWatcher Init: %s\n", WatchPath);
        
        bEndWatchRequestInvoked = false;
        
        // Set up streaming and turn it on
        
        CFStringRef FullPathMac = Neko::Platform::TCHARToCFString(WatchPath);
        CFArrayRef PathsToWatch = CFArrayCreate(NULL, (const void**)&FullPathMac, 1, &kCFTypeArrayCallBacks);
        
        CFAbsoluteTime Latency = 0.2;	// seconds
        
        FSEventStreamContext Context;
        Context.version = 0;
        Context.info = this;
        Context.retain = NULL;
        Context.release = NULL;
        Context.copyDescription = NULL;
        
        EventStream = FSEventStreamCreate( NULL,
                                          &DirectoryWatchMacCallback,
                                          &Context,
                                          (CFArrayRef) PathsToWatch,
                                          kFSEventStreamEventIdSinceNow,
                                          Latency,
                                          kFSEventStreamCreateFlagUseCFTypes | kFSEventStreamCreateFlagFileEvents);
        CFRelease(PathsToWatch);
        CFRelease(FullPathMac);
        
        if (!EventStream)
        {
            assert(false);
            bResult = false;
        }
        
        FSEventStreamScheduleWithRunLoop(EventStream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
        bool Result = FSEventStreamStart(EventStream);
        assert(Result && "FSEventStreamStart failed");
        
        bRunning = true;
    }, NSDefaultRunLoopMode, true);
    
    return bResult;
}

#endif
