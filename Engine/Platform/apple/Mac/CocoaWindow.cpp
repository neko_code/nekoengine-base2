//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  CocoaWindow.cpp
//  Neko engine.
//
//  Copyright (c) 2015 Neko Vision. All rights reserved.
//

#include "CocoaWindow.h"

#if NEKO_OSX

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

#include "../../Application.h"
#include "MetalViewController.h"
#include "MacApplication.h"
#include "Runloop.h"

/**  Default system string keys. */
NSString* NSDraggingExited = @"NSDraggingExited";
NSString* NSDraggingUpdated = @"NSDraggingUpdated";
NSString* NSPrepareForDragOperation = @"NSPrepareForDragOperation";
NSString* NSPerformDragOperation = @"NSPerformDragOperation";

/**
 *  Custom window class used for input handling.
 */
@implementation CCocoaWindow

@synthesize TargetWindowMode;
@synthesize PreFullScreenRect;

/**
 *  Initialize new window.
 */
-(id)initWithContentRect:(NSRect)ContentRect styleMask : (NSUInteger)Style backing : (NSBackingStoreType)BufferingType defer : (BOOL)Flag
{
    bAcceptsInput = true;
    bDisplayReconfiguring = false;
    bRenderInitialized = false;
    
    WindowMode = Neko::ENativeWindowMode::Windowed;
    Opacity = 1.0f;
    
    id NewSelf = [super initWithContentRect : ContentRect styleMask : Style backing : BufferingType defer : Flag];
    if (NewSelf)
    {
        bZoomed = [super isZoomed];
        bIsOnActiveSpace = [super isOnActiveSpace];
        self.TargetWindowMode = Neko::ENativeWindowMode::Windowed;
        [super setAlphaValue : Opacity];
        self.PreFullScreenRect = [super frame];
    }
    [self setMovableByWindowBackground : NO];
    
    return NewSelf;
}

-(NSRect)openGLFrame
{
    @autoreleasepool
    {
        if (self.TargetWindowMode == Neko::ENativeWindowMode::Fullscreen
            || WindowMode == Neko::ENativeWindowMode::Fullscreen)
        {
            return{ { 0, 0 }, self.PreFullScreenRect.size };
        }
        else if ([self styleMask] & NSWindowStyleMaskTexturedBackground)
        {
            return [self frame];
        }
        else
        {
            return [[self contentView] frame];
        }
    }
}

-(NSView*)openGLView
{
    @autoreleasepool
    {
        if ([self styleMask] & (NSWindowStyleMaskTexturedBackground))
        {
            NSView* SuperView = [[self contentView] superview];
            for (NSView* View in[SuperView subviews])
            {
                if ([View isKindOfClass : [CCocoaMediaView class]])
                {
                    return View;
                }
            }
            return nil;
        }
        else
        {
            return[self contentView];
        }
    }
}

-(void)setAcceptsInput:(bool)InAcceptsInput
{
    bAcceptsInput = InAcceptsInput;
}

-(void)setWindowMode : (Neko::ENativeWindowMode)NewWindowMode
{
    WindowMode = NewWindowMode;
    
    NSView* OpenGLView = [self openGLView];
    [[NSNotificationCenter defaultCenter] postNotificationName:NSViewGlobalFrameDidChangeNotification object : OpenGLView];
}

-(Neko::ENativeWindowMode)windowMode
{
    return WindowMode;
}

-(void)setDisplayReconfiguring:(bool)bIsDisplayReconfiguring
{
    bDisplayReconfiguring = bIsDisplayReconfiguring;
}

-(void)orderFrontAndMakeMain : (bool)bMain andKey : (bool)bKey
{
    @autoreleasepool
    {
        if ([NSApp isHidden] == NO)
        {
            [self orderFront : nil];
            
            if (bMain &&[self canBecomeMainWindow] && self != [NSApp mainWindow])
            {
                [self makeMainWindow];
            }
            if (bKey &&[self canBecomeKeyWindow] && self != [NSApp keyWindow])
            {
                [self makeKeyWindow];
            }
        }
    }
}

-(BOOL)canBecomeMainWindow
{
    @autoreleasepool
    {
        return bAcceptsInput && ![self ignoresMouseEvents];
    }
}

-(BOOL)canBecomeKeyWindow
{
    @autoreleasepool
    {
        return bAcceptsInput && ([self styleMask] != NSWindowStyleMaskBorderless);
    }
}

-(BOOL)validateMenuItem:(NSMenuItem*)MenuItem
{
    @autoreleasepool {
        return ([MenuItem action] == @selector(performClose:) || [MenuItem action] == @selector(miniaturize:) || [MenuItem action] == @selector(zoom:)) ? YES : [super validateMenuItem : MenuItem];
    }
}

-(void)setAlphaValue:(CGFloat)WindowAlpha
{
    Opacity = WindowAlpha;
    if (bRenderInitialized)
    {
        [super setAlphaValue : WindowAlpha];
    }
}

-(void)startRendering
{
    if (!bRenderInitialized)
    {
        bRenderInitialized = true;
        [super setAlphaValue : Opacity];
    }
}

-(bool)isRenderInitialized
{
    return bRenderInitialized;
}

-(BOOL)windowShouldClose:(id)sender
{
    // Instead of closing window immediately we'll send event.
    // @todo Check method below 'performClose'
    if (Neko::MacApplication)
    {
        Neko::CMacWindow* Window = Neko::MacApplication->FindWindowByNSWindow(self);
        if (Window)
        {
            Neko::MacApplication->CloseWindow(Window);
        }
    }
	return NO;
}

-(void)performClose:(id)Sender
{
    GameThreadCall(^{
        if (Neko::MacApplication)
        {
            Neko::CMacWindow* Window = Neko::MacApplication->FindWindowByNSWindow(self);
            if (Window)
            {
                Neko::MacApplication->CloseWindow(Window);
            }
        }
    }, @[ NSDefaultRunLoopMode ], false);
}

-(void)performZoom:(id)Sender
{
}

-(void)zoom : (id)Sender
{
    @autoreleasepool{
        bZoomed = !bZoomed;
        [super zoom : Sender];
    }
}

-(void)keyDown:(NSEvent*)Event
{
    // @note Keep window keyboard events empty.
}

-(void)keyUp : (NSEvent*)Event
{
    // @note Keep window keyboard events empty.
}

-(void)windowWillEnterFullScreen : (NSNotification*)Notification
{
    // Handle clicking on the titlebar fullscreen item and manual
    if (self.TargetWindowMode == Neko::ENativeWindowMode::Windowed)
    {
        self.PreFullScreenRect.origin = [self frame].origin;
        self.PreFullScreenRect.size = [self openGLFrame].size;
        
        if (GIsEditor)
        {
            self.TargetWindowMode = Neko::ENativeWindowMode::WindowedFullscreen;
        }
    }
}

-(void)windowDidEnterFullScreen:(NSNotification*)Notification
{
    WindowMode = self.TargetWindowMode;
    
    if (Neko::MacApplication)
    {
        Neko::MacApplication->PreProcessEvent(Notification);
    }
}

-(void)windowWillExitFullScreen:(NSNotification *)Notification
{
    if (self.TargetWindowMode != Neko::ENativeWindowMode::Windowed)
    {
        self.TargetWindowMode = Neko::ENativeWindowMode::Windowed;
    }
}

-(void)windowDidFailToEnterFullScreen:(NSWindow *)window
{
    
}

-(void)windowDidFailToExitFullScreen:(NSWindow *)window
{
    
}

-(void)windowDidExitFullScreen:(NSNotification*)Notification
{
    WindowMode = Neko::ENativeWindowMode::Windowed;
    self.TargetWindowMode = Neko::ENativeWindowMode::Windowed;
    
    // Remove any primary fullscreen behaviour here, we don't support it properly.
    NSWindowCollectionBehavior Behaviour = [self collectionBehavior];
    Behaviour &= ~(NSWindowCollectionBehaviorFullScreenPrimary);
    Behaviour |= NSWindowCollectionBehaviorFullScreenAuxiliary;
    [self setCollectionBehavior : Behaviour];
    
    if (Neko::MacApplication)
    {
        Neko::MacApplication->PreProcessEvent(Notification);
    }
}

-(void)windowDidBecomeMain:(NSNotification*)Notification
{
    @autoreleasepool
    {
        if ([NSApp isHidden] == NO)
        {
            [self orderFrontAndMakeMain : false andKey : false];
        }
        
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
    }
}

-(void)windowDidResignMain:(NSNotification*)Notification
{
    @autoreleasepool
    {
        [self setMovable : YES];
        [self setMovableByWindowBackground : NO];
        
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
    }
}

-(void)windowWillMove:(NSNotification*)Notification
{
    if (Neko::MacApplication)
    {
        Neko::MacApplication->PreProcessEvent(Notification);
    }
}

-(void)windowDidMove:(NSNotification*)Notification
{
    @autoreleasepool
    {
        bZoomed = [self isZoomed];
        
        NSView* OpenGLView = [self openGLView];
        [[NSNotificationCenter defaultCenter] postNotificationName:NSViewGlobalFrameDidChangeNotification object : OpenGLView];
        
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
    }
}

-(NSRect)constrainFrameRect:(NSRect)FrameRect toScreen : (NSScreen*)Screen
{
    NSRect ConstrainedRect = [super constrainFrameRect : FrameRect toScreen : Screen];
    
    if (self.TargetWindowMode == Neko::ENativeWindowMode::Windowed)
    {
        // In windowed mode do not limit the window size to screen size
        ConstrainedRect.origin.y -= FrameRect.size.height - ConstrainedRect.size.height;
        ConstrainedRect.size = FrameRect.size;
    }
    
    return ConstrainedRect;
}

-(void)windowDidChangeScreen:(NSNotification*)Notification
{
    if (bDisplayReconfiguring)
    {
        @autoreleasepool
        {
            NSScreen* Screen = [self screen];
            NSRect Frame = [self frame];
            NSRect VisibleFrame = [Screen visibleFrame];
            if (NSContainsRect(VisibleFrame, Frame) == NO)
            {
                if (Frame.size.width > VisibleFrame.size.width || Frame.size.height > VisibleFrame.size.height) {
                    NSRect NewFrame;
                    NewFrame.size.width = Frame.size.width > VisibleFrame.size.width ? VisibleFrame.size.width : Frame.size.width;
                    NewFrame.size.height = Frame.size.height > VisibleFrame.size.height ? VisibleFrame.size.height : Frame.size.height;
                    NewFrame.origin = VisibleFrame.origin;
                    
                    [self setFrame : NewFrame display : NO];
                }
                else {
                    NSRect Intersection = NSIntersectionRect(VisibleFrame, Frame);
                    NSPoint Origin = Frame.origin;
                    
                    if (Intersection.size.width > 0 && Intersection.size.height > 0) {
                        CGFloat X = Frame.size.width - Intersection.size.width;
                        CGFloat Y = Frame.size.height - Intersection.size.height;
                        
                        if (Intersection.size.width + Intersection.origin.x >= VisibleFrame.size.width + VisibleFrame.origin.x) {
                            Origin.x -= X;
                        }
                        else if (Origin.x < VisibleFrame.origin.x) {
                            Origin.x += X;
                        }
                        
                        if (Intersection.size.height + Intersection.origin.y >= VisibleFrame.size.height + VisibleFrame.origin.y) {
                            Origin.y -= Y;
                        }
                        else if (Origin.y < VisibleFrame.origin.y) {
                            Origin.y += Y;
                        }
                    }
                    else {
                        Origin = VisibleFrame.origin;
                    }
                    
                    [self setFrameOrigin : Origin];
                }
            }
        }
    }
}

-(void)windowWillStartLiveResize:(NSNotification*)Notification
{
    @autoreleasepool
    {
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
    };
}

-(void)windowDidEndLiveResize:(NSNotification*)Notification
{
    @autoreleasepool
    {
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
    }
}

-(void)windowDidResize:(NSNotification*)Notification
{
    @autoreleasepool
    {
        bZoomed = [self isZoomed];
        if (Neko::MacApplication && self.TargetWindowMode == WindowMode)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
    }
}

-(void)windowWillClose:(NSNotification*)Notification
{
    @autoreleasepool
    {
        if (Neko::MacApplication)
        {
            Neko::CMacWindow* Window = Neko::MacApplication->FindWindowByNSWindow(self);
            if (Window)
            {
                Neko::MacApplication->CloseWindow(Window);
            }
        }
        
        [self setDelegate : nil];
    }
}

-(NSDragOperation)draggingEntered:(id <NSDraggingInfo>)Sender
{
    return NSDragOperationGeneric;
}

-(void)draggingExited : (id <NSDraggingInfo>)Sender
{
    @autoreleasepool
    {
        NSNotification* Notification = [NSNotification notificationWithName : NSDraggingExited object : Sender];
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
    }
}

-(NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)Sender
{
    @autoreleasepool
    {
        NSNotification* Notification = [NSNotification notificationWithName : NSDraggingUpdated object : Sender];
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
        return NSDragOperationGeneric;
    }
}

-(BOOL)prepareForDragOperation:(id <NSDraggingInfo>)Sender
{
    @autoreleasepool
    {
        NSNotification* Notification = [NSNotification notificationWithName : NSPrepareForDragOperation object : Sender];
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
        return YES;
    }
}

-(BOOL)performDragOperation:(id <NSDraggingInfo>)Sender
{
    @autoreleasepool
    {
        NSNotification* Notification = [NSNotification notificationWithName : NSPerformDragOperation object : Sender];
        if (Neko::MacApplication)
        {
            Neko::MacApplication->PreProcessEvent(Notification);
        }
        return YES;
    }
}

@end
#endif
