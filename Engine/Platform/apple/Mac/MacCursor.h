//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MacCursor.h
//  Neko engine
//
//  Created by Neko on 4/23/16.
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#pragma once

#include "../../SystemShared.h"
#include "../../InputInterfaces.h"
#include "../../../Math/Vec.h"

#if NEKO_OSX

#include <IOKit/IOTypes.h>
#include <AppKit/AppKit.h>

namespace Neko
{
    /// Implementation for cursor on Mac.
    class CMacCursor final : public ICursor
    {
    public:
        
        CMacCursor();
        
        virtual ~CMacCursor();
        
        virtual Vec2                GetPosition() const override;
        
        virtual void                SetPosition(int32 X, int32 Y) override;
        
        
        virtual void                SetType(EMouseCursorType Type) override;
        
        virtual EMouseCursorType    GetType() const override { return CurrentType; }
        
        
        virtual void                Show(bool bShow) override;
        
    public:
        
        bool SetRelativeMode(bool bEnable);
        
        bool IsRelativeMouseMode() { return bRelativeMode; }
        
        void Wrap(CMacWindow* Window, const int32 X, const int32 Y);
        
    private:
        
        inline void UpdateCursorPosition(Vec2 Position)
        {
            CurrentPosition = Position;
            bPositionInited = true;
        }
        
        void UpdateVisibility();
        
        EMouseCursorType CurrentType;
        
        NSCursor* CursorHandles[EMouseCursorType::TotalCursorCount];
        
        bool bVisible;
        bool bRelativeMode; // high precision
        
        NSCursor* CurrentCursor;
        
        Vec2 CurrentPosition;
        
        bool bPositionInited;
        
        io_object_t HIDInterface;
        double SavedAcceleration;
    };
}

#endif
