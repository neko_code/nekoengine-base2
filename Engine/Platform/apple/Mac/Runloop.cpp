//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Runloop.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#include "../../SystemShared.h"

#if NEKO_APPLE_FAMILY

#if NEKO_OSX

#include "../../../Core/Log.h"
#include "../../../Containers/Queue.h"
#include "../../Application.h"

#include "Runloop.h"

#define MAC_SEPARATE_GAME_THREAD 1 // Separate the main & game threads so that we better handle the interaction between the Cocoa's event delegates and Neko's event polling.

// this is the size of the game thread stack, it must be a multiple of 4k
#if (DEBUG)
#	define GAME_THREAD_STACK_SIZE 64 * 1024 * 1024
#else
#	define GAME_THREAD_STACK_SIZE 128 * 1024 * 1024
#endif

NSString* NekoNilEventMode = @"NekoNilEventMode";
NSString* NekoShowEventMode = @"NekoShowEventMode";
NSString* NekoResizeEventMode = @"NekoResizeEventMode";
NSString* NekoFullscreenEventMode = @"NekoFullscreenEventMode";
NSString* NekoCloseEventMode = @"NekoCloseEventMode";
NSString* NekoIMEEventMode = @"NekoIMEEventMode";

static FCocoaGameThread* GCocoaGameThread = nil;

class FCocoaRunLoopSource;
@interface FCocoaRunLoopSourceInfo : NSObject
{
	@private
	FCocoaRunLoopSource* Source;
	CFRunLoopRef RunLoop;
	CFStringRef Mode;
}
- (id)initWithSource:(FCocoaRunLoopSource*)InSource;
- (void)dealloc;
- (void)scheduleOn:(CFRunLoopRef)InRunLoop inMode:(CFStringRef)InMode;
- (void)cancelFrom:(CFRunLoopRef)InRunLoop inMode:(CFStringRef)InMode;
- (void)perform;
@end

class FCocoaRunLoopTask
{
	friend class FCocoaRunLoopSource;
    
public:
    
	FCocoaRunLoopTask(dispatch_block_t InBlock, NSArray* InModes)
	: Block(Block_copy(InBlock))
	, Modes([InModes retain])
	{
		
	}
	
	~FCocoaRunLoopTask()
	{
		[Modes release];
		Block_release(Block);
	}
    
private:
    
	dispatch_block_t Block;
	NSArray* Modes;
};

class FCocoaRunLoopSource //: public FRefCountedObject
{
    
public:
    
	static void RegisterMainRunLoop(CFRunLoopRef RunLoop)
	{
		assert(!MainRunLoopSource);
		MainRunLoopSource = new FCocoaRunLoopSource(RunLoop);
	}
	
	static void RegisterGameRunLoop(CFRunLoopRef RunLoop)
	{
		assert(!GameRunLoopSource);
		GameRunLoopSource = new FCocoaRunLoopSource(RunLoop);
	}
	
	static FCocoaRunLoopSource& GetMainRunLoopSource(void)
	{
		assert(MainRunLoopSource);
		return *MainRunLoopSource;
	}
	
	static FCocoaRunLoopSource& GetGameRunLoopSource(void)
	{
		assert(GameRunLoopSource);
		return *GameRunLoopSource;
	}
	
	void Schedule(dispatch_block_t InBlock, NSArray* InModes)
	{
		for (NSString* Mode in InModes)
		{
			Register((CFStringRef)Mode);
		}
		
		OutstandingTasks.Push(new FCocoaRunLoopTask(InBlock, InModes));
		
		CFDictionaryApplyFunction(SourceDictionary, &FCocoaRunLoopSource::SignalFunction, nullptr);
	}
	
	void Wake(void)
	{
		CFRunLoopWakeUp(TargetRunLoop);
	}
	
	void RunInMode(CFStringRef WaitMode)
	{
		CFRunLoopRunInMode(WaitMode, 0, true);
	}
	
	void Process(CFStringRef Mode)
	{
		bool bDone = false;
		while (!bDone)
		{
			bDone = true;
			const FCocoaRunLoopTask* Task;
			do
			{
				Task = OutstandingTasks.Pop();
				if (Task && [Task->Modes containsObject:(NSString*)Mode])
				{
					Task->Block();
					delete Task;
					bDone = false;
				}
				
			} while (Task != nullptr);
		}
	}
	
private:
	FCocoaRunLoopSource(CFRunLoopRef RunLoop)
	: TargetRunLoop(RunLoop)
	, SourceDictionary(CFDictionaryCreateMutable(nullptr, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks))
	{
		assert(TargetRunLoop);
		
		// Register for default modes
		Register(kCFRunLoopDefaultMode);
		Register((CFStringRef)NSModalPanelRunLoopMode);
		Register((CFStringRef)NekoNilEventMode);
		Register((CFStringRef)NekoShowEventMode);
		Register((CFStringRef)NekoResizeEventMode);
		Register((CFStringRef)NekoFullscreenEventMode);
		Register((CFStringRef)NekoCloseEventMode);
		Register((CFStringRef)NekoIMEEventMode);
	}
	
	virtual ~FCocoaRunLoopSource()
	{
		if ( MainRunLoopSource == this )
		{
			MainRunLoopSource = nullptr;
		}
		else if ( GameRunLoopSource == this )
		{
			GameRunLoopSource = nullptr;
		}
		
		CFDictionaryApplyFunction(SourceDictionary, &FCocoaRunLoopSource::ShutdownFunction, TargetRunLoop);
		CFRelease(SourceDictionary);
	}
	
	void Register(CFStringRef Mode)
	{
		if (CFDictionaryContainsKey(SourceDictionary, Mode) == false)
		{
			CFRunLoopSourceContext Context;
			FCocoaRunLoopSourceInfo* Info = [[FCocoaRunLoopSourceInfo alloc] initWithSource:this];
			memset(&Context, 0, sizeof(Context));
			Context.version = 0;
			Context.info = Info;
			Context.retain = CFRetain;
			Context.release = CFRelease;
			Context.copyDescription = CFCopyDescription;
			Context.equal = CFEqual;
			Context.hash = CFHash;
			Context.schedule = &FCocoaRunLoopSource::Schedule;
			Context.cancel = &FCocoaRunLoopSource::Cancel;
			Context.perform = &FCocoaRunLoopSource::Perform;
			
			CFRunLoopSourceRef Source = CFRunLoopSourceCreate(nullptr, 0, &Context);
			CFDictionaryAddValue(SourceDictionary, Mode, Source);
			CFRunLoopAddSource(TargetRunLoop, Source, Mode);
			CFRelease(Source);
		}
	}
	
	static void SignalFunction(const void* Key, const void* Value, void* Context)
	{
		CFRunLoopSourceRef RunLoopSource = (CFRunLoopSourceRef)Value;
		if (RunLoopSource)
		{
			CFRunLoopSourceSignal(RunLoopSource);
		}
	}
	static void ShutdownFunction(const void* Key, const void* Value, void* Context)
	{
		CFRunLoopRef RunLoop = static_cast<CFRunLoopRef>(Context);
		if (RunLoop)
		{
			CFRunLoopRemoveSource(RunLoop, (CFRunLoopSourceRef)Value, (CFStringRef)Key);
		}
	}
	static void Schedule(void* Info, CFRunLoopRef RunLoop, CFStringRef Mode)
	{
		FCocoaRunLoopSourceInfo* Source = static_cast<FCocoaRunLoopSourceInfo*>(Info);
		if (Source)
		{
			[Source scheduleOn:RunLoop inMode:Mode];
		}
	}
	static void Cancel(void* Info, CFRunLoopRef RunLoop, CFStringRef Mode)
	{
		FCocoaRunLoopSourceInfo* Source = static_cast<FCocoaRunLoopSourceInfo*>(Info);
		if (Source)
		{
			[Source cancelFrom:RunLoop inMode:Mode];
		}
	}
	static void Perform(void* Info)
	{
		FCocoaRunLoopSourceInfo* Source = static_cast<FCocoaRunLoopSourceInfo*>(Info);
		if (Source)
		{
			[Source perform];
		}
	}
	
private:
    
	Neko::SpScUnboundedQueueLf<FCocoaRunLoopTask> OutstandingTasks;
	CFRunLoopRef TargetRunLoop;
	CFMutableDictionaryRef SourceDictionary;
	
private:
    
	static FCocoaRunLoopSource* MainRunLoopSource;
	static FCocoaRunLoopSource* GameRunLoopSource;
};


FCocoaRunLoopSource* FCocoaRunLoopSource::MainRunLoopSource = nullptr;
FCocoaRunLoopSource* FCocoaRunLoopSource::GameRunLoopSource = nullptr;

@implementation FCocoaRunLoopSourceInfo

- (id)initWithSource:(FCocoaRunLoopSource*)InSource
{
	id Self = [super init];
	if (Self)
	{
		assert(InSource);
		Source = InSource;
		//		Source->AddReference();
	}
	return Self;
}

- (void)dealloc
{
	assert(Source);
	//	Source->Release();
	Source = nullptr;
	[super dealloc];
}

- (void)scheduleOn:(CFRunLoopRef)InRunLoop inMode:(CFStringRef)InMode
{
	assert(!RunLoop);
	assert(!Mode);
	RunLoop = InRunLoop;
	Mode = InMode;
}

- (void)cancelFrom:(CFRunLoopRef)InRunLoop inMode:(CFStringRef)InMode
{
	if (CFEqual(InRunLoop, RunLoop) && CFEqual(Mode, InMode))
	{
		RunLoop = nullptr;
		Mode = nullptr;
	}
}

- (void)perform
{
	assert(Source);
	assert(RunLoop);
	assert(Mode);
	assert(CFEqual(RunLoop, CFRunLoopGetCurrent()));
	CFStringRef CurrentMode = CFRunLoopCopyCurrentMode(CFRunLoopGetCurrent());
	assert(CFEqual(CurrentMode, Mode));
	Source->Process(CurrentMode);
	CFRelease(CurrentMode);
}

@end

@implementation NSThread (FCocoaThread)

+ (NSThread*) gameThread
{
	if (!GCocoaGameThread)
	{
		return [NSThread mainThread];
	}
	return GCocoaGameThread;
}

+ (bool) isGameThread
{
	bool const bIsGameThread = [[NSThread currentThread] isGameThread];
	return bIsGameThread;
}

- (bool) isGameThread
{
	bool const bIsGameThread = (self == GCocoaGameThread);
	return bIsGameThread;
}

@end

@implementation FCocoaGameThread : NSThread

- (id)init
{
	id Self = [super init];
	if (Self)
	{
		GCocoaGameThread = Self;
	}
	return Self;
}

- (id)initWithTarget:(id)Target selector:(SEL)Selector object:(id)Argument
{
	id Self = [super initWithTarget:Target selector:Selector object:Argument];
	if (Self)
	{
		GCocoaGameThread = Self;
	}
	return Self;
}

- (void)dealloc
{
	GCocoaGameThread = nil;
	[super dealloc];
}

- (void)main
{
	NSRunLoop* GameRunLoop = [NSRunLoop currentRunLoop];
	
	// Register game run loop source
	FCocoaRunLoopSource::RegisterGameRunLoop([GameRunLoop getCFRunLoop]);
	
	[super main];
	
	// We have exited the game thread, so any Neko code running now should treat the Main thread
	// as the game thread, so we don't crash in static destructors.
	GGameThreadId = GMainThreadId;
	
	// Tell the main thread we are OK to quit, but don't wait for it.
	if (GIsRequestingExit)
	{
		MainThreadCall(^{
			[NSApp replyToApplicationShouldTerminate:YES];
			[[NSProcessInfo processInfo] enableSuddenTermination];
		}, NSDefaultRunLoopMode, false);
	}
	else
	{
		MainThreadCall(^{
			[[NSProcessInfo processInfo] enableSuddenTermination];
		}, NSDefaultRunLoopMode, false);
	}
	
	[self release];
}

@end

static void PerformBlockOnThread(FCocoaRunLoopSource& ThreadSource, dispatch_block_t Block, NSArray* SendModes, NSString* WaitMode, bool const bWait)
{
	dispatch_block_t CopiedBlock = Block_copy(Block);
	
	if (bWait)
	{
		dispatch_semaphore_t Semaphore = dispatch_semaphore_create(0);
		dispatch_block_t ExecuteBlock = Block_copy(^{ CopiedBlock(); dispatch_semaphore_signal(Semaphore); });
		
		ThreadSource.Schedule(ExecuteBlock, SendModes);
		
		do
		{
			ThreadSource.Wake();
			ThreadSource.RunInMode( (CFStringRef)WaitMode );
		}
		while (dispatch_semaphore_wait(Semaphore, dispatch_time(0, 100000ull)));
		
		Block_release(ExecuteBlock);
		dispatch_release(Semaphore);
	}
	else
	{
		ThreadSource.Schedule(Block, SendModes);
		ThreadSource.Wake();
	}
	
	Block_release(CopiedBlock);
}


void MainThreadCall(dispatch_block_t Block, NSString* WaitMode, bool const bWait)
{
	if ( [NSThread mainThread] != [NSThread currentThread] )
	{
		FCocoaRunLoopSource& MainRunLoopSource = FCocoaRunLoopSource::GetMainRunLoopSource();
		PerformBlockOnThread(MainRunLoopSource, Block, @[ NSDefaultRunLoopMode, NSModalPanelRunLoopMode, NSEventTrackingRunLoopMode ], WaitMode, bWait);
	}
	else
	{
		Block();
	}
}


void GameThreadCall(dispatch_block_t Block, NSArray* SendModes, bool const bWait)
{
	if ( [NSThread gameThread] != [NSThread currentThread] )
	{
		FCocoaRunLoopSource& GameRunLoopSource = FCocoaRunLoopSource::GetGameRunLoopSource();
		PerformBlockOnThread(GameRunLoopSource, Block, SendModes, NSDefaultRunLoopMode, bWait);
	}
	else
	{
		Block();
	}
}


void RunGameThread(id Target, SEL Selector)
{
	@autoreleasepool {

		[[NSProcessInfo processInfo] disableSuddenTermination];
		
		GMainThreadId = Neko::MT::GetCurrentThreadID();
		
#if MAC_SEPARATE_GAME_THREAD
		// Register main run loop source
		FCocoaRunLoopSource::RegisterMainRunLoop(CFRunLoopGetCurrent());
		
		// Create a separate game thread and set it to the stack size to be the same as the main thread default of 8MB ( http://developer.apple.com/library/mac/#qa/qa1419/_index.html )
		FCocoaGameThread* GameThread = [[FCocoaGameThread alloc] initWithTarget:Target selector:Selector object:nil];
		[GameThread setStackSize:GAME_THREAD_STACK_SIZE];
		[GameThread start];
#else
		[Target performSelector:Selector withObject:nil];
		
		if (GIsRequestingExit)
		{
			[NSApp replyToApplicationShouldTerminate:YES];
		}
#endif
	}
}


void ProcessGameThreadEvents(void)
{
	@autoreleasepool {
#if MAC_SEPARATE_GAME_THREAD
		// Make one pass through the loop, processing all events
		CFRunLoopRef RunLoop = CFRunLoopGetCurrent();
        NEKO_UNUSED(RunLoop);
		CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0, false);
#else
		while( NSEvent *Event = [NSApp nextEventMatchingMask: NSEventMaskAny untilDate: nil inMode: NSDefaultRunLoopMode dequeue: true] )
		{
			// Either the windowNumber is 0 or the window must be valid for the event to be processed.
			// Processing events with a windowNumber but no window will crash inside sendEvent as it will try to send to a destructed window.
			if ([Event windowNumber] == 0 || [Event window] != nil)
			{
				[NSApp sendEvent: Event];
			}
		}
#endif
	}
}

namespace Neko
{
    
}
#endif

#endif
