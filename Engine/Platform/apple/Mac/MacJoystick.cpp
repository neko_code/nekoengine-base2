//
//  MacJoystick.cpp
//  Neko Engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#include "../../../Neko.h"

#if NEKO_OSX

#include "../../../Core/Log.h"
#include "../../../Core/Joystick.h"
#include "../../../Core/Application.h"
#include "MacJoystick.h"

namespace Neko
{
#define NEKO_GAMEPAD_RUNLOOP_MODE CFSTR( "NekoJoystick" )

	//! HID Manager API
	static IOHIDManagerRef hidman = nullptr;

	//!	Linked list of all available devices
	static JoystickHardwareDescriptor* GDeviceList = nullptr;

	// Static incrementing counter for new joystick devices seen on the system.
	static int GJoystickInstanceId = -1;

    static bool JoystickAlreadyKnown(IOHIDDeviceRef ioHIDDeviceObject)
    {
        JoystickHardwareDescriptor* i;
        
        for (i = GDeviceList; i != nullptr; i = i->pNext)
        {
            if (i->deviceRef == ioHIDDeviceObject)
            {
                return true;
            }
        }
        
        return false;
    }

	static JoystickHardwareDescriptor* GetDeviceForIndex(int32 deviceIndex)
	{
		JoystickHardwareDescriptor* device = GDeviceList;
		while (device)
		{
			if (!device->removed)
			{
				if (deviceIndex == 0)
				{
					break;
				}

				--deviceIndex;
			}

			device = device->pNext;
		}
		return device;
	}

	static void FreeElementList(DeviceElement* pElement)
	{
		while (pElement)
		{
			DeviceElement* pElementNext = pElement->pNext;
			free(pElement);
			pElement = pElementNext;
		}
	}

	static JoystickHardwareDescriptor* FreeDevice(JoystickHardwareDescriptor* removeDevice)
	{
		JoystickHardwareDescriptor* pDeviceNext = nullptr;

		if (removeDevice)
		{
			if (removeDevice->deviceRef)
			{
				IOHIDDeviceUnscheduleFromRunLoop(removeDevice->deviceRef, CFRunLoopGetCurrent(), NEKO_GAMEPAD_RUNLOOP_MODE);
				removeDevice->deviceRef = nullptr;
			}

			/* save next device prior to disposing of this device */
			pDeviceNext = removeDevice->pNext;

			if (GDeviceList == removeDevice)
			{
				GDeviceList = pDeviceNext;
			}
			else
			{
				JoystickHardwareDescriptor* device = GDeviceList;

				while (device->pNext != removeDevice)
				{
					device = device->pNext;
				}

				device->pNext = pDeviceNext;
			}

			removeDevice->pNext = nullptr;

			/* free element lists */
			FreeElementList(removeDevice->FirstAxis);
			FreeElementList(removeDevice->FirstButton);
			FreeElementList(removeDevice->FirstHat);

			free(removeDevice);
		}

		return pDeviceNext;
	}

	static SInt32 GetHIDElementState(JoystickHardwareDescriptor* pDevice, DeviceElement* pElement)
	{
		SInt32 value = 0;

		if (pDevice && pElement)
		{
			IOHIDValueRef valueRef;
			if (IOHIDDeviceGetValue(pDevice->deviceRef, pElement->elementRef, &valueRef) == kIOReturnSuccess)
			{
				value = (SInt32)IOHIDValueGetIntegerValue(valueRef);

				/* record min and max for auto calibration */
				if (value < pElement->minReport)
				{
					pElement->minReport = value;
				}
				if (value > pElement->maxReport)
				{
					pElement->maxReport = value;
				}
			}
		}

		return value;
	}

	static SInt32 GetHIDScaledCalibratedState(JoystickHardwareDescriptor* pDevice, DeviceElement* pElement, SInt32 min, SInt32 max)
	{
		const float deviceScale = max - min;
		const float readScale = pElement->maxReport - pElement->minReport;
		const SInt32 value = GetHIDElementState(pDevice, pElement);
		if (readScale == 0)
		{
			return value; // no scaling at all
		}
		return ((value - pElement->minReport) * deviceScale / readScale) + min;
	}

	static void JoystickDeviceWasRemovedCallback(void* Context, IOReturn result, void* sender, IOHIDDeviceRef DeviceRef)
	{
        MacJoystick* HIDInput = (MacJoystick*)Context;
        
        // Find the matching device
        JoystickHardwareDescriptor* Device;
        for (Device = GDeviceList; Device != nullptr; Device = Device->pNext)
        {
            if (Device->deviceRef == DeviceRef)
            {
                GamepadHardwareInfo Info;
                Info.Guid = Device->guid;
                Info.InstanceId = Device->iInstanceId;
                Info.Name = Device->product;
                // that's enough of information for disconnect, not needed to add more
                
                
                // Disconnect joystick.
                HIDInput->MessageHandler->OnControllerConnectionChange(0, Info, false);
                Device->deviceRef = nullptr;
                Device->removed = true;
                
#define SAFE_DELETE(Item) \
            if (Item) \
            { \
                delete Item; \
                Item = nullptr; \
            }
                SAFE_DELETE(Device->Hats)
                SAFE_DELETE(Device->Axes)
                SAFE_DELETE(Device->Buttons);
                
                break;
            }
        }
	}

	static void AddHIDElement(const void* value, void* parameter);

	/* Call AddHIDElement() on all elements in an array of IOHIDElementRefs */
	static void AddHIDElements(CFArrayRef array, JoystickHardwareDescriptor* pDevice)
	{
		const CFRange range = { 0, CFArrayGetCount(array) };
		CFArrayApplyFunction(array, range, AddHIDElement, pDevice);
	}

	static bool ElementAlreadyAdded(const IOHIDElementCookie cookie, const DeviceElement* listitem)
	{
		while (listitem)
		{
			if (listitem->cookie == cookie)
			{
				return true;
			}
			listitem = listitem->pNext;
		}
		return false;
	}

    /* See if we care about this HID element, and if so, note it in our recDevice. */
    static void AddHIDElement(const void* value, void* parameter)
    {
        JoystickHardwareDescriptor* pDevice = (JoystickHardwareDescriptor*)parameter;
        IOHIDElementRef refElement = (IOHIDElementRef)value;
        const CFTypeID elementTypeID = refElement ? CFGetTypeID(refElement) : 0;
        
        if (refElement && (elementTypeID == IOHIDElementGetTypeID()))
        {
            const IOHIDElementCookie cookie = IOHIDElementGetCookie(refElement);
            const uint32 usagePage = IOHIDElementGetUsagePage(refElement);
            const uint32 usage = IOHIDElementGetUsage(refElement);
            DeviceElement* element = nullptr;
            DeviceElement** headElement = nullptr;
            
            // look at types of interest
            switch (IOHIDElementGetType(refElement))
            {
                case kIOHIDElementTypeInput_Misc:
                case kIOHIDElementTypeInput_Button:
                case kIOHIDElementTypeInput_Axis:
                {
                    switch (usagePage)
                    {
                        // only interested in kHIDPage_GenericDesktop and kHIDPage_Button
                        case kHIDPage_GenericDesktop:
                        {
                            switch (usage)
                            {
                                case kHIDUsage_GD_X:
                                case kHIDUsage_GD_Y:
                                case kHIDUsage_GD_Z:
                                case kHIDUsage_GD_Rx:
                                case kHIDUsage_GD_Ry:
                                case kHIDUsage_GD_Rz:
                                case kHIDUsage_GD_Slider:
                                case kHIDUsage_GD_Dial:
                                case kHIDUsage_GD_Wheel:
                                {
                                    if (!ElementAlreadyAdded(cookie, pDevice->FirstAxis))
                                    {
                                        element = (DeviceElement*)calloc(1, sizeof(DeviceElement));
                                        if (element)
                                        {
                                            pDevice->axes++;
                                            headElement = &(pDevice->FirstAxis);
                                        }
                                    }
                                    break;
                                }
                                case kHIDUsage_GD_Hatswitch:
                                {
                                    if (!ElementAlreadyAdded(cookie, pDevice->FirstHat))
                                    {
                                        element = (DeviceElement*)calloc(1, sizeof(DeviceElement));
                                        if (element)
                                        {
                                            pDevice->hats++;
                                            headElement = &(pDevice->FirstHat);
                                        }
                                    }
                                    break;
                                }
                                case kHIDUsage_GD_DPadUp:
                                case kHIDUsage_GD_DPadDown:
                                case kHIDUsage_GD_DPadRight:
                                case kHIDUsage_GD_DPadLeft:
                                case kHIDUsage_GD_Start:
                                case kHIDUsage_GD_Select:
                                {
                                    if (!ElementAlreadyAdded(cookie, pDevice->FirstButton))
                                    {
                                        element = (DeviceElement*)calloc(1, sizeof(DeviceElement));
                                        if (element)
                                        {
                                            pDevice->buttons++;
                                            headElement = &(pDevice->FirstButton);
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                        case kHIDPage_Simulation:
                        {
                            switch (usage)
                            {
                                case kHIDUsage_Sim_Rudder:
                                case kHIDUsage_Sim_Throttle:
                                    if (!ElementAlreadyAdded(cookie, pDevice->FirstAxis))
                                    {
                                        element = (DeviceElement*)calloc(1, sizeof(DeviceElement));
                                        if (element)
                                        {
                                            pDevice->axes++;
                                            headElement = &(pDevice->FirstAxis);
                                        }
                                    }
                                    break;
                                    
                                default:
                                    break;
                            }
                            break;
                        }
                        case kHIDPage_Button:
                        case kHIDPage_Consumer: // e.g. 'pause' button on Steelseries MFi gamepads.
                        {
                            if (!ElementAlreadyAdded(cookie, pDevice->FirstButton))
                            {
                                element = (DeviceElement*)calloc(1, sizeof(DeviceElement));
                                if (element)
                                {
                                    pDevice->buttons++;
                                    headElement = &(pDevice->FirstButton);
                                }
                            }
                            break;
                        }
                        default:
                            break;
                    }
                    break;
                }
                    
                case kIOHIDElementTypeCollection:
                {
                    CFArrayRef array = IOHIDElementGetChildren(refElement);
                    if (array)
                    {
                        AddHIDElements(array, pDevice);
                    }
                    break;
                }
                    
                default:
                    break;
            }
            
            if (element && headElement)
            {
                // add to list
                DeviceElement* elementPrevious = nullptr;
                DeviceElement* elementCurrent = *headElement;
                while (elementCurrent && usage >= elementCurrent->usage)
                {
                    elementPrevious = elementCurrent;
                    elementCurrent = elementCurrent->pNext;
                }
                if (elementPrevious)
                {
                    elementPrevious->pNext = element;
                }
                else
                {
                    *headElement = element;
                }
                
                element->elementRef = refElement;
                element->usagePage = usagePage;
                element->usage = usage;
                element->pNext = elementCurrent;
                
                element->minReport = element->min = (SInt32)IOHIDElementGetLogicalMin(refElement);
                element->maxReport = element->max = (SInt32)IOHIDElementGetLogicalMax(refElement);
                element->cookie = IOHIDElementGetCookie(refElement);
                
                pDevice->elements++;
            }
        }
    }
    
    static bool GetDeviceInfo(IOHIDDeviceRef hidDevice, JoystickHardwareDescriptor* pDevice)
    {
        UInt32* guid32 = nullptr;
        CFTypeRef refCF = nullptr;
        CFArrayRef array = nullptr;
        
        // get usage page and usage
        refCF = IOHIDDeviceGetProperty(hidDevice, CFSTR(kIOHIDPrimaryUsagePageKey));
        if (refCF)
        {
            CFNumberGetValue((CFNumberRef)refCF, kCFNumberSInt32Type, &pDevice->usagePage);
        }
        if (pDevice->usagePage != kHIDPage_GenericDesktop)
        {
            return false; // Filter device list to non-keyboard/mouse stuff
        }
        
        refCF = IOHIDDeviceGetProperty(hidDevice, CFSTR(kIOHIDPrimaryUsageKey));
        if (refCF)
        {
            CFNumberGetValue((CFNumberRef)refCF, kCFNumberSInt32Type, &pDevice->usage);
        }
        
        if ((pDevice->usage != kHIDUsage_GD_Joystick && pDevice->usage != kHIDUsage_GD_GamePad && pDevice->usage != kHIDUsage_GD_MultiAxisController))
        {
            return false; /* Filter device list to non-keyboard/mouse stuff */
        }
        
        pDevice->deviceRef = hidDevice;
        
        // get device name
        refCF = IOHIDDeviceGetProperty(hidDevice, CFSTR(kIOHIDProductKey));
        if (!refCF)
        {
            // Maybe we can't get "AwesomeJoystick2000", but we can get "Logitech"?
            refCF = IOHIDDeviceGetProperty(hidDevice, CFSTR(kIOHIDManufacturerKey));
        }
        if ((!refCF) || (!CFStringGetCString((CFStringRef)refCF, pDevice->product, sizeof(pDevice->product), kCFStringEncodingUTF8)))
		{
			strlcpy(pDevice->product, "Undefined joystick", sizeof(pDevice->product));
		}

		refCF = IOHIDDeviceGetProperty(hidDevice, CFSTR(kIOHIDVendorIDKey));
		if (refCF)
		{
			CFNumberGetValue((CFNumberRef)refCF, kCFNumberSInt32Type, &pDevice->guid.A);
		}

		refCF = IOHIDDeviceGetProperty(hidDevice, CFSTR(kIOHIDProductIDKey));
		if (refCF)
		{
			CFNumberGetValue((CFNumberRef)refCF, kCFNumberSInt32Type, &pDevice->guid.C); // 8
		}

		// Check to make sure we have a vendor and product ID If we don't, use the same algorithm as the Linux code for Bluetooth devices
		guid32 = (UInt32*)&pDevice->guid.A;
		if (!guid32[0] && !guid32[1])
		{
			// If we don't have a vendor and product ID this is probably a Bluetooth device
			const UInt16 BUS_BLUETOOTH = 0x05;
			UInt16* guid16 = (UInt16*)guid32;
			*guid16++ = BUS_BLUETOOTH;
			*guid16++ = 0;

			strlcpy((char*)guid16, pDevice->product, sizeof(pDevice->guid) - 4);
		}

		array = IOHIDDeviceCopyMatchingElements(hidDevice, nullptr, kIOHIDOptionsTypeNone);
		if (array)
		{
			AddHIDElements(array, pDevice);
			CFRelease(array);
		}

		return true;
	}

	static void JoystickDeviceWasAddedCallback(void* ctx, IOReturn res, void* sender, IOHIDDeviceRef ioHIDDeviceObject)
	{
		JoystickHardwareDescriptor* device;
		int deviceIndex = 0;

		if (res != kIOReturnSuccess)
		{
			GLogInfo.log("Engine") << "Result != kIOReturnSuccess";
			return;
		}

		if (JoystickAlreadyKnown(ioHIDDeviceObject))
		{
			return;
		}

		device = (JoystickHardwareDescriptor*)calloc(1, sizeof(JoystickHardwareDescriptor));
		if (device == nullptr)
		{
			return;
		}

		if (!GetDeviceInfo(ioHIDDeviceObject, device))
		{
			free(device);
			return; // Not a device we care about, probably
		}

		// Allocate an instance ID for this device
		device->iInstanceId = ++GJoystickInstanceId;

#if HAPTIC_IOKIT
		// We have to do some storage of the io_service_t for HapticOpenFromJoystick
		const io_service_t ioservice = IOHIDDeviceGetService(ioHIDDeviceObject);
		if ((ioservice) && (FFIsForceFeedback(ioservice) == FF_OK))
		{
			device->ffservice = ioservice;
			MacHaptic_MaybeAddDevice(ioservice);
		}
#endif

		// Add device to the end of the list
		if (!GDeviceList)
		{
			GDeviceList = device;
		}
		else
		{
			JoystickHardwareDescriptor* curdevice;

			curdevice = GDeviceList;
			while (curdevice->pNext)
			{
				++deviceIndex;
				curdevice = curdevice->pNext;
			}
			curdevice->pNext = device;
			++deviceIndex; // bump by one since we counted by pNext
		}
        
        device->Hats = (device->hats > 0) ? new uint8[device->hats] : nullptr;
        device->Buttons = (device->buttons > 0) ? new uint8[device->buttons] : nullptr;
        device->Axes = (device->axes > 0) ? new int16[device->axes] : nullptr;

		// Register this controller
        GamepadHardwareInfo Info;
        Info.Guid = device->guid;
        Info.InstanceId = device->iInstanceId;
        Info.Name = device->product;
        Info.NumAxes = device->axes;
        Info.NumButtons = device->buttons;
        Info.NumHats = device->hats;
//        Info.NumBalls
        
        ((MacJoystick*)ctx)->MessageHandler->OnControllerConnectionChange(deviceIndex, Info, true);
	}

    bool MacJoystick::ConfigHIDManager(CFArrayRef matchingArray)
	{
		CFRunLoopRef runloop = CFRunLoopGetCurrent();

		if (IOHIDManagerOpen(hidman, kIOHIDOptionsTypeNone) != kIOReturnSuccess)
		{
			DebugLog("IOHIDManagerOpen() failed\n");
			return false;
		}

		IOHIDManagerSetDeviceMatchingMultiple(hidman, matchingArray);
		IOHIDManagerRegisterDeviceMatchingCallback(hidman, JoystickDeviceWasAddedCallback, this);
        // Get notified when this device is disconnected.
        IOHIDManagerRegisterDeviceRemovalCallback(hidman, JoystickDeviceWasRemovedCallback, this);
        
		IOHIDManagerScheduleWithRunLoop(hidman, runloop, NEKO_GAMEPAD_RUNLOOP_MODE);

		while (CFRunLoopRunInMode(NEKO_GAMEPAD_RUNLOOP_MODE, 0, TRUE) == kCFRunLoopRunHandledSource)
		{
		}

		return true;
	}

	static CFDictionaryRef CreateHIDDeviceMatchDictionary(const UInt32 page, const UInt32 usage, int* okay)
	{
		CFDictionaryRef retval = nullptr;
		CFNumberRef pageNumRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &page);
		CFNumberRef usageNumRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberIntType, &usage);
		const void* keys[2] = { (void*)CFSTR(kIOHIDDeviceUsagePageKey), (void*)CFSTR(kIOHIDDeviceUsageKey) };
		const void* vals[2] = { (void*)pageNumRef, (void*)usageNumRef };

		if (pageNumRef && usageNumRef)
		{
			retval = CFDictionaryCreate(kCFAllocatorDefault, keys, vals, 2, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
		}

		if (pageNumRef)
		{
			CFRelease(pageNumRef);
		}
		if (usageNumRef)
		{
			CFRelease(usageNumRef);
		}

		if (!retval)
		{
			*okay = 0;
		}

		return retval;
	}

    bool MacJoystick::CreateHIDManager(void)
	{
		bool retval = false;
		int okay = 1;
		const void* vals[] =
        {
			(void*)CreateHIDDeviceMatchDictionary(kHIDPage_GenericDesktop, kHIDUsage_GD_Joystick, &okay),
			(void*)CreateHIDDeviceMatchDictionary(kHIDPage_GenericDesktop, kHIDUsage_GD_GamePad, &okay),
			(void*)CreateHIDDeviceMatchDictionary(kHIDPage_GenericDesktop, kHIDUsage_GD_MultiAxisController, &okay),
		};
		const size_t numElements = (sizeof(vals) / sizeof(vals[0]));
		CFArrayRef array = okay ? CFArrayCreate(kCFAllocatorDefault, vals, numElements, &kCFTypeArrayCallBacks) : nullptr;
		size_t i;

		for (i = 0; i < numElements; i++)
		{
			if (vals[i])
			{
				CFRelease((CFTypeRef)vals[i]);
			}
		}

		if (array)
		{
			hidman = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
			if (hidman != nullptr)
			{
				retval = ConfigHIDManager(array);
			}
			CFRelease(array);
		}

		return retval;
	}
    
    MacJoystick::MacJoystick(IGenericMessageHandler* InMessageHandler)
    : MessageHandler(InMessageHandler)
    {
    }
    
    MacJoystick::~MacJoystick()
    {
        while (FreeDevice(GDeviceList))
        {
        }
        
        if (hidman)
        {
            IOHIDManagerUnscheduleFromRunLoop(hidman, CFRunLoopGetCurrent(), NEKO_GAMEPAD_RUNLOOP_MODE);
            IOHIDManagerClose(hidman, kIOHIDOptionsTypeNone);
            CFRelease(hidman);
            hidman = nullptr;
        }
    }
    
    bool MacJoystick::Create()
    {
        assert(!GDeviceList);
        
        return CreateHIDManager();
    }

	int32 MacJoystick::GetNumJoysticks()
	{
		JoystickHardwareDescriptor* device = GDeviceList;
		int nJoySticks = 0;

		while (device)
		{
			if (!device->removed)
			{
				nJoySticks++;
			}
			device = device->pNext;
		}

		return nJoySticks;
	}

	void MacJoystick::Update()
    {
        JoystickHardwareDescriptor* device;
        DeviceElement* element;
        SInt32 range;
        int i;
        
        for (int32 ControllerIndex = 0; ControllerIndex < MAX_NUM_HIDINPUT_CONTROLLERS; ++ControllerIndex)
        {
            device = GetDeviceForIndex(ControllerIndex);
           
            if (!device)
            {
                continue;
            }
            
            if (device->removed)
            {
                // device was unplugged, ignore it
                continue;
            }
            
            element = device->FirstAxis;
            i = 0;
            while (element)
            {
                SInt32 value = GetHIDScaledCalibratedState(device, element, -32768, 32767);
                
                if (value != device->Axes[i])
                {
                    MessageHandler->OnControllerAxis(device->iInstanceId, i, value);
                    device->Axes[i] = value;
                }
                
                element = element->pNext;
                ++i;
            }
            
            element = device->FirstButton;
            i = 0;
            while (element)
            {
                SInt32 value = GetHIDElementState(device, element);
                
                if (value > 1)
                {
                    // handle pressure-sensitive buttons
                    value = 1;
                }
                
                if (value != device->Buttons[i])
                {
                    MessageHandler->OnControllerButton(device->iInstanceId, i, value);
                    device->Buttons[i] = value;
                }
                
                element = element->pNext;
                ++i;
            }
            
            element = device->FirstHat;
            i = 0;
            while (element)
            {
                UInt8 pos = 0;
                
                range = (element->max - element->min + 1);
                SInt32 value = GetHIDElementState(device, element) - element->min;
                
                if (range == 4)
                {
                    // 4 position hatswitch - scale up value
                    value *= 2;
                }
                else if (range != 8)
                {
                    // Neither a 4 nor 8 positions - fall back to default position (centered)
                    value = -1;
                }
                
                switch (value)
                {
                    case 0:
                        pos = GAMEPAD_HAT_UP;
                        break;
                    case 1:
                        pos = GAMEPAD_HAT_RIGHTUP;
                        break;
                    case 2:
                        pos = GAMEPAD_HAT_RIGHT;
                        break;
                    case 3:
                        pos = GAMEPAD_HAT_RIGHTDOWN;
                        break;
                    case 4:
                        pos = GAMEPAD_HAT_DOWN;
                        break;
                    case 5:
                        pos = GAMEPAD_HAT_LEFTDOWN;
                        break;
                    case 6:
                        pos = GAMEPAD_HAT_LEFT;
                        break;
                    case 7:
                        pos = GAMEPAD_HAT_LEFTUP;
                        break;
                    default:
                        pos = GAMEPAD_HAT_CENTERED;
                        break;
                }
                
                if (pos != device->Hats[i])
                {
                    MessageHandler->OnControllerHat(device->iInstanceId, i, pos);
                    device->Hats[i] = pos;
                }
                
                element = element->pNext;
                ++i;
            }
        }
        Detect();
    }

	void MacJoystick::Detect()
	{
		JoystickHardwareDescriptor* device = GDeviceList;
		while (device)
		{
			if (device->removed)
			{
				device = FreeDevice(device);
			}
			else
			{
				device = device->pNext;
			}
		}

		while (CFRunLoopRunInMode(NEKO_GAMEPAD_RUNLOOP_MODE, 0, TRUE) == kCFRunLoopRunHandledSource)
		{
			// no-op. Pending callbacks will fire in CFRunLoopRunInMode().
		}
	}

	const char* MacJoystick::GetNameByIndex(const uint32 index)
	{
		JoystickHardwareDescriptor* device = GetDeviceForIndex(index);
		return device ? device->product : "<unknown>";
	}
}

#endif
