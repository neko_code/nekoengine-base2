//
//  MacOSJoystick.h
//  Neko Engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#pragma once

#if NEKO_OSX

#include <IOKit/hid/IOHIDLib.h>

#include <IOKit/IOKitLib.h>
#include <IOKit/hid/IOHIDKeys.h>
#include <IOKit/hid/IOHIDUsageTables.h>

/* For force feedback testing. */
#include <ForceFeedback/ForceFeedback.h>
#include <ForceFeedback/ForceFeedbackConstants.h>

#include "../../../Core/Joystick.h"

#define MAX_NUM_HIDINPUT_CONTROLLERS 4

namespace Neko
{
    struct DeviceElement
    {
        IOHIDElementRef elementRef;
        IOHIDElementCookie cookie;
        //! HID usage
        uint32 usagePage, usage;
        
        //! Reported min value possible
        SInt32 min;
        // Reported max value possible
        SInt32 max;
        
        // Runtime variables used for auto-calibration
        
        //! Min returned value
        SInt32 minReport;
        //! Max returned value
        SInt32 maxReport;
        
        //! Next element in list
        struct DeviceElement *pNext;
    };
    
    struct JoystickHardwareDescriptor
    {
        //!! HIDManager device handle
        IOHIDDeviceRef deviceRef;
        //! Interface for force feedback, 0 = no ff
        io_service_t ffservice;
        
        //! Name of product
        char product[256];
        
        //! Usage page from IOUSBHID Parser.h which defines general usage
        uint32 usage;
        //! Usage within above page from IOUSBHID Parser.h which defines specific usage
        uint32 usagePage;
        
        //! Number of axis (calculated, not reported by device)
        int axes;
        //! Number of buttons (calculated, not reported by device)
        int buttons;
        //! Number of hat switches (calculated, not reported by device)
        int hats;
        //! Number of total elements (should be total of above) (calculated, not reported by device)
        int elements;
        
        DeviceElement* FirstAxis;
        DeviceElement* FirstButton;
        DeviceElement* FirstHat;
        
        bool removed;
        
        int32 iInstanceId;
        SGuid guid;
        
        // Keeping track on device changes
        
        //! Device hats.
        uint8* Hats;
        //! Device axes.
        int16* Axes;
        //! Device buttons.
        uint8* Buttons;
        
        //! Next device
        struct JoystickHardwareDescriptor *pNext;
    };
    
	/// macOS joystick support
	class MacJoystick
	{
	public:

        MacJoystick(IGenericMessageHandler* InMessageHandler);

        virtual ~MacJoystick();

        bool Create();
        
        void Update();
        void Detect();

        int32 GetNumJoysticks();
        const char* GetNameByIndex(const uint32 Index);

        IGenericMessageHandler* MessageHandler;
        
    private:
        bool CreateHIDManager(void);
        bool ConfigHIDManager(CFArrayRef matchingArray);
        
        JoystickHardwareDescriptor    HardwareInfo[MAX_NUM_HIDINPUT_CONTROLLERS];
	};


}
#endif
