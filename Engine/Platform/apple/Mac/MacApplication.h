//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MacApplication.h
//  Neko engine
//
//  Created by Neko Code on 1/2/15.
//  Copyright (c) 2015 Neko Vision. All rights reserved.
//

#include "../../SystemShared.h"

#if NEKO_OSX

#include "CocoaWindow.h"
#include <Cocoa/Cocoa.h>
#include <Metal/Metal.h>
#include <Carbon/Carbon.h>

#include "../../../Math/Vec.h"
#include "../../../Core/Engine.h"
#include "../../../Core/Application.h"
#include "../../../Core/EventHandler.h"
#include "../../../Data/DefaultAllocator.h"

#include "../../../Mt/Sync.h"

#include "MacWindow.h"
#include "MacCursor.h"

namespace Neko
{
	void ConvertNSRect(NSScreen* Screen, NSRect* Rect);

    class MacJoystick;
	class IAllocator;
	/** Represents macOS Cocoa event. */
	struct SMacEvent
	{
		SMacEvent()
		: Event(nullptr)
		, Window(nullptr)
		, Type(0)
		, ModifierFlags(0)
		, Timestamp(0.0)
		, WindowNumber(0)
		, ButtonNumber(0)
		, ClickCount(0)
		, KeyCode(0)
		, LocationInWindow(Vec2(0, 0))
		, Delta(Vec2(0, 0))
		, ScrollingDelta(Vec2(0, 0))
		, Phase(NSEventPhaseNone)
		, MomentumPhase(NSEventPhaseNone)
		, IsDirectionInvertedFromDevice(false)
		, IsRepeat(false)
		, Characters(nullptr)
		, NotificationName(nullptr)
		, DraggingPasteboard(nullptr)
		, CharactersIgnoringModifiers(nullptr)
		{
		}
		
		SMacEvent(const SMacEvent& Other)
		: Event(Other.Event ? Other.Event : nullptr)
		, Window(Other.Window)
		, Type(Other.Type)
		, LocationInWindow(Other.LocationInWindow)
		, ModifierFlags(Other.ModifierFlags)
		, Timestamp(Other.Timestamp)
		, WindowNumber(Other.WindowNumber)
		, Delta(Other.Delta)
		, ScrollingDelta(Other.ScrollingDelta)
		, ButtonNumber(Other.ButtonNumber)
		, ClickCount(Other.ClickCount)
		, Phase(Other.Phase)
		, MomentumPhase(Other.MomentumPhase)
		, IsRepeat(Other.IsRepeat)
		, KeyCode(Other.KeyCode)
		, IsDirectionInvertedFromDevice(Other.IsDirectionInvertedFromDevice)
		, Characters(Other.Characters ? NEKO_RETAIN(Other.Characters) : nullptr)
		, CharactersIgnoringModifiers(Other.CharactersIgnoringModifiers ? Other.CharactersIgnoringModifiers : nullptr)
		, NotificationName(Other.NotificationName ? Other.NotificationName : nullptr)
		, DraggingPasteboard(Other.DraggingPasteboard ? Other.DraggingPasteboard : nullptr)
		{
			
		}
		
		~SMacEvent()
		{
			
		}
		
		/*  Store all mostly used event properties. */
		
		NSEvent* Event;
		//! Window handle
		CCocoaWindow* Window;
		
		int32 Type;
		NSInteger WindowNumber;
		uint32 KeyCode;
		uint32 ModifierFlags;
		int32 ButtonNumber;
		int32 ClickCount;
		
		NSPasteboard* DraggingPasteboard;
		Vec2 LocationInWindow;
		Vec2 Delta;
		Vec2 ScrollingDelta;
		NSTimeInterval Timestamp;
//		NSGraphicsContext* Context; // deprecated
		NSEventPhase Phase;
		NSEventPhase MomentumPhase;
		NSString* Characters;
		NSString* CharactersIgnoringModifiers;
		NSString* NotificationName;
		bool IsRepeat;
		bool IsDirectionInvertedFromDevice;
	};
	
	/// Native system interface for the engine.
	class CMacApplication final : public IPlatformApplication
	{
	public:
		
		/**
		 *  Constructor/destructor
		 */
		CMacApplication();
		
		virtual ~CMacApplication();
		
		
		virtual void InitializeDevices() override;
		
		virtual void ProcessEvents() override;
		
		virtual void GetEvents() override;
		
		virtual IGenericWindow* MakeWindow(const FWindowDesc& InDesc) override;
		virtual void DestroyWindow(IGenericWindow* Window) override;
		virtual IGenericWindow* GetActiveWindow() override;
		
		virtual bool SetRelativeMouseMode(bool bEnabled) override;
		virtual void SetCursorShow(bool bShown) override;
		virtual void SetCursorType(Neko::ICursor::EMouseCursorType Type) override;
		virtual Neko::ICursor::EMouseCursorType GetCursorType() const override 
		{
			return Cursor->GetType();
		}
		virtual void WarpMouse(int32 X, int32 Y) override;
		
		/**
		 *  Run the loop!!1
		 */
		void NekoEventLoop();
		
		void IgnoreMouseMoveDelta() { bIgnoreMouseMoveDelta = true; }
		
		/** Message handler. */
		virtual void SetMessageHandler(/*const*/ IGenericMessageHandler* InMessageHandler) override;
		
		static CMacApplication* Create();
		static void Destroy(CMacApplication* Application);
		
		
		NEKO_FORCE_INLINE CMacWindow* GetWindow(int32 Index) { return Windows[Index]; }
		
		CMacWindow* FindWindowByNSWindow(CCocoaWindow* WindowHandle);
		
		void CloseWindow(CMacWindow* Window);
		bool OnWindowDestroyed(CMacWindow* Window);
		
		static NSScreen* FindScreenByCocoaPosition(float X, float Y);
		static NSScreen* FindScreenBySlatePosition(float X, float Y, NSRect& FramePixels);
		
		static Vec2 ConvertSlatePositionToCocoa(float X, float Y);
		static Vec2 ConvertCocoaPositionToSlate(float X, float Y);
		
		CMacCursor* Cursor;
		
		bool IsHighDPIModeEnabled() const { return bIsHighDPIModeEnabled; }
		
		/** Processes NSEvent into internal data. */
		void PreProcessEvent(NSObject* Object);
		
		void OnWindowDidMove(CMacWindow* Window);
		void OnWindowDidResize(CMacWindow* Window, bool bRestoreMouseCursorLocking);
		
		void ResetModifierKeys() { CurrentModifierFlags = 0; }
		inline void SetModalMode(bool bEnable)
		{
			bSystemModalMode = bEnable;
		}
		
		//! All created windows.
		TArray<CMacWindow*> Windows;
		
	private:
		
		/** Fills the keymap layout. */
		void UpdateKeymap();
		
		/** Processes the keys from event. */
		EScancode HandleKeyEvent(const SMacEvent& Event, uint32* OutPressedChar);
		/** Checks if internal key modifier state needs to be updated and sends event. */
		void HandleModifierChange(NSUInteger NewModifierFlags, NSUInteger FlagsShift, uint32 TranslatedCode, EScancode Key);
		void UpdateModifierKeysIfNeeded(const SMacEvent& Event);
		/** Callback used by 'EventMonitor'. */
		NSEvent* GetNSEvent(NSEvent* Event);
		/** Returns event's window handle. */
		CCocoaWindow* FindEventWindow(NSEvent* Event) const;
		
		void CloseQueuedWindows();
		
		/*  These should be private.    */
		
		void ProcessGestureEvent(const SMacEvent& Event, CMacWindow* EventWindow);
		void ProcessKeyDownEvent(const SMacEvent& Event, CMacWindow* EventWindow);
		void ProcessKeyUpEvent(const SMacEvent& Event, CMacWindow* EventWindow);
		void ProcessScrollWheelEvent(const SMacEvent& Event, CMacWindow* EventWindow);
		void ProcessMouseUpEvent(const SMacEvent& Event, CMacWindow* EventWindow);
		void ProcessMouseDownEvent(const SMacEvent& Event, CMacWindow* EventWindow);
		void ProcessMouseMovedEvent(const SMacEvent& Event, CMacWindow* EventWindow);
		void ProcessEvent(const SMacEvent& Event);
		
		id EventMonitor;
		id MouseMovedEventMonitor;
		
		bool bIgnoreMouseMoveDelta;
		bool bEmulatingRightClick;
		
		//! Pauses handling of system events so modal windows work properly.
		bool bSystemModalMode;
		
		//! Current dragged window (null if there are none).
		CCocoaWindow* DraggedWindow;
		//! Focused window.
		CMacWindow* ActiveWindow;
		
		TArray<CCocoaWindow*> WindowsToClose;
		TArray<SMacEvent> EventList;
		TArray<SMacEvent> EventToProcessList;
		
		//! Key layout. Updated when input source(language) is changed.
		TISInputSourceRef CurrentKeyLayout;
		
		bool bRelativeMouseMode;
		
		bool bIsHighDPIModeEnabled;
		
		int32 iMouseX;
		int32 iMouseY;
		
		uint32 CurrentModifierFlags;
		
		Neko::MT::SpinMutex EventsLock;
		Neko::MT::SpinMutex WindowsMutex;
		
		DefaultAllocator Allocator;
        
        MacJoystick* GameControllerInterface;
	public:
	};
	
	extern CMacApplication* MacApplication;
}
#endif
