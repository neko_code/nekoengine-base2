//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MacCursor.cpp
//  Neko engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#include "../../SystemShared.h"

#if NEKO_APPLE_FAMILY

#if NEKO_OSX

#include "../../../Core/Log.h"
#include "../../Application.h"

#include "Runloop.h"

#include "MacApplication.h"
#include "MacCursor.h"

#include <IOKit/hid/IOHIDKeys.h>
#include <IOKit/hidsystem/IOHIDShared.h>
#include <AppKit/NSStatusBar.h>

// @todo IOHIDSetAccelerationWithKey is deprecated since macOS 10.12

namespace Neko
{
    int32 GMacDisableMouseAcceleration = 0;
    
    CMacCursor::CMacCursor()
    : bVisible(true)
    , bRelativeMode(false)
    , CurrentPosition(0.0f, 0.0f)
    , bPositionInited(false)
    , HIDInterface(0)
    , SavedAcceleration(0)
    {
        @autoreleasepool {
            // Load up cursors that we'll be using
            for (int32 CursorIndex = 0; CursorIndex < EMouseCursorType::TotalCursorCount; ++CursorIndex)
            {
                CursorHandles[CursorIndex] = nullptr;
                
                NSCursor *CursorHandle = nullptr;
                switch (CursorIndex)
                {
                    case EMouseCursorType::None:
					{
						// Create empty cursor image
						NSImage* data = [[NSImage alloc] initWithSize:NSMakeSize(16, 16)];
						CursorHandle = [[NSCursor alloc] initWithImage:data hotSpot:NSZeroPoint];
                        break;
					}
						
					case EMouseCursorType::Custom:
						break;
						
                    case EMouseCursorType::Default:
                        CursorHandle = [NSCursor arrowCursor];
                        break;
                        
                    case EMouseCursorType::TextEditBeam:
                        CursorHandle = [NSCursor IBeamCursor];
                        break;

                    case EMouseCursorType::ResizeLeftRight:
                        CursorHandle = [NSCursor resizeLeftRightCursor];
                        break;

                    case EMouseCursorType::ResizeUpDown:
                        CursorHandle = [NSCursor resizeUpDownCursor];
                        break;

                    case EMouseCursorType::Crosshairs:
                        CursorHandle = [NSCursor crosshairCursor];
                        break;
                        
                    case EMouseCursorType::Hand:
                        CursorHandle = [NSCursor pointingHandCursor];
                        break;
                        
                    case EMouseCursorType::GrabHand:
                        CursorHandle = [NSCursor openHandCursor];
                        break;
                        
                    case EMouseCursorType::GrabHandClosed:
                        CursorHandle = [NSCursor closedHandCursor];
                        break;
                        
                    case EMouseCursorType::SlashedCircle:
                        CursorHandle = [NSCursor operationNotAllowedCursor];
                        break;

                    default:
                    {
                        // Unrecognized cursor type!
                        assert(0);
                        break;
                    }
                }
                
                CursorHandles[CursorIndex] = CursorHandle;
            }
            SetType(EMouseCursorType::Default);
            
            // @todo Deprecated since macOS 10.12
//            // Get the IOHIDSystem so we can disable mouse acceleration
//            mach_port_t MasterPort;
//            kern_return_t KernResult = IOMasterPort(MACH_PORT_NULL, &MasterPort);
//            if (KERN_SUCCESS == KernResult)
//            {
//                CFMutableDictionaryRef ClassesToMatch = IOServiceMatching("IOHIDSystem");
//                if (ClassesToMatch)
//                {
//                    io_iterator_t MatchingServices;
//                    KernResult = IOServiceGetMatchingServices(MasterPort, ClassesToMatch, &MatchingServices);
//                    if (KERN_SUCCESS == KernResult)
//                    {
//                        io_object_t IntfService;
//                        if ((IntfService = IOIteratorNext(MatchingServices)))
//                        {
//                            KernResult = IOServiceOpen(IntfService, mach_task_self(), kIOHIDParamConnectType, &HIDInterface);
//                            if (KernResult == KERN_SUCCESS)
//                            {
//                                KernResult = IOHIDGetAccelerationWithKey(HIDInterface, CFSTR(kIOHIDMouseAccelerationType), &SavedAcceleration);
//                            }
//                            if (KERN_SUCCESS != KernResult)
//                            {
//                                HIDInterface = 0;
//                            }
//                        }
//                    }
//                }
//            }
        }
    }
    
    
    CMacCursor::~CMacCursor()
    {
        @autoreleasepool {
            bRelativeMode = false;
            
            // Release cursors
            for (int32 CursorIndex = 0; CursorIndex < EMouseCursorType::TotalCursorCount; ++CursorIndex)
            {
                switch (CursorIndex)
                {
                    case EMouseCursorType::Default:
                    case EMouseCursorType::TextEditBeam:
                    case EMouseCursorType::ResizeLeftRight:
                    case EMouseCursorType::ResizeUpDown:
                    case EMouseCursorType::Crosshairs:
                    case EMouseCursorType::Hand:
                    case EMouseCursorType::GrabHand:
                    case EMouseCursorType::GrabHandClosed:
                    case EMouseCursorType::SlashedCircle:
                        // Standard shared cursors don't need to be destroyed
                        break;
                        
					case EMouseCursorType::Custom:
					case EMouseCursorType::None: // Release dummy cursor image
                        if (CursorHandles[CursorIndex] != NULL)
                        {
                            [CursorHandles[CursorIndex] release];
                        }
                        break;
						
                    default:
                    {
                        // Unrecognized cursor type!
                        assert(0);
                        break;
                    }
                }
            }
        }
    }
    
    
    void CMacCursor::UpdateVisibility()
    {
        MainThreadCall(^{
            @autoreleasepool {
                if ([NSApp isActive])
                {
                    if (CurrentCursor && bVisible)
                    {
                        // Enable the cursor.
//						if (!CGCursorIsVisible())
						{
							CGDisplayShowCursor(kCGDirectMainDisplay);
						}
						
//                        if (GMacDisableMouseAcceleration && HIDInterface && bRelativeMode)
//                        {
//                            IOHIDSetAccelerationWithKey(HIDInterface, CFSTR(kIOHIDMouseAccelerationType), SavedAcceleration);
//                        }
                    }
                    else
                    {
                        // Disable the cursor.
//						if (CGCursorIsVisible())
						{
							CGDisplayHideCursor(kCGDirectMainDisplay);
						}
						
						
//                        if (GMacDisableMouseAcceleration && HIDInterface && bRelativeMode && (!CurrentCursor || !bVisible))
//                        {
//                            IOHIDSetAccelerationWithKey(HIDInterface, CFSTR(kIOHIDMouseAccelerationType), -1);
//                        }
                    }
                }
//                else if (GMacDisableMouseAcceleration && HIDInterface && bRelativeMode && (!CurrentCursor || !bVisible))
//                {
//                    IOHIDSetAccelerationWithKey(HIDInterface, CFSTR(kIOHIDMouseAccelerationType), SavedAcceleration);
//                }
			}
		}, NekoNilEventMode, true);
    }
    
    
    void CMacCursor::Show(bool bShow)
    {
        bVisible = bShow;
        UpdateVisibility();
    }
    
    
    bool CMacCursor::SetRelativeMode(bool bEnable)
    {
        CGError result;
        if (bEnable)
        {
            result = CGAssociateMouseAndMouseCursorPosition(false);
        }
        else
        {
            result = CGAssociateMouseAndMouseCursorPosition(true);
        }
        
        if (result != kCGErrorSuccess)
        {
			DebugLog("CGAssociateMouseAndMouseCursorPosition failed with %d", result);
            return false; // hardware fail
        }
        
        return true;
    }

    
    Vec2 CMacCursor::GetPosition() const
    {
        return CurrentPosition;
    };
    
    
    void CMacCursor::Wrap(CMacWindow* Window, const int32 X, const int32 Y)
    {
        if (!bRelativeMode)
        {
            CGAssociateMouseAndMouseCursorPosition(false);
        }
		
        Window->CursorWarpDeltaX = X;
        Window->CursorWarpDeltaY = Y;
        Window->bSeenWrap = true;
        
        const CGPoint point = CGPointMake((float)X, (float)Y);
      
        CGWarpMouseCursorPosition(point);

        // And then reassociate the mouse cursor, which forces the mouse events to come through.
        if (!bRelativeMode)
        {
            CGAssociateMouseAndMouseCursorPosition(true);
        }
        
        UpdateCursorPosition(Vec2(X, Y));
        MacApplication->IgnoreMouseMoveDelta();
        
        if (!bRelativeMode)
        {
            const Vec2& loc = Window->GetLocation();
            MainApplication::Get().OnMouseEvent(Window, X - loc.x, Y - loc.y, false);
        }
    }
    
    
    void CMacCursor::SetType(const EMouseCursorType InNewCursor)
    {
		if (InNewCursor == CurrentType)
		{
			return;
		}
        assert(InNewCursor < EMouseCursorType::TotalCursorCount);
        if (MacApplication && CurrentType == EMouseCursorType::None && InNewCursor != EMouseCursorType::None)
        {
            MacApplication->SetRelativeMouseMode(false);
        }
        CurrentType = InNewCursor;
        CurrentCursor = CursorHandles[InNewCursor];
        if (CurrentCursor)
        {
            [CurrentCursor set];
        }
        UpdateVisibility();
    }

    
    void CMacCursor::SetPosition(const int32 X, const int32 Y)
    {

    }

}
#endif

#endif
