//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MacWindow.h
//  Neko engine
//
//  Created by Neko on 4/23/16.
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#pragma once

#include "../../SystemShared.h"
#include "../../IGenericWindow.h"

#if NEKO_OSX

#include "CocoaWindow.h"

#include <AppKit/AppKit.h>

namespace Neko
{
    /// Implementation for macOS window.
    class CMacWindow final : public IGenericWindow
    {
    public:
        
        CMacWindow();
        
        virtual ~CMacWindow();
        
        virtual void                ReshapeWindow(int32 X, int32 Y, int32 Width, int32 Height) override;
        virtual void                Maximize() override;
        
        virtual void                BringToFront(bool bForce) override;
        
        virtual void                MoveWindowTo(int32 X, int32 Y) override;
        
        virtual Int2                GetSize() override;
        
        virtual Vec2                GetLocation() override;
        
        virtual void                SetTitle(const char* Title) override;
        
        
        virtual void*               GetNativeHandle() const override { return (void*)Handle; }
        
        virtual ENativeWindowMode               GetWindowMode() const override;
        
        virtual void                SetWindowMode(ENativeWindowMode NewWindowMode) override;
        
        virtual bool                IsMaximized() const override;
        
        virtual bool                IsMinimized() const override;
        
        virtual bool                IsVisible() const override;
        
        virtual float               GetDPIScaleFactor() const override;
        
        virtual bool                IsRegularWindow() const override;

        
        virtual void                Show() override;
        
        virtual void                Hide() override;
        
        virtual void                Destroy() override;
		
        
		void OnDisplayReconfiguration(CGDirectDisplayID Display, CGDisplayChangeSummaryFlags Flags);
		
    public:
        
        void Create(const FWindowDesc& Definition);
        
        NEKO_FORCE_INLINE CCocoaWindow* GetNativeHandle()
        {
            return Handle;
        }
        
    private:
        
        CCocoaWindow* Handle;
        NSRect PreFullscreenWindowRect;
        
        bool bIsClosed;
        bool bIsVisible;
     
    public:
        
        int32 PositionX;
        int32 PositionY;
        
        float CursorWarpDeltaX;
        float CursorWarpDeltaY;
        
        bool bSeenWrap;
        bool bMouseWraps;
    };
}

#endif
