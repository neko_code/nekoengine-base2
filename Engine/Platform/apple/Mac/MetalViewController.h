//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MetalViewController.h
//  Neko engine
//
//  Created by Neko on 4/23/16.
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#pragma once

#include "../../SystemShared.h"

#if NEKO_OSX || NEKO_IOS

#if NEKO_OSX
#   import <AppKit/NSView.h>
#   import <AppKit/NSTextInputClient.h>
#   import <Metal/Metal.h>
#elif NEKO_IOS
#   import <UIKit/UIKit.h>
#   import <Metal/Metal.h>
#endif

@interface CCocoaMediaView

#if NEKO_OSX
: NSView <NSTextInputClient>
#endif
{
	NSRange markedRange;
	bool reallyHandledEvent;
}

-(NSInteger)windowLevel;

@end

@interface MetalView : CCocoaMediaView

@end

#endif
