//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MetalViewController.cpp
//  Neko engine
//
//  Created by Neko on 4/23/16.
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#include "../../SystemShared.h"

#if NEKO_APPLE_FAMILY

#import <QuartzCore/CAMetalLayer.h>

#include <AppKit/AppKit.h>
#include "MetalViewController.h"
#include "CocoaWindow.h"

#if NEKO_OSX

#pragma clang diagnostic ignored "-Wprotocol"

@implementation CCocoaMediaView

- (id)initWithFrame:(NSRect)frame
{
	self = [super initWithFrame : frame];
	if (self)
	{
		// Initialization code here.
		reallyHandledEvent = false;
		markedRange = { NSNotFound, 0 };
	}
	return self;
}

-(NSArray*)validAttributesForMarkedText
{
	return[NSArray arrayWithObjects : NSMarkedClauseSegmentAttributeName, NSGlyphInfoAttributeName, nil];
}

-(void)mouseUp:(NSEvent *)theEvent
{
	CCocoaWindow * CocoaWindow = [[self window] isKindOfClass:[CCocoaWindow class]] ? (CCocoaWindow*)[self window] : nil;
	if (CocoaWindow)
	{
		[CocoaWindow mouseUp : theEvent];
	}

	[NSApp preventWindowOrdering];
}

-(void)mouseDown:(NSEvent *)theEvent
{
	CCocoaWindow * CocoaWindow = [[self window] isKindOfClass:[CCocoaWindow class]] ? (CCocoaWindow*)[self window] : nil;
	if (CocoaWindow)
	{
		[CocoaWindow mouseDown : theEvent];
	}

	[NSApp preventWindowOrdering];
}

-(NSInteger)windowLevel
{
	return[[self window] level];
}

-(BOOL)acceptsFirstMouse:(NSEvent *)Event
{
	return YES;
}

-(BOOL)shouldDelayWindowOrderingForEvent : (NSEvent*)Event
{
	return YES;
}

@end

@implementation MetalView

- (id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder : coder];
	if (self)
	{
		
	}

	return self;
}

-(id)initWithFrame:(NSRect)rect
{
	if ((self = [super initWithFrame : rect]))
	{
		
	}

	return self;
}

-(BOOL)isOpaque
{
	return YES;
}

-(BOOL)mouseDownCanMoveWindow
{
	return NO;
}

@end

#endif

#endif
