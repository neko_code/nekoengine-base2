//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MacApplication.cpp
//  Neko engine
//
//  Created by Neko Code on 1/2/15.
//  Copyright (c) 2015 Neko Vision. All rights reserved.
//

#include "../../SystemShared.h"

#if NEKO_OSX

// @todo Multiple window support (events are currently processed for the main window now)

#include "MacApplication.h"

#include <Foundation/Foundation.h>
#include <Carbon/Carbon.h>

#include "../../../Platform/Application.h"
#include "../../../Platform/Platform.h"
#include "../../../Mt/Task.h"
#include "../../../Core/EventHandler.h"
#include "../../../Core/Engine.h"
#include "../../../Core/Log.h"

#include "MacJoystick.h"

#include "Runloop.h"

namespace Neko
{
    void ConvertNSRect(NSScreen* screen, NSRect* r)
    {
        r->origin.y = CGDisplayPixelsHigh(kCGDirectMainDisplay) - r->origin.y - r->size.height;
    }
	
	static void OnDisplayReconfiguration(CGDirectDisplayID Display, CGDisplayChangeSummaryFlags Flags, void* UserInfo)
	{
		CMacApplication* App = (CMacApplication*)UserInfo;
		if (Flags & kCGDisplayDesktopShapeChangedFlag)
		{
			
		}
		
		for (int32 WindowIndex = 0; WindowIndex < App->Windows.GetSize(); ++WindowIndex)
		{
			CMacWindow* Window = App->Windows[WindowIndex];
			Window->OnDisplayReconfiguration(Display, Flags);
		}
	}

	static bool IsAppHighResolutionCapable()
	{
#if !DEBUG // Needs bundled package
		@autoreleasepool
		{
			static bool bIsAppHighResolutionCapable = false;
			static bool bInitialized = false;
			
			if (!bInitialized)
			{
				NSDictionary<NSString *,id>* BundleInfo = [[NSBundle mainBundle] infoDictionary];
				if (BundleInfo)
				{
					NSNumber* Value = (NSNumber*)[BundleInfo objectForKey:@"NSHighResolutionCapable"];
					if (Value)
					{
						bIsAppHighResolutionCapable = [Value boolValue];
					}
				}
				
				bInitialized = true;
			}
			
			return bIsAppHighResolutionCapable;
		}
#else
        return [[NSScreen mainScreen] backingScaleFactor] > 1.0f; // temporary for debug purposes
#endif
	}


    CMacApplication* MacApplication = nullptr;
    
    CMacApplication* CMacApplication::Create()
    {
        MacApplication = new CMacApplication();
        return MacApplication;
    }
    
    void CMacApplication::Destroy(Neko::CMacApplication* Application)
    {
        delete Application;
    }
    
    CMacApplication::CMacApplication()
    : Allocator()
    , EventList(Allocator)
    , EventToProcessList(Allocator)
    , Windows(Allocator)
    , WindowsToClose(Allocator)
    , EventsLock(false)
    , WindowsMutex(false)
    , bIgnoreMouseMoveDelta(false)
    , bRelativeMouseMode(false)
    , bSystemModalMode(false)
	, bIsHighDPIModeEnabled(false)
    , DraggedWindow(nullptr)
    , ActiveWindow(nullptr)
    , CurrentModifierFlags(0)
    , CurrentKeyLayout(nullptr)
    {
        // Switch to the application directory 
//        NSBundle* Bundle = [NSBundle mainBundle];
//        [[NSFileManager defaultManager] changeCurrentDirectoryPath:[Bundle resourcePath]];
       
        // Create the main cursor
        Cursor = NEKO_NEW(Allocator, CMacCursor)();
				
		bIsHighDPIModeEnabled = IsAppHighResolutionCapable();

        // Create event watchers.
		MouseMovedEventMonitor = [NSEvent addGlobalMonitorForEventsMatchingMask:NSEventMaskMouseMoved handler:^(NSEvent* Event) { PreProcessEvent(Event); }];
        EventMonitor = [NSEvent addLocalMonitorForEventsMatchingMask:NSEventMaskAny handler:^(NSEvent* Event) { return GetNSEvent(Event); }];
		
		CGDisplayRegisterReconfigurationCallback(OnDisplayReconfiguration, this);
        
        @autoreleasepool {
            NSDictionary* AppDefaults = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                         [NSNumber numberWithBool:NO], @"AppleMomentumScrollSupported",
                                         [NSNumber numberWithBool:NO], @"ApplePressAndHoldEnabled",
                                         [NSNumber numberWithBool:YES], @"ApplePersistenceIgnoreState",
                                         nil];
            [[NSUserDefaults standardUserDefaults] registerDefaults:AppDefaults];
        }
        
        [NSApp finishLaunching];
        
        // Initialize hardware devices for engine
        InitializeDevices();
    }
    
    CMacApplication::~CMacApplication()
    {
        if (GameControllerInterface)
        {
            NEKO_DELETE(Allocator, GameControllerInterface);
        }
        
        if (MouseMovedEventMonitor)
        {
            [NSEvent removeMonitor:MouseMovedEventMonitor];
        }
        
        if (EventMonitor)
        {
            [NSEvent removeMonitor:EventMonitor];
        }
        
        if (Cursor)
        {
			NEKO_DELETE(Allocator, Cursor);
        }
		
		CGDisplayRemoveReconfigurationCallback(OnDisplayReconfiguration, this);

		assert(Windows.GetSize() == 0);
        Windows.Clear(); // @note Created windows are deallocated manually
        
        MacApplication = nullptr;
    }

    void CMacApplication::ProcessEvents(void)
    {
        EventToProcessList.Clear();
		
		{
			Neko::MT::SpinLock Lock(EventsLock);
			EventToProcessList.Append(EventList);
			EventList.Clear();
		}
		
        for (int32 i = 0; i < EventToProcessList.GetSize(); ++i)
        {
            ProcessEvent(EventToProcessList[i]);
        }
        
        CloseQueuedWindows();
        GameControllerInterface->Update();
    }
    
    const uint32 RESET_EVENT_SUBTYPE = 0x0f00;

    void CMacApplication::GetEvents()
    {
		ProcessGameThreadEvents();
    }
    
    // kVK* keycodes from 'HIToolbox/Events.h'
    static const EScancode GMacScancodeTable[] =
    {
        /*   0 */   SC_A,
        /*   1 */   SC_S,
        /*   2 */   SC_D,
        /*   3 */   SC_F,
        /*   4 */   SC_H,
        /*   5 */   SC_G,
        /*   6 */   SC_Z,
        /*   7 */   SC_X,
        /*   8 */   SC_C,
        /*   9 */   SC_V,
        /*  10 */   SC_NONUSBACKSLASH,
        /*  11 */   SC_B,
        /*  12 */   SC_Q,
        /*  13 */   SC_W,
        /*  14 */   SC_E,
        /*  15 */   SC_R,
        /*  16 */   SC_Y,
        /*  17 */   SC_T,
        /*  18 */   SC_1,
        /*  19 */   SC_2,
        /*  20 */   SC_3,
        /*  21 */   SC_4,
        /*  22 */   SC_6,
        /*  23 */   SC_5,
        /*  24 */   SC_EQUALS,
        /*  25 */   SC_9,
        /*  26 */   SC_7,
        /*  27 */   SC_MINUS,
        /*  28 */   SC_8,
        /*  29 */   SC_0,
        /*  30 */   SC_RIGHTBRACKET,
        /*  31 */   SC_O,
        /*  32 */   SC_U,
        /*  33 */   SC_LEFTBRACKET,
        /*  34 */   SC_I,
        /*  35 */   SC_P,
        /*  36 */   SC_RETURN,
        /*  37 */   SC_L,
        /*  38 */   SC_J,
        /*  39 */   SC_APOSTROPHE,
        /*  40 */   SC_K,
        /*  41 */   SC_SEMICOLON,
        /*  42 */   SC_BACKSLASH,
        /*  43 */   SC_COMMA,
        /*  44 */   SC_SLASH,
        /*  45 */   SC_N,
        /*  46 */   SC_M,
        /*  47 */   SC_PERIOD,
        /*  48 */   SC_TAB,
        /*  49 */   SC_SPACE,
        /*  50 */   SC_GRAVE, /* SC_GRAVE on ANSI and JIS keyboards, SC_NONUSBACKSLASH on ISO (see comment about virtual key code 10 above) */
        /*  51 */   SC_BACKSPACE,
        /*  52 */   SC_KP_ENTER, /* keyboard enter on portables */
        /*  53 */   SC_ESCAPE,
        /*  54 */   SC_RGUI,
        /*  55 */   SC_LGUI,
        /*  56 */   SC_LSHIFT,
        /*  57 */   SC_CAPSLOCK,
        /*  58 */   SC_LALT,
        /*  59 */   SC_LCTRL,
        /*  60 */   SC_RSHIFT,
        /*  61 */   SC_RALT,
        /*  62 */   SC_RCTRL,
        /*  63 */   SC_RGUI, /* fn on portables, acts as a hardware-level modifier already, so we don't generate events for it, also XK_Meta_R */
        /*  64 */   SC_F17,
        /*  65 */   SC_KP_PERIOD,
        /*  66 */   SC_UNKNOWN, /* unknown (unused?) */
        /*  67 */   SC_KP_MULTIPLY,
        /*  68 */   SC_UNKNOWN, /* unknown (unused?) */
        /*  69 */   SC_KP_PLUS,
        /*  70 */   SC_UNKNOWN, /* unknown (unused?) */
        /*  71 */   SC_NUMLOCKCLEAR,
        /*  72 */   SC_VOLUMEUP,
        /*  73 */   SC_VOLUMEDOWN,
        /*  74 */   SC_MUTE,
        /*  75 */   SC_KP_DIVIDE,
        /*  76 */   SC_KP_ENTER, /* keypad enter on external keyboards, fn-return on portables */
        /*  77 */   SC_UNKNOWN, /* unknown (unused?) */
        /*  78 */   SC_KP_MINUS,
        /*  79 */   SC_F18,
        /*  80 */   SC_F19,
        /*  81 */   SC_KP_EQUALS,
        /*  82 */   SC_KP_0,
        /*  83 */   SC_KP_1,
        /*  84 */   SC_KP_2,
        /*  85 */   SC_KP_3,
        /*  86 */   SC_KP_4,
        /*  87 */   SC_KP_5,
        /*  88 */   SC_KP_6,
        /*  89 */   SC_KP_7,
        /*  90 */   SC_UNKNOWN, /* unknown (unused?) */
        /*  91 */   SC_KP_8,
        /*  92 */   SC_KP_9,
        /*  93 */   SC_INTERNATIONAL3, /* Cosmo_USB2ADB.c says "Yen (JIS)" */
        /*  94 */   SC_INTERNATIONAL1, /* Cosmo_USB2ADB.c says "Ro (JIS)" */
        /*  95 */   SC_KP_COMMA, /* Cosmo_USB2ADB.c says ", JIS only" */
        /*  96 */   SC_F5,
        /*  97 */   SC_F6,
        /*  98 */   SC_F7,
        /*  99 */   SC_F3,
        /* 100 */   SC_F8,
        /* 101 */   SC_F9,
        /* 102 */   SC_LANG2, /* Cosmo_USB2ADB.c says "Eisu" */
        /* 103 */   SC_F11,
        /* 104 */   SC_LANG1, /* Cosmo_USB2ADB.c says "Kana" */
        /* 105 */   SC_PRINTSCREEN, /* On ADB keyboards, this key is labeled "F13/print screen". Problem: USB has different usage codes for these two functions. On Apple USB keyboards, the key is labeled "F13" and sends the F13 usage code (SC_F13). I decided to use SC_PRINTSCREEN here nevertheless since SDL applications are more likely to assume the presence of a print screen key than an F13 key. */
        /* 106 */   SC_F16,
        /* 107 */   SC_SCROLLLOCK, /* F14/scroll lock, see comment about F13/print screen above */
        /* 108 */   SC_UNKNOWN, /* unknown (unused?) */
        /* 109 */   SC_F10,
        /* 110 */   SC_APPLICATION, /* windows contextual menu key, fn-enter on portables */
        /* 111 */   SC_F12,
        /* 112 */   SC_UNKNOWN, /* unknown (unused?) */
        /* 113 */   SC_PAUSE, /* F15/pause, see comment about F13/print screen above */
        /* 114 */   SC_INSERT, /* the key is actually labeled "help" on Apple keyboards, and works as such in Mac OS, but it sends the "insert" usage code even on Apple USB keyboards */
        /* 115 */   SC_HOME,
        /* 116 */   SC_PAGEUP,
        /* 117 */   SC_DELETE,
        /* 118 */   SC_F4,
        /* 119 */   SC_END,
        /* 120 */   SC_F2,
        /* 121 */   SC_PAGEDOWN,
        /* 122 */   SC_F1,
        /* 123 */   SC_LEFT,
        /* 124 */   SC_RIGHT,
        /* 125 */   SC_DOWN,
        /* 126 */   SC_UP,
        /* 127 */   SC_POWER
    };
    
    void CMacApplication::UpdateKeymap()
    {
        TISInputSourceRef KeyLayout;
        const void* Data;
        int32 i;
        EScancode scancode;
        EKeyCode keymap[SC_COUNT];
        
        // Check if keymap needs to be updated
        KeyLayout = TISCopyCurrentKeyboardLayoutInputSource();
        if (KeyLayout == CurrentKeyLayout)
        {
            return;
        }
        CurrentKeyLayout = KeyLayout;
        
        GKeymapBase.GetDefaultKeymap(keymap);
        
        // Try Unicode data first
        CFDataRef uchrDataRef = (CFDataRef)TISGetInputSourceProperty(KeyLayout, kTISPropertyUnicodeKeyLayoutData);
        if (uchrDataRef)
        {
            Data = CFDataGetBytePtr(uchrDataRef);
        }
        else
        {
            CFRelease(KeyLayout);
            return;
        }
        
        if (Data)
        {
            UInt32 KeyboardType = LMGetKbdType();
            OSStatus err;
            
            for (i = 0; i < Neko::lengthOf(GMacScancodeTable); i++)
            {
                UniChar s[8];
                UniCharCount len;
                UInt32 DeadKeyState;
                
                // Make sure this scancode is a valid character scancode
                scancode = GMacScancodeTable[i];
                if (scancode == SC_UNKNOWN || (keymap[scancode] & SC_MASK))
                {
                    continue;
                }
                
                DeadKeyState = 0;
                err = UCKeyTranslate ((UCKeyboardLayout *) Data, i, kUCKeyActionDown, 0, KeyboardType, kUCKeyTranslateNoDeadKeysMask, &DeadKeyState, 8, &len, s);
                if (err != noErr)
                {
                    continue;
                }
                
                if (len > 0 && s[0] != 0x10)
                {
                    keymap[scancode] = s[0];
                }
            }
            
            GKeymapBase.SetKeymap(0, keymap, SC_COUNT);
            
            return;
        }
    }

    void CMacApplication::InitializeDevices()
    {
        UpdateKeymap();
		
		GKeymapBase.SetScancodeName(SC_LALT, "Left Option");
		GKeymapBase.SetScancodeName(SC_LGUI, "Left Command");
		GKeymapBase.SetScancodeName(SC_RALT, "Right Option");
		GKeymapBase.SetScancodeName(SC_RGUI, "Right Command");
    }
	
    void CMacApplication::HandleModifierChange(NSUInteger NewModifierFlags, NSUInteger FlagsShift, uint32 TranslatedCode, EScancode Key)
    {
        const bool CurrentPressed = (CurrentModifierFlags & FlagsShift) != 0;
        const bool NewPressed = (NewModifierFlags & FlagsShift) != 0;
        if (CurrentPressed != NewPressed)
        {
            if (NewPressed)
            {
                // Key pressed
				MessageHandler->OnKeyEvent(ActiveWindow, Key, TranslatedCode, true);
            }
            else
            {
                // .. and then released.
                MessageHandler->OnKeyEvent(ActiveWindow, Key, TranslatedCode, false);
            }
        }
    }
    
    void CMacApplication::UpdateModifierKeysIfNeeded( const SMacEvent& Event )
    {
        if (CurrentModifierFlags != Event.ModifierFlags)
        {
            uint32 ModifierFlags = Event.ModifierFlags;
			
			HandleModifierChange(ModifierFlags, (1 << 4), KMOD_RGUI, SC_RGUI);
			HandleModifierChange(ModifierFlags, (1 << 3), KMOD_LGUI, SC_LGUI);
			
			HandleModifierChange(ModifierFlags, (1 << 1), KMOD_LSHIFT, SC_LSHIFT);
			HandleModifierChange(ModifierFlags, (1 << 16), KMOD_CAPS, SC_CAPSLOCK);
			
			HandleModifierChange(ModifierFlags, (1 << 5), KMOD_LALT, SC_LALT);
			HandleModifierChange(ModifierFlags, (1 << 0), KMOD_LCTRL, SC_LCTRL);
		
			HandleModifierChange(ModifierFlags, (1 << 2), KMOD_RSHIFT, SC_RSHIFT);
			HandleModifierChange(ModifierFlags, (1 << 6), KMOD_RALT, SC_RALT);
			
            HandleModifierChange(ModifierFlags, (1 << 13), KMOD_RCTRL, SC_RCTRL);
            
            CurrentModifierFlags = ModifierFlags;
        }
    }
    
    
    static bool IsPrintableKey(uint32 Character)
    {
        switch (Character)
        {
            case NSPauseFunctionKey:		// EKeys::Pause
            case 0x1b:						// EKeys::Escape
            case NSPageUpFunctionKey:		// EKeys::PageUp
            case NSPageDownFunctionKey:		// EKeys::PageDown
            case NSEndFunctionKey:			// EKeys::End
            case NSHomeFunctionKey:			// EKeys::Home
            case NSLeftArrowFunctionKey:	// EKeys::Left
            case NSUpArrowFunctionKey:		// EKeys::Up
            case NSRightArrowFunctionKey:	// EKeys::Right
            case NSDownArrowFunctionKey:	// EKeys::Down
            case NSInsertFunctionKey:		// EKeys::Insert
            case NSDeleteFunctionKey:		// EKeys::Delete
            case NSF1FunctionKey:			// EKeys::F1
            case NSF2FunctionKey:			// EKeys::F2
            case NSF3FunctionKey:			// EKeys::F3
            case NSF4FunctionKey:			// EKeys::F4
            case NSF5FunctionKey:			// EKeys::F5
            case NSF6FunctionKey:			// EKeys::F6
            case NSF7FunctionKey:			// EKeys::F7
            case NSF8FunctionKey:			// EKeys::F8
            case NSF9FunctionKey:			// EKeys::F9
            case NSF10FunctionKey:			// EKeys::F10
            case NSF11FunctionKey:			// EKeys::F11
            case NSF12FunctionKey:			// EKeys::F12
                return false;
                
            default:
                return true;
        }
    }
    
    void CMacApplication::ProcessKeyDownEvent( const SMacEvent& Event, CMacWindow* EventWindow )
    {
		if (!bSystemModalMode)
		{
			// See if keyboard layout needs to be updated
			if (!Event.IsRepeat)
			{
				UpdateKeymap();
			}
			
			uint32 pressedChar;
			EScancode key = HandleKeyEvent( Event, &pressedChar );
			
			if (key != SC_UNKNOWN)
			{
				const bool IsPrintable = IsPrintableKey([Event.Characters characterAtIndex:0]);
				
				MessageHandler->OnKeyEvent(EventWindow, key, 0, true);
				
				bool bCmdKeyPressed = Event.ModifierFlags & NSEventModifierFlagCommand;
				if (!bCmdKeyPressed && IsPrintable)
				{
					MessageHandler->OnCharEvent(EventWindow, pressedChar);
				}
			}
		}
    }
	
    void CMacApplication::ProcessKeyUpEvent( const SMacEvent& Event, CMacWindow* EventWindow )
	{
		if (!bSystemModalMode && [Event.Characters length] > 0 && [Event.CharactersIgnoringModifiers length] > 0)
		{
			uint32 pressedChar;
			EScancode key = HandleKeyEvent( Event, &pressedChar );
			
			if (key != SC_UNKNOWN)
			{
				MessageHandler->OnKeyEvent(EventWindow, key, 0, false);
			}
		}
    }
	
    void CMacApplication::ProcessGestureEvent(const SMacEvent& Event, CMacWindow* EventWindow)
    {
		// NSEventTypeBeginGesture/NSEventTypeEndGesture is not available anymore since macOS 10.11
		// see https://developer.apple.com/reference/appkit/nseventtype/nseventtypebegingesture?language=objc
		
//		if (Event.Phase == NSEventPhaseBegan || Event.MomentumPhase == NSEventPhaseBegan)
//		{
//			MessageHandler->OnBeginGesture();
//		}
//		else if (Event.Phase == NSEventPhaseEnded || Event.MomentumPhase == NSEventPhaseEnded)
//		{
//			MessageHandler->OnEndGesture();
//		}
		
		{
			const ETouchGestureType GestureType = Event.Type == NSEventTypeMagnify ? ETouchGestureType::Magnify : (Event.Type == NSEventTypeSwipe ? ETouchGestureType::Swipe : ETouchGestureType::Rotate);
			MessageHandler->OnTouchGesture(EventWindow, GestureType, Event.Delta, Event.IsDirectionInvertedFromDevice);
		}
    }
    
    void CMacApplication::ProcessMouseMovedEvent(const SMacEvent& Event, CMacWindow* EventWindow)
    {
        NSRect originalFrame = [EventWindow->GetNativeHandle() frame];
        NSRect adjustFrame = [EventWindow->GetNativeHandle() contentRectForFrameRect: originalFrame];
        
        Vec2 location = Event.LocationInWindow;
        
        bool seenWrap = EventWindow->bSeenWrap;
        EventWindow->bSeenWrap = false;
        
        const CGFloat lastMoveX = EventWindow->CursorWarpDeltaX;
        const CGFloat lastMoveY = EventWindow->CursorWarpDeltaY;
        EventWindow->CursorWarpDeltaX = location.x;
        EventWindow->CursorWarpDeltaY = location.y;
        
        // Handle relative mouse movement
        if (bRelativeMouseMode)
        {
            // Ignore events that aren't inside the client area (i.e. title bar.)
            if (EventWindow)
            {
                NSRect windowRect = [[EventWindow->GetNativeHandle() contentView] frame];
                NSPoint point;
                point.x = Event.LocationInWindow.x;
                point.y = Event.LocationInWindow.y;
                if (!NSMouseInRect(point, windowRect, NO))
                {
                    return;
                }
            }
            
            float deltaX = Event.Delta.x;
            float deltaY = Event.Delta.y;
            
            if (seenWrap)
            {
                deltaX = (lastMoveX - EventWindow->CursorWarpDeltaX);
                deltaY += ((CGDisplayPixelsHigh(kCGDirectMainDisplay)/*adjustFrame.size.height*/ - lastMoveY) - EventWindow->CursorWarpDeltaY);
            }
            
//            MainApplication::Get().CenterMouseIfNeeded();
            
            MessageHandler->OnMouseEvent(EventWindow, (int32)deltaX, (int32)deltaY, true);
            
            return;
        }
        
        // Non-relative mouse movement
        
        int32 lx = location.x;
        int32 ly = (int32)adjustFrame.size.height - location.y;
        
        // Clamp within the range of the window.
        
        if (lx < 0)
        {
            lx = 0;
        }
        if (ly < 0)
        {
            ly = 0;
        }
        if (lx > (int32)adjustFrame.size.width)
        {
            lx = (int32)adjustFrame.size.width;
        }
        if (ly > (int32)adjustFrame.size.height)
        {
            ly = (int32)adjustFrame.size.height;
        }
        
        
        iMouseX = lx;
        iMouseY = ly;
        
//        MainApplication::Get().CenterMouseIfNeeded();
        MessageHandler->OnMouseEvent( EventWindow, iMouseX, iMouseY, false );
    }
    
    void CMacApplication::ProcessMouseDownEvent( const SMacEvent& Event, CMacWindow* EventWindow )
    {
        switch( Event.Type )
        {
            case NSEventTypeLeftMouseDown:
            {
                MessageHandler->OnMouseEvent( EventWindow, iMouseX, iMouseY, MouseButton_Left, true );
                break;
            }
            case NSEventTypeRightMouseDown:
            {
                MessageHandler->OnMouseEvent( EventWindow, iMouseX, iMouseY, MouseButton_Right, true );
                break;
            }
            case NSEventTypeOtherMouseDown:
            {
                MessageHandler->OnMouseEvent( EventWindow, iMouseX, iMouseY, MouseButton_Middle, true );
                break;
            }
        }
    }
    
    void CMacApplication::ProcessMouseUpEvent( const SMacEvent& Event, CMacWindow* EventWindow )
    {
        EMouseButtons button = MouseButton_None;
        
        switch (Event.Type)
        {
            case NSEventTypeLeftMouseUp:
            {
                button = MouseButton_Left;
                break;
            }
            case NSEventTypeRightMouseUp:
            {
                button = MouseButton_Right;
                break;
            }
            case NSEventTypeOtherMouseUp:
            {
                button = MouseButton_Middle;
                break;
            }
        }
        
        MessageHandler->OnMouseEvent( EventWindow, iMouseX, iMouseY, button, false );
        DraggedWindow = nullptr; // reset
    }
    
    void CMacApplication::ProcessScrollWheelEvent(const SMacEvent& Event, CMacWindow* EventWindow )
    {
		// trackpad scroll gesture
		if (Event.Phase == NSEventPhaseBegan || Event.MomentumPhase == NSEventPhaseBegan)
		{
			MessageHandler->OnBeginGesture();
		}
		else if (Event.Phase == NSEventPhaseEnded || Event.MomentumPhase == NSEventPhaseEnded)
		{
			MessageHandler->OnEndGesture();
		}
	
//        const float DeltaX = (Event.ModifierFlags & NSEventModifierFlagShift) ? Event.Delta.y : Event.Delta.x;
        const float DeltaY = (Event.ModifierFlags & NSEventModifierFlagShift) ? Event.Delta.x : Event.Delta.y;

        if (Event.MomentumPhase != NSEventPhaseNone || Event.Phase != NSEventPhaseNone)
        {
			// Trackpad
            const Vec2 ScrollDelta(Event.ScrollingDelta.x, Event.ScrollingDelta.y);
            MessageHandler->OnTouchGesture(EventWindow, ETouchGestureType::Scroll, ScrollDelta, Event.IsDirectionInvertedFromDevice);
        }
        else
        {
			// Mouse, this will issue scroll touch event
            MessageHandler->OnTouchGesture(EventWindow, ETouchGestureType::Scroll, Vec2(0.0, DeltaY), Event.IsDirectionInvertedFromDevice);
        }
    }
    
    NSEvent* CMacApplication::GetNSEvent(NSEvent* Event)
    {
        NSEvent* ReturnEvent = Event;
        
        if (MacApplication && !MacApplication->bSystemModalMode)
        {
            const bool bIsResentEvent = [Event type] == NSEventTypeApplicationDefined && [Event subtype] == (NSEventSubtype)RESET_EVENT_SUBTYPE;
            
            if ( bIsResentEvent )
            {
                NSUInteger statusInt = [Event data1];
                NSNumber* status = [NSNumber numberWithInteger:statusInt];
                ReturnEvent = (NSEvent *)status;
            }
            else
            {
                PreProcessEvent(Event);
                
                if ( [Event type] == NSEventTypeKeyDown || [Event type] == NSEventTypeKeyUp )
                {
                    ReturnEvent = nil;
                }
            }
        }
        
        return ReturnEvent;
    }
    
    CCocoaWindow* CMacApplication::FindEventWindow(NSEvent* Event) const
    {
        @autoreleasepool
        {
            CCocoaWindow* EventWindow = [[Event window] isKindOfClass:[CCocoaWindow class]] ? (CCocoaWindow*)[Event window] : nullptr;
            
            if ([Event type] != NSEventTypeKeyDown && [Event type] != NSEventTypeKeyUp)
            {
                NSInteger WindowNumber = [NSWindow windowNumberAtPoint:[NSEvent mouseLocation] belowWindowWithWindowNumber:0];
                NSWindow* WindowUnderCursor = [NSApp windowWithWindowNumber:WindowNumber];
                
                if ([Event type] == NSEventTypeMouseMoved && WindowUnderCursor == nullptr)
                {
                    // Ignore windows owned by other applications
                    return nullptr;
                }
                else if (DraggedWindow)
                {
                    EventWindow = DraggedWindow;
                }
                else if (WindowUnderCursor && [WindowUnderCursor isKindOfClass:[CCocoaWindow class]])
                {
                    EventWindow = (CCocoaWindow*)WindowUnderCursor;
                }
            }
            
            return EventWindow;
        }
    }
    
    void CMacApplication::ProcessEvent( const SMacEvent& Event )
    {
		if (Windows.GetSize() == 0)
		{
			return;
		}
		
        CMacWindow* EventWindow = Windows[0];//FindWindowByNSWindow(Event.Window);
		
        if ( Event.Type )
        {
            switch( Event.Type )
            {
                case NSEventTypeMouseMoved:
                case NSEventTypeLeftMouseDragged:
                case NSEventTypeRightMouseDragged:
                case NSEventTypeOtherMouseDragged:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    ProcessMouseMovedEvent( Event, EventWindow );
                    bIgnoreMouseMoveDelta = false;
                    break;
                }
                case NSEventTypeLeftMouseDown:
                case NSEventTypeRightMouseDown:
                case NSEventTypeOtherMouseDown:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    ProcessMouseDownEvent( Event, EventWindow );
                    break;
                }
                case NSEventTypeLeftMouseUp:
                case NSEventTypeRightMouseUp:
                case NSEventTypeOtherMouseUp:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    ProcessMouseUpEvent( Event, EventWindow );
                    break;
                }
                case NSEventTypeScrollWheel:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    ProcessScrollWheelEvent( Event, EventWindow );
                    break;
				}
                case NSEventTypeMagnify:
                case NSEventTypeSwipe:
				case NSEventTypeRotate:
				case NSEventTypeBeginGesture:
				case NSEventTypeEndGesture:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    ProcessGestureEvent( Event, EventWindow );
                    break;
                }
                case NSEventTypeKeyDown:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    ProcessKeyDownEvent( Event, EventWindow );
                    break;
                }
                case NSEventTypeKeyUp:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    ProcessKeyUpEvent( Event, EventWindow );
                    break;
                }
                case NSEventTypeFlagsChanged:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    break;
                }
                case NSEventTypeMouseEntered:
                case NSEventTypeMouseExited:
                {
                    UpdateModifierKeysIfNeeded( Event );
                    break;
                }
            }
        }
        else
        {
            // @todo
            if (Event.NotificationName == NSWindowWillStartLiveResizeNotification)
            {
                MessageHandler->BeginResizingWindow(EventWindow);
            }
            else if (Event.NotificationName == NSWindowDidEndLiveResizeNotification)
            {
                MessageHandler->EndResizingWindow(EventWindow);
            }
            else if (Event.NotificationName == NSWindowDidEnterFullScreenNotification)
            {
                OnWindowDidResize(EventWindow, true);
            }
            else if (Event.NotificationName == NSWindowDidExitFullScreenNotification)
            {
                OnWindowDidResize(EventWindow, true);
            }
            else if (Event.NotificationName == NSWindowDidBecomeMainNotification)
            {
                // Activated
                ActiveWindow = EventWindow;
                MessageHandler->OnWindowFocus(EventWindow, true);
            }
            else if (Event.NotificationName == NSWindowDidResignMainNotification)
            {
                // Deactivated
                MessageHandler->OnWindowFocus(EventWindow, false);
            }
            else if (Event.NotificationName == NSWindowWillMoveNotification)
            {
                DraggedWindow = EventWindow->GetNativeHandle();
            }
            else if (Event.NotificationName == NSWindowDidMoveNotification)
            {
                
            }
            else if (Event.NotificationName == NSDraggingExited)
            {
                
            }
            else if (Event.NotificationName == NSDraggingUpdated)
            {
                
            }
            else if (Event.NotificationName == NSPrepareForDragOperation)
            {
                @autoreleasepool
                {
                    // @todo drag&drop support for editor
                }
            }
            else if (Event.NotificationName == NSPerformDragOperation)
            {
                
            }
        }
    }
    
    /**
     *  Parse events.
     */
    void CMacApplication::PreProcessEvent( NSObject* Object )
    {
        SMacEvent MacEvent;
        
        if ( Object && [Object isKindOfClass:[NSEvent class]] )
        {
            NSEvent* Event = (NSEvent*)Object;
            
            MacEvent.Window = FindEventWindow(Event);
            MacEvent.Event = NEKO_RETAIN(Event);
            MacEvent.Type = [Event type];
            MacEvent.LocationInWindow = Vec2([Event locationInWindow].x, [Event locationInWindow].y);
            MacEvent.ModifierFlags = (uint32)[Event modifierFlags];
            MacEvent.Timestamp = [Event timestamp];
            MacEvent.WindowNumber = [Event windowNumber];
            
            // @todo Override?
            
            switch( MacEvent.Type )
            {
                case NSEventTypeMouseMoved:
                case NSEventTypeLeftMouseDragged:
                case NSEventTypeRightMouseDragged:
                case NSEventTypeOtherMouseDragged:
                case NSEventTypeSwipe:
                {
                    MacEvent.Delta = bIgnoreMouseMoveDelta ? Vec2(0, 0) : Vec2([Event deltaX], [Event deltaY]);
                    break;
                }
                case NSEventTypeLeftMouseDown:
                case NSEventTypeRightMouseDown:
                case NSEventTypeOtherMouseDown:
                case NSEventTypeLeftMouseUp:
                case NSEventTypeRightMouseUp:
                case NSEventTypeOtherMouseUp:
                {
                    MacEvent.ButtonNumber = static_cast<int32>([Event buttonNumber]);
                    MacEvent.ClickCount = static_cast<int32>([Event clickCount]);
                    
                    if (MacEvent.Type == NSEventTypeLeftMouseDown && (MacEvent.ModifierFlags & NSEventModifierFlagControl))
                    {
                        bEmulatingRightClick = true;
                        MacEvent.Type = NSEventTypeRightMouseDown;
                        MacEvent.ButtonNumber = 2;
                    }
                    else if (MacEvent.Type == NSEventTypeLeftMouseUp && bEmulatingRightClick)
                    {
                        bEmulatingRightClick = false;
                        MacEvent.Type = NSEventTypeRightMouseUp;
                        MacEvent.ButtonNumber = 2;
                    }
                    break;
                }
                case NSEventTypeScrollWheel:
                {
                    MacEvent.Delta = Vec2([Event deltaX], [Event deltaY]);
                    MacEvent.ScrollingDelta = Vec2([Event scrollingDeltaX], [Event scrollingDeltaY]);
                    MacEvent.Phase = [Event phase];
                    MacEvent.MomentumPhase = [Event momentumPhase];
                    MacEvent.IsDirectionInvertedFromDevice = [Event isDirectionInvertedFromDevice];
                    break;
                }
                case NSEventTypeMagnify:
                {
                    MacEvent.Delta = Vec2([Event magnification], [Event magnification]);
                    break;
                }
                case NSEventTypeRotate:
                {
                    MacEvent.Delta = Vec2([Event rotation], [Event rotation]);
                    break;
                }
                case NSEventTypeKeyDown:
                case NSEventTypeKeyUp:
                {
                    if ([[Event characters] length] > 0)
                    {
                        MacEvent.Characters = [Event characters];
                        MacEvent.CharactersIgnoringModifiers = [Event charactersIgnoringModifiers];
                        MacEvent.IsRepeat = [Event isARepeat];
                        MacEvent.KeyCode = [Event keyCode];
                    }
                    else
                    {
                        return;
                    }
                    break;
                }
            }
        }
        else if (Object && [Object isKindOfClass:[NSNotification class]])
        {
            NSNotification* Notification = (NSNotification*)Object;
            MacEvent.NotificationName = [Notification name] ;
            
            if ([[Notification object] isKindOfClass:[CCocoaWindow class]])
            {
                MacEvent.Window = (CCocoaWindow*)[Notification object];
                
                if (MacEvent.NotificationName == NSWindowDidResizeNotification)
                {
                    if (MacEvent.Window)
                    {
                        GameThreadCall(^{
                            CMacWindow* Window = FindWindowByNSWindow(MacEvent.Window);
                            OnWindowDidResize(Window, false); }, @[ NSDefaultRunLoopMode, NekoResizeEventMode, NekoShowEventMode, NekoFullscreenEventMode ], true);
                    }
                    return;
                }
            }
            else if ([[Notification object] conformsToProtocol:@protocol(NSDraggingInfo)])
            {
                NSWindow* NotificationWindow = [(id<NSDraggingInfo>)[Notification object] draggingDestinationWindow];
                
                if (NotificationWindow && [NotificationWindow isKindOfClass:[CCocoaWindow class]])
                {
                    MacEvent.Window = (CCocoaWindow*)NotificationWindow;
                }
                
                if (MacEvent.NotificationName == NSPrepareForDragOperation)
                {
                    MacEvent.DraggingPasteboard = [[(id<NSDraggingInfo>)[Notification object] draggingPasteboard] retain];
                }
            }
        }
        
        // @todo lockless events? (atomic)
        EventsLock.Lock();
        EventList.Push( MacEvent );
        EventsLock.Unlock();
    }
    
    
    void CMacApplication::OnWindowDidMove(CMacWindow* Window)
    {
        @autoreleasepool
        {
            NSRect WindowFrame = [Window->GetNativeHandle() frame];
            NSRect OpenGLFrame = [Window->GetNativeHandle() openGLFrame];// contentRectForFrameRect:[Window->GetNativeHandle() frame]];
            
            const float X = WindowFrame.origin.x;
            float Y = 0.0f;
            
            if ([Window->GetNativeHandle() windowMode] != ENativeWindowMode::Fullscreen)
            {
                Y = WindowFrame.origin.y + OpenGLFrame.size.height;
            }
            
            Vec2 SlatePosition = ConvertCocoaPositionToSlate(X, Y);
            MessageHandler->OnMovedWindow(Window, SlatePosition.x, SlatePosition.y);
            Window->PositionX = SlatePosition.x;
            Window->PositionY = SlatePosition.y;
        }
    }
    
    void CMacApplication::OnWindowDidResize(CMacWindow* Window, bool bRestoreMouseCursorLocking)
    {
        @autoreleasepool
        {
            OnWindowDidMove(Window);
            
            NSRect rect = [Window->GetNativeHandle() openGLFrame];
        
            // default is no override
            uint32 Width = rect.size.width * Window->GetDPIScaleFactor();
            uint32 Height = rect.size.height * Window->GetDPIScaleFactor();
            
            if ([Window->GetNativeHandle() windowMode] == ENativeWindowMode::WindowedFullscreen)
            {
                // Grab current monitor data for sizing
                Width = Math::TruncToInt([[Window->GetNativeHandle() screen] frame].size.width) * Window->GetDPIScaleFactor();
                Height = Math::TruncToInt([[Window->GetNativeHandle() screen] frame].size.height) * Window->GetDPIScaleFactor();
            }

            MessageHandler->OnSizeEvent(Window, Width, Height);
        }
    }

    EScancode CMacApplication::HandleKeyEvent( const SMacEvent& event, uint32* PressedChar )
    {
        // @note Scancode = keycode
        unsigned short scancode = event.KeyCode;
        EScancode code;
        
        if ((scancode == 10 || scancode == 50) && KBGetLayoutType(LMGetKbdType()) == kKeyboardISO)
		{
            /* see comments in SDL_cocoakeys.h */
            scancode = 60 - scancode;
        }
        
        if (scancode < Neko::lengthOf(GMacScancodeTable))
        {
            code = GMacScancodeTable[scancode];
			// Output native characters instead of getting keys from keymap.
			*PressedChar = [event.Characters characterAtIndex:0];
        }
        else
        {
            /* Hmm, does this ever happen?  If so, need to extend the keymap... */
            code = SC_UNKNOWN;
        }
		
        return code;
    }
    
    void CMacApplication::SetMessageHandler(IGenericMessageHandler* InMessageHandler)
    {
        IPlatformApplication::SetMessageHandler(InMessageHandler);
        
        if (!GameControllerInterface)
        {
            GameControllerInterface = NEKO_NEW(Allocator, MacJoystick)(GetMessageHandler());
            if (!GameControllerInterface->Create())
            {
                GLogError.log("System") << "Couldn't initialize platform input device manager.";
                NEKO_DELETE(Allocator, GameControllerInterface);
            }
        }
    }


    void CMacApplication::CloseWindow(CMacWindow* Window)
    {
        MessageHandler->OnWindowClose(Window);
    }
    
    
    bool CMacApplication::SetRelativeMouseMode(bool bEnabled)
    {
        bRelativeMouseMode = bEnabled;
        Cursor->SetRelativeMode(bEnabled);
        
        return true;
    }
    
    void CMacApplication::SetCursorShow(bool bShow)
    {
        Cursor->Show(bShow);
    }
    
    void CMacApplication::WarpMouse(int32 X, int32 Y)
    {
        Vec2 origin = ActiveWindow->GetLocation();
        
        Cursor->Wrap(ActiveWindow, origin.x + X, origin.y + Y );
    }
	
	void CMacApplication::SetCursorType(Neko::ICursor::EMouseCursorType Type)
	{
		Cursor->SetType(Type);
	}
    
    IGenericWindow* CMacApplication::MakeWindow(const FWindowDesc &InDesc)
    {
        CMacWindow* window;
        
        window = new CMacWindow();
        window->Create(InDesc);
        
        Windows.Push(window);
        
        return window;
    }
    
    void CMacApplication::DestroyWindow(IGenericWindow* Window)
    {
        if (Window)
        {
            Window->Destroy(); // calls 'OnWindowDestroyed'
        }
        // Don't do anything here.
    }

    bool CMacApplication::OnWindowDestroyed(CMacWindow* Window)
    {
        @autoreleasepool
        {
            if ([Window->GetNativeHandle() isMainWindow])
            {
                MessageHandler->OnWindowFocus(Window, false);
            }
            
            if (WindowsToClose.IndexOf(Window->GetNativeHandle()) == INDEX_NONE)
            {
                WindowsToClose.Push(Window->GetNativeHandle());
            }
            Windows.EraseItem(Window);
            return true;
        }
    }
    
    void CMacApplication::CloseQueuedWindows()
    {
        if (WindowsToClose.GetSize() > 0)
        {
            MainThreadCall(^
            {
                @autoreleasepool
                {
                    // Set the new key window, if needed. We cannot trust Cocoa to set the key window to the actual top most window. It will prefer windows with title bars so,
                    // for example, will choose the main window over a context menu window, when closing a submenu.
                    NSArray* AllWindows = [NSApp orderedWindows];
                    for (NSWindow* Window : AllWindows)
                    {
                        if ([Window isKindOfClass:[CCocoaWindow class]] && WindowsToClose.IndexOf((CCocoaWindow*)Window) == INDEX_NONE && [Window canBecomeKeyWindow])
                        {
                            if (Window != [NSApp keyWindow])
                            {
                                [Window makeKeyWindow];
                            }
                            break;
                        }
                    }
                    
                    for (CCocoaWindow* Window : WindowsToClose)
                    {
                        [Window close];
                        [Window release];
                    }
                }
            });
            
            WindowsToClose.Clear();
        }
    }

    
    CMacWindow* CMacApplication::FindWindowByNSWindow(CCocoaWindow* WindowHandle)
    {
        MT::SpinLock Lock(WindowsMutex);
        
        for (int32 WindowIndex = 0; WindowIndex < Windows.GetSize(); ++WindowIndex)
        {
            CMacWindow* Window = Windows[WindowIndex];
            if (Window->GetNativeHandle() == WindowHandle)
            {
                return Window;
            }
        }
        
        return nullptr;
    }

    IGenericWindow* CMacApplication::GetActiveWindow()
    {
        return ActiveWindow;
    }

    NSScreen* CMacApplication::FindScreenBySlatePosition(float X, float Y, NSRect& FramePixels)
    {
        NSPoint Point = NSMakePoint(X, Y);
        
        //        FScopeLock Lock(&GAllScreensMutex);
        
        NSArray* screens = [NSScreen screens];
        // Figure out which screen to place this windowю
        NSScreen* screen = screens[0];
        for (NSScreen *candidate in screens)
        {
            FramePixels = [candidate frame];
            
            FramePixels.origin.x *= candidate.backingScaleFactor;
            FramePixels.origin.y *= candidate.backingScaleFactor;
            
            FramePixels.size.width = FramePixels.size.width * candidate.backingScaleFactor;
            FramePixels.size.height = FramePixels.size.height * candidate.backingScaleFactor;
            
            if (NSPointInRect(Point, FramePixels))
            {
                screen = candidate;
                break;
            }
        }
        
        return screen;
    }
    
    NSScreen* CMacApplication::FindScreenByCocoaPosition(float X, float Y)
    {
        NSPoint Point = NSMakePoint(X, Y);
        
        //        FScopeLock Lock(&GAllScreensMutex);
        
        NSArray* screens = [NSScreen screens];
        // Figure out which screen to place this windowю
        NSScreen* screen = nil;
        for (NSScreen *candidate in screens)
        {
            NSRect screenRect = [candidate frame];
            
            if (NSPointInRect(Point, screenRect))
            {
                screen = candidate;
                break;
            }
        }
        
        return screen;
    }
    
    Vec2 CMacApplication::ConvertSlatePositionToCocoa(float X, float Y)
    {
        NSRect framePixels;
		NSScreen* Screen = FindScreenBySlatePosition(X, Y, framePixels);
		const bool bUseHighDPIMode = MacApplication ? MacApplication->IsHighDPIModeEnabled() : IsAppHighResolutionCapable();
		const float DPIScaleFactor = bUseHighDPIMode ? Screen.backingScaleFactor : 1.0f;
		
        const Vec2 OffsetOnScreen = Vec2(X - framePixels.origin.x, framePixels.origin.y + framePixels.size.height - Y) / DPIScaleFactor;
        return Vec2(Screen.frame.origin.x + OffsetOnScreen.x, Screen.frame.origin.y + OffsetOnScreen.y);
    }
    
    Vec2 CMacApplication::ConvertCocoaPositionToSlate(float X, float Y)
    {
        NSScreen* Screen = FindScreenByCocoaPosition(X, Y);
		const bool bUseHighDPIMode = MacApplication ? MacApplication->IsHighDPIModeEnabled() : IsAppHighResolutionCapable();
		const float DPIScaleFactor = bUseHighDPIMode ? Screen.backingScaleFactor : 1.0f;
		
        const Vec2 OffsetOnScreen = Vec2(X - Screen.frame.origin.x, Screen.frame.origin.y + Screen.frame.size.height - Y) * DPIScaleFactor;
        return Vec2(Screen.frame.origin.x + OffsetOnScreen.x, Screen.frame.origin.y + OffsetOnScreen.y);
    }
    
    // Static methods.
 
    
    IPlatformApplication* IPlatformApplication::Create()
    {
        return CMacApplication::Create();
    }
    
    void IPlatformApplication::Destroy(IPlatformApplication& Application)
    {
        CMacApplication::Destroy(static_cast<CMacApplication*>(&Application));
    }
}
#endif
