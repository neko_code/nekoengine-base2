//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  BitMessage.h
//  Neko engine

#pragma once

#include "../Platform/SystemShared.h"
#include "../../Network/NetAddress.h"
#include "Dictionary.h"
#include "Text.h"
#include "Vec.h"

namespace Neko
{
    /// Bit message.
    class NEKO_ENGINE_API CBitMessage
    {
    public:
        CBitMessage()
        {
            InitWrite(NULL, 0);
        }
        CBitMessage(Byte* data, int length)
        {
            InitWrite(data, length);
        }
        //        CBitMessage( const Byte * data, int length ) { InitRead( data, length ); }
        
        // both read & write
        void InitWrite(Byte* data, int length);
        
        // read only
        void InitRead(Byte* data, int length);
        
        // get data for writing
        Byte* GetWriteData();
        
        // get data for reading
        Byte* GetReadData();
        
        // get the maximum message size
        int GetMaxSize() const;
        
        // generate error if not set and message is overflowed
        void SetAllowOverflow(bool set);
        
        // returns true if the message was overflowed
        bool IsOverflowed() const;
        
        // size of the message in bytes
        int GetSize() const;
        
        // set the message size
        void SetSize(int size);
        
        // get current write bit
        int GetWriteBit() const;
        
        // set current write bit
        void SetWriteBit(int bit);
        
        // returns number of bits written
        int GetNumBitsWritten() const;
        
        // space left in bytes for writing
        int GetRemainingSpace() const;
        
        // space left in bits for writing
        int GetRemainingWriteBits() const;
        
        // Write State
        
        // save the write state
        void SaveWriteState(int& s, int& b, uint64& t) const;
        
        // restore the write state
        void RestoreWriteState(int s, int b, uint64 t);
        
        // Reading
        
        // bytes read so far
        int GetReadCount() const;
        
        // set the number of bytes and bits read
        void SetReadCount(int bytes);
        
        // get current read bit
        int GetReadBit() const;
        
        // set current read bit
        void SetReadBit(int bit);
        
        // returns number of bits read
        int GetNumBitsRead() const;
        
        // number of bytes left to read
        int GetRemainingData() const;
        
        // number of bits left to read
        int GetRemainingReadBits() const;
        
        // save the read state
        void SaveReadState(int& c, int& b) const;
        
        // restore the read state
        void RestoreReadState(int c, int b);
        
        // Writing
        
        // begin writing
        void BeginWriting();
        
        // write up to the next Byte boundary
        void WriteByteAlign();
        
        // write the specified number of bits
        // If the number of bits is negative a sign is included.
        void WriteBits(int value, int numBits);
        
        void WriteBool(bool c);
        void WriteChar(int8 c);
        void WriteByte(uint8 c);
        void WriteShort(int16 c);
        void WriteUShort(uint16 c);
        void WriteLong(int32 c);
        void WriteLongLong(int64 c);
        void WriteFloat(float f);
        void WriteFloat(float f, int exponentBits, int mantissaBits);
        void WriteAngle8(float f);
        void WriteAngle16(float f);
        void WriteDir(const Vec3& dir, int numBits);
        void WriteString(const char* s, int maxLength = -1, bool make7Bit = true);
        void WriteData(const void* data, int length);
        void WriteNetadr(const Net::NetAddress adr);
        
        void WriteUNorm8(float f)
        {
            WriteByte(Math::TruncToByte(f * 255.0f));
        }
        void WriteUNorm16(float f)
        {
            WriteUShort(Math::TruncToInt(f * 65535.0f));
        }
        void WriteNorm16(float f)
        {
            WriteShort(Math::TruncToInt(f * 32767.0f));
        }
        
        void WriteDeltaChar(int8 oldValue, int8 newValue)
        {
            WriteByte(newValue - oldValue);
        }
        void WriteDeltaByte(uint8 oldValue, uint8 newValue)
        {
            WriteByte(newValue - oldValue);
        }
        void WriteDeltaShort(int16 oldValue, int16 newValue)
        {
            WriteUShort(newValue - oldValue);
        }
        void WriteDeltaUShort(uint16 oldValue, uint16 newValue)
        {
            WriteUShort(newValue - oldValue);
        }
        void WriteDeltaLong(int32 oldValue, int32 newValue)
        {
            WriteLong(newValue - oldValue);
        }
        void WriteDeltaFloat(float oldValue, float newValue)
        {
            WriteFloat(newValue - oldValue);
        }
        
        bool ReadDeltaDict(CDictionary& dict, const CDictionary* base) const;
        bool WriteDeltaDict(const CDictionary& Dict, const CDictionary* Base);
        
        // Timers, etc..
        int ReadDeltaLongCounter(int oldValue) const;
        void WriteDeltaLongCounter(int oldValue, int newValue);
        
        template <typename T>
        void WriteVectorFloat(const T& v)
        {
            for (int i = 0; i < v.GetDimension(); i++)
            {
                WriteFloat(v[i]);
            }
        }
        template <typename T>
        void WriteVectorUNorm8(const T& v)
        {
            for (int i = 0; i < v.GetDimension(); i++)
            {
                WriteUNorm8(v[i]);
            }
        }
        template <typename T>
        void WriteVectorUNorm16(const T& v)
        {
            for (int i = 0; i < v.GetDimension(); i++)
            {
                WriteUNorm16(v[i]);
            }
        }
        template <typename T>
        void WriteVectorNorm16(const T& v)
        {
            for (int i = 0; i < v.GetDimension(); i++)
            {
                WriteNorm16(v[i]);
            }
        }
        
        // begin reading.
        void BeginReading() const;
        
        // read up to the next Byte boundary
        void ReadByteAlign() const;
        
        // read the specified number of bits
        int ReadBits(int numBits) const;
        
        bool ReadBool() const;
        int ReadChar() const;
        int ReadByte() const;
        int ReadShort() const;
        int ReadUShort() const;
        int ReadLong() const;
        int64 ReadLongLong() const;
        float ReadFloat() const;
        float ReadFloat(int exponentBits, int mantissaBits) const;
        float ReadAngle8() const;
        float ReadAngle16() const;
        Vec3 ReadDir(int numBits) const;
        int ReadString(char* buffer, int bufferSize) const;
        int ReadString(Text& str) const;
        int ReadData(void* data, int length) const;
        void ReadNetadr(Net::NetAddress* adr) const;
        
        float ReadUNorm8() const
        {
            return ReadByte() / 255.0f;
        }
        float ReadUNorm16() const
        {
            return ReadUShort() / 65535.0f;
        }
        float ReadNorm16() const
        {
            return ReadShort() / 32767.0f;
        }
        
        int8 ReadDeltaChar(int8 oldValue) const
        {
            return oldValue + ReadByte();
        }
        uint8 ReadDeltaByte(uint8 oldValue) const
        {
            return oldValue + ReadByte();
        }
        int16 ReadDeltaShort(int16 oldValue) const
        {
            return oldValue + ReadUShort();
        }
        uint16 ReadDeltaUShort(uint16 oldValue) const
        {
            return oldValue + ReadUShort();
        }
        int32 ReadDeltaLong(int32 oldValue) const
        {
            return oldValue + ReadLong();
        }
        float ReadDeltaFloat(float oldValue) const
        {
            return oldValue + ReadFloat();
        }
        
        
        template <typename T>
        void ReadVectorFloat(T& v) const
        {
            for (int i = 0; i < v.GetDimension(); i++)
            {
                v[i] = ReadFloat();
            }
        }
        template <typename T>
        void ReadVectorUNorm8(T& v) const
        {
            for (int i = 0; i < v.GetDimension(); i++)
            {
                v[i] = ReadUNorm8();
            }
        }
        template <typename T>
        void ReadVectorUNorm16(T& v) const
        {
            for (int i = 0; i < v.GetDimension(); i++)
            {
                v[i] = ReadUNorm16();
            }
        }
        template <typename T>
        void ReadVectorNorm16(T& v) const
        {
            for (int i = 0; i < v.GetDimension(); i++)
            {
                v[i] = ReadNorm16();
            }
        }
        
        
        static int DirToBits(const Vec3& dir, int numBits);
        static Vec3 BitsToDir(int bits, int numBits);
        
        void SetHasChanged(bool b)
        {
            hasChanged = b;
        }
        bool HasChanged() const
        {
            return hasChanged;
        }
        
    private:
        Byte* writeData; // pointer to data for writing
        /*const*/ Byte* readData; // pointer to data for reading
        int maxSize; // maximum size of message in bytes
        int curSize; // current size of message in bytes
        mutable int writeBit; // number of bits written to the last written Byte
        mutable int readCount; // number of bytes read so far
        mutable int readBit; // number of bits read from the last read Byte
        bool allowOverflow; // if false, generate error when the message is overflowed
        bool overflowed; // set true if buffer size failed (with allowOverflow set)
        bool hasChanged; // Hack
        
        mutable uint64 tempValue;
        
    private:
        bool CheckOverflow(int numBits);
        Byte* GetByteSpace(int length);
    };
    
    NEKO_FORCE_INLINE void CBitMessage::InitWrite(Byte* data, int length)
    {
        writeData = data;
        readData = data;
        maxSize = length;
        curSize = 0;
        
        writeBit = 0;
        readCount = 0;
        readBit = 0;
        allowOverflow = false;
        overflowed = false;
        
        tempValue = 0;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::InitRead(Byte* data, int length)
    {
        writeData = NULL;
        readData = data;
        maxSize = length;
        curSize = length;
        
        writeBit = 0;
        readCount = 0;
        readBit = 0;
        allowOverflow = false;
        overflowed = false;
        
        tempValue = 0;
    }
    
    NEKO_FORCE_INLINE Byte* CBitMessage::GetWriteData()
    {
        return writeData;
    }
    
    NEKO_FORCE_INLINE Byte* CBitMessage::GetReadData()
    {
        return readData;
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetMaxSize() const
    {
        return maxSize;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::SetAllowOverflow(bool set)
    {
        allowOverflow = set;
    }
    
    NEKO_FORCE_INLINE bool CBitMessage::IsOverflowed() const
    {
        return overflowed;
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetSize() const
    {
        return curSize + (writeBit != 0);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::SetSize(int size)
    {
        assert(writeBit == 0);
        
        if (size > maxSize)
        {
            curSize = maxSize;
        }
        else
        {
            curSize = size;
        }
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetWriteBit() const
    {
        return writeBit;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::SetWriteBit(int bit)
    {
        // see CBitMessage::WriteByteAlign
        assert(false);
        writeBit = bit & 7;
        if (writeBit)
        {
            writeData[curSize - 1] &= (1 << writeBit) - 1;
        }
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetNumBitsWritten() const
    {
        return (curSize << 3) + writeBit;
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetRemainingSpace() const
    {
        return maxSize - GetSize();
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetRemainingWriteBits() const
    {
        return (maxSize << 3) - GetNumBitsWritten();
    }
    
    NEKO_FORCE_INLINE void CBitMessage::SaveWriteState(int& s, int& b, uint64& t) const
    {
        s = curSize;
        b = writeBit;
        t = tempValue;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::RestoreWriteState(int s, int b, uint64 t)
    {
        curSize = s;
        writeBit = b & 7;
        if (writeBit)
        {
            writeData[curSize] &= (1 << writeBit) - 1;
        }
        tempValue = t;
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetReadCount() const
    {
        return readCount;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::SetReadCount(int bytes)
    {
        readCount = bytes;
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetReadBit() const
    {
        return readBit;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::SetReadBit(int bit)
    {
        readBit = bit & 7;
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetNumBitsRead() const
    {
        return ((readCount << 3) - ((8 - readBit) & 7));
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetRemainingData() const
    {
        assert(writeBit == 0);
        return curSize - readCount;
    }
    
    NEKO_FORCE_INLINE int CBitMessage::GetRemainingReadBits() const
    {
        assert(writeBit == 0);
        return (curSize << 3) - GetNumBitsRead();
    }
    
    NEKO_FORCE_INLINE void CBitMessage::SaveReadState(int& c, int& b) const
    {
        assert(writeBit == 0);
        c = readCount;
        b = readBit;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::RestoreReadState(int c, int b)
    {
        assert(writeBit == 0);
        readCount = c;
        readBit = b & 7;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::BeginWriting()
    {
        curSize = 0;
        overflowed = false;
        writeBit = 0;
        tempValue = 0;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteByteAlign()
    {
        // it is important that no uninitialized data slips in the msg stream,
        // because we use memcmp to decide if entities have changed and wether we should transmit them
        // this function has the potential to leave uninitialized bits into the stream,
        // however CBitMessage::WriteBits is properly initializing the Byte to 0 so hopefully we are
        // still safe
        // adding this extra check just in case
        curSize += writeBit != 0;
        assert(writeBit == 0
               || ((writeData[curSize - 1] >> writeBit) == 0)); // had to early out writeBit == 0 because
        // when writeBit == 0 writeData[curSize -
        // 1] may be the previous Byte written and
        // trigger false positives
        writeBit = 0;
        tempValue = 0;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteBool(bool c)
    {
        WriteBits(c, 1);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteChar(int8 c)
    {
        WriteBits(c, -8);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteByte(uint8 c)
    {
        WriteBits(c, 8);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteShort(int16 c)
    {
        WriteBits(c, -16);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteUShort(uint16 c)
    {
        WriteBits(c, 16);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteLong(int32 c)
    {
        WriteBits(c, 32);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteLongLong(int64 c)
    {
        int a = static_cast<int>(c);
        int b = c >> 32;
        WriteBits(a, 32);
        WriteBits(b, 32);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteFloat(float f)
    {
        WriteBits(*reinterpret_cast<int*>(&f), 32);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteAngle8(float f)
    {
        WriteByte(ANGLE2BYTE(f));
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteAngle16(float f)
    {
        WriteShort(ANGLE2SHORT(f));
    }
    
    NEKO_FORCE_INLINE void CBitMessage::WriteDir(const Vec3& dir, int numBits)
    {
        WriteBits(DirToBits(dir, numBits), numBits);
    }
    
    NEKO_FORCE_INLINE void CBitMessage::BeginReading() const
    {
        readCount = 0;
        readBit = 0;
        
        writeBit = 0;
        tempValue = 0;
    }
    
    NEKO_FORCE_INLINE void CBitMessage::ReadByteAlign() const
    {
        readBit = 0;
    }
    
    NEKO_FORCE_INLINE bool CBitMessage::ReadBool() const
    {
        return (ReadBits(1) == 1) ? true : false;
    }
    
    NEKO_FORCE_INLINE int CBitMessage::ReadChar() const
    {
        return (signed char)ReadBits(-8);
    }
    
    NEKO_FORCE_INLINE int CBitMessage::ReadByte() const
    {
        return (unsigned char)ReadBits(8);
    }
    
    NEKO_FORCE_INLINE int CBitMessage::ReadShort() const
    {
        return (short)ReadBits(-16);
    }
    
    NEKO_FORCE_INLINE int CBitMessage::ReadUShort() const
    {
        return (unsigned short)ReadBits(16);
    }
    
    NEKO_FORCE_INLINE int CBitMessage::ReadLong() const
    {
        return ReadBits(32);
    }
    
    NEKO_FORCE_INLINE int64 CBitMessage::ReadLongLong() const
    {
        int64 a = ReadBits(32);
        int64 b = ReadBits(32);
        int64 c = (0x00000000ffffffff & a) | (b << 32);
        return c;
    }
    
    NEKO_FORCE_INLINE float CBitMessage::ReadFloat() const
    {
        float value;
        *reinterpret_cast<int*>(&value) = ReadBits(32);
        return value;
    }
    
    NEKO_FORCE_INLINE float CBitMessage::ReadAngle8() const
    {
        return BYTE2ANGLE(ReadByte());
    }
    
    NEKO_FORCE_INLINE float CBitMessage::ReadAngle16() const
    {
        return SHORT2ANGLE(ReadShort());
    }
    
    NEKO_FORCE_INLINE Vec3 CBitMessage::ReadDir(int numBits) const
    {
        return BitsToDir(ReadBits(numBits), numBits);
    }
}
