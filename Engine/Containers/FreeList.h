//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  FreeList.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Data/IAllocator.h"

namespace Neko
{
    template<class T, int32 ChunkSize>
    class FreeList final : public IAllocator
    {
    public:
        
        explicit FreeList(IAllocator& InAllocator)
        : Allocator(InAllocator)
        {
            Heap = static_cast<T*>(Allocator.AllocateAligned(sizeof(T) * ChunkSize, ALIGN_OF(T)));
            PoolIndex = ChunkSize;
            
            for (int32 i = 0; i < ChunkSize; ++i)
            {
                Pool[i] = &Heap[i];
            }
        }
        
        ~FreeList()
        {
            Allocator.DeallocateAligned(Heap);
        }
        
        void* Allocate(size_t size) override
        {
            assert(size == sizeof(T));
            return (PoolIndex > 0) ? Pool[--PoolIndex] : nullptr;
        }
        
        void Deallocate(void* ptr) override
        {
            assert(((uptr)ptr >= (uptr)&Heap[0]) && ((uptr)ptr < (uptr)&Heap[ChunkSize]));
            Pool[PoolIndex++] = reinterpret_cast<T*>(ptr);
        }
        
        void* Reallocate(void*, size_t) override
        {
            assert(false);
            return nullptr;
        }
        
        void* AllocateAligned(size_t size, size_t align) override
        {
            void* ptr = Allocate(size);
            assert((uptr)ptr % align == 0);
            return ptr;
        }
        
        void DeallocateAligned(void* ptr) override
        {
            return Deallocate(ptr);
        }
        
        void* ReallocateAligned(void* ptr, size_t size, size_t align) override
        {
            assert(size <= ALIGN_OF(T));
            return Reallocate(ptr, size);
        }
        
    private:
        
        IAllocator&	Allocator;
        int32		PoolIndex;
        T*			Pool[ChunkSize];
        T*			Heap;
    };
    
    template<int32 ChunkSize>
    class FreeList<int32, ChunkSize>
    {
    public:
        
        FreeList()
        {
            PoolIndex = ChunkSize;
            
            for (int32 i = 0; i < ChunkSize; ++i)
            {
                Pool[i] = i;
            }
        }
        
        int32 Alloc(void)
        {
            return (PoolIndex > 0) ? Pool[--PoolIndex] : (-1);
        }
        
        void release(int32 id)
        {
            assert (id >= 0 && id < ChunkSize);
            Pool[PoolIndex++] = id;
        }
        
    private:
        
        int32		PoolIndex;
        int32		Pool[ChunkSize];
    };
} // Neko
