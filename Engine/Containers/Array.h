//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Array.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Data/IAllocator.h"
#include "../Utilities/StringUtil.h"

namespace Neko
{
    /// Dynamically allocated element array storage.
    template <typename T> class TArray
    {
    public:
        
        typedef T value_type;
        
        TArray()
        : Allocator(GetDefaultAllocator())
        {
            Data = nullptr;
            Capacity = 0;
            Size = 0;
        }
        
        /** Creates array with custom allocator. */
        explicit TArray(IAllocator& allocator)
        : Allocator(allocator)
        {
            Data = nullptr;
            Capacity = 0;
            Size = 0;
        }
        
        
        /** Copy constructor. */
        explicit TArray(const TArray& rhs)
        : Allocator(rhs.Allocator)
        {
            Data = nullptr;
            Capacity = 0;
            Size = 0;
            *this = rhs;
        }
        
        /** TArray data begin pointer. */
        NEKO_FORCE_INLINE T* begin() const
        {
            return Data;
        }
        
        /** Returns pointer to array end. */
        NEKO_FORCE_INLINE T* end() const
        {
            return Data + Size;
        }
        
        /** Swaps arrays. */
        void Swap(TArray<T>& rhs)
        {
            assert(&rhs.Allocator == &Allocator);
            
            int32 i = rhs.Capacity;
            rhs.Capacity = Capacity;
            Capacity = i;
            
            i = Size;
            Size = rhs.Size;
            rhs.Size = i;
            
            T* p = rhs.Data;
            rhs.Data = Data;
            Data = p;
        }
        
        /** Adds all items from the another array list to this one. */
        inline void Append(const TArray<T>& Other)
        {
            int32 OtherCount = Other.GetSize();
            
            if (OtherCount == 0)
            {
                return;
            }
            
            const int32 CurrentCount = Size;
            Resize(CurrentCount + OtherCount);
            memcpy(&Data[CurrentCount], Other.Data, sizeof(T) * OtherCount);
        }
        
        /** Removes duplicates. */
        void RemoveDuplicates()
        {
            for (int32 i = 0; i < Size - 1; ++i)
            {
                for (int32 j = i + 1; j < Size; ++j)
                {
                    if (Data[i] == Data[j])
                    {
                        EraseFast(j);
                        --j;
                    }
                }
            }
        }
        
        /** Removes duplicates using comparator function. */
        template<typename Comparator>
        void RemoveDuplicates(Comparator EqualsFunc)
        {
            for (int32 i = 0; i < Size - 1; ++i)
            {
                for (int32 j = i + 1; j < Size; ++j)
                {
                    if (EqualsFunc(Data[i], Data[j]))
                    {
                        EraseFast(j);
                        --j;
                    }
                }
            }
        }
        
        void operator = (const TArray& rhs)
        {
            if (this != &rhs)
            {
                CallDestructors(Data, Data + Size);
                Allocator.DeallocateAligned(Data);
                
                Data = (T*)Allocator.AllocateAligned(rhs.Capacity * sizeof(T), ALIGN_OF(T));
                Capacity = rhs.Capacity;
                Size = rhs.Size;
                
                for (int32 i = 0; i < Size; ++i)
                {
                    new (Neko::NewPlaceholder(), (char*)(Data + i)) T(rhs.Data[i]);
                }
            }
        }
        
        ~TArray()
        {
            CallDestructors(Data, Data + Size);
            Allocator.DeallocateAligned(Data);
        }
        
        /** Returns the index of element. */
        template <typename R>
        int32 IndexOf(R item) const
        {
            for (int32 i = 0; i < Size; ++i)
            {
                if (Data[i] == item)
                {
                    return i;
                }
            }
            return INDEX_NONE;
        }
        
        /** Returns the index of element. */
        int32 IndexOf(const T& item) const
        {
            for (int32 i = 0; i < Size; ++i)
            {
                if (Data[i] == item)
                {
                    return i;
                }
            }
            return INDEX_NONE;
        }
        
        NEKO_FORCE_INLINE bool Contains(const T& Item) const
        {
            return IndexOf(Item) != INDEX_NONE;
        }
        
        template <typename F>
        int32 Find(F predicate) const
        {
            for (int i = 0; i < Size; ++i)
            {
                if (predicate(Data[i]))
                {
                    return i;
                }
            }
            return -1;
        }
        
        void EraseItemFast(const T& item)
        {
            for (int32 i = 0; i < Size; ++i)
            {
                if (Data[i] == item)
                {
                    EraseFast(i);
                    return;
                }
            }
        }
        
        void EraseFast(int32 index)
        {
            if (index >= 0 && index < Size)
            {
                Data[index].~T();
                if (index != Size - 1)
                {
                    MoveMemory(Data + index, Data + Size - 1, sizeof(T));
                }
                --Size;
            }
        }
        
        void EraseItem(const T& item)
        {
            for (int32 i = 0; i < Size; ++i)
            {
                if (Data[i] == item)
                {
                    Erase(i);
                    return;
                }
            }
        }
		
        template <typename F>
        int32 EraseItems(F predicate)
        {
            int32 NumRemoved = 0;
            for (int i = Size - 1; i >= 0; --i)
            {
                if (predicate(Data[i]))
                {
                    Erase(i);
                    ++NumRemoved;
                }
            }
            return NumRemoved;
        }
        
        void Erase(int32 index)
        {
            if (index >= 0 && index < Size)
            {
                Data[index].~T();
                if (index < Size - 1)
                {
                    MoveMemory(Data + index, Data + index + 1, sizeof(T) * (Size - index - 1));
                }
                --Size;
            }
        }
        
        /** 
         * Sorts the array using the specified compare function pointer using qsort.
         *
         * @param pCompareFunc The compare function to use to sort elements. 
         */
        template <class CompareFunc>
        inline void Sort(CompareFunc pCompareFunc)
        {
            qsort(Data, Size, sizeof(T), pCompareFunc);
        }
        
        /** Inserts element at index. */
        void Insert(int32 index, const T& value)
        {
            if (Size == Capacity)
            {
                Grow();
            }
            MoveMemory(Data + index + 1, Data + index, sizeof(T) * (Size - index));
            new (NewPlaceholder(), &Data[index]) T(value);
            ++Size;
        }
        
        /** Initialises and adds element in the array end. */
        void Push(const T& value)
        {
            int32 size = Size;
            if (size == Capacity)
            {
                Grow();
            }
            new (NewPlaceholder(), (char*)(Data + size)) T(value);
            ++size;
            Size = size;
        }
        
        
        template <class _Ty> struct remove_reference
        { // remove rvalue reference
            typedef _Ty type;
        };
        
        template <class _Ty> struct remove_reference<_Ty&>
        { // remove rvalue reference
            typedef _Ty type;
        };
        
        template <class _Ty> struct remove_reference<_Ty&&>
        { // remove rvalue reference
            typedef _Ty type;
        };
        
        template <class _Ty> inline _Ty&& myforward(typename remove_reference<_Ty>::type& _Arg)
        {
            return (static_cast<_Ty&&>(_Arg));
        }
        
        /** Inserts the new object and calls its constructor with the given parameters. */
        template <typename... Params> T& Emplace(Params&&... params)
        {
            if (Size == Capacity)
            {
                Grow();
            }
            
            new (NewPlaceholder(), (char*)(Data + Size)) T(myforward<Params>(params)...);
            ++Size;
            
            return Data[Size - 1];
        }
        
        
        /** Inserts the new object at the index and calls its constructor with the given parameters. */
        template <typename... Params> T& EmplaceAt(int32 idx, Params&&... params)
        {
            if (Size == Capacity)
            {
                Grow();
            }
            for (int32 i = Size - 1; i >= idx; --i)
            {
                CopyMemory(&Data[i + 1], &Data[i], sizeof(Data[i]));
            }
            
            new (NewPlaceholder(), (char*)(Data + idx)) T(myforward<Params>(params)...);
            ++Size;
            
            return Data[idx];
        }
        
        
        NEKO_FORCE_INLINE bool IsEmpty() const
        {
            return Size == 0;
        }
        
        /** Destructs every element and resets its size. */
        NEKO_FORCE_INLINE void Clear()
        {
            CallDestructors(Data, Data + Size);
            Size = 0;
        }

		NEKO_FORCE_INLINE void Empty()
		{
			Size = 0;
		}
        
        NEKO_FORCE_INLINE const T& back() const
        {
            return Data[Size - 1];
        }
        
        NEKO_FORCE_INLINE T& back()
        {
            return Data[Size - 1];
        }
        
        
        /** Destructs the last element. */
        NEKO_FORCE_INLINE void Pop()
        {
            if (Size > 0)
            {
                Data[Size - 1].~T();
                --Size;
            }
        }
        
        /** Resizes the array. */
        void Resize(int32 size)
        {
            if (size > Capacity)
            {
                Reserve(size);
            }
            
            for (int32 i = Size; i < size; ++i)
            {
                new (NewPlaceholder(), (char*)(Data + i)) T();
            }
            
            CallDestructors(Data + size, Data + Size);
            Size = size;
        }
        
        /** Allocates more array storage space if required. */
        void Reserve(int32 NewCapacity)
        {
            if (NewCapacity > Capacity)
            {
                T* newData = (T*)Allocator.AllocateAligned(NewCapacity * sizeof(T), ALIGN_OF(T));
                CopyMemory(newData, Data, sizeof(T) * Size);
                
                Allocator.DeallocateAligned(Data);
                Data = newData;
                Capacity = NewCapacity;
            }
        }
        
        
        NEKO_FORCE_INLINE const T& operator[](int32 index) const
        {
            assert(index >= 0 && index < Size);
            return Data[index];
        }
        
        NEKO_FORCE_INLINE T& operator[](int32 index)
        {
            assert(index >= 0 && index < Size);
            return Data[index];
        }
        
        /** Amount of elements in array. */
        NEKO_FORCE_INLINE int32 GetSize() const
        {
            return Size;
        }
        /** Total array capacity. */
        NEKO_FORCE_INLINE int32 GetCapacity() const
        {
            return Capacity;
        }
        
        
        /**
         * Removes element or elements if 'Count' is set. 
         *
         * @param Index Which element to remove.
         * @param Count Number of elements to remove.
         * @param bAllowShrinking   Shrinks array if needed.
         */
        void RemoveAt(int32 Index, int32 Count, bool bAllowShrinking = true)
        {
            if (Count)
            {
                assert((Count >= 0) & (Index >= 0) & (Index + Count <= Size));
                
                CallDestructors(Data + Index, Data + Index + Count);
                
                // Skip memmove in the common case that there is nothing to move.
                int32 NumToMove = Size - Index - Count;
                if (NumToMove)
                {
                    memmove(Data + Index, Data + Index + Count, NumToMove * sizeof(T));
                }
                Size -= Count;
                
                if (bAllowShrinking)
                {
                    Resize(Size);
                }
            }
        }
        
    private:
        
        void Grow()
        {
            int32 NewCapacity = (Capacity) == 0 ? 4 : Capacity * 2;
            Data = (T*)Allocator.ReallocateAligned(Data, NewCapacity * sizeof(T), ALIGN_OF(T));
            Capacity = NewCapacity;
        }
        
        void CallDestructors(T* begin, T* end)
        {
            for (; begin < end; ++begin)
            {
                begin->~T();
            }
        }
        
    private:
        
        IAllocator& Allocator;
        
        int32   Capacity;
        int32   Size;
        
        T*  Data;
    };
} // namespace Neko
