//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  DeltaBitMessage.cpp
//  Neko engine

#include "DeltaBitMessage.h"

namespace Neko
{
    const int MAX_DATA_BUFFER = 1024;
    
    void CBitMessageDelta::WriteBits(int value, int numBits)
    {
        if (newBase)
        {
            newBase->WriteBits(value, numBits);
        }
        
        if (!base)
        {
            writeDelta->WriteBits(value, numBits);
            changed = true;
        }
        else
        {
            int baseValue = base->ReadBits(numBits);
            if (baseValue == value)
            {
                writeDelta->WriteBits(0, 1);
            }
            else
            {
                writeDelta->WriteBits(1, 1);
                writeDelta->WriteBits(value, numBits);
                changed = true;
            }
        }
    }
    
    void CBitMessageDelta::WriteDelta(int oldValue, int newValue, int numBits)
    {
        if (newBase)
        {
            newBase->WriteBits(newValue, numBits);
        }
        
        if (!base)
        {
            if (oldValue == newValue)
            {
                writeDelta->WriteBits(0, 1);
            }
            else
            {
                writeDelta->WriteBits(1, 1);
                writeDelta->WriteBits(newValue, numBits);
            }
            changed = true;
        }
        else
        {
            int baseValue = base->ReadBits(numBits);
            if (baseValue == newValue)
            {
                writeDelta->WriteBits(0, 1);
            }
            else
            {
                writeDelta->WriteBits(1, 1);
                if (oldValue == newValue)
                {
                    writeDelta->WriteBits(0, 1);
                    changed = true;
                }
                else
                {
                    writeDelta->WriteBits(1, 1);
                    writeDelta->WriteBits(newValue, numBits);
                    changed = true;
                }
            }
        }
    }
    
    int CBitMessageDelta::ReadBits(int numBits) const
    {
        int value;
        
        if (!base)
        {
            value = readDelta->ReadBits(numBits);
            changed = true;
        }
        else
        {
            int baseValue = base->ReadBits(numBits);
            if (!readDelta || readDelta->ReadBits(1) == 0)
            {
                value = baseValue;
            }
            else
            {
                value = readDelta->ReadBits(numBits);
                changed = true;
            }
        }
        
        if (newBase)
        {
            newBase->WriteBits(value, numBits);
        }
        return value;
    }
    
    int CBitMessageDelta::ReadDelta(int oldValue, int numBits) const
    {
        int value;
        
        if (!base)
        {
            if (readDelta->ReadBits(1) == 0)
            {
                value = oldValue;
            }
            else
            {
                value = readDelta->ReadBits(numBits);
            }
            changed = true;
        }
        else
        {
            int baseValue = base->ReadBits(numBits);
            if (!readDelta || readDelta->ReadBits(1) == 0)
            {
                value = baseValue;
            }
            else if (readDelta->ReadBits(1) == 0)
            {
                value = oldValue;
                changed = true;
            }
            else
            {
                value = readDelta->ReadBits(numBits);
                changed = true;
            }
        }
        
        if (newBase)
        {
            newBase->WriteBits(value, numBits);
        }
        return value;
    }
    
    void CBitMessageDelta::WriteString(const char* s, int maxLength)
    {
        if (newBase)
        {
            newBase->WriteString(s, maxLength);
        }
        
        if (!base)
        {
            writeDelta->WriteString(s, maxLength);
            changed = true;
        }
        else
        {
            char baseString[MAX_DATA_BUFFER];
            base->ReadString(baseString, sizeof(baseString));
            if (Neko::EqualStrings(s, baseString) == 0)
            {
                writeDelta->WriteBits(0, 1);
            }
            else
            {
                writeDelta->WriteBits(1, 1);
                writeDelta->WriteString(s, maxLength);
                changed = true;
            }
        }
    }
    
    void CBitMessageDelta::WriteData(const void* data, int length)
    {
        if (newBase)
        {
            newBase->WriteData(data, length);
        }
        
        if (!base)
        {
            writeDelta->WriteData(data, length);
            changed = true;
        }
        else
        {
            Byte baseData[MAX_DATA_BUFFER];
            assert(length < sizeof(baseData));
            base->ReadData(baseData, length);
            if (memcmp(data, baseData, length) == 0)
            {
                writeDelta->WriteBits(0, 1);
            }
            else
            {
                writeDelta->WriteBits(1, 1);
                writeDelta->WriteData(data, length);
                changed = true;
            }
        }
    }
    
    void CBitMessageDelta::WriteDeltaLongCounter(int oldValue, int newValue)
    {
        if (newBase)
        {
            newBase->WriteBits(newValue, 32);
        }
        
        if (!base)
        {
            writeDelta->WriteDeltaLongCounter(oldValue, newValue);
            changed = true;
        }
        else
        {
            int baseValue = base->ReadBits(32);
            if (baseValue == newValue)
            {
                writeDelta->WriteBits(0, 1);
            }
            else
            {
                writeDelta->WriteBits(1, 1);
                writeDelta->WriteDeltaLongCounter(oldValue, newValue);
                changed = true;
            }
        }
    }
    
    void CBitMessageDelta::ReadString(char* buffer, int bufferSize) const
    {
        if (!base)
        {
            readDelta->ReadString(buffer, bufferSize);
            changed = true;
        }
        else
        {
            char baseString[MAX_DATA_BUFFER];
            base->ReadString(baseString, sizeof(baseString));
            if (!readDelta || readDelta->ReadBits(1) == 0)
            {
                Neko::CopyString(baseString, bufferSize, buffer);
            }
            else
            {
                readDelta->ReadString(buffer, bufferSize);
                changed = true;
            }
        }
        
        if (newBase)
        {
            newBase->WriteString(buffer);
        }
    }
    
    void CBitMessageDelta::ReadData(void* data, int length) const
    {
        if (!base)
        {
            readDelta->ReadData(data, length);
            changed = true;
        }
        else
        {
            char baseData[MAX_DATA_BUFFER];
            assert(length < sizeof(baseData));
            base->ReadData(baseData, length);
            if (!readDelta || readDelta->ReadBits(1) == 0)
            {
                memcpy(data, baseData, length);
            }
            else
            {
                readDelta->ReadData(data, length);
                changed = true;
            }
        }
        
        if (newBase)
        {
            newBase->WriteData(data, length);
        }
    }
    
    int CBitMessageDelta::ReadDeltaLongCounter(int oldValue) const
    {
        int value;
        
        if (!base)
        {
            value = readDelta->ReadDeltaLongCounter(oldValue);
            changed = true;
        }
        else
        {
            int baseValue = base->ReadBits(32);
            if (!readDelta || readDelta->ReadBits(1) == 0)
            {
                value = baseValue;
            }
            else
            {
                value = readDelta->ReadDeltaLongCounter(oldValue);
                changed = true;
            }
        }
        
        if (newBase)
        {
            newBase->WriteBits(value, 32);
        }
        return value;
    }
}
