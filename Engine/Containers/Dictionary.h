//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Dictionary.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "AssociativeArray.h"
#include "Array.h"
#include "../Neko.h"
#include "Text.h"
#include "../Math/Vec.h"

namespace Neko
{
    // @todo Hashed keys
    
    /// TArray based dictionary.
    class NEKO_ENGINE_API CDictionary
    {
    public:
        
        CDictionary()
        : CDictionary(GetDefaultAllocator())
        {
        }
        
        CDictionary(IAllocator& allocator);
        
        ~CDictionary();
        
        /** Returns the amount of key-value pairs. */
        NEKO_FORCE_INLINE int32 GetNumKeyVals() const
        {
            return Storage.GetSize();
        }
        
        /** Deletes the key-value pair in hashmap. */
        void Delete(const char* Key);
        
        /** Shows the dictionary values. */
        void Print();
        
        void Clear();
        
        /** Clears existing key/value pairs and transfer key/value pairs from other. */
        void TransferKeyValues(CDictionary& Other);
        
        /** Copies the values IF NOT found in this storage. */
        void SetDefaults(const CDictionary& Other);
        
        /** Sets the string value. */
        void Set(const char* Key, const char* Value);
        /** Sets the float value. */
        void SetFloat(const char* Key, const float Value);
        /** Sets the integer value. */
        void SetInt(const char* Key, const int Value);
        /** Sets the boolean value. */
        void SetBool(const char* Key, const bool Value);
        
        
        void SetVec2(const char* Key, const Vec2& Value);
        void SetVec3(const char* Key, const Vec3& Value);
        void SetVec4(const char* Key, const Vec4& Value);
        
        const char* GetString(const char* Key, const char* DefaultValue = "") const;
        float GetFloat(const char* Key, const char* DefaultValue = "0") const;
        int GetInt(const char* Key, const char* DefaultValue = "0") const;
        bool GetBool(const char* Key, const char* DefaultValue = "0") const;
        
        Vec2 GetVec2(const char* Key, const char* DefaultValue = nullptr) const;
        Vec3 GetVec3(const char* Key, const char* DefaultValue = nullptr) const;
        Vec4 GetVec4(const char* Key, const char* DefaultValue = nullptr) const;
        
        
        /** All these methods return TRUE if key is found, FALSE otherwise. */
        bool GetString(const char* Key, const char* DefaultValue, const char** Out) const;
        bool GetString(const char* Key, const char* DefaultValue, Text& Out) const;
        bool GetFloat(const char* Key, const char* DefaultValue, float& Out) const;
        bool GetInt(const char* Key, const char* DefaultValue, int& Out) const;
        bool GetBool(const char* Key, const char* DefaultValue, bool& Out) const;
        
        bool GetVec2(const char* Key, const char* DefaultValue, Vec2& Out) const;
        bool GetVec3(const char* Key, const char* DefaultValue, Vec3& Out) const;
        bool GetVec4(const char* Key, const char* DefaultValue, Vec4& Out) const;
        
        /** Key-value pair. */
        struct KeyValue
        {
            typedef StaticString<32> StrType;
            
            NEKO_FORCE_INLINE const StrType& GetKey() const
            {
                return Key;
            }
            
            NEKO_FORCE_INLINE const StrType& GetValue() const
            {
                return Value;
            }
            

            StrType Key;
            int32 KeyHash;
            StrType Value;
        };
        
        /** Returns the key index. -1 otherwise if not found. */
        int32 FindKeyIndex(const char* Key);
        
        const KeyValue* GetKeyValue(const int32 Index) const;
        const KeyValue* FindKey(const char* Key) const;
        
        TArray<KeyValue> Storage; // Keys and Values are stored as a string type
    };
    
}
