
//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  DelegateList.h
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "Array.h"
#include "Delegate.h"

namespace Neko
{
    template <typename T> class DelegateList;
    
    template <typename R, typename... Args> class DelegateList<R(Args...)>
    {
    public:
        
        DelegateList(IAllocator& allocator)
        : Delegates(allocator)
        { }
        
        template <typename C, R (C::*Function)(Args...)> void Bind(C* instance)
        {
            TDelegate<R(Args...)> cb;
            cb.template Bind<C, Function>(instance);
            Delegates.Push(cb);
        }
        
        template <R (*Function)(Args...)> void Bind()
        {
            TDelegate<R(Args...)> cb;
            cb.template Bind<Function>();
            Delegates.Push(cb);
        }
        
        template <typename C, R (C::*Function)(Args...)> void Unbind(C* instance)
        {
            TDelegate<R(Args...)> cb;
            cb.template Bind<C, Function>(instance);
            for (int32 i = 0; i < Delegates.GetSize(); ++i)
            {
                if (Delegates[i] == cb)
                {
                    Delegates.EraseFast(i);
                    break;
                }
            }
        }
        
        void Invoke(Args... args)
        {
            for (auto& i : Delegates)
            {
                i.Invoke(args...);
            }
        }
        
        void Clear()
        {
            Delegates.Clear();
        }
        
        /** Number of delegates. */
        NEKO_FORCE_INLINE uint32 GetNum() const
        {
            return Delegates.GetSize();
        }
        
    private:
        
        TArray<TDelegate<R(Args...)>> Delegates;
    };
} // namespace Neko
