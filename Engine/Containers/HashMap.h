//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  HashMap.h
//  Neko engine

#pragma once

#include "../Data/IAllocator.h"
#include "../Math/MathUtils.h"
#include "../Neko.h"
#include "../Utilities/StringUtil.h"
#include "../Utilities/Text.h"

namespace Neko
{
    template <class K, class V>
    struct HashNode
    {
        typedef HashNode<K, V> my_node;
        
        HashNode(const K& key, const V& value)
        : Key(key)
        , Value(value)
        , Next(nullptr)
        { }
        
        explicit HashNode(const my_node& src)
        : Key(src.Key)
        , Value(src.Value)
        , Next(src.Next)
        { }
        
        K Key;
        V Value;
        my_node* Next;
    };
    
    template <class Key>
    struct HashFunc
    {
        static uint32 Get(const Key& key);
    };
    
    // https://gist.github.com/badboy/6267743
    template <>
    struct HashFunc<uint64>
    {
        static uint32 Get(const uint64& key)
        {
            uint64 tmp = (~key) + (key << 18);
            tmp = tmp ^ (tmp >> 31);
            tmp = tmp * 21;
            tmp = tmp ^ (tmp >> 11);
            tmp = tmp + (tmp << 6);
            tmp = tmp ^ (tmp >> 22);
            return (uint32)tmp;
        }
    };
    
    template <>
    struct HashFunc<int32>
    {
        static uint32 Get(const int32& key)
        {
            uint32 x = ((key >> 16) ^ key) * 0x45d9f3b;
            x = ((x >> 16) ^ x) * 0x45d9f3b;
            x = ((x >> 16) ^ x);
            return x;
        }
    };
    
    template <>
    struct HashFunc<ComponentType>
    {
        static uint32 Get(const ComponentType& key)
        {
            static_assert(sizeof(int32) == sizeof(key.index), "Check this");
            return HashFunc<int32>::Get(key.index);
        }
    };
    
    template <>
    struct HashFunc<ComponentHandle>
    {
        static uint32 Get(const ComponentHandle& key)
        {
            static_assert(sizeof(int32) == sizeof(key.index), "Check this");
            return HashFunc<int32>::Get(key.index);
        }
    };
    
    template <>
    struct HashFunc<Entity>
    {
        static uint32 Get(const Entity& key)
        {
            static_assert(sizeof(int32) == sizeof(key.index), "Check this");
            return HashFunc<int32>::Get(key.index);
        }
    };
    
    template <>
    struct HashFunc<uint32>
    {
        static uint32 Get(const uint32& key)
        {
            uint32 x = ((key >> 16) ^ key) * 0x45d9f3b;
            x = ((x >> 16) ^ x) * 0x45d9f3b;
            x = ((x >> 16) ^ x);
            return x;
        }
    };
    
    template <>
    struct HashFunc<void*>
    {
        static uint32 Get(const void* key)
        {
#ifdef NEKO_64BIT
            uint64 tmp = (uint64)key;
            tmp = (~tmp) + (tmp << 18);
            tmp = tmp ^ (tmp >> 31);
            tmp = tmp * 21;
            tmp = tmp ^ (tmp >> 11);
            tmp = tmp + (tmp << 6);
            tmp = tmp ^ (tmp >> 22);
            return (uint32)tmp;
#else
            size_t x = ((int32(key) >> 16) ^ int32(key)) * 0x45d9f3b;
            x = ((x >> 16) ^ x) * 0x45d9f3b;
            x = ((x >> 16) ^ x);
            return (uint32)x;
#endif
        }
    };
    
    template <>
    struct HashFunc<char*>
    {
        static uint32 Get(const char* key)
        {
            uint32 result = 0x55555555;
            
            while (*key)
            {
                result ^= *key++;
                result = ((result << 5) | (result >> 27));
            }
            
            return result;
        }
    };
    
    template <>
    struct HashFunc<const char*>
    {
        static uint32 Get(const char* key)
        {
            uint32 result = 0x55555555;
            
            while (*key)
            {
                result ^= *key++;
                result = ((result << 5) | (result >> 27));
            }
            
            return result;
        }
    };
    
    template <>
    struct HashFunc<Text>
    {
        static uint32 Get(const Text& Data)
        {
            const char* key = Data.c_str();
            uint32 result = 0x55555555;
            
            while (*key)
            {
                result ^= *key++;
                result = ((result << 5) | (result >> 27));
            }
            
            return result;
        }
    };
    
    /// Implementation for hashed map set.
    template <class K, class T, class Hasher = HashFunc<K>>
    class THashMap
    {
    public:
        
        typedef T value_type;
        typedef K key_type;
        typedef Hasher hasher_type;
        typedef THashMap<key_type, value_type, hasher_type> my_type;
        typedef HashNode<key_type, value_type> node_type;
        typedef uint32 size_type;
        
        friend class HashMapIterator;
        friend class ConstHashMapIterator;
        
        static const size_type InitialIdsCount = 8;
        
        template <class U, class S, class _Hasher>
        class HashMapIterator
        {
        public:
            
            typedef U key_type;
            typedef S value_type;
            typedef _Hasher hasher_type;
            typedef HashNode<key_type, value_type> node_type;
            typedef THashMap<key_type, value_type, hasher_type> hm_type;
            typedef HashMapIterator<key_type, value_type, hasher_type> my_type;
            
            friend hm_type;
            
            HashMapIterator()
            : pHashMap(nullptr)
            , pCurrentNode(nullptr)
            { }
            
            HashMapIterator(const my_type& src)
            : pHashMap(src.pHashMap)
            , pCurrentNode(src.pCurrentNode)
            { }
            
            HashMapIterator(node_type* node, hm_type* hm)
            : pHashMap(hm)
            , pCurrentNode(node)
            { }
            
            ~HashMapIterator()
            { }
            
            bool IsValid() const { return nullptr != pCurrentNode && pHashMap->Sentinel != pCurrentNode; }
            
            key_type& key() { return pCurrentNode->Key; }
            
            value_type& value() { return pCurrentNode->Value; }
            
            value_type& operator*() { return value(); }
            
            my_type& operator++() { return preInc(); }
            
            my_type operator++(int) { return postInc(); }
            
            bool operator==(const my_type& it) const { return it.pCurrentNode == pCurrentNode; }
            
            bool operator!=(const my_type& it) const { return it.pCurrentNode != pCurrentNode; }
            
        private:
            
            my_type& preInc()
            {
                pCurrentNode = pHashMap->next(pCurrentNode);
                return *this;
            }
            
            my_type postInc()
            {
                my_type p = *this;
                pCurrentNode = pHashMap->next(pCurrentNode);
                return p;
            }
            
            hm_type* pHashMap;
            node_type* pCurrentNode;
        };
        
        template <class U, class S, class _Hasher>
        class ConstHashMapIterator
        {
        public:
            
            typedef U key_type;
            typedef S value_type;
            typedef _Hasher hasher_type;
            typedef HashNode<key_type, value_type> node_type;
            typedef THashMap<key_type, value_type, hasher_type> hm_type;
            typedef ConstHashMapIterator<key_type, value_type, hasher_type> my_type;
            
            friend hm_type;
            
            ConstHashMapIterator()
            : pHashMap(nullptr)
            , pCurrentNode(nullptr)
            { }
            
            ConstHashMapIterator(const my_type& src)
            : pHashMap(src.pHashMap)
            , pCurrentNode(src.pCurrentNode)
            { }
            
            ConstHashMapIterator(node_type* node, const hm_type* hm)
            : pHashMap(hm)
            , pCurrentNode(node)
            { }
            
            ~ConstHashMapIterator()
            { }
            
            bool IsValid() const { return nullptr != pCurrentNode && pHashMap->Sentinel != pCurrentNode; }
            
            const key_type& key() const { return pCurrentNode->Key; }
            
            const value_type& value() const { return pCurrentNode->Value; }
            
            const value_type& operator*() const { return value(); }
            
            my_type& operator++() { return preInc(); }
            
            my_type operator++(int) { return postInc(); }
            
            bool operator==(const my_type& it) const { return it.pCurrentNode == pCurrentNode; }
            
            bool operator!=(const my_type& it) const { return it.pCurrentNode != pCurrentNode; }
            
        private:
            
            my_type& preInc()
            {
                pCurrentNode = pHashMap->next(pCurrentNode);
                return *this;
            }
            
            my_type postInc()
            {
                my_type p = *this;
                pCurrentNode = pHashMap->next(pCurrentNode);
                return p;
            }
            
            const hm_type* pHashMap;
            node_type* pCurrentNode;
        };
        
        typedef HashMapIterator<key_type, value_type, hasher_type> iterator;
        typedef ConstHashMapIterator<key_type, value_type, hasher_type> constIterator;
        
        THashMap()
        : Allocator(GetDefaultAllocator())
        {
            InitSentinel();
            init();
        }
        
        explicit THashMap(IAllocator& allocator)
        : Allocator(allocator)
        {
            InitSentinel();
            init();
        }
        
        THashMap(size_type buckets, IAllocator& allocator)
        : Allocator(allocator)
        {
            InitSentinel();
            init(buckets);
        }
        
        explicit THashMap(const my_type& src)
        : Allocator(src.Allocator)
        {
            InitSentinel();
            init(src.MaxId);
            CopyTableUninitialized(src.Table, src.Sentinel, src.MaxId);
            
            Mask = src.Mask;
            Size = src.Size;
        }
        
        ~THashMap()
        {
            for (node_type* n = first(); Sentinel != n;)
            {
                node_type* dest = n;
                n = next(n);
                Destruct(dest);
                if (dest < Table || dest > &Table[MaxId - 1])
                {
                    Allocator.Deallocate(dest);
                }
            }
            
            Allocator.Deallocate(Table);
            Table = nullptr;
            Size = 0;
            MaxId = 0;
            Mask = 0;
        }
        
        size_type GetSize() const { return Size; }
        
        bool IsEmpty() const { return 0 == Size; }
        
        float loadFactor() const { return float(Size / MaxId); }
        float maxLoadFactor() const { return 0.75f; }
        
        my_type& operator=(const my_type& src)
        {
            if (this != &src)
            {
                Clear();
                init(src.MaxId);
                CopyTableUninitialized(src.Table, src.Sentinel, src.MaxId);
                
                Mask = src.Mask;
                Size = src.Size;
            }
            
            return *this;
        }
        
        value_type& operator[](const key_type& key) const
        {
            node_type* n = _find(key);
            assert(Sentinel != n);
            return n->Value;
        }
        
        void Insert(const key_type& key, const value_type& val)
        {
            size_type pos = GetPosition(key);
            Construct(GetEmptyNode(pos), key, val);
            Size++;
            CheckSize();
        }
        
        iterator InsertEx(const key_type& key, const value_type& val)
        {
            size_type pos = GetPosition(key);
            node_type* node = GetEmptyNode(pos);
            Construct(node, key, val);
            Size++;
            CheckSize();
            return iterator(node, this);
        }
        
        inline bool Get(const key_type& key, value_type& value) const
        {
            size_type pos = GetPosition(key);
            
            assert(pos < MaxId);
            node_type* node = &Table[pos];
            while (node && Sentinel != node->Next)
            {
                if (node->Key == key)
                {
                    value = node->Value;
                    return true;
                }
                node = node->Next;
            }
            return false;
        }
      
        value_type FindValue(const key_type& key) const
        {
            size_type pos = GetPosition(key);
            for (node_type* n = &Table[pos]; n != nullptr && n->Next != Sentinel; n = n->Next)
            {
                if (n->Key == key)
                {
                    return n->Value;
                }
            }
            
            return nullptr;
        }
        
        iterator Erase(iterator it)
        {
            assert(it.IsValid());
            
            size_type idx = GetPosition(it.pCurrentNode->Key);
            node_type* n = &Table[idx];
            node_type* prev = nullptr;
            node_type* next_it = nullptr;
            
            while (n != nullptr && n->Next != Sentinel)
            {
                if (n == it.pCurrentNode)
                {
                    next_it = next(n);
                    DeleteNode(n, prev);
                    
                    --Size;
                    return iterator(next_it, this);
                }
                
                prev = n;
                n = n->Next;
            }
            
            return iterator(Sentinel, this);
        }
        
        size_type Erase(const key_type& key)
        {
            size_type count = 0;
            size_type idx = GetPosition(key);
            node_type* n = &Table[idx];
            node_type* prev = nullptr;
            
            while (n != nullptr && n->Next != Sentinel)
            {
                if (key == n->Key)
                {
                    DeleteNode(n, prev);
                    
                    count++;
                    --Size;
                }
                else
                {
                    prev = n;
                    n = n->Next;
                }
            }
            
            return count;
        }
        
        void Clear()
        {
            for (node_type* n = first(); n != Sentinel;)
            {
                node_type* dest = n;
                n = next(n);
                Destruct(dest);
                if (dest < Table || dest > &Table[MaxId - 1])
                {
                    Allocator.Deallocate(dest);
                }
            }
            
            Allocator.Deallocate(Table);
            Table = nullptr;
            Size = 0;
            MaxId = 0;
            Mask = 0;
            init();
        }
        
        void rehash(size_type idsCount)
        {
            if (MaxId < idsCount)
            {
                grow(idsCount);
            }
        }
        
        iterator begin()
        {
            return iterator(first(), this);
        }
        
        iterator end()
        {
            return iterator(Sentinel, this);
        }
        
        constIterator begin() const
        {
            return constIterator(first(), this);
        }
        
        constIterator end() const
        {
            return constIterator(Sentinel, this);
        }
        
        iterator Find(const key_type& key)
        {
            return iterator(_find(key), this);
        }
        
        constIterator Find(const key_type& key) const
        {
            return constIterator(_find(key), this);
        }
        
        value_type& at(const key_type& key)
        {
            node_type* n = _find(key);
            return n->Value;
        }
        
        NEKO_FORCE_INLINE int GenerateKey(const char* string, bool caseSensitive) const
        {
            if (caseSensitive)
            {
                return (Neko::Hash(string) & Mask);
            }
            else
            {
                return (Neko::IHash(string) & Mask);
            }
        }
        
    private:
        
        void CheckSize()
        {
            if (loadFactor() > maxLoadFactor())
            {
                grow(MaxId);
            }
        }
        
        size_type GetPosition(const key_type& key) const
        {
            size_type pos = Hasher::Get(key) & Mask;
            assert(pos < MaxId);
            return pos;
        }
        
        void init(size_type idsCount = InitialIdsCount)
        {
            assert(Math::IsPowOfTwo(idsCount));
            Table = (node_type*)Allocator.Allocate(sizeof(node_type) * idsCount);
            for (node_type* i = Table; i < &Table[idsCount]; i++)
            {
                Construct(i, Sentinel);
            }
            Mask = (idsCount - 1);
            MaxId = idsCount;
            Size = 0;
        }
        
        void copyTable(node_type* src, const node_type* srcSentinel, size_type idsCount)
        {
            for (size_type i = 0; i < idsCount; i++)
            {
                node_type* n = &src[i];
                while (n != nullptr && n->Next != srcSentinel)
                {
                    int32 pos = GetPosition(n->Key);
                    node_type* newNode = GetEmptyNode(pos);
                    CopyMemory(newNode, n, sizeof(node_type));
                    newNode->Next = nullptr;
                    n = n->Next;
                }
            }
        }
        
        void grow(size_type idsCount)
        {
            size_type oldSize = Size;
            size_type oldIdsCount = MaxId;
            size_type pow2IdsCount = Math::NextPow2(idsCount);
            size_type newIdsCount = pow2IdsCount < 512 ? pow2IdsCount * 4 : pow2IdsCount * 2;
            node_type* old = Table;
            
            init(newIdsCount);
            copyTable(old, Sentinel, oldIdsCount);
            
            Size = oldSize;
            for (size_type i = 0; i < oldIdsCount; ++i)
            {
                node_type* n = &old[i];
                if (n->Next && n->Next != Sentinel)
                {
                    n = n->Next;
                    while (n && n->Next != Sentinel)
                    {
                        node_type* old_n = n;
                        n = n->Next;
                        Allocator.Deallocate(old_n);
                    }
                }
            }
            Allocator.Deallocate(old);
        }
        
        node_type* Construct(node_type* where, const key_type& key, const value_type& val)
        {
            return new (NewPlaceholder(), where) node_type(key, val);
        }
        
        node_type* Construct(node_type* where, node_type* node)
        {
            where->Next = node;
            return where;
        }
        
        node_type* Construct(node_type* where, const node_type& node)
        {
            return new (NewPlaceholder(), where) node_type(node);
        }
        
        void Destruct(node_type* n)
        {
            (void)n; /// to avoid a bug in VS 2013 - n is unused warning
            n->~node_type();
        }
        
        void InitSentinel()
        {
            Sentinel = reinterpret_cast<node_type*>(SentinelMem);
            Sentinel->Next = Sentinel;
        }
        
        void CopyUninitialized(node_type* src, node_type* dst)
        {
            Construct(dst, src->Key, src->Value);
        }
        
        node_type* GetEmptyNode(size_type pos)
        {
            node_type* node = &Table[pos];
            while (Sentinel != node->Next && nullptr != node->Next)
            {
                node = node->Next;
            }
            
            node->Next = (nullptr == node->Next
                          ? Construct(reinterpret_cast<node_type*>(Allocator.Allocate(sizeof(node_type))), Sentinel)
                          : node);
            return node->Next;
        }
        
        node_type* first() const
        {
            if (Size == 0)
            {
                return Sentinel;
            }
            
            for (size_type i = 0; i < MaxId; i++)
            {
                if (Table[i].Next != Sentinel)
                {
                    return &Table[i];
                }
            }
            
            return Sentinel;
        }
        
        node_type* next(node_type* n) const
        {
            if (0 == Size || Sentinel == n)
            {
                return Sentinel;
            }
            
            node_type* next = n->Next;
            if ((next == nullptr || next == Sentinel))
            {
                size_type idx = GetPosition(n->Key) + 1;
                for (size_type i = idx; i < MaxId; i++)
                {
                    if (Table[i].Next != Sentinel)
                    {
                        return &Table[i];
                    }
                }
                return Sentinel;
            }
            else
            {
                return next;
            }
        }
        
        node_type* _find(const key_type& key) const
        {
            size_type pos = GetPosition(key);
            for (node_type* n = &Table[pos]; n != nullptr && n->Next != Sentinel; n = n->Next)
            {
                if (n->Key == key)
                {
                    return n;
                }
            }
            
            return Sentinel;
        }
        
        void DeleteNode(node_type*& n, node_type* prev)
        {
            if (prev == nullptr)
            {
                node_type* next = n->Next;
                Destruct(n);
                Construct(n, next ? *next : *Sentinel);
                if (next)
                {
                    Destruct(next);
                }
                Allocator.Deallocate(next);
            }
            else
            {
                prev->Next = n->Next;
                Destruct(n);
                Allocator.Deallocate(n);
                n = prev->Next;
            }
        }
        
        void CopyTableUninitialized(node_type* src, const node_type* srcSentinel, size_type idsCount)
        {
            for (size_type i = 0; i < idsCount; i++)
            {
                node_type* n = &src[i];
                while (n != nullptr && n->Next != srcSentinel)
                {
                    size_type pos = GetPosition(n->Key);
                    node_type* newNode = GetEmptyNode(pos);
                    CopyUninitialized(n, newNode);
                    newNode->Next = nullptr;
                    n = n->Next;
                }
            }
        }

        node_type* Table;
        char SentinelMem[sizeof(node_type)];
        node_type* Sentinel;
        size_type Size;
        size_type Mask;
        size_type MaxId;
        IAllocator& Allocator;
    };
}
