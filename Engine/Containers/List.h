//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  List.h
//  Neko engine
//
//  Created by Neko Code.
//  Copyright (c) 2015 Neko Vision. All rights reserved.
//

#pragma once

namespace Neko
{
    template <class Type>
    class CLinkList
    {
    public:
        
        CLinkList()
        {
            Owner = nullptr;
            Head = this;
            Next = this;
            Prev = this;
        }
        
        ~CLinkList()
        {
            Clear();
        }
        
        /** Adds node at the end of the list. */
        void AddToTail(CLinkList& Node)
        {
            InsertBefore(*Node.Head);
        }
        
        /** Adds node at the beginning of the list. */
        void AddToHead(CLinkList& Node)
        {
            InsertAfter(*Node.Head);
        }
        
        /** Places the node after the existing node. */
        void InsertAfter(CLinkList& Node)
        {
            Prev = &Node;
            Next = Node.Next;
            Node.Next = this;
            Next->Prev = this;
            Head = Node.Head;
        }
        
        /** Places the node before the existing node. */
        void InsertBefore(CLinkList& Node)
        {
            Next = &Node;
            Prev = Node.Prev;
            Node.Prev = this;
            Prev->Next = this;
            Head = Node.Head;
        }
        
        /** Returns TRUE if the list is empty. */
        bool IsListEmpty() const
        {
            return Head->Next == Head;
        }
        
        /** Returns TRUE if current node is in a list. */
        bool InList() const
        {
            return Head != this;
        }
        
        /** Returns the amount of items in this list. */
        int32 Num() const
        {
            CLinkList<Type>* node;
            int32 num;
            
            num = 0;
            for (node = Head->Next; node != Head; node = node->Next)
            {
                num++;
            }
            
            return num;
        }
        
        /** Removes the node from the list. */
        void Clear()
        {
            if (Head == this)
            {
                while (Next != this)
                {
                    Next->Remove();
                }
            }
            else
            {
                Remove();
            }
        }
        
        /** Removes this node from list. */
        void Remove()
        {
            // keep the chain
            Prev->Next = Next;
            Next->Prev = Prev;
            
            Next = nullptr;
            Prev = nullptr;
            Head = nullptr;
        }
        
        /** Returns the head of the list. */
        CLinkList<Type>* GetListHead() const
        {
            return Head;
        }
        
        /** Returns the next object in the list. */
        Type* GetNext() const
        {
            // There's no Next, can't return or we are at the end
            if (!Next || (Next == Head))
            {
                return nullptr;
            }
            
            return Next->Owner;
        }
        
        /** Returns the previous object in the list. */
        Type* GetPrev() const
        {
            if (!Prev || (Prev == Head))
            {
                return nullptr;
            }
            
            return Prev->Owner;
        }
        
        /** Returns the next node, if we are at the end then returns NULL. */
        CLinkList<Type>* NextNode() const
        {
            if (Next == Head)
            {
                return nullptr;
            }
            
            return Next;
        }
        
        /** Returns the previous node, if we are at the beginning returns NULL. */
        CLinkList<Type>* PrevNode() const
        {
            if (Prev == Head)
            {
                return nullptr;
            }
            
            return Prev;
        }
        
        /** Sets the list owner property. */
        NEKO_FORCE_INLINE void SetOwner(Type* type)
        {
            Owner = type;
        }
        
        /** Gets the list owner property. */
        NEKO_FORCE_INLINE Type* GetOwner()
        {
            return Owner;
        }
        
    private:
        CLinkList*   Head;
        CLinkList*   Next;
        CLinkList*   Prev;
        
        Type*   Owner;
    };
}
