//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Queue.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Math/MathUtils.h"
#include "../Data/IAllocator.h"
#include "../Mt/Atomic.h"

namespace Neko
{
    struct SQueueElem
    {
        void* PtrData;
        struct SQueueElem* Next;
    };
    
    /// Single producer/single consumer queue, lock-free.
    template <typename Ty> class SpScUnboundedQueueLf
    {
    public:
        
        SpScUnboundedQueueLf()
        : First(new Node(nullptr))
        , Divider(First)
        , Last(First)
        { }
        
        ~SpScUnboundedQueueLf()
        {
            while (nullptr != First)
            {
                Node* node = First;
                First = node->Next;
                
                delete node;
            }
        }
        
        void Push(Ty* ptr) // producer only
        {
            Last->Next = new Node((void*)ptr);
            Atomics::InterlockedExchangePtr((void**)&Last, Last->Next);
            while (First != Divider)
            {
                Node* node = First;
                First = First->Next;
                delete node;
            }
        }
        
        Ty* Peek() // consumer only
        {
            if (Divider != Last)
            {
                Ty* ptr = (Ty*)Divider->Next->Ptr;
                return ptr;
            }
            
            return nullptr;
        }
        
        Ty* Pop() // consumer only
        {
            if (Divider != Last)
            {
                Ty* ptr = (Ty*)Divider->Next->Ptr;
                Atomics::InterlockedExchangePtr((void**)&Divider, Divider->Next);
                return ptr;
            }
            
            return nullptr;
        }
        
    private:
        
        struct Node
        {
            Node(void* ptr)
            : Ptr(ptr)
            , Next(nullptr)
            { }
            
            void* Ptr;
            Node* Next;
        };
        
        Node* First;
        Node* Divider;
        Node* Last;
    };
    
    /// Simple queue. Not thread-safe.
	template <typename T, uint32 count> class Queue
	{
	public:
        
        struct Iterator
        {
            Queue* owner;
            uint32 cursor;
            
            bool operator!=(const Iterator& rhs) const { return cursor != rhs.cursor || owner != rhs.owner; }
            
            void operator ++() { ++cursor; }
            
            T& value()
            {
                uint32 idx = cursor & (count - 1); return owner->Buffer[idx];
            }
        };
        
		Queue(IAllocator& allocator)
        : Allocator(allocator)
		{
			assert(Math::IsPowOfTwo(count));
			Buffer = (T*)(Allocator.Allocate(sizeof(T) * count));
			Write = Read = 0;
		}

		~Queue()
		{
			Allocator.Deallocate(Buffer);
		}

        Iterator begin() { return {this, Read}; }
        Iterator end() { return {this, Write}; }
        
        NEKO_FORCE_INLINE bool IsFull() const { return GetSize() == count; }
		NEKO_FORCE_INLINE bool IsEmpty() const { return Read == Write; }
		NEKO_FORCE_INLINE uint32 GetSize() const { return Write - Read; }

		void Push(const T& item)
		{
			assert(Write - Read < count);

			uint32 idx = Write & (count - 1);
			::new (NewPlaceholder(), &Buffer[idx]) T(item);
			++Write;
		}

		void Pop()
		{
			assert(Write != Read);

			uint32 idx = Read & (count - 1);
			(&Buffer[idx])->~T();
			Read++;
		}

		NEKO_FORCE_INLINE T& front()
		{
			uint32 idx = Read & (count - 1);
			return Buffer[idx];
		}

		NEKO_FORCE_INLINE const T& front() const
		{
			uint32 idx = Read & (count - 1);
			return Buffer[idx];
		}

		NEKO_FORCE_INLINE T& back()
		{
			assert(!IsEmpty());

			uint32 idx = Write & (count - 1);
			return Buffer[idx - 1];
		}

		const T& back() const
		{
			assert(!IsEmpty());

			uint32 idx = Write & (count - 1);
			return Buffer[idx - 1];
		}
        
        void Empty()
        {
            Allocator.Deallocate(Buffer);
            Write = Read = 0;
        }

	private:
        
		IAllocator& Allocator;
		uint32 Read;
		uint32 Write;
		T* Buffer;
	};
}
