//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Delegate.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    template <typename T> class TDelegate;
    
    template <typename R> class TDelegate
    {
    private:
        
        typedef void* InstancePtr;
        typedef R (*InternalFunction)(InstancePtr);
        struct StubData
        {
            InstancePtr first;
            InternalFunction second;
        };
        
        template <R (*Function)()> static NEKO_FORCE_INLINE R FunctionStub(InstancePtr)
        {
            return (Function)();
        }
        
        template <class C, R (C::*Function)()> static NEKO_FORCE_INLINE R ClassMethodStub(InstancePtr instance)
        {
            return (static_cast<C*>(instance)->*Function)();
        }
        
        template <class C, R (C::*Function)() const> static NEKO_FORCE_INLINE R ClassMethodStub(InstancePtr instance)
        {
            return (static_cast<C*>(instance)->*Function)();
        }
        
    public:
        
        TDelegate(void)
        {
            Stub.first = nullptr;
            Stub.second = nullptr;
        }
        
        template <R (*Function)()> void Bind(void)
        {
            Stub.first = nullptr;
            Stub.second = &FunctionStub<Function>;
        }
        
        template <class C, R (C::*Function)()> void Bind(C* instance)
        {
            Stub.first = instance;
            Stub.second = &ClassMethodStub<C, Function>;
        }
        
        template <class C, R(C::*Function)() const> void Bind(C* instance)
        {
            Stub.first = instance;
            Stub.second = &ClassMethodStub<C, Function>;
        }
        
        R Invoke() const
        {
            assert(Stub.second != nullptr);
            return Stub.second(Stub.first);
        }
        
        bool operator == (const TDelegate<R>& rhs)
        {
            return Stub.first == rhs.Stub.first && Stub.second == rhs.Stub.second;
        }
        
    private:
        
        StubData Stub;
    };
    
    template <typename R, typename... Args> class TDelegate<R(Args...)>
    {
    private:
        
        typedef void* InstancePtr;
        typedef R (*InternalFunction)(InstancePtr, Args...);
        
        struct StubData
        {
            InstancePtr first;
            InternalFunction second;
        };
        
        template <R (*Function)(Args...)> static NEKO_FORCE_INLINE R FunctionStub(InstancePtr, Args... args)
        {
            return (Function)(args...);
        }
        
        template <class C, R (C::*Function)(Args...)>
        static NEKO_FORCE_INLINE R ClassMethodStub(InstancePtr instance, Args... args)
        {
            return (static_cast<C*>(instance)->*Function)(args...);
        }
        
    public:
        
        TDelegate(void)
        {
            Stub.first = nullptr;
            Stub.second = nullptr;
        }
        
        bool IsValid()
        {
            return Stub.second != nullptr;
        }
        
        template <R (*Function)(Args...)> void Bind(void)
        {
            Stub.first = nullptr;
            Stub.second = &FunctionStub<Function>;
        }
        
        template <class C, R (C::*Function)(Args...)> void Bind(C* instance)
        {
            Stub.first = instance;
            Stub.second = &ClassMethodStub<C, Function>;
        }
        
        R Invoke(Args... args) const
        {
            assert(Stub.second != nullptr);
            return Stub.second(Stub.first, args...);
        }
        
        bool operator == (const TDelegate<R(Args...)>& rhs)
        {
            return Stub.first == rhs.Stub.first && Stub.second == rhs.Stub.second;
        }
        
    private:
        
        StubData Stub;
    };
    
} // namespace Neko
