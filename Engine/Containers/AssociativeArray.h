//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  AssociativeArray.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Data/IAllocator.h"
#include "../Utilities/MetaProgramming.h"
#include "../Utilities/StringUtil.h"
#include "../Neko.h"

namespace Neko
{
    /// Associative element array storage.
    template <typename Key, typename Value>
    class TAssociativeArray
    {
    public:
        
        explicit TAssociativeArray(IAllocator& allocator)
        : Allocator(allocator)
        , Size(0)
        , Capacity(0)
        , Keys(nullptr)
        , Values(nullptr)
        {
        }
        
        ~TAssociativeArray()
        {
            CallDestructors(Keys, Size);
            CallDestructors(Values, Size);
            Allocator.Deallocate(Keys);
        }
        
        /** Inserts key at index and initialises it. */
        Value& Insert(const Key& key)
        {
            if (Capacity == Size)
            {
                Reserve(Capacity < 4 ? 4 : Capacity * 2);
            }
            
            int32 i = IndexOf(key);
            assert(i >= 0 && ((i < Size && Keys[i] != key) || i == Size));
            
            MoveMemory(Keys + i + 1, Keys + i, sizeof(Key) * (Size - i));
            MoveMemory(Values + i + 1, Values + i, sizeof(Value) * (Size - i));
            
            new (NewPlaceholder(), &Values[i]) Value();
            new (NewPlaceholder(), &Keys[i]) Key(key);
            
            ++Size;
            return Values[i];
        }
        

        template <class _Ty> inline _Ty&& myforward(typename RemoveReference<_Ty>::Type& _Arg)
        {
            return (static_cast<_Ty&&>(_Arg));
        }
        
        
        /** Places the key in the array and initialises with given parameters. */
        template <typename... Params> Value& Emplace(const Key& key, Params&&... params)
        {
            if (Capacity == Size)
            {
                Reserve((Capacity < 4) ? 4 : Capacity * 2);
            }
            
            int32 i = IndexOf(key);
            assert(i >= 0 && ((i < Size && Keys[i] != key) || i == Size));
            
            MoveMemory(Keys + i + 1, Keys + i, sizeof(Key) * (Size - i));
            MoveMemory(Values + i + 1, Values + i, sizeof(Value) * (Size - i));
            
            new (NewPlaceholder(), &Values[i]) Value(myforward<Params>(params)...);
            new (NewPlaceholder(), &Keys[i]) Key(key);
            
            ++Size;
            
            return Values[i];
        }
        
        
        /** Inserts a key with value into array and returns the index. */
        int32 Insert(const Key& key, const Value& value)
        {
            if (Capacity == Size)
            {
                Reserve(Capacity < 4 ? 4 : Capacity * 2);
            }
            
            int32 i = IndexOf(key);
            if (i >= 0 && ((i < Size && Keys[i] != key) || i == Size))
            {
                MoveMemory(Keys + i + 1, Keys + i, sizeof(Key) * (Size - i));
                MoveMemory(Values + i + 1, Values + i, sizeof(Value) * (Size - i));
                
                new (NewPlaceholder(), &Values[i]) Value(value);
                new (NewPlaceholder(), &Keys[i]) Key(key);
                ++Size;
                
                return i;
            }
            return INDEX_NONE;
        }
        
        
        /** Looks up for a key and value associatively. Returns TRUE if found. */
        bool Find(const Key& key, Value& value) const
        {
            int32 i = Find(key);
            if (i < 0)
            {
                return false;
            }
            
            value = Values[i];
            return true;
        }
        
        
        /** Looks up for a key and returns its index. */
        int32 Find(const Key& key) const
        {
            int32 l = 0;
            int32 h = Size - 1;
            
            while (l < h)
            {
                int32 mid = (l + h) >> 1;
                if (Keys[mid] < key)
                {
                    l = mid + 1;
                }
                else
                {
                    h = mid;
                }
            }
            
            if (l == h && Keys[l] == key)
            {
                return l;
            }
            return INDEX_NONE;
        }
        
        NEKO_FORCE_INLINE bool Contains(const Key& key) const
        {
            return Find(key) != INDEX_NONE;
        }
        
       
        NEKO_FORCE_INLINE const Value& operator [](const Key& key) const
        {
            int32 index = Find(key);
            if (index >= 0)
            {
                return Values[index];
            }
            else
            {
                assert(false);
                return Values[0];
            }
        }
        
        
        NEKO_FORCE_INLINE Value& operator [](const Key& key)
        {
            int32 index = Find(key);
            if (index >= 0)
            {
                return Values[index];
            }
            else
            {
                return Values[Insert(key, Value())];
            }
        }
        
        
        /** Returns the amount of elements. */
        NEKO_FORCE_INLINE int32 GetSize() const
        {
            return Size;
        }
        
        
        NEKO_FORCE_INLINE Value& Get(const Key& key)
        {
            int32 index = Find(key);
            assert(index >= 0);
            return Values[index];
        }
        
        
        NEKO_FORCE_INLINE Value* begin() { return Values; }
        NEKO_FORCE_INLINE Value* end() { return Values + Size; }
        NEKO_FORCE_INLINE const Value* begin() const { return Values; }
        NEKO_FORCE_INLINE const Value* end() const { return Values + Size; }
        
        
        /** Returns value at index. */
        NEKO_FORCE_INLINE Value& at(int32 index)
        {
            return Values[index];
        }
        
        
        /** Returns value at index. */
        NEKO_FORCE_INLINE const Value& at(int32 index) const
        {
            return Values[index];
        }
        
        
        /** Destructs keys and values, resets this array. */
        NEKO_FORCE_INLINE void Clear()
        {
            CallDestructors(Keys, Size);
            CallDestructors(Values, Size);
            Size = 0;
        }
        
        
        NEKO_FORCE_INLINE void Reserve(int32 NewCapacity)
        {
            if (Capacity >= NewCapacity)
            {
                return;
            }
            
            uint8* pNewData = (uint8*)Allocator.Allocate(NewCapacity * (sizeof(Key) + sizeof(Value)));
            
            CopyMemory(pNewData, Keys, sizeof(Key) * Size);
            CopyMemory(pNewData + sizeof(Key) * NewCapacity, Values, sizeof(Value) * Size);
            
            Allocator.Deallocate(Keys);
            Keys = (Key*)pNewData;
            Values = (Value*)(pNewData + sizeof(Key) * NewCapacity);
            
            Capacity = NewCapacity;
        }
        
        
        /** Returns the key at index. */
        NEKO_FORCE_INLINE Key& GetKey(int32 index)
        {
            return Keys[index];
        }
        /** Returns the key at index. */
        NEKO_FORCE_INLINE const Key& GetKey(int32 index) const
        {
            return Keys[index];
        }
        
        
        void EraseAt(int32 index)
        {
            if (index >= 0 && index < Size)
            {
                Values[index].~Value();
                Keys[index].~Key();
                if (index < Size - 1)
                {
                    MoveMemory(Keys + index, Keys + index + 1, sizeof(Key) * (Size - index - 1));
                    MoveMemory(Values + index, Values + index + 1, sizeof(Value) * (Size - index - 1));
                }
                --Size;
            }
        }
        
        
        /** Removes the key if exists. */
        NEKO_FORCE_INLINE int32 Erase(const Key& key)
        {
            int32 i = Find(key);
            if (i >= 0)
            {
                EraseAt(i);
            }
            
            return i;
        }
        
    private:
        
        int32 IndexOf(const Key& key) const
        {
            int32 l = 0;
            int32 h = Size - 1;
            while (l < h)
            {
                int32 mid = (l + h) >> 1;
                if (Keys[mid] < key)
                {
                    l = mid + 1;
                }
                else
                {
                    h = mid;
                }
            }
            if (l + 1 == Size && Keys[l] < key)
            {
                return l + 1;
            }
            return l;
        }
        
        
        template <typename T> void CallDestructors(T* ptr, int32 count)
        {
            for (int32 i = 0; i < count; ++i)
            {
                ptr[i].~T();
            }
        }
        
    private:
        
        IAllocator& Allocator;
        
        Key* Keys;
        Value* Values;
        
        int32 Size;
        int32 Capacity;
    };
    
} // namespace Neko
