//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Dictionary.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Dictionary.h"
#include "../Core/Log.h"

namespace Neko
{
    CDictionary::CDictionary(IAllocator& allocator)
    : Storage(allocator)
    {
        
    }
    
    CDictionary::~CDictionary()
    {
        Clear();
    }
    
    void CDictionary::Print()
    {
        
    }

    void CDictionary::Clear()
    {
        Storage.Clear();
    }
    
    const CDictionary::KeyValue* CDictionary::GetKeyValue(const int32 Index) const
    {
        for (int32 i = 0; i < Storage.GetSize(); ++i)
        {
            if (Index == i)
            {
                return &Storage[i];
            }
        }
        return nullptr;
    }
    
    int CDictionary::FindKeyIndex(const char* Key)
    {
        int32 keyHash = Crc32(Key);
        for (int32 i = 0; i < Storage.GetSize(); ++i)
        {
            if (Storage[i].KeyHash == keyHash)
            {
                return i;
            }
        }
        return -1;
    }
    
    const CDictionary::KeyValue* CDictionary::FindKey(const char *Key) const
    {
        if (Key == nullptr || Key[0] == '\0')
        {
            assert(false);
            return nullptr;
        }
        
        int32 keyHash = Crc32(Key);
        for (auto& i : Storage)
        {
            if (i.KeyHash == keyHash)
            {
                return &i;
            }
        }
        
        return nullptr;
    }
    
    void CDictionary::TransferKeyValues(CDictionary& Other)
    {
        if (this == &Other)
        {
            return;
        }

        Clear();
        
        Storage = Other.Storage; // 'operator =' in Array.h

        Other.Clear();
    }
    
    void CDictionary::SetDefaults(const Neko::CDictionary& Other)
    {
        int32 i, num;
        
        KeyValue newKv;
        
        num = Other.GetNumKeyVals();
        for (i = 0; i < num; ++i)
        {
            const KeyValue& def = Other.Storage[i];
            const KeyValue* existingKv = FindKey(def.GetKey());
            
            if (!existingKv)
            {
                newKv.Key.Set(def.GetKey());
                newKv.KeyHash = Crc32(newKv.Key.data);
                newKv.Value.Set(def.GetValue());
                // Add this key-value pair.
                Storage.Push(newKv);
            }
        }
    }
    
    void CDictionary::Delete(const char *Key)
    {
        int32 index = FindKeyIndex(Key);
        Storage.Erase(index);
    }
    
    void CDictionary::Set(const char *Key, const char *Value)
    {
        assert(Key);
        assert(Value);
        
        int index;
        
        index = FindKeyIndex(Key);
        
        if (index == -1)
        {
            // insert
            KeyValue kv;
            kv.Key.Set(Key);
            kv.KeyHash = Crc32(Key);
            kv.Value.Set(Value);
            
            Storage.Push(kv);
        }
        else
        {
            // update
            KeyValue& value = Storage[index];
            value.Value.Set(Value);
        }
        
    }
    
    void CDictionary::SetFloat(const char *Key, const float Value)
    {
        Set(Key, va("%f", Value));
    }
    
    void CDictionary::SetInt(const char *Key, const int Value)
    {
        Set(Key, va("%i", Value));
    }
    
    void CDictionary::SetBool(const char *Key, const bool Value)
    {
        Set(Key, va("%i", Value));
    }
    
    void CDictionary::SetVec2(const char *Key, const Neko::Vec2& Value)
    {
        Set(Key, Value.ToString());
    }
    
    void CDictionary::SetVec3(const char *Key, const Neko::Vec3& Value)
    {
        Set(Key, Value.ToString());
    }
    
    void CDictionary::SetVec4(const char *Key, const Neko::Vec4& Value)
    {
        Set(Key, Value.ToString());
    }
    
    const char* CDictionary::GetString(const char *Key, const char* DefaultValue) const
    {
        auto key = FindKey(Key);
        if (key)
        {
            return key->Value;
        }
        
        return DefaultValue;
    }
    
    float CDictionary::GetFloat(const char *Key, const char* DefaultValue) const
    {
        return atof(GetString(Key, DefaultValue));
    }
    
    int CDictionary::GetInt(const char *Key, const char* DefaultValue) const
    {
        return atoi(GetString(Key, DefaultValue));
    }
    
    bool CDictionary::GetBool(const char *Key, const char* DefaultValue) const
    {
        return atoi(GetString(Key, DefaultValue)) != 0;
    }
    
    Vec2 CDictionary::GetVec2(const char *Key, const char* DefaultValue) const
    {
        Vec2 out;
        const char *s;
        
        if (!DefaultValue)
        {
            DefaultValue = "0 0";
        }
        
        s = GetString(Key, DefaultValue);
        out.MakeNull();
        sscanf(s, "%f %f", &out.x, &out.y);
        return out;
    }
    
    Vec3 CDictionary::GetVec3(const char *Key, const char* DefaultValue) const
    {
        Vec3 out;
        const char *s;
        
        if (!DefaultValue)
        {
            DefaultValue = "0 0 0";
        }
        
        s = GetString(Key, DefaultValue);
        out.MakeZero();
        sscanf( s, "%f %f %f", &out.x, &out.y, &out.z);
        return out;
    }
    
    Vec4 CDictionary::GetVec4(const char *Key, const char* DefaultValue) const
    {
        Vec4 out;
        const char *s;
        
        if (!DefaultValue)
        {
            DefaultValue = "0 0 0";
        }
        
        s = GetString(Key, DefaultValue);
        out.MakeZero();
        sscanf( s, "%f %f %f %f", &out.x, &out.y, &out.z, &out.w);
        return out;
    }
    
    bool CDictionary::GetString(const char* Key, const char* DefaultValue, const char** Out) const
    {
        auto key = FindKey(Key);
        
        if (Key != nullptr)
        {
            *Out = key->GetValue();
            return true;
        }
        
        *Out = DefaultValue;
        
        return false;
    }
    
    bool CDictionary::GetString(const char* Key, const char* DefaultValue, Text& Out) const
    {
        auto key = FindKey(Key);
        if (key)
        {
            Out = Key;
            return true;
        }
        
        Out = DefaultValue;
        return false;
    }
    
    bool CDictionary::GetFloat(const char* Key, const char* DefaultValue, float& Out) const
    {
        const char*	s;
        bool found;
        
        found = GetString(Key, DefaultValue, &s);
        Out = atof(s);
        return found;
    }
    
    bool CDictionary::GetInt(const char* Key, const char* DefaultValue, int& Out) const
    {
        const char*	s;
        bool found;
        
        found = GetString(Key, DefaultValue, &s);
        Out = atoi(s);
        return found;
    }
    
    bool CDictionary::GetBool(const char* Key, const char* DefaultValue, bool& Out) const
    {
        const char*	s;
        bool found;
        
        found = GetString(Key, DefaultValue, &s);
        Out = (atoi(s) != 0);
        return found;
    }
}
