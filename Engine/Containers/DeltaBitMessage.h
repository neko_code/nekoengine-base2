//
//  DeltaBitMessage.h
//  Nanocat

#pragma once

#include "BitMessage.h"
#include "../Platform/SystemShared.h"

namespace Neko
{
    /// Delta bit message
    class NEKO_ENGINE_API CBitMessageDelta
    {
    public:
        
        CBitMessageDelta();
        
        ~CBitMessageDelta()
        {
        }
        
        void Init(const CBitMessage* base, CBitMessage* newBase, CBitMessage* delta);
        void Init(const CBitMessage* base, CBitMessage* newBase, const CBitMessage* delta);
        
        bool HasChanged(void) const;
        
        void WriteBits(int value, int numBits);
        void WriteChar(int c);
        void WriteByte(int c);
        void WriteShort(int c);
        void WriteUShort(int c);
        void WriteLong(int c);
        void WriteFloat(float f);
        void WriteFloat(float f, int exponentBits, int mantissaBits);
        void WriteAngle8(float f);
        void WriteAngle16(float f);
        void WriteDir(const Vec3& dir, int numBits);
        void WriteString(const char* s, int maxLength = -1);
        void WriteData(const void* data, int length);
        
        void WriteDeltaChar(int oldValue, int newValue);
        void WriteDeltaByte(int oldValue, int newValue);
        void WriteDeltaShort(int oldValue, int newValue);
        void WriteDeltaLong(int oldValue, int newValue);
        void WriteDeltaFloat(float oldValue, float newValue);
        void WriteDeltaFloat(float oldValue, float newValue, int exponentBits, int mantissaBits);
        void WriteDeltaByteCounter(int oldValue, int newValue);
        void WriteDeltaShortCounter(int oldValue, int newValue);
        void WriteDeltaLongCounter(int oldValue, int newValue);
        
        int ReadBits(int numBits) const;
        int ReadChar(void) const;
        int ReadByte(void) const;
        int ReadShort(void) const;
        int ReadUShort(void) const;
        int ReadLong(void) const;
        float ReadFloat(void) const;
        float ReadFloat(int exponentBits, int mantissaBits) const;
        float ReadAngle8(void) const;
        float ReadAngle16(void) const;
        Vec3 ReadDir(int numBits) const;
        void ReadString(char* buffer, int bufferSize) const;
        void ReadData(void* data, int length) const;
        int ReadDeltaChar(int oldValue) const;
        int ReadDeltaByte(int oldValue) const;
        int ReadDeltaShort(int oldValue) const;
        int ReadDeltaLong(int oldValue) const;
        float ReadDeltaFloat(float oldValue) const;
        float ReadDeltaFloat(float oldValue, int exponentBits, int mantissaBits) const;
        int ReadDeltaByteCounter(int oldValue) const;
        int ReadDeltaShortCounter(int oldValue) const;
        int ReadDeltaLongCounter(int oldValue) const;
        
    private:
        const CBitMessage* base; // base
        CBitMessage* newBase; // new base
        CBitMessage* writeDelta; // delta from base to new base for writing
        const CBitMessage* readDelta; // delta from base to new base for reading
        mutable bool changed; // true if the new base is different from the base
        
    private:
        void WriteDelta(int oldValue, int newValue, int numBits);
        int ReadDelta(int oldValue, int numBits) const;
    };

    NEKO_FORCE_INLINE CBitMessageDelta::CBitMessageDelta()
    {
        base = NULL;
        newBase = NULL;
        writeDelta = NULL;
        readDelta = NULL;
        changed = false;
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::Init(const CBitMessage* base, CBitMessage* newBase, CBitMessage* delta)
    {
        this->base = base;
        this->newBase = newBase;
        this->writeDelta = delta;
        this->readDelta = delta;
        this->changed = false;
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::Init(const CBitMessage* base, CBitMessage* newBase, const CBitMessage* delta)
    {
        this->base = base;
        this->newBase = newBase;
        this->writeDelta = NULL;
        this->readDelta = delta;
        this->changed = false;
    }

    NEKO_FORCE_INLINE bool CBitMessageDelta::HasChanged(void) const
    {
        return changed;
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteChar(int c)
    {
        WriteBits(c, -8);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteByte(int c)
    {
        WriteBits(c, 8);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteShort(int c)
    {
        WriteBits(c, -16);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteUShort(int c)
    {
        WriteBits(c, 16);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteLong(int c)
    {
        WriteBits(c, 32);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteFloat(float f)
    {
        WriteBits(*reinterpret_cast<int*>(&f), 32);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteAngle8(float f)
    {
        WriteBits(ANGLE2BYTE(f), 8);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteAngle16(float f)
    {
        WriteBits(ANGLE2SHORT(f), 16);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteDir(const Vec3& dir, int numBits)
    {
        WriteBits(CBitMessage::DirToBits(dir, numBits), numBits);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteDeltaChar(int oldValue, int newValue)
    {
        WriteDelta(oldValue, newValue, -8);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteDeltaByte(int oldValue, int newValue)
    {
        WriteDelta(oldValue, newValue, 8);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteDeltaShort(int oldValue, int newValue)
    {
        WriteDelta(oldValue, newValue, -16);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteDeltaLong(int oldValue, int newValue)
    {
        WriteDelta(oldValue, newValue, 32);
    }
    
    NEKO_FORCE_INLINE void CBitMessageDelta::WriteDeltaFloat(float oldValue, float newValue)
    {
        WriteDelta(*reinterpret_cast<int*>(&oldValue), *reinterpret_cast<int*>(&newValue), 32);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadChar(void) const
    {
        return (signed char)ReadBits(-8);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadByte(void) const
    {
        return (unsigned char)ReadBits(8);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadShort(void) const
    {
        return (short)ReadBits(-16);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadUShort(void) const
    {
        return (unsigned short)ReadBits(16);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadLong(void) const
    {
        return ReadBits(32);
    }
    
    NEKO_FORCE_INLINE float CBitMessageDelta::ReadFloat(void) const
    {
        float value;
        *reinterpret_cast<int*>(&value) = ReadBits(32);
        return value;
    }
    
    NEKO_FORCE_INLINE float CBitMessageDelta::ReadAngle8(void) const
    {
        return BYTE2ANGLE(ReadByte());
    }
    
    NEKO_FORCE_INLINE float CBitMessageDelta::ReadAngle16(void) const
    {
        return SHORT2ANGLE(ReadShort());
    }
    
    NEKO_FORCE_INLINE Vec3 CBitMessageDelta::ReadDir(int numBits) const
    {
        return CBitMessage::BitsToDir(ReadBits(numBits), numBits);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadDeltaChar(int oldValue) const
    {
        return (signed char)ReadDelta(oldValue, -8);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadDeltaByte(int oldValue) const
    {
        return (unsigned char)ReadDelta(oldValue, 8);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadDeltaShort(int oldValue) const
    {
        return (short)ReadDelta(oldValue, -16);
    }
    
    NEKO_FORCE_INLINE int CBitMessageDelta::ReadDeltaLong(int oldValue) const
    {
        return ReadDelta(oldValue, 32);
    }
    
    NEKO_FORCE_INLINE float CBitMessageDelta::ReadDeltaFloat(float oldValue) const
    {
        float value;
        *reinterpret_cast<int*>(&value) = ReadDelta(*reinterpret_cast<int*>(&oldValue), 32);
        return value;
    }
};
