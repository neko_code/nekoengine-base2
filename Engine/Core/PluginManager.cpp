//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  PluginManager.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "PluginManager.h"
#include "Profiler.h"
#include "IPlugin.h"
#include "Engine.h"
#include "Log.h"
#include "Debug.h"
#include "../Platform/Platform.h"
#include "../Containers/Array.h"

#if NEKO_APPLE_FAMILY
#   include <dlfcn.h>
#endif

namespace Neko
{
    /// Implementation for plugin manager.
    class PluginManagerImpl final : public IPluginManager
    {
    private:
        
        typedef TArray<IPlugin*> PluginList;
        typedef TArray<void*> LibraryList;
        
    public:
        
        PluginManagerImpl(IEngine& engine, IAllocator& allocator)
        : Plugins(allocator)
        , Libraries(allocator)
        , Allocator(allocator)
        , Engine(engine)
        , LibraryLoadedList(allocator)
        { }
        
        ~PluginManagerImpl()
        {
            // Unload everything
            for (int32 i = Plugins.GetSize() - 1; i >= 0; --i)
            {
                NEKO_DELETE(Engine.GetAllocator(), Plugins[i]);
            }
            
            for (int32 i = 0; i < Libraries.GetSize(); ++i)
            {
                Platform::UnloadLibrary(Libraries[i]);
            }
        }
        
        void Update(float dt, bool bPaused) override
        {
            PROFILE_FUNCTION();
            
            for (int32 i = 0, c = Plugins.GetSize(); i < c; ++i)
            {
                Plugins[i]->Update(dt);
            }
        }
        
        void Serialize(OutputBlob& serializer) override
        {
            for (int32 i = 0, c = Plugins.GetSize(); i < c; ++i)
            {
                Plugins[i]->Serialize(serializer);
            }
        }
        
        void Deserialize(InputBlob& serializer) override
        {
            for (int32 i = 0, c = Plugins.GetSize(); i < c; ++i)
            {
                Plugins[i]->Deserialize(serializer);
            }
        }
        
        NEKO_FORCE_INLINE const TArray<void*>& GetLibraries() const override { return Libraries; }
        
        NEKO_FORCE_INLINE const TArray<IPlugin*>& GetPlugins() const override { return Plugins; }
        
        IPlugin* GetPlugin(const char* NEKO_RESTRICT Name) override
        {
            for (int32 i = 0; i < Plugins.GetSize(); ++i)
            {
                if (Neko::EqualStrings(Plugins[i]->GetName(), Name))
                {
                    return Plugins[i];
                }
            }
            return 0;
        }
        
        NEKO_FORCE_INLINE DelegateList<void(void*)>& OnLibraryLoaded() override { return LibraryLoadedList; }
        
        IPlugin* Load(const char* Path) override
        {
            Text CurrentDirectory("");
#if NEKO_UNIX_FAMILY
            CurrentDirectory.Append("lib"); // Add 'lib' prefix
            CurrentDirectory.Append(Path);
            CurrentDirectory.Append(".dylib");
#else
            CurrentDirectory.Append(Path);
            CurrentDirectory.Append(".dll");
#endif
            
            GLogInfo.log("Core") << "Loading plugin " << Path;
            
            typedef IPlugin* (*PluginCreator)(IEngine&);
            auto* lib = Platform::LoadSystemLibrary(CurrentDirectory);
            if (lib)
            {
                // entry point
                PluginCreator creator = (PluginCreator)Platform::GetLibrarySymbol(lib, "createPlugin");
                if (creator)
                {
                    IPlugin* plugin = creator(Engine);
                    if (!plugin)
                    {
                        GLogError.log("Core") << "CreatePlugin() failed.";
                        
                        NEKO_DELETE(Engine.GetAllocator(), plugin);
                        assert(false);
                    }
                    else
                    {
                        AddPlugin(plugin);
                        Libraries.Push(lib);
                        LibraryLoadedList.Invoke(lib);
                        
                        GLogInfo.log("Core") << "Plugin loaded.";
                        Neko::Debug::StackTree::RefreshModuleList();
                        
                        return plugin;
                    }
                }
                else
                {
                    GLogError.log("Core") << "No entry point function found in plugin.";
                }
                Platform::UnloadLibrary(lib);
            }
            else // Plugin couldn't load, probably static one
            {
                auto* plugin = StaticPluginRegister::Create(Path, Engine);
                if (plugin)
                {
                    GLogInfo.log("Core") << "Plugin loaded (static).";
                    
                    AddPlugin(plugin);
                    return plugin;
                }
                
                GLogWarning.log("Core") << "Couldn't load plugin. ";
            }
            return nullptr;
        }
        
        NEKO_FORCE_INLINE IAllocator& GetAllocator() { return Allocator; }
        
        void AddPlugin(IPlugin* plugin) override
        {
            Plugins.Push(plugin);
            for (auto* i : Plugins)
            {
                i->PluginAdded(*plugin);
                plugin->PluginAdded(*i);
            }
        }
    
    private:
        
        IEngine& Engine;
        DelegateList<void(void*)> LibraryLoadedList;
        LibraryList Libraries;
        PluginList Plugins;
        IAllocator& Allocator;
    };
    
    
    IPluginManager* IPluginManager::Create(IEngine& engine)
    {
        return NEKO_NEW(engine.GetAllocator(), PluginManagerImpl)(engine, engine.GetAllocator());
    }
    
    void IPluginManager::Destroy(IPluginManager* manager)
    {
        NEKO_DELETE(static_cast<PluginManagerImpl*>(manager)->GetAllocator(), manager);
    }
} // ~namespace Neko
