//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Application.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Platform/SystemShared.h"
#include "../Platform/InputInterfaces.h"
#include "../Core/EventHandler.h"

namespace Neko
{
    //! Flags which are determined on the runtime
    namespace EWindowRuntimeFlags
    {
        enum Type
        {
            HasInputFocus = BIT(0),        //<! window has input focus
        };
    }
    
    /// System window activation methods.
	enum class ENativeWindowActivation : int
	{
		Activate = 0,
		ActivateByMouse,
		Deactivate
	};
        
    /// Application base interface. Used for each platform.
    class IPlatformApplication
    {
    public:
        
        IPlatformApplication()
        : MessageHandler(nullptr)
        { }
        
        /**
         * Virtual destructor.
         */
        virtual ~IPlatformApplication()
        { }
        
        virtual void SetMessageHandler(/*const*/ IGenericMessageHandler* InMessageHandler)
        {
            MessageHandler = InMessageHandler;
        }
        
        NEKO_FORCE_INLINE IGenericMessageHandler* GetMessageHandler()
        {
            assert( MessageHandler ); // make sure its set
            return MessageHandler;
        }

        
        // Empty functionality
        /** Processes system events. */
        virtual void ProcessEvents() { };
        
        /** Gets system events and puts them into the queue. */
        virtual void GetEvents() { };
        
        /** Returns the pointer to the currently active window. */
        virtual IGenericWindow* GetActiveWindow() { return nullptr; };
        
        /** 
         * Creates the new window with given parameters. 
         *
         * @see 'FWindowDesc'
         */
        virtual IGenericWindow* MakeWindow(const FWindowDesc& InDesc) { return nullptr; };
        
        /** Destroys the window. */
        virtual void DestroyWindow(IGenericWindow* Window) { }

        /** Toggles the cursor visibility. */
        virtual void SetCursorShow(bool bShown) { }
        
        /** Warps mouse to the given location. */
        virtual void WarpMouse(int32 X, int32 Y) { }
        
        /** Sets the current cursor type. */
        virtual void SetCursorType(Neko::ICursor::EMouseCursorType Type) { }
        
        /** Toggles the relative mouse mode. */
        virtual bool SetRelativeMouseMode(bool bEnabled) { return false; }
        
        /** Returns current cursor type. */
        virtual Neko::ICursor::EMouseCursorType GetCursorType() const = 0;
        
        /** Initialises system hardware devices. */
        virtual void InitializeDevices() { };

    public:

        static NEKO_ENGINE_API IPlatformApplication* Create();
        
        static NEKO_ENGINE_API void Destroy(IPlatformApplication& Application);
        
    protected:
        
        IGenericMessageHandler* MessageHandler;
    };
    
}
