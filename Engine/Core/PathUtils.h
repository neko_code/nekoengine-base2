//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  PathUtils.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Utilities/Text.h"
#include "Path.h"

#include <string>

namespace Neko
{
    namespace PathUtils
    {
        NEKO_ENGINE_API void Normalize(const char* Path, char* Out, uint32 MaxSize);
        NEKO_ENGINE_API void GetDirectory(char* Directory, int32 MaxLength, const char* Src);
        NEKO_ENGINE_API void GetBasename(char* Basename, int32 MaxLength, const char* Src);
        NEKO_ENGINE_API void GetFilename(char* Filename, int32 MaxLength, const char* Src);
        NEKO_ENGINE_API void GetExtension(char* Extension, int32 MaxLength, const char* Src);
        NEKO_ENGINE_API bool HasExtension(const char* Filename, const char* Extension);
        NEKO_ENGINE_API bool IsAbsolute(const char* Path);
        NEKO_ENGINE_API void RemoveExtension(char* FilePath);
        NEKO_ENGINE_API void ReplaceExtension(Text& filepath, const char* Extension);
        
        struct NEKO_ENGINE_API PathDirectory
        {
            explicit PathDirectory(const char* path)
            {
                GetDirectory(Directory, sizeof(Directory), path);
            }
            
            operator const char*()
            {
                return Directory;
            }
            
            char Directory[MAX_PATH_LENGTH];
        };
        
        
        struct NEKO_ENGINE_API FileInfo
        {
            explicit FileInfo(const char* path)
            {
                GetExtension(Extension, sizeof(Extension), path);
                GetBasename(Basename, sizeof(Basename), path);
                GetDirectory(Directory, sizeof(Directory), path);
            }
            
            char Extension[10];
            char Basename[MAX_PATH_LENGTH];
            char Directory[MAX_PATH_LENGTH];
        };
        
    } // namespace PathUtils
} // namespace Neko
