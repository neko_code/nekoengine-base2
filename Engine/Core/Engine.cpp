//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Engine.cpp
//  Neko engine
//
//  Copyright © 2013 Neko Vision. All rights reserved.
//

#include "Engine.h"
#include "../FS/MemoryFileDevice.h"
#include "../FS/ResourceFileDevice.h"
#include "../FS/DiskFileDevice.h"
#include "../FS/PlatformFile.h"
#include "../FS/FileSystem.h"
#include "../Math/MathUtils.h"
#include "../Platform/Application.h"
#include "../Universe/Universe.h"
#include "ResourceManager.h"
#include "PluginManager.h"
#include "LifoAllocator.h"
#include "EventHandler.h"
#include "Application.h"
#include "InputSystem.h"
#include "LuaWrapper.h"
#include "Reflection.h"
#include "JobSystem.h"
#include "Profiler.h"
#include "IPlugin.h"
#include "Prefab.h"
#include "Crc32.h"
#include "Debug.h"
#include "Timer.h"
#include "Blob.h"
#include "Path.h"
#include "Log.h"

namespace Neko
{
    /// Engine base implementation.
    class EngineImpl final : public IEngine
    {
    public:
        
        EngineImpl(const char* Basepath0, const char* Basepath1, FS::FileSystem* fs, IAllocator& allocator);
        
        ~EngineImpl();
        
        void SetPatchPath(const char* path) override;
        
        Universe& CreateUniverse(bool bSetLuaGlobals) override;
        void DestroyUniverse(Universe& universe) override;
        
        
        void StartGame(Universe& context) override;
        void StopGame(Universe& context) override;
        
        void Pause(bool pause) override;
        
        void NextFrame() override;
        
        void SetTimeMultiplier(float multiplier) override;
        
        void SleepIfNeeded();
        
        void Update(Universe& context) override;
        
        // State serialisation
        
        void SerializerSceneVersions(OutputBlob& serializer, Universe& ctx);
        void SerializePluginList(OutputBlob& serializer);
        
        bool HasSupportedSceneVersions(InputBlob& serializer, Universe& ctx);
        bool HasSerializedPlugins(InputBlob& serializer);
        
        uint32 Serialize(Universe& ctx, OutputBlob& serializer) override;
        bool Deserialize(Universe& ctx, InputBlob& serializer) override;
        
        ComponentUID CreateComponent(Universe& universe, Entity entity, ComponentType type) override;
        
        
        NEKO_FORCE_INLINE void SetPlatformData(const SPlatformData& data) override { PlatformData = data; }
        NEKO_FORCE_INLINE const SPlatformData& GetPlatformData() override { return PlatformData; }
        
        NEKO_FORCE_INLINE IAllocator& GetAllocator() override { return Allocator; }
        
        NEKO_FORCE_INLINE CEventDispatcher& GetEventDispatcher() override { return *EventSystem; }
        
        IPluginManager& GetPluginManager() override { return *PluginManager; }
        
        NEKO_FORCE_INLINE FS::FileSystem& GetFileSystem() override { return *pFileSystem; }
        NEKO_FORCE_INLINE FS::DiskFileDevice* GetDiskFileDevice() override { return pDiskFileDevice; }
        NEKO_FORCE_INLINE FS::DiskFileDevice* GetPatchFileDevice() override { return pPatchFileDevice; }
        NEKO_FORCE_INLINE FS::ResourceFileDevice* GetResourceFileDevice() override { return pResourceFileDevice; }
        
        NEKO_FORCE_INLINE IInputSystem& GetInputSystem() override { return *pInputSystem; }
        
        NEKO_FORCE_INLINE CResourceManager& GetResourceManager() override { return ResourceManager; }
        
        NEKO_FORCE_INLINE CTimer& GetTimer() override { return MainTimer; }
        
        NEKO_FORCE_INLINE float GetFPS() const override { return fCurrentFramerate; }
        
        NEKO_FORCE_INLINE lua_State* GetState() override { return State; }
        NEKO_FORCE_INLINE CPathManager& GetPathManager() override { return PathManager; }
        
        NEKO_FORCE_INLINE float GetLastTimeDelta() const override { return LastTimeDelta / MainTimer.GetTimeScale(); }
        
        NEKO_FORCE_INLINE IAllocator& GetLIFOAllocator() override { return LifoAllocator; }
        
        
        // Lua

        static bool LUA_hasFilesystemWork(IEngine* engine)
        {
            return engine->GetFileSystem().HasWork();
        }
        
        static void LUA_processFilesystemWork(IEngine* engine)
        {
            engine->GetFileSystem().UpdateAsyncActions();
        }
        
        static void LUA_startGame(IEngine* engine, Universe* universe)
        {
            if (engine && universe)
            {
                engine->StartGame(*universe);
            }
        }
        
        static ComponentHandle LUA_createComponent(Universe* universe, Entity entity, const char* type)
        {
            if (!universe)
            {
                return INVALID_COMPONENT;
            }
            
            ComponentType CompntType = Reflection::GetComponentType(type);
            IScene* scene = universe->GetScene(CompntType);
            if (!scene)
            {
                return INVALID_COMPONENT;
            }
            
            if (scene->GetComponent(entity, CompntType) != INVALID_COMPONENT)
            {
                GLogError.log("Lua Script") << "Component " << type << " already exists in entity " << entity.index;
                return INVALID_COMPONENT;
            }
            
            return scene->CreateComponent(CompntType, entity);
        }
        
        static Entity LUA_CreateEntity(Universe* univ)
        {
            return univ->CreateEntity(Vec3::Zero, Quat(0, 0, 0, 1));
        }
        
        struct SetPropertyVisitor : public Reflection::IPropertyVisitor
        {
            void visit(const Reflection::Property<float>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_isnumber(L, -1))
                {
                    float f = (float)lua_tonumber(L, -1);
                    InputBlob inputBlob(&f, sizeof(f));
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<int>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_isinteger(L, -1))
                {
                    int i = (int)lua_tointeger(L, -1);
                    InputBlob inputBlob(&i, sizeof(i));
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<Entity>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_isinteger(L, -1))
                {
                    int i = (int)lua_tointeger(L, -1);
                    InputBlob inputBlob(&i, sizeof(i));
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<Int2>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_istable(L, -1))
                {
                    auto v = LuaWrapper::toType<Int2>(L, -1);
                    InputBlob inputBlob(&v, sizeof(v));
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<Vec2>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_istable(L, -1))
                {
                    auto v = LuaWrapper::toType<Vec2>(L, -1);
                    InputBlob inputBlob(&v, sizeof(v));
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<Vec3>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_istable(L, -1))
                {
                    auto v = LuaWrapper::toType<Vec3>(L, -1);
                    InputBlob inputBlob(&v, sizeof(v));
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<Vec4>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_istable(L, -1))
                {
                    auto v = LuaWrapper::toType<Vec4>(L, -1);
                    InputBlob inputBlob(&v, sizeof(v));
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<CPath>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_isstring(L, -1))
                {
                    const char* str = lua_tostring(L, -1);
                    InputBlob inputBlob(str, StringLength(str) + 1);
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<bool>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_isboolean(L, -1))
                {
                    bool b = lua_toboolean(L, -1) != 0;
                    InputBlob inputBlob(&b, sizeof(b));
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::Property<const char*>& prop) override
            {
                if (!EqualStrings(property_name, prop.name))
                {
                    return;
                }
                if (lua_isstring(L, -1))
                {
                    const char* str = lua_tostring(L, -1);
                    InputBlob inputBlob(str, StringLength(str) + 1);
                    prop.SetValue(component, -1, inputBlob);
                }
            }
            void visit(const Reflection::IArrayProperty& prop) override
            {
                if (!EqualStrings(property_name, prop.name)) return;
                GLogError.log("Lua Script") << "Property " << prop.name << " has unsupported type";
            }
            void visit(const Reflection::IEnumProperty& prop) override { notSupported(prop); }
            void visit(const Reflection::IBlobProperty& prop) override { notSupported(prop); }
            void visit(const Reflection::ISampledFuncProperty& prop) override { notSupported(prop); }
            void notSupported(const Reflection::PropertyBase& prop)
            {
                if (!EqualStrings(property_name, prop.name)) return;
                GLogError.log("Lua Script") << "Property " << prop.name << " has unsupported type";
            }
            lua_State* L;
            ComponentUID component;
            const char* property_name;
        };

        static int LUA_GetComponentType(const char* ComponentType)
        {
            return Reflection::GetComponentType(ComponentType).index;
        }
        
        static ComponentHandle LUA_GetComponent(Universe* universe, Entity entity, int componentType)
        {
            if (!universe->HasComponent(entity, {componentType}))
            {
                return INVALID_COMPONENT;
            }
            
            ComponentType type = { componentType};
            auto scene = universe->GetScene(type);
            if (scene)
            {
                return scene->GetComponent(entity, type);
            }
            
            assert(false);
            return INVALID_COMPONENT;
        }
        
        static int LUA_createEntityEx(lua_State* L)
        {
            auto* ctx = LuaWrapper::checkArg<Universe*>(L, 2);
            LuaWrapper::checkTableArg(L, 3);
            Entity e = ctx->CreateEntity(Vec3(0, 0, 0), Quat(0, 0, 0, 1));
            lua_pushvalue(L, 3);
            lua_pushnil(L);
            while (lua_next(L, -2) != 0)
            {
                const char* parameter_name = luaL_checkstring(L, -2);
                if (EqualStrings(parameter_name, "position"))
                {
                    auto pos = LuaWrapper::toType<Vec3>(L, -1);
                    ctx->SetPosition(e, pos);
                }
                else if (EqualStrings(parameter_name, "rotation"))
                {
                    auto rot = LuaWrapper::toType<Quat>(L, -1);
                    ctx->SetRotation(e, rot);
                }
                else
                {
                    ComponentType componentType = Reflection::GetComponentType(parameter_name);
                    IScene* scene = ctx->GetScene(componentType);
                    if (scene)
                    {
                        ComponentUID component(e, componentType, scene, scene->CreateComponent(componentType, e));
                        const Reflection::ComponentBase* cmp_des = Reflection::GetComponent(componentType);
                        if (component.IsValid())
                        {
                            lua_pushvalue(L, -1);
                            lua_pushnil(L);
                            while (lua_next(L, -2) != 0)
                            {
                                const char* property_name = luaL_checkstring(L, -2);
                                SetPropertyVisitor v;
                                v.property_name = property_name;
                                v.component = component;
                                v.L = L;
                                cmp_des->visit(v);
                                lua_pop(L, 1);
                            }
                            lua_pop(L, 1);
                        }
                    }
                }
                lua_pop(L, 1);
            }
            lua_pop(L, 1);
            LuaWrapper::push(L, e);
            return 1;
        }

        static Entity LUA_GetFirstEntity(Universe* universe)
        {
            return universe->GetFirstEntity();
        }
        
        static Entity LUA_GetNextEntity(Universe* universe, Entity entity)
        {
            return universe->GetNextEntity(entity);
        }
        
        static void LUA_setEntityPosition(Universe* univ, Entity entity, Vec3 pos)
        {
            univ->SetPosition(entity, pos);
        }
        
        static int LUA_setEntityRotation(lua_State* L)
        {
            Universe* univ = LuaWrapper::checkArg<Universe*>(L, 1);
            int entity_index = LuaWrapper::checkArg<int>(L, 2);
            if (entity_index < 0)// || entity_index > univ->getEntityCount())
            {
                return 0;
            }
            
            if (lua_gettop(L) > 3)
            {
                Vec3 axis = LuaWrapper::checkArg<Vec3>(L, 3);
                float angle = LuaWrapper::checkArg<float>(L, 4);
                univ->SetRotation({ entity_index }, Quat(axis, angle));
            }
            else
            {
                Quat rot = LuaWrapper::checkArg<Quat>(L, 3);
                univ->SetRotation({ entity_index }, rot);
            }
            return 0;
        }

        static int32 LUA_loadResource(EngineImpl* engine, const char* path, const char* type)
        {
            IResourceManagerBase* resManager = engine->GetResourceManager().Get(ResourceType(type));
            if (!resManager)
            {
                return -1;
            }
            
            auto res = resManager->Load(CPath(path));
            ++engine->LastLuaResourceIndex;
            engine->LuaResources.Insert(engine->LastLuaResourceIndex, res);
            
            return engine->LastLuaResourceIndex;
        }
        
        static void LUA_setEntityLocalRotation(Universe* universe, Entity entity, const Quat& rotation)
        {
            if (!entity.IsValid())
            {
                return;
            }
            
            if (!universe->GetParent(entity).IsValid())
            {
                return;
            }
            
            universe->SetLocalRotation(entity, rotation);
        }
        
        static void LUA_setEntityLocalPosition(Universe* universe, Entity entity, const Vec3& position)
        {
            if (!entity.IsValid())
            {
                return;
            }
            
            if (!universe->GetParent(entity).IsValid())
            {
                return;
            }
            
            universe->SetLocalPosition(entity, position);
        }
        
        static void LUA_logError(const char* text)
        {
            GLogError.log("Lua Script") << text;
        }
        
        static void LUA_logInfo(const char* text)
        {
            GLogInfo.log("Lua Script") << text;
        }
        
        static Vec4 LUA_multMatrixVec(const Matrix& m, const Vec4& v)
        {
            return m * v;
        }
        
        static Quat LUA_multQuat(const Quat& a, const Quat& b)
        {
            return a * b;
        }
        
        static int LUA_multVecQuat(lua_State* L)
        {
            Vec3 v = LuaWrapper::checkArg<Vec3>(L, 1);
            Quat q;
            if (LuaWrapper::isType<Quat>(L, 2))
            {
                q = LuaWrapper::checkArg<Quat>(L, 2);
            }
            else
            {
                Vec3 axis = LuaWrapper::checkArg<Vec3>(L, 2);
                float angle = LuaWrapper::checkArg<float>(L, 3);
                
                q = Quat(axis, angle);
            }
            
            Vec3 res = q.Rotate(v);
            
            LuaWrapper::push(L, res);
            return 1;
        }
        
        static Vec3 LUA_getEntityPosition(Universe* universe, Entity entity)
        {
            if (!entity.IsValid())
            {
                GLogWarning.log("Engine") << "Requesting position on invalid entity";
                return Vec3(0, 0, 0);
            }
            return universe->GetPosition(entity);
        }
        
        static Quat LUA_getEntityRotation(Universe* universe, Entity entity)
        {
            if (!entity.IsValid())
            {
                GLogWarning.log("Engine") << "Requesting rotation on invalid entity.";
                return Quat(0, 0, 0, 1);
            }
            return universe->GetRotation(entity);
        }
        
        static void LUA_DestroyEntity(Universe* universe, Entity entity)
        {
            universe->DestroyEntity(entity);
        }
        
        static Vec3 LUA_getEntityDirection(Universe* universe, Entity entity)
        {
            Quat rot = universe->GetRotation(entity);
            return rot.Rotate(Vec3(0, 0, 1));
        }
        
        static void LUA_pause(IEngine* engine, bool pause)
        {
            engine->Pause(pause);
        }
        
        void RegisterLuaAPI();
        
        void InstallLuaPackageLoader() const;
    
        static int LUA_packageLoader(lua_State* L)
        {
            const char* module = LuaWrapper::toType<const char*>(L, 1);
            StaticString<MAX_PATH_LENGTH> tmp(module);
            tmp << ".lua";
            
            lua_getglobal(L, "g_engine");
            auto* engine = (IEngine*)lua_touserdata(L, -1);
            
            lua_pop(L, 1);
            
            auto& fs = engine->GetFileSystem();
            auto* file = fs.Open(fs.GetDefaultDevice(), CPath(tmp), FS::Mode::OPEN_AND_READ);
            if (!file)
            {
                GLogError.log("Engine") << "Failed to open file " << tmp;
                StaticString<MAX_PATH_LENGTH + 40> msg("Failed to open file ");
                msg << tmp;
                lua_pushstring(L, msg);
            }
            else if (luaL_loadbuffer(L, (const char*)file->GetBuffer(), file->GetSize(), tmp) != LUA_OK)
            {
                GLogError.log("Engine") << "Failed to load package " << tmp << ": " << lua_tostring(L, -1);
            }
            
            if (file)
            {
                fs.Close(*file);
            }
            
            return 1;
        }
        
        static void* luaAllocator(void* ud, void* ptr, size_t osize, size_t nsize)
        {
            auto& allocator = *static_cast<IAllocator*>(ud);
            if (nsize == 0)
            {
                allocator.Deallocate(ptr);
                return nullptr;
            }
            if (nsize > 0 && ptr == nullptr)
            {
                return allocator.Allocate(nsize);
            }
            
            void* new_mem = allocator.Allocate(nsize);
            CopyMemory(new_mem, ptr, Math::Minimum(osize, nsize));
            allocator.Deallocate(ptr);
            return new_mem;
        }
        
        static void LUA_UnloadResource(EngineImpl* engine, int resourceIndex)
        {
            engine->UnloadLuaResource(resourceIndex);
        }
        
        static Universe* LUA_CreateUniverse(EngineImpl* engine)
        {
            return &engine->CreateUniverse(false);
        }
        
        static void LUA_DestroyUniverse(EngineImpl* engine, Universe* universe)
        {
            engine->DestroyUniverse(*universe);
        }
        
        static Universe* LUA_getSceneUniverse(IScene* scene)
        {
            return &scene->GetUniverse();
        }

        static IScene* LUA_GetScene(Universe* universe, const char* name)
        {
            uint32 hash = Crc32(name);
            return universe->GetScene(hash);
        }
        
        static int LUA_instantiatePrefab(lua_State* L)
        {
            auto* engine = LuaWrapper::checkArg<EngineImpl*>(L, 1);
            auto* universe = LuaWrapper::checkArg<Universe*>(L, 2);
            Vec3 position = LuaWrapper::checkArg<Vec3>(L, 3);
            int32 prefab_id = LuaWrapper::checkArg<int>(L, 4);
            PrefabResource* prefab = static_cast<PrefabResource*>(engine->GetLuaResource(prefab_id));
            if (!prefab)
            {
                GLogError.log("Editor") << "Cannot instantiate null prefab.";
                return 0;
            }
            if (!prefab->IsReady())
            {
                GLogError.log("Editor") << "Prefab " << prefab->GetPath().c_str() << " is not ready, preload it.";
                return 0;
            }
            
            Entity entity = universe->InstantiatePrefab(*prefab, position, {0, 0, 0, 1}, 1);
            LuaWrapper::push(L, entity);
            return 1;
        }
        
        void UnloadLuaResource(int32 resourceIndex) override;
        
        int32 AddLuaResource(const CPath& path, ResourceType type) override;
        
        NEKO_FORCE_INLINE ResourceBase* GetLuaResource(int32 Index) const override
        {
            auto iter = LuaResources.Find(Index);
            if (iter.IsValid())
            {
                return iter.value();
            }
            return nullptr;
        }
        
        void RunScript(const char* src, int32 sourceLength, const char* path) override;

    private:
        
        IAllocator& Allocator;
        LIFOAllocator LifoAllocator;
        
        FS::FileSystem* pFileSystem;
        FS::MemoryFileDevice* pMemoryFileDevice;
        FS::DiskFileDevice* pDiskFileDevice;
        FS::DiskFileDevice* pPatchFileDevice;
        FS::ResourceFileDevice* pResourceFileDevice;
        
        CPathManager PathManager;
        CResourceManager ResourceManager;
        IPluginManager* PluginManager;
        CPrefabResourceManager PrefabResourceManager;
        
        //! Input system
        IInputSystem* pInputSystem;
        //! Event dispatcher
        CEventDispatcher* EventSystem;
        
        // engine timing
        CTimer MainTimer;
            
        int32 FrameCounter;
                
        float fCurrentFramerate; // fps
        float LastTimeDelta;
                
        bool bIsGameRunning;
        bool bNextFrame;
        
        SPlatformData PlatformData;
                
        // Lua
        lua_State* State;
        THashMap<int32, ResourceBase*> LuaResources;
        int32 LastLuaResourceIndex;
        
    private:
        void operator = (const EngineImpl&);
        EngineImpl(const EngineImpl&);
    };
    
    
    // Error logging
    static FS::CPlatformFile GErrorFile;
    static bool GbIsErrorFileOpened = false;
    
    static void ShowLogInIDE(const char* System, const char* Message)
    {
        Debug::DebugOutput(System);
        Debug::DebugOutput(" : ");
        Debug::DebugOutput(Message);
        Debug::DebugOutput("\n");
    }
    
    static void LogErrorToFile(const char*, const char* Message)
    {
        if (!GbIsErrorFileOpened)
        {
            return; // Ignore
        }
        GErrorFile.Write(Message, StringLength(Message));
        GErrorFile.Flush();
    }
    
    static const ResourceType PREFAB_TYPE("prefab");
    
    static const uint32 SERIALIZED_ENGINE_MAGIC = 0x5f4e454b;
    
#pragma pack(1)
    class SerializedEngineHeader
    {
    public:
        uint32 Magic;
        uint32 Reserved; // for crc
    };
#pragma pack()
    
    
    EngineImpl::EngineImpl(const char* Basepath0, const char* Basepath1, FS::FileSystem* fs, IAllocator& allocator)
    : Allocator(allocator)
    , ResourceManager(Allocator)
    , EventSystem(nullptr)
    , pInputSystem(nullptr)
    , PluginManager(nullptr)
    , PrefabResourceManager(Allocator)
    , PathManager(Allocator)
    , LuaResources(Allocator)
    , fCurrentFramerate(0.f)
    , LastTimeDelta(0)
    , FrameCounter(0)
    , LastLuaResourceIndex(-1)
    , bIsGameRunning(false)
    , bNextFrame(false)
    , LifoAllocator(Allocator, 10 * 1024 * 1024)
    {
        Profiler::SetThreadName("Main");
        MemStack::BeginThread();
        
        InstallUnhandledExceptionHandler();
        
        GGameThreadId = MT::GetCurrentThreadID();
        
        GbIsErrorFileOpened = GErrorFile.Open("error.log", FS::Mode::CREATE_AND_WRITE);
        
        // Bind default log output
        GLogError.GetCallback().Bind<LogErrorToFile>();
        GLogError.GetCallback().Bind<ShowLogInIDE>();
        GLogInfo.GetCallback().Bind<ShowLogInIDE>();
        GLogWarning.GetCallback().Bind<ShowLogInIDE>();
        
        GLogInfo.log("Core") << "Initialising engine framework...";
        memset(&PlatformData, 0, sizeof(PlatformData));;
        
        // Initialize the main timer
        MainTimer.EnableTimer(true);
        
        // Initializes Lua state machine
        RegisterLuaAPI();
        
        // Initialize multithreaded job task manager
        JobSystem::Init(allocator);
        // Initialize file system
        if (!fs)
        {
            pFileSystem = FS::FileSystem::Create(allocator);
            
            pMemoryFileDevice = NEKO_NEW(allocator, FS::MemoryFileDevice)(allocator);
            pDiskFileDevice = NEKO_NEW(allocator, FS::DiskFileDevice)("disk", Basepath0, allocator);
            pResourceFileDevice = NEKO_NEW(allocator, FS::ResourceFileDevice)(allocator);
            
            pFileSystem->Mount(pMemoryFileDevice);
            pFileSystem->Mount(pDiskFileDevice);
            pFileSystem->Mount(pResourceFileDevice);
            
            bool patching = (Basepath1[0] != 0) && !EqualStrings(Basepath0, Basepath1);
            if (patching)
            {
                pPatchFileDevice = NEKO_NEW(allocator, FS::DiskFileDevice)("patch", Basepath1, allocator);
                pFileSystem->Mount(pPatchFileDevice);
                pFileSystem->SetDefaultDevice("memory:patch:disk:resource");
                pFileSystem->SetSaveGameDevice("memory:disk:resource");
            }
            else
            {
                pPatchFileDevice = nullptr;
                pFileSystem->SetDefaultDevice("memory:disk:resource");
                pFileSystem->SetSaveGameDevice("memory:disk:resource");
            }
        }
        else
        {
            pFileSystem = fs;
            pMemoryFileDevice = nullptr;
            pDiskFileDevice = nullptr;
            pPatchFileDevice = nullptr;
            pResourceFileDevice = nullptr;
        }
        
        // Initialize resource managers
        ResourceManager.Create(*pFileSystem);
        PrefabResourceManager.Create(PREFAB_TYPE, ResourceManager);
        // Initialize plugin manager
        PluginManager = IPluginManager::Create(*this);
        
        // Initialize global property register system
        Reflection::Init(allocator);
        // Initialize event manager
        EventSystem = NEKO_NEW(allocator, CEventDispatcher)(*this);
        // Initialise input system
        pInputSystem = IInputSystem::Create(*this);
        
        GLogInfo.log("Core") << "Engine initialised.";
    }
    
    EngineImpl::~EngineImpl()
    {
        GLogInfo.log("Engine") << "Freeing up engine resources";
        
        for (auto* res : LuaResources)
        {
            res->GetResourceManager().Unload(*res);
        }
        
        Reflection::Shutdown();
        
        IPluginManager::Destroy(PluginManager);
        
        if (pInputSystem)
        {
            IInputSystem::Destroy(*pInputSystem);
        }
        
        NEKO_DELETE(Allocator, EventSystem);;
        
        if (pDiskFileDevice)
        {
            FS::FileSystem::Destroy(pFileSystem);
            
            NEKO_DELETE(Allocator, pMemoryFileDevice);
            NEKO_DELETE(Allocator, pDiskFileDevice);
            NEKO_DELETE(Allocator, pPatchFileDevice);
            NEKO_DELETE(Allocator, pResourceFileDevice);
        }
        
        PrefabResourceManager.Destroy();
        ResourceManager.Destroy();
        JobSystem::Shutdown();
        
        lua_close(State);
        
        MemStack::EndThread();
        
        if (GbIsErrorFileOpened)
        {
            GErrorFile.Close();
        }
    }
    
    void EngineImpl::SetPatchPath(const char* path)
    {
        if (!path || path[0] == '\0')
        {
            if (pPatchFileDevice)
            {
                pFileSystem->SetDefaultDevice("memory:disk:resource");
                pFileSystem->Unmount(pPatchFileDevice);
                NEKO_DELETE(Allocator, pPatchFileDevice);
                pPatchFileDevice = nullptr;
            }
            
            return;
        }
        
        if (!pPatchFileDevice)
        {
            pPatchFileDevice = NEKO_NEW(Allocator, FS::DiskFileDevice)("patch", path, Allocator);
            pFileSystem->Mount(pPatchFileDevice);
            pFileSystem->SetDefaultDevice("memory:patch:disk:resource");
            pFileSystem->SetSaveGameDevice("memory:disk:resource");
        }
        else
        {
            pPatchFileDevice->SetBasePath(path);
        }
    }
    
    Universe& EngineImpl::CreateUniverse(bool bSetLuaGlobals)
    {
        auto universe = NEKO_NEW(Allocator, Universe)(Allocator);
        const TArray<IPlugin*>& plugins = PluginManager->GetPlugins();
        for (auto* plugin : plugins)
        {
            plugin->CreateScenes(*universe);
        }
        
        if (bSetLuaGlobals)
        {
            for (auto* scene : universe->GetScenes())
            {
                const char* name = scene->GetPlugin().GetName();
                char tmp[128];
                
                CopyString(tmp, "g_scene_");
                CatString(tmp, name);
                lua_pushlightuserdata(State, scene);
                lua_setglobal(State, tmp);
            }
            lua_pushlightuserdata(State, universe);
            lua_setglobal(State, "g_universe");
        }
        return *universe;
    }
    
    void EngineImpl::DestroyUniverse(Universe& universe)
    {
        auto& scenes = universe.GetScenes();
        for (int32 i = scenes.GetSize() - 1; i >= 0; --i)
        {
            auto* scene = scenes[i];
            scenes.Pop();
            scene->Clear();
            scene->GetPlugin().DestroyScene(scene);
        }
        NEKO_DELETE(Allocator, &universe);
        ResourceManager.RemoveUnreferenced();
    }
    
    void EngineImpl::StartGame(Universe& context)
    {
        assert(!bIsGameRunning);
        bIsGameRunning = true;
        
        for (auto* scene : context.GetScenes())
        {
            scene->StartGame();
        }
        for (auto* plugin : PluginManager->GetPlugins())
        {
            plugin->StartGame();
        }
    }
    
    void EngineImpl::StopGame(Universe& context)
    {
        assert(bIsGameRunning);
        bIsGameRunning = false;
        for (auto* scene : context.GetScenes())
        {
            scene->StopGame();
        }
        for (auto* plugin : PluginManager->GetPlugins())
        {
            plugin->StopGame();
        }
    }
    
    void EngineImpl::Pause(bool pause)
    {
        MainTimer.PauseTimer(ETimer::ETIMER_GAME, pause);
    }
    
    void EngineImpl::NextFrame()
    {
        bNextFrame = true;
    }
    
    void EngineImpl::SetTimeMultiplier(float multiplier)
    {
        MainTimer.SetTimeScale(Math::Maximum(multiplier, 0.001f));
    }
    
    void EngineImpl::SleepIfNeeded()
    {
        //            int32 maxFPS = 0;
        
        // if dedicated..
    }
    
    void EngineImpl::Update(Universe& context)
    {
        PROFILE_FUNCTION();
        
        const int maxFPS = 60;
        float dt;
        {
            const int64 safeMarginMS = 5; // microseconds
            const int64 thresholdMs = (1000 * 1000) / (maxFPS);
            
            static int64 sTimeLast = MainTimer.GetAsyncTime().GetMicroSecondsAsInt64();
            int64 currentTime = MainTimer.GetAsyncTime().GetMicroSecondsAsInt64();
            for (;;)
            {
                const int64 frameTime = currentTime - sTimeLast;
                if (frameTime >= thresholdMs)
                {
                    break;
                }
                if (thresholdMs - frameTime > 10 * 1000)
                {
                    Neko::MT::Sleep(1);
                }
                else
                {
                    Neko::MT::Sleep(0);
                }
                currentTime = MainTimer.GetAsyncTime().GetMicroSecondsAsInt64();
            }
            
            // Update framerate counter
            {
                ++FrameCounter;
                
                static float sNextUpdate = 0.0f;
                float CurrentSeconds = MainTimer.GetAsyncTime().GetSeconds();
                if (CurrentSeconds >= sNextUpdate)
                {
                    fCurrentFramerate = FrameCounter / MainTimer.GetRealFrameTime(); // Using raw frametime (not multiplied by timescale)
                    // Update every second
                    sNextUpdate = Neko::Math::FloorToInt(CurrentSeconds) + 1;
                }
                FrameCounter = 0;
            }
            // @note Already multiplied by timescale
            dt = MainTimer.GetFrameTime();
            
            if (bNextFrame)
            {
                MainTimer.PauseTimer(ETimer::ETIMER_GAME, false);
                dt = 1.0f / 30.0f; // fixed step
            }
            sTimeLast = MainTimer.GetAsyncTime().GetMicroSecondsAsInt64() + safeMarginMS;
        }
        
        MainTimer.UpdateOnFrameStart();
        
        const bool Paused = MainTimer.IsTimerPaused(ETimer::ETIMER_GAME);
        
        LastTimeDelta = dt;
        {
            PROFILE_SECTION("Update scenes");
            for (auto* scene : context.GetScenes())
            {
                scene->Update(dt, Paused);
            }
        }
        {
            PROFILE_SECTION("Late update scenes");
            for (auto* scene : context.GetScenes())
            {
                scene->PostUpdate(dt, Paused);
            }
        }
        
        // update systems
        PluginManager->Update(dt, Paused);
        pInputSystem->Update(dt);
        pFileSystem->UpdateAsyncActions();
        
        // Process system messages and send them to receivers
        EventSystem->Process();
        
        if (bNextFrame)
        {
            MainTimer.PauseTimer(ETimer::ETIMER_GAME, true);
            bNextFrame = false;
        }
    }
    
    // State serialisation
    
    void EngineImpl::SerializerSceneVersions(OutputBlob& serializer, Universe& context)
    {
        serializer.Write(context.GetScenes().GetSize());
        for (auto* scene : context.GetScenes())
        {
            serializer.Write(Crc32(scene->GetPlugin().GetName()));
            serializer.Write(scene->GetVersion());
        }
    }
    
    void EngineImpl::SerializePluginList(OutputBlob& serializer)
    {
        serializer.Write((int32)PluginManager->GetPlugins().GetSize());
        for (auto* plugin : PluginManager->GetPlugins())
        {
            serializer.WriteString(plugin->GetName());
        }
    }
    
    bool EngineImpl::HasSupportedSceneVersions(InputBlob& serializer, Universe& context)
    {
        int32 count;
        serializer.Read(count);
        for (int32 i = 0; i < count; ++i)
        {
            uint32 hash;
            serializer.Read(hash);
            auto* scene = context.GetScene(hash);
            
            int32 version;
            serializer.Read(version);
            if (version != scene->GetVersion())
            {
                GLogError.log("Core") << "Plugin " << scene->GetPlugin().GetName() << " has incompatible version";
                return false;
            }
        }
        return true;
    }
    
    bool EngineImpl::HasSerializedPlugins(InputBlob& serializer)
    {
        int32 count;
        serializer.Read(count);
        for (int32 i = 0; i < count; ++i)
        {
            char tmp[32];
            serializer.ReadString(tmp, sizeof(tmp));
            
            if (!PluginManager->GetPlugin(tmp))
            {
                GLogError.log("Core") << "Missing plugin \"" << tmp << "\"";
                return false;
            }
        }
        return true;
    }
    
    uint32 EngineImpl::Serialize(Universe& context, OutputBlob& serializer)
    {
        SerializedEngineHeader header;
        header.Magic = SERIALIZED_ENGINE_MAGIC;
        header.Reserved = 0;
        serializer.Write(header);
        
        SerializePluginList(serializer);
        SerializerSceneVersions(serializer, context);
        PathManager.Serialize(serializer);
        int32 pos = serializer.GetPos();
        context.SerializeBlob(serializer);
        PluginManager->Serialize(serializer);
        serializer.Write((int32)context.GetScenes().GetSize());
        
        for (auto* scene : context.GetScenes())
        {
            serializer.WriteString(scene->GetPlugin().GetName());
            scene->Serialize(serializer);
        }
        
        uint32 crc = Crc32((const uint8*)serializer.GetData() + pos, serializer.GetPos() - pos);
        return crc;
    }
    
    bool EngineImpl::Deserialize(Universe& ctx, InputBlob& serializer)
    {
        SerializedEngineHeader header;
        serializer.Read(header);
        
        if (header.Magic != SERIALIZED_ENGINE_MAGIC)
        {
            GLogError.log("Core") << "Wrong or corrupted file.";
            return false;
        }
        
        if (!HasSerializedPlugins(serializer))
        {
            return false;
        }
        if (!HasSupportedSceneVersions(serializer, ctx))
        {
            return false;
        }
        
        PathManager.Deserialize(serializer);
        ctx.DeserializeBlob(serializer);
        
        PluginManager->Deserialize(serializer);
        
        int32 sceneCount;
        serializer.Read(sceneCount);
        for (int32 i = 0; i < sceneCount; ++i)
        {
            char sceneName[32];
            serializer.ReadString(sceneName, sizeof(sceneName));
            IScene* scene = ctx.GetScene(Crc32(sceneName));
            scene->Deserialize(serializer);
        }
        
        PathManager.Clear();
        
        return true;
    }
    
    ComponentUID EngineImpl::CreateComponent(Universe& universe, Entity entity, ComponentType type)
    {
        ComponentUID component;
        IScene* scene = universe.GetScene(type);
        if (!scene)
        {
            return ComponentUID::INVALID;
        }
        
        return ComponentUID(entity, type, scene, scene->CreateComponent(type, entity));
    }
    
    void EngineImpl::RegisterLuaAPI()
    {
        State = lua_newstate(luaAllocator, &Allocator);
        luaL_openlibs(State);
        
        lua_pushlightuserdata(State, this);
        lua_setglobal(State, "g_engine");
        
#define REGISTER_FUNCTION(name) \
LuaWrapper::CreateSystemFunction(State, "Engine", #name, \
&LuaWrapper::wrap<decltype(&LUA_##name), LUA_##name>); \

        REGISTER_FUNCTION(CreateUniverse);
        REGISTER_FUNCTION(DestroyUniverse);
        REGISTER_FUNCTION(GetScene);
        REGISTER_FUNCTION(getSceneUniverse);
        REGISTER_FUNCTION(loadResource);
        REGISTER_FUNCTION(UnloadResource);
        REGISTER_FUNCTION(createComponent);
        REGISTER_FUNCTION(CreateEntity);
        REGISTER_FUNCTION(setEntityPosition);
        REGISTER_FUNCTION(getEntityPosition);
        REGISTER_FUNCTION(getEntityDirection);
        REGISTER_FUNCTION(setEntityRotation);
        REGISTER_FUNCTION(getEntityRotation);
        REGISTER_FUNCTION(setEntityLocalRotation);
        REGISTER_FUNCTION(setEntityLocalPosition);
        REGISTER_FUNCTION(logError);
        REGISTER_FUNCTION(logInfo);
        REGISTER_FUNCTION(pause);
        REGISTER_FUNCTION(startGame);
        REGISTER_FUNCTION(hasFilesystemWork);
        REGISTER_FUNCTION(processFilesystemWork);
        REGISTER_FUNCTION(DestroyEntity);
        REGISTER_FUNCTION(GetComponent);
        REGISTER_FUNCTION(GetComponentType);
        REGISTER_FUNCTION(multMatrixVec);
        REGISTER_FUNCTION(multQuat);
        REGISTER_FUNCTION(GetFirstEntity);
        REGISTER_FUNCTION(GetNextEntity);
        
#undef REGISTER_FUNCTION
        
        LuaWrapper::CreateSystemFunction(State, "Engine", "InstantiatePrefab", &LUA_instantiatePrefab);
        LuaWrapper::CreateSystemFunction(State, "Engine", "createEntityEx", &LUA_createEntityEx);
        LuaWrapper::CreateSystemFunction(State, "Engine", "multVecQuat", &LUA_multVecQuat);
        
        //Input
        LuaWrapper::CreateSystemVariable(State, "Engine", "INPUT_DEVICE_KEYBOARD", IInputSystem::Device::Keyboard);
        LuaWrapper::CreateSystemVariable(State, "Engine", "INPUT_DEVICE_MOUSE", IInputSystem::Device::Mouse);
        LuaWrapper::CreateSystemVariable(State, "Engine", "INPUT_DEVICE_CONTROLLER", IInputSystem::Device::Controller);
        
        LuaWrapper::CreateSystemVariable(State, "Engine", "INPUT_EVENT_BUTTON", IInputSystem::InputEvent::Button);
        LuaWrapper::CreateSystemVariable(State, "Engine", "INPUT_EVENT_AXIS", IInputSystem::InputEvent::Axis);
        LuaWrapper::CreateSystemVariable(State, "Engine", "INPUT_EVENT_DEVICE_ADDED", IInputSystem::InputEvent::DeviceAdded);
        LuaWrapper::CreateSystemVariable(State, "Engine", "INPUT_EVENT_DEVICE_REMOVED", IInputSystem::InputEvent::DeviceRemoved);
        
        InstallLuaPackageLoader();
    }
    
    void EngineImpl::InstallLuaPackageLoader() const
    {
        if (lua_getglobal(State, "package") != LUA_TTABLE)
        {
            GLogError.log("Engine") << "Lua \"package\" is not a table";
            return;
        };
        if (lua_getfield(State, -1, "searchers") != LUA_TTABLE)
        {
            GLogError.log("Engine") << "Lua \"package.searchers\" is not a table";
            return;
        }
        int numLoaders = 0;
        lua_pushnil(State);
        while (lua_next(State, -2) != 0)
        {
            lua_pop(State, 1);
            numLoaders++;
        }
        
        lua_pushinteger(State, numLoaders + 1);
        lua_pushcfunction(State, LUA_packageLoader);
        lua_rawset(State, -3);
        lua_pop(State, 2);
    }
    
    void EngineImpl::RunScript(const char* src, int32 sourceLength, const char* path)
    {
        if (luaL_loadbuffer(State, src, sourceLength, path) != LUA_OK)
        {
            GLogError.log("Engine") << path << ": " << lua_tostring(State, -1);
            lua_pop(State, 1);
            return;
        }
        
        if (lua_pcall(State, 0, 0, 0) != LUA_OK)
        {
            GLogError.log("Engine") << path << ": " << lua_tostring(State, -1);
            lua_pop(State, 1);
        }
    }
    
    void EngineImpl::UnloadLuaResource(int32 resourceIndex)
    {
        if (resourceIndex < 0)
        {
            return;
        }
        ResourceBase* res = LuaResources[resourceIndex];
        LuaResources.Erase(resourceIndex);
        res->GetResourceManager().Unload(*res);
    }
    
    int32 EngineImpl::AddLuaResource(const CPath& path, ResourceType type)
    {
        IResourceManagerBase* manager = ResourceManager.Get(type);
        if (!manager)
        {
            return -1;
        }
        ResourceBase* res = manager->Load(path);
        ++LastLuaResourceIndex;
        LuaResources.Insert(LastLuaResourceIndex, res);
        return LastLuaResourceIndex;
    }
            
    
    IEngine* IEngine::Create(const char* BasePath0, const char* BasePath1, FS::FileSystem* fs, IAllocator& allocator)
    {
        return NEKO_NEW(allocator, EngineImpl)(BasePath0, BasePath1, fs, allocator);
    }
    
    void IEngine::Destroy(IEngine* engine, IAllocator& allocator)
    {
        NEKO_DELETE(allocator, engine);
    }

} // ~namespace Neko
