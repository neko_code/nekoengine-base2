//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Engine.h
//  Neko engine
//
//  Copyright © 2013 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

struct lua_State;

namespace Neko
{
    namespace FS
    {
        class DiskFileDevice;
        class ResourceFileDevice;
        class FileSystem;
    }
    
    class IAllocator;
    class IInputSystem;
    class CPath;
    class CPathManager;
    class IPluginManager;
    class CResourceManager;
    class Universe;
    class InputBlob;
    class OutputBlob;
    class CTimer;
    class CEventDispatcher;
    
    struct Vec3;
    struct ComponentUID;
    
    template <typename T> class TArray;
    
    /** Core engine interface. */ 
    class NEKO_ENGINE_API IEngine
    {
    public:
        
        struct SPlatformData
        {
            void* WindowHandle;
            void* Display;
        };
        
    public:
        
        virtual ~IEngine() { }
        
        /** Creates new engine instance. */
        static IEngine* Create(const char* basepath0, const char* basepath1, FS::FileSystem* fileSystem, IAllocator& allocator);
        static void Destroy(IEngine* engine, IAllocator& allocator);
        
        /** Initialises a new universe. */
        virtual Universe& CreateUniverse(bool setLuaGlobals) = 0;
        /** Unloads universe file. */
        virtual void DestroyUniverse(Universe& context) = 0;
        
        /** Sets the patch path in the file system. */
        virtual void SetPatchPath(const char* path) = 0;
        
        virtual FS::FileSystem& GetFileSystem() = 0;
        virtual FS::DiskFileDevice* GetDiskFileDevice() = 0;
        virtual FS::DiskFileDevice* GetPatchFileDevice() = 0;
        virtual FS::ResourceFileDevice* GetResourceFileDevice() = 0;
        
        virtual IInputSystem& GetInputSystem() = 0;
        virtual IPluginManager& GetPluginManager() = 0;
        virtual CResourceManager& GetResourceManager() = 0;
        virtual CPathManager& GetPathManager() = 0;
        virtual IAllocator& GetAllocator() = 0;
        virtual CEventDispatcher& GetEventDispatcher() = 0;
        
        virtual void StartGame(Universe& context) = 0;
        virtual void StopGame(Universe& context) = 0;
        
        virtual void Update(Universe& context) = 0;
        
        virtual uint32 Serialize(Universe& context, OutputBlob& serializer) = 0;
        virtual bool Deserialize(Universe& context, InputBlob& serializer) = 0;
        
        virtual float GetFPS() const = 0;
        virtual CTimer& GetTimer() = 0;
        virtual float GetLastTimeDelta() const = 0;
        virtual void SetTimeMultiplier(float multiplier) = 0;
        virtual void Pause(bool pause) = 0;
        virtual void NextFrame() = 0;
        virtual lua_State* GetState() = 0;
        
        virtual void RunScript(const char* src, int32 sourceLength, const char* path) = 0;
        virtual ComponentUID CreateComponent(Universe& universe, Entity entity, ComponentType type) = 0;

        virtual IAllocator& GetLIFOAllocator() = 0;
        virtual class ResourceBase* GetLuaResource(int32 idx) const = 0;
        virtual int AddLuaResource(const CPath& path, struct ResourceType type) = 0;
        virtual void UnloadLuaResource(int32 resourceIndex) = 0;
        
        virtual void SetPlatformData(const SPlatformData& data) = 0;
        virtual const SPlatformData& GetPlatformData() = 0;

    protected:
        
        IEngine() { }
    };

} // ~namespace Neko
