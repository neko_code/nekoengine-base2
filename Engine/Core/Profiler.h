//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Profiler.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Data/DefaultAllocator.h"
#include "../Containers/DelegateList.h"
#include "../Containers/HashMap.h"
#include "../Mt/Thread.h"
#include "../Neko.h"

namespace Neko
{
	/** 
	 * The profiler reports how much time is spent in different parts of engine. This is very helpful when you want to optimize your game. Parts of the engine you want to track must be marked by following macros: PROFILE_FUNCTION, PROFILE_SECTION. 
     * @note If the PROFILE_INT is called several times per frame, the resulting value is the sum of all calls.
	 */
    namespace Profiler
    {
        struct Section;
        enum class ESectionType
        {
            TIME,
            INT
        };
        
        /**
         * Gets the thread identifier the profiler is used on.
         */
        NEKO_ENGINE_API ThreadID GetThreadID(int32 Index);
        
        /**
         * Sets the current thread name.
         */
        NEKO_ENGINE_API void SetThreadName(const char* Name);
        
        /** 
         * Returns thread name on the given index.
         */
        NEKO_ENGINE_API const char* GetThreadName(ThreadID ThreadId);
        
        /**
         * Returns index of thread as it's stored.
         */
        NEKO_ENGINE_API int32 GetThreadIndex(ThreadID Id);
        
        /**
         * Returns total amount of threads we are profiling.
         */
        NEKO_ENGINE_API int32 GetThreadCount();
        
        /**
         * Returns the time passed since thread has started.
         */
        NEKO_ENGINE_API uint64 now();
        
        /** 
         * Returns the parent section where thread identificator is attached. 
         */
        NEKO_ENGINE_API Section* GetRootBlock(ThreadID ThreadId);
        
        NEKO_ENGINE_API Section* GetCurrentSection();
        
        NEKO_ENGINE_API int32 GetSectionInt(Section* block);
        
        /**
         * Returns the section type.
         * @see ESectionType
         */
        NEKO_ENGINE_API ESectionType GetSectionType(Section* block);
        
        NEKO_ENGINE_API Section* GetSectionFirstChild(Section* block);
        NEKO_ENGINE_API Section* GetSectionNext(Section* block);
        NEKO_ENGINE_API float GetSectionLength(Section* block);
        
        /**
         * Returns the amount of hits for a section.
         */
        NEKO_ENGINE_API int32 GetSectionHitCount(Section* block);
        NEKO_ENGINE_API uint64 GetSectionHitStart(Section* block, int32 HitIndex);
        NEKO_ENGINE_API uint64 GetSectionHitLength(Section* block, int32 HitIndex);
        
        /**
         * Returns name for a profiling section.
         */
        NEKO_ENGINE_API const char* GetSectionName(Section* block);
        
        /** 
         * Records value (e.g. amount of items) for a profiling section.
         * @see PROFILE_INT macro
         */
        NEKO_ENGINE_API void Record(const char* Name, int32 Value);
        
        /** 
         * Registers a new section we will be profiling. 
         * @name Name that will allow you easily identify the thread.
         * @see PROFILE_SECTION macro
         */
        NEKO_ENGINE_API void* BeginSection(const char* Name);
        
        /** 
         * Ends profiling for the current thread.
         * @see PROFILE_SECTION macro
         */
        NEKO_ENGINE_API void* EndSection();
        
        /**
         * Updates the profiler. 
         */
        NEKO_ENGINE_API void Frame();
        
        NEKO_ENGINE_API DelegateList<void ()>& GetFrameListeners();
        
        /** Profiler event block. */
#ifdef DEBUG
        struct Scope
        {
            explicit Scope(const char* Name)
            {
                Ptr = BeginSection(Name);
            }
            ~Scope()
            {
                void* Tmp = EndSection();
                assert(Tmp == Ptr);
            }
            
            const void* Ptr;
        };
#else
        struct Scope
        {
            explicit Scope(const char* Name)
            {
                BeginSection(Name);
            }
            ~Scope()
            {
                EndSection();
            }
        };
#endif
        
    } // namespace Profiler
    
#define PROFILE_INT(name, x) Neko::Profiler::Record((name), (x));
#define PROFILE_FUNCTION() Neko::Profiler::Scope profile_scope(__FUNCTION__);
#define PROFILE_SECTION(name) Neko::Profiler::Scope profile_scope(name);
    
} // namespace Neko
