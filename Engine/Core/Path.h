//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  CPath.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Containers/AssociativeArray.h"
#include "../Mt/Sync.h"

namespace Neko
{
    const uint32 MAX_PATH_LENGTH = 260;
    
    class IAllocator;
    class InputBlob;
    class OutputBlob;
    
    class PathInternal
    {
    public:
        char Path[MAX_PATH_LENGTH];
        uint32 Id;
        volatile int32 ReferenceCount;
    };

    /// Object to represent system paths.
    class NEKO_ENGINE_API CPath
    {
    public:
        
        CPath();
        
        CPath(const CPath& rhs);
        
        explicit CPath(uint32 hash);
        
        explicit CPath(const char* path);
        
        void operator = (const CPath& rhs);
        
        void operator = (const char* rhs);
        
        bool operator == (const CPath& rhs) const
        {
            return Data->Id == rhs.Data->Id;
        }
        
        bool operator != (const CPath& rhs) const
        {
            return Data->Id != rhs.Data->Id;
        }
        
        const char* operator* () const
        {
            return c_str();
        }
        
        ~CPath();
        
        NEKO_FORCE_INLINE uint32 GetHash() const
        {
            return Data->Id;
        }
        
        NEKO_FORCE_INLINE const char* c_str() const
        {
            return Data->Path;
        }
        
        int Length() const;
        NEKO_FORCE_INLINE bool IsValid() const { return Data->Path[0] != '\0'; }
        
    private:
        
        PathInternal* Data;
    };
    
    
    /// System path manager.
    class NEKO_ENGINE_API CPathManager
    {
        friend class CPath;
        
    public:
        
        explicit CPathManager(IAllocator& allocator);
        
        ~CPathManager();
        
        void Serialize(OutputBlob& serializer);
        void Deserialize(InputBlob& serializer);
        
        void Clear();
        
        static const CPath& GetEmptyPath();
    private:
        
        PathInternal* GetPath(uint32 Hash, const char* Path);
        PathInternal* GetPath(uint32 Hash);
        PathInternal* GetPathMultithreadUnsafe(uint32 Hash, const char* Path);
        
        void IncrementRefCount(PathInternal* Path);
        void DecrementRefCount(PathInternal* Path);
        
    private:
        
        IAllocator& Allocator;
        TAssociativeArray<uint32, PathInternal*> Paths;
        MT::SpinMutex Mutex;
        CPath* EmptyPath;
    };
    
} // namespace Neko
