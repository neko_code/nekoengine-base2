//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  PathUtils.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "PathUtils.h"
#include "StringUtil.h"

namespace Neko
{
    namespace PathUtils
    {
        void Normalize(const char* path, char* out, uint32 MaxSize)
        {
            assert(MaxSize > 0);
            uint32 i = 0;
            
            bool is_prev_slash = false;
            
            if (path[0] == '.' && (path[1] == '\\' || path[1] == '/'))
            {
                ++path;
            }
            // fix path slashes
#   if NEKO_WINDOWS_FAMILY
            if (path[0] == '\\' || path[0] == '/')
            {
                ++path;
            }
#   endif
            
            while (*path != '\0' && i < MaxSize)
            {
                bool is_current_slash = *path == '\\' || *path == '/';
                
                if (is_current_slash && is_prev_slash)
                {
                    ++path;
                    continue;
                }
                
                *out = *path == '\\' ? '/' : *path;
                // fix path slashes
#   if NEKO_WINDOWS_FAMILY
                *out = *path >= 'A' && *path <= 'Z' ? *path - 'A' + 'a' : *out;
#   endif
                
                path++;
                out++;
                i++;
                
                is_prev_slash = is_current_slash;
            }
            (i < MaxSize ? *out : *(out - 1)) = '\0';
        }
        
        void GetDirectory(char* dir, int max_length, const char* src)
        {
            CopyString(dir, max_length, src);
            for (int32 i = StringLength(dir) - 1; i >= 0; --i)
            {
                if (dir[i] == '\\' || dir[i] == '/')
                {
                    ++i;
                    dir[i] = '\0';
                    return;
                }
            }
            dir[0] = '\0';
        }
        
        void GetBasename(char* BaseName, int32 MaxLength, const char* Src)
        {
            BaseName[0] = '\0';
            for (int32 i = StringLength(Src) - 1; i >= 0; --i)
            {
                if (Src[i] == '\\' || Src[i] == '/' || i == 0)
                {
                    if (Src[i] == '\\' || Src[i] == '/')
                    {
                        ++i;
                    }
                    
                    int32 j = 0;
                    BaseName[j] = Src[i];
                    while (j < MaxLength - 1 && Src[i + j] && Src[i + j] != '.')
                    {
                        ++j;
                        BaseName[j] = Src[j + i];
                    }
                    BaseName[j] = '\0';
                    return;
                }
            }
        }
        
        void GetFilename(char* filename, int max_length, const char* src)
        {
            for (int i = StringLength(src) - 1; i >= 0; --i)
            {
                if (src[i] == '\\' || src[i] == '/')
                {
                    ++i;
                    CopyString(filename, max_length, src + i);
                    break;
                }
            }
        }
        
        void GetExtension(char* extension, int max_length, const char* src)
        {
            assert(max_length > 0);
            for (int i = StringLength(src) - 1; i >= 0; --i)
            {
                if (src[i] == '.')
                {
                    ++i;
                    CopyString(extension, max_length, src + i);
                    return;
                }
            }
            extension[0] = '\0';
        }
        
        bool HasExtension(const char* filename, const char* ext)
        {
            char tmp[20];
            GetExtension(tmp, sizeof(tmp), filename);
            MakeLowercase(tmp, lengthOf(tmp), tmp);
            
            return EqualStrings(tmp, ext);
        }
        
        bool IsAbsolute(const char* path)
        {
            return path[0] != '\0' && path[1] == ':';
        }
        
        void RemoveExtension(Text& filepath)
        {
            const char* szFilepath = filepath.c_str();
            for (const char* p = szFilepath + filepath.Length() - 1; p >= szFilepath; --p)
            {
                switch (*p)
                {
                    case ':':
                    case '/':
                    case '\\':
                        // we've reached a path separator - it means there's no extension in this name
                        return;
                    case '.':
                        // there's an extension in this file name
                        int32 Place = static_cast<int32>(p - szFilepath);
                        int32 remaining = filepath.Length() - Place;
                        filepath.Erase(Place, remaining);
                        return;
                }
            }
            // it seems the file name is a pure name, without extension
        }
        
        //! Remove extension for given file.
        void RemoveExtension(char* szFilePath)
        {
            for (char* p = szFilePath + strlen(szFilePath) - 1; p >= szFilePath; --p)
            {
                switch (*p)
                {
                    case ':':
                    case '/':
                    case '\\':
                        // we've reached a path separator - it means there's no extension in this name
                        return;
                    case '.':
                        // there's an extension in this file name
                        *p = '\0';
                        return;
                }
            }
            // it seems the file name is a pure name, without extension
        }
        
        //! Replace extension for given file.
        void ReplaceExtension(Text& filepath, const char* szExtension)
        {
            assert(szExtension != nullptr);
            
            if (szExtension != nullptr)
            {
                RemoveExtension(filepath);
                if (szExtension[0] != 0 && szExtension[0] != '.')
                {
                    filepath.Append(".");
                }
                filepath.Append(szExtension);
            }
        }
    }
}
