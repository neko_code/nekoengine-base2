//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  InputSystem.cpp
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#include "../Containers/AssociativeArray.h"
#include "../Core/InputSystem.h"
#include "../Core/Profiler.h"
#include "../Core/LuaWrapper.h"
#include "../Core/Joystick.h"
#include "../Core/Log.h"
#include "../Math/Vec.h"
#include "../Platform/Application.h"
#include "../Utilities/StringUtil.h"
#include "../Utilities/Utilities.h"
#include "../Utilities/Timer.h"
#include "../Core/Guid.h"

// UI

#define INERTIAL_GESTURE_DURATION				0.95f // In seconds

#define SCROLL_RESISTANCE_COEFFICIENT			80.0f

#define MIN_GESTURE_VELOCITY                    -5.0f
#define MAX_GESTURE_VELOCITY                    5.0f

// Controllers

// Xinput constants, taken from Micro$oft xinput.h
#if !defined(XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
#   define XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE   7849
#endif
#if !defined(XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
#   define XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE  8689
#endif
#if !defined(XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
#   define XINPUT_GAMEPAD_TRIGGER_THRESHOLD     30
#endif

namespace Neko
{
    // Controller item names.
    static const char* DefaultControllerItemNames[7] =
    {
        "leftx",
        "lefty",
        
        "rightx",
        "righty",
        
        "lefttrigger",
        "righttrigger",
        
        nullptr
    };
    
    // Controller button names.
    static const char* DefaultControllerButtonNames[16] =
    {
        "a",
        "b",
        "x",
        "y",
        
        "back",
        "guide",
        "start",
        
        "leftstick",
        "rightstick",
        
        "leftshoulder",
        "rightshoulder",
        
        "dpup",
        "dpdown",
        "dpleft",
        "dpright",
        
        nullptr
    };

    /**
     *  String guid to guid object.
     *  @todo ToGuid() method for Guid object.
     */
    SGuid GetGuidFromString(const char* string);
    
    /**  Axis string to enum item. */
    EGameControllerAxis GetAxisFromString(const char* string);
    /**  Button string to enum item. */
    EGameControllerButton GetButtonFromString(const char* string);
    /**  Parse game controller button. */
    void GameControllerParseButton(const char* gameButtonString, const char* joystickString, SControllerMapping* mapping);
    /**  Parse controller mapping.*/
    static void GameControllerParseControllerConfigString(SControllerMapping* mapping, const char* dataString);
    /**  Load a button mapping. */
    void LoadButtonMapping(SControllerMapping* mapping, SGuid Guid, const char* mappingName, const char* mappingString);
    /**  Look for guid string in mapping data. */
    char* GetControllerGuidFromMappingString(const char* mapping);
    /**  Name info from mapping data. */
    char* GetControllerNameFromMappingString(const char* mapping);
    /**  Button info from mapping data. */
    char* GetControllerMappingFromMappingString(const char* mapping);
    
    /// Gamepad controller device.
    class CGameControllerDevice : public IInputSystem::Device
    {
    public:
        
        CGameControllerDevice(IInputSystem& System);
        
        virtual ~CGameControllerDevice();
        
        /** Creates the new game controller instance at the given index. */
        bool OpenController(const int32 DeviceIndex);
        
        /**  Refreshes game controller mapping. */
        void RefreshMappingsForAllControllers(SControllerMappingItem* ControllerMapping);
        
        /** Add mapping from GUID. */
        SControllerMappingItem* AddMappingForGuid(SGuid guid, const char* mappingString, bool* outExisting);
        
        /**  Find mappings for specified guid. */
        SControllerMappingItem* GetControllerMappingForGuid(SGuid* guid);
        
        /** Add a mapping entry. */
        bool GameControllerAddMapping(const char* mappingString);
        
        /** Gets joystick mappings. */
        SControllerMappingItem* GetControllerMapping(const int32 deviceIndex, SGuid guid, const char* name);
        
        /** Checks if the controller at index exists and returns it. If there are none returns null. */
        CGamepadController* CheckControllerExistance(const int32 deviceIndex);
        
        /** Filters axis, checks for deadzones and inverts input if neccesary. */
        bool FilterAxisMotion(EGameControllerAxis axis, int32 previous, int32* value);

        bool IsGameController(const int32 deviceIndex);
        
        void OnSystemEvent(const Event* inEvent);
        
    private:
        
        void OnConnection(const Event* event);
        
        void ProcessGamepadAxes(const Event* event);
        
        void ProcessGamepadButtons(const Event* event);
        
        void ProcessGamepadButton(CGamepadController* Controller, const uint8 Button, uint8 Value);
        
        virtual void Update(float DeltaTime) override { };
        
        virtual const char* GetName() const override { return "Controller"; }
        
        TArray< CGamepadController* > Controllers;
        
        IInputSystem& InputSystem;
        
        //! Deadzone values for each axis.
        int32 Deadzone[(uint32)EGameControllerAxis::Max];
        //! Determines if axis input should be inverted (values are -1 or 1).
        int8 Flip[(uint32)EGameControllerAxis::Max];
        
        SControllerMappingItem*     SupportedControllers;
        SControllerMappingItem*     XInputMapping;
    };

    CGameControllerDevice::CGameControllerDevice(IInputSystem& System)
    : InputSystem(System)
    , Controllers(System.GetAllocator())
    , SupportedControllers(nullptr)
    , XInputMapping(nullptr)
    {
        Deadzone[(int32)EGameControllerAxis::LeftX] =
        Deadzone[(int32)EGameControllerAxis::LeftY] = XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;
        Deadzone[(int32)EGameControllerAxis::RightX] =
        Deadzone[(int32)EGameControllerAxis::RightY] = XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;
        Deadzone[(int32)EGameControllerAxis::TriggerLeft] =
        Deadzone[(int32)EGameControllerAxis::TriggerRight] = XINPUT_GAMEPAD_TRIGGER_THRESHOLD;
        
        memset(Flip, 1, sizeof(Flip));
        Flip[(int32)EGameControllerAxis::LeftY] = -1;
        Flip[(int32)EGameControllerAxis::RightY] = 1;
        
        // Load controller mappings
        int32 index = 0;
        const char* mappingString = nullptr;
        mappingString = GControllerMappings[index];
        while (mappingString)
        {
            GameControllerAddMapping(mappingString);
            
            index++;
            mappingString = GControllerMappings[index];
        }
    }
    
    CGameControllerDevice::~CGameControllerDevice()
    {
        SControllerMappingItem* pControllerMap = nullptr;
        
        if (Controllers.GetSize())
        {
            Controllers.Clear();
        }
        
        // Clear mappings
        while (SupportedControllers)
        {
            pControllerMap = SupportedControllers;
            SupportedControllers = SupportedControllers->Next;
            
            ::free(pControllerMap->Name);
            ::free(pControllerMap->Mapping);
            ::free(pControllerMap);
        }
    }
    
    // Axis can be emulated as a button, so move out the code to keep away from a mess
    void CGameControllerDevice::ProcessGamepadButton(CGamepadController* Controller, const uint8 Button, uint8 Value)
    {
        uint8 RemappedButton = 0;
        uint8 State;
        
        State = Value;
        if (Controller->Mapping.controllerButton[Button] >= static_cast<EGameControllerButton>(0))
        {
            RemappedButton = (uint8)Controller->Mapping.controllerButton[Button];
        }
        else if (Controller->Mapping.controllerButtonAxis[Button] >= static_cast<EGameControllerAxis>(0))
        {
            RemappedButton = (uint8)Controller->Mapping.controllerButtonAxis[Button];
            State = (Value > 0) ? 255 : 0;
        }
        Controller->FilteredButtons[RemappedButton] = State;
        
        // Create event
        IInputSystem::InputEvent event;
        event.device = this;
        event.type = IInputSystem::InputEvent::Button;
        event.data.button.KeyId = RemappedButton;
        event.data.button.state = State != 0 ? IInputSystem::ButtonEvent::DOWN : IInputSystem::ButtonEvent::UP;
        InputSystem.InjectEvent(event);
    };

    void CGameControllerDevice::ProcessGamepadAxes(const Event* event)
    {
        auto* ev = static_cast<const GamepadAxisEvent*>(event);
        int8 Axis = 0;
        
        for (auto Controller : Controllers)
        {
            if (Controller->GetJoystick()->GetInstanceID() == ev->Instance)
            {
                // Set raw values
                Controller->GetJoystick()->OnJoystickAxisAction(ev->Axis, ev->Value);
                
                if (!Controller->HasMapping())
                {
                    continue;
                }
                // Check if controller has actual axes or emulate buttons
                if (Controller->Mapping.controllerAxes[(uint32)ev->Axis] >= (EGameControllerAxis)0)
                {
                    auto AxisMapped = Controller->Mapping.controllerAxes[ev->Axis];
                    int16 ValueChanged = ev->Value;
                    
                    // Axis buttons.
                    switch (AxisMapped)
                    {
                        case EGameControllerAxis::TriggerLeft:
                        case EGameControllerAxis::TriggerRight:
                        {
                            ValueChanged = ValueChanged / 2 + 16384;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    
                    Axis = (uint8)AxisMapped;
                    int32 PrevValue = int32(ValueChanged);
                    FilterAxisMotion(AxisMapped, ValueChanged, &PrevValue);
                    {
                        Controller->FilteredAxes[Axis] = PrevValue;
                        //Send event
                        IInputSystem::InputEvent event;
                        event.device = this;
                        event.type = IInputSystem::InputEvent::Axis;
                        event.data.axis.AbsX = Neko::Math::ShortToNormalizedFloat(PrevValue);
                        event.data.axis.axis = (AxisMapped == EGameControllerAxis::TriggerLeft) ? IInputSystem::AxisEvent::LTRIGGER : IInputSystem::AxisEvent::RTRIGGER;
                        InputSystem.InjectEvent(event);
                    }
                }
                else if (Controller->Mapping.controllerAxisButton[(uint32)ev->Axis] >= (EGameControllerButton)0)
                {
                    // Emulate button
                    int8 Button = (int8)Controller->Mapping.controllerAxisButton[ev->Axis];
                    uint8 Value = (Math::abs(ev->Value) > 32768 / 2) ? 1 : 0;
                    Controller->GetJoystick()->OnJoystickButtonAction(Button, Value);
                    ProcessGamepadButton(Controller, Button, Value);
                }
            }
        }
    }
    
    void CGameControllerDevice::ProcessGamepadButtons(const Event* event)
    {
        auto ev = static_cast<const GamepadButtonEvent*>(event);
        
        for (auto Controller : Controllers)
        {
            if (Controller->JoystickHandle->GetInstanceID() == ev->Instance)
            {
                Controller->JoystickHandle->OnJoystickButtonAction(ev->Button, ev->Value);
                ProcessGamepadButton(Controller, ev->Button, ev->Value);
            }
        }
    }
    
    static void Recenter(CGamepadController* Controller)
    {
        int32 i;
        CGamepad* Joystick = Controller->GetJoystick();
        
        // Reset states.
        for (i = 0; i < Joystick->AxesNum; ++i)
        {
            Joystick->OnJoystickAxisAction(i, 0);
        }
        
        for (i = 0; i < Joystick->ButtonNum; ++i)
        {
            Joystick->OnJoystickButtonAction(i, 0);
        }
        
        for (i = 0; i < Joystick->HatsNum; ++i)
        {
            Joystick->OnJoystickHatAction(i, GAMEPAD_HAT_CENTERED);
        }
    };
    
    void CGameControllerDevice::OnConnection(const Event* event)
    {
        const GamepadEvent* ev = static_cast<const GamepadEvent*>(event);
        
        // Type of event
        const bool Connected = ev->Connected;
        if (Connected)
        {
            // Find the controller mapping
            auto SupportedController = GetControllerMapping(ev->DeviceIndex, ev->Info.Guid, ev->Info.Name);
            SControllerMapping Mapping;
            if (SupportedController == nullptr)
            {
                GLogWarning.log("Engine") << "Couldn't find mappings for " << ev->Info.Name;
            }
            else
            {
                LoadButtonMapping(&Mapping, SupportedController->Guid, SupportedController->Name, SupportedController->Mapping);
            }
            
            // Create new game controller
            auto NewController = NEKO_NEW(InputSystem.GetAllocator(), CGamepadController)(InputSystem.GetAllocator());
            NewController->OpenController(ev->DeviceIndex, ev->Info, SupportedController ? &Mapping : nullptr);
            
            Controllers.Push(NewController);
            
            const char* IdentifiedName = SupportedController ? NewController->GetName() : *ev->Info.Name;
            GLogInfo.log("Engine") << "Controller " << IdentifiedName << " connected.";
        }
        else
        {
            auto Controller = CheckControllerExistance(ev->Info.InstanceId);
            
            if (Controller)
            {
                // Reset first
                Recenter(Controller);
                
                Controller->CloseController();
                Controllers.EraseItem(Controller);
                NEKO_DELETE(InputSystem.GetAllocator(), Controller);
                GLogInfo.log("Engine") << "Device " << ev->Info.Name << " disconnected.";
            }
        }
        
        DebugLog("%d controller devices connected.\n", Controllers.GetSize());
    }
    
    void CGameControllerDevice::OnSystemEvent(const Event* event)
    {
        switch (event->Type)
        {
            case Event::JoyGamepad: { OnConnection(event); break; }
            case Event::JoyAxisMotion: { ProcessGamepadAxes(event); break; }
            case Event::JoyButton: { ProcessGamepadButtons(event); break; }
                
            default: break;
        }
    }
    
    void CGameControllerDevice::RefreshMappingsForAllControllers(SControllerMappingItem* controllerMapping)
    {
        for (auto Controller : Controllers)
        {
            if (!memcmp(&Controller->Mapping.Guid, &controllerMapping->Guid, sizeof(controllerMapping->Guid)))
            {
                LoadButtonMapping(&Controller->Mapping, controllerMapping->Guid, controllerMapping->Name, controllerMapping->Mapping);
            }
        }
    }
    
    SControllerMappingItem* CGameControllerDevice::AddMappingForGuid(SGuid guid, const char* MappingString, bool* bExisting)
    {
        char* mappingName = nullptr;
        char* mappingStr = nullptr;
        SControllerMappingItem* controllerMapping = nullptr;
        
        mappingName = GetControllerNameFromMappingString(MappingString);
        if (mappingName == nullptr)
        {
            DebugLog("Couldn't parse name from \"%s\"\n", MappingString);
            
            return nullptr;
        }
        
        mappingStr = GetControllerMappingFromMappingString(MappingString);
        if (mappingStr == nullptr)
        {
            free(mappingName);
            
            DebugLog("Couldn't parse \"%s\"\n", MappingString);
            return nullptr;
        }
        
        controllerMapping = GetControllerMappingForGuid(&guid);
        if (controllerMapping != nullptr)
        {
            free(controllerMapping->Name);
            controllerMapping->Name = mappingName;
            
            free(controllerMapping->Mapping);
            controllerMapping->Mapping = mappingStr;
            
            RefreshMappingsForAllControllers(controllerMapping);
            *bExisting = true;
        }
        else
        {
            controllerMapping = (SControllerMappingItem*)malloc(sizeof(*controllerMapping));
            if (controllerMapping == nullptr)
            {
                free(mappingName);
                free(mappingStr);
                
                return nullptr;
            }
            
            controllerMapping->Guid = guid;
            controllerMapping->Name = mappingName;
            controllerMapping->Mapping = mappingStr;
            controllerMapping->Next = SupportedControllers;
            
            SupportedControllers = controllerMapping;
            *bExisting = false;
        }
        
        return controllerMapping;
    }
    
    CGamepadController* CGameControllerDevice::CheckControllerExistance(const int32 InstanceId)
    {
        if (Controllers.GetSize() == 0)
        {
            return nullptr;
        }
        
        for (auto Controller : Controllers)
        {
            if (InstanceId == Controller->JoystickHandle->GetInstanceID())
            {
                return Controller;
            }
        }
        
        return nullptr;
    }
    
    SControllerMappingItem* CGameControllerDevice::GetControllerMappingForGuid(SGuid* guid)
    {
        SControllerMappingItem* pSupportedController = SupportedControllers;
        
        while (pSupportedController != nullptr)
        {
            // See if we found what we need
            if (::memcmp(guid, &pSupportedController->Guid, sizeof(*guid)) == 0)
            {
                return pSupportedController;
            }
            
            pSupportedController = pSupportedController->Next;
        }
        
        return nullptr;
    }
    
    bool CGameControllerDevice::GameControllerAddMapping(const char* mappingString)
    {
        if (mappingString == nullptr)
        {
            GLogError.log("Engine") << "GameControllerAddMapping: Empty mapping string.";
            return false;
        }
        
        char* guidStr = GetControllerGuidFromMappingString(mappingString);
        if (guidStr == nullptr)
        {
            GLogError.log("Engine") << "GameControllerAddMapping: Couldn't parse guid from " << mappingString;
            return false;
        }
        
        // Check if its Xinput.
        bool xinput = EqualIStrings(guidStr, "xinput");
        
        SGuid guid = GetGuidFromString(guidStr);
        free(guidStr);
        
        bool exists = false;
        auto* controllerMapping = AddMappingForGuid(guid, mappingString, &exists);
        
        if (xinput)
        {
            GLogError.log("Engine") << "GameControllerAddMapping: XInput mapping.";
            XInputMapping = controllerMapping;
        }
        
        return exists;
    }
    
    SControllerMappingItem* CGameControllerDevice::GetControllerMapping(const int32 DeviceIndex, SGuid Guid, const char* Name)
    {
        auto* Mapping = GetControllerMappingForGuid(&Guid);
        
#if NEKO_JOYSTICK_XINPUT
        if (!Mapping)//&& IsXInput(DeviceIndex))
        {
            Mapping = XInputMapping;
        }
#endif
        
#if NEKO_LINUX_FAMILY
        if (Mapping && Name)
        {
            if (strstr(Name, kXbox360Wireless))
            {
                bool Exists;
                Mapping = AddMappingForGuid(Guid, kLinuxXboxStr, &Exists);
            }
        }
#endif
        
        if (Mapping == nullptr && Name != nullptr)
        {
            // Check if the name contains "Xbox" (doesn't matter what controller it is, it's still Xinput).
            if (Neko::FindSubstring(Name, "Xbox") || Neko::FindSubstring(Name, "X-Box"))
            {
                Mapping = XInputMapping;
            }
        }
        
        return Mapping;
    }
    
    bool CGameControllerDevice::FilterAxisMotion(EGameControllerAxis Axis, int32 Previous, int32* Value)
    {
        const int32 deadzone = Deadzone[(int32)Axis];
        
        int32 value = *Value;
        value = (value > deadzone) || (value < -deadzone) ? value : 0;
        *Value = value * Flip[(int32)Axis];
        
        return Previous != value;
    }
    
    bool CGameControllerDevice::IsGameController(int32 DeviceIndex)
    {
        const auto controller = Controllers[DeviceIndex];
        auto supportedController = GetControllerMapping(DeviceIndex, controller->JoystickHandle->Info.Guid, controller->JoystickHandle->GetJoystickName());
        if (supportedController != nullptr)
        {
            return true;
        }
        
        return false;
    }
    
    /// Mouse device.
    struct MouseDevice : IInputSystem::Device
    {
        void Update(float dt) override { }
        const char* GetName() const override { return "mouse"; }
    };
    
    /// Keyboard device.
    struct KeyboardDevice : IInputSystem::Device
    {
        void Update(float dt) override { }
        const char* GetName() const override { return "keyboard"; }
    };
    
    //! Keyboard keymap is accessible on every platform interface
    struct KeymapBase GKeymapBase;

    /// Input system implementation.
    struct InputSystemImpl final : public IInputSystem
    {
	public:
		
		InputSystemImpl(IEngine& Engine);
        
        virtual ~InputSystemImpl();
		
		virtual void Update(float DeltaTime) override;

		virtual void SetRelativeMouseMode(bool bEnabled) override;
		
		virtual void SetCursorShow(bool bShown) override;
		
		virtual NEKO_FORCE_INLINE bool IsRelativeMouseMode() override { return MainApplication::Get().bMouseRelative; }
		
        virtual NEKO_FORCE_INLINE float GetMouseXMove() const override { return MouseRelPos.x; }
		
        virtual NEKO_FORCE_INLINE float GetMouseYMove() const override { return MouseRelPos.y; }
		
        virtual NEKO_FORCE_INLINE Vec2 GetMousePos() const override { return MousePos; }

        virtual NEKO_FORCE_INLINE bool GetMouseButtonState(EMouseButtons Button) const override { return MouseButtonStates[Button]; }
		
        virtual NEKO_FORCE_INLINE void SetMouseButtonState(EMouseButtons Button, bool const bIsDown) override { MouseButtonStates[Button] = bIsDown; }
        
        virtual NEKO_FORCE_INLINE uint16 GetModifiersState() const override { return ModifierState; }
		
        virtual NEKO_FORCE_INLINE void SetKeyState(EScancode Key, bool Down) override { Keystate[Key] = Down ? 1 : 0; }
		
        virtual NEKO_FORCE_INLINE bool GetKeyState(EScancode Key) const override { return Keystate[Key] == 1 ? true : false; }
        
        virtual NEKO_FORCE_INLINE IAllocator& GetAllocator() const override { return Allocator; }
        
        virtual NEKO_FORCE_INLINE int GetDevicesCount() const override { return Devices.GetSize(); }
        virtual NEKO_FORCE_INLINE Device* GetDevice(int index) override { return Devices[index]; }
        virtual NEKO_FORCE_INLINE Device* GetMouseDevice() override { return pMouseDevice; }
        virtual NEKO_FORCE_INLINE Device* GetKeyboardDevice() override { return pKeyboardDevice; }
        virtual NEKO_FORCE_INLINE int GetEventsCount() const override { return Events.GetSize(); }
        virtual NEKO_FORCE_INLINE const InputEvent* GetEvents() const override { return Events.IsEmpty() ? nullptr : &Events[0]; }
		
		virtual void Enable(bool enabled) override;
		
		virtual void ProcessMouse(int32 X, int32 Y, int32 RelativeX, int32 RelativeY) override;
		
		virtual void OnTouchEvent(ETouchType TouchType, ETouchGestureType GestureType, const Neko::Vec2 Delta) override;
        
		virtual void OnKeyEvent(EScancode Code, bool Down) override;
		
        /** Resets the input. */
		virtual void Reset() override;
		
        /** Returns postprocessed mouse scroll value (kinematics applied). */
		NEKO_FORCE_INLINE virtual Vec2 GetMouseScrollValue() override { return CurrentMouseMove; }
		
        virtual void AddDevice(Device* device) override;
        virtual void RemoveDevice(Device* device) override;
		
		NEKO_FORCE_INLINE IEngine& GetEngine() { return Engine; }
        
    private:
        
        void OnSystemEvent(const Event* inEvent);
        void RegisterLuaAPI();
        
	private:
		
        CGameControllerDevice* pGamepadDevice;
        MouseDevice* pMouseDevice;
        KeyboardDevice* pKeyboardDevice;
        
        void InjectEvent(const InputEvent& event) override
        {
            Events.Push(event);
        }
        
        TArray< InputEvent > Events;
        TArray< Device* > Devices;
        TArray< Device* > DevicesToRemove;
        
		//! True if input is able to process events.
        bool bIsEnabled;
		
		//! Cursor scroll info
		Vec2 CurrentMouseMove;
        
		//! Amount of scroll calculated.
		Vec2 ScrollPosition;
		//! True if physically scrolling.
		bool bScrolling;
		//! Last scroll velocity.
		Vec2 ScrollVelocity;
		//! Time when gesture events were ended by user.
		float fTimeTouchPhaseEnded;
        
		
        Vec2 InjectedMouseRelPos;
        Vec2 MousePos;
        Vec2 MouseRelPos;
		
		//! Current keyboard modifiers.
        uint16 ModifierState;
		//! Keyboard key states.
        uint8 Keystate[SC_COUNT];
        
        //! Mouse button state.
        bool MouseButtonStates[MouseButton_Count];
		
		//! True if cursor is shown.
		bool bCursorShown : 1;
	
		IEngine& Engine;
        IAllocator& Allocator;
    };
    
	InputSystemImpl::InputSystemImpl(IEngine& engine)
	: Engine(engine)
	, MouseRelPos(0, 0)
	, InjectedMouseRelPos(0, 0)
    , Allocator(engine.GetAllocator())
	, Events(engine.GetAllocator())
    , Devices(engine.GetAllocator())
    , DevicesToRemove(engine.GetAllocator())
	, bIsEnabled(false)
	, bCursorShown(true)
	, ScrollVelocity(0.0f)
	, ScrollPosition(0.0f)
	, CurrentMouseMove(0.0f)
	, bScrolling(false)
	, fTimeTouchPhaseEnded(0.0f)
	{
		Reset();
        
        pMouseDevice = NEKO_NEW(Allocator, MouseDevice);
        pMouseDevice->type = Device::Mouse;
        pKeyboardDevice = NEKO_NEW(Allocator, KeyboardDevice);
        pKeyboardDevice->type = Device::Keyboard;
        pGamepadDevice = NEKO_NEW(Allocator, CGameControllerDevice)(*this);
        pGamepadDevice->type = Device::Controller;
        Devices.Push(pKeyboardDevice);
        Devices.Push(pMouseDevice);
        Devices.Push(pGamepadDevice);
        
        Engine.GetEventDispatcher().RegisterListener<InputSystemImpl, &InputSystemImpl::OnSystemEvent>(this);
        
        RegisterLuaAPI();
	}
    
    InputSystemImpl::~InputSystemImpl()
    {
        for (Device* device : Devices)
        {
            NEKO_DELETE(Allocator, device);
        }
        Engine.GetEventDispatcher().UnregisterListener<InputSystemImpl, &InputSystemImpl::OnSystemEvent>(this);
    }
    
    void InputSystemImpl::AddDevice(Device* device)
    {
        Devices.Push(device);
        InputEvent event;
        event.type = InputEvent::DeviceAdded;
        event.device = device;
        InjectEvent(event);
    }
    
    void InputSystemImpl::RemoveDevice(Device* device)
    {
        assert(device != pKeyboardDevice);
        assert(device != pMouseDevice);
        DevicesToRemove.Push(device);
        
        InputEvent event;
        event.type = InputEvent::DeviceRemoved;
        event.device = device;
        InjectEvent(event);
    }
    
    // this method retrieves system events and parses them for further use
    void InputSystemImpl::OnSystemEvent(const Event* inEvent)
    {
        switch (inEvent->Type)
        {
            case Event::JoyGamepad:
            case Event::JoyAxisMotion:
            case Event::JoyButton:
            {
                pGamepadDevice->OnSystemEvent(inEvent);
                break;
            }
            case Event::Touch:
            {
                auto touch = static_cast<const Neko::TouchEvent*>(inEvent);
                
                OnTouchEvent(touch->TouchType, touch->Type, touch->Delta);
                break;
            }
            case Event::Key:
            {
                auto key = static_cast<const KeyEvent*>(inEvent);
                
                InputEvent event;
                event.type = InputEvent::Button;
                event.device = GetMouseDevice();
                event.data.button.KeyId = key->Key;
                event.data.button.state = ButtonEvent::UP;
//                event.data.button.AbsX = rel_mp.x;
//                event.data.button.AbsY = rel_mp.y;
                InjectEvent(event);
                
                OnKeyEvent(key->Key, key->bDown);
                break;
            }
            case Event::Mouse:
            {
                auto mouse = static_cast<const MouseEvent*>(inEvent);
                
                InputEvent event;
                event.type = InputEvent::Axis;
                event.device = GetMouseDevice();
                event.data.axis.AbsX = mouse->MouseX;
                event.data.axis.AbsY = mouse->MouseY;
                event.data.axis.x = (float)mouse->RelativeX;
                event.data.axis.y = (float)mouse->RelativeY;
                InjectEvent(event);
                
                const bool bMouseMoved = (mouse->Flags & MouseEvent::MOUSE_FLAG_MOVED) != 0;
                ProcessMouse(mouse->MouseX, mouse->MouseY, mouse->RelativeX, mouse->RelativeY);
                if (!bMouseMoved)
                {
                    const bool bMouseKeyDown = (mouse->Flags & MouseEvent::MOUSE_FLAG_DOWN) != 0;
                    SetMouseButtonState(mouse->Button, bMouseKeyDown);
                }
                break;
            }
            default: break;
        }
    }
    
	void InputSystemImpl::Update(float DeltaTime)
	{
        PROFILE_SECTION("Input update");
		
        for (auto device : DevicesToRemove)
        {
            Devices.EraseItem(device);
            NEKO_DELETE(Allocator, device);
        }
        
        Events.Clear();
        
        for (auto device : Devices)
        {
            device->Update(DeltaTime);
        }
        
		MouseRelPos = InjectedMouseRelPos;
		InjectedMouseRelPos = { 0, 0 };

		// Update scroll kinematic (inertial moves)
		if (ScrollVelocity.y != 0.0f)
		{
			// Slow down over time
			float Time = Engine.GetTimer().GetCurrTime();

			float TimeSinceTouch = (Time - fTimeTouchPhaseEnded) / INERTIAL_GESTURE_DURATION; // in seconds
			
			float FrameVelocity = Neko::Math::Clamp(Neko::Math::Lerp(ScrollVelocity.y, 0.0f, TimeSinceTouch), MIN_GESTURE_VELOCITY, MAX_GESTURE_VELOCITY);
            
            ScrollPosition.y += (FrameVelocity * Engine.GetTimer().GetFrameStartTime().GetSeconds());
			
			// We finish after the time is up.
			if (TimeSinceTouch >= INERTIAL_GESTURE_DURATION)
			{
				ScrollVelocity.y = 0.0f;
			}
            
//            SpringToEdge(); // @todo Move to UI lib.. Shouldn't be there
			CurrentMouseMove.y = ScrollPosition.y;
		}
		
	}
    
	void InputSystemImpl::SetRelativeMouseMode(bool bEnabled)
	{
        MainApplication::Get().SetRelativeMouseMode(bEnabled);
		
		// Update cursor visibility
//		SetCursorShow(bCursorShown); // @todo This will show the cursor when we want it hidden
	}
    
	void InputSystemImpl::SetCursorShow(bool bShown)
	{
		MainApplication::Get().SetCursorShow(bShown);
		bCursorShown = bShown;
	}
    
	void InputSystemImpl::ProcessMouse(int32 X, int32 Y, int32 RelativeX, int32 RelativeY)
	{
        MousePos.x = X;
        MousePos.y = Y;
        
        // Will be used in the next frame
		InjectedMouseRelPos.x = RelativeX;
		InjectedMouseRelPos.y = RelativeY;
	}
    
	void InputSystemImpl::Enable(bool enabled)
	{
		bIsEnabled = enabled;
	}
    
	void InputSystemImpl::OnTouchEvent(ETouchType TouchType, ETouchGestureType GestureType, const Neko::Vec2 Delta)
	{
		static float LastDeltaTime = 1.0f;
		
		if (TouchType == Neko::ETouchType::Began)
		{
            // Reset previous velocity 
			ScrollVelocity = 0.0f;
			bScrolling = true;
		}
		else if (TouchType == Neko::ETouchType::Moved)
		{
			if (GestureType == Neko::ETouchGestureType::Scroll)
			{
				float drag;
				float velocity;
				
				// Figure out where we are starting from
				drag = ScrollPosition.y;
				
				// Add scroll resistance if we are past the bounds of the screen AKA elastic scrolling - doesn't work quite right.
				// With micro-movements of the fingers, we get to a place where we start working our way back to the start.
				//					if (ScrollPosition.y <= 0)
				//					{
				//						drag -= drag / SCROLL_RESISTANCE_COEFFICIENT;
				//					}
				//					else if (scrollPosition.y > contentOverflowY)
				//					{
				//						drag -= SCROLL_RESISTANCE_COEFFICIENT;
				//					}
				
				drag += Delta.y;
				ScrollPosition.y = drag;
                
				// Impart momentum to the scrolled view, using the velocity of the finger to compute the momentum of the scroll.
				// @todo Check it on the another platforms. On macOS delta can be 0.0 if gesture stopped immediately.
                velocity = Delta.y / LastDeltaTime;
                
                // Moving average filter to get rid of wild variations...
                ScrollVelocity = 0.8f * velocity + 0.2f * ScrollVelocity;
				
				// If we move past a certain threshold, then we are scrolling otherwise, we are possibly trying to tap.  This allows us to detect when we are just trying to push a button.
				const float ScrollDetectionThreshold = 2.0f;
				bScrolling = Neko::Math::abs(Delta.y) >= ScrollDetectionThreshold ? true : false;
			}
		}
		else if (TouchType == Neko::ETouchType::Ended)
		{
//            // Impart momentum, using last delta as the starting velocity ignore delta < 10; rounding issue can cause ultra-high velocity
//            if (Neko::Math::abs(Delta.y) >= 10)
//            {
//                ScrollVelocity.y = Delta.y / LastDeltaTime;
//            }
            
			// Save time value (in seconds)
			fTimeTouchPhaseEnded = Engine.GetTimer().GetCurrTime();
			bScrolling = false;
		}
		LastDeltaTime = Engine.GetTimer().GetFrameStartTime().GetSeconds();
	}
	
    void InputSystemImpl::OnKeyEvent(EScancode Code, bool Down)
	{
		if (!Code)
		{
			return;
		}
		
		bool repeated = Down && Keystate[Code];
		if (!repeated && Keystate[Code] == Down)
		{
			// Didn't change
			return;
		}
		
		// Update internal keyboard state
		Keystate[Code] = Down ? 1 : 0;
		
        uint16 modifierState;
        uint16 modifier;
        
		EKeyCode keycode = GKeymapBase.Get(Code);
		
		// Update modifiers state if activated
		switch (keycode)
		{
			case KC_LCTRL: modifier = KMOD_LCTRL; break;
			case KC_RCTRL: modifier = KMOD_RCTRL; break;
			case KC_LSHIFT: modifier = KMOD_LSHIFT; break;
			case KC_RSHIFT: modifier = KMOD_RSHIFT; break;
			case KC_LALT: modifier = KMOD_LALT; break;
			case KC_RALT: modifier = KMOD_RALT; break;
			case KC_LGUI: modifier = KMOD_LGUI; break;
			case KC_RGUI: modifier = KMOD_RGUI; break;
			case KC_MODE: modifier = KMOD_MODE; break;
				
			default: modifier = KMOD_NONE; break;
		}
		
		// Update internal keys modifier state
		if (Down)
		{
			modifierState = ModifierState;
			switch (keycode)
			{
				case KC_NUMLOCKCLEAR: ModifierState ^= KMOD_NUM; break;
				case KC_CAPSLOCK: ModifierState ^= KMOD_CAPS; break;
					
				default: ModifierState |= modifier; break;
			}
		}
		else
		{
			ModifierState &= ~modifier;
			modifierState = ModifierState;
		}
	}
    
	void InputSystemImpl::Reset()
	{
        memset(MouseButtonStates, 0 /* MouseButton_None */, sizeof(MouseButtonStates));
		memset(Keystate, 0, sizeof(Keystate));
		ModifierState = 0;
		CurrentMouseMove.MakeNull();
	}
	
    void InputSystemImpl::RegisterLuaAPI()
    {
        lua_State* L = Engine.GetState();
        
#define REGISTER_SCANCODE(SCANCODE) \
LuaWrapper::CreateSystemVariable(L, "Engine", "INPUT_SCANCODE_" #SCANCODE, (int)SC_##SCANCODE);
        
        REGISTER_SCANCODE(A);
        REGISTER_SCANCODE(B);
        REGISTER_SCANCODE(C);
        REGISTER_SCANCODE(D);
        REGISTER_SCANCODE(E);
        REGISTER_SCANCODE(F);
        REGISTER_SCANCODE(G);
        REGISTER_SCANCODE(H);
        REGISTER_SCANCODE(I);
        REGISTER_SCANCODE(J);
        REGISTER_SCANCODE(K);
        REGISTER_SCANCODE(L);
        REGISTER_SCANCODE(M);
        REGISTER_SCANCODE(N);
        REGISTER_SCANCODE(O);
        REGISTER_SCANCODE(P);
        REGISTER_SCANCODE(Q);
        REGISTER_SCANCODE(R);
        REGISTER_SCANCODE(S);
        REGISTER_SCANCODE(T);
        REGISTER_SCANCODE(U);
        REGISTER_SCANCODE(V);
        REGISTER_SCANCODE(W);
        REGISTER_SCANCODE(X);
        REGISTER_SCANCODE(Y);
        REGISTER_SCANCODE(Z);
        REGISTER_SCANCODE(1);
        REGISTER_SCANCODE(2);
        REGISTER_SCANCODE(3);
        REGISTER_SCANCODE(4);
        REGISTER_SCANCODE(5);
        REGISTER_SCANCODE(6);
        REGISTER_SCANCODE(7);
        REGISTER_SCANCODE(8);
        REGISTER_SCANCODE(9);
        REGISTER_SCANCODE(0);
        REGISTER_SCANCODE(RETURN);
        REGISTER_SCANCODE(ESCAPE);
        REGISTER_SCANCODE(BACKSPACE);
        REGISTER_SCANCODE(TAB);
        REGISTER_SCANCODE(SPACE);
        REGISTER_SCANCODE(MINUS);
        REGISTER_SCANCODE(EQUALS);
        REGISTER_SCANCODE(LEFTBRACKET);
        REGISTER_SCANCODE(RIGHTBRACKET);
        REGISTER_SCANCODE(BACKSLASH);
        REGISTER_SCANCODE(NONUSHASH);
        REGISTER_SCANCODE(SEMICOLON);
        REGISTER_SCANCODE(APOSTROPHE);
        REGISTER_SCANCODE(GRAVE);
        REGISTER_SCANCODE(COMMA);
        REGISTER_SCANCODE(PERIOD);
        REGISTER_SCANCODE(SLASH);
        REGISTER_SCANCODE(CAPSLOCK);
        REGISTER_SCANCODE(F1);
        REGISTER_SCANCODE(F2);
        REGISTER_SCANCODE(F3);
        REGISTER_SCANCODE(F4);
        REGISTER_SCANCODE(F5);
        REGISTER_SCANCODE(F6);
        REGISTER_SCANCODE(F7);
        REGISTER_SCANCODE(F8);
        REGISTER_SCANCODE(F9);
        REGISTER_SCANCODE(F10);
        REGISTER_SCANCODE(F11);
        REGISTER_SCANCODE(F12);
        REGISTER_SCANCODE(PRINTSCREEN);
        REGISTER_SCANCODE(SCROLLLOCK);
        REGISTER_SCANCODE(PAUSE);
        REGISTER_SCANCODE(INSERT);
        REGISTER_SCANCODE(HOME);
        REGISTER_SCANCODE(PAGEUP);
        REGISTER_SCANCODE(DELETE);
        REGISTER_SCANCODE(END);
        REGISTER_SCANCODE(PAGEDOWN);
        REGISTER_SCANCODE(RIGHT);
        REGISTER_SCANCODE(LEFT);
        REGISTER_SCANCODE(DOWN);
        REGISTER_SCANCODE(UP);
        REGISTER_SCANCODE(NUMLOCKCLEAR);
        REGISTER_SCANCODE(KP_DIVIDE);
        REGISTER_SCANCODE(KP_MULTIPLY);
        REGISTER_SCANCODE(KP_MINUS);
        REGISTER_SCANCODE(KP_PLUS);
        REGISTER_SCANCODE(KP_ENTER);
        REGISTER_SCANCODE(KP_1);
        REGISTER_SCANCODE(KP_2);
        REGISTER_SCANCODE(KP_3);
        REGISTER_SCANCODE(KP_4);
        REGISTER_SCANCODE(KP_5);
        REGISTER_SCANCODE(KP_6);
        REGISTER_SCANCODE(KP_7);
        REGISTER_SCANCODE(KP_8);
        REGISTER_SCANCODE(KP_9);
        REGISTER_SCANCODE(KP_0);
        REGISTER_SCANCODE(KP_PERIOD);
        REGISTER_SCANCODE(NONUSBACKSLASH);
        REGISTER_SCANCODE(APPLICATION);
        REGISTER_SCANCODE(POWER);
        REGISTER_SCANCODE(KP_EQUALS);
        REGISTER_SCANCODE(F13);
        REGISTER_SCANCODE(F14);
        REGISTER_SCANCODE(F15);
        REGISTER_SCANCODE(F16);
        REGISTER_SCANCODE(F17);
        REGISTER_SCANCODE(F18);
        REGISTER_SCANCODE(F19);
        REGISTER_SCANCODE(F20);
        REGISTER_SCANCODE(F21);
        REGISTER_SCANCODE(F22);
        REGISTER_SCANCODE(F23);
        REGISTER_SCANCODE(F24);
        REGISTER_SCANCODE(EXECUTE);
        REGISTER_SCANCODE(HELP);
        REGISTER_SCANCODE(MENU);
        REGISTER_SCANCODE(SELECT);
        REGISTER_SCANCODE(STOP);
        REGISTER_SCANCODE(AGAIN);
        REGISTER_SCANCODE(UNDO);
        REGISTER_SCANCODE(CUT);
        REGISTER_SCANCODE(COPY);
        REGISTER_SCANCODE(PASTE);
        REGISTER_SCANCODE(FIND);
        REGISTER_SCANCODE(MUTE);
        REGISTER_SCANCODE(VOLUMEUP);
        REGISTER_SCANCODE(VOLUMEDOWN);
        REGISTER_SCANCODE(KP_COMMA);
        REGISTER_SCANCODE(KP_EQUALSAS400);
        REGISTER_SCANCODE(INTERNATIONAL1);
        REGISTER_SCANCODE(INTERNATIONAL2);
        REGISTER_SCANCODE(INTERNATIONAL3);
        REGISTER_SCANCODE(INTERNATIONAL4);
        REGISTER_SCANCODE(INTERNATIONAL5);
        REGISTER_SCANCODE(INTERNATIONAL6);
        REGISTER_SCANCODE(INTERNATIONAL7);
        REGISTER_SCANCODE(INTERNATIONAL8);
        REGISTER_SCANCODE(INTERNATIONAL9);
        REGISTER_SCANCODE(LANG1);
        REGISTER_SCANCODE(LANG2);
        REGISTER_SCANCODE(LANG3);
        REGISTER_SCANCODE(LANG4);
        REGISTER_SCANCODE(LANG5);
        REGISTER_SCANCODE(LANG6);
        REGISTER_SCANCODE(LANG7);
        REGISTER_SCANCODE(LANG8);
        REGISTER_SCANCODE(LANG9);
        REGISTER_SCANCODE(ALTERASE);
        REGISTER_SCANCODE(SYSREQ);
        REGISTER_SCANCODE(CANCEL);
        REGISTER_SCANCODE(CLEAR);
        REGISTER_SCANCODE(PRIOR);
        REGISTER_SCANCODE(RETURN2);
        REGISTER_SCANCODE(SEPARATOR);
        REGISTER_SCANCODE(OUT);
        REGISTER_SCANCODE(OPER);
        REGISTER_SCANCODE(CLEARAGAIN);
        REGISTER_SCANCODE(CRSEL);
        REGISTER_SCANCODE(EXSEL);
        REGISTER_SCANCODE(KP_00);
        REGISTER_SCANCODE(KP_000);
        REGISTER_SCANCODE(THOUSANDSSEPARATOR);
        REGISTER_SCANCODE(DECIMALSEPARATOR);
        REGISTER_SCANCODE(CURRENCYUNIT);
        REGISTER_SCANCODE(CURRENCYSUBUNIT);
        REGISTER_SCANCODE(KP_LEFTPAREN);
        REGISTER_SCANCODE(KP_RIGHTPAREN);
        REGISTER_SCANCODE(KP_LEFTBRACE);
        REGISTER_SCANCODE(KP_RIGHTBRACE);
        REGISTER_SCANCODE(KP_TAB);
        REGISTER_SCANCODE(KP_BACKSPACE);
        REGISTER_SCANCODE(KP_A);
        REGISTER_SCANCODE(KP_B);
        REGISTER_SCANCODE(KP_C);
        REGISTER_SCANCODE(KP_D);
        REGISTER_SCANCODE(KP_E);
        REGISTER_SCANCODE(KP_F);
        REGISTER_SCANCODE(KP_XOR);
        REGISTER_SCANCODE(KP_POWER);
        REGISTER_SCANCODE(KP_PERCENT);
        REGISTER_SCANCODE(KP_LESS);
        REGISTER_SCANCODE(KP_GREATER);
        REGISTER_SCANCODE(KP_AMPERSAND);
        REGISTER_SCANCODE(KP_DBLAMPERSAND);
        REGISTER_SCANCODE(KP_VERTICALBAR);
        REGISTER_SCANCODE(KP_DBLVERTICALBAR);
        REGISTER_SCANCODE(KP_COLON);
        REGISTER_SCANCODE(KP_HASH);
        REGISTER_SCANCODE(KP_SPACE);
        REGISTER_SCANCODE(KP_AT);
        REGISTER_SCANCODE(KP_EXCLAM);
        REGISTER_SCANCODE(KP_MEMSTORE);
        REGISTER_SCANCODE(KP_MEMRECALL);
        REGISTER_SCANCODE(KP_MEMCLEAR);
        REGISTER_SCANCODE(KP_MEMADD);
        REGISTER_SCANCODE(KP_MEMSUBTRACT);
        REGISTER_SCANCODE(KP_MEMMULTIPLY);
        REGISTER_SCANCODE(KP_MEMDIVIDE);
        REGISTER_SCANCODE(KP_PLUSMINUS);
        REGISTER_SCANCODE(KP_CLEAR);
        REGISTER_SCANCODE(KP_CLEARENTRY);
        REGISTER_SCANCODE(KP_BINARY);
        REGISTER_SCANCODE(KP_OCTAL);
        REGISTER_SCANCODE(KP_DECIMAL);
        REGISTER_SCANCODE(KP_HEXADECIMAL);
        REGISTER_SCANCODE(LCTRL);
        REGISTER_SCANCODE(LSHIFT);
        REGISTER_SCANCODE(LALT);
        REGISTER_SCANCODE(LGUI);
        REGISTER_SCANCODE(RCTRL);
        REGISTER_SCANCODE(RSHIFT);
        REGISTER_SCANCODE(RALT);
        REGISTER_SCANCODE(RGUI);
        REGISTER_SCANCODE(MODE);
        REGISTER_SCANCODE(AUDIONEXT);
        REGISTER_SCANCODE(AUDIOPREV);
        REGISTER_SCANCODE(AUDIOSTOP);
        REGISTER_SCANCODE(AUDIOPLAY);
        REGISTER_SCANCODE(AUDIOMUTE);
        REGISTER_SCANCODE(MEDIASELECT);
        REGISTER_SCANCODE(WWW);
        REGISTER_SCANCODE(MAIL);
        REGISTER_SCANCODE(CALCULATOR);
        REGISTER_SCANCODE(COMPUTER);
        REGISTER_SCANCODE(AC_SEARCH);
        REGISTER_SCANCODE(AC_HOME);
        REGISTER_SCANCODE(AC_BACK);
        REGISTER_SCANCODE(AC_FORWARD);
        REGISTER_SCANCODE(AC_STOP);
        REGISTER_SCANCODE(AC_REFRESH);
        REGISTER_SCANCODE(AC_BOOKMARKS);
        REGISTER_SCANCODE(BRIGHTNESSDOWN);
        REGISTER_SCANCODE(BRIGHTNESSUP);
        REGISTER_SCANCODE(DISPLAYSWITCH);
        REGISTER_SCANCODE(KBDILLUMTOGGLE);
        REGISTER_SCANCODE(KBDILLUMDOWN);
        REGISTER_SCANCODE(KBDILLUMUP);
        REGISTER_SCANCODE(EJECT);
        REGISTER_SCANCODE(SLEEP);
        REGISTER_SCANCODE(APP1);
        REGISTER_SCANCODE(APP2);
        
#undef REGISTER_SCANCODE
        
        
#define REGISTER_KEYCODE(KEYCODE) \
LuaWrapper::CreateSystemVariable(L, "Engine", "INPUT_KEYCODE_" #KEYCODE, (int)KC_##KEYCODE);
        
        REGISTER_KEYCODE(RETURN);
        REGISTER_KEYCODE(ESCAPE);
        REGISTER_KEYCODE(BACKSPACE);
        REGISTER_KEYCODE(TAB);
        REGISTER_KEYCODE(SPACE);
        REGISTER_KEYCODE(EXCLAIM);
        REGISTER_KEYCODE(QUOTEDBL);
        REGISTER_KEYCODE(HASH);
        REGISTER_KEYCODE(PERCENT);
        REGISTER_KEYCODE(DOLLAR);
        REGISTER_KEYCODE(AMPERSAND);
        REGISTER_KEYCODE(QUOTE);
        REGISTER_KEYCODE(LEFTPAREN);
        REGISTER_KEYCODE(RIGHTPAREN);
        REGISTER_KEYCODE(ASTERISK);
        REGISTER_KEYCODE(PLUS);
        REGISTER_KEYCODE(COMMA);
        REGISTER_KEYCODE(MINUS);
        REGISTER_KEYCODE(PERIOD);
        REGISTER_KEYCODE(SLASH);
        REGISTER_KEYCODE(0);
        REGISTER_KEYCODE(1);
        REGISTER_KEYCODE(2);
        REGISTER_KEYCODE(3);
        REGISTER_KEYCODE(4);
        REGISTER_KEYCODE(5);
        REGISTER_KEYCODE(6);
        REGISTER_KEYCODE(7);
        REGISTER_KEYCODE(8);
        REGISTER_KEYCODE(9);
        REGISTER_KEYCODE(COLON);
        REGISTER_KEYCODE(SEMICOLON);
        REGISTER_KEYCODE(LESS);
        REGISTER_KEYCODE(EQUALS);
        REGISTER_KEYCODE(GREATER);
        REGISTER_KEYCODE(QUESTION);
        REGISTER_KEYCODE(AT);
        REGISTER_KEYCODE(LEFTBRACKET);
        REGISTER_KEYCODE(BACKSLASH);
        REGISTER_KEYCODE(RIGHTBRACKET);
        REGISTER_KEYCODE(CARET);
        REGISTER_KEYCODE(UNDERSCORE);
        REGISTER_KEYCODE(BACKQUOTE);
        REGISTER_KEYCODE(a);
        REGISTER_KEYCODE(b);
        REGISTER_KEYCODE(c);
        REGISTER_KEYCODE(d);
        REGISTER_KEYCODE(e);
        REGISTER_KEYCODE(f);
        REGISTER_KEYCODE(g);
        REGISTER_KEYCODE(h);
        REGISTER_KEYCODE(i);
        REGISTER_KEYCODE(j);
        REGISTER_KEYCODE(k);
        REGISTER_KEYCODE(l);
        REGISTER_KEYCODE(m);
        REGISTER_KEYCODE(n);
        REGISTER_KEYCODE(o);
        REGISTER_KEYCODE(p);
        REGISTER_KEYCODE(q);
        REGISTER_KEYCODE(r);
        REGISTER_KEYCODE(s);
        REGISTER_KEYCODE(t);
        REGISTER_KEYCODE(u);
        REGISTER_KEYCODE(v);
        REGISTER_KEYCODE(w);
        REGISTER_KEYCODE(x);
        REGISTER_KEYCODE(y);
        REGISTER_KEYCODE(z);
        REGISTER_KEYCODE(CAPSLOCK);
        REGISTER_KEYCODE(F1);
        REGISTER_KEYCODE(F2);
        REGISTER_KEYCODE(F3);
        REGISTER_KEYCODE(F4);
        REGISTER_KEYCODE(F5);
        REGISTER_KEYCODE(F6);
        REGISTER_KEYCODE(F7);
        REGISTER_KEYCODE(F8);
        REGISTER_KEYCODE(F9);
        REGISTER_KEYCODE(F10);
        REGISTER_KEYCODE(F11);
        REGISTER_KEYCODE(F12);
        REGISTER_KEYCODE(PRINTSCREEN);
        REGISTER_KEYCODE(SCROLLLOCK);
        REGISTER_KEYCODE(PAUSE);
        REGISTER_KEYCODE(INSERT);
        REGISTER_KEYCODE(HOME);
        REGISTER_KEYCODE(PAGEUP);
        REGISTER_KEYCODE(DELETE);
        REGISTER_KEYCODE(END);
        REGISTER_KEYCODE(PAGEDOWN);
        REGISTER_KEYCODE(RIGHT);
        REGISTER_KEYCODE(LEFT);
        REGISTER_KEYCODE(DOWN);
        REGISTER_KEYCODE(UP);
        REGISTER_KEYCODE(NUMLOCKCLEAR);
        REGISTER_KEYCODE(KP_DIVIDE);
        REGISTER_KEYCODE(KP_MULTIPLY);
        REGISTER_KEYCODE(KP_MINUS);
        REGISTER_KEYCODE(KP_PLUS);
        REGISTER_KEYCODE(KP_ENTER);
        REGISTER_KEYCODE(KP_1);
        REGISTER_KEYCODE(KP_2);
        REGISTER_KEYCODE(KP_3);
        REGISTER_KEYCODE(KP_4);
        REGISTER_KEYCODE(KP_5);
        REGISTER_KEYCODE(KP_6);
        REGISTER_KEYCODE(KP_7);
        REGISTER_KEYCODE(KP_8);
        REGISTER_KEYCODE(KP_9);
        REGISTER_KEYCODE(KP_0);
        REGISTER_KEYCODE(KP_PERIOD);
        REGISTER_KEYCODE(APPLICATION);
        REGISTER_KEYCODE(POWER);
        REGISTER_KEYCODE(KP_EQUALS);
        REGISTER_KEYCODE(F13);
        REGISTER_KEYCODE(F14);
        REGISTER_KEYCODE(F15);
        REGISTER_KEYCODE(F16);
        REGISTER_KEYCODE(F17);
        REGISTER_KEYCODE(F18);
        REGISTER_KEYCODE(F19);
        REGISTER_KEYCODE(F20);
        REGISTER_KEYCODE(F21);
        REGISTER_KEYCODE(F22);
        REGISTER_KEYCODE(F23);
        REGISTER_KEYCODE(F24);
        REGISTER_KEYCODE(EXECUTE);
        REGISTER_KEYCODE(HELP);
        REGISTER_KEYCODE(MENU);
        REGISTER_KEYCODE(SELECT);
        REGISTER_KEYCODE(STOP);
        REGISTER_KEYCODE(AGAIN);
        REGISTER_KEYCODE(UNDO);
        REGISTER_KEYCODE(CUT);
        REGISTER_KEYCODE(COPY);
        REGISTER_KEYCODE(PASTE);
        REGISTER_KEYCODE(FIND);
        REGISTER_KEYCODE(MUTE);
        REGISTER_KEYCODE(VOLUMEUP);
        REGISTER_KEYCODE(VOLUMEDOWN);
        REGISTER_KEYCODE(KP_COMMA);
        REGISTER_KEYCODE(KP_EQUALSAS400);
        REGISTER_KEYCODE(ALTERASE);
        REGISTER_KEYCODE(SYSREQ);
        REGISTER_KEYCODE(CANCEL);
        REGISTER_KEYCODE(CLEAR);
        REGISTER_KEYCODE(PRIOR);
        REGISTER_KEYCODE(RETURN2);
        REGISTER_KEYCODE(SEPARATOR);
        REGISTER_KEYCODE(OUT);
        REGISTER_KEYCODE(OPER);
        REGISTER_KEYCODE(CLEARAGAIN);
        REGISTER_KEYCODE(CRSEL);
        REGISTER_KEYCODE(EXSEL);
        REGISTER_KEYCODE(KP_00);
        REGISTER_KEYCODE(KP_000);
        REGISTER_KEYCODE(THOUSANDSSEPARATOR);
        REGISTER_KEYCODE(DECIMALSEPARATOR);
        REGISTER_KEYCODE(CURRENCYUNIT);
        REGISTER_KEYCODE(CURRENCYSUBUNIT);
        REGISTER_KEYCODE(KP_LEFTPAREN);
        REGISTER_KEYCODE(KP_RIGHTPAREN);
        REGISTER_KEYCODE(KP_LEFTBRACE);
        REGISTER_KEYCODE(KP_RIGHTBRACE);
        REGISTER_KEYCODE(KP_TAB);
        REGISTER_KEYCODE(KP_BACKSPACE);
        REGISTER_KEYCODE(KP_A);
        REGISTER_KEYCODE(KP_B);
        REGISTER_KEYCODE(KP_C);
        REGISTER_KEYCODE(KP_D);
        REGISTER_KEYCODE(KP_E);
        REGISTER_KEYCODE(KP_F);
        REGISTER_KEYCODE(KP_XOR);
        REGISTER_KEYCODE(KP_POWER);
        REGISTER_KEYCODE(KP_PERCENT);
        REGISTER_KEYCODE(KP_LESS);
        REGISTER_KEYCODE(KP_GREATER);
        REGISTER_KEYCODE(KP_AMPERSAND);
        REGISTER_KEYCODE(KP_DBLAMPERSAND);
        REGISTER_KEYCODE(KP_VERTICALBAR);
        REGISTER_KEYCODE(KP_DBLVERTICALBAR);
        REGISTER_KEYCODE(KP_COLON);
        REGISTER_KEYCODE(KP_HASH);
        REGISTER_KEYCODE(KP_SPACE);
        REGISTER_KEYCODE(KP_AT);
        REGISTER_KEYCODE(KP_EXCLAM);
        REGISTER_KEYCODE(KP_MEMSTORE);
        REGISTER_KEYCODE(KP_MEMRECALL);
        REGISTER_KEYCODE(KP_MEMCLEAR);
        REGISTER_KEYCODE(KP_MEMADD);
        REGISTER_KEYCODE(KP_MEMSUBTRACT);
        REGISTER_KEYCODE(KP_MEMMULTIPLY);
        REGISTER_KEYCODE(KP_MEMDIVIDE);
        REGISTER_KEYCODE(KP_PLUSMINUS);
        REGISTER_KEYCODE(KP_CLEAR);
        REGISTER_KEYCODE(KP_CLEARENTRY);
        REGISTER_KEYCODE(KP_BINARY);
        REGISTER_KEYCODE(KP_OCTAL);
        REGISTER_KEYCODE(KP_DECIMAL);
        REGISTER_KEYCODE(KP_HEXADECIMAL);
        REGISTER_KEYCODE(LCTRL);
        REGISTER_KEYCODE(LSHIFT);
        REGISTER_KEYCODE(LALT);
        REGISTER_KEYCODE(LGUI);
        REGISTER_KEYCODE(RCTRL);
        REGISTER_KEYCODE(RSHIFT);
        REGISTER_KEYCODE(RALT);
        REGISTER_KEYCODE(RGUI);
        REGISTER_KEYCODE(MODE);
        REGISTER_KEYCODE(AUDIONEXT);
        REGISTER_KEYCODE(AUDIOPREV);
        REGISTER_KEYCODE(AUDIOSTOP);
        REGISTER_KEYCODE(AUDIOPLAY);
        REGISTER_KEYCODE(AUDIOMUTE);
        REGISTER_KEYCODE(MEDIASELECT);
        REGISTER_KEYCODE(WWW);
        REGISTER_KEYCODE(MAIL);
        REGISTER_KEYCODE(CALCULATOR);
        REGISTER_KEYCODE(COMPUTER);
        REGISTER_KEYCODE(AC_SEARCH);
        REGISTER_KEYCODE(AC_HOME);
        REGISTER_KEYCODE(AC_BACK);
        REGISTER_KEYCODE(AC_FORWARD);
        REGISTER_KEYCODE(AC_STOP);
        REGISTER_KEYCODE(AC_REFRESH);
        REGISTER_KEYCODE(AC_BOOKMARKS);
        REGISTER_KEYCODE(BRIGHTNESSDOWN);
        REGISTER_KEYCODE(BRIGHTNESSUP);
        REGISTER_KEYCODE(DISPLAYSWITCH);
        REGISTER_KEYCODE(KBDILLUMTOGGLE);
        REGISTER_KEYCODE(KBDILLUMDOWN);
        REGISTER_KEYCODE(KBDILLUMUP);
        REGISTER_KEYCODE(EJECT);
        REGISTER_KEYCODE(SLEEP);
        
#undef REGISTER_KEYCODE
    }
	
    IInputSystem* IInputSystem::Create(IEngine& engine)
    {
        auto* system = NEKO_NEW(engine.GetAllocator(), InputSystemImpl)(engine);
        return system;
    }
    
    void IInputSystem::Destroy(IInputSystem& system)
    {
        auto* impl = static_cast<InputSystemImpl*>(&system);
        NEKO_DELETE(impl->GetEngine().GetAllocator(), impl);
    }

	// Tools
	
    void KeymapBase::GetDefaultKeymap(EKeyCode* OutKeymap)
    {
        memcpy(OutKeymap, GDefaultKeymap, sizeof(GDefaultKeymap));
    }
	
    void KeymapBase::SetKeymap(int Start, EKeyCode* InKeys, int Length)
    {
        if (Start < 0 || Start + Length > SC_COUNT)
        {
            return;
        }
		
        memcpy(&Keymap[Start], InKeys, sizeof(*InKeys) * Length);
    }

    EScancode KeymapBase::GetScancodeFromKey(EKeyCode Key)
    {
        int scancode;
		
        for (scancode = SC_UNKNOWN; scancode < SC_COUNT; ++scancode)
        {
            if (Keymap[scancode] == Key)
            {
                return (EScancode)scancode;
            }
        }
        return SC_UNKNOWN;
    }
	
    void KeymapBase::SetScancodeName(EScancode Scancode, const char* Name)
    {
        GScancodeNames[Scancode] = Name;
    }
	
    const char* KeymapBase::GetScancodeName(EScancode scancode)
    {
        const char* name;
        if (scancode < SC_UNKNOWN || scancode >= SC_COUNT)
        {
            return "";
        }
		
        name = GScancodeNames[scancode];
        if (name)
        {
            return name;
        }
        else
        {
            return "";
        }
    }
	
    EScancode KeymapBase::GetScancodeFromName(const char *name)
    {
        int i;
		
        if (!name || !*name)
        {
            assert(false);
            return SC_UNKNOWN;
        }
		
        for (i = 0; i < Neko::lengthOf(GScancodeNames); ++i)
        {
            if (!GScancodeNames[i])
            {
                continue;
            }
            if (Neko::EqualIStrings(name, GScancodeNames[i]) == 0)
            {
                return (EScancode)i;
            }
        }
		
        return SC_UNKNOWN;
    }
	
    EKeyCode KeymapBase::GetKeyFromScancode(EScancode Scancode)
    {
        if (Scancode < SC_UNKNOWN || Scancode >= SC_COUNT)
        {
            return 0;
        }
		
        return Keymap[Scancode];
    }

    const char* KeymapBase::GetKeyName(EKeyCode Key)
    {
        static char name[8];
        char* end;
		
        if (Key & SC_MASK)
        {
            return GetScancodeName((EScancode)(Key & ~SC_MASK));
        }
		
        switch (Key)
        {
            case KC_RETURN:
            {
                return GetScancodeName(SC_RETURN);
            }
            case KC_ESCAPE:
            {
                return GetScancodeName(SC_ESCAPE);
            }
            case KC_BACKSPACE:
            {
                return GetScancodeName(SC_BACKSPACE);
            }
            case KC_TAB:
            {
                return GetScancodeName(SC_TAB);
            }
            case KC_SPACE:
            {
                return GetScancodeName(SC_SPACE);
            }
            case KC_DELETE:
            {
                return GetScancodeName(SC_DELETE);
            }
            default:
            {
                if (Key >= 'a' && Key <= 'z')
                {
                    Key -= 32;
                }
                
                end = Util::UCS4ToUTF8((uint32)Key, name);
                *end = '\0';
                return name;
            }
        }
    }
    
    
    
    // Controller mapping parser tools
    
    SGuid GetGuidFromString(const char* GuidString)
    {
        SGuid guid;
        int32 maxoutputbytes = sizeof(guid);
        
        size_t len = StringLength(GuidString);
        uint8* p;
        size_t i;
        
        len = (len)& ~0x1;
        
        p = (uint8* )&guid;
        for (i = 0; (i < len) && ((p - (uint8 *)&guid) < maxoutputbytes); i += 2, p++)
        {
            *p = (Nibble(GuidString[i]) << 4) | Nibble(GuidString[i + 1]);
        }
        
        return guid;
    }
    
    EGameControllerAxis GetAxisFromString(const char* string)
    {
        int32 entry;
        
        if (!string || !string[0])
        {
            return EGameControllerAxis::Invalid;
        }
        
        for (entry = 0; DefaultControllerItemNames[entry]; ++entry)
        {
            // @note case-depend
            if (EqualIStrings(string, DefaultControllerItemNames[entry]))
            {
                return (EGameControllerAxis)entry;
            }
        }
        return EGameControllerAxis::Invalid;
    }
    
    EGameControllerButton GetButtonFromString(const char* string)
    {
        int32 entry;
        if (!string || !string[0])
        {
            return EGameControllerButton::Invalid;
        }
        
        for (entry = 0; DefaultControllerButtonNames[entry]; ++entry)
        {
            if (EqualIStrings(string, DefaultControllerButtonNames[entry]))
            {
                return (EGameControllerButton)entry;
            }
        }
        
        return EGameControllerButton::Invalid;
    }
    
    //@todo: limits
    void GameControllerParseButton(const char* gameButtonString, const char* joystickString, SControllerMapping* mapping)
    {
        int32 iButton;
        
        EGameControllerButton button;
        EGameControllerAxis axis;
        
        // Info from string.
        button = GetButtonFromString(gameButtonString);
        axis = GetAxisFromString(gameButtonString);
        iButton = atoi(&joystickString[1]);
        
        if (joystickString[0] == 'a')
        {
            if (axis != EGameControllerAxis::Invalid)
            {
                mapping->iAxes[(uint32)axis] = iButton;
                mapping->controllerAxes[iButton] = axis;
            }
            else if (button != EGameControllerButton::Invalid)
            {
                mapping->iAxesButton[(uint32)button] = iButton;
                mapping->controllerAxisButton[iButton] = button;
            }
        }
        else if (joystickString[0] == 'b')
        {
            if (button != EGameControllerButton::Invalid)
            {
                mapping->iButtons[(uint32)button] = iButton;
                mapping->controllerButton[iButton] = button;
            }
            else if (axis != EGameControllerAxis::Invalid)
            {
                mapping->iButtonAxix[(uint32)axis] = iButton;
                mapping->controllerButtonAxis[iButton] = axis;
            }
            
        }
        else if (joystickString[0] == 'h')
        {
            // hat and its mask
            int32 hat = atoi(&joystickString[1]);
            int32 mask = atoi(&joystickString[3]);
            
            if (button != EGameControllerButton::Invalid)
            {
                int32 ridx;
                
                mapping->buttonHat[(uint32)button].Hat = hat;
                mapping->buttonHat[(uint32)button].Mask = mask;
                
                ridx = (hat << 4) | mask;
                mapping->controllerHatButton[ridx] = button;
                
            }
            else if (axis != EGameControllerAxis::Invalid) { /* @nothing to do */ }
        }
    }
    
    static void GameControllerParseControllerConfigString(SControllerMapping* mapping, const char* dataString)
    {
        static char gameButtonStr[20];
        char joystickStr[20];
        bool gameButton = true;
        
        int32 i = 0;
        const char* charPos = dataString;
        
        memset(gameButtonStr, 0x0, sizeof(gameButtonStr));
        memset(joystickStr, 0x0, sizeof(joystickStr));
        
        while (charPos && *charPos)
        {
            if (*charPos == ':')
            {
                i = 0;
                gameButton = false;
            }
            else if (*charPos == ' ')
            { /* skip */ }
            else if (*charPos == ',')
            {
                i = 0;
                gameButton = true;
                GameControllerParseButton(gameButtonStr, joystickStr, mapping);
                
                memset(gameButtonStr, 0x0, sizeof(gameButtonStr));
                memset(joystickStr, 0x0, sizeof(joystickStr));
                
            }
            else if (gameButton)
            {
                gameButtonStr[i] = *charPos;
                ++i;
            }
            else
            {
                joystickStr[i] = *charPos;
                ++i;
            }
            
            ++charPos;
        }
        
        GameControllerParseButton(gameButtonStr, joystickStr, mapping);
    }

    void LoadButtonMapping(SControllerMapping* mapping, SGuid Guid, const char* mappingName, const char* mappingString)
    {
        int32 j;
        
        mapping->Guid = Guid;
        mapping->Name = mappingName;
        
        for (j = 0; j < (uint32)EGameControllerAxis::Max; ++j)
        {
            mapping->iAxes[j] = -1;
            mapping->iButtonAxix[j] = -1;
        }
        for (j = 0; j < (uint32)EGameControllerButton::Max; ++j)
        {
            mapping->iButtons[j] = -1;
            mapping->iAxesButton[j] = -1;
            mapping->buttonHat[j].Hat = -1;
        }
        
        for (j = 0; j < kMaxEntries; ++j)
        {
            mapping->controllerAxes[j] = EGameControllerAxis::Invalid;
            mapping->controllerButtonAxis[j] = EGameControllerAxis::Invalid;
            mapping->controllerButton[j] = EGameControllerButton::Invalid;
            mapping->controllerAxisButton[j] = EGameControllerButton::Invalid;
        }
        
        for (j = 0; j < kMaxHatEntries; ++j)
        {
            mapping->controllerHatButton[j] = EGameControllerButton::Invalid;
        }
        
        GameControllerParseControllerConfigString(mapping, mappingString);
    }
    
    char* GetControllerGuidFromMappingString(const char* mapping)
    {
        const char* Comma = strchr(mapping, ',');
        // Find first comma
        if (Comma)
        {
            char* GuidStr = (char *)malloc(Comma - mapping + 1);
            if (!GuidStr)
            {
                return nullptr;
            }
            
            memcpy(GuidStr, mapping, Comma - mapping);
            GuidStr[Comma - mapping] = 0;
            
            return GuidStr;
        }
        
        return nullptr;
    }

    char* GetControllerNameFromMappingString(const char* mapping)
    {
        const char* Comma, *Comma2;
        char* mappingName;
        
        Comma = strchr(mapping, ',');
        if (!Comma)
        {
            return nullptr;
        }
        
        Comma2 = strchr(Comma + 1, ',');
        if (!Comma2)
        {
            return nullptr;
        }
        
        mappingName = (char*)malloc(Comma2 - Comma);
        if (!mappingName)
        {
            return nullptr;
        }
        
        memcpy(mappingName, Comma + 1, Comma2 - Comma);
        mappingName[Comma2 - Comma - 1] = 0;
        
        return mappingName;
    }

    char* GetControllerMappingFromMappingString(const char* mapping)
    {
        const char* Comma, *Comma2;
        
        Comma = strchr(mapping, ',');
        if (!Comma)
        {
            return nullptr;
        }
        
        Comma2 = strchr(Comma + 1, ',');
        if (!Comma2)
        {
            return nullptr;
        }
        
        return strdup(Comma2 + 1);
    }
    
} // namespace Neko
