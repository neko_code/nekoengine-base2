//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  EventHandler.h
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "../Core/Joystick.h"
#include "../Core/Engine.h"
#include "../Platform/IGenericWindow.h"
#include "../Platform/SystemShared.h"
#include "../Containers/DelegateList.h"
#include "../Containers/Queue.h"
#include "../Mt/Sync.h"

namespace Neko
{
    // Each engine component can implement 'OnSystemEvent' binding to receive system events directly

    class IEngine;
    
#define ENTRY_IMPLEMENT_EVENT(class, type) class( IGenericWindow* handle ) : Event(type, handle) { }
    
    typedef uint16 GamepadHandle;
	
    /** Event types. */
    struct Event
    {
        enum Enum
        {
            // Gamepad
            JoyButton,
            JoyAxisMotion,
            JoyHatMotion,
            //! Connection event
            JoyGamepad,
            
            // System
            Char,
            Exit,
            Key,
            Mouse,
            Touch,
            Size,
            Window,
            Suspend,
        };
        
        Event(Enum type)
        : Type(type)
        {
            Handle = nullptr;
        }
        
        Event(Enum type, IGenericWindow* Handle_)
        : Type(type)
        , Handle(Handle_)
        {
        }
        
        ~Event() {}
        
        Event::Enum Type;
        IGenericWindow* Handle;
    };
    
    
    /** Gamepad button. */
    struct GamepadButtonEvent final : public Event
    {
        ENTRY_IMPLEMENT_EVENT(GamepadButtonEvent, Event::JoyButton);
        
        uint8 Button;
        int8 Value;
        GamepadHandle Instance;
    };
    
    
    /** Gamepad axis. */
    struct GamepadAxisEvent final : public Event
    {
        ENTRY_IMPLEMENT_EVENT(GamepadAxisEvent, Event::JoyAxisMotion);
        
        uint8 Axis;
        int16 Value;
        GamepadHandle Instance;
    };
    
    
    /** Gamepad hat. */
    struct GamepadHatEvent final : public Event
    {
        ENTRY_IMPLEMENT_EVENT(GamepadHatEvent, Event::JoyHatMotion);
        
        uint8 Hat;
        int32 Value;
        GamepadHandle Instance;
    };
    
    
    /** Input keyboard character. */
    struct CharEvent final : public Event
    {
        ENTRY_IMPLEMENT_EVENT(CharEvent, Event::Char);
        
        uint16 CurrentInputChar;
    };
    
    
    /** Gamepad connection. */
    struct GamepadEvent final : public Event
    {
        ENTRY_IMPLEMENT_EVENT(GamepadEvent, Event::JoyGamepad);
        
        bool Connected;
        int32 DeviceIndex;
        
        GamepadHardwareInfo Info;
    };
    
    
    /** Key handler event. */
    struct KeyEvent final : public Event
    {
        ENTRY_IMPLEMENT_EVENT(KeyEvent, Event::Key);
        
        EScancode Key;
        uint32 Modifiers;
        bool bDown;
    };
    
    
    /** Mouse event. */
    struct MouseEvent final : public Event
    {
        enum
        {
            MOUSE_FLAG_NONE = BIT(0),
            MOUSE_FLAG_DOWN = BIT(1),
            MOUSE_FLAG_MOVED = BIT(2),
            MOUSED_FLAG_PRECISION = BIT(3),
        };
        
        ENTRY_IMPLEMENT_EVENT(MouseEvent, Event::Mouse);
        
        int32 MouseX;
        int32 MouseY;
        
        int32 RelativeX;
        int32 RelativeY;
        
        EMouseButtons Button;
        
        int32 Flags; // MOUSE_FLAG_DOWN, MOUSE_FLAG_MOVED, MOUSED_FLAG_PRECISION
    };
    
    
    /** Touch event (trackpad, gesture etc..) */
    struct TouchEvent final : public Event
    {
        ENTRY_IMPLEMENT_EVENT(TouchEvent, Event::Touch);
        
        //! Delta (X = 0 if it's just simple mouse scroll).
        Vec2 Delta;

        //! True if delta is inverted from device
        bool bInverted;
		
        //! Type of touch gesture stage (began, moved, ended)
		ETouchType TouchType;
		
        //! Type of the touch gesture (e.g. scroll, magnify, rotate, swipe..)
        ETouchGestureType Type;
    };
    
    
    /** Window resize event. */
    struct SizeEvent final : public Event
    {
        enum
        {
            Sized = BIT(0),
            Moved = BIT(1)
        };
        
        ENTRY_IMPLEMENT_EVENT(SizeEvent, Event::Size);
        
        int32 Flags;
        
        int32 X;
        int32 Y;
    };
    
    
    /** Window handle event. */
    struct WindowEvent final : public Event
    {
        ENTRY_IMPLEMENT_EVENT(WindowEvent, Event::Window);
        
        enum
        {
            BecameActive = BIT(0),
            BecameResigned = BIT(1),
            Closed = BIT(2),
        };
        
        IGenericWindow* Window; // is this useless ? we already receive events from window
        
        int32 Flags;
    };
    
    
    /** Application suspend modes. */
    enum class ESuspendState : uint8
    {
        WillSuspend,
        DidSuspend,
        WillResume,
        DidResume
    };
    
    
    /** Application suspend event. */
    struct SuspendEvent : public Event
    {
        ENTRY_IMPLEMENT_EVENT(SuspendEvent, Event::Suspend);
        
        ESuspendState State;
    };
    
    /// Main system event dispatcher.
    class NEKO_ENGINE_API CEventDispatcher final
    {
    public:
        
        CEventDispatcher(IEngine& Engine);
        
        ~CEventDispatcher();
        
        void OnEvent(const Event* InEvent);
        
        void Process();
        
        /**
         * Binds the function to callback when a new event is received.
         */
        template <typename C, void (C::*Function)(const Event*)> void RegisterListener(C* instance)
        {
            MT::SpinLock Lock(ListenerRegistrationLock);
            OnEventCb.Bind<C, Function>(instance);
        }
        
        template <typename C, void (C::*Function)(const Event*)> void UnregisterListener(C* instance)
        {
            MT::SpinLock Lock(ListenerRegistrationLock);
            OnEventCb.Unbind<C, Function>(instance);
        }
        
        
        NEKO_FORCE_INLINE IAllocator& GetAllocator() const
        {
            return Engine.GetAllocator();
        }
        
    private:
        
        void ProcessOnAnyThread(Event* event);
        
    private:
        
        DelegateList<void(const Event*)> OnEventCb;
        
        //! Engine handle.
        IEngine& Engine;
        
        MT::SpinMutex ListenerRegistrationLock;
    };
    
    /// Used to handle system platform events, this is mostly for platforms implementation.
    class NEKO_ENGINE_API IGenericMessageHandler
    {
    public:
        
        virtual ~IGenericMessageHandler() { }
    
        virtual void OnCharEvent(IGenericWindow* Handle, const uint32 Key) { }
        
        virtual void OnExitEvent() { }
        
        virtual void OnKeyEvent(IGenericWindow* Handle, EScancode Key, uint32 Modifiers, bool bDown) { }
        
        virtual void OnMouseEvent(IGenericWindow* Handle, int32 MouseX, int32 MouseY, bool bHighPrecision) { }
        
        virtual void OnMouseEvent(IGenericWindow* Handle, int32 MouseX, int32 MouseY, EMouseButtons Button, bool _down) { }
		
		virtual void OnBeginGesture() { }
		
		virtual void OnEndGesture() { }
		
		virtual void OnTouchBegin() { }
		
		virtual void OnTouchEnd() { }
		
        virtual void OnTouchGesture(IGenericWindow* Handle, ETouchGestureType Type, Vec2 Delta, bool bInverted) { }
        
        virtual void OnControllerConnectionChange(const GamepadHandle DeviceIndex, const GamepadHardwareInfo Info, bool Connected) { }
        
        virtual void OnControllerButton(const GamepadHandle DeviceIndex, uint8 Button, uint8 Value){ }
        
        virtual void OnControllerHat(const GamepadHandle DeviceIndex, uint8 Hat, uint8 Value) { }
        
        virtual void OnControllerAxis(const GamepadHandle DeviceIndex, uint8 Axis, int16 Value) { }
        
        virtual void OnSizeEvent(IGenericWindow* Handle, uint32 Width, uint32 Height) { }
        
        virtual void OnMovedWindow(IGenericWindow* Handle, int32 X, int32 Y) { }
        
        virtual void OnWindowEvent(IGenericWindow* Handle, void* Native = nullptr) { }
        
        virtual void OnSuspendEvent(IGenericWindow* Handle, ESuspendState State) { }

        virtual void BeginResizingWindow(IGenericWindow* Handle) { }
        
        virtual void EndResizingWindow(IGenericWindow* Handle) { }
        
        virtual void OnWindowFocus(IGenericWindow* Handle, bool bGained) { }
        // Empty
        virtual void OnWindowClose(IGenericWindow* Handle) { }
    };
}
