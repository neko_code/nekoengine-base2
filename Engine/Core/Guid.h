#pragma once

#include "../Utilities/Text.h"

namespace Neko
{
    /**
     * Enumerates known GUID formats.
     */
    enum class EGuidFormats
    {
        /**
         * 32 digits. E.g. "00000000000000000000000000000000"
         */
        Digits,
        
        /**
         * 32 digits separated by hyphens. E.g. 00000000-0000-0000-0000-000000000000
         */
        DigitsWithHyphens,
        
        /**
         * 32 digits separated by hyphens and enclosed in braces. E.g. {00000000-0000-0000-0000-000000000000}
         */
        DigitsWithHyphensInBraces,
        
        /**
         * 32 digits separated by hyphens and enclosed in parentheses. E.g. (00000000-0000-0000-0000-000000000000)
         */
        DigitsWithHyphensInParentheses,
        
        /**
         * Comma-separated hexadecimal values enclosed in braces. E.g. {0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}}
         */
        HexValuesInBraces
    };

    
    /**
     * Implements a globally universally unique identifier.
     */
    struct SGuid
    {
    public:
        
        /** Default constructor. */
        SGuid()
        : A(0) , B(0) , C(0) , D(0)
        { }
        
        SGuid(uint32 InA, uint32 InB, uint32 InC, uint32 InD)
        : A(InA), B(InB), C(InC), D(InD)
        { }
        
    public:
        
        /**
         * Compares two GUIDs for equality.
         */
        friend bool operator == (const SGuid& X, const SGuid& Y)
        {
            return ((X.A ^ Y.A) | (X.B ^ Y.B) | (X.C ^ Y.C) | (X.D ^ Y.D)) == 0;
        }
        
        /**
         * Compares two GUIDs for inequality.
         */
        friend bool operator != (const SGuid& X, const SGuid& Y)
        {
            return ((X.A ^ Y.A) | (X.B ^ Y.B) | (X.C ^ Y.C) | (X.D ^ Y.D)) != 0;
        }
        
        /**
         * Compares two GUIDs.
         */
        friend bool operator < (const SGuid& X, const SGuid& Y)
        {
            return	((X.A < Y.A) ? true : ((X.A > Y.A) ? false :
                                           ((X.B < Y.B) ? true : ((X.B > Y.B) ? false :
                                                                  ((X.C < Y.C) ? true : ((X.C > Y.C) ? false :
                                                                                         ((X.D < Y.D) ? true : ((X.D > Y.D) ? false : false )))))))); //-V583
        }
        
        /**
         * Provides access to the GUIDs components.
         *
         * @param Index 0...3.
         */
        uint32& operator[]( int32 Index )
        {
            assert(Index >= 0);
            assert(Index < 4);
            
            switch(Index)
            {
                case 0: return A;
                case 1: return B;
                case 2: return C;
                case 3: return D;
            }
            
            return A;
        }
        
        /**
         * Provides read-only access to the GUIDs components.
         *
         * @param Index 0...3.
         */
        const uint32& operator[]( int32 Index ) const
        {
            assert(Index >= 0);
            assert(Index < 4);
            
            switch(Index)
            {
                case 0: return A;
                case 1: return B;
                case 2: return C;
                case 3: return D;
            }
            
            return A;
        }
        
        /**
         * Invalidates the GUID.
         */
        void Invalidate()
        {
            A = B = C = D = 0;
        }
        
        /**
         * Checks whether this GUID is valid or not.
         *
         * A GUID that has all its components set to zero is considered invalid.
         */
        bool IsValid() const
        {
            return ((A | B | C | D) != 0);
        }
        
        /**
         * Converts this GUID to its string representation.=
         */
        Text ToString() const
        {
            return ToString(EGuidFormats::Digits);
        }
        
        /**
         * Converts this GUID to its string representation using the specified format.
         */
        NEKO_ENGINE_API Text ToString(EGuidFormats Format) const;
        
        //private:
    public:
        
        uint32 A;
        uint32 B;
        uint32 C;
        uint32 D;
    };
} // namespace Neko
