//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  PluginManager.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    class IEngine;
    class IPlugin;
    class InputBlob;
    class OutputBlob;
    class JsonSerializer;
    class Universe;
    
    template <typename T> class TArray;
    template <typename T> class DelegateList;
    
    /// Interface for plugin manager. Can load dynamic and static plugins.
    class NEKO_ENGINE_API IPluginManager
    {
    public:
        
        virtual ~IPluginManager() { }
        
        /** Initialises plugin manager. */
        static IPluginManager* Create(IEngine& engine);
        static void Destroy(IPluginManager* Manager);
        
        /** Loads the plugin. */
        virtual IPlugin* Load(const char* Path) = 0;
        
        /** Adds loaded plugin to the system. */
        virtual void AddPlugin(IPlugin* Plugin) = 0;
        
        /** Update plugin system. */
        virtual void Update(float DeltaTime, bool bPaused) = 0;
        
        virtual void Serialize(OutputBlob& Serializer) = 0;
        virtual void Deserialize(InputBlob& Serializer) = 0;
        
        /** Finds the plugin by its name. */
        virtual IPlugin* GetPlugin(const char* NEKO_RESTRICT Name) = 0;
        
        /** Returns all loaded plugins. */
        virtual const TArray<IPlugin*>& GetPlugins() const = 0;
        
        /** Returns all loaded plugin libraries (owners). */
        virtual const TArray<void*>& GetLibraries() const = 0;
        
        /** Callback, gets called when library has been loaded. */
        virtual DelegateList<void(void*)>& OnLibraryLoaded() = 0;
    };
} // ~namespace Neko
