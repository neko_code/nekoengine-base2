//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  JobSystem.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Data/IAllocator.h"

namespace Neko
{
    class IAllocator;
    
    namespace JobSystem
    {
        struct NEKO_ENGINE_API JobDecl
        {
            void (*task)(void*);
            void* data;
        };
        
        NEKO_ENGINE_API bool Init(IAllocator& Allocator);
        NEKO_ENGINE_API void Shutdown();
        NEKO_ENGINE_API void RunJobs(const JobDecl* jobs, int count, int volatile* counter);
        NEKO_ENGINE_API void Wait(int volatile* Counter);
        
        struct NEKO_ENGINE_API LambdaJob : JobDecl
        {
            LambdaJob()
            {
                data = pool;
            }
            ~LambdaJob()
            {
                if (data != pool)
                {
                    allocator->Deallocate(data);
                }
            }
            uint8 pool[64];
            IAllocator* allocator;
        };
        
        template <typename T> void LambdaInvoker(void* data)
        {
            (*(T*)data)();
        }
        
        template<typename T>
        void FromLambda(T lambda, LambdaJob* job, JobDecl* jobDecl, IAllocator* allocator)
        {
            job->allocator = allocator;
            if (sizeof(lambda) <= sizeof(job->pool))
            {
                job->data = job->pool;
            }
            else
            {
                assert(allocator);
                job->data = allocator->Allocate(sizeof(T));
            }
            new (NewPlaceholder(), job->data) T(lambda);
            job->task = &LambdaInvoker<T>;
            *jobDecl = *job;
        }
    } // namespace JobSystem
} // namespace Neko
