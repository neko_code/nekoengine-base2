//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Debug.h
//  Neko engine
//
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Data/IAllocator.h"
#include "../Mt/Sync.h"

namespace Neko
{
    namespace Debug
    {
        struct PlatformSymbolInfo
        {
            char Filename[1024];
            char FunctionName[1024];
            char ModuleName[1024];
            
            int32 LineNumber;
        };
        
        void NEKO_ENGINE_API DebugBreak();
        void NEKO_ENGINE_API DebugOutput(const char* Message);
        
        class StackNode;
        
        /// Stack tree. This is used to keep a trace on allocations.
        class NEKO_ENGINE_API StackTree
        {
        public:
            
            StackTree();
            
            ~StackTree();
            
            /** Collects stack traces using system API and pushes info to the tree. */
            StackNode* Record();
            
            /** Prints the call stack starting from the given node. */
            void PrintCallstack(StackNode* Node);
            
            /** Extracts the function name from the node. */
            static bool GetFunction(StackNode* Node, char* out, int MaxSize, int* Line);
            /** Returns node's owner. */
            static StackNode* GetParent(StackNode* Node);
            
            static int GetPath(StackNode* Node, StackNode** Output, int MaxSize);
            
            static void RefreshModuleList();
            
        private:
            
            StackNode* InsertChildren(StackNode* Node, void** Instruction, void** Stack);
            
        private:
            
            StackNode* Root;
            static int32 Instances;
        };
        
        /// Debug allocator object. This is the proxy for any another allocator. On any operation it will record the information (e.g. call stack where allocation did happen).
        class NEKO_ENGINE_API Allocator : public IAllocator
        {
        public:
            
            struct AllocationInfo
            {
                AllocationInfo* previous;
                AllocationInfo* next;
                size_t size;
                StackNode* stack_leaf;
                uint16 align;
            };
            
        public:
            
            explicit Allocator(IAllocator& source);
            
            virtual ~Allocator();
            
            void* Allocate(size_t size) override;
            void Deallocate(void* ptr) override;
            void* Reallocate(void* ptr, size_t size) override;
            void* AllocateAligned(size_t size, size_t align) override;
            void DeallocateAligned(void* ptr) override;
            void* ReallocateAligned(void* ptr, size_t size, size_t align) override;
            
            size_t GetTotalSize() const { return TotalSize; }
            
            void CheckGuards();
            
            IAllocator& GetSourceAllocator() { return Source; }
            AllocationInfo* GetFirstAllocationInfo() const { return Root; }
            
            void Lock();
            void Unlock();
            
        private:
            
            inline size_t GetAllocationOffset();
            inline AllocationInfo* GetAllocationInfoFromSystem(void* system_ptr);
            inline AllocationInfo* GetAllocationInfoFromUser(void* user_ptr);
            inline uint8* GetUserFromSystem(void* system_ptr, size_t align);
            inline uint8* GetSystemFromUser(void* user_ptr);
            inline size_t GetNeededMemory(size_t size);
            inline size_t GetNeededMemory(size_t size, size_t align);
            inline void* GetUserPtrFromAllocationInfo(AllocationInfo* info);
            
        private:
            
            IAllocator& Source;
            class StackTree StackTree;
            MT::SpinMutex Mutex;
            AllocationInfo* Root;
            AllocationInfo Sentinels[2];
            size_t TotalSize;
            bool IsFillEnabled;
            bool AreGuardsEnabled;
        };
    } // Debug
    
    NEKO_ENGINE_API void EnablePlatformSymbolication(bool bEnable);
    NEKO_ENGINE_API void EnableCrashReporting(bool enable);
    NEKO_ENGINE_API void InstallUnhandledExceptionHandler();
} // Neko
