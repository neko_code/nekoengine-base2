//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Reflection.h
//  Neko engine
//
//  Copyright © 2018 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Data/Blob.h"
#include "../Math/Vec.h"
#include "../Resource/Resource.h"
#include "../Universe/Component.h"
#include "../Utilities/MetaProgramming.h"

#define NEKO_PROP(Scene, Getter, Setter) \
    &Scene::Getter, #Scene "::" #Getter, &Scene::Setter, #Scene "::" #Setter

#define NEKO_FUNC(Func) \
    &Func, #Func

namespace Neko
{
    template <typename T> class TArray;
    struct IAllocator;
    class CPath;
    class PropertyDescriptorBase;
    struct Int2;
    struct Vec2;
    struct Vec3;
    struct Vec4;
    
    namespace Reflection
    {
        namespace inner
        {
            template <typename T> void WriteToStream(OutputBlob& stream, T value)
            {
                stream.Write(value);
            };
            
            template <typename T> T ReadFromStream(InputBlob& stream)
            {
                return stream.Read<T>();
            };
            
            template <> NEKO_ENGINE_API CPath ReadFromStream(InputBlob& stream);
            template <> NEKO_ENGINE_API void WriteToStream(OutputBlob& stream, CPath);
            template <> NEKO_ENGINE_API void WriteToStream<const CPath&>(OutputBlob& stream, const CPath& path);
            template <> NEKO_ENGINE_API const char* ReadFromStream<const char*>(InputBlob& stream);
            template <> NEKO_ENGINE_API void WriteToStream<const char*>(OutputBlob& stream, const char* path);
            
            template <typename Getter> struct GetterProxy;
            
            template <typename R, typename C>
            struct GetterProxy<R(C::*)(ComponentHandle, int)>
            {
                using Getter = R(C::*)(ComponentHandle, int);
                static void invoke(OutputBlob& stream, C* inst, Getter getter, ComponentHandle component, int index)
                {
                    R value = (inst->*getter)(component, index);
                    WriteToStream(stream, value);
                }
            };
            
            template <typename R, typename C>
            struct GetterProxy<R(C::*)(ComponentHandle)>
            {
                using Getter = R(C::*)(ComponentHandle);
                static void invoke(OutputBlob& stream, C* inst, Getter getter, ComponentHandle component, int index)
                {
                    R value = (inst->*getter)(component);
                    WriteToStream(stream, value);
                }
            };
            
            
            template <typename Setter> struct SetterProxy;
            
            template <typename C, typename A>
            struct SetterProxy<void (C::*)(ComponentHandle, int, A)>
            {
                using Setter = void (C::*)(ComponentHandle, int, A);
                static void invoke(InputBlob& stream, C* inst, Setter setter, ComponentHandle component, int index)
                {
                    using Value = RemoveCR<A>;
                    auto value = ReadFromStream<Value>(stream);
                    (inst->*setter)(component, index, value);
                }
            };
            
            template <typename C, typename A>
            struct SetterProxy<void (C::*)(ComponentHandle, A)>
            {
                using Setter = void (C::*)(ComponentHandle, A);
                static void invoke(InputBlob& stream, C* inst, Setter setter, ComponentHandle component, int index)
                {
                    using Value = RemoveCR<A>;
                    auto value = ReadFromStream<Value>(stream);
                    (inst->*setter)(component, value);
                }
            };
        } // namespace inner
        
        struct IAttribute
        {
            enum Type
            {
                MIN,
                CLAMP,
                RADIANS,
                COLOR,
                RESOURCE
            };
            
            virtual int GetType() const = 0;
        };
        
        struct IAttributeVisitor
        {
            virtual void visit(const IAttribute& attr) = 0;
        };
        
        struct ResourceAttribute : IAttribute
        {
            ResourceAttribute(const char* file_type, ResourceType type) { this->file_type = file_type; this->type = type; }
            ResourceAttribute() { }
            
            int GetType() const override { return RESOURCE; }
            
            const char* file_type;
            ResourceType type;
        };
        
        struct MinAttribute : IAttribute
        {
            MinAttribute(float min) { this->min = min; }
            MinAttribute() { }
            
            int GetType() const override { return MIN; }
            
            float min;
        };
        
        struct ClampAttribute : IAttribute
        {
            ClampAttribute(float min, float max) { this->min = min; this->max = max; }
            ClampAttribute() { }
            
            int GetType() const override { return CLAMP; }
            
            float min;
            float max;
        };
        
        struct RadiansAttribute : IAttribute
        {
            int GetType() const override { return RADIANS; }
        };
        
        struct ColorAttribute : IAttribute
        {
            int GetType() const override { return COLOR; }
        };
        
        struct PropertyBase
        {
            virtual void visit(IAttributeVisitor& visitor) const = 0;
            virtual void SetValue(ComponentUID component, int index, InputBlob& stream) const = 0;
            virtual void GetValue(ComponentUID component, int index, OutputBlob& stream) const = 0;
            
            const char* name;
            const char* GetterCode = "";
            const char* SetterCode = "";
        };
        
        template <typename T> struct Property : PropertyBase { };
        
        struct IBlobProperty : PropertyBase { };
        
        struct ISampledFuncProperty : PropertyBase
        {
            virtual float GetMaxX() const = 0;
        };
        
        struct IEnumProperty : public PropertyBase
        {
            void visit(IAttributeVisitor& visitor) const override {}
            virtual int GetEnumCount(ComponentUID component) const = 0;
            virtual const char* GetEnumName(ComponentUID component, int index) const = 0;
        };
        
        struct ComponentBase;
        struct IPropertyVisitor;
        struct SceneBase;
        
        struct IArrayProperty : PropertyBase
        {
            virtual bool CanAddRemove() const = 0;
            virtual void AddItem(ComponentUID component, int index) const = 0;
            virtual void RemoveItem(ComponentUID component, int index) const = 0;
            virtual int GetCount(ComponentUID component) const = 0;
            virtual void visit(IPropertyVisitor& visitor) const = 0;
        };
        
        struct IPropertyVisitor
        {
            virtual void begin(const ComponentBase&) {}
            virtual void visit(const Property<float>& prop) = 0;
            virtual void visit(const Property<int>& prop) = 0;
            virtual void visit(const Property<Entity>& prop) = 0;
            virtual void visit(const Property<Int2>& prop) = 0;
            virtual void visit(const Property<Vec2>& prop) = 0;
            virtual void visit(const Property<Vec3>& prop) = 0;
            virtual void visit(const Property<Vec4>& prop) = 0;
            virtual void visit(const Property<CPath>& prop) = 0;
            virtual void visit(const Property<bool>& prop) = 0;
            virtual void visit(const Property<const char*>& prop) = 0;
            virtual void visit(const IArrayProperty& prop) = 0;
            virtual void visit(const IEnumProperty& prop) = 0;
            virtual void visit(const IBlobProperty& prop) = 0;
            virtual void visit(const ISampledFuncProperty& prop) = 0;
            virtual void end(const ComponentBase&) {}
        };
        
        struct ISimpleComponentVisitor : IPropertyVisitor
        {
            virtual void VisitProperty(const PropertyBase& prop) = 0;
            
            void visit(const Property<float>& prop) override { VisitProperty(prop); }
            void visit(const Property<int>& prop) override { VisitProperty(prop); }
            void visit(const Property<Entity>& prop) override { VisitProperty(prop); }
            void visit(const Property<Int2>& prop) override { VisitProperty(prop); }
            void visit(const Property<Vec2>& prop) override { VisitProperty(prop); }
            void visit(const Property<Vec3>& prop) override { VisitProperty(prop); }
            void visit(const Property<Vec4>& prop) override { VisitProperty(prop); }
            void visit(const Property<CPath>& prop) override { VisitProperty(prop); }
            void visit(const Property<bool>& prop) override { VisitProperty(prop); }
            void visit(const Property<const char*>& prop) override { VisitProperty(prop); }
            void visit(const IArrayProperty& prop) override { VisitProperty(prop); }
            void visit(const IEnumProperty& prop) override { VisitProperty(prop); }
            void visit(const IBlobProperty& prop) override { VisitProperty(prop); }
            void visit(const ISampledFuncProperty& prop) override { VisitProperty(prop); }
        };
        
        struct IFunctionVisitor
        {
            virtual void visit(const struct FunctionBase& func) = 0;
        };
        
        
        struct ComponentBase
        {
            virtual int GetPropertyCount() const = 0;
            virtual int GetFunctionCount() const = 0;
            virtual void visit(IPropertyVisitor&) const = 0;
            virtual void visit(IFunctionVisitor&) const = 0;
            
            const char* name;
            ComponentType componentType;
        };
        
        template <typename Getter, typename Setter, typename Namer>
        struct EnumProperty : IEnumProperty
        {
            void GetValue(ComponentUID component, int index, OutputBlob& stream) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                static_assert(4 == sizeof(typename ResultOf<Getter>::Type), "enum must have 4 bytes");
                inner::GetterProxy<Getter>::invoke(stream, inst, getter, component.handle, index);
            }
            
            void SetValue(ComponentUID component, int index, InputBlob& stream) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                
                static_assert(4 == sizeof(typename ResultOf<Getter>::Type), "enum must have 4 bytes");
                inner::SetterProxy<Setter>::invoke(stream, inst, setter, component.handle, index);
            }
        
            int GetEnumCount(ComponentUID component) const override
            {
                return count;
            }
            
            const char* GetEnumName(ComponentUID component, int index) const override
            {
                return namer(index);
            }
            
            Getter getter;
            Setter setter;
            int count;
            Namer namer;
        };
        
        template <typename Getter, typename Setter, typename Counter, typename Namer>
        struct DynEnumProperty : IEnumProperty
        {
            void GetValue(ComponentUID component, int index, OutputBlob& stream) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                static_assert(4 == sizeof(ResultOf<Getter>::Type), "enum must have 4 bytes");
                inner::GetterProxy<Getter>::invoke(stream, inst, getter, component.handle, index);
            }
            
            void SetValue(ComponentUID component, int index, InputBlob& stream) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                
                static_assert(4 == sizeof(ResultOf<Getter>::Type), "enum must have 4 bytes");
                inner::SetterProxy<Setter>::invoke(stream, inst, setter, component.handle, index);
            }
            
            int GetEnumCount(ComponentUID component) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                return (inst->*counter)();
            }
            
            const char* GetEnumName(ComponentUID component, int index) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                return (inst->*namer)(index);
            }
            
            Getter getter;
            Setter setter;
            Counter counter;
            Namer namer;
        };
        
        template <typename Getter, typename Setter, typename Counter>
        struct SampledFuncProperty : ISampledFuncProperty
        {
            void GetValue(ComponentUID component, int index, OutputBlob& stream) const override
            {
                assert(index == -1);
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                int count = (inst->*counter)(component.handle);
                stream.Write(count);
                const Vec2* values = (inst->*getter)(component.handle);
                stream.Write(values, sizeof(float) * 2 * count);
            }
            
            void SetValue(ComponentUID component, int index, InputBlob& stream) const override
            {
                assert(index == -1);
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                int count;
                stream.Read(count);
                auto* buf = (const Vec2*)stream.Skip(sizeof(float) * 2 * count);
                (inst->*setter)(component.handle, buf, count);
            }
            
            float GetMaxX() const override { return fMaxX; }
            
            void visit(IAttributeVisitor& visitor) const override {}
            
            Getter getter;
            Setter setter;
            Counter counter;
            float fMaxX;
        };
        
        template <typename Getter, typename Setter, typename... Attributes>
        struct BlobProperty : IBlobProperty
        {
            void GetValue(ComponentUID component, int index, OutputBlob& stream) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                (inst->*getter)(component.handle, stream);
            }
            
            void SetValue(ComponentUID component, int index, InputBlob& stream) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                (inst->*setter)(component.handle, stream);
            }
            
            void visit(IAttributeVisitor& visitor) const override
            {
                auto f = [&](auto& x) { visitor.visit(x); };
                apply(f, attributes);
            }
            
            Tuple<Attributes...> attributes;
            Getter getter;
            Setter setter;
        };
        
        template <typename T, typename Getter, typename Setter, typename... Attributes>
        struct CommonProperty : Property<T>
        {
            void visit(IAttributeVisitor& visitor) const override
            {
                auto f = [&](auto& x) { visitor.visit(x); };
                apply(f, attributes);
            }
            
            void GetValue(ComponentUID component, int index, OutputBlob& stream) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                inner::GetterProxy<Getter>::invoke(stream, inst, getter, component.handle, index);
            }
            
            void SetValue(ComponentUID component, int index, InputBlob& stream) const override
            {
                using C = typename ClassOf<Getter>::Type;
                C* inst = static_cast<C*>(component.scene);
                inner::SetterProxy<Setter>::invoke(stream, inst, setter, component.handle, index);
            }
            
            
            Tuple<Attributes...> attributes;
            Getter getter;
            Setter setter;
        };
        
        template <typename Counter, typename Adder, typename Remover, typename... Reflection>
        struct ArrayProperty : IArrayProperty
        {
            ArrayProperty() { }
            
            bool CanAddRemove() const override
            {
                return true;
            }
            
            void SetValue(ComponentUID component, int index, InputBlob& stream) const override
            {
                assert(index == -1);
                
                int count;
                stream.Read(count);
                while (GetCount(component) < count)
                {
                    AddItem(component, -1);
                }
                while (GetCount(component) > count)
                {
                    RemoveItem(component, GetCount(component) - 1);
                }
                for (int i = 0; i < count; ++i)
                {
                    struct : ISimpleComponentVisitor
                    {
                        void VisitProperty(const PropertyBase& prop) override
                        {
                            prop.SetValue(component, index, *stream);
                        }
                        
                        InputBlob* stream;
                        int index;
                        ComponentUID component;
                        
                    } v;
                    v.stream = &stream;
                    v.index = i;
                    v.component = component;
                    visit(v);
                }
            }
            
            void GetValue(ComponentUID component, int index, OutputBlob& stream) const override
            {
                assert(index == -1);
                int count = GetCount(component);
                stream.Write(count);
                for (int i = 0; i < count; ++i)
                {
                    struct : ISimpleComponentVisitor
                    {
                        void VisitProperty(const PropertyBase& prop) override
                        {
                            prop.GetValue(component, index, *stream);
                        }
                        
                        OutputBlob* stream;
                        int index;
                        ComponentUID component;
                        
                    } v;
                    v.stream = &stream;
                    v.index = i;
                    v.component = component;
                    visit(v);
                }
            }
            
            void AddItem(ComponentUID component, int index) const override
            {
                using C = typename ClassOf<Counter>::Type;
                C* inst = static_cast<C*>(component.scene);
                (inst->*adder)(component.handle, index);
            }
            
            void RemoveItem(ComponentUID component, int index) const override
            {
                using C = typename ClassOf<Counter>::Type;
                C* inst = static_cast<C*>(component.scene);
                (inst->*remover)(component.handle, index);
            }
            
            int GetCount(ComponentUID component) const override
            {
                using C = typename ClassOf<Counter>::Type;
                C* inst = static_cast<C*>(component.scene);
                return (inst->*counter)(component.handle);
            }
            
            void visit(IPropertyVisitor& visitor) const override
            {
                auto f =[&](auto& x) { visitor.visit(x); };
                apply(f, properties);
            }
            
            void visit(IAttributeVisitor& visitor) const override {}
            
            Tuple<Reflection...> properties;
            Counter counter;
            Adder adder;
            Remover remover;
        };
        
        template <typename Counter, typename... Reflection>
        struct ConstArrayProperty : IArrayProperty
        {
            ConstArrayProperty() {}
            
            bool CanAddRemove() const override { return false; }
            
            void SetValue(ComponentUID component, int index, InputBlob& stream) const override
            {
                assert(index == -1);
                
                int count;
                stream.Read(count);
                if (GetCount(component) != count)
                {
                    return;
                }
                
                for (int i = 0; i < count; ++i)
                {
                    struct : ISimpleComponentVisitor
                    {
                        void VisitProperty(const PropertyBase& prop) override
                        {
                            prop.SetValue(component, index, *stream);
                        }
                        
                        InputBlob* stream;
                        int index;
                        ComponentUID component;
                        
                    } v;
                    v.stream = &stream;
                    v.index = i;
                    v.component = component;
                    visit(v);
                }
            }
            
            void GetValue(ComponentUID component, int index, OutputBlob& stream) const override
            {
                assert(index == -1);
                int count = GetCount(component);
                stream.Write(count);
                for (int i = 0; i < count; ++i)
                {
                    struct : ISimpleComponentVisitor
                    {
                        void VisitProperty(const PropertyBase& prop) override
                        {
                            prop.GetValue(component, index, *stream);
                        }
                        
                        OutputBlob* stream;
                        int index;
                        ComponentUID component;
                        
                    } v;
                    v.stream = &stream;
                    v.index = i;
                    v.component = component;
                    visit(v);
                }
            }
            
            void AddItem(ComponentUID component, int index) const override { assert(false); }
            void RemoveItem(ComponentUID component, int index) const override { assert(false); }
            
            int GetCount(ComponentUID component) const override
            {
                using C = typename ClassOf<Counter>::Type;
                C* inst = static_cast<C*>(component.scene);
                return (inst->*counter)(component.handle);
            }
            
            void visit(IPropertyVisitor& visitor) const override
            {
                auto f = [&](auto& x) { visitor.visit(x); };
                apply(f, properties);
            }
            
            void visit(IAttributeVisitor& visitor) const override
            {
            }
            
            Tuple<Reflection...> properties;
            Counter counter;
        };
        
        
        NEKO_ENGINE_API void Init(IAllocator& allocator);
        NEKO_ENGINE_API void Shutdown();
        
        NEKO_ENGINE_API const IAttribute* GetAttribute(const PropertyBase& prop, IAttribute::Type type);
        NEKO_ENGINE_API void RegisterComponent(const ComponentBase& desc);
        NEKO_ENGINE_API const ComponentBase* GetComponent(ComponentType componentType);
        NEKO_ENGINE_API const PropertyBase* GetProperty(ComponentType componentType, const char* property);
        NEKO_ENGINE_API const PropertyBase* GetProperty(ComponentType componentType, uint32 property_name_hash);
        NEKO_ENGINE_API const PropertyBase* GetProperty(ComponentType componentType, const char* property, const char* subproperty);
        
        NEKO_ENGINE_API ComponentType GetComponentType(const char* id);
        NEKO_ENGINE_API uint32 GetComponentTypeHash(ComponentType type);
        NEKO_ENGINE_API ComponentType GetComponentTypeFromHash(uint32 hash);
        NEKO_ENGINE_API int GetComponentTypesCount();
        NEKO_ENGINE_API const char* GetComponentTypeID(int index);
        
        NEKO_ENGINE_API void registerScene(const SceneBase& scene);
        
        NEKO_ENGINE_API int GetScenesCount();
        NEKO_ENGINE_API const SceneBase& GetScene(int index);
        
       
        struct IComponentVisitor
        {
            virtual void visit(const ComponentBase& cmp) = 0;
        };
        
        
        struct SceneBase
        {
            virtual int GetFunctionCount() const = 0;
            virtual void visit(IFunctionVisitor&) const = 0;
            virtual void visit(IComponentVisitor& visitor) const = 0;
            
            const char* name;
        };
        
        
        template <typename Components, typename Funcs>
        struct Scene : SceneBase
        {
            int GetFunctionCount() const override { return TupleSize<Funcs>::result; }
            
            
            void visit(IFunctionVisitor& visitor) const override
            {
                apply([&](const auto& func) { visitor.visit(func); }, functions);
            }
            
            
            void visit(IComponentVisitor& visitor) const override
            {
                apply([&](const auto& cmp) { visitor.visit(cmp); }, components);
            }
            
            
            Components components;
            Funcs functions;
        };
        
        template <typename Funcs, typename Props>
        struct Component : ComponentBase
        {
            int GetPropertyCount() const override { return TupleSize<Props>::result; }
            int GetFunctionCount() const override { return TupleSize<Funcs>::result; }
            
            void visit(IPropertyVisitor& visitor) const override
            {
                visitor.begin(*this);
                auto f = [&](auto& x) { visitor.visit(x); };
                apply(f, properties);
                visitor.end(*this);
            }
            
            void visit(IFunctionVisitor& visitor) const override
            {
                apply([&](auto& x) { visitor.visit(x); }, functions);
            }
            
            
            Props properties;
            Funcs functions;
        };
        
        template <typename... Components, typename... Funcs>
        auto scene(const char* name, Tuple<Funcs...> funcs, Components... components)
        {
            Scene<Tuple<Components...>, Tuple<Funcs...>> scene;
            scene.name = name;
            scene.functions = funcs;
            scene.components = MakeTuple(components...);
            return scene;
        }
        
        template <typename... Components>
        auto scene(const char* name, Components... components)
        {
            Scene<Tuple<Components...>, Tuple<>> scene;
            scene.name = name;
            scene.components = MakeTuple(components...);
            return scene;
        }
        
        
        struct FunctionBase
        {
            const char* decl_code;
            
            virtual int GetArgCount() const = 0;
            virtual const char* GetReturnType() const = 0;
            virtual const char* GetArgType(int i) const = 0;
        };
        
        
        namespace internal
        {
            static const unsigned int FRONT_SIZE = sizeof("Neko::Properties::internal::GetTypeNameHelper<") - 1u;
            static const unsigned int BACK_SIZE = sizeof(">::GetTypeName") - 1u;
            
            template <typename T>
            struct GetTypeNameHelper
            {
                static const char* GetTypeName(void)
                {
#if NEKO_WINDOWS_FAMILY
                    static const size_t size = sizeof(__FUNCTION__) - FRONT_SIZE - BACK_SIZE;
                    static char typeName[size] = {};
                    CopyMemory(typeName, __FUNCTION__ + FRONT_SIZE, size - 1u);
#else
                    static const size_t size = sizeof(__PRETTY_FUNCTION__) - FRONT_SIZE - BACK_SIZE;
                    static char typeName[size] = {};
                    CopyMemory(typeName, __PRETTY_FUNCTION__ + FRONT_SIZE, size - 1u);
#endif
                    
                    return typeName;
                }
            };
        }
        
        
        template <typename T>
        const char* GetTypeName(void)
        {
            return internal::GetTypeNameHelper<T>::GetTypeName();
        }
        
        
        template <typename F> struct Function;
        
        
        template <typename R, typename C, typename... Args>
        struct Function<R (C::*)(Args...)> : FunctionBase
        {
            using F = R(C::*)(Args...);
            F function;
            
            int GetArgCount() const override { return ArgCount<F>::result; }
            const char* GetReturnType() const override { return GetTypeName<typename ResultOf<F>::Type>(); }
            
            const char* GetArgType(int i) const override
            {
                const char* expand[] = {
                    GetTypeName<Args>()...
                };
                return expand[i];
            }
        };
        
        
        template <typename F>
        auto function(F func, const char* decl_code)
        {
            Function<F> ret;
            ret.function = func;
            ret.decl_code = decl_code;
            return ret;
        }
        
        template <typename... F>
        auto functions(F... functions)
        {
            Tuple<F...> f = MakeTuple(functions...);
            return f;
        }
        
        template <typename... Props, typename... Funcs>
        auto component(const char* name, Tuple<Funcs...> functions, Props... props)
        {
            Component<Tuple<Funcs...>, Tuple<Props...>> cmp;
            cmp.name = name;
            cmp.properties = MakeTuple(props...);
            cmp.componentType = Reflection::GetComponentType(name);
            return cmp;
        }
        
        template <typename... Props>
        auto component(const char* name, Props... props)
        {
            Component<Tuple<>, Tuple<Props...>> cmp;
            cmp.name = name;
            cmp.properties = MakeTuple(props...);
            cmp.componentType = GetComponentType(name);
            return cmp;
        }
        
        template <typename Getter, typename Setter, typename... Attributes>
        auto blob_property(const char* name, Getter getter, const char* GetterCode, Setter setter, const char* SetterCode, Attributes... attributes)
        {
            BlobProperty<Getter, Setter, Attributes...> p;
            p.attributes = MakeTuple(attributes...);
            p.getter = getter;
            p.setter = setter;
            p.GetterCode = GetterCode;
            p.SetterCode = SetterCode;
            p.name = name;
            return p;
        }
        
        template <typename Getter, typename Setter, typename Counter>
        auto sampled_func_property(const char* name, Getter getter, const char* GetterCode, Setter setter, const char* SetterCode, Counter counter, float fMaxX)
        {
            using R = typename ResultOf<Getter>::Type;
            SampledFuncProperty<Getter, Setter, Counter> p;
            p.getter = getter;
            p.setter = setter;
            p.GetterCode = GetterCode;
            p.SetterCode = SetterCode;
            p.counter = counter;
            p.name = name;
            p.fMaxX = fMaxX;
            return p;
        }
        
        template <typename Getter, typename Setter, typename... Attributes>
        auto property(const char* name, Getter getter, const char* GetterCode, Setter setter, const char* SetterCode, Attributes... attributes)
        {
            using R = typename ResultOf<Getter>::Type;
            CommonProperty<R, Getter, Setter, Attributes...> p;
            p.attributes = MakeTuple(attributes...);
            p.getter = getter;
            p.setter = setter;
            p.GetterCode = GetterCode;
            p.SetterCode = SetterCode;
            p.name = name;
            return p;
        }
        
        template <typename Getter, typename Setter, typename Namer, typename... Attributes>
        auto enum_property(const char* name, Getter getter, const char* GetterCode, Setter setter, const char* SetterCode, int count, Namer namer, Attributes... attributes)
        {
            EnumProperty<Getter, Setter, Namer> p;
            p.getter = getter;
            p.setter = setter;
            p.GetterCode = GetterCode;
            p.SetterCode = SetterCode;
            p.namer = namer;
            p.count = count;
            p.name = name;
            return p;
        }
        
        template <typename Getter, typename Setter, typename Counter, typename Namer, typename... Attributes>
        auto dyn_enum_property(const char* name, Getter getter, const char* GetterCode, Setter setter, const char* SetterCode, Counter counter, Namer namer, Attributes... attributes)
        {
            DynEnumProperty<Getter, Setter, Counter, Namer> p;
            p.getter = getter;
            p.setter = setter;
            p.GetterCode = GetterCode;
            p.SetterCode = SetterCode;
            p.namer = namer;
            p.counter = counter;
            p.name = name;
            return p;
        }
        
        template <typename Counter, typename Adder, typename Remover, typename... Reflection>
        auto array(const char* name, Counter counter, Adder adder, Remover remover, Reflection... properties)
        {
            ArrayProperty<Counter, Adder, Remover, Reflection...> p;
            p.name = name;
            p.counter = counter;
            p.adder = adder;
            p.remover = remover;
            p.properties = MakeTuple(properties...);
            return p;
        }
        
        template <typename Counter, typename... Reflection>
        auto const_array(const char* name, Counter counter, Reflection... properties)
        {
            ConstArrayProperty<Counter, Reflection...> p;
            p.name = name;
            p.counter = counter;
            p.properties = MakeTuple(properties...);
            return p;
        }
    } // namespace Reflection
}
