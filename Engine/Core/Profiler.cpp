//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Profiler.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Log.h"
#include "../Containers/HashMap.h"
#include "../Platform/Platform.h"
#include "../Utilities/Timer.h"
#include "../Mt/Sync.h"
#include "../Mt/Thread.h"
#include "Profiler.h"

namespace Neko
{
    namespace Profiler
    {
        /** Profiler information block data. */
        struct Section
        {
            struct Hit
            {
                uint64 Length;
                uint64 Start;
            };
            
            explicit Section(IAllocator& allocator)
            : allocator(allocator)
            , Hits(allocator)
            , Type(ESectionType::TIME)
            {
                
            }
            
            ~Section()
            {
                while (FirstChild)
                {
                    Section* child = FirstChild->Next;
                    NEKO_DELETE(allocator, FirstChild);
                    FirstChild = child;
                }
            }
            
            void frame()
            {
                Values.IntValue = 0;
                Hits.Clear();
                
                if (FirstChild)
                {
                    FirstChild->frame();
                }
                if (Next)
                {
                    Next->frame();
                }
            }
            
            IAllocator& allocator;
            
            Section* Parent;
            Section* Next;
            Section* FirstChild;
            
            const char* Name;
            TArray<Hit> Hits;
            ESectionType Type;
            
            union
            {
                float FloatValue;
                int IntValue;
            } Values;
        };
        
        const char* GetSectionName(Section* section)
        {
            return section->Name;
        }
        
        int32 GetSectionInt(Section* section)
        {
            return section->Values.IntValue;
        }
        
        ESectionType GetSectionType(Section* section)
        {
            return section->Type;
        }
        
        Section* GetSectionFirstChild(Section* section)
        {
            return section->FirstChild;
        }
        
        Section* GetSectionNext(Section* section)
        {
            return section->Next;
        }
        
        uint64 GetSectionHitStart(Section* section, int32 HitIndex)
        {
            return section->Hits[HitIndex].Start;
        }
        
        uint64 GetSectionHitLength(Section* section, int32 HitIndex)
        {
            return section->Hits[HitIndex].Length;
        }
        
        int32 GetSectionHitCount(Section* section)
        {
            return section->Hits.GetSize();
        }
        
        struct ThreadData
        {
            ThreadData()
            {
                pRootBlock = pCurrentBlock = nullptr;
                name[0] = '\0';
            }
            
            Section* pRootBlock;
            Section* pCurrentBlock;
            char name[30];
        };
        
        
        struct Instance
        {
            Instance()
            : Threads(allocator)
            , FrameListeners(allocator)
            , Mutex(false)
            {
                Threads.Insert(MT::GetCurrentThreadID(), &MainThread);
                StartTime = Neko::Platform::GetTicks();
            }
            
            ~Instance()
            {
                for (auto* i : Threads)
                {
                    if (i != &MainThread)
                    {
                        NEKO_DELETE(allocator, i);
                    }
                }
            }
            
            
            NEKO_FORCE_INLINE int64 GetTimeSinceStart()
            {
                return Neko::Platform::GetTicks() - StartTime;
            }
            
            DefaultAllocator allocator;
            DelegateList<void()> FrameListeners;
            THashMap<ThreadID, ThreadData*> Threads;
            ThreadData MainThread;
           
            int64 StartTime;
            MT::SpinMutex Mutex;
        };
        
        
        Instance GInstance;
        
        
        float GetSectionLength(Section* block)
        {
            uint64 ret = 0;
            for(int32 i = 0, c = block->Hits.GetSize(); i < c; ++i)
            {
                ret += block->Hits[i].Length;
            }
            return float(ret / (double)Neko::Platform::GetTicksPerSecond());
        }
        
        
        struct SectionInfo
        {
            Section* block;
            ThreadData* threadData;
        };
        
        
        static SectionInfo GetSection(const char* name)
        {
            auto threadId = MT::GetCurrentThreadID();
            
            ThreadData* threadData = nullptr;
            {
                MT::SpinLock lock(GInstance.Mutex);
                auto iter = GInstance.Threads.Find(threadId);
                if (iter == GInstance.Threads.end())
                {
                    GInstance.Threads.Insert(threadId, NEKO_NEW(GInstance.allocator, ThreadData));
                    iter = GInstance.Threads.Find(threadId);
                }
                
                threadData = iter.value();
            }
            
            if (!threadData->pCurrentBlock)
            {
                Section* NEKO_RESTRICT root = threadData->pRootBlock;
                while (root && !EqualStrings(root->Name, name))
                {
                    root = root->Next;
                }
                if (root)
                {
                    threadData->pCurrentBlock = root;
                }
                else
                {
                    Section* root = NEKO_NEW(GInstance.allocator, Section)(GInstance.allocator);
                    root->Parent = nullptr;
                    root->Next = threadData->pRootBlock;
                    root->FirstChild = nullptr;
                    root->Name = name;
                    threadData->pRootBlock = threadData->pCurrentBlock = root;
                }
            }
            else
            {
                Section* NEKO_RESTRICT child = threadData->pCurrentBlock->FirstChild;
                while (child && !EqualStrings(child->Name, name))
                {
                    child = child->Next;
                }
                if (!child)
                {
                    child = NEKO_NEW(GInstance.allocator, Section)(GInstance.allocator);
                    child->Parent = threadData->pCurrentBlock;
                    child->FirstChild = nullptr;
                    child->Name = name;
                    child->Next = threadData->pCurrentBlock->FirstChild;
                    threadData->pCurrentBlock->FirstChild = child;
                }
                
                threadData->pCurrentBlock = child;
            }
            
            return { threadData->pCurrentBlock, threadData };
        }
        
        
        void Record(const char* name, int32 value)
        {
            auto data = GetSection(name);
            if (data.block->Type != ESectionType::INT)
            {
                data.block->Values.IntValue = 0;
                data.block->Type = ESectionType::INT;
            }
            data.block->Values.IntValue += value;
            data.threadData->pCurrentBlock = data.block->Parent;
        }
        
        void* BeginSection(const char* name)
        {
            auto data = GetSection(name);
            
            auto& hit = data.block->Hits.Emplace();
            hit.Start = GInstance.GetTimeSinceStart();
            hit.Length = 0;
            
            return data.block;
        }
        
        const char* GetThreadName(ThreadID threadId)
        {
            auto iter = GInstance.Threads.Find(threadId);
            if (iter == GInstance.Threads.end())
            {
                return "n/a";
            }
            return iter.value()->name;
        }
        
        void SetThreadName(const char* name)
        {
            MT::SpinLock lock(GInstance.Mutex);
            ThreadID threadId = MT::GetCurrentThreadID();
            auto iter = GInstance.Threads.Find(threadId);
            if (iter == GInstance.Threads.end())
            {
                GInstance.Threads.Insert(threadId, NEKO_NEW(GInstance.allocator, ThreadData));
                iter = GInstance.Threads.Find(threadId);
            }
            Neko::CopyString(iter.value()->name, name);
        }
        
        ThreadID GetThreadID(int32 index)
        {
            auto iter = GInstance.Threads.begin();
            auto end = GInstance.Threads.end();
            for (int32 i = 0; i < index; ++i)
            {
                ++iter;
                if (iter == end)
                {
                    return 0;
                }
            }
            return iter.key();
        }
        
        int32 GetThreadIndex(ThreadID id)
        {
            auto iter = GInstance.Threads.begin();
            auto end = GInstance.Threads.end();
            int32 idx = 0;
            while(iter != end)
            {
                if (iter.key() == id)
                {
                    return idx;
                }
                ++idx;
                ++iter;
            }
            return -1;
        }
        
        int32 GetThreadCount()
        {
            return GInstance.Threads.GetSize();
        }
        
        uint64 now()
        {
            return GInstance.GetTimeSinceStart();
        }
        
        Section* GetRootBlock(ThreadID threadId)
        {
            auto iter = GInstance.Threads.Find(threadId);
            if (!iter.IsValid())
            {
                return nullptr;
            }
            
            return iter.value()->pRootBlock;
        }
        
        void* EndSection()
        {
            auto threadId = MT::GetCurrentThreadID();
            
            ThreadData* threadData = nullptr;
            {
                MT::SpinLock lock(GInstance.Mutex);
                auto iter = GInstance.Threads.Find(threadId);
                assert(iter.IsValid());
                threadData = iter.value();
            }
            
            auto* block = threadData->pCurrentBlock;
            
            assert(block);
            
            uint64 now = GInstance.GetTimeSinceStart();
            threadData->pCurrentBlock->Hits.back().Length = now - threadData->pCurrentBlock->Hits.back().Start;
            threadData->pCurrentBlock = threadData->pCurrentBlock->Parent;
            
            return block;
        }
        
        Section* GetCurrentSection()
        {
            auto threadId = MT::GetCurrentThreadID();
            
            ThreadData* threadData = nullptr;
            {
                MT::SpinLock lock(GInstance.Mutex);
                auto iter = GInstance.Threads.Find(threadId);
                assert(iter.IsValid());
                threadData = iter.value();
            }
            
            return threadData->pCurrentBlock;
        }
        
        void Frame()
        {
            PROFILE_FUNCTION();
            
            MT::SpinLock lock(GInstance.Mutex);
            GInstance.FrameListeners.Invoke();
            uint64 now = GInstance.GetTimeSinceStart();
            
            for (auto* i : GInstance.Threads)
            {
                if (!i->pRootBlock)
                {
                    continue;
                }
                
                i->pRootBlock->frame();
                auto* block = i->pCurrentBlock;
                
                while (block)
                {
                    auto& hit = block->Hits.Emplace();
                    hit.Start = now;
                    hit.Length = 0;
                    block = block->Parent;
                }
            }
        }
        
        DelegateList<void()>& GetFrameListeners()
        {
            return GInstance.FrameListeners;
        }
    } // namespace Neko
} //	namespace Profiler
