//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  InputSystem.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    class IAllocator;
	class IEngine;
    struct Vec2;
    template <typename T> class DelegateList;
	
	enum class ETouchType
	{
		Began = 0,
		Moved,
		Ended
	};
	
	/** Touch gesture type. */
	enum ETouchGestureType
	{
		None = 0,
		Scroll,
		Magnify,
		Swipe,
		Rotate,
		Count
	};

    /** Key modifiers. */
    struct Modifier
    {
        enum Enum
        {
            None        = BIT(0),
            LeftAlt     = BIT(1),
            RightAlt    = BIT(2),
            LeftCtrl    = BIT(3),
            RightCtrl   = BIT(4),
            LeftShift   = BIT(6),
            RightShift  = BIT(7),
            LeftMeta    = BIT(8),
            RightMeta   = BIT(9),
            Num         = BIT(10),
            Caps        = BIT(11),
            Mode        = BIT(12)
        };
    };
    
    // Scancodes taken from SDL
    enum EScancode
    {
        SC_UNKNOWN = 0,

        // 0x07
        
        SC_A = 4,
        SC_B = 5,
        SC_C = 6,
        SC_D = 7,
        SC_E = 8,
        SC_F = 9,
        SC_G = 10,
        SC_H = 11,
        SC_I = 12,
        SC_J = 13,
        SC_K = 14,
        SC_L = 15,
        SC_M = 16,
        SC_N = 17,
        SC_O = 18,
        SC_P = 19,
        SC_Q = 20,
        SC_R = 21,
        SC_S = 22,
        SC_T = 23,
        SC_U = 24,
        SC_V = 25,
        SC_W = 26,
        SC_X = 27,
        SC_Y = 28,
        SC_Z = 29,
        
        SC_1 = 30,
        SC_2 = 31,
        SC_3 = 32,
        SC_4 = 33,
        SC_5 = 34,
        SC_6 = 35,
        SC_7 = 36,
        SC_8 = 37,
        SC_9 = 38,
        SC_0 = 39,
        
        SC_RETURN = 40,
        SC_ESCAPE = 41,
        SC_BACKSPACE = 42,
        SC_TAB = 43,
        SC_SPACE = 44,
        
        SC_MINUS = 45,
        SC_EQUALS = 46,
        SC_LEFTBRACKET = 47,
        SC_RIGHTBRACKET = 48,
        SC_BACKSLASH = 49,
        
        SC_NONUSHASH = 50,
        
        SC_SEMICOLON = 51,
        SC_APOSTROPHE = 52,
        SC_GRAVE = 53, // '~' or '`'
        
        SC_COMMA = 54,
        SC_PERIOD = 55,
        SC_SLASH = 56,
        
        SC_CAPSLOCK = 57,
        
        SC_F1 = 58,
        SC_F2 = 59,
        SC_F3 = 60,
        SC_F4 = 61,
        SC_F5 = 62,
        SC_F6 = 63,
        SC_F7 = 64,
        SC_F8 = 65,
        SC_F9 = 66,
        SC_F10 = 67,
        SC_F11 = 68,
        SC_F12 = 69,
        
        SC_PRINTSCREEN = 70,
        SC_SCROLLLOCK = 71,
        SC_PAUSE = 72,
        SC_INSERT = 73,
        
        SC_HOME = 74,
        SC_PAGEUP = 75,
        SC_DELETE = 76,
        SC_END = 77,
        SC_PAGEDOWN = 78,
        SC_RIGHT = 79,
        SC_LEFT = 80,
        SC_DOWN = 81,
        SC_UP = 82,
        
        SC_NUMLOCKCLEAR = 83,
        
        SC_KP_DIVIDE = 84,
        SC_KP_MULTIPLY = 85,
        SC_KP_MINUS = 86,
        SC_KP_PLUS = 87,
        SC_KP_ENTER = 88,
        SC_KP_1 = 89,
        SC_KP_2 = 90,
        SC_KP_3 = 91,
        SC_KP_4 = 92,
        SC_KP_5 = 93,
        SC_KP_6 = 94,
        SC_KP_7 = 95,
        SC_KP_8 = 96,
        SC_KP_9 = 97,
        SC_KP_0 = 98,
        SC_KP_PERIOD = 99,
        
        SC_NONUSBACKSLASH = 100,
        
        SC_APPLICATION = 101,
        SC_POWER = 102,
        
        SC_KP_EQUALS = 103,
        SC_F13 = 104,
        SC_F14 = 105,
        SC_F15 = 106,
        SC_F16 = 107,
        SC_F17 = 108,
        SC_F18 = 109,
        SC_F19 = 110,
        SC_F20 = 111,
        SC_F21 = 112,
        SC_F22 = 113,
        SC_F23 = 114,
        SC_F24 = 115,
        SC_EXECUTE = 116,
        SC_HELP = 117,
        SC_MENU = 118,
        SC_SELECT = 119,
        SC_STOP = 120,
        SC_AGAIN = 121,
        SC_UNDO = 122,
        SC_CUT = 123,
        SC_COPY = 124,
        SC_PASTE = 125,
        SC_FIND = 126,
        SC_MUTE = 127,
        SC_VOLUMEUP = 128,
        SC_VOLUMEDOWN = 129,

        
        SC_KP_COMMA = 133,
        SC_KP_EQUALSAS400 = 134,
        
        SC_INTERNATIONAL1 = 135, /**< used on Asian keyboards, see
                                            footnotes in USB doc */
        SC_INTERNATIONAL2 = 136,
        SC_INTERNATIONAL3 = 137, /**< Yen */
        SC_INTERNATIONAL4 = 138,
        SC_INTERNATIONAL5 = 139,
        SC_INTERNATIONAL6 = 140,
        SC_INTERNATIONAL7 = 141,
        SC_INTERNATIONAL8 = 142,
        SC_INTERNATIONAL9 = 143,
        SC_LANG1 = 144, /**< Hangul/English toggle */
        SC_LANG2 = 145, /**< Hanja conversion */
        SC_LANG3 = 146, /**< Katakana */
        SC_LANG4 = 147, /**< Hiragana */
        SC_LANG5 = 148, /**< Zenkaku/Hankaku */
        SC_LANG6 = 149, /**< reserved */
        SC_LANG7 = 150, /**< reserved */
        SC_LANG8 = 151, /**< reserved */
        SC_LANG9 = 152, /**< reserved */
        
        SC_ALTERASE = 153, /**< Erase-Eaze */
        SC_SYSREQ = 154,
        SC_CANCEL = 155,
        SC_CLEAR = 156,
        SC_PRIOR = 157,
        SC_RETURN2 = 158,
#undef SC_SEPARATOR
        SC_SEPARATOR = 159,
        SC_OUT = 160,
        SC_OPER = 161,
        SC_CLEARAGAIN = 162,
        SC_CRSEL = 163,
        SC_EXSEL = 164,
        
        SC_KP_00 = 176,
        SC_KP_000 = 177,
        SC_THOUSANDSSEPARATOR = 178,
        SC_DECIMALSEPARATOR = 179,
        SC_CURRENCYUNIT = 180,
        SC_CURRENCYSUBUNIT = 181,
        SC_KP_LEFTPAREN = 182,
        SC_KP_RIGHTPAREN = 183,
        SC_KP_LEFTBRACE = 184,
        SC_KP_RIGHTBRACE = 185,
        SC_KP_TAB = 186,
        SC_KP_BACKSPACE = 187,
        SC_KP_A = 188,
        SC_KP_B = 189,
        SC_KP_C = 190,
        SC_KP_D = 191,
        SC_KP_E = 192,
        SC_KP_F = 193,
        SC_KP_XOR = 194,
        SC_KP_POWER = 195,
        SC_KP_PERCENT = 196,
        SC_KP_LESS = 197,
        SC_KP_GREATER = 198,
        SC_KP_AMPERSAND = 199,
        SC_KP_DBLAMPERSAND = 200,
        SC_KP_VERTICALBAR = 201,
        SC_KP_DBLVERTICALBAR = 202,
        SC_KP_COLON = 203,
        SC_KP_HASH = 204,
        SC_KP_SPACE = 205,
        SC_KP_AT = 206,
        SC_KP_EXCLAM = 207,
        SC_KP_MEMSTORE = 208,
        SC_KP_MEMRECALL = 209,
        SC_KP_MEMCLEAR = 210,
        SC_KP_MEMADD = 211,
        SC_KP_MEMSUBTRACT = 212,
        SC_KP_MEMMULTIPLY = 213,
        SC_KP_MEMDIVIDE = 214,
        SC_KP_PLUSMINUS = 215,
        SC_KP_CLEAR = 216,
        SC_KP_CLEARENTRY = 217,
        SC_KP_BINARY = 218,
        SC_KP_OCTAL = 219,
        SC_KP_DECIMAL = 220,
        SC_KP_HEXADECIMAL = 221,
        
        SC_LCTRL = 224,
        SC_LSHIFT = 225,
        SC_LALT = 226, // option
        SC_LGUI = 227, // meta key
        SC_RCTRL = 228,
        SC_RSHIFT = 229,
        SC_RALT = 230, // option
        SC_RGUI = 231, // meta key
        
        SC_MODE = 257,
        
        // 0x0C

        SC_AUDIONEXT = 258,
        SC_AUDIOPREV = 259,
        SC_AUDIOSTOP = 260,
        SC_AUDIOPLAY = 261,
        SC_AUDIOMUTE = 262,
        SC_MEDIASELECT = 263,
        SC_WWW = 264,
        SC_MAIL = 265,
        SC_CALCULATOR = 266,
        SC_COMPUTER = 267,
        SC_AC_SEARCH = 268,
        SC_AC_HOME = 269,
        SC_AC_BACK = 270,
        SC_AC_FORWARD = 271,
        SC_AC_STOP = 272,
        SC_AC_REFRESH = 273,
        SC_AC_BOOKMARKS = 274,
        

        // Mac keyboard
        
        
        SC_BRIGHTNESSDOWN = 275,
        SC_BRIGHTNESSUP = 276,
        SC_DISPLAYSWITCH = 277, /**< display mirroring/dual display
                                           switch, video mode switch */
        SC_KBDILLUMTOGGLE = 278,
        SC_KBDILLUMDOWN = 279,
        SC_KBDILLUMUP = 280,
        SC_EJECT = 281,
        SC_SLEEP = 282,
        
        SC_APP1 = 283,
        SC_APP2 = 284,
        
        
        
        SC_COUNT = 512
    };
    
    typedef int32 EKeyCode;
    
#define SC_MASK (1 << 30)
#define SC_TO_KEYCODE(X)  (X | SC_MASK)
    
    enum
    {
        KC_UNKNOWN = 0,
        
        KC_RETURN = '\r',
        KC_ESCAPE = '\033',
        KC_BACKSPACE = '\b',
        KC_TAB = '\t',
        KC_SPACE = ' ',
        KC_EXCLAIM = '!',
        KC_QUOTEDBL = '"',
        KC_HASH = '#',
        KC_PERCENT = '%',
        KC_DOLLAR = '$',
        KC_AMPERSAND = '&',
        KC_QUOTE = '\'',
        KC_LEFTPAREN = '(',
        KC_RIGHTPAREN = ')',
        KC_ASTERISK = '*',
        KC_PLUS = '+',
        KC_COMMA = ',',
        KC_MINUS = '-',
        KC_PERIOD = '.',
        KC_SLASH = '/',
        KC_0 = '0',
        KC_1 = '1',
        KC_2 = '2',
        KC_3 = '3',
        KC_4 = '4',
        KC_5 = '5',
        KC_6 = '6',
        KC_7 = '7',
        KC_8 = '8',
        KC_9 = '9',
        KC_COLON = ':',
        KC_SEMICOLON = ';',
        KC_LESS = '<',
        KC_EQUALS = '=',
        KC_GREATER = '>',
        KC_QUESTION = '?',
        KC_AT = '@',
        /*
         Skip uppercase letters
         */
        KC_LEFTBRACKET = '[',
        KC_BACKSLASH = '\\',
        KC_RIGHTBRACKET = ']',
        KC_CARET = '^',
        KC_UNDERSCORE = '_',
        KC_BACKQUOTE = '`',
        KC_a = 'a',
        KC_b = 'b',
        KC_c = 'c',
        KC_d = 'd',
        KC_e = 'e',
        KC_f = 'f',
        KC_g = 'g',
        KC_h = 'h',
        KC_i = 'i',
        KC_j = 'j',
        KC_k = 'k',
        KC_l = 'l',
        KC_m = 'm',
        KC_n = 'n',
        KC_o = 'o',
        KC_p = 'p',
        KC_q = 'q',
        KC_r = 'r',
        KC_s = 's',
        KC_t = 't',
        KC_u = 'u',
        KC_v = 'v',
        KC_w = 'w',
        KC_x = 'x',
        KC_y = 'y',
        KC_z = 'z',
        
        KC_CAPSLOCK = SC_TO_KEYCODE(SC_CAPSLOCK),
        
        KC_F1 = SC_TO_KEYCODE(SC_F1),
        KC_F2 = SC_TO_KEYCODE(SC_F2),
        KC_F3 = SC_TO_KEYCODE(SC_F3),
        KC_F4 = SC_TO_KEYCODE(SC_F4),
        KC_F5 = SC_TO_KEYCODE(SC_F5),
        KC_F6 = SC_TO_KEYCODE(SC_F6),
        KC_F7 = SC_TO_KEYCODE(SC_F7),
        KC_F8 = SC_TO_KEYCODE(SC_F8),
        KC_F9 = SC_TO_KEYCODE(SC_F9),
        KC_F10 = SC_TO_KEYCODE(SC_F10),
        KC_F11 = SC_TO_KEYCODE(SC_F11),
        KC_F12 = SC_TO_KEYCODE(SC_F12),
        
        KC_PRINTSCREEN = SC_TO_KEYCODE(SC_PRINTSCREEN),
        KC_SCROLLLOCK = SC_TO_KEYCODE(SC_SCROLLLOCK),
        KC_PAUSE = SC_TO_KEYCODE(SC_PAUSE),
        KC_INSERT = SC_TO_KEYCODE(SC_INSERT),
        KC_HOME = SC_TO_KEYCODE(SC_HOME),
        KC_PAGEUP = SC_TO_KEYCODE(SC_PAGEUP),
        KC_DELETE = '\177',
        KC_END = SC_TO_KEYCODE(SC_END),
        KC_PAGEDOWN = SC_TO_KEYCODE(SC_PAGEDOWN),
        KC_RIGHT = SC_TO_KEYCODE(SC_RIGHT),
        KC_LEFT = SC_TO_KEYCODE(SC_LEFT),
        KC_DOWN = SC_TO_KEYCODE(SC_DOWN),
        KC_UP = SC_TO_KEYCODE(SC_UP),
        
        KC_NUMLOCKCLEAR = SC_TO_KEYCODE(SC_NUMLOCKCLEAR),
        KC_KP_DIVIDE = SC_TO_KEYCODE(SC_KP_DIVIDE),
        KC_KP_MULTIPLY = SC_TO_KEYCODE(SC_KP_MULTIPLY),
        KC_KP_MINUS = SC_TO_KEYCODE(SC_KP_MINUS),
        KC_KP_PLUS = SC_TO_KEYCODE(SC_KP_PLUS),
        KC_KP_ENTER = SC_TO_KEYCODE(SC_KP_ENTER),
        KC_KP_1 = SC_TO_KEYCODE(SC_KP_1),
        KC_KP_2 = SC_TO_KEYCODE(SC_KP_2),
        KC_KP_3 = SC_TO_KEYCODE(SC_KP_3),
        KC_KP_4 = SC_TO_KEYCODE(SC_KP_4),
        KC_KP_5 = SC_TO_KEYCODE(SC_KP_5),
        KC_KP_6 = SC_TO_KEYCODE(SC_KP_6),
        KC_KP_7 = SC_TO_KEYCODE(SC_KP_7),
        KC_KP_8 = SC_TO_KEYCODE(SC_KP_8),
        KC_KP_9 = SC_TO_KEYCODE(SC_KP_9),
        KC_KP_0 = SC_TO_KEYCODE(SC_KP_0),
        KC_KP_PERIOD = SC_TO_KEYCODE(SC_KP_PERIOD),
        
        KC_APPLICATION = SC_TO_KEYCODE(SC_APPLICATION),
        KC_POWER = SC_TO_KEYCODE(SC_POWER),
        KC_KP_EQUALS = SC_TO_KEYCODE(SC_KP_EQUALS),
        KC_F13 = SC_TO_KEYCODE(SC_F13),
        KC_F14 = SC_TO_KEYCODE(SC_F14),
        KC_F15 = SC_TO_KEYCODE(SC_F15),
        KC_F16 = SC_TO_KEYCODE(SC_F16),
        KC_F17 = SC_TO_KEYCODE(SC_F17),
        KC_F18 = SC_TO_KEYCODE(SC_F18),
        KC_F19 = SC_TO_KEYCODE(SC_F19),
        KC_F20 = SC_TO_KEYCODE(SC_F20),
        KC_F21 = SC_TO_KEYCODE(SC_F21),
        KC_F22 = SC_TO_KEYCODE(SC_F22),
        KC_F23 = SC_TO_KEYCODE(SC_F23),
        KC_F24 = SC_TO_KEYCODE(SC_F24),
        KC_EXECUTE = SC_TO_KEYCODE(SC_EXECUTE),
        KC_HELP = SC_TO_KEYCODE(SC_HELP),
        KC_MENU = SC_TO_KEYCODE(SC_MENU),
        KC_SELECT = SC_TO_KEYCODE(SC_SELECT),
        KC_STOP = SC_TO_KEYCODE(SC_STOP),
        KC_AGAIN = SC_TO_KEYCODE(SC_AGAIN),
        KC_UNDO = SC_TO_KEYCODE(SC_UNDO),
        KC_CUT = SC_TO_KEYCODE(SC_CUT),
        KC_COPY = SC_TO_KEYCODE(SC_COPY),
        KC_PASTE = SC_TO_KEYCODE(SC_PASTE),
        KC_FIND = SC_TO_KEYCODE(SC_FIND),
        KC_MUTE = SC_TO_KEYCODE(SC_MUTE),
        KC_VOLUMEUP = SC_TO_KEYCODE(SC_VOLUMEUP),
        KC_VOLUMEDOWN = SC_TO_KEYCODE(SC_VOLUMEDOWN),
        KC_KP_COMMA = SC_TO_KEYCODE(SC_KP_COMMA),
        KC_KP_EQUALSAS400 = SC_TO_KEYCODE(SC_KP_EQUALSAS400),
        
        KC_ALTERASE = SC_TO_KEYCODE(SC_ALTERASE),
        KC_SYSREQ = SC_TO_KEYCODE(SC_SYSREQ),
        KC_CANCEL = SC_TO_KEYCODE(SC_CANCEL),
        KC_CLEAR = SC_TO_KEYCODE(SC_CLEAR),
        KC_PRIOR = SC_TO_KEYCODE(SC_PRIOR),
        KC_RETURN2 = SC_TO_KEYCODE(SC_RETURN2),
        KC_SEPARATOR = SC_TO_KEYCODE(SC_SEPARATOR),
        KC_OUT = SC_TO_KEYCODE(SC_OUT),
        KC_OPER = SC_TO_KEYCODE(SC_OPER),
        KC_CLEARAGAIN = SC_TO_KEYCODE(SC_CLEARAGAIN),
        KC_CRSEL = SC_TO_KEYCODE(SC_CRSEL),
        KC_EXSEL = SC_TO_KEYCODE(SC_EXSEL),
        
        KC_KP_00 = SC_TO_KEYCODE(SC_KP_00),
        KC_KP_000 = SC_TO_KEYCODE(SC_KP_000),
        KC_THOUSANDSSEPARATOR = SC_TO_KEYCODE(SC_THOUSANDSSEPARATOR),
        KC_DECIMALSEPARATOR = SC_TO_KEYCODE(SC_DECIMALSEPARATOR),
        KC_CURRENCYUNIT = SC_TO_KEYCODE(SC_CURRENCYUNIT),
        KC_CURRENCYSUBUNIT = SC_TO_KEYCODE(SC_CURRENCYSUBUNIT),
        KC_KP_LEFTPAREN = SC_TO_KEYCODE(SC_KP_LEFTPAREN),
        KC_KP_RIGHTPAREN = SC_TO_KEYCODE(SC_KP_RIGHTPAREN),
        KC_KP_LEFTBRACE = SC_TO_KEYCODE(SC_KP_LEFTBRACE),
        KC_KP_RIGHTBRACE = SC_TO_KEYCODE(SC_KP_RIGHTBRACE),
        KC_KP_TAB = SC_TO_KEYCODE(SC_KP_TAB),
        KC_KP_BACKSPACE = SC_TO_KEYCODE(SC_KP_BACKSPACE),
        KC_KP_A = SC_TO_KEYCODE(SC_KP_A),
        KC_KP_B = SC_TO_KEYCODE(SC_KP_B),
        KC_KP_C = SC_TO_KEYCODE(SC_KP_C),
        KC_KP_D = SC_TO_KEYCODE(SC_KP_D),
        KC_KP_E = SC_TO_KEYCODE(SC_KP_E),
        KC_KP_F = SC_TO_KEYCODE(SC_KP_F),
        KC_KP_XOR = SC_TO_KEYCODE(SC_KP_XOR),
        KC_KP_POWER = SC_TO_KEYCODE(SC_KP_POWER),
        KC_KP_PERCENT = SC_TO_KEYCODE(SC_KP_PERCENT),
        KC_KP_LESS = SC_TO_KEYCODE(SC_KP_LESS),
        KC_KP_GREATER = SC_TO_KEYCODE(SC_KP_GREATER),
        KC_KP_AMPERSAND = SC_TO_KEYCODE(SC_KP_AMPERSAND),
        KC_KP_DBLAMPERSAND = SC_TO_KEYCODE(SC_KP_DBLAMPERSAND),
        KC_KP_VERTICALBAR = SC_TO_KEYCODE(SC_KP_VERTICALBAR),
        KC_KP_DBLVERTICALBAR = SC_TO_KEYCODE(SC_KP_DBLVERTICALBAR),
        KC_KP_COLON = SC_TO_KEYCODE(SC_KP_COLON),
        KC_KP_HASH = SC_TO_KEYCODE(SC_KP_HASH),
        KC_KP_SPACE = SC_TO_KEYCODE(SC_KP_SPACE),
        KC_KP_AT = SC_TO_KEYCODE(SC_KP_AT),
        KC_KP_EXCLAM = SC_TO_KEYCODE(SC_KP_EXCLAM),
        KC_KP_MEMSTORE = SC_TO_KEYCODE(SC_KP_MEMSTORE),
        KC_KP_MEMRECALL = SC_TO_KEYCODE(SC_KP_MEMRECALL),
        KC_KP_MEMCLEAR = SC_TO_KEYCODE(SC_KP_MEMCLEAR),
        KC_KP_MEMADD = SC_TO_KEYCODE(SC_KP_MEMADD),
        KC_KP_MEMSUBTRACT = SC_TO_KEYCODE(SC_KP_MEMSUBTRACT),
        KC_KP_MEMMULTIPLY = SC_TO_KEYCODE(SC_KP_MEMMULTIPLY),
        KC_KP_MEMDIVIDE = SC_TO_KEYCODE(SC_KP_MEMDIVIDE),
        KC_KP_PLUSMINUS = SC_TO_KEYCODE(SC_KP_PLUSMINUS),
        KC_KP_CLEAR = SC_TO_KEYCODE(SC_KP_CLEAR),
        KC_KP_CLEARENTRY = SC_TO_KEYCODE(SC_KP_CLEARENTRY),
        KC_KP_BINARY = SC_TO_KEYCODE(SC_KP_BINARY),
        KC_KP_OCTAL = SC_TO_KEYCODE(SC_KP_OCTAL),
        KC_KP_DECIMAL = SC_TO_KEYCODE(SC_KP_DECIMAL),
        KC_KP_HEXADECIMAL =
        SC_TO_KEYCODE(SC_KP_HEXADECIMAL),
        
        KC_LCTRL = SC_TO_KEYCODE(SC_LCTRL),
        KC_LSHIFT = SC_TO_KEYCODE(SC_LSHIFT),
        KC_LALT = SC_TO_KEYCODE(SC_LALT),
        KC_LGUI = SC_TO_KEYCODE(SC_LGUI),
        KC_RCTRL = SC_TO_KEYCODE(SC_RCTRL),
        KC_RSHIFT = SC_TO_KEYCODE(SC_RSHIFT),
        KC_RALT = SC_TO_KEYCODE(SC_RALT),
        KC_RGUI = SC_TO_KEYCODE(SC_RGUI),
        
        KC_MODE = SC_TO_KEYCODE(SC_MODE),
        
        KC_AUDIONEXT = SC_TO_KEYCODE(SC_AUDIONEXT),
        KC_AUDIOPREV = SC_TO_KEYCODE(SC_AUDIOPREV),
        KC_AUDIOSTOP = SC_TO_KEYCODE(SC_AUDIOSTOP),
        KC_AUDIOPLAY = SC_TO_KEYCODE(SC_AUDIOPLAY),
        KC_AUDIOMUTE = SC_TO_KEYCODE(SC_AUDIOMUTE),
        KC_MEDIASELECT = SC_TO_KEYCODE(SC_MEDIASELECT),
        KC_WWW = SC_TO_KEYCODE(SC_WWW),
        KC_MAIL = SC_TO_KEYCODE(SC_MAIL),
        KC_CALCULATOR = SC_TO_KEYCODE(SC_CALCULATOR),
        KC_COMPUTER = SC_TO_KEYCODE(SC_COMPUTER),
        KC_AC_SEARCH = SC_TO_KEYCODE(SC_AC_SEARCH),
        KC_AC_HOME = SC_TO_KEYCODE(SC_AC_HOME),
        KC_AC_BACK = SC_TO_KEYCODE(SC_AC_BACK),
        KC_AC_FORWARD = SC_TO_KEYCODE(SC_AC_FORWARD),
        KC_AC_STOP = SC_TO_KEYCODE(SC_AC_STOP),
        KC_AC_REFRESH = SC_TO_KEYCODE(SC_AC_REFRESH),
        KC_AC_BOOKMARKS = SC_TO_KEYCODE(SC_AC_BOOKMARKS),
        
        KC_BRIGHTNESSDOWN = SC_TO_KEYCODE(SC_BRIGHTNESSDOWN),
        KC_BRIGHTNESSUP = SC_TO_KEYCODE(SC_BRIGHTNESSUP),
        KC_DISPLAYSWITCH = SC_TO_KEYCODE(SC_DISPLAYSWITCH),
        KC_KBDILLUMTOGGLE = SC_TO_KEYCODE(SC_KBDILLUMTOGGLE),
        KC_KBDILLUMDOWN = SC_TO_KEYCODE(SC_KBDILLUMDOWN),
        KC_KBDILLUMUP = SC_TO_KEYCODE(SC_KBDILLUMUP),
        KC_EJECT = SC_TO_KEYCODE(SC_EJECT),
        KC_SLEEP = SC_TO_KEYCODE(SC_SLEEP)
    };
   
    static const char *GScancodeNames[SC_COUNT] =
    {
        NULL, NULL, NULL, NULL,
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "0",
        "Return",
        "Escape",
        "Backspace",
        "Tab",
        "Space",
        "-",
        "=",
        "[",
        "]",
        "\\",
        "#",
        ";",
        "'",
        "`",
        ",",
        ".",
        "/",
        "CapsLock",
        "F1",
        "F2",
        "F3",
        "F4",
        "F5",
        "F6",
        "F7",
        "F8",
        "F9",
        "F10",
        "F11",
        "F12",
        "PrintScreen",
        "ScrollLock",
        "Pause",
        "Insert",
        "Home",
        "PageUp",
        "Delete",
        "End",
        "PageDown",
        "Right",
        "Left",
        "Down",
        "Up",
        "Numlock",
        "Keypad /",
        "Keypad *",
        "Keypad -",
        "Keypad +",
        "Keypad Enter",
        "Keypad 1",
        "Keypad 2",
        "Keypad 3",
        "Keypad 4",
        "Keypad 5",
        "Keypad 6",
        "Keypad 7",
        "Keypad 8",
        "Keypad 9",
        "Keypad 0",
        "Keypad .",
        NULL,
        "Application",
        "Power",
        "Keypad =",
        "F13",
        "F14",
        "F15",
        "F16",
        "F17",
        "F18",
        "F19",
        "F20",
        "F21",
        "F22",
        "F23",
        "F24",
        "Execute",
        "Help",
        "Menu",
        "Select",
        "Stop",
        "Again",
        "Undo",
        "Cut",
        "Copy",
        "Paste",
        "Find",
        "Mute",
        "VolumeUp",
        "VolumeDown",
        NULL, NULL, NULL,
        "Keypad ,",
        "Keypad = (AS400)",
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL,
        "AltErase",
        "SysReq",
        "Cancel",
        "Clear",
        "Prior",
        "Return",
        "Separator",
        "Out",
        "Oper",
        "Clear / Again",
        "CrSel",
        "ExSel",
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        "Keypad 00",
        "Keypad 000",
        "ThousandsSeparator",
        "DecimalSeparator",
        "CurrencyUnit",
        "CurrencySubUnit",
        "Keypad (",
        "Keypad )",
        "Keypad {",
        "Keypad }",
        "Keypad Tab",
        "Keypad Backspace",
        "Keypad A",
        "Keypad B",
        "Keypad C",
        "Keypad D",
        "Keypad E",
        "Keypad F",
        "Keypad XOR",
        "Keypad ^",
        "Keypad %",
        "Keypad <",
        "Keypad >",
        "Keypad &",
        "Keypad &&",
        "Keypad |",
        "Keypad ||",
        "Keypad :",
        "Keypad #",
        "Keypad Space",
        "Keypad @",
        "Keypad !",
        "Keypad MemStore",
        "Keypad MemRecall",
        "Keypad MemClear",
        "Keypad MemAdd",
        "Keypad MemSubtract",
        "Keypad MemMultiply",
        "Keypad MemDivide",
        "Keypad +/-",
        "Keypad Clear",
        "Keypad ClearEntry",
        "Keypad Binary",
        "Keypad Octal",
        "Keypad Decimal",
        "Keypad Hexadecimal",
        NULL, NULL,
        "Left Ctrl",
        "Left Shift",
        "Left Alt",
        "Left GUI",
        "Right Ctrl",
        "Right Shift",
        "Right Alt",
        "Right GUI",
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL,
        "ModeSwitch",
        "AudioNext",
        "AudioPrev",
        "AudioStop",
        "AudioPlay",
        "AudioMute",
        "MediaSelect",
        "WWW",
        "Mail",
        "Calculator",
        "Computer",
        "AC Search",
        "AC Home",
        "AC Back",
        "AC Forward",
        "AC Stop",
        "AC Refresh",
        "AC Bookmarks",
        "BrightnessDown",
        "BrightnessUp",
        "DisplaySwitch",
        "KBDIllumToggle",
        "KBDIllumDown",
        "KBDIllumUp",
        "Eject",
        "Sleep",
    };
    
	typedef enum
	{
		KMOD_NONE = 0x0000,
		KMOD_LSHIFT = 0x0001,
		KMOD_RSHIFT = 0x0002,
		KMOD_LCTRL = 0x0040,
		KMOD_RCTRL = 0x0080,
		KMOD_LALT = 0x0100,
		KMOD_RALT = 0x0200,
		KMOD_LGUI = 0x0400,
		KMOD_RGUI = 0x0800,
		KMOD_NUM = 0x1000,
		KMOD_CAPS = 0x2000,
		KMOD_MODE = 0x4000,
		KMOD_RESERVED = 0x8000
	} SDL_Keymod;
	
#define KMOD_CTRL   (Neko::KMOD_LCTRL | Neko::KMOD_RCTRL)
#define KMOD_SHIFT  (Neko::KMOD_LSHIFT | Neko::KMOD_RSHIFT)
#define KMOD_ALT    (Neko::KMOD_LALT | Neko::KMOD_RALT)
#define KMOD_GUI    (Neko::KMOD_LGUI | Neko::KMOD_RGUI)
	
    /// Mouse button states.
    enum EMouseButtons
    {
        MouseButton_None = 0,
        MouseButton_Left,
        MouseButton_Right,
        MouseButton_Middle,

		Thumb01,
		Thumb02,
        
        MouseButton_Count
    };

    struct KeymapBase
    {
        const EKeyCode GDefaultKeymap[SC_COUNT] =
        {
            0, 0, 0, 0,
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '0',
            KC_RETURN,
            KC_ESCAPE,
            KC_BACKSPACE,
            KC_TAB,
            KC_SPACE,
            '-',
            '=',
            '[',
            ']',
            '\\',
            '#',
            ';',
            '\'',
            '`',
            ',',
            '.',
            '/',
            KC_CAPSLOCK,
            KC_F1,
            KC_F2,
            KC_F3,
            KC_F4,
            KC_F5,
            KC_F6,
            KC_F7,
            KC_F8,
            KC_F9,
            KC_F10,
            KC_F11,
            KC_F12,
            KC_PRINTSCREEN,
            KC_SCROLLLOCK,
            KC_PAUSE,
            KC_INSERT,
            KC_HOME,
            KC_PAGEUP,
            KC_DELETE,
            KC_END,
            KC_PAGEDOWN,
            KC_RIGHT,
            KC_LEFT,
            KC_DOWN,
            KC_UP,
            KC_NUMLOCKCLEAR,
            KC_KP_DIVIDE,
            KC_KP_MULTIPLY,
            KC_KP_MINUS,
            KC_KP_PLUS,
            KC_KP_ENTER,
            KC_KP_1,
            KC_KP_2,
            KC_KP_3,
            KC_KP_4,
            KC_KP_5,
            KC_KP_6,
            KC_KP_7,
            KC_KP_8,
            KC_KP_9,
            KC_KP_0,
            KC_KP_PERIOD,
            0,
            KC_APPLICATION,
            KC_POWER,
            KC_KP_EQUALS,
            KC_F13,
            KC_F14,
            KC_F15,
            KC_F16,
            KC_F17,
            KC_F18,
            KC_F19,
            KC_F20,
            KC_F21,
            KC_F22,
            KC_F23,
            KC_F24,
            KC_EXECUTE,
            KC_HELP,
            KC_MENU,
            KC_SELECT,
            KC_STOP,
            KC_AGAIN,
            KC_UNDO,
            KC_CUT,
            KC_COPY,
            KC_PASTE,
            KC_FIND,
            KC_MUTE,
            KC_VOLUMEUP,
            KC_VOLUMEDOWN,
            0, 0, 0,
            KC_KP_COMMA,
            KC_KP_EQUALSAS400,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            KC_ALTERASE,
            KC_SYSREQ,
            KC_CANCEL,
            KC_CLEAR,
            KC_PRIOR,
            KC_RETURN2,
            KC_SEPARATOR,
            KC_OUT,
            KC_OPER,
            KC_CLEARAGAIN,
            KC_CRSEL,
            KC_EXSEL,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            KC_KP_00,
            KC_KP_000,
            KC_THOUSANDSSEPARATOR,
            KC_DECIMALSEPARATOR,
            KC_CURRENCYUNIT,
            KC_CURRENCYSUBUNIT,
            KC_KP_LEFTPAREN,
            KC_KP_RIGHTPAREN,
            KC_KP_LEFTBRACE,
            KC_KP_RIGHTBRACE,
            KC_KP_TAB,
            KC_KP_BACKSPACE,
            KC_KP_A,
            KC_KP_B,
            KC_KP_C,
            KC_KP_D,
            KC_KP_E,
            KC_KP_F,
            KC_KP_XOR,
            KC_KP_POWER,
            KC_KP_PERCENT,
            KC_KP_LESS,
            KC_KP_GREATER,
            KC_KP_AMPERSAND,
            KC_KP_DBLAMPERSAND,
            KC_KP_VERTICALBAR,
            KC_KP_DBLVERTICALBAR,
            KC_KP_COLON,
            KC_KP_HASH,
            KC_KP_SPACE,
            KC_KP_AT,
            KC_KP_EXCLAM,
            KC_KP_MEMSTORE,
            KC_KP_MEMRECALL,
            KC_KP_MEMCLEAR,
            KC_KP_MEMADD,
            KC_KP_MEMSUBTRACT,
            KC_KP_MEMMULTIPLY,
            KC_KP_MEMDIVIDE,
            KC_KP_PLUSMINUS,
            KC_KP_CLEAR,
            KC_KP_CLEARENTRY,
            KC_KP_BINARY,
            KC_KP_OCTAL,
            KC_KP_DECIMAL,
            KC_KP_HEXADECIMAL,
            0, 0,
            KC_LCTRL,
            KC_LSHIFT,
            KC_LALT,
            KC_LGUI,
            KC_RCTRL,
            KC_RSHIFT,
            KC_RALT,
            KC_RGUI,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            KC_MODE,
            KC_AUDIONEXT,
            KC_AUDIOPREV,
            KC_AUDIOSTOP,
            KC_AUDIOPLAY,
            KC_AUDIOMUTE,
            KC_MEDIASELECT,
            KC_WWW,
            KC_MAIL,
            KC_CALCULATOR,
            KC_COMPUTER,
            KC_AC_SEARCH,
            KC_AC_HOME,
            KC_AC_BACK,
            KC_AC_FORWARD,
            KC_AC_STOP,
            KC_AC_REFRESH,
            KC_AC_BOOKMARKS,
            KC_BRIGHTNESSDOWN,
            KC_BRIGHTNESSUP,
            KC_DISPLAYSWITCH,
            KC_KBDILLUMTOGGLE,
            KC_KBDILLUMDOWN,
            KC_KBDILLUMUP,
            KC_EJECT,
            KC_SLEEP,
        };
        
        // Tools
        void SetScancodeName(EScancode Scancode, const char* Name);
        EScancode GetScancodeFromName(const char* Name);
        const char* GetScancodeName(EScancode Scancode);
        EKeyCode GetKeyFromScancode(EScancode Scancode);
        const char* GetKeyName(EKeyCode Key);
        EScancode GetScancodeFromKey(EKeyCode Key);
        
        /** Sets the default keymap which we'll use, usually only the platform code should use this. */
        void SetKeymap(int32 Start, EKeyCode* Keys, int32 Length);
        /** Resets keymap if needed. */
        void GetDefaultKeymap(EKeyCode* Keymap);
        
        NEKO_FORCE_INLINE EKeyCode Get(int index) { return Keymap[index]; }
        
        //! Keyboard keymap is accessible on every platform interface
        EKeyCode Keymap[SC_COUNT];
    };
    
    extern struct KeymapBase GKeymapBase;
    
    /// Input system interface. 
    class NEKO_ENGINE_API IInputSystem
    {
    public:
        
        struct Device
        {
            enum Type : uint32
            {
                Mouse,
                Keyboard,
                Controller
            };
            
            Type type;
            int index = 0;
            
            virtual ~Device() { }
            
            virtual void Update(float deltaTime) = 0;
            virtual const char* GetName() const = 0;
        };
        
        
        struct ButtonEvent
        {
            uint32 KeyId;
            uint32 scancode;
            float AbsX;
            float AbsY;
            enum : uint32
            {
                UP,
                DOWN
            } state;
        };
        
        struct AxisEvent
        {
            enum Axis
            {
                LTRIGGER,
                RTRIGGER,
                LTHUMB,
                RTHUMB
            };
            
            float x;
            float y;
            float AbsX;
            float AbsY;
            Axis axis;
        };

        struct InputEvent
        {
            enum Type : uint32
            {
                Button,
                Axis,
                DeviceAdded,
                DeviceRemoved
            };
            
            Type type;
            Device* device;
            union EventData
            {
                ButtonEvent button;
                AxisEvent axis;
            } data;
        };
        
    public:

        static IInputSystem* Create(IEngine& Engine);
        static void Destroy(IInputSystem& System);
        
        virtual ~IInputSystem()
        { }
        
        /** Activates the input and parses the actions. */
        virtual void Enable(bool enabled) = 0;
        virtual void Update(float deltaTime) = 0;
        
        virtual IAllocator& GetAllocator() const = 0;
        
        /** Returns state for mouse buttons. Used for low-level ops (e.g. instead of 'GetActionValue', which is used by a game. */
        virtual bool GetMouseButtonState(EMouseButtons button) const = 0;
        /** Sets the mouse state. This is called from event handler. */
        virtual void SetMouseButtonState(EMouseButtons button, bool const isDown) = 0;
        
        /** Returns mouse X delta. */
        virtual float GetMouseXMove() const = 0;
        /** Returns mouse Y delta. */
        virtual float GetMouseYMove() const = 0;
        
        /** Returns mouse cursor position. */
        virtual Vec2 GetMousePos() const = 0;
        
        virtual void InjectEvent(const InputEvent& event) = 0;
        virtual void RemoveDevice(Device* device) = 0;
        virtual void AddDevice(Device* device) = 0;
        
        /** Returns the total amount of mountded input devices. */
        virtual int32 GetDevicesCount() const = 0;
        /** Returns the pointer to device at index. */
        virtual Device* GetDevice(int32 index) = 0;
        /** Returns basic mouse device. */
        virtual Device* GetMouseDevice() = 0;
        /** Returns basic keyboard device. */
        virtual Device* GetKeyboardDevice() = 0;
        /** Queued events count. */
        virtual int32 GetEventsCount() const = 0;
        /** Queued events. */
        virtual const InputEvent* GetEvents() const = 0;
        
        /** Whether or not to enable relative mode. */
        virtual void SetRelativeMouseMode(bool enabled) = 0;
        /** Returns true if mouse is currently in relative mode. */
        virtual bool IsRelativeMouseMode() = 0;
        
        /** Processes mouse events. This is called from event handler. */
        virtual void ProcessMouse(int32 X, int32 Y, int32 relativeX, int32 relativeY) = 0;
        
        /** Sets whether or not to show mouse cursor. */
        virtual void SetCursorShow(bool bShown) = 0;
 
        virtual void SetKeyState(EScancode key, bool isDown) = 0;
        virtual bool GetKeyState(EScancode key) const = 0;
        
        /** Returns current key modifier state flag. */
        virtual uint16 GetModifiersState() const = 0;
        
        /** Processes keyboard events. This is called from event handler. */
        virtual void OnKeyEvent(EScancode key, bool isDown) = 0;
		
        /** Returns processed mouse scroll value (kinematics already applied). */
		virtual Vec2 GetMouseScrollValue() = 0;
		
        /** Processes touch events (e.g. touchpads or any sensor-like surface). This is called from event handler. */
		virtual void OnTouchEvent(ETouchType touchType, ETouchGestureType gestureType, const Vec2 delta) = 0;
        
        /** Resets the input. */
        virtual void Reset() = 0;
    };
} // ~namespace Neko
