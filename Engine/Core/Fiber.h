//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Fiber.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    class IEngine;
    
    namespace Fiber
    {
#ifdef NEKO_WINDOWS
        typedef void* Handle;
        typedef void(__stdcall *FiberProc)(void*);
#else
        typedef void* Handle;
        typedef void (*FiberProc)(void*);
#endif
        constexpr void* INVALID_FIBER = nullptr;
        
        /** Initializes fiber base on the initializer thread. */
        void InitThread(Handle* Out);
        
        /*
         * This function creates a new coroutine.
         *
         * @param Proc   The coroutine code to be executed
         * @param Parameter A single pointer passed to the coro
         * @param StackSize Size of stack area in bytes
         * @return An initialised context
         */
        Handle Create(int StackSize, FiberProc Proc, void* Parameter);
        
        /** Destroys fiber. */
        void Destroy(Handle Fiber);
        
        /** Switches context. */
        void SwitchTo(Handle* From, Handle Fiber);
        
        void* GetParameter();
    } // namespace Fiber

} // namespace Neko
