//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Log.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Containers/DelegateList.h"
#include "../Utilities/StringUtil.h"
#include "../Utilities/Text.h"

#define DebugLog printf

namespace Neko
{
    class CLog;
    class CPath;
    
    class NEKO_ENGINE_API LogProxy
    {
    public:
        
        LogProxy(CLog& Log, const char* System, IAllocator& Allocator);
        
        ~LogProxy();
        
        // append operators
        LogProxy& operator << (const char* Message);
        LogProxy& operator << (float Message);
        LogProxy& operator << (int32 Message);
        LogProxy& operator << (uint32 Message);
        LogProxy& operator << (uint64 Message);
        LogProxy& operator << (const Text& Path);
        LogProxy& operator << (const CPath& Path);
        
        LogProxy& substring(const char* String, int32 Start, int32 Length);
        
        LogProxy(const LogProxy&);
        
    private:
        
        IAllocator& Allocator;
        Text System;
        Text Message;
        CLog& Log;
        
        void operator = (const LogProxy&);
    };
    
    /// Used for logging messages. Can be sorted in each group (e.g. error, warning, info).
    class NEKO_ENGINE_API CLog
    {
    public:
        
        typedef DelegateList<void (const char*, const char*)> Callback;
        
    public:
        
        CLog()
        : Callbacks(Allocator)
        {
            
        }
        
        LogProxy log(const char* System);
        
        Callback& GetCallback();
        
    private:
        
        CLog(const CLog&);
        void operator = (const CLog&);
        
    private:
        
        DefaultAllocator Allocator;
        Callback Callbacks;
    };
    
    extern CLog NEKO_ENGINE_API GLogInfo;
    extern CLog NEKO_ENGINE_API GLogWarning;
    extern CLog NEKO_ENGINE_API GLogError;
} // Neko
