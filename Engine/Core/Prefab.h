//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Prefab.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Data/Blob.h"

#include "../Resource/Resource.h"
#include "../Resource/ResourceManager.h"
#include "../Resource/ResourceManagerBase.h"

#include "../Data/Streams.h"

namespace Neko
{
    enum class PrefabVersion : uint32
    {
        FIRST = 0,
        WITH_HIERARCHY, 
        
        LAST
    };
    
    /// Prefab resource implementation.
    struct PrefabResource final : public ResourceBase
    {
        PrefabResource(const class CPath& path, IResourceManagerBase& resourceManager, IAllocator& allocator)
        : ResourceBase(path, resourceManager, allocator)
        , blob(allocator)
        { }
        
        void Unload(void) override
        {
            blob.Clear();
        }
        
        bool Load(IStream& file) override
        {
            file.GetContents(blob);
            return true;
        }
        
        Neko::OutputBlob blob;
    };
    
    /// Prefab resource manager implementation.
    class CPrefabResourceManager final : public IResourceManagerBase
    {
    public:
        
        CPrefabResourceManager(IAllocator& allocator)
        : IResourceManagerBase(allocator)
        , Allocator(allocator)
        { }
        
    protected:
        
        ResourceBase* CreateResource(const CPath& path) override
        {
            return NEKO_NEW(Allocator, PrefabResource)(path, *this, Allocator);
        }
        
        void DestroyResource(ResourceBase& resource) override
        {
            return NEKO_DELETE(Allocator, &static_cast<PrefabResource&>(resource));
        }
        
    private:
        
        IAllocator& Allocator;
    };
    
} // namespace Neko
