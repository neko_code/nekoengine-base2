//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Reflection.cpp
//  Neko engine
//
//  Copyright © 2018 Neko Vision. All rights reserved.
//

#include "Reflection.h"
#include "AssociativeArray.h"
#include "Crc32.h"
#include "DefaultAllocator.h"
#include "Log.h"
#include "Path.h"

namespace Neko
{
    namespace Reflection
    {
        namespace inner
        {
            template <> CPath ReadFromStream<CPath>(InputBlob& stream)
            {
                const char* string = (const char*)stream.GetData() + stream.GetPosition();
                CPath path(string);
                stream.Skip(StringLength(string) + 1);
                return path;
            };
            
            template <> void WriteToStream<const CPath&>(OutputBlob& stream, const CPath& path)
            {
                const char* string = path.c_str();
                stream.Write(string, StringLength(string) + 1);
            };
            
            template <> void WriteToStream<CPath>(OutputBlob& stream, CPath path)
            {
                const char* string = path.c_str();
                stream.Write(string, StringLength(string) + 1);
            };
            
            template <> const char* ReadFromStream<const char*>(InputBlob& stream)
            {
                const char* string = (const char*)stream.GetData() + stream.GetPosition();
                stream.Skip(StringLength(string) + 1);
                return string;
            };
            
            template <> void WriteToStream<const char*>(OutputBlob& stream, const char* value)
            {
                stream.Write(value, StringLength(value) + 1);
            };
        }
        
        struct ComponentTypeData
        {
            char id[50];
            uint32 hash;
        };
        
        static IAllocator* GAllocator = nullptr;
        static const SceneBase* GScenes[64];
        static int GScenesCount = 0;
        
        struct ComponentLink
        {
            const ComponentBase* desc;
            ComponentLink* next;
        };
        
        static ComponentLink* GFirstComponent = nullptr;
        
        void Init(IAllocator& allocator)
        {
            GAllocator = &allocator;
        }
        
        static void Destroy(ComponentLink* link)
        {
            if (!link)
            {
                return;
            }
            Destroy(link->next);
            NEKO_DELETE(*GAllocator, link);
        }
        
        void Shutdown()
        {
            Destroy(GFirstComponent);
            GAllocator = nullptr;
        }
        
        const IAttribute* GetAttribute(const PropertyBase& prop, IAttribute::Type type)
        {
            struct AttrVisitor : IAttributeVisitor
            {
                void visit(const IAttribute& attr) override
                {
                    if (attr.GetType() == type)
                    {
                        result = &attr;
                    }
                }
                const IAttribute* result = nullptr;
                IAttribute::Type type;
            } attrVisitor;
            
            attrVisitor.type = type;
            prop.visit(attrVisitor);
            return attrVisitor.result;
        }
        
        const ComponentBase* GetComponent(ComponentType componentType)
        {
            ComponentLink* link = GFirstComponent;
            while (link)
            {
                if (link->desc->componentType == componentType)
                {
                    return link->desc;
                }
                link = link->next;
            }
            
            return nullptr;
        }
        
        const PropertyBase* GetProperty(ComponentType componentType, uint32 property_name_hash)
        {
            auto* component = GetComponent(componentType);
            if (!component)
            {
                return nullptr;
            }
            
            struct Visitor : ISimpleComponentVisitor
            {
                void VisitProperty(const PropertyBase& prop) override
                {
                    if (Crc32(prop.name) == property_name_hash)
                    {
                        result = &prop;
                    }
                }
                void visit(const IArrayProperty& prop) override
                {
                    if (result)
                    {
                        return;
                    }
                    VisitProperty(prop);
                    prop.visit(*this);
                }
                
                uint32 property_name_hash;
                const PropertyBase* result = nullptr;
            } visitor;
            visitor.property_name_hash = property_name_hash;
            component->visit(visitor);
            return visitor.result;
        }
        
        const PropertyBase* GetProperty(ComponentType componentType, const char* property, const char* subproperty)
        {
            auto* component = GetComponent(componentType);
            if (!component)
            {
                return nullptr;
            }
            
            struct Visitor : ISimpleComponentVisitor
            {
                void VisitProperty(const PropertyBase& prop) override
                {
                    
                }
                
                void visit(const IArrayProperty& prop) override
                {
                    if (EqualStrings(prop.name, property))
                    {
                        struct Subvisitor : ISimpleComponentVisitor
                        {
                            void VisitProperty(const PropertyBase& prop) override
                            {
                                if (EqualStrings(prop.name, property))
                                {
                                    result = &prop;
                                }
                            }
                            const char* property;
                            const PropertyBase* result = nullptr;
                        } subvisitor;
                        subvisitor.property = subproperty;
                        prop.visit(subvisitor);
                        result = subvisitor.result;
                    }
                }
                
                const char* subproperty;
                const char* property;
                const PropertyBase* result = nullptr;
            } visitor;
            visitor.subproperty = subproperty;
            visitor.property = property;
            component->visit(visitor);
            return visitor.result;
        }
        
        const PropertyBase* GetProperty(ComponentType componentType, const char* property)
        {
            auto* component = GetComponent(componentType);
            if (!component)
            {
                return nullptr;
            }
            
            struct Visitor : ISimpleComponentVisitor
            {
                void VisitProperty(const PropertyBase& prop) override
                {
                    if (EqualStrings(prop.name, property))
                    {
                        result = &prop;
                    }
                }
                
                const char* property;
                const PropertyBase* result = nullptr;
            } visitor;
            visitor.property = property;
            component->visit(visitor);
            return visitor.result;
        }
        
        void registerScene(const SceneBase& scene)
        {
            struct : IComponentVisitor
            {
                void visit(const ComponentBase& cmp) override
                {
                    RegisterComponent(cmp);
                }
            } visitor;
            scene.visit(visitor);
            
            assert(GScenesCount < lengthOf(GScenes));
            GScenes[GScenesCount] = &scene;
            ++GScenesCount;
        }
        
        void RegisterComponent(const ComponentBase& desc)
        {
            ComponentLink* link = NEKO_NEW(*GAllocator, ComponentLink);
            link->next = GFirstComponent;
            link->desc = &desc;
            GFirstComponent = link;
        }
        
        static TArray<ComponentTypeData>& GetComponentTypes()
        {
            static DefaultAllocator allocator;
            static TArray<ComponentTypeData> types(allocator);
            return types;
        }
        
        ComponentType GetComponentTypeFromHash(uint32 hash)
        {
            auto& types = GetComponentTypes();
            for (int i = 0, c = types.GetSize(); i < c; ++i)
            {
                if (types[i].hash == hash)
                {
                    return {i};
                }
            }
            assert(false);
            return { -1 };
        }
        
        uint32 GetComponentTypeHash(ComponentType type)
        {
            return GetComponentTypes()[type.index].hash;
        }
        
        ComponentType GetComponentType(const char* id)
        {
            uint32 hash = Crc32(id);
            auto& types = GetComponentTypes();
            for (int i = 0, c = types.GetSize(); i < c; ++i)
            {
                if (types[i].hash == hash)
                {
                    return { i };
                }
            }
            
            auto& componentTypes = GetComponentTypes();
            if (types.GetSize() == ComponentType::MAX_TYPES_COUNT)
            {
                GLogError.log("Engine") << "Too many component types.";
                return INVALID_COMPONENT_TYPE;
            }
            
            auto& type = componentTypes.Emplace();
            CopyString(type.id, id);
            type.hash = hash;
            return { GetComponentTypes().GetSize() - 1 };
        }
        
        int GetComponentTypesCount()
        {
            return GetComponentTypes().GetSize();
        }
        
        const char* GetComponentTypeID(int index)
        {
            return GetComponentTypes()[index].id;
        }
        
        int GetScenesCount()
        {
            return GScenesCount;
        }
        
        const SceneBase& GetScene(int index)
        {
            assert(index < lengthOf(GScenes) && GScenes[index]);
            return *GScenes[index];
        }
    } // namespace Reflection
} // namespace Neko
