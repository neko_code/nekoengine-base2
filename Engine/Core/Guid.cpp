#include "Guid.h"

namespace Neko
{
    Text SGuid::ToString(EGuidFormats Format) const
    {
        switch (Format)
        {
            case EGuidFormats::DigitsWithHyphens:
                return Neko::va("%08X-%04X-%04X-%04X-%04X%08X", A, B >> 16, B & 0xFFFF, C >> 16, C & 0xFFFF, D);
                
            case EGuidFormats::DigitsWithHyphensInBraces:
                return Neko::va("{%08X-%04X-%04X-%04X-%04X%08X}", A, B >> 16, B & 0xFFFF, C >> 16, C & 0xFFFF, D);
                
            case EGuidFormats::DigitsWithHyphensInParentheses:
                return Neko::va("(%08X-%04X-%04X-%04X-%04X%08X)", A, B >> 16, B & 0xFFFF, C >> 16, C & 0xFFFF, D);
                
            case EGuidFormats::HexValuesInBraces:
                return Neko::va("{0x%08X,0x%04X,0x%04X,{0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X}}", A, B >> 16, B & 0xFFFF, C >> 24, (C >> 16) & 0xFF, (C >> 8) & 0xFF, C & 0XFF, D >> 24, (D >> 16) & 0XFF, (D >> 8) & 0XFF, D & 0XFF);
                
            default:
                return Neko::va("%08X%08X%08X%08X", A, B, C, D);
        }
    }
} // Neko
