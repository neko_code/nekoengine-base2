//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  EventHandler.cpp
//  Neko engine
//
//  8/23/15
//  Copyright © 2015 Neko Vision. All rights reserved.
//

#include "EventHandler.h"
#include "../Data/IAllocator.h"
#include "../Core/Profiler.h"
#include "../Core/Log.h"
#include "../Mt/Thread.h"
#include "../Mt/Sync.h"
#include "../Platform/Application.h"

namespace Neko
{
    CEventDispatcher::CEventDispatcher(IEngine& InEngine)
    : OnEventCb(InEngine.GetAllocator())
    , Engine(InEngine)
    , ListenerRegistrationLock(false)
    {
        
    }
    
    CEventDispatcher::~CEventDispatcher()
    {
        // Every instance which uses our callback must unregister itself before
        assert(OnEventCb.GetNum() == 0);
    }
    
    void CEventDispatcher::ProcessOnAnyThread(Event* event)
    {
        // @todo Probably..
    }

    void CEventDispatcher::OnEvent(const Neko::Event* Event_)
    {
        // Event will be never null there
        assert(Event_ != nullptr);
        
        if (Neko::IsInGameThread())
        {
            OnEventCb.Invoke(Event_);
        }
    }
    
    void CEventDispatcher::Process()
    {
        PROFILE_SECTION("Event dispatch");
        
        IGenericWindow* handle = nullptr;
        
        const Event* event;
        do
        {
            struct SE
            {
                SE(IGenericWindow* handle)
                : Ev(MainApplication::Get().Poll(handle)) { }
                
                ~SE()
                {
                    if (Ev != nullptr)
                    {
                        MainApplication::Get().Release(Ev);
                    }
                }

                const Event* Ev;
            } scopeEvent(handle);
            event = scopeEvent.Ev;
            
            // Event is valid
            if (event)
            {
                handle = event->Handle;
                // Process main (system) thread events
                switch (event->Type)
                {
                    // Implementation for base events and components residing in engine library
					
                    case Event::Window:
                    {
                        const WindowEvent* window = static_cast<const WindowEvent*>(event);
                        
                        const bool bFocusGained = (window->Flags & WindowEvent::BecameActive) != 0;
                        const bool bFocusLost = (window->Flags & WindowEvent::BecameResigned) != 0;
                        
                        if (handle)
                        {
                            auto& definition = handle->GetDefinition();
                            // Check window activation/deactivation
                            if (bFocusGained)
                            {
                                definition.RuntimeFlags |= EWindowRuntimeFlags::HasInputFocus;
                            }
                            else if (bFocusLost)
                            {
                                definition.RuntimeFlags &= ~EWindowRuntimeFlags::HasInputFocus;
                            }
                        }
                        break;
                    }
                    // Silence the warning
                    default: break;
                }
				
				// Send event to all listeners
				OnEvent(event);
            }
        } while (event != nullptr);
    }
}
