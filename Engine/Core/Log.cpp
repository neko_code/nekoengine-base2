//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Log.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Log.h"
#include "Array.h"
#include "Path.h"
#include "StringUtil.h"

namespace Neko
{
    CLog GLogInfo;
    CLog GLogWarning;
    CLog GLogError;
    
    LogProxy CLog::log(const char* system)
    {
        return LogProxy(*this, system, Allocator);
    }
    
    CLog::Callback& CLog::GetCallback()
    {
        return Callbacks;
    }
    
    
    LogProxy::LogProxy(CLog& log, const char* system, IAllocator& allocator)
    : Allocator(allocator)
    , Log(log)
    , System(system, allocator)
    , Message(allocator)
    {
    }
    
    LogProxy::~LogProxy()
    {
        Log.GetCallback().Invoke(System.c_str(), Message.c_str());
    }
    
    LogProxy& LogProxy::substring(const char* str, int start, int length)
    {
        Message.Append(str + start, length);
        return *this;
    }
    
    LogProxy& LogProxy::operator<<(const char* message)
    {
        Message.Append(message);
        return *this;
    }
    
    LogProxy& LogProxy::operator<<(float message)
    {
        Message.Append(message);
        return *this;
    }
    
    LogProxy& LogProxy::operator<<(uint32 message)
    {
        Message.Append(message);
        return *this;
    }
    
    LogProxy& LogProxy::operator<<(uint64 message)
    {
        Message.Append(message);
        return *this;
    }
    
    LogProxy& LogProxy::operator<<(int32 message)
    {
        Message.Append(message);
        return *this;
    }
    
    LogProxy& LogProxy::operator<<(const Text& path)
    {
        Message.Append(path.c_str());
        return *this;
    }
    
    LogProxy& LogProxy::operator<<(const CPath& path)
    {
        Message.Append(path.c_str());
        return *this;
    }
} // Neko
