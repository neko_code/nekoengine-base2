//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  IPlugin.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Data/Serializer.h"
#include "../Neko.h"

namespace Neko
{
    class IEngine;
    class InputBlob;
    class IPlugin;
    class OutputBlob;
    class Universe;
    
    /// Scene interface for each plugin.
    class NEKO_ENGINE_API IScene
    {
    public:
        virtual ~IScene()
        {
            
        }
        
        virtual ComponentHandle CreateComponent(ComponentType, Entity) = 0;
		
        virtual void DestroyComponent(ComponentHandle Component, ComponentType Type) = 0;
		
        virtual void Serialize(OutputBlob& Serializer) = 0;
		
        virtual void Serialize(ISerializer& Serializer) { }
		
        virtual void Deserialize(IDeserializer& Serializer) { }
		
        virtual void Deserialize(InputBlob& Serializer) = 0;
		
        virtual IPlugin& GetPlugin() const = 0;
		
        virtual void Update(float TimeDelta, bool bPaused) = 0;
        
        virtual void PostUpdate(float TimeDelta, bool bPaused) { }; 
		
        virtual ComponentHandle GetComponent(Entity entity, ComponentType Type) = 0;
		
        virtual Universe& GetUniverse() = 0;
		
        virtual void StartGame()
        {
            
        }
		
        virtual void StopGame()
        {
            
        }
        virtual int32 GetVersion() const { return -1; }
        virtual void Clear() = 0;
    };
    
    /// Plugin interface.
    class NEKO_ENGINE_API IPlugin
    {
    public:
        virtual ~IPlugin();
        
        virtual void Serialize(OutputBlob&)
        {
        }
        
        virtual void Deserialize(InputBlob&)
        {
        }
        
        virtual void Update(float)
        {
        }
        virtual const char* GetName() const = 0;
        
        virtual void CreateScenes(Universe&)
        {
        }
        
        virtual void DestroyScene(IScene*)
        {
            assert(false);
        }
        
        virtual void StartGame()
        {
        }
        
        virtual void StopGame()
        {
        }
        
        virtual void PluginAdded(IPlugin& plugin)
        {
        }
    };
    
    struct NEKO_ENGINE_API StaticPluginRegister
    {
        typedef IPlugin* (*Creator)(IEngine& engine);
        StaticPluginRegister(const char* name, Creator creator);
        
        static IPlugin* Create(const char* name, IEngine& engine);
        
        StaticPluginRegister* next;
        Creator creator;
        const char* name;
    };
} // namespace Neko

#ifdef STATIC_PLUGINS
#   define NEKO_PLUGIN_ENTRY(plugin_name)                                           \
    extern "C" Neko::IPlugin* createPlugin_##plugin_name(Neko::IEngine& engine); \
    extern "C" { Neko::StaticPluginRegister NEKO_ATTRIBUTE_USED s_##plugin_name##_plugin_register(          \
#plugin_name, createPlugin_##plugin_name); }                              \
    extern "C" Neko::IPlugin* createPlugin_##plugin_name(Neko::IEngine& engine)
#else
#   define NEKO_PLUGIN_ENTRY(plugin_name) \
    extern "C" NEKO_LIBRARY_EXPORT Neko::IPlugin* createPlugin(Neko::IEngine& engine)
#endif

