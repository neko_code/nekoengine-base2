//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Joystick.cpp
//  Neko engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#include "Vec.h"
#include "Joystick.h"

namespace Neko
{
    CGamepad::CGamepad(const GamepadHardwareInfo InInfo)
    : AxesNum(InInfo.NumAxes)
    , BallsNum(InInfo.NumBalls)
    , ButtonNum(InInfo.NumButtons)
    , HatsNum(InInfo.NumHats)
    , bForceReset(false)
    , Hats(nullptr)
    , Balls(nullptr)
    , Axes(nullptr)
    , Buttons(nullptr)
    , Info(InInfo)
    , PowerLevel(EJoystickPowerLevel::Unknown)
    {

    }
    
    
    CGamepad::~CGamepad()
    {
#define SAFE_DELETE(Item)   \
        if (Item)           \
        {                   \
            free(Item);     \
            Item = nullptr; \
        }                   \

        // Check what needs to be freed.
        SAFE_DELETE(Axes);
        SAFE_DELETE(Hats);
        SAFE_DELETE(Balls);
        SAFE_DELETE(Buttons);
    }
    
    
    void CGamepad::Init(const int32 DeviceIndex)
    {
        if (AxesNum > 0)
        {
            Axes = (int16 *)malloc(AxesNum * sizeof(int16));
            memset(Axes, 0, AxesNum * sizeof(int16));
        }
        
        if (HatsNum > 0)
        {
            Hats = (uint8 *)malloc(HatsNum * sizeof(uint8));
            memset(Hats, 0, HatsNum * sizeof(uint8));
        }
        
        if (BallsNum > 0)
        {
            Balls = (Vec2 *)malloc(BallsNum * sizeof(Vec2));
            memset(Balls, 0, BallsNum * sizeof(Vec2));
        }
        
        if (ButtonNum > 0)
        {
            Buttons = (uint8 *)malloc(ButtonNum * sizeof(uint8));
            memset(Buttons, 0, ButtonNum * sizeof(uint8));
        }
        
        // Check memory.
        if (((AxesNum > 0) && !Axes) || ((BallsNum > 0) && !Balls) || ((HatsNum > 0) && !Hats) || ((ButtonNum > 0) && !Buttons))
        {
            assert(false);
        }
    }
    
    
    void CGamepad::OnJoystickAxisAction(uint8 axis, int16 value)
    {
        if (axis >= AxesNum)
        {
            return;
        }
        
        if (value == Axes[axis])
        {
            return;
        }
        
        Axes[axis] = value;
    }
    
    
    void CGamepad::OnJoystickButtonAction(uint8 button, uint8 state)
    {
        if (button >= ButtonNum)
        {
            return;
        }
        
        if (state == Buttons[button])
        {
            return;
        }
        
        Buttons[button] = state;
    }
    
    
    void CGamepad::OnJoystickHatAction(uint8 hat, uint8 value)
    {
        if (hat >= HatsNum)
        {
            return;
        }
        
        if (value == Hats[hat])
        {
            return;
        }
        
        Hats[hat] = value;
    }
    
    
    int16 CGamepad::GetAxis(int32 axis)
    {
        int16 state;
        
        if (axis < AxesNum)
        {
            state = Axes[axis];
        }
        else
        {
            state = 0;
        }
        
        return state;
    }
    
    
    uint8 CGamepad::GetHat(int32 hat)
    {
        uint8 state;
        
        if (hat < HatsNum)
        {
            state = Hats[hat];
        }
        else
        {
            state = 0;
        }
        
        return state;
    }
    
    
    int32 CGamepad::GetBall(int32 ball, int32* dx, int32* dy)
    {
        int32 retval;
        retval = 0;
        
        if (ball < BallsNum)
        {
            if (dx != nullptr)
            {
                *dx = Balls[ball].x;
            }
            
            if (dy != nullptr)
            {
                *dy = Balls[ball].y;
            }
            
            Balls[ball].x = 0;
            Balls[ball].y = 0;
        }
        else
        {
            return 0;
        }
        
        return retval;
    }
    
    
    uint8 CGamepad::GetButton(int32 button)
    {
        uint8 state;
        
        if (button < ButtonNum)
        {
            state = Buttons[button];
        }
        else
        {
            state = 0;
        }
        return state;
    }
    
    
    CGamepadController::CGamepadController(IAllocator& InAllocator)
    : JoystickHandle(nullptr)
    , bSupportsMapping(false)
    , Allocator(InAllocator)
    {
        memset(hatState, 0x0, sizeof(hatState));
        memset(&Mapping, 0x0, sizeof(SControllerMapping));
    }
    
    CGamepadController::~CGamepadController()
    {
        
    }
    
    
    uint8 CGamepadController::GetButtonMapped(EGameControllerButton button)
    {
        const int32 iButton = (int32)button;
        
        if (Mapping.iButtons[iButton] >= 0)
        {
            return JoystickHandle->GetButton(Mapping.iButtons[iButton]);
        }
        else if (Mapping.iAxesButton[iButton] >= 0)
        {
            int16 value;
            
            value = JoystickHandle->GetAxis(Mapping.iAxesButton[iButton]);
            
            if (Math::abs(value) > 32768 / 2) {
                return 1;
            }
            
            return 0;
        }
        else if (Mapping.buttonHat[iButton].Hat >= 0)
        {
            uint8 value;
            value = JoystickHandle->GetHat(Mapping.buttonHat[iButton].Hat);
            
            if (value & Mapping.buttonHat[iButton].Mask) {
                return 1;
            }
            
            return 0;
        }
        
        return 0;
    }
    
    
    int16 CGamepadController::GetAxisMapped(Neko::EGameControllerAxis axis)
    {
        const int32 iAxis = (const int32)axis;
        
        if (Mapping.iAxes[iAxis] >= 0)
        {
            int16 value = JoystickHandle->GetAxis(Mapping.iAxes[iAxis]);
            
            switch (axis)
            {
                    // Process them just like buttons
                case EGameControllerAxis::TriggerLeft:
                case EGameControllerAxis::TriggerRight:
                {
                    value = value / 2 + 16384;
                    break;
                }
                default:
                {
                    break;
                }
            }
            
            return value;
            
        }
        else if (Mapping.iButtonAxix[iAxis] >= 0)
        {
            uint8 value;
            
            value = JoystickHandle->GetButton(Mapping.iButtonAxix[iAxis]);
            if (value > 0)
            {
                return 32767;
            }
            
            return 0;
            
        }
        return 0;
    }
    
    
    void CGamepadController::OpenController(const int32 DeviceIndex, const GamepadHardwareInfo Info, SControllerMapping* NewMapping)
    {
        bSupportsMapping = true;
        
        // Create joystick.
        JoystickHandle = NEKO_NEW(Allocator, CGamepad)(Info);
        
        JoystickHandle->Init(DeviceIndex);
        
        // Load mappings.
        if (NewMapping) // In some cases devices may not require mapping (e.g. accelerometer)
        {
            Mapping = *NewMapping; // copy
        }
        else
        {
            bSupportsMapping = false;
        }
    }
    
    
    EGameControllerButtonBind CGamepadController::GetBindForAxis(EGameControllerAxis axis)
    {
        EGameControllerButtonBind bind;
        memset(&bind, 0x0, sizeof(bind));
        
        if (axis == EGameControllerAxis::Invalid)
        {
            return bind;
        }
        
        const uint32 iAxis = (const uint32)axis;
        
        if (Mapping.iAxes[iAxis] >= 0)
        {
            bind.BindType = EGameControllerBindType::Axis;
            bind.Value.Button = Mapping.iAxes[iAxis];
        }
        else if (Mapping.iButtonAxix[iAxis] >= 0)
        {
            bind.BindType = EGameControllerBindType::Button;
            bind.Value.Button = Mapping.iButtonAxix[iAxis];
        }
        
        return bind;
    }
    
    
    EGameControllerButtonBind CGamepadController::GetBindForButton(EGameControllerButton button)
    {
        EGameControllerButtonBind bind;
        memset(&bind, 0x0, sizeof(bind));
        
        if (button == EGameControllerButton::Invalid)
        {
            return bind;
        }
        
        if (Mapping.iButtons[(uint32)button] >= 0)
        {
            bind.BindType = EGameControllerBindType::Button;
            bind.Value.Button = Mapping.iButtons[(uint32)button];
        }
        else if (Mapping.iAxesButton[(uint32)button] >= 0)
        {
            bind.BindType = EGameControllerBindType::Axis;
            bind.Value.Axis = Mapping.iAxesButton[(uint32)button];
        }
        else if (Mapping.buttonHat[(uint32)button].Hat >= 0)
        {
            bind.BindType = EGameControllerBindType::Hat;
            bind.Value.Hat.Hat = Mapping.buttonHat[(uint32)button].Hat;
            bind.Value.Hat.HatMask = Mapping.buttonHat[(uint32)button].Mask;
        }
        
        return bind;
    }
    
    
    /**
     *  Close game controller.
     */
    void CGamepadController::CloseController()
    {
        assert(JoystickHandle);
        
        NEKO_DELETE(Allocator, JoystickHandle);
    }
}
