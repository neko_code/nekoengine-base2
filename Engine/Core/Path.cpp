//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Path.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright Â© 2014 Neko Vision. All rights reserved.
//

#include "../Neko.h"
#include "Path.h"

#include "PathUtils.h"
#include "Log.h"
#include "../Data/Blob.h"
#include "../Utilities/Crc32.h"
#include "../Utilities/StringUtil.h"
#include "../Mt/Sync.h"

namespace Neko
{
    static CPathManager* GPathManager = nullptr;
    
    CPathManager::CPathManager(Neko::IAllocator& allocator)
    : Paths(allocator)
    , Mutex(false)
    , Allocator(allocator)
    {
        GPathManager = this;
        EmptyPath = NEKO_NEW(Allocator, CPath)();
    }
    
    CPathManager::~CPathManager()
    {
        NEKO_DELETE(Allocator, EmptyPath);
        assert(Paths.GetSize() == 0);
        GPathManager = nullptr;
    }
    
    const CPath& CPathManager::GetEmptyPath()
    {
        return *GPathManager->EmptyPath;
    }
    
    void CPathManager::Serialize(OutputBlob& serializer)
    {
        MT::SpinLock lock(Mutex);
        Clear();
        serializer.Write((int32)Paths.GetSize());
        
        for (int32 i = 0; i < Paths.GetSize(); ++i)
        {
            serializer.WriteString(Paths.at(i)->Path);
        }
    }
    
    void CPathManager::Deserialize(InputBlob& serializer)
    {
        MT::SpinLock lock(Mutex);
        int32 size;
        serializer.Read(size);
        
        for (int32 i = 0; i < size; ++i)
        {
            char path[MAX_PATH_LENGTH];
            serializer.ReadString(path, sizeof(path));
            uint32 hash = Crc32(path);
            PathInternal* internal = GetPathMultithreadUnsafe(hash, path);
            --internal->ReferenceCount;
        }
    }
    
    
    CPath::CPath()
    {
        Data = GPathManager->GetPath(0, "");
    }
    
    CPath::CPath(uint32 hash)
    {
        Data = GPathManager->GetPath(hash);
        assert(Data);
    }
    
    PathInternal* CPathManager::GetPath(uint32 hash)
    {
        MT::SpinLock lock(Mutex);
        int32 index = Paths.Find(hash);
        
        if (index < 0)
        {
            return nullptr;
        }
        ++Paths.at(index)->ReferenceCount;
        return Paths.at(index);
    }
    
    PathInternal* CPathManager::GetPath(uint32 hash, const char* path)
    {
        MT::SpinLock lock(Mutex);
        return GetPathMultithreadUnsafe(hash, path);
    }
    
    void CPathManager::Clear()
    {
        for (int32 i = Paths.GetSize() - 1; i >= 0; --i)
        {
            if (Paths.at(i)->ReferenceCount == 0)
            {
                NEKO_DELETE(Allocator, Paths.at(i));
                Paths.EraseAt(i);
            }
        }
    }
    
    PathInternal* CPathManager::GetPathMultithreadUnsafe(uint32 hash, const char* path)
    {
        int32 index = Paths.Find(hash);
        if (index < 0)
        {
            PathInternal* internal = NEKO_NEW(Allocator, PathInternal);
            internal->ReferenceCount = 1;
            internal->Id = hash;
            CopyString(internal->Path, path);
            Paths.Insert(hash, internal);
            return internal;
        }
        else
        {
            ++Paths.at(index)->ReferenceCount;
            return Paths.at(index);
        }
    }
    
    void CPathManager::IncrementRefCount(PathInternal* path)
    {
        MT::SpinLock lock(Mutex);
        ++path->ReferenceCount;
    }
    
    void CPathManager::DecrementRefCount(PathInternal* path)
    {
        MT::SpinLock lock(Mutex);
        --path->ReferenceCount;
        if (path->ReferenceCount == 0)
        {
            Paths.Erase(path->Id);
            NEKO_DELETE(Allocator, path);
        }
    }
    
    CPath::CPath(const CPath& rhs)
    : Data(rhs.Data)
    {
        GPathManager->IncrementRefCount(Data);
    }
    
    CPath::CPath(const char* path)
    {
        char tmp[MAX_PATH_LENGTH];
        size_t len = StringLength(path);
        assert(len < MAX_PATH_LENGTH);
        PathUtils::Normalize(path, tmp, (uint32)len + 1);
        uint32 hash = Crc32(tmp);
        Data = GPathManager->GetPath(hash, tmp);
    }
    
    CPath::~CPath()
    {
        GPathManager->DecrementRefCount(Data);
    }
    
    int32 CPath::Length() const
    {
        return StringLength(Data->Path);
    }
    
    void CPath::operator =(const CPath& rhs)
    {
        GPathManager->DecrementRefCount(Data);
        Data = rhs.Data;
        GPathManager->IncrementRefCount(Data);
    }
    
    void CPath::operator =(const char* Other)
    {
        GPathManager->DecrementRefCount(Data);
        char tmp[MAX_PATH_LENGTH];
        size_t len = StringLength(Other);
        assert(len < MAX_PATH_LENGTH);
        PathUtils::Normalize(Other, tmp, (uint32)len + 1);
        uint32 hash = Crc32(tmp);
        Data = GPathManager->GetPath(hash, tmp);
    }
} // namespace Neko
