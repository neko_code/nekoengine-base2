//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  IPlugin.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "IPlugin.h"
#include "StringUtil.h"

namespace Neko
{
	IPlugin::~IPlugin()
    {
    
    }
	
	static StaticPluginRegister* GFirstPlugin = nullptr;
    
	StaticPluginRegister::StaticPluginRegister(const char* name, IPlugin* (*creator)(IEngine& engine))
	{
		this->creator = creator;
		this->name = name;
		next = GFirstPlugin;
		GFirstPlugin = this;
	}


	IPlugin* StaticPluginRegister::Create(const char* name, IEngine& engine)
	{
		auto* i = GFirstPlugin;
		while (i)
		{
			if (EqualStrings(name, i->name))
			{
				return i->creator(engine);
			}
			i = i->next;
		}
		return nullptr;
	}
}
