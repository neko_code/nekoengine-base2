//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  JobSystem.cpp
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#include "JobSystem.h"
#include "Profiler.h"
#include "Engine.h"
#include "Fiber.h"
#include "Log.h"
#include "../Containers/Array.h"
#include "../Data/IAllocator.h"
#include "../Math/MathUtils.h"
#include "../Mt/Atomic.h"
#include "../Mt/Sync.h"
#include "../Mt/Task.h"
#include "../Mt/Thread.h"
#include "../Platform/Platform.h"

namespace Neko
{
    namespace JobSystem
    {
        const static int FIBERPOOL_SIZE = 160;
        
        struct Job
        {
            JobDecl decl;
            volatile int* counter;
        };
        
        struct FiberDecl
        {
            int idx;
            Fiber::Handle fiber;
            Job CurrentJob;
            struct WorkerTask* pWorkerTask;
            void* SwitchState;
        };
    
        struct SleepingFiber
        {
            volatile int* WaitingCondition = nullptr;
            FiberDecl* fiber = nullptr;
        };
        
        
        struct System
        {
            System(IAllocator& allocator)
            : Allocator(allocator)
            , Workers(allocator)
            , JobQueue(allocator)
            , SleepingFibers(allocator)
            , Sync(false)
            , WorkSignal(true)
            , EventOutsideJob(true)
            {
                EventOutsideJob.Trigger();
                WorkSignal.Reset();
            }
            
            MT::SpinMutex Sync;
            MT::Event EventOutsideJob;
            MT::Event WorkSignal;
            TArray<MT::Task*> Workers;
            TArray<Job> JobQueue;
            FiberDecl FiberPool[FIBERPOOL_SIZE];
            int FreeFibersIndices[FIBERPOOL_SIZE];
            int NumFreeFibers;
            TArray<SleepingFiber> SleepingFibers;
            IAllocator& Allocator;
        };
        
        static System* GSystem = nullptr;
        
        static bool GetReadySleepingFiber(System& system, SleepingFiber* out)
        {
            MT::SpinLock lock(system.Sync);
            
            int count = system.SleepingFibers.GetSize();
            for (int i = 0; i < count; ++i)
            {
                SleepingFiber job = system.SleepingFibers[i];
                if (*job.WaitingCondition <= 0)
                {
                    system.SleepingFibers.EraseFast(i);
                    *out = job;
                    return true;
                }
            }
            return false;
        }
        
        static bool GetReadyJob(System& system, Job* out)
        {
            MT::SpinLock lock(system.Sync);
            
            if (system.JobQueue.IsEmpty())
            {
                return false;
            }
            
            Job job = system.JobQueue.back();
            system.JobQueue.Pop();
            if (system.JobQueue.IsEmpty())
            {
                system.WorkSignal.Reset();
            }
            *out = job;
            
            return true;
        }
        
        
        static thread_local MT::Task* GWorker = nullptr;
        
        struct WorkerTask : MT::Task
        {
            WorkerTask(System& system)
            : Task(system.Allocator)
            , m_system(system)
            {
            }
            
            static FiberDecl& GetFreeFiber()
            {
                MT::SpinLock lock(GSystem->Sync);
                
                assert(GSystem->NumFreeFibers > 0);
                --GSystem->NumFreeFibers;
                int freeFiberIndex = GSystem->FreeFibersIndices[GSystem->NumFreeFibers];
                
                return GSystem->FiberPool[freeFiberIndex];
            }
        
            static void HandleSwitch(FiberDecl& fiber)
            {
                MT::SpinLock lock(GSystem->Sync);
                
                if (!fiber.SwitchState)
                {
                    GSystem->FreeFibersIndices[GSystem->NumFreeFibers] = fiber.idx;
                    ++GSystem->NumFreeFibers;
                    return;
                }
                
                volatile int* counter = (volatile int*)fiber.SwitchState;
                SleepingFiber sleepingFiber;
                sleepingFiber.fiber = &fiber;
                sleepingFiber.WaitingCondition = counter;
                
                GSystem->SleepingFibers.Push(sleepingFiber);
            }
            
            int DoTask() override
            {
                GWorker = this;
                Fiber::InitThread(&PrimaryFiber);
                Process(nullptr);
                
                return 0;
            }
            
#ifdef NEKO_WINDOWS_FAMILY
            static void __stdcall Process(void* data)
#else
            static void Process(void* data)
#endif
            {
                WorkerTask* that = (WorkerTask*)GWorker;
                that->bFinished = false;
                while (!that->bFinished)
                {
                    SleepingFiber readySleepingFiber;
                    if (GetReadySleepingFiber(*GSystem, &readySleepingFiber))
                    {
                        readySleepingFiber.fiber->pWorkerTask = that;
                        readySleepingFiber.fiber->SwitchState = nullptr;
                        
                        PROFILE_SECTION("work");
                        that->CurrentFiber = readySleepingFiber.fiber;
                        Fiber::SwitchTo(&that->PrimaryFiber, readySleepingFiber.fiber->fiber);
                        that->CurrentFiber = nullptr;
                        assert(Profiler::GetCurrentSection() == Profiler::GetRootBlock(MT::GetCurrentThreadID()));
                        HandleSwitch(*readySleepingFiber.fiber);
                        continue;
                    }
                    
                    Job job;
                    if (GetReadyJob(*GSystem, &job))
                    {
                        FiberDecl& fiberDecl = GetFreeFiber();
                        fiberDecl.pWorkerTask = that;
                        fiberDecl.CurrentJob = job;
                        fiberDecl.SwitchState = nullptr;
                        
                        PROFILE_SECTION("work");
                        that->CurrentFiber = &fiberDecl;
                        Fiber::SwitchTo(&that->PrimaryFiber, fiberDecl.fiber);
                        that->CurrentFiber = nullptr;
                        assert(Profiler::GetCurrentSection() == Profiler::GetRootBlock(MT::GetCurrentThreadID()));
                        HandleSwitch(fiberDecl);
                    }
                    else
                    {
                        GSystem->WorkSignal.WaitTimeout(1);
                    }
                }
            }
            
            bool bFinished = false;
            FiberDecl* CurrentFiber = nullptr;
            Fiber::Handle PrimaryFiber;
            System& m_system;
        };
        
        
#ifdef _WIN32
        static void __stdcall fiberProc(void* data)
#else
        static void fiberProc(void* data)
#endif
        {
            printf("fiberProc");
            FiberDecl* fiberDecl = (FiberDecl*)data;
            for (;;)
            {
                Job job = fiberDecl->CurrentJob;
                job.decl.task(job.decl.data);
                if (job.counter)
                {
                    Atomics::Decrement(job.counter);
                }
                
                fiberDecl->SwitchState = nullptr;
                Fiber::SwitchTo(&fiberDecl->fiber, fiberDecl->pWorkerTask->PrimaryFiber);
            }
        }
        
        bool Init(IAllocator& allocator)
        {
            assert(!GSystem);
            
            GSystem = NEKO_NEW(allocator, System)(allocator);
            GSystem->WorkSignal.Reset();
            
            int32 workersNum = Math::Maximum(1, int(Neko::Platform::GetNumberOfLogicalCores() - 1));
            for (int32 i = 0; i < workersNum; ++i)
            {
                WorkerTask* task = NEKO_NEW(allocator, WorkerTask)(*GSystem);
                StaticString<32> jobName("Job system worker #", i);
                if (task->Create(jobName))
                {
                    GSystem->Workers.Push(task);
                    task->SetAffinityMask((uint64)1 << i);
                }
                else
                {
                    GLogError.log("Engine") << "Job system worker failed to initialize.";
                    NEKO_DELETE(allocator, task);
                }
            }
            
            int32 fiberNum = lengthOf(GSystem->FiberPool);
            GSystem->NumFreeFibers = fiberNum;
            for(int32 i = 0; i < fiberNum; ++i)
            {
                FiberDecl& decl = GSystem->FiberPool[i];
                decl.fiber = Fiber::Create(64 * 1024, fiberProc, &GSystem->FiberPool[i]);
                decl.idx = i;
                decl.pWorkerTask = nullptr;
                GSystem->FreeFibersIndices[i] = i;
            }
            
            return !GSystem->Workers.IsEmpty();
        }
        
        void Shutdown()
        {
            if (!GSystem)
            {
                return;
            }
            
            auto& allocator = GSystem->Allocator;
            for (auto task : GSystem->Workers)
            {
                WorkerTask* wt = (WorkerTask*)task;
                wt->bFinished = true;
            }
            for (auto task : GSystem->Workers)
            {
                while (!task->IsFinished())
                {
                    GSystem->WorkSignal.Trigger();
                }
                task->Destroy();
                NEKO_DELETE(allocator, task);
            }
            
            for (auto& fiber : GSystem->FiberPool)
            {
                Fiber::Destroy(fiber.fiber);
            }
            
            NEKO_DELETE(allocator, GSystem);
            GSystem = nullptr;
        }
        
        void RunJobs(const JobDecl* jobs, int count, int volatile* counter)
        {
            assert(GSystem);
            assert(count > 0);
            
            MT::SpinLock lock(GSystem->Sync);
            GSystem->WorkSignal.Trigger();
            if (counter)
            {
                Atomics::Add(counter, count);
            }
            for (int i = 0; i < count; ++i)
            {
                Job job;
                job.decl = jobs[i];
                job.counter = counter;
                GSystem->JobQueue.Push(job);
            }
        }
        
        void Wait(int volatile* counter)
        {
            if (*counter <= 0)
            {
                return;
            }
            if (GWorker)
            {
//                assert(Profiler::GetCurrentSection() == Profiler::GetRootBlock(MT::GetCurrentThreadID()));
                FiberDecl* fiberDecl = ((WorkerTask*)GWorker)->CurrentFiber;
                fiberDecl->SwitchState = (void*)counter;
                Fiber::SwitchTo(&fiberDecl->fiber, fiberDecl->pWorkerTask->PrimaryFiber);
            }
            else
            {
                PROFILE_SECTION("n/a job waiting");
                
                //assert(GSystem->EventOutsideJob.poll());
                GSystem->EventOutsideJob.Reset();
                
                JobDecl job;
                job.data = (void*)counter;
                job.task = [](void* data)
                {
                    JobSystem::Wait((volatile int*)data);
                    GSystem->EventOutsideJob.Trigger();
                };
                RunJobs(&job, 1, nullptr);
                
                MT::YieldCpu();
                
                while (*counter > 0)
                {
                    GSystem->EventOutsideJob.WaitTimeout(1);
                }
            }
        }
    } // namespace JobSystem
} // namespace Neko
