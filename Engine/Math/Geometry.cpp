//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Geometry.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Geometry.h"

namespace Neko
{
    Frustum::Frustum()
    {
        xs[6] = xs[7] = 1;
        ys[6] = ys[7] = 0;
        zs[6] = zs[7] = 0;
        ds[6] = ds[7] = 0;
    }
    
    void Frustum::ComputeOrtho(const Vec3& position,
                               const Vec3& direction,
                               const Vec3& up,
                               float width,
                               float height,
                               float near_distance,
                               float far_distance)
    {
        ComputeOrtho(position, direction, up, width, height, near_distance, far_distance, {-1, -1}, {1, 1});
    }
    
    
    static void setPlanesFromPoints(Frustum& frustum)
    {
        Vec3* points = frustum.points;
        
        Vec3 normal_near = -CrossProduct(points[0] - points[1], points[0] - points[2]).Normalized();
        Vec3 normal_far = CrossProduct(points[4] - points[5], points[4] - points[6]).Normalized();
        frustum.SetPlane(Frustum::Planes::EXTRA0, normal_near, points[0]);
        frustum.SetPlane(Frustum::Planes::EXTRA1, normal_near, points[0]);
        frustum.SetPlane(Frustum::Planes::NEAR, normal_near, points[0]);
        frustum.SetPlane(Frustum::Planes::FAR, normal_far, points[4]);
        
        frustum.SetPlane(Frustum::Planes::LEFT, CrossProduct(points[1] - points[2], points[1] - points[5]).Normalized(), points[1]);
        frustum.SetPlane(Frustum::Planes::RIGHT, -CrossProduct(points[0] - points[3], points[0] - points[4]).Normalized(), points[0]);
        frustum.SetPlane(Frustum::Planes::TOP, CrossProduct(points[0] - points[1], points[0] - points[4]).Normalized(), points[0]);
        frustum.SetPlane(Frustum::Planes::BOTTOM, CrossProduct(points[2] - points[3], points[2] - points[6]).Normalized(), points[2]);
    }
    
    
    static void setPoints(Frustum& frustum
                          , const Vec3& near_center
                          , const Vec3& far_center
                          , const Vec3& right_near
                          , const Vec3& up_near
                          , const Vec3& right_far
                          , const Vec3& up_far
                          , const Vec2& viewport_min
                          , const Vec2& viewport_max)
    {
        assert(viewport_max.x >= viewport_min.x);
        assert(viewport_max.y >= viewport_min.y);
        
        Vec3* points = frustum.points;
        
        points[0] = near_center + right_near * viewport_max.x + up_near * viewport_max.y;
        points[1] = near_center + right_near * viewport_min.x + up_near * viewport_max.y;
        points[2] = near_center + right_near * viewport_min.x + up_near * viewport_min.y;
        points[3] = near_center + right_near * viewport_max.x + up_near * viewport_min.y;
        
        points[4] = far_center + right_far * viewport_max.x + up_far * viewport_max.y;
        points[5] = far_center + right_far * viewport_min.x + up_far * viewport_max.y;
        points[6] = far_center + right_far * viewport_min.x + up_far * viewport_min.y;
        points[7] = far_center + right_far * viewport_max.x + up_far * viewport_min.y;
        
        setPlanesFromPoints(frustum);
    }
    

    
    void Frustum::ComputeOrtho(const Vec3& position,
                               const Vec3& direction,
                               const Vec3& up,
                               float width,
                               float height,
                               float near_distance,
                               float far_distance,
                               const Vec2& viewport_min,
                               const Vec2& viewport_max)
    {
        Vec3 z = direction;
        z.Normalize();
        Vec3 near_center = position - z * near_distance;
        Vec3 far_center = position - z * far_distance;
        
        Vec3 x = CrossProduct(up, z).Normalized() * width;
        Vec3 y = CrossProduct(z, x).Normalized() * height;
        
        setPoints(*this, near_center, far_center, x, y, x, y, viewport_min, viewport_max);
    }

    
    void Frustum::SetPlane(Planes side, const Vec3& normal, const Vec3& point)
    {
        xs[(uint32)side] = normal.x;
        ys[(uint32)side] = normal.y;
        zs[(uint32)side] = normal.z;
        ds[(uint32)side] = -DotProduct(point, normal);
    }
    
    void Frustum::SetPlane(Planes side, const Vec3& normal, float d)
    {
        xs[(uint32)side] = normal.x;
        ys[(uint32)side] = normal.y;
        zs[(uint32)side] = normal.z;
        ds[(uint32)side] = d;
    }
    
    
    void Frustum::ComputePerspective(const Vec3& position,
                                     const Vec3& direction,
                                     const Vec3& up,
                                     float fov,
                                     float ratio,
                                     float near_distance,
                                     float far_distance,
                                     const Vec2& viewport_min,
                                     const Vec2& viewport_max)
    {
        assert(near_distance > 0);
        assert(far_distance > 0);
        assert(near_distance < far_distance);
        assert(fov > 0);
        assert(ratio > 0);
//        float tang = (float)tan(fov * 0.5f);
//        float near_height = near_distance * tang;
//        float near_width = near_height * ratio;
        float scale = (float)tan(fov * 0.5f);
        Vec3 right = CrossProduct(direction, up);
        Vec3 up_near = up * near_distance * scale;
        Vec3 right_near = right * (near_distance * scale * ratio);
        Vec3 up_far = up * far_distance * scale;
        Vec3 right_far = right * (far_distance * scale * ratio);
        
        Vec3 z = direction.Normalized();
        
        Vec3 near_center = position + z * near_distance;
        Vec3 far_center = position + z * far_distance;
        
        setPoints(*this, near_center, far_center, right_near, up_near, right_far, up_far, viewport_min, viewport_max);
    }
    
    
    void Frustum::ComputePerspective(const Vec3& position,
                                     const Vec3& direction,
                                     const Vec3& up,
                                     float fov,
                                     float ratio,
                                     float near_distance,
                                     float far_distance)
    {
        ComputePerspective(position, direction, up, fov, ratio, near_distance, far_distance, {-1, -1}, {1, 1});
    }
    
    bool Frustum::IntersectAABB(const AABB& aabb) const
    {
        Vec3 box[] = { aabb.min, aabb.max };
        
        for (int i = 0; i < 6; ++i)
        {
            int px = (int)(xs[i] > 0.0f);
            int py = (int)(ys[i] > 0.0f);
            int pz = (int)(zs[i] > 0.0f);
            
            float dp =
            (xs[i] * box[px].x) +
            (ys[i] * box[py].y) +
            (zs[i] * box[pz].z);
            
            if (dp < -ds[i]) { return false; }
        }
        return true;
    }

    void Frustum::Transform(const Matrix& mtx)
    {
        for (Vec3& p : points)
        {
            p = mtx.TransformPoint(p);
        }
        
        for (int i = 0; i < lengthOf(xs); ++i)
        {
            Vec3 p;
            if (xs[i] != 0) p.Set(-ds[i] / xs[i], 0, 0);
            else if (ys[i] != 0) p.Set(0, -ds[i] / ys[i], 0);
            else p.Set(0, 0, -ds[i] / zs[i]);
            
            Vec3 n = {xs[i], ys[i], zs[i]};
            n = mtx.TransformVector(n);
            p = mtx.TransformPoint(p);
            
            xs[i] = n.x;
            ys[i] = n.y;
            zs[i] = n.z;
            ds[i] = -DotProduct(p, n);
        }
    }
    
    
    Sphere Frustum::ComputeBoundingSphere() const
    {
        Sphere sphere;
        sphere.position = points[0];
        for (int i = 1; i < lengthOf(points); ++i)
        {
            sphere.position += points[i];
        }
        sphere.position *= 1.0f / lengthOf(points);
        
        sphere.radius = 0;
        for (int i = 0; i < lengthOf(points); ++i)
        {
            float len_sq = (points[i] - sphere.position).LengthSq();
            if (len_sq > sphere.radius) sphere.radius = len_sq;
        }
        sphere.radius = sqrtf(sphere.radius);
        return sphere;
    }
    
    
    bool Frustum::IsSphereInside(const Vec3& center, float radius) const
    {
        VectorRegister px = simd_ld(xs);
        VectorRegister py = simd_ld(ys);
        VectorRegister pz = simd_ld(zs);
        VectorRegister pd = simd_ld(ds);

        VectorRegister cx = simd_splat(center.x);
        VectorRegister cy = simd_splat(center.y);
        VectorRegister cz = simd_splat(center.z);

        VectorRegister t = simd_mul(cx, px);
        t = simd_add(t, simd_mul(cy, py));
        t = simd_add(t, simd_mul(cz, pz));
        t = simd_add(t, pd);
        t = simd_sub(t, simd_splat(-radius));
        if (simd_movemask(t)) return false;

        px = simd_ld(&xs[4]);
        py = simd_ld(&ys[4]);
        pz = simd_ld(&zs[4]);
        pd = simd_ld(&ds[4]);

        t = simd_mul(cx, px);
        t = simd_add(t, simd_mul(cy, py));
        t = simd_add(t, simd_mul(cz, pz));
        t = simd_add(t, pd);
        t = simd_sub(t, simd_splat(-radius));
        if (simd_movemask(t)) return false;

        return true;
    }
    
    
    // AABB
    
    void AABB::Transform(const Matrix& matrix)
    {
        Vec3 points[8];
        points[0] = min;
        points[7] = max;
        points[1].Set(points[0].x, points[0].y, points[7].z);
        points[2].Set(points[0].x, points[7].y, points[0].z);
        points[3].Set(points[0].x, points[7].y, points[7].z);
        points[4].Set(points[7].x, points[0].y, points[0].z);
        points[5].Set(points[7].x, points[0].y, points[7].z);
        points[6].Set(points[7].x, points[7].y, points[0].z);
        
        for (int j = 0; j < 8; ++j)
        {
            points[j] = matrix.TransformPoint(points[j]);
        }
        
        Vec3 new_min = points[0];
        Vec3 new_max = points[0];
        
        for (int j = 0; j < 8; ++j)
        {
            new_min = minCoords(points[j], new_min);
            new_max = maxCoords(points[j], new_max);
        }
        
        min = new_min;
        max = new_max;
    }
    
    void AABB::getCorners(const Matrix& matrix, Vec3* points) const
    {
        Vec3 p(min.x, min.y, min.z);
        points[0] = matrix.TransformPoint(p);
        p.Set(min.x, min.y, max.z);
        points[1] = matrix.TransformPoint(p);
        p.Set(min.x, max.y, min.z);
        points[2] = matrix.TransformPoint(p);
        p.Set(min.x, max.y, max.z);
        points[3] = matrix.TransformPoint(p);
        p.Set(max.x, min.y, min.z);
        points[4] = matrix.TransformPoint(p);
        p.Set(max.x, min.y, max.z);
        points[5] = matrix.TransformPoint(p);
        p.Set(max.x, max.y, min.z);
        points[6] = matrix.TransformPoint(p);
        p.Set(max.x, max.y, max.z);
        points[7] = matrix.TransformPoint(p);
    }
    

} // namespace Neko
