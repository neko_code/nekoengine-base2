//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  simd128_sse.inl
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

// This code is from bx library

#pragma once

#include "SimdNi.inl"

namespace Neko
{
    typedef __m128	VectorRegister;
    typedef __m128i VectorRegisterInt;
    typedef __m128d VectorRegisterDouble;
    
    /**
     * Swizzles the 4 components of a vector and returns the result.
     *
     * @param Vec		Source vector
     * @param X			Index for which component to use for X (literal 0-3)
     * @param Y			Index for which component to use for Y (literal 0-3)
     * @param Z			Index for which component to use for Z (literal 0-3)
     * @param W			Index for which component to use for W (literal 0-3)
     * @return			The swizzled vector
     */
//#define VectorSwizzle( Vec, X, Y, Z, W )	 _mm_shuffle_ps( Vec, Vec, _MM_SHUFFLE(X,Y,Z,W) )
    

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_shuf_xyAB(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_movelh_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_shuf_ABxy(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_movelh_ps(_b, _a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_shuf_CDzw(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_movehl_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_shuf_zwCD(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_movehl_ps(_b, _a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_shuf_xAyB(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_unpacklo_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_shuf_yBxA(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_unpacklo_ps(_b, _a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_shuf_zCwD(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_unpackhi_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_shuf_CzDw(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_unpackhi_ps(_b, _a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE float simd_x(simd128_sse_t _a)
	{
		return _mm_cvtss_f32(_a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE float simd_y(simd128_sse_t _a)
	{
		const simd128_sse_t yyyy = VectorSwizzle(_a, 1, 1, 1, 1);
		const float result  = _mm_cvtss_f32(yyyy);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE float simd_z(simd128_sse_t _a)
	{
		const simd128_sse_t zzzz = VectorSwizzle(_a, 2, 2, 2, 2);
		const float result  = _mm_cvtss_f32(zzzz);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE float simd_w(simd128_sse_t _a)
	{
		const simd128_sse_t wwww = VectorSwizzle(_a, 3, 3, 3, 3);
		const float result  = _mm_cvtss_f32(wwww);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_ld(const void* _ptr)
	{
		return _mm_load_ps(reinterpret_cast<const float*>(_ptr) );
	}

	template<>
	NEKO_SIMD_FORCE_INLINE void simd_st(void* _ptr, simd128_sse_t _a)
	{
		_mm_store_ps(reinterpret_cast<float*>(_ptr), _a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE void simd_stx(void* _ptr, simd128_sse_t _a)
	{
		_mm_store_ss(reinterpret_cast<float*>(_ptr), _a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE void simd_stream(void* _ptr, simd128_sse_t _a)
	{
		_mm_stream_ps(reinterpret_cast<float*>(_ptr), _a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_ld(float _x, float _y, float _z, float _w)
	{
		return _mm_set_ps(_w, _z, _y, _x);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_ild(uint32_t _x, uint32_t _y, uint32_t _z, uint32_t _w)
	{
		const __m128i set     = _mm_set_epi32(_w, _z, _y, _x);
		const simd128_sse_t result = _mm_castsi128_ps(set);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_splat(const void* _ptr)
	{
		const simd128_sse_t x___   = _mm_load_ss(reinterpret_cast<const float*>(_ptr) );
		const simd128_sse_t result = VectorSwizzle(x___, 0, 0, 0, 0);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_splat(float _a)
	{
		return _mm_set1_ps(_a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_isplat(uint32_t _a)
	{
		const __m128i splat   = _mm_set1_epi32(_a);
		const simd128_sse_t result = _mm_castsi128_ps(splat);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_zero()
	{
		return _mm_setzero_ps();
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_itof(simd128_sse_t _a)
	{
		const __m128i  itof   = _mm_castps_si128(_a);
		const simd128_sse_t result = _mm_cvtepi32_ps(itof);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_ftoi(simd128_sse_t _a)
	{
		const __m128i ftoi    = _mm_cvtps_epi32(_a);
		const simd128_sse_t result = _mm_castsi128_ps(ftoi);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_round(simd128_sse_t _a)
	{
#if defined(__SSE4_1__)
		return _mm_round_ps(_a, _MM_FROUND_NINT);
#else
		const __m128i round   = _mm_cvtps_epi32(_a);
		const simd128_sse_t result = _mm_cvtepi32_ps(round);

		return result;
#endif // defined(__SSE4_1__)
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_add(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_add_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_sub(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_sub_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_mul(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_mul_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_div(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_div_ps(_a, _b);
	}
    
    template<>
    NEKO_SIMD_FORCE_INLINE int simd_movemask(simd128_sse_t _a)
    {
        return _mm_movemask_ps(_a);
    }

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_rcp_est(simd128_sse_t _a)
	{
		return _mm_rcp_ps(_a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_sqrt(simd128_sse_t _a)
	{
		return _mm_sqrt_ps(_a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_rsqrt_est(simd128_sse_t _a)
	{
		return _mm_rsqrt_ps(_a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_dot3(simd128_sse_t _a, simd128_sse_t _b)
	{
#if defined(__SSE4_1__)
		return _mm_dp_ps(_a, _b, 0x77);
#else
		return simd_dot3_ni(_a, _b);
#endif // defined(__SSE4__)
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_dot(simd128_sse_t _a, simd128_sse_t _b)
	{
#if defined(__SSE4_1__)
		return _mm_dp_ps(_a, _b, 0xFF);
#else
		return simd_dot_ni(_a, _b);
#endif // defined(__SSE4__)
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_cmpeq(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_cmpeq_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_cmplt(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_cmplt_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_cmple(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_cmple_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_cmpgt(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_cmpgt_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_cmpge(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_cmpge_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_min(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_min_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_max(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_max_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_and(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_and_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_andc(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_andnot_ps(_b, _a);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_or(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_or_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_xor(simd128_sse_t _a, simd128_sse_t _b)
	{
		return _mm_xor_ps(_a, _b);
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_sll(simd128_sse_t _a, int _count)
	{
		const __m128i a       = _mm_castps_si128(_a);
		const __m128i shift   = _mm_slli_epi32(a, _count);
		const simd128_sse_t result = _mm_castsi128_ps(shift);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_srl(simd128_sse_t _a, int _count)
	{
		const __m128i a       = _mm_castps_si128(_a);
		const __m128i shift   = _mm_srli_epi32(a, _count);
		const simd128_sse_t result = _mm_castsi128_ps(shift);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_sra(simd128_sse_t _a, int _count)
	{
		const __m128i a       = _mm_castps_si128(_a);
		const __m128i shift   = _mm_srai_epi32(a, _count);
		const simd128_sse_t result = _mm_castsi128_ps(shift);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_icmpeq(simd128_sse_t _a, simd128_sse_t _b)
	{
		const __m128i tmp0    = _mm_castps_si128(_a);
		const __m128i tmp1    = _mm_castps_si128(_b);
		const __m128i tmp2    = _mm_cmpeq_epi32(tmp0, tmp1);
		const simd128_sse_t result = _mm_castsi128_ps(tmp2);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_icmplt(simd128_sse_t _a, simd128_sse_t _b)
	{
		const __m128i tmp0    = _mm_castps_si128(_a);
		const __m128i tmp1    = _mm_castps_si128(_b);
		const __m128i tmp2    = _mm_cmplt_epi32(tmp0, tmp1);
		const simd128_sse_t result = _mm_castsi128_ps(tmp2);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_icmpgt(simd128_sse_t _a, simd128_sse_t _b)
	{
		const __m128i tmp0    = _mm_castps_si128(_a);
		const __m128i tmp1    = _mm_castps_si128(_b);
		const __m128i tmp2    = _mm_cmpgt_epi32(tmp0, tmp1);
		const simd128_sse_t result = _mm_castsi128_ps(tmp2);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_imin(simd128_sse_t _a, simd128_sse_t _b)
	{
#if defined(__SSE4_1__)
		const __m128i tmp0    = _mm_castps_si128(_a);
		const __m128i tmp1    = _mm_castps_si128(_b);
		const __m128i tmp2    = _mm_min_epi32(tmp0, tmp1);
		const simd128_sse_t result = _mm_castsi128_ps(tmp2);

		return result;
#else
		return simd_imin_ni(_a, _b);
#endif // defined(__SSE4_1__)
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_imax(simd128_sse_t _a, simd128_sse_t _b)
	{
#if defined(__SSE4_1__)
		const __m128i tmp0    = _mm_castps_si128(_a);
		const __m128i tmp1    = _mm_castps_si128(_b);
		const __m128i tmp2    = _mm_max_epi32(tmp0, tmp1);
		const simd128_sse_t result = _mm_castsi128_ps(tmp2);

		return result;
#else
		return simd_imax_ni(_a, _b);
#endif // defined(__SSE4_1__)
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_iadd(simd128_sse_t _a, simd128_sse_t _b)
	{
		const __m128i a       = _mm_castps_si128(_a);
		const __m128i b       = _mm_castps_si128(_b);
		const __m128i add     = _mm_add_epi32(a, b);
		const simd128_sse_t result = _mm_castsi128_ps(add);

		return result;
	}

	template<>
	NEKO_SIMD_FORCE_INLINE simd128_sse_t simd_isub(simd128_sse_t _a, simd128_sse_t _b)
	{
		const __m128i a       = _mm_castps_si128(_a);
		const __m128i b       = _mm_castps_si128(_b);
		const __m128i sub     = _mm_sub_epi32(a, b);
		const simd128_sse_t result = _mm_castsi128_ps(sub);

		return result;
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_shuf_xAzC(simd128_sse_t _a, simd128_sse_t _b)
	{
		return simd_shuf_xAzC_ni(_a, _b);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_shuf_yBwD(simd128_sse_t _a, simd128_sse_t _b)
	{
		return simd_shuf_yBwD_ni(_a, _b);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_rcp(simd128_sse_t _a)
	{
		return simd_rcp_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_orx(simd128_sse_t _a)
	{
		return simd_orx_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_orc(simd128_sse_t _a, simd128_sse_t _b)
	{
		return simd_orc_ni(_a, _b);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_neg(simd128_sse_t _a)
	{
		return simd_neg_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_madd(simd128_sse_t _a, simd128_sse_t _b, simd128_sse_t _c)
	{
		return simd_madd_ni(_a, _b, _c);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_nmsub(simd128_sse_t _a, simd128_sse_t _b, simd128_sse_t _c)
	{
		return simd_nmsub_ni(_a, _b, _c);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_div_nr(simd128_sse_t _a, simd128_sse_t _b)
	{
		return simd_div_nr_ni(_a, _b);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_selb(simd128_sse_t _mask, simd128_sse_t _a, simd128_sse_t _b)
	{
		return simd_selb_ni(_mask, _a, _b);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_sels(simd128_sse_t _test, simd128_sse_t _a, simd128_sse_t _b)
	{
		return simd_sels_ni(_test, _a, _b);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_not(simd128_sse_t _a)
	{
		return simd_not_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_abs(simd128_sse_t _a)
	{
		return simd_abs_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_clamp(simd128_sse_t _a, simd128_sse_t _min, simd128_sse_t _max)
	{
		return simd_clamp_ni(_a, _min, _max);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_lerp(simd128_sse_t _a, simd128_sse_t _b, simd128_sse_t _s)
	{
		return simd_lerp_ni(_a, _b, _s);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_rsqrt(simd128_sse_t _a)
	{
		return simd_rsqrt_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_rsqrt_nr(simd128_sse_t _a)
	{
		return simd_rsqrt_nr_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_rsqrt_carmack(simd128_sse_t _a)
	{
		return simd_rsqrt_carmack_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_sqrt_nr(simd128_sse_t _a)
	{
		return simd_sqrt_nr_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_log2(simd128_sse_t _a)
	{
		return simd_log2_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_exp2(simd128_sse_t _a)
	{
		return simd_exp2_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_pow(simd128_sse_t _a, simd128_sse_t _b)
	{
		return simd_pow_ni(_a, _b);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_cross3(simd128_sse_t _a, simd128_sse_t _b)
	{
		return simd_cross3_ni(_a, _b);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_normalize3(simd128_sse_t _a)
	{
		return simd_normalize3_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_ceil(simd128_sse_t _a)
	{
		return simd_ceil_ni(_a);
	}

	template<>
	NEKO_SIMD_INLINE simd128_sse_t simd_floor(simd128_sse_t _a)
	{
		return simd_floor_ni(_a);
	}

	typedef simd128_sse_t simd128_t;

} // namespace bx
