//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MathUtils.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

// Random math utils I ever needed in this engine

#include "MathUtils.h"
#include "Vec.h"
#include <cmath>
#include <random>

namespace Neko
{
    namespace Math
    {
        Vec3 DegreesToRadians(const Vec3& v)
        {
            return Vec3(DegreesToRadians(v.x), DegreesToRadians(v.y), DegreesToRadians(v.z));
        }
        
        Vec3 RadiansToDegrees(const Vec3& v)
        {
            return Vec3(RadiansToDegrees(v.x), RadiansToDegrees(v.y), RadiansToDegrees(v.z));
        }
        
        
        float AngleDiff(float a, float b)
        {
            float delta = a - b;
            delta = fmodf(delta, PI * 2);
            if (delta > PI)
            {
                return -PI * 2 + delta;
            }
            if (delta < -PI)
            {
                return PI * 2 + delta;
            }
            return delta;
        }
        
        bool GetRayPlaneIntersecion(const Vec3& origin,
                                    const Vec3& dir,
                                    const Vec3& plane_point,
                                    const Vec3& normal,
                                    float& out)
        {
            float d = DotProduct(dir, normal);
            if (d == 0)
            {
                return false;
            }
            d = DotProduct(plane_point - origin, normal) / d;
            out = d;
            return true;
        }
        
        bool GetRaySphereIntersection(const Vec3& origin,
                                      const Vec3& dir,
                                      const Vec3& center,
                                      float radius,
                                      Vec3& out)
        {
            assert(dir.Length() < 1.01f && dir.Length() > 0.99f);
            Vec3 L = center - origin;
            float tca = DotProduct(L, dir);
            if (tca < 0) return false;
            float d2 = DotProduct(L, L) - tca * tca;
            if (d2 > radius * radius) return false;
            float thc = sqrt(radius * radius - d2);
            float t0 = tca - thc;
            out = origin + dir * t0;
            return true;
        }
        
        bool GetRayAABBIntersection(const Vec3& origin,
                                    const Vec3& dir,
                                    const Vec3& min,
                                    const Vec3& size,
                                    Vec3& out)
        {
            Vec3 dirfrac;
            
            dirfrac.x = 1.0f / (dir.x == 0 ? 0.00000001f : dir.x);
            dirfrac.y = 1.0f / (dir.y == 0 ? 0.00000001f : dir.y);
            dirfrac.z = 1.0f / (dir.z == 0 ? 0.00000001f : dir.z);
            
            Vec3 max = min + size;
            float t1 = (min.x - origin.x) * dirfrac.x;
            float t2 = (max.x - origin.x) * dirfrac.x;
            float t3 = (min.y - origin.y) * dirfrac.y;
            float t4 = (max.y - origin.y) * dirfrac.y;
            float t5 = (min.z - origin.z) * dirfrac.z;
            float t6 = (max.z - origin.z) * dirfrac.z;
            
            float tmin = Math::Maximum(Math::Maximum(Math::Minimum(t1, t2), Math::Minimum(t3, t4)), Math::Minimum(t5, t6));
            float tmax = Math::Minimum(Math::Minimum(Math::Maximum(t1, t2), Math::Maximum(t3, t4)), Math::Maximum(t5, t6));
            
            if (tmax < 0)
            {
                return false;
            }
            
            if (tmin > tmax)
            {
                return false;
            }
            
            out = tmin < 0 ? origin : origin + dir * tmin;
            return true;
        }
        
        
        float getLineSegmentDistance(const Vec3& origin, const Vec3& dir, const Vec3& a, const Vec3& b)
        {
            Vec3 a_origin = origin - a;
            Vec3 ab = b - a;
            
            float dot1 = DotProduct(ab, a_origin);
            float dot2 = DotProduct(ab, dir);
            float dot3 = DotProduct(dir, a_origin);
            float dot4 = DotProduct(ab, ab);
            float dot5 = DotProduct(dir, dir);
            
            float denom = dot4 * dot5 - dot2 * dot2;
            if (abs(denom) < 1e-5f)
            {
                Vec3 X = origin + dir * DotProduct(b - origin, dir);
                return (b - X).Length();
            }
            
            float numer = dot1 * dot2 - dot3 * dot4;
            float param_a = numer / denom;
            float param_b = (dot1 + dot2 * param_a) / dot4;
            
            if (param_b < 0 || param_b > 1)
            {
                param_b = Math::Clamp(param_b, 0.0f, 1.0f);
                Vec3 B = a + ab * param_b;
                Vec3 X = origin + dir * DotProduct(b - origin, dir);
                return (B - X).Length();
            }
            
            Vec3 vec = (origin + dir * param_a) - (a + ab * param_b);
            return vec.Length();
        }
        
        
        bool GetRayTriangleIntersection(const Vec3& origin,
                                        const Vec3& dir,
                                        const Vec3& p0,
                                        const Vec3& p1,
                                        const Vec3& p2,
                                        float* out_t)
        {
            Vec3 normal = CrossProduct(p1 - p0, p2 - p0);
            float q = DotProduct(normal, dir);
            if (q == 0) return false;
            
            float d = -DotProduct(normal, p0);
            float t = -(DotProduct(normal, origin) + d) / q;
            if (t < 0) return false;
            
            Vec3 hit_point = origin + dir * t;
            
            Vec3 edge0 = p1 - p0;
            Vec3 VP0 = hit_point - p0;
            if (DotProduct(normal, CrossProduct(edge0, VP0)) < 0)
            {
                return false;
            }
            
            Vec3 edge1 = p2 - p1;
            Vec3 VP1 = hit_point - p1;
            if (DotProduct(normal, CrossProduct(edge1, VP1)) < 0)
            {
                return false;
            }
            
            Vec3 edge2 = p0 - p2;
            Vec3 VP2 = hit_point - p2;
            if (DotProduct(normal, CrossProduct(edge2, VP2)) < 0)
            {
                return false;
            }
            
            if (out_t) *out_t = t;
            return true;
        }
        
        NEKO_ENGINE_API bool GetSphereTriangleIntersection(const Vec3& center,
                                                           float radius,
                                                           const Vec3& v0,
                                                           const Vec3& v1,
                                                           const Vec3& v2)
        {
            // TODO
            float squared_radius = radius * radius;
            if ((v0 - center).LengthSq() < squared_radius) return true;
            if ((v1 - center).LengthSq() < squared_radius) return true;
            if ((v2 - center).LengthSq() < squared_radius) return true;
            return false;
        }
        
        static std::mt19937_64& GetGUIDRandomGenerator()
        {
            static std::random_device seed;
            static std::mt19937_64 gen(seed());
            
            return gen;
        }
        
        static std::mt19937& GetRandomGenerator()
        {
            static std::random_device seed;
            static std::mt19937 gen(seed());
            
            return gen;
        }
        
        uint64 RandGUID()
        {
            return GetGUIDRandomGenerator()();
        }
        
        void SeedRandomGUID(uint32 seed)
        {
            GetGUIDRandomGenerator().seed(seed);
        }
        
        float ShortToNormalizedFloat(int16 AxisVal)
        {
            // normalize [-32768..32767] -> [-1..1]
            const float Norm = (AxisVal <= 0 ? 32768.f : 32767.f);
            return float(AxisVal) / Norm;
        }
        
        float pow(float base, float exponent)
        {
            return ::pow(base, exponent);
        }
        
        
        uint32 rand()
        {
            return GetRandomGenerator()();
        }
        
        
        uint32 rand(uint32 from, uint32 to)
        {
            std::uniform_int_distribution<> dist(from, to);
            return dist(GetRandomGenerator());
        }
        
        
        float RandFloat()
        {
            std::uniform_real_distribution<float> dist;
            return dist(GetRandomGenerator());
        }
        
        
        void SeedRandom(uint32 seed)
        {
            GetRandomGenerator().seed(seed);
        }
        
        
        float RandFloat(float from, float to)
        {
            std::uniform_real_distribution<float> dist(from, to);
            return dist(GetRandomGenerator());
        }
    } // ~namespace Math
} // ~namespace Neko
