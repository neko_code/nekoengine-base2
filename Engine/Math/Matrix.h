//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Matrix.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "Quat.h"
#include "Vec.h"

namespace Neko
{
    namespace EAxis3D
    {
        enum Type
        {
            X,
            Y,
            Z
        };
    }
    
    struct Quat;
    
    /// Angles (pitch, yaw, roll)
    class NEKO_ENGINE_API CAngles
    {
    public:
        float pitch;
        float yaw;
        float roll;
        
        NEKO_FORCE_INLINE CAngles(void)
        {
        }
        
        NEKO_FORCE_INLINE CAngles(float pitch, float yaw, float roll)
        {
            this->pitch = pitch;
            this->yaw = yaw;
            this->roll = roll;
        }
        
        explicit NEKO_FORCE_INLINE CAngles(const Vec3& v)
        {
            this->pitch = v.x;
            this->yaw = v.y;
            this->roll = v.z;
        }
        
        NEKO_FORCE_INLINE void Set(float pitch, float yaw, float roll)
        {
            this->pitch = pitch;
            this->yaw = yaw;
            this->roll = roll;
        }
        
        NEKO_FORCE_INLINE CAngles& Zero(void)
        {
            pitch = yaw = roll = 0.0f;
            return *this;
        }
        
        NEKO_FORCE_INLINE bool Compare(const CAngles& a) const
        {
            return ((a.pitch == pitch) && (a.yaw == yaw) && (a.roll == roll));
        }
        
        NEKO_FORCE_INLINE bool Compare(const CAngles& a, const float epsilon) const
        {
            if (Math::abs(pitch - a.pitch) > epsilon)
            {
                return false;
            }
            
            if (Math::abs(yaw - a.yaw) > epsilon)
            {
                return false;
            }
            
            if (Math::abs(roll - a.roll) > epsilon)
            {
                return false;
            }
            
            return true;
        }
        
        NEKO_FORCE_INLINE void Clamp(const CAngles& min, const CAngles& max)
        {
            if (pitch < min.pitch)
            {
                pitch = min.pitch;
            }
            else if (pitch > max.pitch)
            {
                pitch = max.pitch;
            }
            if (yaw < min.yaw)
            {
                yaw = min.yaw;
            }
            else if (yaw > max.yaw)
            {
                yaw = max.yaw;
            }
            if (roll < min.roll)
            {
                roll = min.roll;
            }
            else if (roll > max.roll)
            {
                roll = max.roll;
            }
        }
        
        NEKO_FORCE_INLINE int GetDimension(void) const
        {
            return 3;
        }
        
        NEKO_FORCE_INLINE const float* ToFloatPtr(void) const
        {
            return &pitch;
        }
        
        NEKO_FORCE_INLINE float* ToFloatPtr(void)
        {
            return &pitch;
        }
        
        
        NEKO_FORCE_INLINE float operator[](int index) const
        {
            assert((index >= 0) && (index < 3));
            return (&pitch)[index];
        }
        
        NEKO_FORCE_INLINE float& operator[](int index)
        {
            assert((index >= 0) && (index < 3));
            return (&pitch)[index];
        }
        
        NEKO_FORCE_INLINE CAngles operator-() const
        {
            return CAngles(-pitch, -yaw, -roll);
        }
        
        NEKO_FORCE_INLINE CAngles& operator=(const CAngles& a)
        {
            pitch = a.pitch;
            yaw = a.yaw;
            roll = a.roll;
            return *this;
        }
        
        NEKO_FORCE_INLINE CAngles operator+(const CAngles& a) const
        {
            return CAngles(pitch + a.pitch, yaw + a.yaw, roll + a.roll);
        }
        
        NEKO_FORCE_INLINE CAngles& operator+=(const CAngles& a)
        {
            pitch += a.pitch;
            yaw += a.yaw;
            roll += a.roll;
            
            return *this;
        }
        
        NEKO_FORCE_INLINE CAngles operator-(const CAngles& a) const
        {
            return CAngles(pitch - a.pitch, yaw - a.yaw, roll - a.roll);
        }
        
        NEKO_FORCE_INLINE CAngles& operator-=(const CAngles& a)
        {
            pitch -= a.pitch;
            yaw -= a.yaw;
            roll -= a.roll;
            
            return *this;
        }
        
        NEKO_FORCE_INLINE CAngles operator*(const float a) const
        {
            return CAngles(pitch * a, yaw * a, roll * a);
        }
        
        NEKO_FORCE_INLINE CAngles& operator*=(float a)
        {
            pitch *= a;
            yaw *= a;
            roll *= a;
            return *this;
        }
        
        NEKO_FORCE_INLINE CAngles operator/(const float a) const
        {
            float inva = 1.0f / a;
            return CAngles(pitch * inva, yaw * inva, roll * inva);
        }
        
        NEKO_FORCE_INLINE CAngles& operator/=(float a)
        {
            float inva = 1.0f / a;
            pitch *= inva;
            yaw *= inva;
            roll *= inva;
            return *this;
        }
        
        friend NEKO_FORCE_INLINE CAngles operator*(const float a, const CAngles& b)
        {
            return CAngles(a * b.pitch, a * b.yaw, a * b.roll);
        }
        
        NEKO_FORCE_INLINE bool operator==(const CAngles& a) const
        {
            return Compare(a);
        }
        
        NEKO_FORCE_INLINE bool operator!=(const CAngles& a) const
        {
            return !Compare(a);
        }
        
        
        CAngles& Normalize360();
        CAngles& Normalize180();
    };
    

    struct FTransform;
    
    struct NEKO_ENGINE_API RigidTransform
    {
        RigidTransform()
        { }
        
        RigidTransform(const Vec3& _pos, const Quat& _rot)
        : pos(_pos)
        , rot(_rot)
        { }
        
        RigidTransform Inverted() const
        {
            RigidTransform result;
            result.rot = rot.Conjugated();
            result.pos = result.rot.Rotate(-pos);
            return result;
        }
        
        RigidTransform operator*(const RigidTransform& rhs) const
        {
            return { rot.Rotate(rhs.pos) + pos, rot * rhs.rot };
        }
        
        Vec3 Transform(const Vec3& value) const
        {
            return pos + rot.Rotate(value);
        }
        
        RigidTransform Interpolate(const RigidTransform& rhs, float t)
        {
            RigidTransform ret;
            Lerp(pos, rhs.pos, &ret.pos, t);
            nlerp(rot, rhs.rot, &ret.rot, t);
            return ret;
        }
        
        Vec3 InverseTransformPositionNoScale(const Vec3& V) const
        {
            const Vec3 Translated = V - pos;
            RigidTransform result;
            result.rot = rot.Conjugated();
            result.pos = result.rot.Rotate(-Translated);
            return result.pos;
        }
        
        NEKO_FORCE_INLINE Vec3 GetUnitAxis(EAxis3D::Type InAxis) const
        {
            if (InAxis == EAxis3D::X)
            {
                return Transform(Vec3(1.f, 0.f, 0.f));
            }
            else if (InAxis == EAxis3D::Y)
            {
                return Transform(Vec3(0.f, 1.f, 0.f));
            }
            
            return Transform(Vec3(0.f, 0.f, 1.f));
        }
        
        NEKO_FORCE_INLINE FTransform ToScaled(float scale) const;
        
        Matrix ToMatrix() const;
        
        Quat rot;
        Vec3 pos;
    };
    
    
    struct NEKO_ENGINE_API FTransform
    {
        FTransform()
        {
        }
        
        FTransform(const Vec3& _pos, const Quat& _rot, float _scale)
        : rot(_rot)
        , pos(_pos)
        , scale(_scale)
        {
        }
        
        FTransform Inverted() const
        {
            FTransform result;
            result.rot = rot.Conjugated();
            result.pos = result.rot.Rotate(-pos / scale);
            result.scale = 1.0f / scale;
            return result;
        }
        
        FTransform operator*(const FTransform& rhs) const
        {
            return { rot.Rotate(rhs.pos * scale) + pos, rot * rhs.rot, scale * rhs.scale };
        }
        
        Vec3 Transform(const Vec3& value) const
        {
            return pos + rot.Rotate(value) * scale;
        }
        
        FTransform Interpolate(const FTransform& rhs, float t)
        {
            FTransform ret;
            Lerp(pos, rhs.pos, &ret.pos, t);
            nlerp(rot, rhs.rot, &ret.rot, t);
            return ret;
        }
        
        
        NEKO_FORCE_INLINE Vec3 GetUnitAxis(EAxis3D::Type InAxis) const
        {
            if (InAxis == EAxis3D::X)
            {
                return Transform(Vec3(1.f, 0.f, 0.f));
            }
            else if (InAxis == EAxis3D::Y)
            {
                return Transform(Vec3(0.f, 1.f, 0.f));
            }
            
            return Transform(Vec3(0.f, 0.f, 1.f));
        }
        
        RigidTransform GetRigidPart() const
        {
            return { pos, rot };
        }
        
        Matrix ToMatrix() const;
        
        Quat rot;
        Vec3 pos;
        float scale;
    };
    
    
    FTransform RigidTransform::ToScaled(float scale) const
    {
        return { pos, rot, scale };
    }
    
    
    NEKO_ALIGN_PREFIX(16) struct NEKO_ENGINE_API Matrix
    {
        static Matrix RotationX(float angle);
        static Matrix RotationY(float angle);
        static Matrix RotationZ(float angle);
        
        Matrix()
        {
        }
        
        Matrix(const Vec3& pos, const Quat& rot);
        
        Matrix(const float* m)
        {
            m11 = m[0];
            m12 = m[1];
            m13 = m[2];
            m14 = m[3];
            m21 = m[4];
            m22 = m[5];
            m23 = m[6];
            m24 = m[7];
            m31 = m[8];
            m32 = m[9];
            m33 = m[10];
            m34 = m[11];
            m41 = m[12];
            m42 = m[13];
            m43 = m[14];
            m44 = m[15];
        }
        
        Matrix(float r11, float r12, float r13, float r14, float r21, float r22, float r23, float r24,
               float r31, float r32, float r33, float r34, float r41, float r42, float r43, float r44)
        {
            m11 = r11;
            m12 = r12;
            m13 = r13;
            m14 = r14;
            m21 = r21;
            m22 = r22;
            m23 = r23;
            m24 = r24;
            m31 = r31;
            m32 = r32;
            m33 = r33;
            m34 = r34;
            m41 = r41;
            m42 = r42;
            m43 = r43;
            m44 = r44;
        }
        
        Matrix(const Vec3& InX, const Vec3& InY, const Vec3& InZ, const Vec3& InW)
        {
            m11 = InX.x;
            m12 = InX.y;
            m13 = InX.z;
            m14 = 0.0f;
            m21 = InY.x;
            m22 = InY.y;
            m23 = InY.z;
            m24 = 0.0f;
            m31 = InZ.x;
            m32 = InZ.y;
            m33 = InZ.z;
            m34 = 0.0f;
            m41 = InW.x;
            m42 = InW.y;
            m43 = InW.z;
            m44 = 0.0f;
        }
        
        Matrix operator*(const Matrix& rhs) const;
        Matrix operator+(const Matrix& rhs) const;
        
        Matrix operator*(float rhs) const;
        
        Vec4 operator*(const Vec4& rhs) const
        {
            return Vec4(m11 * rhs.x + m21 * rhs.y + m31 * rhs.z + m41 * rhs.w,
                        m12 * rhs.x + m22 * rhs.y + m32 * rhs.z + m42 * rhs.w,
                        m13 * rhs.x + m23 * rhs.y + m33 * rhs.z + m43 * rhs.w,
                        m14 * rhs.x + m24 * rhs.y + m34 * rhs.z + m44 * rhs.w);
        }
        
        Vec3 GetWVector() const
        {
            return Vec3(m41, m42, m43);
        }
        
        Vec3 GetZVector() const
        {
            return Vec3(m31, m32, m33);
        }
        
        Vec3 GetYVector() const
        {
            return Vec3(m21, m22, m23);
        }
        
        Vec3 GetXVector() const
        {
            return Vec3(m11, m12, m13);
        }
        
        void SetXVector(const Vec3& v)
        {
            m11 = v.x;
            m12 = v.y;
            m13 = v.z;
        }
        
        void SetYVector(const Vec3& v)
        {
            m21 = v.x;
            m22 = v.y;
            m23 = v.z;
        }
        
        void SetZVector(const Vec3& v)
        {
            m31 = v.x;
            m32 = v.y;
            m33 = v.z;
        }
        
        float Determinant() const
        {
            return m14 * m23 * m32 * m41 - m13 * m24 * m32 * m41 - m14 * m22 * m33 * m41
            + m12 * m24 * m33 * m41 + m13 * m22 * m34 * m41 - m12 * m23 * m34 * m41
            - m14 * m23 * m31 * m42 + m13 * m24 * m31 * m42 + m14 * m21 * m33 * m42
            - m11 * m24 * m33 * m42 - m13 * m21 * m34 * m42 + m11 * m23 * m34 * m42
            + m14 * m22 * m31 * m43 - m12 * m24 * m31 * m43 - m14 * m21 * m32 * m43
            + m11 * m24 * m32 * m43 + m12 * m21 * m34 * m43 - m11 * m22 * m34 * m43
            - m13 * m22 * m31 * m44 + m12 * m23 * m31 * m44 + m13 * m21 * m32 * m44
            - m11 * m23 * m32 * m44 - m12 * m21 * m33 * m44 + m11 * m22 * m33 * m44;
        }
        
        void Decompose(Vec3& position, Quat& rotation, float& scale) const;
        
        void Inverse()
        {
            Matrix mtx;
            float d = Determinant();
            if (d == 0)
                return;
            d = 1.0f / d;
            
            mtx.m11 = d * (m23 * m34 * m42 - m24 * m33 * m42 + m24 * m32 * m43 - m22 * m34 * m43
                           - m23 * m32 * m44 + m22 * m33 * m44);
            mtx.m12 = d * (m14 * m33 * m42 - m13 * m34 * m42 - m14 * m32 * m43 + m12 * m34 * m43
                           + m13 * m32 * m44 - m12 * m33 * m44);
            mtx.m13 = d * (m13 * m24 * m42 - m14 * m23 * m42 + m14 * m22 * m43 - m12 * m24 * m43
                           - m13 * m22 * m44 + m12 * m23 * m44);
            mtx.m14 = d * (m14 * m23 * m32 - m13 * m24 * m32 - m14 * m22 * m33 + m12 * m24 * m33
                           + m13 * m22 * m34 - m12 * m23 * m34);
            mtx.m21 = d * (m24 * m33 * m41 - m23 * m34 * m41 - m24 * m31 * m43 + m21 * m34 * m43
                           + m23 * m31 * m44 - m21 * m33 * m44);
            mtx.m22 = d * (m13 * m34 * m41 - m14 * m33 * m41 + m14 * m31 * m43 - m11 * m34 * m43
                           - m13 * m31 * m44 + m11 * m33 * m44);
            mtx.m23 = d * (m14 * m23 * m41 - m13 * m24 * m41 - m14 * m21 * m43 + m11 * m24 * m43
                           + m13 * m21 * m44 - m11 * m23 * m44);
            mtx.m24 = d * (m13 * m24 * m31 - m14 * m23 * m31 + m14 * m21 * m33 - m11 * m24 * m33
                           - m13 * m21 * m34 + m11 * m23 * m34);
            mtx.m31 = d * (m22 * m34 * m41 - m24 * m32 * m41 + m24 * m31 * m42 - m21 * m34 * m42
                           - m22 * m31 * m44 + m21 * m32 * m44);
            mtx.m32 = d * (m14 * m32 * m41 - m12 * m34 * m41 - m14 * m31 * m42 + m11 * m34 * m42
                           + m12 * m31 * m44 - m11 * m32 * m44);
            mtx.m33 = d * (m12 * m24 * m41 - m14 * m22 * m41 + m14 * m21 * m42 - m11 * m24 * m42
                           - m12 * m21 * m44 + m11 * m22 * m44);
            mtx.m34 = d * (m14 * m22 * m31 - m12 * m24 * m31 - m14 * m21 * m32 + m11 * m24 * m32
                           + m12 * m21 * m34 - m11 * m22 * m34);
            mtx.m41 = d * (m23 * m32 * m41 - m22 * m33 * m41 - m23 * m31 * m42 + m21 * m33 * m42
                           + m22 * m31 * m43 - m21 * m32 * m43);
            mtx.m42 = d * (m12 * m33 * m41 - m13 * m32 * m41 + m13 * m31 * m42 - m11 * m33 * m42
                           - m12 * m31 * m43 + m11 * m32 * m43);
            mtx.m43 = d * (m13 * m22 * m41 - m12 * m23 * m41 - m13 * m21 * m42 + m11 * m23 * m42
                           + m12 * m21 * m43 - m11 * m22 * m43);
            mtx.m44 = d * (m12 * m23 * m31 - m13 * m22 * m31 + m13 * m21 * m32 - m11 * m23 * m32
                           - m12 * m21 * m33 + m11 * m22 * m33);
            
            *this = mtx;
        }
        
        // orthonormal
        void FastInverse()
        {
            float tmp = m21;
            m21 = m12;
            m12 = tmp;
            
            tmp = m32;
            m32 = m23;
            m23 = tmp;
            
            tmp = m31;
            m31 = m13;
            m13 = tmp;
            
            float m41 = -this->m41;
            float m42 = -this->m42;
            float m43 = -this->m43;
            this->m41 = m41 * m11 + m42 * m21 + m43 * m31;
            this->m42 = m41 * m12 + m42 * m22 + m43 * m32;
            this->m43 = m41 * m13 + m42 * m23 + m43 * m33;
        }
        
        void Copy3x3(const Matrix& mtx)
        {
            m11 = mtx.m11;
            m12 = mtx.m12;
            m13 = mtx.m13;
            
            m21 = mtx.m21;
            m22 = mtx.m22;
            m23 = mtx.m23;
            
            m31 = mtx.m31;
            m32 = mtx.m32;
            m33 = mtx.m33;
        }
        
        void Translate(const Vec3& t)
        {
            m41 += t.x;
            m42 += t.y;
            m43 += t.z;
        }
        
        void Translate(float x, float y, float z)
        {
            m41 += x;
            m42 += y;
            m43 += z;
        }
        
        void SetTranslation(const Vec3& t)
        {
            m41 = t.x;
            m42 = t.y;
            m43 = t.z;
        }
        
        
        void SetOrtho(float left, float right, float bottom, float top, float z_near, float z_far, bool is_homogenous_depth)
        {
            *this = IDENTITY;
            m11 = 2 / (right - left);
            m22 = 2 / (top - bottom);
            m33 = (is_homogenous_depth ? -2 : -1) / (z_far - z_near);
            m41 = (right + left) / (left - right);
            m42 = (top + bottom) / (bottom - top);
            m43 = is_homogenous_depth ? (z_near + z_far) / (z_near - z_far) : z_near / (z_near - z_far);
        }
        
        void SetPerspective(float fov, float ratio, float near_plane, float far_plane, bool is_homogenous_depth);
        
        Matrix GetPerspectiveProjection(float left, float right, float bottom, float top, float n, float f);
        
        void FromEuler(float yaw, float pitch, float roll);
        
        void LookAt(const Vec3& eye, const Vec3& at, const Vec3& up)
        {
            *this = Matrix::IDENTITY;
            Vec3 f = eye - at;
            f.Normalize();
            Vec3 r = CrossProduct(up, f);
            r.Normalize();
            Vec3 u = CrossProduct(f, r);
            SetXVector(r);
            SetYVector(u);
            SetZVector(f);
            Transpose();
            SetTranslation(Vec3(-DotProduct(r, eye), -DotProduct(u, eye), -DotProduct(f, eye)));
        }
        
        void GetTranslation(Vec3& pos) const
        {
            pos.Set(m41, m42, m43);
        }
        
        void NormalizeScale();
        
        Vec3 GetTranslation() const
        {
            return Vec3(m41, m42, m43);
        }
        
        RigidTransform ToTransform()
        {
            return
            {
                GetTranslation(),
                GetRotation()
            };
        }
        
        Quat GetRotation() const;
        void Transpose();
        Vec3 TransformPoint(const Vec3& Position) const;
        Vec3 TransformVector(const Vec3& Position) const;
        void Multiply3x3(float scale);
        void SetIdentity();
        
        float m11, m12, m13, m14;
        float m21, m22, m23, m24;
        float m31, m32, m33, m34;
        float m41, m42, m43, m44;
        
        static const Matrix IDENTITY;
    } NEKO_ALIGN_SUFFIX(16);
} // Neko

