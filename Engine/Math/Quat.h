//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Quat.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "Vec.h"

namespace Neko
{
    struct Matrix;
    
    struct NEKO_ENGINE_API Quat
    {
        struct AxisAngle
        {
            Vec3 axis;
            float angle;
        };
        
        Quat()
        { }
        
        Quat(const Vec3& axis, float angle);
        Quat(float _x, float _y, float _z, float _w)
        {
            x = _x;
            y = _y;
            z = _z;
            w = _w;
        }
        
        void FromEuler(const Vec3& euler);
        Vec3 ToEuler() const;
        AxisAngle GetAxisAngle() const;
        void Set(float _x, float _y, float _z, float _w) { x = _x; y = _y; z = _z; w = _w; }
        void Conjugate();
        Quat Conjugated() const;
        void Normalize();
        Quat Normalized() const;
        Matrix ToMatrix() const;
        
        Vec3 Rotate(const Vec3& v) const
        {
            // nVidia SDK implementation
            
            Vec3 uv, uuv;
            Vec3 qvec(x, y, z);
            uv = CrossProduct(qvec, v);
            uuv = CrossProduct(qvec, uv);
            uv *= (2.0f * w);
            uuv *= 2.0f;
            
            return v + uv + uuv;
        }
        
        Quat operator*(const Quat& q) const;
        Quat operator-() const;
        Quat operator+(const Quat& q) const;
        Quat operator*(float m) const;
        Vec3 operator*(const Vec3& q) const;
        
        static Quat Vec3ToVec3(const Vec3& a, const Vec3& b);
        
        float x, y, z, w;
        
        static const Quat IDENTITY;
    };
    
    
    NEKO_ENGINE_API void nlerp(const Quat& q1, const Quat& q2, Quat* out, float t);
    
    
} // namespace Neko
