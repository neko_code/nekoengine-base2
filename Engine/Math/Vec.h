//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Vec.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "MathUtils.h"
#include "Simd.h"
#include "../Utilities/StringUtil.h"
#include "../Neko.h"

namespace Neko
{
    /// Simple 2D point
    struct Int2
    {
        static Int2 Zero;
        
        Int2() : x(0), y(0)
        {
        }
        
        Int2(int X, int Y) : x(X), y(Y)
        {
        }
        
        int x;
        int y;
    };
    
    /// Two-dimensional vector.
    struct NEKO_ENGINE_API Vec2
    {
        static Vec2 Zero;
        
        Vec2() { }
        
        Vec2(float v)
        : x(v)
        , y(v) { }
        
        Vec2(float a, float b)
        : x(a)
        , y(b) { }
        
        Vec2(const Vec2& Other)
        : x(Other.x)
        , y(Other.y) { }
        
        void Set(float a, float b)
        {
            x = a;
            y = b;
        }
        
        /** Makes all vector values to zero. */
        NEKO_FORCE_INLINE void MakeNull()
        {
            x = y = 0.0f;
        }
        
        // Operators
        
        NEKO_FORCE_INLINE Vec2 operator*(const float rhs) const
        {
            return Vec2(x * rhs, y * rhs);
        }
        NEKO_FORCE_INLINE Vec2 operator/(const float rhs) const
        {
            return Vec2(x / rhs, y / rhs);
        }
        NEKO_FORCE_INLINE Vec2 operator+(const Vec2& rhs) const
        {
            return Vec2(x + rhs.x, y + rhs.y);
        }
        NEKO_FORCE_INLINE Vec2 operator-(const Vec2& rhs) const
        {
            return Vec2(x - rhs.x, y - rhs.y);
        }
        NEKO_FORCE_INLINE Vec2 operator*(const Vec2& rhs) const
        {
            return Vec2(x * rhs.x, y * rhs.y);
        }
        NEKO_FORCE_INLINE Vec2 operator/(const Vec2& rhs) const
        {
            return Vec2(x / rhs.x, y / rhs.y);
        }
        
        NEKO_FORCE_INLINE Vec2& operator+=(const Vec2& rhs)
        {
            x += rhs.x;
            y += rhs.y;
            return *this;
        }
        NEKO_FORCE_INLINE Vec2& operator-=(const Vec2& rhs)
        {
            x -= rhs.x;
            y -= rhs.y;
            return *this;
        }
        NEKO_FORCE_INLINE Vec2& operator*=(const float rhs)
        {
            x *= rhs;
            y *= rhs;
            return *this;
        }
        NEKO_FORCE_INLINE Vec2& operator/=(float rhs)
        {
            x /= rhs;
            y /= rhs;
            return *this;
        }
        
        NEKO_FORCE_INLINE bool operator>=(const Vec2& Rhs) const
        {
            if (x >= Rhs.x && y >= Rhs.y)
            {
                return true;
            }
            
            return false;
        }
        NEKO_FORCE_INLINE bool operator<=(const Vec2& Rhs) const
        {
            if (x <= Rhs.x && y <= Rhs.y)
            {
                return true;
            }
            
            return false;
        }
        
        template<typename L>
        float& operator[](L i)
        {
            return (&x)[i];
        }
        
        template<typename L>
        float operator[](L i) const
        {
            return (&x)[i];
        }
        
        bool operator==(const Vec2& rhs) const
        {
            return x == rhs.x && y == rhs.y;
        }
        
        bool operator!=(const Vec2& rhs) const
        {
            return x != rhs.x || y != rhs.y;
        }
        
        
        Vec2 Normalized() const
        {
            return *this * (1 / Length());
        }
        
        float Length() const;
        float LengthSq() const;
        
        float x, y;
        
        // Helpers
        // These are dedicated in this class, not in 'Math' because we can mess this up
        
        static NEKO_FORCE_INLINE Vec2 Min(const Vec2& lhs, const Vec2& rhs)
        {
            return Vec2(Math::Minimum(lhs.x, rhs.x), Math::Minimum(lhs.y, rhs.y));
        }
        
        static NEKO_FORCE_INLINE Vec2 Max(const Vec2& lhs, const Vec2& rhs)
        {
            return Vec2(Math::Maximum(lhs.x, rhs.x), Math::Maximum(lhs.y, rhs.y));
        }
        
        static NEKO_FORCE_INLINE Vec2 Lerp(const Vec2& a, const Vec2& b, const Vec2& value)
        {
            return Vec2(a.x + (b.x - a.x) * value.x, a.y + (b.y - a.y) * value.y);
        }
        
        static NEKO_FORCE_INLINE Vec2 Clamp(const Vec2& f, const Vec2& mn, Vec2 mx)
        {
            return Vec2(Math::Clamp(f.x, mn.x, mx.x), Math::Clamp(f.y, mn.y, mx.y));
        }
        
      
        
        NEKO_FORCE_INLINE const float* ToFloatPtr() const
        {
            return &x;
        }
        
        NEKO_FORCE_INLINE float* ToFloatPtr()
        {
            return &x;
        }
        
        NEKO_FORCE_INLINE int32 GetDimension() const
        {
            return 2;
        }
        
        NEKO_FORCE_INLINE const char* ToString(const int Precision = 2) const
        {
            return FloatArrayToString(ToFloatPtr(), GetDimension(), Precision);
        }
    };
	
	inline Vec2 operator * (float f, const Vec2& v)
	{
		return Vec2(f * v.x, f * v.y);
	}
	
	inline Vec2 operator + (float f, const Vec2& v)
	{
		return Vec2(f + v.x, f + v.y);
	}
    
    class CAngles;
	
    /// Three-dimensional vector.
    struct NEKO_ENGINE_API Vec3
	{
		static Vec3 Zero;
        static Vec3 UpVector;
		

        Vec3()
        : x(0.f)
        , y(0.f)
        , z(0.f)
        { }
        
        Vec3(const Vec2& v, float c)
        : x(v.x)
        , y(v.y)
        , z(c)
        { }
        
        Vec3(float a, float b, float c)
        : x(a)
        , y(b)
        , z(c)
        { }
        
        Vec3 operator+(const Vec3& rhs) const
        {
            return Vec3(x + rhs.x, y + rhs.y, z + rhs.z);
        }
        
        Vec3 operator-() const
        {
            return Vec3(-x, -y, -z);
        }
        
        Vec3 operator-(const Vec3& rhs) const
        {
            return Vec3(x - rhs.x, y - rhs.y, z - rhs.z);
        }
        
        void operator+=(const Vec3& rhs)
        {
            float x = this->x;
            float y = this->y;
            float z = this->z;
            x += rhs.x;
            y += rhs.y;
            z += rhs.z;
            this->x = x;
            this->y = y;
            this->z = z;
        }
        
        void operator-=(const Vec3& rhs)
        {
            float x = this->x;
            float y = this->y;
            float z = this->z;
            x -= rhs.x;
            y -= rhs.y;
            z -= rhs.z;
            this->x = x;
            this->y = y;
            this->z = z;
        }
        
        Vec3 operator*(float s) const
        {
            return Vec3(x * s, y * s, z * s);
        }
        
        Vec3 operator/(float s) const
        {
            float tmp = 1 / s;
            return Vec3(x * tmp, y * tmp, z * tmp);
        }
        
        void operator*=(float rhs)
        {
            float x = this->x;
            float y = this->y;
            float z = this->z;
            x *= rhs;
            y *= rhs;
            z *= rhs;
            this->x = x;
            this->y = y;
            this->z = z;
        }
        template<typename L>
        float& operator[](L i)
        {
            return (&x)[i];
        }
        
        template<typename L>
        float operator[](L i) const
        {
            return (&x)[i];
        }
        
        bool operator==(const Vec3& rhs) const
        {
            return x == rhs.x && y == rhs.y && z == rhs.z;
        }
        
        bool operator!=(const Vec3& rhs) const
        {
            return x != rhs.x || y != rhs.y || z != rhs.z;
        }
        
        void operator/=(float rhs)
        {
            *this *= 1.0f / rhs;
        }
        
        Vec3 Normalized() const;
        
        void Normalize();
        
        CAngles ToOrientationAngles() const;
        
        NEKO_FORCE_INLINE void ToDirectionAndLength(Vec3& OutDir, float& OutLength) const
        {
            OutLength = sqrtf(x * x + y * y + z * z);
            if (OutLength > SMALL_NUMBER)
            {
                float OneOverLength = 1.0f / OutLength;
                OutDir = Vec3(x * OneOverLength, y * OneOverLength, z * OneOverLength);
            }
            else
            {
                OutDir = Vec3::Zero;
            }
        }

        Vec3 GetSafeNormal(float Tolerance = SMALL_NUMBER) const
        {
            const float Square = x*x + y*y + z*z;
            
            if (Square == 1.0f)
            {
                return *this;
            }
            else if (Square < Tolerance)
            {
                return Vec3::Zero;
            }
            
            const float Scale = Math::InvSqrt(Square);
            return Vec3(x * Scale, y * Scale, z * Scale);
        }
        
        void Set(float x, float y, float z)
        {
            this->x = x;
            this->y = y;
            this->z = z;
        }
        
        static NEKO_FORCE_INLINE float Dist(const Vec3 &V1, const Vec3 &V2)
        {
            return sqrtf(DistSquared(V1, V2));
        }
        
        static NEKO_FORCE_INLINE float DistSquared(const Vec3 &V1, const Vec3 &V2)
        {
            return Math::Square(V2.x-V1.x) + Math::Square(V2.y-V1.y) + Math::Square(V2.z-V1.z);
        }

        float Length() const;
        
        float LengthSq() const
        {
            float x = this->x;
            float y = this->y;
            float z = this->z;
            return x * x + y * y + z * z;
        }
        
        NEKO_FORCE_INLINE void MakeZero()
        {
            x = y = z = 0.0f;
        }
        
        NEKO_FORCE_INLINE const float* ToFloatPtr() const
        {
            return &x;
        }
        
        NEKO_FORCE_INLINE float* ToFloatPtr()
        {
            return &x;
        }
        
        NEKO_FORCE_INLINE int32 GetDimension() const
        {
            return 3;
        }
        
        NEKO_FORCE_INLINE const char* ToString(const int Precision = 2) const
        {
            return FloatArrayToString(ToFloatPtr(), GetDimension(), Precision);
        }
        
        union
        {
            struct { float x, y, z; };
            struct { float r, g, b; };
        };
    };
    
    
    inline Vec3 operator*(float f, const Vec3& v)
    {
        return Vec3(f * v.x, f * v.y, f * v.z);
    }
    
    /// Four-dimensional vector.
    struct NEKO_ENGINE_API Vec4
    {
        static Vec4 Zero;
        
        Vec4()
        {
        }
        Vec4(float a, float b, float c, float d)
        : x(a)
        , y(b)
        , z(c)
        , w(d)
        { }
        
        Vec4(const Vec3& v, float d)
        : x(v.x)
        , y(v.y)
        , z(v.z)
        , w(d)
        { }
        
        Vec4(const Vec2& v1, const Vec2& v2)
        : x(v1.x)
        , y(v1.y)
        , z(v2.x)
        , w(v2.y)
        { }
        
        Vec3 xyz() const { return Vec3(x, y, z); }
        Vec3 rgb() const { return Vec3(x, y, z); }
        
    
        template<typename L>
        float& operator[](L i)
        {
            return (&x)[i];
        }
        
        template<typename L>
        float operator[](L i) const
        {
            return (&x)[i];
        }
        
        bool operator==(const Vec4& rhs) const
        {
            return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w;
        }
        
        bool operator!=(const Vec4& rhs) const
        {
            return x != rhs.x || y != rhs.y || z != rhs.z || w != rhs.w;
        }
        
        Vec4 operator+(const Vec4& rhs) const
        {
            return Vec4(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
        }
        
        Vec4 operator-() const
        {
            return Vec4(-x, -y, -z, -w);
        }
        
        Vec4 operator-(const Vec4& rhs) const
        {
            return Vec4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
        }
        
        void operator/=(float rhs)
        {
            *this *= 1.0f / rhs;
        }
        
        void operator+=(const Vec4& rhs)
        {
            float x = this->x;
            float y = this->y;
            float z = this->z;
            float w = this->w;
            x += rhs.x;
            y += rhs.y;
            z += rhs.z;
            w += rhs.w;
            this->x = x;
            this->y = y;
            this->z = z;
            this->w = w;
        }
        
        void operator-=(const Vec4& rhs)
        {
            float x = this->x;
            float y = this->y;
            float z = this->z;
            float w = this->w;
            x -= rhs.x;
            y -= rhs.y;
            z -= rhs.z;
            w -= rhs.w;
            this->x = x;
            this->y = y;
            this->z = z;
            this->w = w;
        }
        
        NEKO_FORCE_INLINE Vec4 operator-(const Vec4& rhs)
        {
            return Vec4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
        }
        
        
        Vec4 operator*(float s)
        {
            return Vec4(x * s, y * s, z * s, w * s);
        }
        
        void operator*=(float rhs)
        {
            float x = this->x;
            float y = this->y;
            float z = this->z;
            float w = this->w;
            x *= rhs;
            y *= rhs;
            z *= rhs;
            w *= rhs;
            this->x = x;
            this->y = y;
            this->z = z;
            this->w = w;
        }
        
        void Normalize();
        
        void Set(const Vec3& v, float w)
        {
            this->x = v.x;
            this->y = v.y;
            this->z = v.z;
            this->w = w;
        }
        
        void Set(float x, float y, float z, float w)
        {
            this->x = x;
            this->y = y;
            this->z = z;
            this->w = w;
        }
        
        void Set(const Vec4& rhs)
        {
            x = rhs.x;
            y = rhs.y;
            z = rhs.z;
            w = rhs.w;
        }
        
        NEKO_FORCE_INLINE void MakeZero()
        {
            x = y = z = w = 0.0f;
        }
        
        float Length() const;
        
        float LengthSq() const
        {
            float x = this->x;
            float y = this->y;
            float z = this->z;
            float w = this->w;
            return x * x + y * y + z * z + w * w;
        }
        
        operator Vec3()
        {
            return Vec3(x, y, z);
        }
        
        
        NEKO_FORCE_INLINE const float* ToFloatPtr() const
        {
            return &x;
        }
        
        NEKO_FORCE_INLINE float* ToFloatPtr()
        {
            return &x;
        }
        
        NEKO_FORCE_INLINE int32 GetDimension() const
        {
            return 4;
        }
        
        NEKO_FORCE_INLINE const char* ToString(const int Precision = 2) const
        {
            return FloatArrayToString(ToFloatPtr(), GetDimension(), Precision);
        }
        
        union
        {
            struct { float x, y, z, w; };
            struct { float r, g, b, a; };
        };
    };
    
    
    inline Vec4 operator*(const Vec4& v, float s)
    {
        return Vec4(v.x * s, v.y * s, v.z * s, v.w * s);
    }
    
    
    inline float DotProduct(const Vec4& op1, const Vec4& op2)
    {
        return op1.x * op2.x + op1.y * op2.y + op1.z * op2.z + op1.w * op2.w;
    }
    
    
    inline void Lerp(const Vec4& op1, const Vec4& op2, Vec4* out, float t)
    {
        const float invt = 1.0f - t;
        out->x = op1.x * invt + op2.x * t;
        out->y = op1.y * invt + op2.y * t;
        out->z = op1.z * invt + op2.z * t;
        out->w = op1.w * invt + op2.w * t;
    }
    
    
    inline void Lerp(const Vec3& op1, const Vec3& op2, Vec3* out, float t)
    {
        const float invt = 1.0f - t;
        out->x = op1.x * invt + op2.x * t;
        out->y = op1.y * invt + op2.y * t;
        out->z = op1.z * invt + op2.z * t;
    }
    
    
    inline void Lerp(const Vec2& op1, const Vec2& op2, Vec2* out, float t)
    {
        const float invt = 1.0f - t;
        out->x = op1.x * invt + op2.x * t;
        out->y = op1.y * invt + op2.y * t;
    }
    
    
    inline float DotProduct(const Vec3& op1, const Vec3& op2)
    {
        return op1.x * op2.x + op1.y * op2.y + op1.z * op2.z;
    }
    
    
    inline Vec3 CrossProduct(const Vec3& op1, const Vec3& op2)
    {
        return Vec3(op1.y * op2.z - op1.z * op2.y, op1.z * op2.x - op1.x * op2.z,
                    op1.x * op2.y - op1.y * op2.x);
    }
    
    /** Operator used to cross vectors. */
    inline Vec3 operator ^ (const Vec3& op1, const Vec3& op2)
    {
        return CrossProduct(op1, op2);
    }
    
    
    /**
     * Util to calculate distance from a point to a bounding box
     *
     * @param Mins 3D Point defining the lower values of the axis of the bound box
     * @param Max 3D Point defining the lower values of the axis of the bound box
     * @param Point 3D position of interest
     * @return the distance from the Point to the bounding box.
     */
    inline float ComputeSquaredDistanceFromBoxToPoint(const Vec3& Mins, const Vec3& Maxs, const Vec3& Point)
    {
        // Accumulates the distance as we iterate axis
        float DistSquared = 0.f;
        
        // Check each axis for min/max and add the distance accordingly
        // NOTE: Loop manually unrolled for > 2x speed up
        if (Point.x < Mins.x)
        {
            DistSquared += Math::Square(Point.x - Mins.x);
        }
        else if (Point.x > Maxs.x)
        {
            DistSquared += Math::Square(Point.x - Maxs.x);
        }
        
        if (Point.y < Mins.y)
        {
            DistSquared += Math::Square(Point.y - Mins.y);
        }
        else if (Point.y > Maxs.y)
        {
            DistSquared += Math::Square(Point.y - Maxs.y);
        }
        
        if (Point.z < Mins.z)
        {
            DistSquared += Math::Square(Point.z - Mins.z);
        }
        else if (Point.z > Maxs.z)
        {
            DistSquared += Math::Square(Point.z - Maxs.z);
        }
        
        return DistSquared;
    }
    
    NK_ALLOW_MEMCPY_SERIALIZATION(Vec3);
    
    namespace Math
    {
        NEKO_FORCE_INLINE float GetRangePct(Vec2 const& Range, float Value)
        {
            return (Range.x != Range.y) ? (Value - Range.x) / (Range.y - Range.x) : Range.x;
        }
        
        NEKO_FORCE_INLINE float GetRangeValue(Vec2 const& Range, float Pct)
        {
            return Lerp<float>(Range.x, Range.y, Pct);
        }
        
        /** For the given Value clamped to the [Input:Range] inclusive, returns the corresponding percentage in [Output:Range] Inclusive. */
        static NEKO_FORCE_INLINE float GetMappedRangeValueClamped(const Vec2& InputRange, const Vec2& OutputRange, const float Value)
        {
            // taken from UE
            const float ClampedPct = Clamp<float>(GetRangePct(InputRange, Value), 0.f, 1.f);
            return GetRangeValue(OutputRange, ClampedPct);
        }
        
    }
} // namespace Neko
