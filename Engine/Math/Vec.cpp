//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Vec.cpp
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "Vec.h"
#include "Matrix.h"
#include <cmath>

namespace Neko
{
    Vec2 Vec2::Zero(0.0, 0.0);
    Int2 Int2::Zero(0.0, 0.0);
	
	Vec3 Vec3::Zero(0.0, 0.0, 0.0);
    Vec4 Vec4::Zero(0.0, 0.0, 0.0, 0.0);
    
    Vec3 Vec3::UpVector(0.0, 0.0, 1.0);
    
    float Vec2::LengthSq() const
    {
        float x = this->x;
        float y = this->y;
        return x * x + y * y;
    }
    
    float Vec2::Length() const
    {
        float x = this->x;
        float y = this->y;
        return sqrt(x * x + y * y);
    }
    
    void Vec3::Normalize()
    {
        float x = this->x;
        float y = this->y;
        float z = this->z;
        float inv_len = 1 / sqrt(x * x + y * y + z * z);
        x *= inv_len;
        y *= inv_len;
        z *= inv_len;
        this->x = x;
        this->y = y;
        this->z = z;
    }
    
    Vec3 Vec3::Normalized() const
    {
        float x = this->x;
        float y = this->y;
        float z = this->z;
        float inv_len = 1 / sqrt(x * x + y * y + z * z);
        x *= inv_len;
        y *= inv_len;
        z *= inv_len;
        return Vec3(x, y, z);
    }
    
    float Vec3::Length() const
    {
        float x = this->x;
        float y = this->y;
        float z = this->z;
        return sqrt(x * x + y * y + z * z);
    }
    
    CAngles Vec3::ToOrientationAngles() const
    {
        CAngles Angle;
        
        Angle.yaw = atan2f(y, x) * (180.0f / Math::PI);
        
        Angle.pitch = atan2f(z, sqrtf(x * x + y * y)) * (180.0f / Math::PI);
        
        Angle.roll = 0;
        
        return Angle;
    }
    
    void Vec4::Normalize()
    {
        float x = this->x;
        float y = this->y;
        float z = this->z;
        float w = this->w;
        float inv_len = 1 / sqrt(x * x + y * y + z * z + w * w);
        x *= inv_len;
        y *= inv_len;
        z *= inv_len;
        this->x = x;
        this->y = y;
        this->z = z;
        this->w = w;
    }
    
    float Vec4::Length() const
    {
        float x = this->x;
        float y = this->y;
        float z = this->z;
        float w = this->w;
        return sqrt(x * x + y * y + z * z + w * w);
    }
} // namespace Neko
