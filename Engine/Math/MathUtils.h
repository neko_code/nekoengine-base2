//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  MathUtils.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Platform/SystemShared.h"
#include "../Platform/PlatformMath.h"
#include "../Neko.h"

#define	ANGLE2BYTE(x)			( Neko::Math::TruncToInt( (x) * 256.0f / 360.0f ) & 255 )
#define	BYTE2ANGLE(x)			( (x) * ( 360.0f / 256.0f ) )
#define	ANGLE2SHORT(x)			( Neko::Math::TruncToInt( (x) * 65536.0f / 360.0f ) & 65535 )
#define	SHORT2ANGLE(x)			( (x) * ( 360.0f / 65536.0f ) )

#define IEEE_FLT_SIGN_BIT			31
#define IEEE_FLT_SIGN_MASK			( 1UL << IEEE_FLT_SIGN_BIT )

#define IEEE_FLT_SIGNBITSET( a )	(reinterpret_cast<const unsigned int &>(a) >> IEEE_FLT_SIGN_BIT)

#define SMALL_NUMBER (1.e-8f)
#define KINDA_SMALL_NUMBER	(1.e-4f)

#include <math.h>

namespace Neko
{
    struct Vec3;
    
    namespace Math
    {
        const float PI = 3.14159265f;
        const float HALF_PI = 3.14159265f * 0.5f;
        const float SQRT2 = 1.41421356237f;
        const float SQRT3 = 1.73205080757f;
        
        const static float	M_MS2SEC		= 0.001f;
        
        
        const float Deg2Rad = (PI * 2.0f) / 360.0f;
        const float Rad2Deg = 360.0f / (PI * 2.0f);

        
        NEKO_ENGINE_API NEKO_FORCE_INLINE Byte TruncToByte(float F)
        {
            // @todo SSE
            int i = (int)F;
            if (i < 0)
            {
                return 0;
            }
            else if (i > 255)
            {
                return 255;
            }
            return static_cast<Byte>( i );
        }
        
        NEKO_ENGINE_API bool GetRayPlaneIntersecion(const Vec3& origin, const Vec3& dir, const Vec3& plane_point, const Vec3& normal, float& out);
        NEKO_ENGINE_API bool GetRaySphereIntersection(const Vec3& origin, const Vec3& dir,const Vec3& center, float radius, Vec3& out);
        NEKO_ENGINE_API bool GetRayAABBIntersection(const Vec3& origin, const Vec3& dir, const Vec3& min, const Vec3& size, Vec3& out);
        NEKO_ENGINE_API float getLineSegmentDistance(const Vec3& origin, const Vec3& dir, const Vec3& a, const Vec3& b);
        NEKO_ENGINE_API bool GetRayTriangleIntersection(const Vec3& origin, const Vec3& dir, const Vec3& a, const Vec3& b, const Vec3& c, float* out_t);
        NEKO_ENGINE_API bool GetSphereTriangleIntersection(const Vec3& center, float radius, const Vec3& v0, const Vec3& v1, const Vec3& v2);
        
        
        NEKO_ENGINE_API void SeedRandomGUID(uint32 seed);
        NEKO_ENGINE_API uint64 RandGUID();
        
        template <typename T> NEKO_FORCE_INLINE void swap(T& a, T& b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }
        
        /** Clamps X to be between Min and Max, inclusive */
        template< class T >
        NEKO_FORCE_INLINE T Clamp( const T X, const T Min, const T Max )
        {
            return X < Min ? Min : X < Max ? X : Max;
        }
        
        template <class T>
        NEKO_FORCE_INLINE T Saturate(const T a)
        {
            return Clamp(a, (T)0, (T)1);
        }

        /** Calculates the percentage along a line from MinValue to MaxValue that Value is. */
        static NEKO_FORCE_INLINE float GetRangePct(float MinValue, float MaxValue, float Value)
        {
            return (Value - MinValue) / (MaxValue - MinValue);
        }
        
        template <typename T> NEKO_FORCE_INLINE T Minimum(T a, T b)
        {
            return a < b ? a : b;
        }
        
        template <typename T> NEKO_FORCE_INLINE T Minimum(T a, T b, T c)
        {
            return Minimum(Minimum(a, b), c);
        }
        
        template <typename T> NEKO_FORCE_INLINE T Minimum(T a, T b, T c, T d)
        {
            return Minimum(Minimum(a, b, c), d);
        }
        
        template <typename T> NEKO_FORCE_INLINE T Maximum(T a, T b)
        {
            return a < b ? b : a;
        }
        
        template <typename T> NEKO_FORCE_INLINE T Maximum(T a, T b, T c)
        {
            return Maximum(Maximum(a, b), c);
        }
        
        template <typename T> NEKO_FORCE_INLINE T Maximum(T a, T b, T c, T d)
        {
            return Maximum(Maximum(a, b, c), d);
        }
        
        /** Returns highest of 3 values */
        template< class T > static NEKO_FORCE_INLINE T Max3( const T A, const T B, const T C )
        {
            return Maximum ( Maximum( A, B ), C );
        }
        
        /** Returns lowest of 3 values */
        template< class T > static NEKO_FORCE_INLINE T Min3( const T A, const T B, const T C )
        {
            return Minimum ( Minimum( A, B ), C );
        }
        
        /** Multiples value by itself */
        template< class T > static NEKO_FORCE_INLINE T Square( const T A )
        {
            return A*A;
        }

        NEKO_ENGINE_API int32 TruncToInt(float F);
        
        NEKO_ENGINE_API int32 FloorToInt(float F);
        
        NEKO_ENGINE_API float InvSqrt(float F);
        
		/** Converts a float value to the nearest greatest or equal integer. */
		NEKO_FORCE_INLINE int32 CeilToInt(float F)
		{
			return TruncToInt(F);
		}
		
		NEKO_FORCE_INLINE float CeilToFloat(float F)
		{
			return ceilf(F);
		}
        
        template <typename T> NEKO_FORCE_INLINE T abs(T a)
        {
            return a > 0 ? a : -a;
        }
        
        template <typename T> NEKO_FORCE_INLINE T signum(T a)
        {
            return a > 0 ? (T)1 : (a < 0 ? (T)-1 : 0);
        }
     
        
		/** Performs a linear interpolation between two values, Alpha ranges from 0-1 */
		template< class T, class U >
		static NEKO_FORCE_INLINE T Lerp( const T& A, const T& B, const U& Alpha )
		{
			return (T)(A + Alpha * (B-A));
		}
		
		/** Performs a linear interpolation between two values, Alpha ranges from 0-1. Handles full numeric range of T */
		template< class T >
		static NEKO_FORCE_INLINE T LerpStable( const T& A, const T& B, double Alpha )
		{
			return (T)((A * (1.0 - Alpha)) + (B * Alpha));
		}
		
		/** Performs a linear interpolation between two values, Alpha ranges from 0-1. Handles full numeric range of T */
		template< class T >
		static NEKO_FORCE_INLINE T LerpStable(const T& A, const T& B, float Alpha)
		{
			return (T)((A * (1.0f - Alpha)) + (B * Alpha));
		}
		
        /**
         *	Checks if two floating point numbers are nearly equal.
         *	@param A				First number to compare
         *	@param B				Second number to compare
         *	@param ErrorTolerance	Maximum allowed difference for considering them as 'nearly equal'
         *	@return					true if A and B are nearly equal
         */
        static NEKO_FORCE_INLINE bool IsNearlyEqual(float A, float B, float ErrorTolerance = SMALL_NUMBER)
        {
            return abs<float>( A - B ) <= ErrorTolerance;
        }
        
        /**
         *	Checks if two floating point numbers are nearly equal.
         *	@param A				First number to compare
         *	@param B				Second number to compare
         *	@param ErrorTolerance	Maximum allowed difference for considering them as 'nearly equal'
         *	@return					true if A and B are nearly equal
         */
        static NEKO_FORCE_INLINE bool IsNearlyEqual(double A, double B, double ErrorTolerance = SMALL_NUMBER)
        {
            return abs<double>( A - B ) <= ErrorTolerance;
        }
        
        inline uint32 NextPow2(uint32 v)
        {
            v--;
            v |= v >> 1;
            v |= v >> 2;
            v |= v >> 4;
            v |= v >> 8;
            v |= v >> 16;
            v++;
            return v;
        }
        
        inline uint32 log2(uint32 v)
        {
            uint32 r;
            uint32 shift;
            r = (v > 0xffff) << 4; v >>= r;
            shift = (v > 0xff) << 3; v >>= shift; r |= shift;
            shift = (v > 0xf) << 2; v >>= shift; r |= shift;
            shift = (v > 0x3) << 1; v >>= shift; r |= shift;
            r |= (v >> 1);
            return r;
        }
        
        NEKO_ENGINE_API float ShortToNormalizedFloat(int16 AxisVal);
        
        template <typename T> bool IsPowOfTwo(T n)
        {
            return (n) && !(n & (n - 1));
        }
        
        NEKO_FORCE_INLINE float DegreesToRadians(float angle)
        {
            return angle * PI / 180.0f;
        }
        
        NEKO_ENGINE_API Vec3 DegreesToRadians(const Vec3& v);
        
        NEKO_FORCE_INLINE float RadiansToDegrees(float angle)
        {
            return angle / PI * 180.0f;
        }
        
        NEKO_ENGINE_API Vec3 RadiansToDegrees(const Vec3& v);
        
        NEKO_ENGINE_API float AngleDiff(float a, float b);
        
        inline float EaseInOut(float t)
        {
            float scaled_t = t * 2;
            if (scaled_t < 1)
            {
                return 0.5f * scaled_t * scaled_t;
            }
            --scaled_t;
            return -0.5f * (scaled_t * (scaled_t - 2) - 1);
        }
        
        
        NEKO_FORCE_INLINE signed short ClampShort(int i)
        {
            if (i < -32768)
            {
                return -32768;
            }
            if (i > 32767)
            {
                return 32767;
            }
            return i;
        }

        
        NEKO_FORCE_INLINE signed char ClampChar( int i )
        {
            if (i < -128)
            {
                return -128;
            }
            if (i > 127)
            {
                return 127;
            }
            return i;
        }
        
        // converts float to uint so it can be used in radix sort
        // float float_value = 0;
        // u32 sort_key = floatFlip(*(u32*)&float_value);
        // http://stereopsis.com/radix.html
        NEKO_FORCE_INLINE uint32 FloatFlip(uint32 float_bits_value)
        {
            uint32 mask = -int32(float_bits_value >> 31) | 0x80000000;
            return float_bits_value ^ mask;
        }
        
        NEKO_FORCE_INLINE float AngleNormalize360( float angle )
        {
            if ( ( angle >= 360.0f ) || ( angle < 0.0f ) )
            {
                angle -= floor( angle / 360.0f ) * 360.0f;
            }
            return angle;
        }
        
        NEKO_FORCE_INLINE float AngleNormalize180( float angle )
        {
            angle = AngleNormalize360( angle );
            if ( angle > 180.0f )
            {
                angle -= 360.0f;
            }
            return angle;
        }
        
        NEKO_FORCE_INLINE float AngleDelta( float angle1, float angle2 )
        {
            return AngleNormalize180( angle1 - angle2 );
        }
        

#define IEEE_FLT_MANTISSA_BITS	23
#define IEEE_FLT_EXPONENT_BITS	8
#define IEEE_FLT_EXPONENT_BIAS	127
#define IEEE_FLT_SIGN_BIT		31
        NEKO_FORCE_INLINE int ILog2(float f)
        {
            return ( ( (*reinterpret_cast<int *>(&f)) >> IEEE_FLT_MANTISSA_BITS ) & ( ( 1 << IEEE_FLT_EXPONENT_BITS ) - 1 ) ) - IEEE_FLT_EXPONENT_BIAS;
        }
        
        NEKO_FORCE_INLINE int ILog2(int i)
        {
            return ILog2( (float)i );
        }
        
        NEKO_FORCE_INLINE int BitsForFloat( float f ) {
            return ILog2( f ) + 1;
        }
        
        NEKO_FORCE_INLINE int BitsForInteger( int i )
        {
            return ILog2((float)i) + 1;
        }
        
        NEKO_ENGINE_API float pow(float base, float exponent);
        NEKO_ENGINE_API uint32 rand();
        NEKO_ENGINE_API uint32 rand(uint32 from, uint32 to);
        NEKO_ENGINE_API void SeedRandom(uint32 seed);
        NEKO_ENGINE_API float RandFloat();
        NEKO_ENGINE_API float RandFloat(float from, float to);
        
        
    } // namespace Math
} // namespace Neko
