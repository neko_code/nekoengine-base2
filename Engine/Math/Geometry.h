//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Geometry.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "MathUtils.h"
#include "Matrix.h"
#include "Simd.h"
#include "Vec.h"
#include "../Neko.h"

namespace Neko
{
#if NEKO_WINDOWS_FAMILY
#	undef FAR
#	undef NEAR
#	undef min
#	undef max
#endif
    /// Base plane implementation.
    struct Plane
    {
        Plane()
        {
            
        }
        
        Plane(const Vec3& normal, float d)
        : normal(normal.x, normal.y, normal.z)
        , d(d)
        {
            
        }
        
        explicit Plane(const Vec4& rhs)
        : normal(rhs.x, rhs.y, rhs.z)
        , d(rhs.w)
        {
            
        }
        
        Plane(const Vec3& point, const Vec3& normal)
        : normal(normal.x, normal.y, normal.z)
        , d(-DotProduct(point, normal))
        {
            
        }
        
        void Set(const Vec3& normal, float d)
        {
            this->normal = normal;
            this->d = d;
        }
        
        void Set(const Vec3& normal, const Vec3& point)
        {
            this->normal = normal;
            d = -DotProduct(point, normal);
        }
        
        void Set(const Vec4& rhs)
        {
            normal.x = rhs.x;
            normal.y = rhs.y;
            normal.z = rhs.z;
            d = rhs.w;
        }
        
        NEKO_FORCE_INLINE Vec3 GetNormal() const
        {
            return normal;
        }
        
        NEKO_FORCE_INLINE float getD() const
        {
            return d;
        }
        
        NEKO_FORCE_INLINE float distance(const Vec3& point) const
        {
            return DotProduct(point, normal) + d;
        }
        
        bool GetIntersectionWithLine(const Vec3& line_point,
                                     const Vec3& line_vect,
                                     Vec3& out_intersection) const
        {
            float t2 = DotProduct(normal, line_vect);
            
            if (t2 == 0.f)
            {
                return false;
            }
            
            float t = -(DotProduct(normal, line_point) + d) / t2;
            out_intersection = line_point + (line_vect * t);
            return true;
        }
        
        Vec3 normal;
        float d;
    };
    
    /// Base sphere
    struct Sphere
    {
        Sphere()
        {
            
        }
        
        Sphere(float x, float y, float z, float _radius)
        : position(x, y, z)
        , radius(_radius)
        {
            
        }
        
        Sphere(const Vec3& point, float _radius)
        : position(point)
        , radius(_radius)
        {
            
        }
        
        explicit Sphere(const Vec4& sphere)
        : position(sphere.x, sphere.y, sphere.z)
        , radius(sphere.w)
        {
            
        }
        
        Vec3 position;
        float radius;
    };
    
    struct AABB;
    struct Matrix;
    
    /// Camera frustum implementation.
    NEKO_ALIGN_PREFIX(16) struct NEKO_ENGINE_API Frustum
    {
        Frustum();
        
        void ComputeOrtho(const Vec3& position, const Vec3& direction, const Vec3& up, float width, float height, float near_distance, float far_distance);
        void ComputeOrtho(const Vec3& position,
                          const Vec3& direction,
                          const Vec3& up,
                          float width,
                          float height,
                          float near_distance,
                          float far_distance,
                          const Vec2& viewport_min,
                          const Vec2& viewport_max);
        
        
        void ComputePerspective(const Vec3& position, const Vec3& direction, const Vec3& up, float fov, float ratio, float near_distance, float far_distance);
        void ComputePerspective(const Vec3& position,
                                const Vec3& direction,
                                const Vec3& up,
                                float fov,
                                float ratio,
                                float near_distance,
                                float far_distance,
                                const Vec2& viewport_min,
                                const Vec2& viewport_max);
        
        bool IntersectNearPlane(const Vec3& center, float radius) const
        {
            float x = center.x;
            float y = center.y;
            float z = center.z;
            uint32 i = (uint32)Planes::NEAR;
            float distance = xs[i] * x + ys[i] * y + z * zs[i] + ds[i];
            distance = distance < 0 ? -distance : distance;
            return distance < radius;
        }
        
        bool IntersectAABB(const AABB& aabb) const;
        
        bool IsSphereInside(const Vec3& center, float radius) const;
        
        Sphere ComputeBoundingSphere() const;
        void Transform(const Matrix& mtx);

        enum class Planes : uint32
        {
            NEAR,
            FAR,
            LEFT,
            RIGHT,
            TOP,
            BOTTOM,
            EXTRA0,
            EXTRA1,
            COUNT
        };
        
        NEKO_FORCE_INLINE Vec3 GetNormal(Planes side) const
        {
            return Vec3(xs[(int)side], ys[(int)side], zs[(int)side]);
        }
        
        void SetPlane(Planes side, const Vec3& normal, const Vec3& point);
        void SetPlane(Planes side, const Vec3& normal, float d);
        

        float xs[(int)Planes::COUNT];
        float ys[(int)Planes::COUNT];
        float zs[(int)Planes::COUNT];
        float ds[(int)Planes::COUNT];
        
        Vec3 points[8];
    } NEKO_ALIGN_SUFFIX(16);
    
    /// Axis aligned bounding box
    struct NEKO_ENGINE_API AABB
    {
        AABB()
        {
            
        }
        
        AABB(const Vec3& _min, const Vec3& _max)
        : min(_min)
        , max(_max)
        {
            
        }
        
        void Set(const Vec3& _min, const Vec3& _max)
        {
            min = _min;
            max = _max;
        }
        
        void Merge(const AABB& rhs)
        {
            AddPoint(rhs.min);
            AddPoint(rhs.max);
        }
        
        void AddPoint(const Vec3& point)
        {
            min = minCoords(point, min);
            max = maxCoords(point, max);
        }
    
        bool Overlaps(const AABB& aabb)
        {
            if (min.x > aabb.max.x)
            {
                return false;
            }
            if (min.y > aabb.max.y)
            {
                return false;
            }
            if (min.z > aabb.max.z)
            {
                return false;
            }
            if (aabb.min.x > max.x)
            {
                return false;
            }
            if (aabb.min.y > max.y)
            {
                return false;
            }
            if (aabb.min.z > max.z)
            {
                return false;
            }
            return true;
        }
        
        void Transform(const Matrix& matrix);
        
        void getCorners(const Matrix& matrix, Vec3* points) const;
        
        Vec3 minCoords(const Vec3& a, const Vec3& b)
        {
            return Vec3(Math::Minimum(a.x, b.x), Math::Minimum(a.y, b.y), Math::Minimum(a.z, b.z));
        }
        
        
        Vec3 maxCoords(const Vec3& a, const Vec3& b)
        {
            return Vec3(Math::Maximum(a.x, b.x), Math::Maximum(a.y, b.y), Math::Maximum(a.z, b.z));
        }
        
        Vec3 min;
        Vec3 max;
    };
} // namespace Neko
