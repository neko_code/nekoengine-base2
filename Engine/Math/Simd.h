//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Simd.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//


#pragma once

#include "../Platform/SystemShared.h"


#if NEKO_WINDOWS_FAMILY
#	include <xmmintrin.h>
#else
#	include <cmath>
#endif

#define NEKO_SIMD_FORCE_INLINE NEKO_FORCE_INLINE
#define NEKO_SIMD_INLINE inline

#define NEKO_SIMD_AVX     0
#define NEKO_SIMD_LANGEXT 0     // @todo
#define NEKO_SIMD_NEON    0
#define NEKO_SIMD_SSE     0


#if defined(__AVX__) || defined(__AVX2__)
#	include <immintrin.h>
#	undef  NEKO_SIMD_AVX
#	define NEKO_SIMD_AVX 1
#endif

#if defined(__SSE2__) || (NEKO_VC && (NEKO_64BIT || _M_IX86_FP >= 2) )
#	include <emmintrin.h> // __m128i
#	if defined(__SSE4_1__)
#		include <smmintrin.h>
#	endif
#	include <xmmintrin.h> // __m128
#	undef NEKO_SIMD_SSE
#	define NEKO_SIMD_SSE 1
#elif defined(__ARM_NEON__) && !NEKO_CLANG
#	include <arm_neon.h>
#	undef NEKO_SIMD_NEON
#	define NEKO_SIMD_NEON 1
#elif   NEKO_CLANG \
        && !NEKO_EMSCRIPTEN \
        && !NEKO_IOS \
        &&  NEKO_CLANG_HAS_EXTENSION(attribute_ext_vector_type)
#	include <math.h>
#	undef  NEKO_SIMD_LANGEXT
#	define NEKO_SIMD_LANGEXT 1
#endif
 
namespace Neko
{
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_shuf_xyAB(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_shuf_ABxy(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_shuf_CDzw(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_shuf_zwCD(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_shuf_xAyB(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_shuf_yBxA(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_shuf_zCwD(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_shuf_CzDw(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE float simd_x(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE float simd_y(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE float simd_z(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE float simd_w(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_ld(const void* _ptr);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE void simd_st(void* _ptr, Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE void simd_stx(void* _ptr, Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE void simd_stream(void* _ptr, Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_ld(float _x, float _y, float _z, float _w);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_ld(float _x, float _y, float _z, float _w, float _a, float _b, float _c, float _d);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_ild(uint32_t _x, uint32_t _y, uint32_t _z, uint32_t _w);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_ild(uint32_t _x, uint32_t _y, uint32_t _z, uint32_t _w, uint32_t _a, uint32_t _b, uint32_t _c, uint32_t _d);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_splat(const void* _ptr);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_splat(float _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_isplat(uint32_t _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_zero();
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_itof(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_ftoi(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_round(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_add(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_sub(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_mul(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_div(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE int simd_movemask(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_rcp_est(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_sqrt(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_rsqrt_est(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_dot3(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_dot(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_cmpeq(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_cmplt(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_cmple(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_cmpgt(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_cmpge(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_min(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_max(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_and(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_andc(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_or(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_xor(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_sll(Ty _a, int _count);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_srl(Ty _a, int _count);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_sra(Ty _a, int _count);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_icmpeq(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_icmplt(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_icmpgt(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_imin(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_imax(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_iadd(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_FORCE_INLINE Ty simd_isub(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_shuf_xAzC(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_shuf_yBwD(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_rcp(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_orx(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_orc(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_neg(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_madd(Ty _a, Ty _b, Ty _c);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_nmsub(Ty _a, Ty _b, Ty _c);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_div_nr(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_selb(Ty _mask, Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_sels(Ty _test, Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_not(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_abs(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_clamp(Ty _a, Ty _min, Ty _max);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_lerp(Ty _a, Ty _b, Ty _s);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_rsqrt(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_rsqrt_nr(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_rsqrt_carmack(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_sqrt_nr(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_log2(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_exp2(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_pow(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_cross3(Ty _a, Ty _b);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_normalize3(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_ceil(Ty _a);
    
    template<typename Ty>
    NEKO_SIMD_INLINE Ty simd_floor(Ty _a);
    
#if NEKO_SIMD_AVX
    typedef __m256 simd256_avx_t;
#endif // NEKO_SIMD_SSE
    
#if NEKO_SIMD_LANGEXT
    union simd128_langext_t
    {
        float    __attribute__((vector_size(16))) vf;
        int32_t  __attribute__((vector_size(16))) vi;
        uint32_t __attribute__((vector_size(16))) vu;
        float    fxyzw[4];
        int32_t  ixyzw[4];
        uint32_t uxyzw[4];
        
    };
#endif // NEKO_SIMD_LANGEXT
    
#if NEKO_SIMD_NEON
    typedef float32x4_t simd128_neon_t;
#endif // NEKO_SIMD_NEON
    
#if NEKO_SIMD_SSE
    typedef __m128 simd128_sse_t;
#endif // NEKO_SIMD_SSE
    
} // namespace bx

#if NEKO_SIMD_AVX
#	include "simd256_avx.inl"
#endif // NEKO_SIMD_AVX

#if NEKO_SIMD_LANGEXT
#	include "simd128_langext.inl"
#endif // NEKO_SIMD_LANGEXT

#if NEKO_SIMD_NEON
#	include "simd128_neon.inl"
#endif // NEKO_SIMD_NEON

#if NEKO_SIMD_SSE
#	include "simd128_sse.inl"
#endif // NEKO_SIMD_SSE

namespace Neko
{
    union simd128_ref_t
    {
        float    fxyzw[4];
        int32_t  ixyzw[4];
        uint32_t uxyzw[4];
    };
    
#ifndef SIMD_WARN_REFERENCE_IMPL
#	define SIMD_WARN_REFERENCE_IMPL 0
#endif
    
#if !(NEKO_SIMD_LANGEXT || NEKO_SIMD_NEON || NEKO_SIMD_SSE)
#	if SIMD_WARN_REFERENCE_IMPL
#		pragma message("*** Using SIMD128 reference implementation! ***")
#	endif
    
    typedef simd128_ref_t simd128_t;
#endif
    
    struct simd256_ref_t
    {
        simd128_t simd128_0;
        simd128_t simd128_1;
    };
    
#if !NEKO_SIMD_AVX
#	if SIMD_WARN_REFERENCE_IMPL
#		pragma message("*** Using SIMD256 reference implementation! ***")
#	endif
    
    typedef simd256_ref_t simd256_t;
#endif // !NEKO_SIMD_AVX
    
} // namespace bx

#include "simd128_fpu.inl"
#include "simd256_fpu.inl"

namespace Neko
{
    NEKO_SIMD_FORCE_INLINE simd128_t simd_zero()
    {
        return simd_zero<simd128_t>();
    }
    
    NEKO_SIMD_FORCE_INLINE simd128_t simd_ld(const void* _ptr)
    {
        return simd_ld<simd128_t>(_ptr);
    }
    
    NEKO_SIMD_FORCE_INLINE simd128_t simd_ld(float _x, float _y, float _z, float _w)
    {
        return simd_ld<simd128_t>(_x, _y, _z, _w);
    }
    
    NEKO_SIMD_FORCE_INLINE simd128_t simd_ild(uint32_t _x, uint32_t _y, uint32_t _z, uint32_t _w)
    {
        return simd_ild<simd128_t>(_x, _y, _z, _w);
    }
    
    NEKO_SIMD_FORCE_INLINE simd128_t simd_splat(const void* _ptr)
    {
        return simd_splat<simd128_t>(_ptr);
    }
    
    NEKO_SIMD_FORCE_INLINE simd128_t simd_splat(float _a)
    {
        return simd_splat<simd128_t>(_a);
    }
    
    NEKO_SIMD_FORCE_INLINE simd128_t simd_isplat(uint32_t _a)
    {
        return simd_isplat<simd128_t>(_a);
    }

    namespace Math
    {
    };
} // namespace Neko
