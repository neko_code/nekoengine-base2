//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Utilities.cpp
//  Neko engine
//
//  Copyright (c) 2014 Neko Code. All rights reserved.
//

#include "Utilities.h"
#include "../Math/MathUtils.h"
#include "../Platform/Platform.h"

namespace Neko
{
    namespace Util
    {
        void PrettyTime(double Seconds, Neko::StaticString<16>& Out)
        {
            if (Seconds < 1.0)
            {
                Out << Neko::Math::TruncToInt(Seconds * 1000) << " ms";
            }
            else if (Seconds < 10.0)
            {
                int32 Sec = Neko::Math::TruncToInt(Seconds);
                int32 Ms = Neko::Math::TruncToInt(Seconds * 1000) - Sec * 1000;
                Out << Sec << "." << Ms / 10 << " sec";
            }
            else if (Seconds < 60.0)
            {
                int32 Sec = Neko::Math::TruncToInt(Seconds);
                int32 Ms = Neko::Math::TruncToInt(Seconds * 1000) - Sec * 1000;
                Out << Sec << "." << Ms / 100 << " sec";
            }
            else if (Seconds < 60.0 * 60.0)
            {
                int32 Min = Neko::Math::TruncToInt(Seconds / 60.0);
                int32 Sec = Neko::Math::TruncToInt(Seconds) - Min * 60;
                Out << Min << ":" << Sec << " min";
            }
            else
            {
                int32 Hr = Neko::Math::TruncToInt(Seconds / 3600.0);
                int32 Min = Neko::Math::TruncToInt((Seconds - Hr * 3600) / 60.0);
                int32 Sec = Neko::Math::TruncToInt(Seconds - Hr * 3600 - Min * 60);
                Out << Hr << ":" << Min << ":" << Sec << " hours";
            }
        }
        
        char* CurrentDateToString(char* Dest, size_t DestSize)
        {
            int32 Year;
            int32 Month;
            int32 DayOfWeek;
            int32 Day;
            int32 Hour;
            int32 Min;
            int32 Sec;
            int32 MSec;
            
            Platform::GetSystemTime(Year, Month, DayOfWeek, Day, Hour, Min, Sec, MSec);
            snprintf(Dest, DestSize, "%02d/%02d/%02d", Month, Day, Year % 100); // @todo
            return Dest;
        }
        
        char* CurrentTimeToString(char* Dest, size_t DestSize)
        {
            int32 Year;
            int32 Month;
            int32 DayOfWeek;
            int32 Day;
            int32 Hour;
            int32 Min;
            int32 Sec;
            int32 MSec;
            
            Platform::GetSystemTime(Year, Month, DayOfWeek, Day, Hour, Min, Sec, MSec);
            snprintf(Dest, DestSize, "%02d:%02d:%02d", Hour, Min, Sec); // @todo
            return Dest;
        }
        
        char* UCS4ToUTF8(uint32 ch, char* NEKO_RESTRICT dst)
        {
            uint8* p = (uint8*)dst;
            if (ch <= 0x7F)
            {
                *p = (uint8)ch;
                ++dst;
            }
            else if (ch <= 0x7FF)
            {
                p[0] = 0xC0 | (uint8)((ch >> 6) & 0x1F);
                p[1] = 0x80 | (uint8)(ch & 0x3F);
                dst += 2;
            }
            else if (ch <= 0xFFFF)
            {
                p[0] = 0xE0 | (uint8)((ch >> 12) & 0x0F);
                p[1] = 0x80 | (uint8)((ch >> 6) & 0x3F);
                p[2] = 0x80 | (uint8)(ch & 0x3F);
                dst += 3;
            }
            else if (ch <= 0x1FFFFF)
            {
                p[0] = 0xF0 | (uint8)((ch >> 18) & 0x07);
                p[1] = 0x80 | (uint8)((ch >> 12) & 0x3F);
                p[2] = 0x80 | (uint8)((ch >> 6) & 0x3F);
                p[3] = 0x80 | (uint8)(ch & 0x3F);
                dst += 4;
            }
            else if (ch <= 0x3FFFFFF)
            {
                p[0] = 0xF8 | (uint8)((ch >> 24) & 0x03);
                p[1] = 0x80 | (uint8)((ch >> 18) & 0x3F);
                p[2] = 0x80 | (uint8)((ch >> 12) & 0x3F);
                p[3] = 0x80 | (uint8)((ch >> 6) & 0x3F);
                p[4] = 0x80 | (uint8)(ch & 0x3F);
                dst += 5;
            }
            else
            {
                p[0] = 0xFC | (uint8)((ch >> 30) & 0x01);
                p[1] = 0x80 | (uint8)((ch >> 24) & 0x3F);
                p[2] = 0x80 | (uint8)((ch >> 18) & 0x3F);
                p[3] = 0x80 | (uint8)((ch >> 12) & 0x3F);
                p[4] = 0x80 | (uint8)((ch >> 6) & 0x3F);
                p[5] = 0x80 | (uint8)(ch & 0x3F);
                dst += 6;
            }
            return dst;
        }
    }
}

