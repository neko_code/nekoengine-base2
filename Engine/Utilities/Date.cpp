//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Date.cpp
//  Neko engine
//
//  Copyright (c) 2014 Neko Code. All rights reserved.
//

#include "Date.h"
#include "../Platform/Platform.h"
#include "../Math/MathUtils.h"

namespace Neko
{
    const int32 DaysPerMonth[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    const int32 DaysToMonth[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
    
    CDateTime::CDateTime( int32 Year, int32 Month, int32 Day, int32 Hour, int32 Minute, int32 Second, int32 Millisecond )
    {
//        check(Validate(Year, Month, Day, Hour, Minute, Second, Millisecond));
        
        int32 TotalDays = 0;
        
        if ((Month > 2) && IsLeapYear(Year))
        {
            ++TotalDays;
        }
        
        Year--;											// the current year is not a full year yet
        Month--;										// the current month is not a full month yet
        
        TotalDays += Year * 365;
        TotalDays += Year / 4;							// leap year day every four years...
        TotalDays -= Year / 100;						// ...except every 100 years...
        TotalDays += Year / 400;						// ...but also every 400 years
        TotalDays += DaysToMonth[Month];				// days in this year up to last month
        TotalDays += Day - 1;							// days in this month minus today
        
        Ticks = TotalDays * ETimespan::TicksPerDay
        + Hour * ETimespan::TicksPerHour
        + Minute * ETimespan::TicksPerMinute
        + Second * ETimespan::TicksPerSecond
        + Millisecond * ETimespan::TicksPerMillisecond;
    }

    
    CDateTime CDateTime::UtcNow()
    {
        int32 Year, Month, Day, DayOfWeek;
        int32 Hour, Minute, Second, Millisecond;
        
        Neko::Platform::UtcTime(Year, Month, DayOfWeek, Day, Hour, Minute, Second, Millisecond);
        
        return CDateTime(Year, Month, Day, Hour, Minute, Second, Millisecond);
    }
    
    
    bool CDateTime::IsLeapYear(int32 Year)
    {
        if ((Year % 4) == 0)
        {
            return (((Year % 100) != 0) || ((Year % 400) == 0));
        }
        
        return false;
    }
    
    
    Text CDateTime::ToString(const char* Format)
    {
        Text Result;
        
        if (Format)
        {
            while (*Format != '\0')
            {
                if ((*Format == '%') && (*(++Format) != '\0'))
                {
                    switch (*Format)
                    {
                        case 'd': Result.Append(Neko::va("%02i", GetDay())); break;
                        case 'D': Result.Append(Neko::va("%03i", GetDayOfYear())); break;
                        case 'm': Result.Append(Neko::va("%02i", GetMonth())); break;
                        case 'y': Result.Append(Neko::va("%02i", GetYear() % 100)); break;
                        case 'Y': Result.Append(Neko::va("%04i", GetYear())); break;
                        case 'h': Result.Append(Neko::va("%02i", GetHour12())); break;
                        case 'H': Result.Append(Neko::va("%02i", GetHour())); break;
                        case 'M': Result.Append(Neko::va("%02i", GetMinute())); break;
                        case 'S': Result.Append(Neko::va("%02i", GetSecond())); break;
                        case 's': Result.Append(Neko::va("%02i", GetMillisecond())); break;
                        default: Result.Append(Neko::va("%c", *Format)); break;
                    }
                }
                else
                {
                    Result.Append(Neko::va("%c", *Format));
                }
                
                Format++;
            }
        }
        
        return Result;
    }
    
    
    void CDateTime::GetDate(int32& OutYear, int32& OutMonth, int32& OutDay) const
    {
        // Based on FORTRAN code in:
        // Fliegel, H. F. and van Flandern, T. C.,
        // Communications of the ACM, Vol. 11, No. 10 (October 1968).
        
        int32 i, j, k, l, n;
        
        l = Math::FloorToInt(GetJulianDay() + 0.5) + 68569;
        n = 4 * l / 146097;
        l = l - (146097 * n + 3) / 4;
        i = 4000 * (l + 1) / 1461001;
        l = l - 1461 * i / 4 + 31;
        j = 80 * l / 2447;
        k = l - 2447 * j / 80;
        l = j / 11;
        j = j + 2 - 12 * l;
        i = 100 * (n - 49) + i + l;
        
        OutYear = i;
        OutMonth = j;
        OutDay = k;
    }
    
    
    int32 CDateTime::GetDay() const
    {
        int32 Year, Month, Day;
        GetDate(Year, Month, Day);
        
        return Day;
    }
    
    
    int32 CDateTime::GetMonth() const
    {
        int32 Year, Month, Day;
        GetDate(Year, Month, Day);
        
        return Month;
    }
    
    
    int32 CDateTime::GetYear() const
    {
        int32 Year, Month, Day;
        GetDate(Year, Month, Day);
        
        return Year;
    }
    
    
    int32 CDateTime::GetHour12() const
    {
        int32 Hour = GetHour();
        
        if (Hour < 1)
        {
            return 12;
        }
        
        if (Hour > 12)
        {
            return (Hour - 12);
        }
        
        return Hour;
    }
    
    
    int32 CDateTime::DaysInMonth(int32 Year, int32 Month)
    {
        assert((Month >= 1) && (Month <= 12));
        
        if ((Month == 2) && IsLeapYear(Year))
        {
            return 29;
        }
        
        return DaysPerMonth[Month];
    }
    
    
    int32 CDateTime::DaysInYear( int32 Year )
    {
        if (IsLeapYear(Year))
        {
            return 366;
        }
        
        return 365;
    }
    
    
    int32 CDateTime::GetDayOfYear() const
    {
        int32 Year, Month, Day;
        GetDate(Year, Month, Day);
        
        for (int32 CurrentMonth = 1; CurrentMonth < Month; ++CurrentMonth)
        {
            Day += DaysInMonth(Year, CurrentMonth);
        }
        
        return Day;
    }

}
