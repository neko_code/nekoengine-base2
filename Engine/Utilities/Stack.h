//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Stack.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

#ifndef CORO_STACKALLOC
#   define CORO_STACKALLOC 1
#endif

namespace Neko
{
    namespace Stack
    {
        extern "C" {
#if CORO_STACKALLOC
            struct CoroutineStack
            {
                void *sptr;
                size_t ssze;
            };
            
            /*
             * Try to allocate a stack of at least the given size and return true if
             * successful, or false otherwise.
             *
             * The size is *NOT* specified in bytes, but in units of sizeof (void *),
             * i.e. the stack is typically 4(8) times larger on 32 bit(64 bit) platforms
             * then the size passed in.
             *
             * If size is 0, then a "suitable" stack size is chosen (usually 1-2MB).
             */
            int StackAlloc(struct CoroutineStack *stack, unsigned int size);
            
            /*
             * Free the stack allocated by StackAlloc again. It is safe to
             * call this function on the CoroutineStack structure even if StackAlloc
             * failed.
             */
            void StackFree(struct CoroutineStack *stack);
#endif
        }
        
        
# if !defined(STACK_ADJUST_PTR)
#   define STACK_ADJUST_PTR(sp,ss) (sp)
#   define STACK_ADJUST_SIZE(sp,ss) (ss)
# endif

    } // namespace Stack
    
} // namespace Neko
