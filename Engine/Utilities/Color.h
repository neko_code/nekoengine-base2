//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_\     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Color.h
//  Neko engine
//
//  Copyright © 2016 Neko Vision. All rights reserved.
//

#pragma once

#include "../Math/MathUtils.h"
#include "../Math/Vec.h"

namespace Neko
{
    // Helpers macros to generate 32-bits encoded colors
#ifdef USE_BGRA_PACKED_COLOR
#define COL32_R_SHIFT    16
#define COL32_G_SHIFT    8
#define COL32_B_SHIFT    0
#define COL32_A_SHIFT    24
#define COL32_A_MASK     0xFF000000
#else
#define COL32_R_SHIFT    0
#define COL32_G_SHIFT    8
#define COL32_B_SHIFT    16
#define COL32_A_SHIFT    24
#define COL32_A_MASK     0xFF000000
#endif

#define COL_F32_TO_INT8_UNBOUND(_VAL)    ((int)((_VAL) * 255.0f + ((_VAL)>=0 ? 0.5f : -0.5f)))   // Unsaturated, for display purpose
#define COL_F32_TO_INT8_SAT(_VAL)        ((int)(Math::Saturate(_VAL) * 255.0f + 0.5f))               // Saturated, always output 0..255

    /// Color utility with helpers such as Color to Uint32, Color to Vec4 and vice-versa. @note Don't store color directly using this struct, store either Uint32 or Vec4.
	struct NEKO_ENGINE_API SColor
	{
		Vec4 Value;

		SColor()
		{
            Value = Vec4::Zero;
		}
        
		SColor(int32 r, int32 g, int32 b, int32 a = 255)
		{
			float sc = 1.0f / 255.0f;
			Value.x = (float)r * sc;
			Value.y = (float)g * sc;
			Value.z = (float)b * sc;
			Value.w = (float)a * sc;
		}
        
		SColor(uint32 rgba)
		{
			float sc = 1.0f / 255.0f;
			Value.x = (float)(rgba & 0xFF) * sc;
			Value.y = (float)((rgba >> 8) & 0xFF) * sc;
			Value.z = (float)((rgba >> 16) & 0xFF) * sc;
			Value.w = (float)(rgba >> 24) * sc;
		}
        
		SColor(float r, float g, float b, float a = 1.0f)
		{
			Value.x = r; Value.y = g;
			Value.z = b; Value.w = a;
		}
        
		SColor(const Vec4& col)
		{
			Value = col;
		}
        
		NEKO_FORCE_INLINE operator uint32() const { return ColorConvertFloat4ToU32(Value); }
    
		NEKO_FORCE_INLINE operator Vec4() const { return Value; }

		NEKO_FORCE_INLINE void SetHSV(float h, float s, float v, float a = 1.0f)
		{
			ColorConvertHSVtoRGB(h, s, v, Value.x, Value.y, Value.z); Value.w = a;
		}

		static SColor HSV(float h, float s, float v, float a = 1.0f)
		{
			float r, g, b;
			ColorConvertHSVtoRGB(h, s, v, r, g, b);
			return SColor(r, g, b, a);
		}

		static Vec4 ColorConvertU32ToFloat4(uint32 in)
		{
			float s = 1.0f / 255.0f;
			return Vec4((in & 0xFF) * s, ((in >> 8) & 0xFF) * s, ((in >> 16) & 0xFF) * s, (in >> 24) * s);
		}

		static uint32 ColorConvertFloat4ToU32(const Vec4& in)
		{
			uint32 out;
			out = ((uint32)COL_F32_TO_INT8_SAT(in.x));
			out |= ((uint32)COL_F32_TO_INT8_SAT(in.y)) << 8;
			out |= ((uint32)COL_F32_TO_INT8_SAT(in.z)) << 16;
			out |= ((uint32)COL_F32_TO_INT8_SAT(in.w)) << 24;
			return out;
		}

		// Convert rgb floats ([0-1],[0-1],[0-1]) to hsv floats ([0-1],[0-1],[0-1]), from Foley & van Dam p592
		// Optimized http://lolengine.net/blog/2013/01/13/fast-rgb-to-hsv
		static void ColorConvertRGBtoHSV(float r, float g, float b, float& out_h, float& out_s, float& out_v)
		{
			float K = 0.f;
			if (g < b)
			{
				const float tmp = g; g = b; b = tmp;
				K = -1.f;
			}
			if (r < g)
			{
				const float tmp = r; r = g; g = tmp;
				K = -2.f / 6.f - K;
			}

			const float chroma = r - (g < b ? g : b);
			out_h = fabsf(K + (g - b) / (6.f * chroma + 1e-20f));
			out_s = chroma / (r + 1e-20f);
			out_v = r;
		}

		// Convert hsv floats ([0-1],[0-1],[0-1]) to rgb floats ([0-1],[0-1],[0-1]), from Foley & van Dam p593
		// also http://en.wikipedia.org/wiki/HSL_and_HSV
		static void ColorConvertHSVtoRGB(float h, float s, float v, float& out_r, float& out_g, float& out_b)
		{
			if (s == 0.0f)
			{
				// gray
				out_r = out_g = out_b = v;
				return;
			}

			h = fmodf(h, 1.0f) / (60.0f / 360.0f);
			int   i = (int)h;
			float f = h - (float)i;
            float p = v * (1.0f - s);
            float q = v * (1.0f - s * f);
            float t = v * (1.0f - s * (1.0f - f));
            
            switch (i)
            {
                case 0: out_r = v; out_g = t; out_b = p; break;
                case 1: out_r = q; out_g = v; out_b = p; break;
                case 2: out_r = p; out_g = v; out_b = t; break;
                case 3: out_r = p; out_g = q; out_b = v; break;
                case 4: out_r = t; out_g = p; out_b = v; break;
                case 5: default: out_r = v; out_g = p; out_b = q; break;
            }
		}
	};
}
