//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  StringUtil.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Data/DefaultAllocator.h"
#include "../Platform/SystemShared.h"
#include <stdarg.h>
#include <stdio.h>

namespace Neko
{
    /** Searches for a substring in the given string, not case insensitive. */
    NEKO_ENGINE_API const char* FindISubstring(const char* Haystack, const char* Needle);
    /** Searches for a substring in the given string, case insensitive. */
    NEKO_ENGINE_API const char* FindSubstring(const char* str, const char* substr);
    
    // Conversion tools
    
    NEKO_ENGINE_API bool ToCStringHex(uint8 value, char* output, int length);
    NEKO_ENGINE_API bool ToCStringPretty(int32 value, char* output, int length);
    NEKO_ENGINE_API bool ToCStringPretty(uint32 value, char* output, int length);
    NEKO_ENGINE_API bool ToCStringPretty(uint64 value, char* output, int length);
    NEKO_ENGINE_API bool ToCString(int32 value, char* output, int length);
    NEKO_ENGINE_API bool ToCString(int64 value, char* output, int length);
    NEKO_ENGINE_API bool ToCString(uint64 value, char* output, int length);
    NEKO_ENGINE_API bool ToCString(uint32 value, char* output, int length);
    NEKO_ENGINE_API bool ToCString(float value, char* output, int length, int after_point);
    
    NEKO_ENGINE_API const char* FromCString(const char* input, int length, uint16* value);
    NEKO_ENGINE_API const char* FromCString(const char* input, int length, int32* value);
    NEKO_ENGINE_API const char* FromCString(const char* input, int length, uint64* value);
    NEKO_ENGINE_API const char* FromCString(const char* input, int length, int64* value);
    NEKO_ENGINE_API const char* FromCString(const char* input, int length, uint32* value);
    
    NEKO_ENGINE_API const char* ReverseFind(const char* begin_haystack, const char* end_haystack, char c);
    
    NEKO_ENGINE_API bool CopyString(char* destination, int length, const char* source);
    NEKO_ENGINE_API bool CopyNString(char* destination, int length, const char* source, int source_len);
    
    NEKO_ENGINE_API bool CopyStringClamp(char* const Destination, size_t const DestSize, const char* const Src, size_t const SrcSize = (size_t)-1);
    
    NEKO_ENGINE_API bool CatString(char* destination, int length, const char* source);
    NEKO_ENGINE_API bool CatNString(char* destination, int length, const char* source, int source_len);
    NEKO_ENGINE_API bool MakeLowercase(char* destination, int length, const char* source);
    NEKO_ENGINE_API char* Trimmed(char* str);
    
    NEKO_ENGINE_API int StringLength(const char* str);
    NEKO_ENGINE_API bool EqualStrings(const char* lhs, const char* rhs);
    NEKO_ENGINE_API bool EqualIStrings(const char* lhs, const char* rhs);
    NEKO_ENGINE_API int CompareString(const char* lhs, const char* rhs);
    NEKO_ENGINE_API int CompareIString(const char* lhs, const char* rhs);
    NEKO_ENGINE_API int CompareStringN(const char* lhs, const char* rhs, int length);
    NEKO_ENGINE_API int CompareIStringN(const char* lhs, const char* rhs, int length);
    
    NEKO_ENGINE_API int32 CountChar(const char* String, const char Symbol);
    
    NEKO_ENGINE_API int CompareMemory(const void* First, const void* Other, size_t Size);

    NEKO_ENGINE_API void CopyMemory(void* Destination, const void* Source, size_t Count);
    NEKO_ENGINE_API void MoveMemory(void* Destination, const void* Source, size_t Count);
    NEKO_ENGINE_API void SetMemory(void* ptr, uint8 Value, size_t Count);
    
    // @todo ESearchCase flag for both
    NEKO_ENGINE_API bool StartsWith(const char* String, const char* Prefix);
    NEKO_ENGINE_API bool EndsWith(const char* String, const char* Substr);
    
    /** Picks the best measuring size unit (KB, MB, GB, etc) for the given value. */
    NEKO_ENGINE_API int BestUnit(char* Out, float Value);
    
    NEKO_ENGINE_API Byte Nibble(char c);
    
    /** Tokenizer. Tokenizes one string piece into multiple string pieces. */
    struct Tokenizer
    {
        Tokenizer()
        {
        }
        
        /** Returns TRUE if tokenized parameter at index equals to requested string. */
        NEKO_FORCE_INLINE bool IsEqualTo(const int32 Index, const char* Text)
        {
            return Neko::EqualStrings(Text, lastCommandArguments[Index]);
        }
        
        /** Returns tokenized parameter at index. */
        NEKO_FORCE_INLINE const char* GetParamAtIndex(int32 Index)
        {
            return lastCommandArguments[Index];
        }
        
        /** Tokenizes given string. */
        NEKO_FORCE_INLINE int32 TokenizeString(const char *buffer, const char delimiter = ' ')
        {
            const char* text;
            char* textOut;
            int32 argumentCount;
            
            argumentCount = 0;
            
            text = buffer;
            textOut = tokenizedCommand;
            
            while (true)
            {
                // Find the argument before spacing.
                while (true)
                {
                    while (*text /* end of the text */ && *text <= delimiter /* delimiter */)
                    {
                        ++text; // Character position.
                    }
                    
                    break;
                }
                
                // Quote argument parsing.
                if (*text == '"')
                {
                    lastCommandArguments[argumentCount] = textOut;
                    
                    ++argumentCount;
                    ++text;
                    
                    while (*text && *text != '"' /* final quote symbol */)
                    {
                        *textOut++ = *text++;
                    }
                    
                    *textOut++ = '\0';  // Terminate string.
                    
                    if (!*text)
                    {
                        return argumentCount;
                    }
                    
                    ++text;
                    
                    continue;
                }
                
                lastCommandArguments[argumentCount] = textOut;
                ++argumentCount;
                
                while (*text > delimiter)
                {
                    if (text[0] == '"')
                    {
                        break;
                    }
                    
                    // Last tokenized string.
                    *textOut++ = *text++;
                }
                
                *textOut++ = '\0';
                
                if (!*text)
                {
                    return argumentCount;
                }
            }
            
            return argumentCount;
        }
        
        //! Tokenizer parameters.
        char* lastCommandArguments[32];
        char tokenizedCommand[8092];
    };

    /** varargs printf into a temp buffers, not threadsafe */
    NEKO_FORCE_INLINE char* va(const char* fmt, ...)
    {
        va_list argptr;
        static int index = 0;
        static char string[4][16384];	// in case called by nested functions
        char *buf;
        
        buf = string[index];
        index = (index + 1) & 3;
        
        va_start( argptr, fmt );
        vsprintf( buf, fmt, argptr );
        va_end( argptr );
        
        return buf;
    }
    
    NEKO_FORCE_INLINE char ToLower(char c)
    {
        if (c <= 'Z' && c >= 'A')
        {
            return (c + ( 'a' - 'A' ));
        }
        return c;
    }
    
    NEKO_FORCE_INLINE char ToUpper(char c)
    {
        if (c >= 'a' && c <= 'z')
        {
            return (c - ( 'a' - 'A' ));
        }
        return c;
    }
    
    /** String hashing functions. */
    
    NEKO_FORCE_INLINE int Hash(const char* String)
    {
        int i, hash = 0;
        for (i = 0; *String != '\0'; i++)
        {
            hash += ( *String++ ) * ( i + 119 );
        }
        return hash;
    }
    
    NEKO_FORCE_INLINE int Hash(const char* string, int length)
    {
        int i, hash = 0;
        for (i = 0; i < length; i++)
        {
            hash += ( *string++ ) * ( i + 119 );
        }
        return hash;
    }
    
    NEKO_FORCE_INLINE int IHash(const char* string)
    {
        int i, hash = 0;
        for (i = 0; *string != '\0'; i++)
        {
            hash += ToLower( *string++ ) * ( i + 119 );
        }
        return hash;
    }
    
    NEKO_FORCE_INLINE int IHash(const char* string, int length)
    {
        int i, hash = 0;
        for (i = 0; i < length; i++)
        {
            hash += ToLower( *string++ ) * ( i + 119 );
        }
        return hash;
    }
    
    NEKO_FORCE_INLINE bool IsLetter(char c)
    {
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
    }
    
    NEKO_FORCE_INLINE bool IsUpperCase(char c)
    {
        return c >= 'A' && c <= 'Z';
    }

    NEKO_FORCE_INLINE bool IsNumeric(char c)
    {
        return c >= '0' && c <= '9';
    }

    
    template <int32 SIZE> bool CopyString(char(&destination)[SIZE], const char* source)
    {
        return CopyString(destination, SIZE, source);
    }
    
    template <int32 SIZE> bool CatString(char(&destination)[SIZE], const char* source)
    {
        return CatString(destination, SIZE, source);
    }
    
    const char* FloatArrayToString(const float* InArray, const int32 Length, const int Precision);
    
#define    MAX_STRING_CHARS        1024 // Max length of a static string.
    
    /** Statically stored string with the fixed size. */
    template <int32 size> struct StaticString
    {
        static_assert(size <= MAX_STRING_CHARS, "StaticString maximum size exceeded");
        
        StaticString()
        {
            Clear();
        }
        
        explicit StaticString(const char* String)
        {
            Neko::CopyString(data, size, String);
        }
        
        template <typename... Args> StaticString(const char* String, Args... args)
        {
            Neko::CopyString(data, size, String);
            int tmp[] = { (Add(args), 0)... };
            (void)tmp;
        }
        
        template <int value_size> StaticString& operator<<(StaticString<value_size>& Value)
        {
            Add(Value);
            return *this;
        }
        
        template <typename T> StaticString& operator <<(T value)
        {
            Add(value);
            return *this;
        }
        
        template <int32 value_size> void Add(StaticString<value_size>& value)
        {
            Neko::CatString(data, size, value.data);
        }
        void Add(const char* value)
        {
            Neko::CatString(data, size, value);
        }
        void Add(char* value)
        {
            Neko::CatString(data, size, value);
        }
        
        void operator =(const char* String)
        {
            CopyString(data, String);
        }
        
        const char* operator * () const
        {
            return data;
        }
        
        void Add(float value)
        {
            int len = Neko::StringLength(data);
            Neko::ToCString(value, data + len, size - len, 3);
        }
        
        template <typename T> void Add(T value)
        {
            int len = Neko::StringLength(data);
            Neko::ToCString(value, data + len, size - len);
        }
        
        void Set(const char* p, size_t n)
        {
            int Count = 0;
            if (p == 0 || n <= 0)
            {
                Count = 0;
            }
            else
            {
                Count = (n > size) ? size : (int32)n;
                // memmove() is used because p may point to Buffer
                memmove(data, p, Count * sizeof(*p));
            }
            data[Count] = 0;
        }
        
        void Set(const char* p)
        {
            if (p && p[0])
            {
                Set(p, Neko::StringLength(p));
            }
            else
            {
                Clear();
            }
        }
        
        void Clear()
        {
            data[0] = '\0';
        }
		
		NEKO_FORCE_INLINE bool IsEmpty() const
		{
			return data[0] == '\0';
		}
        
        bool operator < (const char* str) const
        {
            return Neko::CompareString(data, str) < 0;
        }
        
        bool operator == (const char* str) const
        {
            return Neko::EqualStrings(data, str);
        }
        
        StaticString<size> operator +(const char* rhs)
        {
            return StaticString<size>(*this, rhs);
        
        }
        
        NEKO_FORCE_INLINE operator const char*() const
        {
            return data;
        }
        
        char data[size + 1]; // trailing zero
    };    
} // !namespace Neko
