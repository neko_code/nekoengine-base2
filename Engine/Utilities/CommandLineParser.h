//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  CommandLineParser.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    // Replace this by Tokenizer??
    
    class CCommandLineParser
    {
    public:
        
        CCommandLineParser(const char* InCommandLine)
        : CommandLine(InCommandLine)
        , Current(nullptr)
        {
            assert(CommandLine != nullptr);
        }
        
        bool Next()
        {
            if (!Current)
            {
                Current = CommandLine;
                SkipWhitespaces();
                return *Current != 0;
            }
            
            while (*Current && !IsWhitespace(*Current))
            {
                if (*Current == '"')
                {
                    SkipString();
                }
                else
                {
                    ++Current;
                }
            }
            SkipWhitespaces();
            return *Current != 0;
        }
        
        void GetCurrent(char* output, int MaxSize)
        {
            assert(*Current);
            assert(MaxSize > 0);
            const char* rhs = Current;
            char* end = output + MaxSize - 1;
            char* lhs = output;
            if (*Current == '"')
            {
                ++rhs;
                while (*rhs && *rhs != '"' && lhs != end)
                {
                    *lhs = *rhs;
                    ++lhs;
                    ++rhs;
                }
                *lhs = 0;
                return;
            }
            
            while (*rhs && !IsWhitespace(*rhs) && lhs != end)
            {
                *lhs = *rhs;
                ++lhs;
                ++rhs;
            }
            *lhs = 0;
        }
        
        bool CurrentEquals(const char* value)
        {
            assert(*Current);
            
            const char* lhs = Current;
            const char* rhs = value;
            while (*lhs && *rhs && *lhs == *rhs)
            {
                ++lhs;
                ++rhs;
            }
            
            return *rhs == 0 && (*lhs == 0 || IsWhitespace(*lhs));
        }
        
    private:
        
        static bool IsWhitespace(char c) { return c == ' ' || c == '\n' || c == '\r' || c == '\t'; }
        
        void SkipWhitespaces()
        {
            while (*Current && IsWhitespace(*Current))
            {
                ++Current;
            }
        }
        
        void SkipString()
        {
            assert(*Current == '"');
            ++Current;
            while (*Current && *Current != '"')
            {
                ++Current;
            }
            
            if (*Current)
            {
                ++Current;
            }
        }
        
    private:
        
        const char* Current;
        const char* CommandLine;
    };
} // namespace Neko
