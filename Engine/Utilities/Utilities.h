//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_\     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Utilities.h
//  Neko engine
//
//  Copyright (c) 2014 Neko Code. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Platform/SystemShared.h"
#include "StringUtil.h"

namespace Neko
{
    // Miscellaneous utils, these are not platform-depend in any ways
    namespace Util
    {
        NEKO_ENGINE_API void PrettyTime(double Seconds, StaticString<16>& Out);
        
        NEKO_ENGINE_API char* CurrentDateToString(char* Dest, size_t DestSize);
        NEKO_ENGINE_API char* CurrentTimeToString(char* Dest, size_t DestSize);
        
        NEKO_ENGINE_API char* UCS4ToUTF8(uint32 Char, char* NEKO_RESTRICT Destination);
    }
}

