//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Timer.cpp
//  Neko engine
//
//  Copyright (c) 2014 Neko Code. All rights reserved.
//

#include "Date.h"
#include "Timer.h"
#include "../Core/Engine.h"
#include "../Core/Log.h"
#include "../Data/Blob.h"
#include "../Math/MathUtils.h"
#include "../Mt/Thread.h"
#include "../Platform/Platform.h"
#include "../Platform/SystemShared.h"

#if NEKO_WINDOWS_FAMILY
#	include <Windows.h>
#	include <windowsx.h>
#endif

namespace Neko
{
    //#define PROFILING 1
#ifdef PROFILING
    static int64 GCurrentTime = 0;
#endif
    
    //! Profile smoothing time in seconds (original default was .8 / log(10) ~= .35 s)
    static const float DEFAULT_PROFILE_SMOOTHING = 1.0f;
    
#define DEFAULT_FRAME_SMOOTHING 1
    
    CTimer::CTimer()
    {
        // Default CVar values
        fFixedTimeStep = 0;
        fMaxTimeStep = 0.25f;
        fCvarTimescale = 1.0f;
        bTimeSmoothing = DEFAULT_FRAME_SMOOTHING;
        eTimeDebug = 0;
        
        fProfileSmoothTime = DEFAULT_PROFILE_SMOOTHING;
        ProfileWeighting = 1;
        
        // Persistant state
        bEnabled = true;
        FrameCounter = 0;
        
        TicksPerSec = Neko::Platform::GetTicksPerSecond();

        fSecsPerTick = 1.0 / TicksPerSec;
        
        fAverageFrameTime = 1.0f / 30.0f;
        for (int32 i = 0; i < MAX_FRAME_AVERAGE; i++)
        {
            fFrameTimes[i] = fAverageFrameTime;
        }
        
        fAvgFrameTime = 0.0f;
        fProfileBlend = 1.0f;
        fSmoothTime = 0;
        
        fTotalTimeScale = 1.0f;
        ClearTimeScales();
        
        ResetTimer();
    }
    
    float CTimer::GetFrameTime(ETimer which) const
    {
        return bEnabled && (which != ETIMER_GAME || !bGameTimerPaused) ? fFrameTime : 0.0f;
    }
    
    float CTimer::GetCurrTime(ETimer which) const
    {
        assert(which >= 0 && which < ETIMER_LAST && "Bad timer index");
        return CurrentTime[which].GetSeconds();
    }
    
    float CTimer::GetRealFrameTime() const
    {
        return bEnabled ? fRealFrameTime : 0.0f;
    }
    
    float CTimer::GetTimeScale() const
    {
        return fCvarTimescale * fTotalTimeScale;
    }
    
    float CTimer::GetTimeScale(uint32 channel) const
    {
        assert(channel < NUM_TIME_SCALE_CHANNELS);
        if (channel >= NUM_TIME_SCALE_CHANNELS)
        {
            return GetTimeScale();
        }
        return fCvarTimescale * fTimeScaleChannels[channel];
    }
    
    void CTimer::SetTimeScale(float scale, uint32 channel /* = 0 */)
    {
        assert(channel < NUM_TIME_SCALE_CHANNELS);
        if (channel >= NUM_TIME_SCALE_CHANNELS)
        {
            return;
        }
        
        const float currentScale = fTimeScaleChannels[channel];
        
        if (scale != currentScale)
        {
            // Need to adjust previous frame times for time scale to have immediate effect
            const float adjustFactor = scale / currentScale;
            for (uint32 i = 0; i < MAX_FRAME_AVERAGE; ++i)
            {
                fFrameTimes[i] *= adjustFactor;
            }
            
            // Update total time scale immediately
            fTotalTimeScale *= adjustFactor;
        }
        
        fTimeScaleChannels[channel] = scale;
    }
    
    void CTimer::ClearTimeScales()
    {
        if (fTotalTimeScale != 1.0f)
        {
            // Need to adjust previous frame times for time scale to have immediate effect
            const float adjustFactor = 1.0f / fTotalTimeScale;
            for (uint32 i = 0; i < MAX_FRAME_AVERAGE; ++i)
            {
                fFrameTimes[i] *= adjustFactor;
            }
        }
        
        for (int32 i = 0; i < NUM_TIME_SCALE_CHANNELS; ++i)
        {
            fTimeScaleChannels[i] = 1.0f;
        }
        fTotalTimeScale = 1.0f;
    }
    
    float CTimer::GetAsyncCurTime()
    {
        int64 llNow = Neko::Platform::GetTicks() - BaseTime;
        return TicksToSeconds(llNow);
    }
    
    float CTimer::GetFrameRate()
    {
        // Use real frame time.
        if (fRealFrameTime != 0.f)
        {
            return 1.f / fRealFrameTime;
        }
        return 0.f;
    }
    
    void CTimer::UpdateBlending()
    {
        // Accumulate smoothing time up to specified max.
        float fFrameTime = fRealFrameTime;
        fSmoothTime = Math::Minimum(fSmoothTime + fFrameTime, fProfileSmoothTime);
        
        if (fSmoothTime <= fFrameTime)
        {
            fAvgFrameTime = fFrameTime;
            fProfileBlend = 1.f;
            return;
        }
        
        if (ProfileWeighting <= 2)
        {
            // Update average frame time.
            if (fSmoothTime < fAvgFrameTime)
            {
                fAvgFrameTime = fSmoothTime;
            }
            fAvgFrameTime *= fSmoothTime / (fSmoothTime - fFrameTime + fAvgFrameTime);
            
            if (ProfileWeighting == 1)
            {
                // Weight all frames equally.
                fProfileBlend = fAvgFrameTime / fSmoothTime;
            }
            else
            {
                // Weight frames by time.
                fProfileBlend = fFrameTime / fSmoothTime;
            }
        }
        else
        {
            // Decay avg frame time, set as new peak.
            fAvgFrameTime *= 1.f - fFrameTime / fSmoothTime;
            if (fFrameTime > fAvgFrameTime)
            {
                fAvgFrameTime = fFrameTime;
                fProfileBlend = 1.f;
            }
            else
            {
                fProfileBlend = 0.f;
            }
        }
    }
    
    float CTimer::GetProfileFrameBlending(float* pfBlendTime, int* piBlendMode)
    {
        if (piBlendMode)
        {
            *piBlendMode = ProfileWeighting;
        }
        if (pfBlendTime)
        {
            *pfBlendTime = fSmoothTime;
        }
        return fProfileBlend;
    }
    
    void CTimer::RefreshGameTime(int64 curTime)
    {
        assert(curTime + OffsetTime >= 0);
        CurrentTime[ETIMER_GAME].SetSeconds(TicksToSeconds(curTime + OffsetTime));
    }
    
    void CTimer::RefreshUITime(int64 curTime)
    {
        assert(curTime >= 0);
        CurrentTime[ETIMER_UI].SetSeconds(TicksToSeconds(curTime));
    }
    
    void CTimer::UpdateOnFrameStart()
    {
        if (!bEnabled)
        {
            return;
        }
        
#ifdef PROFILING
        fRealFrameTime = fFrameTime = 0.020f; // 20ms = 50fps
        GCurrentTime += (int)(fFrameTime * (float)(CTimeValue::TIMEVALUE_PRECISION));
        LastTime = GCurrentTime;
        RefreshGameTime(LastTime);
        RefreshUITime(LastTime);
        return;
#endif
        
        if (fFixedTimeStep < 0.0f)
        {
            // Enforce real framerate by sleeping.
            const int64 elapsedTicks = Neko::Platform::GetTicks() - BaseTime - LastTime;
            const int64 minTicks = SecondsToTicks(-fFixedTimeStep);
            if (elapsedTicks < minTicks)
            {
                const int64 ms = (minTicks - elapsedTicks) * 1000 / TicksPerSec;
                Neko::MT::Sleep((unsigned int)ms);
            }
        }
        
        const int64 now = Neko::Platform::GetTicks();
        assert(now + 1 >= BaseTime && "Invalid base time"); //+1 margin because QPC may be one off across cores
        
        fRealFrameTime = TicksToSeconds(now - BaseTime - LastTime);
        
        if (0.0f != fFixedTimeStep)
        {
            // Apply fixed_time_step
            fFrameTime = Math::abs(fFixedTimeStep);
        }
        else
        {
            // Clamp to max_time_step
            fFrameTime = Math::Minimum(fRealFrameTime, fMaxTimeStep);
        }
        
        // Dilate time.
        fFrameTime *= GetTimeScale();
        
        if (bTimeSmoothing > 0)
        {
            fFrameTime = GetAverageFrameTime();
        }
        
        // Time can only go forward.
        if (fFrameTime < 0.0f)
        {
            fFrameTime = 0.0f;
        }
        if (fRealFrameTime < 0.0f)
        {
            fRealFrameTime = 0.0;
        }
        
        if (bEnabled && !bGameTimerPaused)
        {
            fReplicationTime += fFrameTime;
        }
        
        // Adjust the base time so that time actually seems to have moved forward fFrameTime
        const int64 frameTicks = SecondsToTicks(fFrameTime);
        const int64 realTicks = SecondsToTicks(fRealFrameTime);
        BaseTime += realTicks - frameTicks;
        if (BaseTime > now)
        {
            // Guard against rounding errors due to float <-> int64 precision
            assert(BaseTime - now <= 10 && "Bad base time or adjustment, too much difference for a rounding error");
            BaseTime = now;
        }
        const int64 currentTime = now - BaseTime;
        
        assert(fabsf(TicksToSeconds(currentTime - LastTime) - fFrameTime) < 0.01f && "Bad calculation");
        assert(currentTime >= LastTime && "Bad adjustment in previous frame");
        assert(currentTime + OffsetTime >= 0 && "Sum of game time is negative");
        
        // Update timers
        RefreshUITime(currentTime);
        if (!bGameTimerPaused)
        {
            RefreshGameTime(currentTime);
        }
        
        LastTime = currentTime;
        
        UpdateBlending();
        
        //if (eTimeDebug > 1)
        {
           // Log::Info("[CTimer]: Frame=%d Cur=%lld Now=%lld Off=%lld Async=%f CurrTime=%f UI=%f", 6, (long long)currentTime, (long long)now, (long long)OffsetTime, GetAsyncCurTime(), GetCurrTime(ETIMER_GAME), GetCurrTime(ETIMER_UI));
        }
    }
    
    float CTimer::GetAverageFrameTime()
    {
        float LastAverageFrameTime = fAverageFrameTime;
        float FrameTime = fFrameTime;
        
        uint32 numFT = MAX_FRAME_AVERAGE;
        for (int32 i = (numFT - 2); i > -1; i--)
        {
            fFrameTimes[i + 1] = fFrameTimes[i];
        }
        
        if (FrameTime > 0.4f) FrameTime = 0.4f;
        if (FrameTime < 0.0f) FrameTime = 0.0f;
        fFrameTimes[0] = FrameTime;
        
        // Get smoothed frame
        uint32 avrg_ftime = 1;
        if (LastAverageFrameTime)
        {
            avrg_ftime = uint32(0.25f / LastAverageFrameTime + 0.5f); //average the frame-times for a certain time-period (sec)
            if (avrg_ftime > numFT)
            {
                avrg_ftime = numFT;
            }
            if (avrg_ftime < 1)
            {
                avrg_ftime = 1;
            }
        }
        
        float AverageFrameTime = 0;
        for (uint32 i = 0; i < avrg_ftime; i++)
        {
            AverageFrameTime += fFrameTimes[i];
        }
        AverageFrameTime /= avrg_ftime;
        
        //don't smooth if we pause the game
        if (FrameTime < 0.0001f)
        {
            AverageFrameTime = FrameTime;
        }
        
        fAverageFrameTime = AverageFrameTime;
        return AverageFrameTime;
    }
    
    void CTimer::ResetTimer()
    {
        BaseTime = Neko::Platform::GetTicks();
        LastTime = 0;
        OffsetTime = 0;
        
        fFrameTime = 0.0f;
        fRealFrameTime = 0.0f;
        fReplicationTime = 0.0f;
        
        RefreshGameTime(0);
        RefreshUITime(0);
        
        bGameTimerPaused = false;
        GameTimerPausedTime = 0;
    }

    CTimeValue CTimer::GetAsyncTime() const
    {
        int64 llNow = Neko::Platform::GetTicks();
        double fConvert = CTimeValue::TIMEVALUE_PRECISION * fSecsPerTick;
        return CTimeValue(int64(llNow * fConvert));
    }
    
    bool CTimer::PauseTimer(ETimer which, bool bPause)
    {
        if (which != ETIMER_GAME)
        {
            return false;
        }
        
        if (bGameTimerPaused == bPause)
        {
            return false;
        }
        
        bGameTimerPaused = bPause;
        
        if (bPause)
        {
            GameTimerPausedTime = LastTime + OffsetTime;
            if (eTimeDebug)
            {
                //CryLogAlways("[CTimer]: Pausing ON: Frame=%d Last=%lld Off=%lld Async=%f CurrTime=%f UI=%f", gEnv->pRenderer->GetFrameID(false), (long long)LastTime, (long long)OffsetTime, GetAsyncCurTime(), GetCurrTime(ETIMER_GAME), GetCurrTime(ETIMER_UI));
            }
        }
        else
        {
            SetOffsetToMatchGameTime(GameTimerPausedTime);
            GameTimerPausedTime = 0;
            if (eTimeDebug)
            {
              //  CryLogAlways("[CTimer]: Pausing OFF: Frame=%d Last=%lld Off=%lld Async=%f CurrTime=%f UI=%f", gEnv->pRenderer->GetFrameID(false), (long long)LastTime, (long long)OffsetTime, GetAsyncCurTime(), GetCurrTime(ETIMER_GAME), GetCurrTime(ETIMER_UI));
            }
        }
        
        return true;
    }
    
    bool CTimer::IsTimerPaused(ETimer which)
    {
        if (which != ETIMER_GAME)
        {
            return false;
        }
        return bGameTimerPaused;
    }
    
    bool CTimer::SetTimer(ETimer which, float timeInSeconds)
    {
        if (which != ETIMER_GAME)
        {
            return false;
        }
        
        SetOffsetToMatchGameTime(SecondsToTicks(timeInSeconds));
        return true;
    }
    
    void CTimer::SecondsToDateUTC(time_t inTime, struct tm& outDateUTC)
    {
        outDateUTC = *gmtime(&inTime);
    }
    
#if NEKO_WINDOWS_FAMILY
    time_t gmt_to_local_win32(void)
    {
        TIME_ZONE_INFORMATION tzinfo;
        DWORD dwStandardDaylight;
        long bias;
        
        dwStandardDaylight = GetTimeZoneInformation(&tzinfo);
        bias = tzinfo.Bias;
        
        if (dwStandardDaylight == TIME_ZONE_ID_STANDARD)
            bias += tzinfo.StandardBias;
        
        if (dwStandardDaylight == TIME_ZONE_ID_DAYLIGHT)
            bias += tzinfo.DaylightBias;
        
        return (-bias * 60);
    }
#endif
    
    time_t CTimer::DateToSecondsUTC(struct tm& inDate)
    {
#if NEKO_WINDOWS
        return mktime(&inDate) + gmt_to_local_win32();
#elif NEKO_LINUX || NEKO_ANDROID
#if defined(HAVE_TIMEGM)
        // return timegm(&inDate);
#else
        // craig: temp disabled the +tm.tm_gmtoff because i can't see the intention here
        // and it doesn't compile anymore
        // alexl: tm_gmtoff is the offset to greenwhich mean time, whereas mktime uses localtime
        //        but not all linux distributions have it...
        return mktime(&inDate) /*+ tm.tm_gmtoff*/;
#endif
#else
        return mktime(&inDate);
#endif
    }
    
    void CTimer::SetOffsetToMatchGameTime(int64 ticks)
    {
        //const int64 previousOffset = OffsetTime;
        //const float previousGameTime = GetCurrTime(ETIMER_GAME);
        
        OffsetTime = ticks - LastTime;
        RefreshGameTime(LastTime);
        
        if (bGameTimerPaused)
        {
            // On un-pause, we will restore the specified time.
            // If we don't do this, the un-pause will over-write the offset again.
            GameTimerPausedTime = ticks;
        }
        
        if (eTimeDebug)
        {
           // GLogInfo.log("Engine") << "[CTimer] SetOffset: Offset " << (int64)previousOffset << " -> " << (int64)OffsetTime << ", GameTime " << (float)GetCurrTime(ETIMER_GAME) << " -> " << (float)previousGameTime;
        }
    }
    
    int64 CTimer::SecondsToTicks(double seconds) const
    {
        return (int64)(seconds * (double)TicksPerSec);
    }
    
    void CTimer::Serialize(Neko::OutputBlob& Blob)
    {
        int64 currentGameTime = LastTime + OffsetTime;
        
        Blob.Write(currentGameTime);
        Blob.Write(TicksPerSec);
    }
    
    void CTimer::Deserialize(Neko::InputBlob& Blob)
    {
        int64 ticksPerSecond = 1, curTime = 1;
        Blob.Read(curTime);
        Blob.Read(ticksPerSecond);
        
        // Adjust curTime for ticksPerSecond on this machine.
        // Some precision will be lost if the frequencies are not identical.
        const double multiplier = (double)TicksPerSec / (double)ticksPerSecond;
        curTime = (int64)((double)curTime * multiplier);
        
        SetOffsetToMatchGameTime(curTime);
        
        if (eTimeDebug)
        {
            const int64 now = Neko::Platform::GetTicks();
            GLogInfo.log("Engine") << "Timer / Deserialize: Last=" << (uint64)LastTime << " Now=" << (uint64)now << " Off=" << (uint64)OffsetTime << " Async=" << GetAsyncCurTime() << " CurrTime=" << GetCurrTime(ETIMER_GAME);
        }
    }
}
