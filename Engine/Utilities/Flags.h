//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Flags.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    /// Wrapper around an enum that allows simple use of bitwise logic operations.
    template <typename Enum, typename Storage = uint32>
    class NEKO_ENGINE_API CFlags
    {
    public:
        
        typedef Storage InternalType;
        
        CFlags()
        : Bits(0)
        { }
        
        CFlags(Enum value)
        {
            Bits = static_cast<Storage>(value);
        }
        
        CFlags(const Flags<Enum, Storage>& value)
        {
            Bits = value.Bits;
        }
        
        explicit CFlags(Storage bits)
        {
            Bits = bits;
        }
        
        /** Checks whether all of the provided bits are set */
        bool IsSet(Enum value) const
        {
            return (Bits & static_cast<Storage>(value)) == static_cast<Storage>(value);
        }
        
        /** Activates all of the provided bits. */
        Flags<Enum, Storage>& Set(Enum value)
        {
            Bits = static_cast<Storage>(value);
            
            return *this;
        }
        
        /** Deactivates all of the provided bits. */
        void Unset(Enum value)
        {
            Bits &= ~static_cast<Storage>(value);
        }
        
        bool operator==(Enum rhs) const { return Bits == static_cast<Storage>(rhs); }
        
        bool operator==(const CFlags<Enum, Storage>& rhs) const { return Bits == rhs.Bits; }
        
        bool operator==(bool rhs) const { return ((bool)*this) == rhs; }
        
        bool operator!=(Enum rhs) const { return Bits != static_cast<Storage>(rhs); }
        
        bool operator!=(const CFlags<Enum, Storage>& rhs) const { return Bits != rhs.Bits; }
        
        CFlags<Enum, Storage>& operator=(Enum rhs)
        {
            Bits = static_cast<Storage>(rhs);
            
            return *this;
        }
        
        CFlags<Enum, Storage>& operator=(const CFlags<Enum, Storage>& rhs)
        {
            Bits = rhs.Bits;
            
            return *this;
        }
        
        CFlags<Enum, Storage>& operator|=(Enum rhs)
        {
            Bits |= static_cast<Storage>(rhs);
            
            return *this;
        }
        
        CFlags<Enum, Storage>& operator|=(const CFlags<Enum, Storage>& rhs)
        {
            Bits |= rhs.Bits;
            
            return *this;
        }
        
        CFlags<Enum, Storage> operator|(Enum rhs) const
        {
            Flags<Enum, Storage> out(*this);
            out |= rhs;
            
            return out;
        }
        
        CFlags<Enum, Storage> operator|(const CFlags<Enum, Storage>& rhs) const
        {
            CFlags<Enum, Storage> out(*this);
            out |= rhs;
            
            return out;
        }
        
        CFlags<Enum, Storage>& operator&=(Enum rhs)
        {
            Bits &= static_cast<Storage>(rhs);
            
            return *this;
        }
        
        CFlags<Enum, Storage>& operator&=(const CFlags<Enum, Storage>& rhs)
        {
            Bits &= rhs.Bits;
            
            return *this;
        }
        
        CFlags<Enum, Storage> operator&(Enum rhs) const
        {
            CFlags<Enum, Storage> out = *this;
            out.Bits &= static_cast<Storage>(rhs);
            
            return out;
        }
        
        CFlags<Enum, Storage> operator&(const CFlags<Enum, Storage>& rhs) const
        {
            CFlags<Enum, Storage> out = *this;
            out.Bits &= rhs.Bits;
            
            return out;
        }
        
        CFlags<Enum, Storage>& operator^=(Enum rhs)
        {
            Bits ^= static_cast<Storage>(rhs);
            
            return *this;
        }
        
        CFlags<Enum, Storage>& operator^=(const CFlags<Enum, Storage>& rhs)
        {
            Bits ^= rhs.Bits;
            
            return *this;
        }
        
        CFlags<Enum, Storage> operator^(Enum rhs) const
        {
            CFlags<Enum, Storage> out = *this;
            out.Bits ^= static_cast<Storage>(rhs);
            
            return out;
        }
        
        CFlags<Enum, Storage> operator^(const CFlags<Enum, Storage>& rhs) const
        {
            Flags<Enum, Storage> out = *this;
            out.Bits ^= rhs.Bits;
            
            return out;
        }
        
        CFlags<Enum, Storage> operator~() const
        {
            CFlags<Enum, Storage> out;
            out.Bits = (Storage)~Bits;
            
            return out;
        }
        
        operator bool() const { return Bits ? true : false; }
        
        operator uint8() const { return static_cast<uint8>(Bits); }
        
        operator uint16() const { return static_cast<uint16>(Bits); }
        
        operator uint32() const { return static_cast<uint32>(Bits); }
        
        friend CFlags<Enum, Storage> operator&(Enum a, CFlags<Enum, Storage>& b)
        {
            CFlags<Enum, Storage> out;
            out.Bits = a & b.Bits;
            return out;
        }
        
    private:
        
        Storage Bits;
    };
    
    /** CFlags<Enum, Storage> implementation. */
#define NEKO_FLAGS_OPERATORS(Enum) NEKO_FLAGS_OPERATORS_EXT(Enum, uint32)
    
    /** CFlags<Enum, Storage> implementation. */
#define NEKO_FLAGS_OPERATORS_EXT(Enum, Storage)																   \
inline Flags<Enum, Storage> operator|(Enum a, Enum b) { Flags<Enum, Storage> r(a); r |= b; return r; } \
inline Flags<Enum, Storage> operator&(Enum a, Enum b) { Flags<Enum, Storage> r(a); r &= b; return r; } \
inline Flags<Enum, Storage> operator~(Enum a) { return ~Flags<Enum, Storage>(a); }
} // Neko
