//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Text.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Data/IAllocator.h"
#include "../Data/DefaultAllocator.h"
#include "../Math/MathUtils.h"
#include "../Containers/Array.h"
#include "../Utilities/Crc32.h"

#include "MemoryTrackingUtilities.h"

#include "StringUtil.h"

#include <limits.h> // INT_MAX

typedef void* mono_string;

#if NEKO_APPLE_FAMILY
#   ifdef __OBJC__
#       include <Foundation/NSString.h>
#       include <CoreFoundation/CFString.h>
#   endif
#endif

namespace Neko
{
    NEKO_ENGINE_API IAllocator& GetDefaultStringAllocator();
    
    /** Determines search direction for string operations. */
    namespace ESearchDir
    {
        enum Type
        {
            /** Search from the start. */
            FromStart,
            
            /** Search from the end. */
            FromEnd,
        };
    }
    
    /** Search case modes. */
    enum class ESearchCase : int
    {
        IgnoreCase = 0,
        
        CaseSensitive
    };
    
    /// Null-terminated string with meta-data. Meta-data contains length of the string, reference count and et cetera.
    template<typename symbol>
    class NEKO_ENGINE_API TString
    {
    protected:
        
        typedef NullTerminatedArrayHeader<symbol> TextHeader;
        
    public:
        
        typedef symbol value_type;
        
        /** Gets the number of characters in this text. */
        int32 Length() const
        {
            return this->Header->Length;
        }
        
        /** Gets the number of characters that can fit in the memory block that was allocated for this text. */
        __declspec(property(get = GetCapacity)) int32 Capacity;
        int32 GetCapacity() const
        {
            return this->Header->Capacity;
        }
        
        /** Indicates whether this string is empty. */
        __declspec(property(get = IsEmpty)) bool Empty;
        bool IsEmpty() const
        {
            return this->Length() == 0;
        }
        
    public:
        
#pragma region Construction
        
        /** Creates an empty text object with default allocator. */
        TString();
        
        /** Creates a new object that encapsulates a deep copy of specified number of characters. */
        TString(const symbol* Chars, int32 Length);
        
        /** Creates a new empty object with specified number of characters. */
        TString(int32 Length);
        
        /** Creates a new object of this type that encapsulates a deep copy of given null-terminated string. */
        TString(const symbol *chars)
        : TString(chars, _strlen(chars), GetDefaultStringAllocator())
        {
        }
        
        /** Creates an empty text object with the specified allocator. */
        TString(Neko::IAllocator& allocator)
        : Allocator(allocator)
        {
            this->InitEmpty();
        }
        
        /** A copy constructor. */
        TString(const TString& Other)
        : Allocator(Other.Allocator)
        {
            // Make a shallow copy.
            if (Other.Header->ReferenceCount >= 0)
            {
                this->str = Other.str;
                this->Header->RegisterReference();
            }
            else
            {
                this->InitEmpty();
            }
        }
        
        /**
         * Creates a new object of this type that is a substring of another one.
         * @param other  Reference to the string to get the substring out of.
         * @param offset Zero-based index of the first character in the sub-string. If this number is greater then number of characters in the given characters, then this object remains the same.
         * @param count  Number of characters in the sub-string. Will be trimmed, if necessary.
         */
        TString(const TString& other, int32 offset, int32 count)
        : Allocator(other.Allocator)
        {
            this->InitEmpty();
            this->Assign(other, offset, count);
        }

        
        /** Creates a new object that encapsulates a deep copy of specified number of characters. */
        TString(const symbol *chars, int32 length, Neko::IAllocator& allocator)
        : Allocator(allocator)
        {
            this->InitEmpty();
            
            // Make a deep copy.
            if (length != 0)
            {
                this->AllocateMemory(length);
                CopyInternal(this->str, chars, length);
            }
        }

        
        /** Creates a new empty object with specified number of characters. */
        TString(int32 length, Neko::IAllocator& allocator)
        : Allocator(allocator)
        {
            this->InitEmpty();
            
            if (length != 0)
            {
                this->AllocateMemory(length);
            }
        }
        
        
        /** Creates a new object of this type that encapsulates a deep copy of given null-terminated string. */
        TString(const symbol *chars, Neko::IAllocator& allocator)
        : TString(chars, _strlen(chars), allocator)
        {
            
        }
        
        
        /**
         * Creates a new null-terminated string from given .Net/Mono string.
         * @param managedString Instance of type System.String.
         */
        TString(mono_string managedString, Neko::IAllocator& allocator)
        : Allocator(allocator)
        {
#if defined(MONO_API)
            this->InitEmpty();
            
            MonoError error;
            symbol* ntText = mono_string_native(managedString, &error);
            
            if (mono_error_ok(&error))
            {
                this->Assign(ntText);
                
                mono_free(ntText);
            }
            else
            {
                std::logic_error(mono_error_get_message(&error));
            }
#else
            this->str = nullptr;
#endif // MONO_API
        }
        
        
#if __OBJC__
        /** Initializes TString with Objective-C NSString* */
        TString(const NSString* In);
#endif
        
#pragma endregion
        
        /**
         * Breaks up a delimited string into elements of a string array.
         *
         * @param	OutArray		The array to fill with the string pieces
         * @param	pchDelim	The string to delimit on
         * @param	InCullEmpty	If 1, empty strings are not added to the array
         *
         * @return	The number of elements in InArray
         */
        int32 ParseIntoArray(Neko::TArray<TString>& OutArray, const char* pchDelim, bool InCullEmpty) const
        {
            // Make sure the delimit string is not null or empty
            assert(pchDelim);
            OutArray.Clear();
            const char* Start = this->str;
            int32 DelimLength = Neko::StringLength(pchDelim);
            if (Start && DelimLength)
            {
                while (const char* At = Neko::FindSubstring(Start, pchDelim))
                {
                    if (!InCullEmpty || At-Start)
                    {
                        OutArray.Push(TString(Start, static_cast<int32>(At - Start), Allocator));
                    }
                    Start = At + DelimLength;
                }
                if (!InCullEmpty || *Start)
                {
                    OutArray.Push(TString(Start, Allocator));
                }
            }
            return OutArray.GetSize();
        }
        
    private:
        
#pragma region Construction Utilities
        void MakeUnique()
        {
            if (this->Header->referenceCount <= 0)
            {
                return;
            }
            
            TextHeader *oldHeader = this->Header;
            this->Release();
            this->AllocateMemory(oldHeader->capacity);
            CopyInternal(this->str, oldHeader->Elements, oldHeader->length + 1);
        }
        
        
        static size_t CalculateMemoryToAllocate(int32 characterCapacity)
        {
            return sizeof(TextHeader) + (characterCapacity + 1) * sizeof(symbol);
        }
        
        
        /** Initializes an empty string object. */
        void InitEmpty()
        {
            // Thanks to this code, no empty objects consume excessive amounts of memory.
            this->str = TextHeader::EmptyHeader()->Elements;
        }
        
        
        void AllocateMemory(int32 capacity)
        {
            assert(capacity >= 0);
            assert(capacity <= INT_MAX - 1);
            
            if (capacity == 0)
            {
                this->InitEmpty();
            }
            else
            {
                auto byteCount = CalculateMemoryToAllocate(capacity);
                
                TextHeader *header = static_cast<TextHeader *>(Allocator.Allocate(byteCount));
                
                header->ReferenceCount = 1;
                header->Length         = capacity;
                header->Capacity       = capacity;
                this->str              = header->Elements;
                this->str[capacity]    = '\0';
            }
        }
        
#pragma endregion
        
    public:
        
#pragma region Destruction
        
        /** Releases this text object. */
        ~TString()
        {
            ReleaseData(Allocator, this->Header);
        }
        
#pragma endregion
        
    private:
        
#pragma region Destruction Utilities
        
        /** Makes this object into an empty one. */
        void Release()
        {
            // Release, if not empty.
            if (this->Header->ReferenceCount >= 0)
            {
                ReleaseData(Allocator, this->Header);
                this->InitEmpty();
            }
        }
        
        
        /** Releases this object's data. */
        static void ReleaseData(Neko::IAllocator& Allocator, TextHeader *header)
        {
            if (header->ReferenceCount >= 0) // Check if empty.
            {
                if (!header->UnregisterReference()) // Check, if has any live references.
                {
                    Allocator.Deallocate(header);
                }
            }
        }
#pragma endregion
        
    public:
        
#pragma region Interface
        
        void Clear()
        {
            if (this->Empty)
            {
                return;
            }
            if (this->Header->referenceCount >= 0)
            {
                this->Release();
            }
            else
            {
                this->Resize(0);
            }
        }
        
        
        /**
         *  Creates a deep copy of text data that is held by this object.
         *  @returns A pointer to data that has to be deleted when no longer in use.
         */
        const symbol* Duplicate() const
        {
            if (this->Empty)
            {
                return nullptr;
            }
            
            int32 charCount = this->Length;
            symbol* dup       = new char[charCount + 1];
            CopyInternal(dup, this->str, charCount);
            dup[charCount] = '\0';
            return dup;
        }
        
        
        /** Determines relative order of this object and another one in sequence that is sorted in ascending order. */
        int32 CompareTo(const TString& other) const
        {
            return _compare(this->str, other.str);
        }
        
        
        /** Determines relative order of this object and another one in sequence that is sorted in ascending order. */
        int32 CompareTo(const char* other) const
        {
            return _compare(this->str, other);
        }
        
#pragma region Assignment
        
        /**
         *  Assigns a deep copy of a sub-string to this object.
         *  @param other  Reference to the object that contains a sub-string that will be assigned to this object.
         *  @param offset Zero-based index of the first character in the sub-string. If this number is greater then number of characters in the given characters, then this object remains the same.
         *  @param count  Number of characters in the sub-string. Will be trimmed, if necessary.
         *  @returns A reference to this object.
         */
        TString& Assign(const TString& other, int32 offset, int32 count)
        {
            int32 length = other.Length;
            if (offset > length)
            {
                // Assignment impossible.
                return *this;
            }
            if (offset + count > length)
            {
                // Trim the count.
                count = length - offset;
            }
            
            this->AssignInternal(other.str + offset, count);
            
            return *this;
        }
        
        
        /**
         *  Assigns a deep copy of a null-terminated string to this object.
         *  @param chars Pointer to the null-terminated string to assign to this object.
         *  @returns A reference to this object.
         */
        TString& Assign(const symbol* chars)
        {
            if (chars)
            {
                this->AssignInternal(chars, _strlen(chars));
            }
            
            return *this;
        }
        
        
        /**
         *  Assigns a string built out of parts to this object.
         *  @param Parts Pointer to the null-terminated string to assign to this object.
         *  @returns A reference to this object.
         */
        TString& Assign(std::initializer_list< const symbol* > Parts)
        {
            int32 totalLength = 0;
            for (auto current = Parts.begin(); current < Parts.end(); current++)
            {
                totalLength += _strlen(*current);
            }
            
            if (totalLength == 0)
            {
                return *this;
            }
            
            if (this->Shared || this->Capacity < totalLength)
            {
                this->Release();
                this->AllocateMemory(totalLength);
            }
            
            this->Header->Length   = totalLength;
            this->str[totalLength] = '\0';
            
            int32 length = 0;
            for (auto current = Parts.begin(); current < Parts.end(); current++)
            {
                int32 currentLength = _strlen(*current);
                CopyInternal(this->str + length, *current, currentLength);
                length += currentLength;
            }
            
            return *this;
        }
        
#pragma endregion
        
    private:
        
#pragma region Assignment Utilities
        
        void AssignInternal(const symbol* chars, int32 count)
        {
            // Allocate more data, if this string is shared, or doesn't have enough allocated memory.
            if (this->Shared || this->Capacity < count)
            {
                this->Release();
                this->AllocateMemory(count);
            }
            
            CopyInternal(this->str, chars, count);
            
            this->Header->Length = count;
            this->str[count]     = '\0';
        }
        
#pragma endregion
        
    public:
        
#pragma region Size
        
        //! Resizes this string and pads it with a filling character, if necessary.
        void Resize(int32 newCount, symbol filler = 0)
        {
            this->MakeUnique();
            auto length = this->Length();
            if (newCount > length)
            {
                this->Append(newCount - length, filler);
            }
            else if (newCount < length)
            {
                this->Header->Length = newCount;
                this->str[newCount]  = '\0';
            }
        }
        
        
        /** Expands the memory block to fit more text, if given new capacity is greater then current one, otherwise trims the memory block to be exact size to fit current text. */
        void Reserve(int32 newCapacity)
        {
            auto oldCapacity = this->Capacity;
            if (newCapacity > oldCapacity)
            {
                TextHeader *oldHeader = this->Header;
                
                this->AllocateMemory(newCapacity);
                CopyInternal(this->str, oldHeader->Elements, oldHeader->length);
                this->Header->Length         = oldHeader->length;
                this->str[oldHeader->length] = '\0';
                ReleaseData(Allocator, oldHeader);
            }
            else if (oldCapacity != this->Length)
            {
                TextHeader *oldHeader = this->Header;
                
                this->AllocateMemory(this->Length);
                CopyInternal(this->str, oldHeader->Elements, oldHeader->length);
                ReleaseData(Allocator, oldHeader);
            }
        }
        
#pragma endregion
        
#pragma region Appending
        
        /** Appends a number of specified characters to the end of this string. */
        TString& Append(int32 count, symbol character)
        {
            if (count <= 0)
            {
                return *this;
            }
            
            if (this->Shared || this->Length() + count > this->Capacity)
            {
                TextHeader *oldHeader = this->Header;
                this->AllocateMemory(this->Length() + count);
                CopyInternal(this->str, oldHeader->Elements, oldHeader->length);
                SetInternal(this->str + oldHeader->length, character, count);
                ReleaseData(Allocator, oldHeader);
            }
            else
            {
                auto oldLength = this->Length();
                SetInternal(this->str + oldLength, character, count);
                this->Header->Length    = oldLength + count;
                this->str[this->Length()] = '\0';
            }
            
            return *this;
        }
        
        
        /** Appends a null-terminated string to the end of this string. */
        TString& Append(symbol *chars)
        {
            if (chars)
            {
                this->ConcatenateInPlace(chars, _strlen(chars));
            }
            
            return *this;
        }
        /** Appends a null-terminated string to the end of this string. */
        TString& Append(const symbol *chars)
        {
            if (chars)
            {
                this->ConcatenateInPlace(chars, _strlen(chars));
            }
            
            return *this;
        }
        /** Appends a null-terminated string to the end of this string. */
        TString& Append(const symbol *chars, int32 Count)
        {
            if (chars)
            {
                this->ConcatenateInPlace(chars, Count);
            }
            
            return *this;
        }
        
        template <class S> TString& Append(S value)
        {
            char tmp[30];
            Neko::ToCString(value, tmp, 30);
            Append(tmp);
            return *this;
        }
        
        /** Appends float value to the string. */
        TString& Append(const float value)
        {
            char tmp[40];
            Neko::ToCString(value, tmp, 30, 10);
            Append(tmp);
            return *this;
        }
        
#pragma endregion
        
        NEKO_FORCE_INLINE IAllocator& GetAllocator() const
        {
            return Allocator;
        }
        
    public:
        
#pragma region Operators
        
        /** TArray accessor operator. */
        NEKO_FORCE_INLINE const symbol operator[](int32 Index) const
        {
            assert(Index >= 0 && Index < Length());
            return this->str[Index];
        }
        
        
        /** TArray accessor operator. */
        NEKO_FORCE_INLINE symbol& operator[](int32 Index)
        {
            assert(Index >= 0/* && Index < Length*/);
            return this->str[Index];
        }
        
        
        /** Assigns another string to this one. */
        NEKO_FORCE_INLINE TString& operator=(const TString &other)
        {
            if (this->str == other.str)
            {
                return *this;
            }
            
            if (this->Header->ReferenceCount < 0)
            {
                if (other.Header->ReferenceCount >= 0)
                {
                    this->str = other.str;
                    this->Header->RegisterReference();
                }
            }
            else if (other.Header->ReferenceCount < 0)
            {
                this->Release();
                this->str = other.str;
            }
            else
            {
                this->Release();
                this->str = other.str;
                this->Header->RegisterReference();
            }
            return *this;
        }
        
        
        /** Assigns another string to this one. */
        NEKO_FORCE_INLINE TString& operator=(const symbol *other)
        {
            this->Assign(other);
            
            return *this;
        }
        
        
        /** Assigns another string to this one. */
        NEKO_FORCE_INLINE TString& operator =(std::initializer_list< const symbol* > Parts)
        {
            this->Assign(Parts);
            
            return *this;
        }
        
        
        NEKO_FORCE_INLINE bool operator ==(const TString& other) const
        {
            return _compare(this->str, other.str) == 0;
        }
        
        
        NEKO_FORCE_INLINE bool operator !=(const TString& other) const
        {
            return _compare(this->str, other.str) != 0;
        }
        
        
        NEKO_FORCE_INLINE bool operator ==(const symbol* other) const
        {
            return _compare(this->str, other) == 0;
        }
        
        
        NEKO_FORCE_INLINE bool operator !=(const symbol* other) const
        {
            return _compare(this->str, other) != 0;
        }
        
        
        NEKO_FORCE_INLINE bool operator > (const TString<symbol>& Other) const
        {
            return _compare(Other.c_str()) > 0;
        }
        
        
        NEKO_FORCE_INLINE bool operator < (const TString<symbol>& Other) const
        {
            return CompareTo(Other.c_str()) < 0;
        }
        
        
        /** Concatenates TString with TString. */
        NEKO_FORCE_INLINE TString& operator += (const TString<symbol>& Other)
        {
            if (Other.Length() == 0)
            {
                return *this;
            }
            
            Append(Other.c_str(), Other.Length());
            return *this;
        }
        
        
        /** Concatenates TString with raw C string. */
        NEKO_FORCE_INLINE TString& operator += (const symbol* Other)
        {
            assert(Other);
            
            const int32 OtherLength = _strlen(Other);
            if (OtherLength == 0)
            {
                return *this;
            }
            
            Append(Other, OtherLength);
            return *this;
        }
        
        
        /** Concatenates TString with C char symbol. */
        NEKO_FORCE_INLINE TString& operator += (const symbol Other)
        {
            Append(Other);
            return *this;
        }
        
        
        /** Integer operator, returns string hash. */
        NEKO_FORCE_INLINE int32 operator() () const
        {
            return Neko::Crc32(this->str);
        }
        
        
        /** Implicit conversion. Returns wrapped pointer. */
        NEKO_FORCE_INLINE operator const symbol* () const
        {
            return this->str;
        }
        
        
        /** Used when this object has to be passed to the function with variadic parameter list. */
        NEKO_FORCE_INLINE const symbol* c_str() const
        {
            return this->str;
        }
        
        
        /** 
         * Gets pointer to the string data.
         */
        NEKO_FORCE_INLINE const symbol* operator*() const
        {
            return Length() ? this->str : "";
        }
        
#pragma endregion
        
        /** @return the substring from Start position for Count characters. */
        NEKO_FORCE_INLINE TString Mid( int32 Start, int32 Count = INT_MAX ) const
        {
            assert(Count >= 0);
            uint32 End = Start+Count;
            Start    = Math::Clamp( (uint32)Start, (uint32)0,     (uint32)Length() );
            End      = Math::Clamp( (uint32)End,   (uint32)Start, (uint32)Length() );
            return TString(**this + Start, End - Start);
        }
        
        
        TString TrimQuotes( bool* bQuotesRemoved ) const
        {
            bool bQuotesWereRemoved=false;
            int32 Start = 0, Count = Length();
            if ( Count > 0 )
            {
                if ( (*this)[0] == '"' )
                {
                    Start++;
                    Count--;
                    bQuotesWereRemoved=true;
                }
                
                if ( Length() > 1 && (*this)[Length() - 1] == '"' )
                {
                    Count--;
                    bQuotesWereRemoved=true;
                }
            }
            
            if ( bQuotesRemoved != nullptr )
            {
                *bQuotesRemoved = bQuotesWereRemoved;
            }
            return Mid(Start, Count);
        }
        
        
        /**
         * Erases amount of data from offset.
         * @param Offset    Start offset in this string to remove from.
         * @param Count     Amount of data to remove.
         */
        void Erase(int32 Offset, int32 Count)
        {
            // Check the position
            Offset = Neko::Math::Maximum(0, Neko::Math::Minimum<int32>(this->Header->Length, Offset));
            Count = Neko::Math::Minimum<int32>(this->Header->Length - Offset, Count);
            
            if (Count <= 0)
            {
                return;
            }
            
            memmove(this->str + Offset, this->str + Offset + Count, this->Header->Length - (Offset + Count));
            this->Header->Length -= Count;
            this->str[this->Header->Length] = '\0';
        }
        
        
        /** Replaces occurences of needed string to a new value. */
        TString Replace(const symbol* From, const symbol* To, Neko::ESearchCase Case = Neko::ESearchCase::IgnoreCase)
        {
            assert(To);
            
            if (IsEmpty() || !From || !*From)
            {
                return *this;
            }
            
            const symbol* Data = this->str;
            
            
            int32 FromLength = Neko::StringLength(From);
            int32 ToLength = Neko::StringLength(To);
            
            TString Result;
            while (true)
            {
                const symbol* FromString = (Case == Neko::ESearchCase::IgnoreCase) ? Neko::FindISubstring(Data, From) : Neko::FindSubstring(Data, From);
                
                if (!FromString)
                {
                    break;
                }
                
                Result.Append(Data, (int32)(FromString - Data));
                
                // Copy to the 'To'
                Result.Append(To, ToLength);
                
                Data = FromString + FromLength;
            }
            
            // Left over
            Result.Append(Data);
            return Result;
        }
        
        
        int32 Find(const symbol* SubStr, ESearchCase SearchCase = ESearchCase::IgnoreCase, ESearchDir::Type SearchDir = ESearchDir::FromStart, int32 StartPosition = INDEX_NONE) const
        {
            if (SubStr == nullptr)
            {
                return INDEX_NONE;
            }
            
            if (SearchDir == ESearchDir::FromStart)
            {
                const symbol* Start = **this;
                if ( StartPosition != INDEX_NONE )
                {
                    Start += Math::Clamp(StartPosition, 0, Length() - 1);
                }
                const symbol* Tmp = SearchCase == ESearchCase::IgnoreCase ? FindISubstring(Start, SubStr) : FindSubstring(Start, SubStr);
                
                return (int32)(Tmp ? (Tmp - **this) : INDEX_NONE);
            }
            // @todo Implement FromEnd algorithm
            return INDEX_NONE;
        }
        
#ifdef __OBJC__
        NSString* GetNSString() const;
#endif
        
    private:
        
#pragma region Utilities
        
        static void CopyInternal(symbol *destination, const symbol *source, int32 count)
        {
            if (destination != source)
            {
                memcpy(destination, source, count * sizeof(symbol));
            }
        }
        
        
        static void SetInternal(symbol *destination, symbol filler, int32 count)
        {
            memset(destination, filler, count * sizeof(symbol));
        }
        
        
        void ConcatenateInPlace(const char* chars, int32 count)
        {
            if (count <= 0)
            {
                return;
            }
            
            if (this->Shared || this->Length() + count > this->Capacity)
            {
                TextHeader *oldHeader = this->Header;
                this->Concatenate(oldHeader->Elements, oldHeader->Length, chars, count);
                ReleaseData(Allocator, oldHeader);
            }
            else
            {
                CopyInternal(this->str + this->Length(), chars, count);
                this->Header->Length   += count;
                this->str[this->Length()] = '\0';
            }
        }
        
        
        void Concatenate(const char* chars1, int32 count1, const char* chars2, int32 count2)
        {
            auto sumLength = count1 + count2;
            
            auto doubleCount1 = count1 * 2;
            if (doubleCount1 > sumLength)
            {
                sumLength = doubleCount1;
            }
            
            if (sumLength != 0)
            {
                if (sumLength < 8)
                {
                    sumLength = 8;
                }
                
                this->AllocateMemory(sumLength);
                CopyInternal(this->str,          chars1, count1);
                CopyInternal(this->str + count1, chars2, count2);
                this->Header->Length       = count1 + count2;
                this->str[count1 + count2] = 0;
            }
        }
        
#pragma endregion
        
        
#pragma region Specialization Utilities
        //! Counts number of characters in the string.
        static int32 _strlen(const symbol* str);
        //! Compares 2 strings.
        static int32 _compare(const symbol* str1, const symbol* str2);
        //! To managed string.
        static mono_string _mono_str(const symbol* str);
        //! From managed string.
        static symbol* mono_string_native(mono_string str, void* _error);
        //! Checks for the presence of the substring.
        static bool _contains_substring(const symbol* str0, const symbol* str1, bool ignoreCase);
#pragma endregion
        
        
    protected:
        
#pragma region Fields
        symbol* str;
        
        Neko::IAllocator& Allocator;
        
#pragma endregion
        
#pragma region Reflection
        /** Gets the object that describes this text. */
        __declspec(property(get = GetHeader)) TextHeader* Header;
        TextHeader* GetHeader() const
        {
            TString* _this  = const_cast< TString* >(this);
            TextHeader   *header = reinterpret_cast< TextHeader* >(_this->str) - 1;
            return header;
        }
        
        /** Indicates whether this object shares its text data with others. */
        __declspec(property(get = IsShared)) bool Shared;
        bool IsShared() const
        {
            return this->Header->ReferenceCount > 1;
        }
        
    };
    
    typedef TString<char> Text;
    typedef TString<wchar_t> Text16;
        
        
    /**
     * RTTIPlainType specialization for String that allows strings be serialized as value types.
     *
     * @see        RTTIPlainType
     */
    template<> struct RTTIPlainType<Text>
    {
        enum { id = 20 }; enum { HasDynamicSize = 1 };
        
        static void toMemory(const Text& data, char* memory)
        {
            uint32 size = getDynamicSize(data);
            
            memcpy(memory, &size, sizeof(uint32));
            memory += sizeof(uint32);
            size -= sizeof(uint32);
            memcpy(memory, data.c_str(), size);
        }
        
        static uint32 fromMemory(Text& data, char* memory)
        {
            uint32 size;
            memcpy(&size, memory, sizeof(uint32));
            memory += sizeof(uint32);
            
            uint32 stringSize = size - sizeof(uint32);
            char* buffer = (char*)malloc(stringSize + 1);
            memcpy(buffer, memory, stringSize);
            buffer[stringSize] = '\0';
            data = Text(buffer);
            
            free(buffer);
            
            return size;
        }
        
        static uint32 getDynamicSize(const Text& data)
        {
            uint64 dataSize = data.Length() * sizeof(Text::value_type) + sizeof(uint32);
            
#if BS_DEBUG_MODE
            if (dataSize > std::numeric_limits<uint32>::max())
            {
                __string_throwDataOverflowException();
            }
#endif
            
            return (uint32)dataSize;
        }
    };
    
    /**
     * RTTIPlainType specialization for WString that allows strings be serialized as value types.
     *
     * @see        RTTIPlainType
     */
    template<> struct RTTIPlainType<Text16>
    {
        enum { id = TID_WString }; enum { HasDynamicSize = 1 };
        
        static void toMemory(const Text16& data, char* memory)
        {
            uint32 size = getDynamicSize(data);
            
            memcpy(memory, &size, sizeof(uint32));
            memory += sizeof(uint32);
            size -= sizeof(uint32);
            memcpy(memory, data.c_str(), size);
        }
        
        static uint32 fromMemory(Text16& data, char* memory)
        {
            uint32 size;
            memcpy(&size, memory, sizeof(uint32));
            memory += sizeof(uint32);
            
            uint32 stringSize = size - sizeof(uint32);
            Text16::value_type* buffer = (Text16::value_type*)malloc(stringSize + sizeof(Text16::value_type));
            memcpy(buffer, memory, stringSize);
            
            uint32 numChars =  stringSize / sizeof(Text16::value_type);
            buffer[numChars] = L'\0';
            
            data = Text16(buffer);
            
            free(buffer);
            
            return size;
        }
        
        static uint32 getDynamicSize(const Text16& data)
        {
            uint64 dataSize = data.Length() * sizeof(Text16::value_type) + sizeof(uint32);
            
#if BS_DEBUG_MODE
            if (dataSize > std::numeric_limits<uint32>::max())
            {
                __string_throwDataOverflowException();
            }
#endif
            
            return (uint32)dataSize;
        }
    };
}

namespace std
{
    template <>
    struct hash<Neko::Text>
    {
        std::size_t operator()(const Neko::Text& k) const
        {
            return Neko::Crc32(k.c_str());
        }
    };
    
}
