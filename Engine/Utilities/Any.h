//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_\     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Any.h
//  Neko engine
//
//  Copyright © 2017 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    /// Class capable of storing any general type, and safely extracting the proper type from the internal data.
    class AnyObject
    {
    private:
        
        class DataBase
        {
        public:
            
            virtual ~DataBase()
            { }
            
            virtual DataBase* Clone() const = 0;
        };
        
        template <typename ValueType>
        class Data : public DataBase
        {
        public:
            
            Data(const ValueType& value)
            : value(value)
            { }
            
            virtual DataBase* Clone() const override
            {
                return new Data(value);
            }
            
            ValueType value;
        };
        
    public:
        
        AnyObject()
        : pData(nullptr)
        { }
        
        template <typename ValueType>
        AnyObject(const ValueType& value)
        : pData(new Data<ValueType>(value))
        {  }
        
        AnyObject(std::nullptr_t)
        : pData(nullptr)
        { }
        
        AnyObject(const AnyObject& other)
        : pData(other.pData != nullptr ? other.pData->Clone() : nullptr)
        { }
        
        ~AnyObject()
        {
            if (pData != nullptr)
            {
                delete pData;
            }
        }
        
        /** Swaps the contents of this object with another. */
        AnyObject& swap(AnyObject& rhs)
        {
            std::swap(pData, rhs.pData);
            return *this;
        }
        
        template <typename ValueType>
        AnyObject& operator= (const ValueType& rhs)
        {
            AnyObject(rhs).swap(*this);
            return *this;
        }
        
        AnyObject& operator= (const AnyObject& rhs)
        {
            AnyObject(rhs).swap(*this);
            return *this;
        }
        
        /** Returns true if no type is set. */
        NEKO_FORCE_INLINE bool IsEmpty() const { return pData == nullptr; }
        
    private:
        
        template <typename ValueType>
        friend ValueType* any_cast(AnyObject*);
        
        template <typename ValueType>
        friend ValueType* any_cast_unsafe(AnyObject*);
        
        DataBase* pData;
    };
    
    /**
     * Returns a pointer to the internal data of the specified type.
     *
     * @note		Will return null if cast fails.
     */
    template <typename ValueType>
    ValueType* any_cast(AnyObject* operand)
    {
        if (operand != nullptr)
        {
            return &static_cast<AnyObject::Data<ValueType>*>(operand->pData)->value;
        }
        else
        {
            return nullptr;
        }
    }
    
    /**
     * Returns a const pointer to the internal data of the specified type.
     *
     * @note	Will return null if cast fails.
     */
    template <typename ValueType>
    const ValueType* any_cast(const AnyObject* operand)
    {
        return any_cast<ValueType>(const_cast<AnyObject*>(operand));
    }
    
    /**
     * Returns a copy of the internal data of the specified type.
     *
     * @note	Throws an exception if cast fails.
     */
    template <typename ValueType>
    ValueType any_cast(const AnyObject& operand)
    {
        return *any_cast<ValueType>(const_cast<AnyObject*>(&operand));
    }
    
    /**
     * Returns a copy of the internal data of the specified type.
     *
     * @note	Throws an exception if cast fails.
     */
    template <typename ValueType>
    ValueType any_cast(AnyObject& operand)
    {
        return *any_cast<ValueType>(&operand);
    }
    
    /**
     * Returns a reference to the internal data of the specified type.
     *
     * @note	Throws an exception if cast fails.
     */
    template <typename ValueType>
    const ValueType& any_cast_ref(const AnyObject & operand)
    {
        return *any_cast<ValueType>(const_cast<AnyObject*>(&operand));
    }
    
    /**
     * Returns a reference to the internal data of the specified type.
     *
     * @note	Throws an exception if cast fails.
     */
    template <typename ValueType>
    ValueType& any_cast_ref(AnyObject& operand)
    {
        return *any_cast<ValueType>(&operand);
    }
    
    /** Casts a type without performing any kind of checks. */
    template <typename ValueType>
    ValueType* any_cast_unsafe(AnyObject* operand)
    {
        return &static_cast<AnyObject::Data<ValueType>*>(operand->pData)->value;
    }
    
    /** Casts a type without performing any kind of checks. */
    template <typename ValueType>
    const ValueType* any_cast_unsafe(const AnyObject* operand)
    {
        return any_cast_unsafe<ValueType>(const_cast<AnyObject*>(operand));
    }
}
