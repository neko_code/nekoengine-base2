//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Date.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Utilities/Text.h"

namespace Neko
{
    namespace ETimespan
    {
        /** The number of timespan ticks per day. */
        const int64 TicksPerDay = 864000000000;
        
        /** The number of timespan ticks per hour. */
        const int64 TicksPerHour = 36000000000;
        
        /** The number of timespan ticks per microsecond. */
        const int64 TicksPerMicrosecond = 10;
        
        /** The number of timespan ticks per millisecond. */
        const int64 TicksPerMillisecond = 10000;
        
        /** The number of timespan ticks per minute. */
        const int64 TicksPerMinute = 600000000;
        
        /** The number of timespan ticks per second. */
        const int64 TicksPerSecond = 10000000;
        
        /** The number of timespan ticks per week. */
        const int64 TicksPerWeek = 6048000000000;
    }
    
    /**
     * Represents time span interval.
     */
    class NEKO_ENGINE_API CTimeValue
    {
    public:
        static const int64 TIMEVALUE_PRECISION = 100000; //!< One second.
        
    public:
        
        NEKO_FORCE_INLINE CTimeValue()
        : Ticks(0)
        {
            
        }
        
        NEKO_FORCE_INLINE CTimeValue(const float fSeconds)
        {
            SetSeconds(fSeconds);
        }
        
        NEKO_FORCE_INLINE CTimeValue(const double fSeconds)
        {
            SetSeconds(fSeconds);
        }
        
        /** 
         * @param inllValue Positive negative, absolute or relative in 1 second= TIMEVALUE_PRECISION units. 
         */
        NEKO_FORCE_INLINE CTimeValue(const int64 inllValue)
        {
            Ticks = inllValue;
        }
        
        /** Copy constructor. */
        NEKO_FORCE_INLINE CTimeValue(const CTimeValue& Other)
        {
            Ticks = Other.Ticks;
        }
        
        NEKO_FORCE_INLINE CTimeValue(int32 Hours, int32 Minutes, int32 Seconds)
        {
            Assign(0, Hours, Minutes, Seconds, 0, 0);
        }
        
        NEKO_FORCE_INLINE CTimeValue(int32 Days, int32 Hours, int32 Minutes, int32 Seconds)
        {
            Assign(Days, Hours, Minutes, Seconds, 0, 0);
        }
        
        NEKO_FORCE_INLINE ~CTimeValue()
        {
        }
        
        /**
         * Assignment operator.
         * @param Other Right side.
         */
        NEKO_FORCE_INLINE CTimeValue& operator = (const CTimeValue& Other)
        {
            Ticks = Other.Ticks;
            return *this;
        };

        /** @note Use only for relative value, absolute values suffer a lot from precision loss. */
        NEKO_FORCE_INLINE float GetSeconds() const
        {
            return Ticks * (1.0f / TIMEVALUE_PRECISION);
        }
        
        /**
         * Gets relative time difference in seconds.
         * Call this on the EndTime object: EndTime.GetDifferenceInSeconds(StartTime);
         */
        NEKO_FORCE_INLINE float GetDifferenceInSeconds(const CTimeValue& StartTime) const
        {
            return (Ticks - StartTime.Ticks) * (1.f / TIMEVALUE_PRECISION);
        }
        
        NEKO_FORCE_INLINE void SetSeconds(const float infSec)
        {
            Ticks = (int64)(infSec * TIMEVALUE_PRECISION);
        }
        
        NEKO_FORCE_INLINE void SetSeconds(const double infSec)
        {
            Ticks = (int64)(infSec * TIMEVALUE_PRECISION);
        }
        
        NEKO_FORCE_INLINE void SetSeconds(const int64 indwSec)
        {
            Ticks = indwSec * TIMEVALUE_PRECISION;
        }
        
        NEKO_FORCE_INLINE void SetMilliSeconds(const int64 indwMilliSec)
        {
            Ticks = indwMilliSec * (TIMEVALUE_PRECISION / 1000);
        }
        
        /** @note Use only for relative value, absolute values suffer a lot from precision loss. */
        NEKO_FORCE_INLINE float GetMilliSeconds() const
        {
            return Ticks * (1000.0f / TIMEVALUE_PRECISION);
        }
        
        NEKO_FORCE_INLINE int64 GetMilliSecondsAsInt64() const
        {
            return Ticks * 1000 / TIMEVALUE_PRECISION;
        }
        
        NEKO_FORCE_INLINE int64 GetMicroSecondsAsInt64() const
        {
            return Ticks * (1000 * 1000) / TIMEVALUE_PRECISION;
        }
        
        NEKO_FORCE_INLINE int64 GetValue() const
        {
            return Ticks;
        }
        
        NEKO_FORCE_INLINE void SetValue(int64 NewValue)
        {
            Ticks = NewValue;
        }
        
        /**
         * Useful for periodic events (e.g. water wave, blinking).
         * Changing TimePeriod can results in heavy changes in the returned value.
         * @return [0..1]
         */
        float GetPeriodicFraction(const CTimeValue TimePeriod) const
        {
            // todo: change float implement to int64 for more precision
            float fAbs = GetSeconds() / TimePeriod.GetSeconds();
            return fAbs - (int)(fAbs);
        }
        
        // Math operations
        
        //! Binary subtraction.
        NEKO_FORCE_INLINE CTimeValue operator-(const CTimeValue& Other) const
        {
            CTimeValue ret;
            ret.Ticks = Ticks - Other.Ticks;
            return ret;
        };
        
        //! Binary addition.
        NEKO_FORCE_INLINE CTimeValue operator+(const CTimeValue& Other) const
        {
            CTimeValue ret;
            ret.Ticks = Ticks + Other.Ticks;
            return ret;
        };
        
        //! Sign inversion.
        NEKO_FORCE_INLINE CTimeValue  operator-() const
        {
            CTimeValue ret;
            ret.Ticks = -Ticks;
            return ret;
        };
        
        NEKO_FORCE_INLINE CTimeValue& operator += (const CTimeValue& Other)
        {
            Ticks += Other.Ticks;
            return *this;
        }
        NEKO_FORCE_INLINE CTimeValue& operator -= (const CTimeValue& Other)
        {
            Ticks -= Other.Ticks;
            return *this;
        }
        
        NEKO_FORCE_INLINE CTimeValue& operator /= (int Other)
        {
            Ticks /= Other;
            return *this;
        }
        
        // Comparison
        
        NEKO_FORCE_INLINE bool operator < (const CTimeValue& Other) const
        {
            return Ticks < Other.Ticks;
        };
        NEKO_FORCE_INLINE bool operator > (const CTimeValue& Other) const
        {
            return Ticks > Other.Ticks;
        };
        NEKO_FORCE_INLINE bool operator >= (const CTimeValue& Other) const
        {
            return Ticks >= Other.Ticks;
        };
        NEKO_FORCE_INLINE bool operator <= (const CTimeValue& Other) const
        {
            return Ticks <= Other.Ticks;
        };
        NEKO_FORCE_INLINE bool operator == (const CTimeValue& Other) const
        {
            return Ticks == Other.Ticks;
        };
        NEKO_FORCE_INLINE bool operator != (const CTimeValue& Other) const
        {
            return Ticks != Other.Ticks;
        };
        
    private:
        
        void Assign(int32 Days, int32 Hours, int32 Minutes, int32 Seconds, int32 Milliseconds, int32 Microseconds)
        {
            int64 TotalTicks = ETimespan::TicksPerMicrosecond * (1000 * (1000 * (60 * 60 * 24 * (int64)Days + 60 * 60 * (int64)Hours + 60 * (int64)Minutes + (int64)Seconds) + (int64)Milliseconds) + (int64)Microseconds);
            Ticks = TotalTicks;
        }
        
        
        //! Absolute or relative value in 1/TIMEVALUE_PRECISION, might be negative.
        int64 Ticks;
        
        friend class CTimer;
    };
    
    /** Represents world date time. */
    class NEKO_ENGINE_API CDateTime
    {
    public:
        
        CDateTime()
        {
        }
        
        
        CDateTime(int64 InTicks)
        : Ticks(InTicks)
        {

        }
        
        
        CDateTime(int32 Year, int32 Month, int32 Day, int32 Hour = 0, int32 Minute = 0, int32 Second = 0, int32 Millisecond = 0);
        
        
        CDateTime operator + (const CTimeValue& Other) const
        {
            return CDateTime(Ticks + Other.GetValue());
        }
        
        
        static bool IsLeapYear(int32 Year);
        
        static CDateTime UtcNow();
        
        /**
         * Converts current date time to string with formatting.
         * Available format fields: %d, %D, %m, %y, %Y, %h, %H, %M, %S, %s
         */
        Text ToString(const char* Format);
        
        double GetJulianDay() const
        {
            return (double)(1721425.5 + Ticks / ETimespan::TicksPerDay);
        }
        
        void GetDate(int32& OutYear, int32& OutMonth, int32& OutDay) const;
        
        int32 GetDay() const;
        int32 GetMonth() const;
        int32 GetYear() const;
        
        /**
         * Gets this date's hour part in 24-hour clock format (0 to 23).
         */
        NEKO_FORCE_INLINE int32 GetHour() const
        {
            return (int32)((Ticks / ETimespan::TicksPerHour) % 24);
        }
        
        int32 GetHour12() const;
        
        /**
         * Gets this date's minute part (0 to 59).
         */
        NEKO_FORCE_INLINE int32 GetMinute() const
        {
            return (int32)((Ticks / ETimespan::TicksPerMinute) % 60);
        }
        
        /**
         * Gets this date's second part.
         */
        NEKO_FORCE_INLINE int32 GetSecond() const
        {
            return (int32)((Ticks / ETimespan::TicksPerSecond) % 60);
        }
        
        /**
         * Gets this date's millisecond part (0 to 999).
         */
        NEKO_FORCE_INLINE int32 GetMillisecond() const
        {
            return (int32)((Ticks / ETimespan::TicksPerMillisecond) % 1000);
        }
        
        static int32 DaysInMonth(int32 Year, int32 Month);
        
        static int32 DaysInYear(int32 Year);
        
        int32 GetDayOfYear() const;
        
        /**
         * Returns the maximum date value.
         *
         * The maximum date value is December 31, 9999, 23:59:59.9999999.
         *
         * @see MinValue
         */
        static CDateTime MaxValue()
        {
            return CDateTime(3652059 * ETimespan::TicksPerDay - 1);
        }
        
        /**
         * Returns the minimum date value.
         *
         * The minimum date value is January 1, 0001, 00:00:00.0.
         *
         * @see MaxValue
         */
        static CDateTime MinValue()
        {
            return CDateTime(0);
        }
        
        
    public:

        /**
         * Returns time span between this date and the given date.
         *
         * @return A time span whose value is the difference of this date and the given date.
         * @see FTimespan
         */
        CTimeValue operator-( const CDateTime& Other ) const
        {
            return CTimeValue(Ticks - Other.Ticks);
        }
        
        /**
         * Returns result of subtracting the given time span from this date.
         *
         * @return A date whose value is the difference of this date and the given time span.
         * @see CTimeValue
         */
        CDateTime operator-( const CTimeValue& Other ) const
        {
            return CDateTime(Ticks - Other.GetValue());
        }
        
        /**
         * Subtracts the given time span from this date.
         *
         * @return This date.
         * @see CTimeValue
         */
        CDateTime& operator-=( const CTimeValue& Other )
        {
            Ticks -= Other.GetValue();
            
            return *this;
        }
        
        /**
         * Compares this date with the given date for equality.
         *
         * @param other The date to compare with.
         * @return true if the dates are equal, false otherwise.
         */
        bool operator==( const CDateTime& Other ) const
        {
            return (Ticks == Other.Ticks);
        }
        
        /**
         * Compares this date with the given date for inequality.
         *
         * @param other The date to compare with.
         * @return true if the dates are not equal, false otherwise.
         */
        bool operator!=( const CDateTime& Other ) const
        {
            return (Ticks != Other.Ticks);
        }
        
        /**
         * Checks whether this date is greater than the given date.
         *
         * @param other The date to compare with.
         * @return true if this date is greater, false otherwise.
         */
        bool operator>( const CDateTime& Other ) const
        {
            return (Ticks > Other.Ticks);
        }
        
        /**
         * Checks whether this date is greater than or equal to the date span.
         *
         * @param other The date to compare with.
         * @return true if this date is greater or equal, false otherwise.
         */
        bool operator>=( const CDateTime& Other ) const
        {
            return (Ticks >= Other.Ticks);
        }
        
        /**
         * Checks whether this date is less than the given date.
         *
         * @param other The date to compare with.
         * @return true if this date is less, false otherwise.
         */
        bool operator<( const CDateTime& Other ) const
        {
            return (Ticks < Other.Ticks);
        }
        
        /**
         * Checks whether this date is less than or equal to the given date.
         *
         * @param other The date to compare with.
         * @return true if this date is less or equal, false otherwise.
         */
        bool operator<=( const CDateTime& Other ) const
        {
            return (Ticks <= Other.Ticks);
        }

        
    private:
        
        int64 Ticks;
    };

} // !namespace Neko
