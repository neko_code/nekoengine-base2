//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Timer.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Utilities/Date.h"

namespace Neko
{
    enum ETimer
    {
        ETIMER_GAME = 0, //!< Pausable, serialized, frametime is smoothed/scaled/clamped.
        ETIMER_UI,       //!< Non-pausable, non-serialized, frametime unprocessed.
        ETIMER_LAST
    };
    
    enum ETimeScaleChannels
    {
        eTSC_Trackview = 0,
        eTSC_GameStart
    };
    
    class InputBlob;
    class OutputBlob;
    
    /// Implements all common timing routines.
    class NEKO_ENGINE_API CTimer
    {
    public:
        
        CTimer();
        
        ~CTimer()
        {
            
        };
        
        void ResetTimer();
        
        void UpdateOnFrameStart();
        
        float GetCurrTime(ETimer which = ETIMER_GAME) const;
        
        CTimeValue GetAsyncTime() const;
        
        float GetAsyncCurTime();
        
        float GetFrameTime(ETimer which = ETIMER_GAME) const;
        
        float GetRealFrameTime() const;
        
        float GetTimeScale() const;
        
        float GetTimeScale(uint32 channel) const;
        
        void SetTimeScale(float scale, uint32 channel = 0);
        
        void ClearTimeScales();
        
        float GetFrameRate();
        
        float GetProfileFrameBlending(float* pfBlendTime = 0, int* piBlendMode = 0);
        
        
        
        void EnableTimer(const bool bEnable)
        {
            bEnabled = bEnable;
        }
        
        bool IsTimerEnabled() const
        {
            return bEnabled;
        }
        
        
        
        /**
         * Tries to pause/unpause a timer
         * @returns true if successfully paused/unpaused, false otherwise
         */
        bool PauseTimer(ETimer which, bool bPause);
        
        /**
         * Determines if a timer is paused
         * @returns true if paused, false otherwise
         */
        bool IsTimerPaused(ETimer which);
        
        /**
         * Tries to set a timer
         * @return true if successful, false otherwise
         */
        bool SetTimer(ETimer which, float timeInSeconds);
        
        /** Makes a tm struct from a time_t in UTC (like gmtime) */
        void SecondsToDateUTC(time_t time, struct tm& outDateUTC);
        
        /** Makes a UTC time from a tm (like timegm, but not available on all platforms) */
        time_t DateToSecondsUTC(struct tm& timePtr);
        
        /** Converts from Ticks to Seconds */
        NEKO_FORCE_INLINE float TicksToSeconds(int64 ticks) const
        {
            return float((double)ticks * fSecsPerTick);
        }
        
        /** Gets number of ticks per second */
        NEKO_FORCE_INLINE int64 GetTicksPerSecond()
        {
            return TicksPerSec;
        }
        
        void SetFixedFrameTime(float time)
        {
            fFixedTimeStep = time;
        }
        
        const CTimeValue& GetFrameStartTime(ETimer which = ETIMER_GAME) const
        {
            return CurrentTime[(int)which];
        }
        
        void Serialize(OutputBlob& Blob);
        void Deserialize(InputBlob& Blob);
        
    private:
        
        // updates CurrentTime (either pass m_lCurrentTime or custom curTime)
        void  RefreshGameTime(int64 curTime);
        void  RefreshUITime(int64 curTime);
        void  UpdateBlending();
        
        /**
         * Average frame-times to avoid stalls and peaks in framerate
         * @note that is is time-base averaging and not frame-based
         */
        float GetAverageFrameTime();
        
        /**
         * Updates the game-time offset to match the the specified time.
         * @param Ticks  New value of ticks since the last Reset().
         */
        void SetOffsetToMatchGameTime(int64 Ticks);
        
        /**
         * Convert seconds to ticks using the timer frequency.
         * @note Loss of precision may occur, especially if magnitude of argument or timer frequency is large.
         */
        int64 SecondsToTicks(double seconds) const;
        
        enum
        {
            MAX_FRAME_AVERAGE       = 100,
            NUM_TIME_SCALE_CHANNELS = 8,
        };
        
        
        // Dynamic state, reset by ResetTimer()
        
        //! Time since last Reset(), cached during Update()
        CTimeValue CurrentTime[ETIMER_LAST];
        
        //! Ticks elapsed since system boot, all other tick-unit variables are relative to this.
        int64      BaseTime;
        //! Ticks since last Reset(). This is the base for UI time. UI time is monotonic, it always moves forward at a constant rate until the timer is Reset()).
        int64      LastTime;
        //! Additional ticks for Game time (relative to UI time). Game time can be affected by loading, pausing, time smoothing and time clamping, as well as SetTimer().
        int64      OffsetTime;
        
        //! In seconds since the last Update(), clamped/smoothed etc.
        float      fFrameTime;
        //! In real seconds since the last Update(), non-clamped/un-smoothed etc.
        float      fRealFrameTime;
        
        //! In seconds, sum of all frames used in replication.
        float fReplicationTime;
        
        //! Set if the game is paused. GetFrameTime() will return 0, GetCurrTime(ETIMER_GAME) will not progress.
        bool       bGameTimerPaused;
        //! The UI time when the game timer was paused. On un-pause, offset will be adjusted to match.
        int64      GameTimerPausedTime;
    
        
        // Persistant state, kept by ResetTimer()
        
        
        bool bEnabled;
        unsigned int FrameCounter;
        
        int64        TicksPerSec; // Ticks per second
        double       fSecsPerTick; // Seconds per tick
        
        // smoothing
        float fFrameTimes[MAX_FRAME_AVERAGE];
        //! Used for smoothing (AverageFrameTime())
        float fAverageFrameTime;
        
        //! Used for blend weighting (UpdateBlending())
        float fAvgFrameTime;
        //! Current blending amount for profile.
        float fProfileBlend;
        //! Smoothing interval (up to fProfileSmoothTime).
        float fSmoothTime;
        
        // time scale
        float fTimeScaleChannels[NUM_TIME_SCALE_CHANNELS];
        float fTotalTimeScale;
        
        // Console vars, always have default value on secondary CTimer instances
        // these are customizable, but always must initialize to the default values
        float fFixedTimeStep; // in seconds
        float fMaxTimeStep; // in seconds
        int eTimeDebug; // 0 - nothing, 1 - verbose events, 2 - very verbose events
        int bTimeSmoothing;
        float fCvarTimescale;
        
        // Profile averaging help.
        
        //! Seconds to exponentially smooth profile results.
        float fProfileSmoothTime;
        //! Weighting mode (see RegisterVar desc).
        int   ProfileWeighting;
    };
} // !namespace Neko
