#include "BinaryArray.h"
#include "IAllocator.h"
#include "StringUtil.h"

namespace Neko
{
    static const int BITMASK_7BIT = sizeof(int32) * 8 - 1;
    
    BinaryArray::StoreType BinaryArray::BINARY_MASK[32];
    BinaryArray::StoreType BinaryArray::INDEX_BIT[32];
    
    BinaryArray::Accessor::Accessor(BinaryArray& array, int index)
    : Array(array)
    , Index(index)
    {
    }
    
    BinaryArray::BinaryArray(IAllocator& allocator)
    : Allocator(allocator)
    , Data(nullptr)
    , Size(0)
    , Capacity(0)
    {
        static bool init = false;
        if (!init)
        {
            init = true;
            for (int i = BITMASK_7BIT; i >= 0; --i)
            {
                BINARY_MASK[i] = i == BITMASK_7BIT ? 0xffffFFFF : BINARY_MASK[i + 1]
                << 1;
                INDEX_BIT[i] = 1 << (BITMASK_7BIT - i);
            }
        }
    }
    
    BinaryArray::~BinaryArray()
    {
        Allocator.Deallocate(Data);
    }
    
    void BinaryArray::SetAllZeros()
    {
        SetMemory(Data, 0, Capacity >> 3);
    }
    
    void BinaryArray::EraseFast(int index)
    {
        this->operator[](index) = back();
        Pop();
    }
    
    void BinaryArray::Erase(int index)
    {
        if (0 <= index && index < Size)
        {
            int major = index >> 5;
            const int last_major = (Size - 1) >> 5;
            Data[major] = ((index & BITMASK_7BIT) == 0 ? 0
                           : (Data[major] & BINARY_MASK[(index & BITMASK_7BIT) - 1])) | ((Data[major] & ~BINARY_MASK[index & BITMASK_7BIT]) << 1);
            if (major < last_major)
            {
                Data[major] |= (Data[major + 1] & 0x80000000) >> BITMASK_7BIT;
            }
            for (int i = major + 1; i <= last_major; ++i)
            {
                Data[i] <<= 1;
                if (i < (Size >> 5))
                {
                    Data[i] |= (Data[i + 1] & 0x80000000) >> BITMASK_7BIT;
                }
            }
            --Size;
        }
    }
    
    void BinaryArray::Clear()
    {
        Size = 0;
    }
    
    void BinaryArray::Push(bool value)
    {
        if (Capacity == Size)
        {
            grow(Capacity > 0 ? (Capacity << 1) : 32);
        }
        if (value)
        {
            Data[Size >> 5] |= INDEX_BIT[Size & BITMASK_7BIT];
        }
        else
        {
            Data[Size >> 5] &= ~INDEX_BIT[Size & BITMASK_7BIT];
        }
        ++Size;
    }
    
    void BinaryArray::Pop()
    {
        assert(Size > 0);
        --Size;
    }
    
    bool BinaryArray::operator[](int index) const
    {
        assert(index < Size);
        return (Data[index >> 5] & INDEX_BIT[index & BITMASK_7BIT]) > 0;
    }
    
    BinaryArray::Accessor BinaryArray::operator[](int index)
    {
        return Accessor(*this, index);
    }
    
    bool BinaryArray::back() const
    {
        assert(Size > 0);
        return (*this)[Size - 1];
    }
    
    BinaryArray::Accessor BinaryArray::back()
    {
        assert(Size > 0);
        return (*this)[Size - 1];
    }
    
    int BinaryArray::GetSize() const
    {
        return Size;
    }
    
    void BinaryArray::Reserve(int capacity)
    {
        if (((Capacity + BITMASK_7BIT) >> 5) < ((capacity + BITMASK_7BIT) >> 5))
        {
            grow(capacity);
        }
    }
    
    void BinaryArray::Resize(int size)
    {
        Reserve((size + BITMASK_7BIT) & ~BITMASK_7BIT);
        Size = size;
    }
    
    BinaryArray::StoreType* BinaryArray::GetRaw()
    {
        return Data;
    }
    
    int BinaryArray::GetRawSize() const
    {
        return (Size + BITMASK_7BIT) >> 5;
    }
    
    void BinaryArray::grow(int capacity)
    {
        StoreType* new_data = static_cast<StoreType*>(Allocator.Allocate(capacity >> 3));
        if (Data)
        {
            CopyMemory(new_data, Data, sizeof(StoreType) * (Size + BITMASK_7BIT) >> 5);
            Allocator.Deallocate(Data);
        }
        Data = new_data;
        Capacity = capacity;
    }
} // Neko
