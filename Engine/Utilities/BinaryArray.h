#pragma once

#include "../Neko.h"

namespace Neko
{
    class IAllocator;
    
    class NEKO_ENGINE_API BinaryArray
    {
    public:
        
        typedef uint32 StoreType;
        static const size_t ITEM_SIZE = sizeof(StoreType);
        
        class Accessor
        {
        public:
            
            Accessor(BinaryArray& array, int index);
            
            Accessor(const Accessor& rhs) : Array(rhs.Array), Index(rhs.Index) {}
            
            Accessor& operator =(const Accessor& value)
            {
                return *this = (bool)value;
            }
            
            Accessor& operator =(bool value)
            {
                if (value)
                {
                    Array.Data[Index >> 5] |= Array.INDEX_BIT[Index & 31];
                }
                else
                {
                    Array.Data[Index >> 5] &= ~Array.INDEX_BIT[Index & 31];
                }
                return *this;
            }
            
            
            operator bool() const
            {
                return (Array.Data[Index >> 5] & Array.INDEX_BIT[Index & 31]) > 0;
            }
            
        private:
            
            BinaryArray& Array;
            int Index;
        };
        
    public:
        
        explicit BinaryArray(IAllocator& allocator);
        ~BinaryArray();
        
        Accessor operator[](int index);
        Accessor back();
        bool back() const;
        bool operator[](int index) const;
        void Reserve(int capacity);
        void Resize(int capacity);
        void Erase(int index);
        void EraseFast(int index);
        void Clear();
        void Push(bool value);
        void Pop();
        void SetAllZeros();
        int GetSize() const;
        int GetRawSize() const;
        StoreType* GetRaw();
        
    private:
        
        void grow(int capacity);
        BinaryArray(const BinaryArray& rhs);
        void operator =(const BinaryArray& rhs);
        
    private:
        
        static StoreType BINARY_MASK[sizeof(StoreType) << 3];
        static StoreType INDEX_BIT[sizeof(StoreType) << 3];
        IAllocator& Allocator;
        StoreType* Data;
        int Size;
        int Capacity;
    };
    
} // namespace Neko

