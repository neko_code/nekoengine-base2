//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  StringUtil.cpp
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#include "StringUtil.h"
#include <cstring>

#include "../Math/MathUtils.h"

#include "Text.h"

namespace Neko
{
    static char MakeLowercase(char c)
    {
        return c >= 'A' && c <= 'Z' ? c - ('A' - 'a') : c;
    }
    
    int CompareStringN(const char* lhs, const char* rhs, int Length)
    {
        return strncmp(lhs, rhs, Length);
    }
    
    int CompareIStringN(const char* lhs, const char* rhs, int Length)
    {
#if NEKO_WINDOWS_FAMILY
        return strnicmp(lhs, rhs, Length);
#else
        return strncasecmp(lhs, rhs, Length);
#endif
    }
    
    int CompareIString(const char* lhs, const char* rhs)
    {
#if NEKO_WINDOWS_FAMILY
        return stricmp(lhs, rhs);
#else
        return strcasecmp(lhs, rhs);
#endif
    }
    
    int CompareString(const char* lhs, const char* rhs)
    {
        return strcmp(lhs, rhs);
    }
    
    bool EqualStrings(const char* lhs, const char* rhs)
    {
        return strcmp(lhs, rhs) == 0;
    }
    
    bool EqualIStrings(const char* lhs, const char* rhs)
    {
#if NEKO_UNIX_FAMILY
        return strcasecmp(lhs, rhs) == 0;
#else
        return stricmp(lhs, rhs) == 0;
#endif
    }

    int StringLength(const char* str)
    {
        return (int)strlen(str);
    }
    
    
    void MoveMemory(void* dest, const void* src, size_t Count)
    {
        memmove(dest, src, Count);
    }
    
    void SetMemory(void* ptr, uint8 value, size_t Num)
    {
        memset(ptr, value, Num);
    }
    
    void CopyMemory(void* dest, const void* src, size_t Count)
    {
        memcpy(dest, src, Count);
    }
    
    int CompareMemory(const void* lhs, const void* rhs, size_t Size)
    {
        return memcmp(lhs, rhs, Size);
    }
    
    
    bool EndsWith(const char* String, const char* Substring)
    {
        int len = StringLength(String);
        int len2 = StringLength(Substring);
        if (len2 > len)
        {
            return false;
        }
        return EqualStrings(String + len - len2, Substring);
    }
    
    const char* FindISubstring(const char* haystack, const char* needle)
    {
        const char* c = haystack;
        while (*c)
        {
            if (MakeLowercase(*c) == MakeLowercase(needle[0]))
            {
                const char* n = needle + 1;
                const char* c2 = c + 1;
                while (*n && *c2)
                {
                    if (MakeLowercase(*n) != MakeLowercase(*c2)) break;
                    ++n;
                    ++c2;
                }
                if (*n == 0) return c;
            }
            ++c;
        }
        return nullptr;
    }
    
    const char* FindSubstring(const char* haystack, const char* needle)
    {
        return strstr(haystack, needle);
    }
    
    bool MakeLowercase(char* destination, int length, const char* source)
    {
        if (!source)
        {
            return false;
        }
        
        while (*source && length)
        {
            *destination = MakeLowercase(*source);
            --length;
            ++destination;
            ++source;
        }
        if (length > 0)
        {
            *destination = 0;
            return true;
        }
        return false;
    }
    
    bool CopyNString(char* destination, int length, const char* source, int source_len)
    {
        if (!source)
        {
            return false;
        }
        
        while (*source && length > 1 && source_len)
        {
            *destination = *source;
            --source_len;
            --length;
            ++destination;
            ++source;
        }
        if (length > 0)
        {
            *destination = 0;
            return *source == '\0' || source_len == 0;
        }
        return false;
    }
    
    bool CopyString(char* destination, int length, const char* source)
    {
        if (!source || length < 1)
        {
            return false;
        }
        
        while (*source && length > 1)
        {
            *destination = *source;
            --length;
            ++destination;
            ++source;
        }
        *destination = 0;
        return *source == '\0';
    }
    
    bool CopyStringClamp(char* const Dest, size_t const DestSize, const char* const src, size_t const SrcSize)
    {
        if (!Dest || DestSize < sizeof(char))
        {
            return false;
        }
        
        if (!src || SrcSize < sizeof(char))
        {
            Dest[0] = 0;
            return src != 0;    // we return true for non-null src without characters
        }
        
        const size_t src_n = SrcSize;
        const size_t n = Math::Minimum(DestSize - 1, src_n);
        
        for (size_t i = 0; i < n; ++i)
        {
            Dest[i] = src[i];
            if (!src[i])
            {
                return true;
            }
        }
        
        Dest[n] = 0;
        return n >= src_n || src[n] == 0;
    }
    
    const char* ReverseFind(const char* begin_haystack, const char* end_haystack, char c)
    {
        const char* tmp = end_haystack == nullptr ? nullptr : end_haystack - 1;
        if (tmp == nullptr)
        {
            tmp = begin_haystack;
            while (*tmp)
            {
                ++tmp;
            }
        }
        while (tmp >= begin_haystack && *tmp != c)
        {
            --tmp;
        }
        if (tmp >= begin_haystack)
        {
            return tmp;
        }
        return nullptr;
    }
    
    
    bool CatNString(char* destination, int length, const char* source, int source_len)
    {
        while (*destination && length)
        {
            --length;
            ++destination;
        }
        return CopyNString(destination, length, source, source_len);
    }
    
    
    bool CatString(char* destination, int length, const char* source)
    {
        while (*destination && length)
        {
            --length;
            ++destination;
        }
        return CopyString(destination, length, source);
    }
    
    
    static void ReverseString(char* str, int length)
    {
        char* beg = str;
        char* end = str + length - 1;
        while (beg < end)
        {
            char tmp = *beg;
            *beg = *end;
            *end = tmp;
            ++beg;
            --end;
        }
    }
    
    
    const char* FromCString(const char* input, int length, uint16* value)
    {
        uint32 tmp;
        const char* ret = FromCString(input, length, &tmp);
        *value = uint16(tmp);
        return ret;
    }
    
    
    const char* FromCString(const char* input, int length, int32* value)
    {
        int64 val;
        const char* ret = FromCString(input, length, &val);
        *value = (int32)val;
        return ret;
    }
    
    
    const char* FromCString(const char* input, int length, int64* value)
    {
        if (length > 0)
        {
            const char* c = input;
            *value = 0;
            if (*c == '-')
            {
                ++c;
                --length;
                if (!length)
                {
                    return nullptr;
                }
            }
            while (length && *c >= '0' && *c <= '9')
            {
                *value *= 10;
                *value += *c - '0';
                ++c;
                --length;
            }
            if (input[0] == '-')
            {
                *value = -*value;
            }
            return c;
        }
        return nullptr;
    }
    
    
    const char* FromCString(const char* input, int length, uint32* value)
    {
        if (length > 0)
        {
            const char* c = input;
            *value = 0;
            if (*c == '-')
            {
                return nullptr;
            }
            while (length && *c >= '0' && *c <= '9')
            {
                *value *= 10;
                *value += *c - '0';
                ++c;
                --length;
            }
            return c;
        }
        return nullptr;
    }
    
    
    const char* FromCString(const char* input, int length, uint64* value)
    {
        if (length > 0)
        {
            const char* c = input;
            *value = 0;
            if (*c == '-')
            {
                return nullptr;
            }
            while (length && *c >= '0' && *c <= '9')
            {
                *value *= 10;
                *value += *c - '0';
                ++c;
                --length;
            }
            return c;
        }
        return nullptr;
    }
    
    
    bool ToCStringPretty(int32 value, char* output, int length)
    {
        char* c = output;
        if (length > 0)
        {
            if (value < 0)
            {
                value = -value;
                --length;
                *c = '-';
                ++c;
            }
            return ToCStringPretty((unsigned int)value, c, length);
        }
        return false;
    }
    
    
    bool ToCStringPretty(uint32 value, char* output, int length)
    {
        return ToCStringPretty(uint64(value), output, length);
    }
    
    
    bool ToCStringPretty(uint64 value, char* output, int length)
    {
        char* c = output;
        char* num_start = output;
        if (length > 0)
        {
            if (value == 0)
            {
                if (length == 1)
                {
                    return false;
                }
                *c = '0';
                *(c + 1) = 0;
                return true;
            }
            int counter = 0;
            while (value > 0 && length > 1)
            {
                *c = value % 10 + '0';
                value = value / 10;
                --length;
                ++c;
                if ((counter + 1) % 3 == 0 && length > 1 && value > 0)
                {
                    *c = ' ';
                    ++c;
                    counter = 0;
                }
                else
                {
                    ++counter;
                }
            }
            if (length > 0)
            {
                ReverseString(num_start, (int)(c - num_start));
                *c = 0;
                return true;
            }
        }
        return false;
    }
    
    
    bool ToCString(int32 value, char* output, int length)
    {
        char* c = output;
        if (length > 0)
        {
            if (value < 0)
            {
                value = -value;
                --length;
                *c = '-';
                ++c;
            }
            return ToCString((unsigned int)value, c, length);
        }
        return false;
    }
    
    
    bool ToCString(int64 value, char* output, int length)
    {
        char* c = output;
        if (length > 0)
        {
            if (value < 0)
            {
                value = -value;
                --length;
                *c = '-';
                ++c;
            }
            return ToCString((uint64)value, c, length);
        }
        return false;
    }
    
    
    bool ToCString(uint64 value, char* output, int length)
    {
        char* c = output;
        char* num_start = output;
        if (length > 0)
        {
            if (value == 0)
            {
                if (length == 1)
                {
                    return false;
                }
                *c = '0';
                *(c + 1) = 0;
                return true;
            }
            while (value > 0 && length > 1)
            {
                *c = value % 10 + '0';
                value = value / 10;
                --length;
                ++c;
            }
            if (length > 0)
            {
                ReverseString(num_start, (int)(c - num_start));
                *c = 0;
                return true;
            }
        }
        return false;
    }
    
    
    bool ToCStringHex(uint8 value, char* output, int length)
    {
        if (length < 2)
        {
            return false;
        }
        uint8 first = value / 16;
        if (first > 9)
        {
            output[0] = 'A' + first - 10;
        }
        else
        {
            output[0] = '0' + first;
        }
        uint8 second = value % 16;
        if (second > 9)
        {
            output[1] = 'A' + second - 10;
        }
        else
        {
            output[1] = '0' + second;
        }
        return true;
    }
    
    
    bool ToCString(uint32 value, char* output, int length)
    {
        char* c = output;
        char* num_start = output;
        if (length > 0)
        {
            if (value == 0)
            {
                if (length == 1)
                {
                    return false;
                }
                *c = '0';
                *(c + 1) = 0;
                return true;
            }
            while (value > 0 && length > 1)
            {
                *c = value % 10 + '0';
                value = value / 10;
                --length;
                ++c;
            }
            if (length > 0)
            {
                ReverseString(num_start, (int)(c - num_start));
                *c = 0;
                return true;
            }
        }
        return false;
    }
    
    
    static bool increment(char* output, char* end, bool is_space_after)
    {
        char carry = 1;
        char* c = end;
        while (c >= output)
        {
            if (*c == '.')
            {
                --c;
            }
            *c += carry;
            if (*c > '9')
            {
                *c = '0';
                carry = 1;
            }
            else
            {
                carry = 0;
                break;
            }
            --c;
        }
        if (carry && is_space_after)
        {
            char* c = end + 1; // including '\0' at the end of the string
            while (c >= output)
            {
                *(c + 1) = *c;
                --c;
            }
            ++c;
            *c = '1';
            return true;
        }
        return !carry;
    }
    
    
    bool ToCString(float value, char* output, int length, int after_point)
    {
        if (length < 2)
        {
            return false;
        }
        if (value < 0)
        {
            *output = '-';
            ++output;
            value = -value;
            --length;
        }
        // int part
        int exponent = value == 0 ? 0 : (int)log10(value);
        float num = value;
        char* c = output;
        if (num  < 1 && num > -1 && length > 1)
        {
            *c = '0';
            ++c;
            --length;
        }
        else
        {
            while ((num >= 1 || exponent >= 0) && length > 1)
            {
                float power = (float)pow(10, exponent);
                char digit = (char)floor(num / power);
                num -= digit * power;
                *c = digit + '0';
                --exponent;
                --length;
                ++c;
            }
        }
        // decimal part
        float dec_part = num;
        if (length > 1 && after_point > 0)
        {
            *c = '.';
            ++c;
            --length;
        }
        else if (length > 0 && after_point == 0)
        {
            *c = 0;
            return true;
        }
        else
        {
            return false;
        }
        while (length > 1 && after_point > 0)
        {
            dec_part *= 10;
            char tmp = (char)dec_part;
            *c = tmp + '0';
            dec_part -= tmp;
            ++c;
            --length;
            --after_point;
        }
        *c = 0;
        if ((int)(dec_part + 0.5f))
            increment(output, c - 1, length > 1);
        return true;
    }
    
    
    char* Trimmed(char* str)
    {
        while (*str && (*str == '\t' || *str == ' '))
        {
            ++str;
        }
        return str;
    }
    
    
    bool StartsWith(const char* str, const char* prefix)
    {
        const char* lhs = str;
        const char* rhs = prefix;
        while (*rhs && *lhs && *lhs == *rhs)
        {
            ++lhs;
            ++rhs;
        }
        
        return *rhs == 0;
    }
    
    
    int32 CountChar(const char* String, const char Symbol)
    {
        int32 Count;
        
        for (Count = 0; *String; String++)
        {
            if (*String == Symbol)
            {
                Count++;
            }
        }
        
        return Count;
    }
    
    
    const char* FloatArrayToString(const float* array, const int length, const int precision)
    {
        static int index = 0;
        static char str[4][16384];	// in case called by nested functions
        int i, n;
        char format[16], *s;
        
        // use an array of string so that multiple calls won't collide
        s = str[ index ];
        index = (index + 1) & 3;
        
        snprintf( format, sizeof( format ), "%%.%df", precision );
        n = snprintf( s, sizeof( str[0] ), format, array[0] );
        if (precision > 0)
        {
            while( n > 0 && s[n-1] == '0' )
            {
                s[--n] = '\0';
            }
            while( n > 0 && s[n-1] == '.' )
            {
                s[--n] = '\0';
            }
        }
        snprintf( format, sizeof( format ), " %%.%df", precision );
        for ( i = 1; i < length; i++ )
        {
            n += snprintf( s + n, sizeof( str[0] ) - n, format, array[i] );
            if ( precision > 0 )
            {
                while( n > 0 && s[n-1] == '0' )
                {
                    s[--n] = '\0';
                }
                while( n > 0 && s[n-1] == '.' )
                {
                    s[--n] = '\0';
                }
            }
        }
        return s;
    }
    
    
    int BestUnit(char* Out, float Value)
    {
        static const char* units[2][4] =
        {
            { "B", "KB", "MB", "GB" },
            { "B/s", "KB/s", "MB/s", "GB/s" }
        };
        
        int unit = 1;
        while (unit <= 3 && (1 << (unit * 10) < Value))
        {
            unit++;
        }
        unit--;
        Value /= 1 << (unit * 10);
        sprintf(Out, "%.2f %s", Value, units[0][unit]);
        return unit;
    }
    
    
    Byte Nibble(char c)
    {
        if ( (c >= '0') && (c <= '9') )
        {
            return (Byte)(c - '0');
        }
        
        if ( (c >= 'A') && (c <= 'F') )
        {
            return (Byte)(c - 'A' + 0x0a);
        }
        
        if ( (c >= 'a') && (c <= 'f') )
        {
            return (Byte)(c - 'a' + 0x0a);
        }
        
        return 0;
    }
} // ~namespace Neko
