#include "Text.h"
#include "StringUtil.h"

#include <string.h>

namespace Neko
{
    IAllocator& GetDefaultStringAllocator()
    {
        static DefaultAllocator GDefaultAllocator;
        return GDefaultAllocator;
    }
#ifdef __OBJC__
    template<> NSString* TString<char>::GetNSString() const
    {
        return NEKO_AUTORELEASE([[NSString alloc] initWithBytes:this->str length:Length() * sizeof(char) encoding:NSUTF16LittleEndianStringEncoding]);
    }
    
    
    template<> NSString* TString<wchar_t>::GetNSString() const
    {
        return NEKO_AUTORELEASE([[NSString alloc] initWithBytes:this->str length:Length() * sizeof(wchar_t) encoding:NSUTF16LittleEndianStringEncoding]);
    }
    
    
    /** Initializes TString with Objective-C NSString* */
    template<> TString<char>::TString(const NSString* In)
    : Allocator(GetDefaultStringAllocator())
    {
        if (In && [In length] > 0)
        {
            const CFStringEncoding Encoding = kCFStringEncodingUTF32LE; // 16?
            
            CFRange Range = CFRangeMake(0, CFStringGetLength((__bridge CFStringRef)In));
            CFIndex BytesNeeded;
            if (CFStringGetBytes((__bridge CFStringRef)In, Range, Encoding, '?', false, NULL, 0, &BytesNeeded) > 0)
            {
                const size_t Length = BytesNeeded / sizeof(char);
                this->InitEmpty();
                this->AllocateMemory((int32)Length);
                CFStringGetBytes((__bridge CFStringRef)In, Range, Encoding, '?', false, (uint8*)this->str, Length * sizeof(char) + 1, NULL);
                this->str[Length] = 0;
            }
        }
    }
#endif
    
    
    template<> int32 TString<wchar_t>::_strlen(const wchar_t *str)
    {
        return static_cast<int32>(wcslen(str));
    }
    
    template<> int32 TString<char>::_strlen(const char *str)
    {
        return static_cast<int32>(Neko::StringLength(str));
    }
    
    template<> mono_string TString<char>::_mono_str(const char *str)
    {
#ifdef MONO_API
        return mono_string_new(mono_domain_get(), str);
#else
        return nullptr;
#endif // MONO_API
    }
    
    template<> char *TString<char>::mono_string_native(mono_string str, void *error)
    {
        if (!str)
        {
            return nullptr;
        }
#ifdef MONO_API
        return mono_string_to_utf8_checked(static_cast<MonoString *>(str), static_cast<MonoError *>(error));
#else
        return nullptr;
#endif // MONO_API
    }
    
    template<> int TString<char>::_compare(const char *str1, const char *str2)
    {
        return strcmp(str1, str2);
    }
    
    template<> bool TString<char>::_contains_substring(const char *str0, const char *str1, bool ignoreCase)
    {
        if (!ignoreCase)
        {
            return FindSubstring(str0, str1) != nullptr;
        }
        
        
        // @todo Use FindISubstring
        int32 SuperstringLength = Neko::StringLength(str0);
        int32 SubstringLength   = Neko::StringLength(str1);
        
        for (int32 SubstringPos = 0; SubstringPos <= SuperstringLength - SubstringLength; ++SubstringPos)
        {
            if (Neko::CompareIStringN(str0 + SubstringPos, str1, SubstringLength) == 0)
            {
                return (str0 + SubstringPos) != nullptr;
            }
        }
        return false;
    }
    
    template<> mono_string TString<wchar_t>::_mono_str(const wchar_t *str)
    {
#ifdef MONO_API
        return mono_string_from_utf16(reinterpret_cast<mono_unichar2 *>(const_cast<wchar_t *>(str)));
#else
        return nullptr;
#endif // MONO_API
    }
    
    template<> wchar_t *TString<wchar_t>::mono_string_native(mono_string str, void* Error)
    {
#ifdef MONO_API
        // Initialize the error object with no error message in it.
        mono_error_init(static_cast<MonoError *>(Error));
        return reinterpret_cast<wchar_t *>(mono_string_to_utf16(str));
#else
        return nullptr;
#endif // MONO_API
    }
    
    template<> int TString<wchar_t>::_compare(const wchar_t *str1, const wchar_t *str2)
    {
        return wcscmp(str1, str2);
    }
    
    
    template<> bool TString<wchar_t>::_contains_substring(const wchar_t *str0, const wchar_t *str1, bool ignoreCase)
    {
        if (!ignoreCase)
        {
            return wcsstr(str0, str1) != nullptr;
        }
        
        int32 superstringLength = int32(wcslen(str0));
        int32 substringLength   = int32(wcslen(str1));
        
        for (int32 substringPos = 0; substringPos <= superstringLength - substringLength; ++substringPos)
        {
            if (/*wcsnicmp*/wcsncmp(str0 + substringPos, str1, substringLength) == 0)
            {
                return (str0 + substringPos) != nullptr;
            }
        }
        return false;
    }
    
    // String constructors with default allocators.
    
    template<> TString<char>::TString()
    : TString(GetDefaultStringAllocator())
    {
    }
    
    template<> TString<wchar_t>::TString()
    : TString(GetDefaultStringAllocator())
    {
    }
    
    template<> TString<char>::TString(const char *chars, int32 length)
    : TString(chars, length, GetDefaultAllocator())
    {
    }
    
    template<> TString<wchar_t>::TString(const wchar_t *chars, int32 length)
    : TString(chars, length, GetDefaultAllocator())
    {
    }
    
    template<> TString<char>::TString(int32 Length)
    : TString(Length, GetDefaultAllocator())
    {
    }
    
    template<> TString<wchar_t>::TString(int32 Length)
    : TString(Length, GetDefaultAllocator())
    {
    }

}
