//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Transaction.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "Sync.h"

namespace Neko
{
    namespace MT
    {
        /// Event based transaction.
        template <class T> struct Transaction
        {
            Transaction()
            : m_event(false)
            {
            }
            
            /** Triggers the event. */
            NEKO_FORCE_INLINE void SetCompleted()
            {
                m_event.Trigger();
            }
            /** Checks if event is done waiting. */
            NEKO_FORCE_INLINE bool IsCompleted()
            {
                return m_event.poll();
            }
            /** Event wait. */
            NEKO_FORCE_INLINE void WaitForCompletion()
            {
                return m_event.Wait();
            }
            
            NEKO_FORCE_INLINE void Reset()
            {
                m_event.Reset();
            }
            
            T			data;
            MT::Event	m_event;
        };
    } // namespace MT
} // namespace Neko
