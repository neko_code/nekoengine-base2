//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Sync.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Platform/SystemShared.h"

namespace Neko
{
    namespace MT
    {
#   if NEKO_WINDOWS_FAMILY
        typedef void* SemaphoreHandle;
        typedef void* MutexHandle;
        typedef void* EventHandle;
        typedef volatile int32 SpinMutexHandle;
#   elif NEKO_UNIX_FAMILY
        struct SemaphoreHandle
        {
            pthread_mutex_t mutex;
            pthread_cond_t cond;
            int32 count;
        };
        typedef pthread_mutex_t MutexHandle;
        struct EventHandle
        {
            pthread_mutex_t mutex;
            pthread_cond_t cond;
            bool signaled;
            bool manual_reset;
        };
        typedef volatile int32 SpinMutexHandle;
#   endif
        
        
        class NEKO_ENGINE_API Semaphore
        {
        public:
            Semaphore(int32 InitCount, int32 MaxCount);
            ~Semaphore();
            
            void signal();
            
            void Wait();
            bool poll();
            
        private:
            SemaphoreHandle Id;
        };
        
        
        class NEKO_ENGINE_API Event
        {
        public:
            explicit Event(const bool bManualReset);
            ~Event();
            
            void Reset();
            
            void Trigger();
            
            void WaitTimeout(uint32 TimeoutMs);
            void Wait();
            bool poll();
            
        private:
            EventHandle Id;
        };
        
        
        class NEKO_ENGINE_API SpinMutex
        {
        public:
            explicit SpinMutex(bool locked);
            ~SpinMutex();
            
            void Lock();
            bool poll();
            
            void Unlock();
            
        private:
            SpinMutexHandle Id;
        };
        
        
        class SpinLock
        {
        public:
            SpinLock(SpinMutex& mutex)
            : Mutex(mutex)
            {
                mutex.Lock();
            }
            ~SpinLock() { Mutex.Unlock(); }
            
        private:
            SpinLock(const SpinLock&);
            void operator=(const SpinLock&);
            SpinMutex& Mutex;
        };
    } // namespace MT
} // namespace Neko
