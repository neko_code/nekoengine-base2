//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  ThreadSafeCounter.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "Atomic.h"

namespace Neko
{
    class CThreadSafeCounter
    {
    public:
        
        typedef int32 IntegerType;
        
        
        CThreadSafeCounter()
        {
            Counter = 0;
        }
        
        
        CThreadSafeCounter(const CThreadSafeCounter& Other)
        {
            Counter = Other.GetValue();
        }
        
        
        CThreadSafeCounter(int32 Value)
        {
            Counter = Value;
        }
        
        /** Increments counter and returns value. */
        int32 Increment()
        {
            return Atomics::Increment(&Counter);
        }

        /** Adds an amount and returns the previous value. */
        int32 Add(int32 Amount)
        {
            return Atomics::Add(&Counter, Amount);
        }
        
        /** Decrements and returns new value. */
        int32 Decrement()
        {
            return Atomics::Decrement(&Counter);
        }
        
        /** Subtracts an amount and returns the previous value. */
        int32 Subract(int32 Amount)
        {
            return Atomics::Subtract(&Counter, Amount);
        }
        
        /** Sets the counter to a specific value and returns the previous value. */
        int32 Set(int32 Value)
        {
            return Atomics::Exchange(&Counter, Value);
        }
        
        /** Resets the counter's value to zero. */
        int32 Reset()
        {
            return Atomics::Exchange(&Counter, 0);
        }
        
        /** Gets the current counter value. */
        int32 GetValue() const
        {
            return Counter;
        }
        
    private:
        
        void operator=(const CThreadSafeCounter& Other) {}
        
        
        volatile int32 Counter;
    };
    
    /// Thread safe bool
    class CThreadSafeBool
            : private CThreadSafeCounter
    {
    public:
        /**
         * Constructor optionally takes value to initialize with, otherwise initializes false
         * @param bValue	Value to initialize with
         */
        CThreadSafeBool(bool bValue = false)
        : CThreadSafeCounter(bValue ? 1 : 0)
        {
        }
        
        /**
         * Operator to use this struct as a bool with thread safety
         */
        NEKO_FORCE_INLINE operator bool() const
        {
            return GetValue() != 0;
        }
        
        /**
         * Operator to set the bool value with thread safety
         */
        NEKO_FORCE_INLINE bool operator=(bool bNewValue)
        {
            Set(bNewValue ? 1 : 0);
            return bNewValue;
        }
        
        /**
         * Sets a new value atomically, and returns the old value.
         *
         * @param bNewValue   Value to set
         * @return The old value
         */
        NEKO_FORCE_INLINE bool AtomicSet(bool bNewValue)
        {
            return Set(bNewValue ? 1 : 0) != 0;
        }
    };
} // ~namespace Neko
