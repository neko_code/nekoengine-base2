//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Atomic.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"

namespace Neko
{
    namespace Atomics
    {
        NEKO_ENGINE_API int32 Increment(int32 volatile* value);
        NEKO_ENGINE_API int32 Decrement(int32 volatile* value);
        NEKO_ENGINE_API int32 Add(int32 volatile* addend, int32 value);
        NEKO_ENGINE_API int32 Subtract(int32 volatile* addend, int32 value);
        
        NEKO_ENGINE_API bool CompareAndExchange(int32 volatile* dest, int32 exchange, int32 comperand);
        NEKO_ENGINE_API bool CompareAndExchange64(int64 volatile* dest, int64 exchange, int64 comperand);
        
        
        NEKO_ENGINE_API int32 Exchange( volatile int32* Value, int32 Exchange );
        NEKO_ENGINE_API int64 Exchange( volatile int64* Value, int64 Exchange );
        
        NEKO_ENGINE_API void* InterlockedExchangePtr(void** Dest, void* Exchange);
        
        NEKO_ENGINE_API void MemoryBarrierFence();
    } // ~namespace MT
} // ~namespace Neko
