//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  LockFreeFixedQueue.h
//  Neko engine
//
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Data/IAllocator.h"
#include "../Neko.h"
#include "Atomic.h"
#include "Sync.h"

namespace Neko
{
    namespace MT
    {
        template <class T, int32 size>
        class LockFreeFixedQueue
        {
        public:
            LockFreeFixedQueue()
            : m_al(0)
            , m_fr(0)
            , Read(0)
            , Write(0)
            , m_aborted(false)
            , m_data_signal(0, size)
            {
                for (int32 i = 0; i < size; i++)
                {
                    m_alloc[i].data.pair.key = i;
                    m_alloc[i].data.pair.el = i;
                    m_queue[i].data.pair.key = i;
                    m_queue[i].data.pair.el = -1;
                }
            }
            
            ~LockFreeFixedQueue()
            {
            }
            
            T* Alloc(bool wait)
            {
                do
                {
                    if ((m_al - m_fr) < size)
                    {
                        int32 alloc_ptr = m_al;
                        int32 alloc_idx = alloc_ptr & (size - 1);
                        
                        Node cur_val(alloc_ptr, m_alloc[alloc_idx].data.pair.el);
                        
                        if (cur_val.data.pair.el > -1)
                        {
                            Node new_val(alloc_ptr, -1);
                            
                            if (Atomics::CompareAndExchange64(&m_alloc[alloc_idx].data.val, new_val.data.val, cur_val.data.val))
                            {
                                Atomics::Increment(&m_al);
                                auto* val = (T*)&Pool[cur_val.data.pair.el * sizeof(T)];
                                new (NewPlaceholder(), val) T();
                                return val;
                            }
                        }
                    }
                } while (wait);
                
                return nullptr;
            }
            
            void dealoc(T* tr)
            {
                tr->~T();
                int32 idx = int32(tr - (T*)Pool);
                assert(idx >= 0 && idx < size);
                
                Node cur_val(0, -1);
                Node new_val(0, idx);
                
                for(;;)
                {
                    int32 free_ptr = m_fr;
                    int32 free_idx = free_ptr & (size - 1);
                    
                    cur_val.data.pair.key = free_ptr;
                    new_val.data.pair.key = free_ptr + size;
                    if (Atomics::CompareAndExchange64(&m_alloc[free_idx].data.val, new_val.data.val, cur_val.data.val))
                    {
                        Atomics::Increment(&m_fr);
                        break;
                    }
                };
            }
            
            bool Push(const T* tr, bool wait)
            {
                int32 idx = int32(tr - (T*)Pool);
                assert(idx >= 0 && idx < size);
                
                do
                {
                    assert((Write - Read) < size);
                    
                    Node cur_node(0, -1);
                    Node newNode(0, idx);
                    
                    int32 cur_write_idx = Write;
                    int32 idx = cur_write_idx & (size - 1);
                    
                    cur_node.data.pair.key = cur_write_idx;
                    newNode.data.pair.key = cur_write_idx;
                    
                    if (Atomics::CompareAndExchange64(&m_queue[idx].data.val, newNode.data.val, cur_node.data.val))
                    {
                        Atomics::Increment(&Write);
                        m_data_signal.signal();
                        return true;
                    }
                } while (wait);
                
                return false;
            }
            
            T* Pop(bool wait)
            {
                bool can_read = wait ? m_data_signal.Wait(), wait : m_data_signal.poll();
                
                if (isAborted())
                {
                    return nullptr;
                }
                
                while (can_read)
                {
                    if (Read != Write)
                    {
                        int32 cur_read_idx = Read;
                        int32 idx = cur_read_idx & (size - 1);
                        
                        Node cur_node(cur_read_idx, m_queue[idx].data.pair.el);
                        
                        if (cur_node.data.pair.el > -1)
                        {
                            Node newNode(cur_read_idx + size, -1);
                            
                            if (Atomics::CompareAndExchange64(&m_queue[idx].data.val, newNode.data.val, cur_node.data.val))
                            {
                                Atomics::Increment(&Read);
                                return (T*)&Pool[cur_node.data.pair.el * sizeof(T)];
                            }
                        }
                    }
                }
                
                return nullptr;
            }
            
            
            bool isAborted() const
            {
                return m_aborted;
            }
            
            bool IsEmpty() const
            {
                return Read == Write;
            }
            
            void abort()
            {
                m_aborted = true;
                m_data_signal.signal();
            }
            
        private:
            
            struct Node
            {
                union
                {
                    struct
                    {
                        int32 key;
                        int32	el;
                    } pair;
                    int64		val;
                } data;
                
                Node()
                {
                }
                
                Node(int32 k, int32 i)
                {
                    data.pair.key = k;
                    data.pair.el = i;
                }
            };
            
            volatile int32	m_al;
            volatile int32	m_fr;
            volatile int32	Read;
            volatile int32	Write;
            Node				m_alloc[size];
            Node				m_queue[size];
            uint8				Pool[sizeof(T) * size];
            volatile bool		m_aborted;
            MT::Semaphore		m_data_signal;
        };
    } // ~namespace MT
} // ~namespace Neko
