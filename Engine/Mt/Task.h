//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Task.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Platform/SystemShared.h"


namespace Neko
{
    class IAllocator;
    namespace MT
    {
        /// Runnable threaded task.
        class NEKO_ENGINE_API Task
        {
        public:
            
            Task(IAllocator& allocator);
            
            virtual ~Task();
            
            virtual int32               DoTask() = 0;
            
            
            /** 
             * Creates this task with the given name.
             */
            bool Create(const char* Name);
            
            /** Shortcut for calling the entry point. */
            bool CreateSynchronous(const char* Name)
            {
                return DoTask() == 0;
            }
            
            /** 
             * Destroys this task. 
             */
            bool Destroy();
            
            /** Sets the thread's affinity mask. */
            void SetAffinityMask(uint64 NewAffinityMask);
            
            uint64 GetAffinityMask() const;
            
            
            /** Returns true if thread is active. */
            bool IsRunning() const;

            /** Returns true if thread has done working. */
            bool IsFinished() const;
            
            /** Returns true if thread is forced to exit. */
            bool IsForceExit() const;
            
            /** 
             * Quits this thread.
             * 
             * @param bWait Set to true if we should wait until thread will complete its work. 
             */
            void ForceExit(bool bWait);
            
        protected:
            
            IAllocator& GetAllocator();
            
        private:
            
            struct TaskImpl* Implementation;
        };
        
    } // namespace MT
} // namespace Neko

