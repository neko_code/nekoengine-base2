//
//          *                  *
//             __                *
//           ,db'    *     *
//          ,d8/       *        *    *
//          888
//          `db\       *     *
//            `o`_                    **
//         *               *   *    _      *
//               *                 / )
//             *    /\__/\ *       ( (  *
//           ,-.,-.,)    (.,-.,-.,-.) ).,-.,-.
//          | @|  ={      }= | @|  / / | @|o |
//         _j__j__j_)     `-------/ /__j__j__j_
//          ________(               /___________
//          |  | @| \              || o|O | @|
//          |o |  |,'\       ,   ,'"|  |  |  |  hjw
//          vV\|/vV|`-'\  ,---\   | \Vv\hjwVv\//v
//                     _) )    `. \ /
//                    (__/       ) )
//    _   _        _                                _
//   | \ | |  ___ | | __ ___     ___  _ __    __ _ (_) _ __    ___
//   |  \| | / _ \| |/ // _ \   / _ \| '_ \  / _` || || '_ \  / _ \
//   | |\  ||  __/|   <| (_) | |  __/| | | || (_| || || | | ||  __/
//   |_| \_| \___||_|\_\\___/   \___||_| |_| \__, ||_||_| |_| \___|
//                                           |___/
//  Thread.h
//  Neko engine
//
//  Created by Neko.
//  Copyright © 2014 Neko Vision. All rights reserved.
//

#pragma once

#include "../Neko.h"
#include "../Platform/SystemShared.h"

namespace Neko
{
#if NEKO_WINDOWS_FAMILY
    typedef uint32 ThreadID;
#else
    typedef uint64 ThreadID;
#endif
    
    namespace MT
    {
        NEKO_ENGINE_API void SetThreadName(ThreadID threadId, const char* thread_name);
        NEKO_ENGINE_API void Sleep(uint32 Milliseconds);
        NEKO_ENGINE_API void YieldCpu();
        
        NEKO_ENGINE_API ThreadID GetCurrentThreadID();
        NEKO_ENGINE_API uint32 GetThreadAffinityMask();
        
    } // namespace MT
} // namespace Neko
